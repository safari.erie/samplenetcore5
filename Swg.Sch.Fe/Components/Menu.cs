using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;
using Swg.Models;
using Swg.Models.Constants;
using Swg.Services;
using Swg.Sch.Shared.Interfaces;

namespace Swg.Sch.Fe.Components
{
    public class Menu : ViewComponent
    {
        public const string OPEN_LIST_TAG = "<ul>";
        public const string CLOSE_LIST_TAG = "</ul>";
        public const string OPEN_LIST_ITEM_TAG = "<li>";
        public const string CLOSE_LIST_ITEM_TAG = "</li>";
        public const string OPEN_LIST_ITEM_TAG_CLASS = "<li class='nav-item'>";
        public const string OPEN_LIST_UL_CLASS_DROPDOWN = "<ul class='dropdown-menu'>";

        private readonly IUserAppService applTask;
        private readonly IWebService webService;
        IList<ApplTaskModel> allMenuItems;
        // Constructor
        public Menu(IUserAppService ApplTask, IWebService WebService)
        {
            applTask = ApplTask;
            webService = WebService;
        }
        public IViewComponentResult Invoke()
        {
            var ret = webService.GetPropen(0, out string oMessage);
            var sekolah = webService.GetProfileSekolah(out oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Kegiatan = ret.Kegiatan.Count != 0 ? ret.Kegiatan.Take(10).ToList() : null;
                ViewBag.Sekolah = sekolah;
            }
            return View();
        }
    }
}