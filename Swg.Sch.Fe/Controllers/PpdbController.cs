using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swg.Sch.Fe.Models;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Web.Models;

namespace Swg.Sch.Fe.Controllers
{
    public class PpdbController : Controller
    {
        private readonly ILogger<PpdbController> _logger;
        private readonly IWebService webService;
        private readonly IPpdbService ppdbService;
        private readonly ISchService schService;
        public PpdbController(
            ILogger<PpdbController> logger,
            IWebService WebService,
            IPpdbService PpdbService,
            ISchService SchService)
        {
            webService = WebService;
            ppdbService = PpdbService;
            schService = SchService;
            _logger = logger;
        }

        public IActionResult Index()
        {
            ViewBag.ModuleJs = ModuleJs("ppdb"); return View();
        }

        public IActionResult Daftar()
        {
            var webnopanitiappdb = webService.GetApplSetting("webnopanitiappdb", out string oMsg_webnopanitiappdb);

            if (string.IsNullOrEmpty(oMsg_webnopanitiappdb)) ViewBag.webnopanitiappdb = webnopanitiappdb.ValueString;
            ViewBag.ModuleJs = ModuleJs("ppdb"); return View("Index");
        }

        public IActionResult Formulir()
        {
            var webnopanitiappdb = webService.GetApplSetting("webnopanitiappdb", out string oMsg_webnopanitiappdb);

            if (string.IsNullOrEmpty(oMsg_webnopanitiappdb)) ViewBag.webnopanitiappdb = webnopanitiappdb.ValueString;
            ViewBag.ModuleJs = ModuleJs("formulir"); return View();
        }

        public IActionResult Aturan()
        {
            return View();
        }

        public IActionResult Alur()
        {
            return View();
        }

        public IActionResult Biaya()
        {
            return View();
        }

        public IActionResult Pengumuman()
        {
            return View();
        }

        public IActionResult Brosur()
        {
            return View();
        }

        public IActionResult PetunjukTeknis()
        {
            return View();
        }
        public IActionResult Informasi()
        {
            var konten = ppdbService.GetPpdbKontens(out string oMessage);
            if (string.IsNullOrEmpty(oMessage)) ViewBag.Konten = konten;
            return View();
        }
        public IActionResult KirimLembarKomitmen(int IdPpdbDaftar)
        {
            var cekPpdb = ppdbService.GetPpdbDaftar(IdPpdbDaftar, out string oMessage);
            ViewBag.IdPpdbDaftar = IdPpdbDaftar;
            var webnopanitiappdb = webService.GetApplSetting("webnopanitiappdb", out string oMsg_webnopanitiappdb);
            if (string.IsNullOrEmpty(oMsg_webnopanitiappdb)) ViewBag.webnopanitiappdb = webnopanitiappdb.ValueString;
            ViewBag.ModuleJs = ModuleJs("kirimlembarkomitmen");
            if (string.IsNullOrEmpty(oMessage))
            {
                return View();
            }
            else
            {
                // return View();
                return RedirectToAction("Index", "Home");
            }
        }

        public IActionResult DaftarPeminat()
        {
            var webnopanitiappdb = webService.GetApplSetting("webnopanitiappdb", out string oMsg_webnopanitiappdb);

            if (string.IsNullOrEmpty(oMsg_webnopanitiappdb)) ViewBag.webnopanitiappdb = webnopanitiappdb.ValueString;
            ViewBag.ModuleJs = ModuleJs("daftarpeminat"); return View();
        }
        public IActionResult CekStatus()
        {
            ViewBag.ModuleJs = ModuleJs("cekstatus"); return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public List<LibraryJsModel> ModuleJs(string Module)
        {
            List<LibraryJsModel> js = new List<LibraryJsModel>();
            LibraryJsModel ppdb = new LibraryJsModel
            {
                NameJs = "index.js",
                TypeJs = "Register",
                Path = "Ppdb"
            };
            LibraryJsModel cekstatus = new LibraryJsModel
            {
                NameJs = "index.js",
                TypeJs = "CekStatus",
                Path = "Ppdb"
            };
            LibraryJsModel wawancara = new LibraryJsModel
            {
                NameJs = "index.js",
                TypeJs = "Wawancara",
                Path = "Ppdb"
            };
            LibraryJsModel formulir = new LibraryJsModel
            {
                NameJs = "index.js",
                TypeJs = "Formulir",
                Path = "Ppdb"
            };
            LibraryJsModel daftarpeminat = new LibraryJsModel
            {
                NameJs = "index.js",
                TypeJs = "DaftarPeminat",
                Path = "Ppdb"
            };
            LibraryJsModel kirimlembarkomitmen = new LibraryJsModel
            {
                NameJs = "index.js",
                TypeJs = "KirimLembarKomitmen",
                Path = "Ppdb"
            };

            if (Module == "ppdb")
            {
                js.Add(ppdb);
            }
            if (Module == "cekstatus")
            {
                js.Add(cekstatus);
            }
            if (Module == "wawancara")
            {
                js.Add(wawancara);
            }
            if (Module == "formulir")
            {
                js.Add(formulir);
            }
            if (Module == "daftarpeminat")
            {
                js.Add(daftarpeminat);
            }
            if (Module == "kirimlembarkomitmen")
            {
                js.Add(kirimlembarkomitmen);
            }

            return js;
        }
    }
}
