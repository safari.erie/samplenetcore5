using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swg.Sch.Fe.Models;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Web.Models;

namespace Swg.Sch.Fe.Controllers
{
    public class OtherController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IWebService webService;

        public OtherController(ILogger<HomeController> logger, IWebService WebService)
        {
            _logger = logger;
            webService = WebService;
        }

        public IActionResult Testimoni()
        {
            var ret = webService.GetTestimonis(out string oMessage);

            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Data = ret;
            }
            return View();
        }
        public IActionResult BuatTestimoni()
        {
            ViewBag.ModuleJs = ModuleJs("BuatTestimoni"); return View();
        }
        public IActionResult HubungiKami()
        {
            var ret = webService.GetProfSekolah(out string oMessage);

            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Sekolah = ret;
            }
            return View();
        }
        public IActionResult PressRelease()
        {
            return View();
        }
        public IActionResult DetailPressRelease()
        {
            return View();
        }
        public IActionResult Event()
        {
            return View();
        }
        public IActionResult DetailEvent()
        {
            return View();
        }
        public IActionResult DetailEkskul(int Id)
        {
            var ret = webService.GetEkskul(Id, out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Ekskul = ret;
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public List<LibraryJsModel> ModuleJs(string Module)
        {
            List<LibraryJsModel> js = new List<LibraryJsModel>();
            LibraryJsModel BuatTestimoni = new LibraryJsModel
            {
                NameJs = "BuatTestimoni.js",
                TypeJs = "Testimoni",
                Path = "Landing"
            };
            if (Module == "BuatTestimoni")
            {
                js.Add(BuatTestimoni);
            }
            return js;
        }
    }
}
