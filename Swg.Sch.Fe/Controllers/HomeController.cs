﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swg.Sch.Fe.Models;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Web.Models;

namespace Swg.Sch.Fe.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IWebService webService;
        private readonly ISchService schService;

        public HomeController(ILogger<HomeController> logger, IWebService WebService, ISchService SchService)
        {
            _logger = logger;
            webService = WebService;
            schService = SchService;
        }

        public IActionResult Index()
        {
            var ret = webService.GetPropen(0, out string oMessage);
            var ta = schService.GetTahunAjaranPpdb(out oMessage);
            var webppdbtampil = webService.GetApplSetting("webppdbtampil", out oMessage);
            var webppdbgambarberanda = webService.GetApplSetting("webppdbgambarberanda", out oMessage);
            var webberandasecjudul1 = webService.GetApplSetting("webberandasecjudul1", out oMessage);
            var webberandasecket1 = webService.GetApplSetting("webberandasecket1", out oMessage);

            var webberandasecjudul2 = webService.GetApplSetting("webberandasecjudul2", out oMessage);
            var webberandasecket2 = webService.GetApplSetting("webberandasecket2", out oMessage);
            var webberandasec2bg1 = webService.GetApplSetting("webberandasec2bg1", out oMessage);
            var webberandasec2bg2 = webService.GetApplSetting("webberandasec2bg2", out oMessage);
            var webberandasec2judul1 = webService.GetApplSetting("webberandasec2judul1", out oMessage);
            var webberandasec2ket1 = webService.GetApplSetting("webberandasec2ket1", out oMessage);
            var webberandasec2judul2 = webService.GetApplSetting("webberandasec2judul2", out oMessage);
            var webberandasec2ket2 = webService.GetApplSetting("webberandasec2ket2", out oMessage);
            var webberandasec2judul3 = webService.GetApplSetting("webberandasec2judul3", out oMessage);
            var webberandasec2ket3 = webService.GetApplSetting("webberandasec2ket3", out oMessage);
            var webberandasec2judul4 = webService.GetApplSetting("webberandasec2judul4", out oMessage);
            var webberandasec2ket4 = webService.GetApplSetting("webberandasec2ket4", out oMessage);
            var webberandasec2judul5 = webService.GetApplSetting("webberandasec2judul5", out oMessage);
            var webberandasec2ket5 = webService.GetApplSetting("webberandasec2ket5", out oMessage);
            var webberandasec2judul6 = webService.GetApplSetting("webberandasec2judul6", out oMessage);
            var webberandasec2ket6 = webService.GetApplSetting("webberandasec2ket6", out oMessage);

            var banner = schService.GetBanners("web", out oMessage);
            var testimoni = webService.GetTestimonis(out string oMessageTestimoni);

            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Kegiatan = ret.Kegiatan;
                ViewBag.Tkit = ret.Tkit;
                ViewBag.Sdit = ret.Sdit;
                ViewBag.Smpit = ret.Smpit;
                ViewBag.Smait = ret.Smait;
                ViewBag.TahunAjaran = ta;
                ViewBag.WebPpdbTampil = webppdbtampil;
                ViewBag.WebPpdbGambarBeranda = webppdbgambarberanda;
                ViewBag.WebBerandaSecJudul1 = webberandasecjudul1;
                ViewBag.WebBerandaSecKet1 = webberandasecket1;

                ViewBag.WebBerandaSecJudul2 = webberandasecjudul2;
                ViewBag.WebBerandaSecKet2 = webberandasecket2;
                ViewBag.WebBerandaSec2Bg1 = webberandasec2bg1;
                ViewBag.WebBerandaSec2Bg2 = webberandasec2bg2;
                ViewBag.WebBerandaSec2Judul1 = webberandasec2judul1;
                ViewBag.WebBerandaSec2Ket1 = webberandasec2ket1;
                ViewBag.WebBerandaSec2Judul2 = webberandasec2judul2;
                ViewBag.WebBerandaSec2Ket2 = webberandasec2ket2;
                ViewBag.WebBerandaSec2Judul3 = webberandasec2judul3;
                ViewBag.WebBerandaSec2Ket3 = webberandasec2ket3;
                ViewBag.WebBerandaSec2Judul4 = webberandasec2judul4;
                ViewBag.WebBerandaSec2Ket4 = webberandasec2ket4;
                ViewBag.WebBerandaSec2Judul5 = webberandasec2judul5;
                ViewBag.WebBerandaSec2Ket5 = webberandasec2ket5;
                ViewBag.WebBerandaSec2Judul6 = webberandasec2judul6;
                ViewBag.WebBerandaSec2Ket6 = webberandasec2ket6;

                ViewBag.TotSiswaAktif = ret.Tkit.TotSiswa + ret.Sdit.TotSiswa + ret.Smpit.TotSiswa + ret.Smait.TotSiswa;
                ViewBag.TotGuruAktif = ret.Staff.Count();
                ViewBag.TotAlumni = ret.TotAlumni;
                ViewBag.TotSiswaPrestasi = ret.TotSiswaPrestasi;
                ViewBag.BannerIklan = banner;
            }
            if (string.IsNullOrEmpty(oMessageTestimoni))
            {
                ViewBag.Testimoni = testimoni;
            }
            ViewBag.ModuleJs = ModuleJs("home");
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public List<LibraryJsModel> ModuleJs(string Module)
        {
            List<LibraryJsModel> js = new List<LibraryJsModel>();
            LibraryJsModel home = new LibraryJsModel
            {
                NameJs = "index.js",
                TypeJs = "Home",
                Path = "Landing"
            };
            if (Module == "home")
            {
                js.Add(home);
            }
            return js;
        }
    }
}
