using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swg.Sch.Fe.Models;
using Swg.Sch.Shared.Interfaces;

namespace Swg.Sch.Fe.Controllers
{
    public class BiografiKepsekController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IWebService webService;

        public BiografiKepsekController(ILogger<HomeController> logger, IWebService WebService)
        {
            _logger = logger;
            webService = WebService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Tkit()
        {
            var ret = webService.GetPropen(1, out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Kegiatan = ret.Kegiatan;
                ViewBag.Guru = ret.Staff;
                ViewBag.Tkit = ret.Tkit;
            }
            return View();
        }
        public IActionResult Sdit()
        {
            var ret = webService.GetPropen(2, out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Kegiatan = ret.Kegiatan;
                ViewBag.Guru = ret.Staff;
                ViewBag.Sdit = ret.Sdit;
            }
            return View();
        }
        public IActionResult Smpit()
        {
            var ret = webService.GetPropen(3, out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Kegiatan = ret.Kegiatan;
                ViewBag.Guru = ret.Staff;
                ViewBag.Smpit = ret.Smpit;
            }
            return View();
        }
        public IActionResult Smait()
        {
            var ret = webService.GetPropen(4, out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Kegiatan = ret.Kegiatan;
                ViewBag.Guru = ret.Staff;
                ViewBag.Smait = ret.Smait;
            }
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
