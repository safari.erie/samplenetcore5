using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Models;
using Swg.Models.Constants;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;

namespace Swg.Sch.Fe.Controllers
{
    [Route("api/[controller]")]
    public class WebsController : Controller
    {
        private readonly IWebService webService;
        private readonly IKelasService kelasService;
        private readonly IUserAppService applTask;
        private readonly ISchService schService;
        private readonly IPpdbService ppdbService;
        private readonly IPegawaiService pegawaiService;
        public WebsController(
            IWebService WebService,
            IKelasService KelasService,
            IUserAppService ApplTask,
            ISchService SchService,
            IPpdbService PpdbService,
            IPegawaiService PegawaiService)
        {
            webService = WebService;
            kelasService = KelasService;
            applTask = ApplTask;
            schService = SchService;
            ppdbService = PpdbService;
            pegawaiService = PegawaiService;
        }
        #region InfoWeb
        [HttpPost("AddTestimoni", Name = "AddTestimoni")]
        public ResponseModel<string> AddTestimoni(WebTestimoniAddModel Data)
        {

            var ret = webService.AddTestimoni(Data, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetMenus", Name = "GetMenus")]
        public ResponseModel<List<ApplTaskModel>> GetMenus()
        {

            var ret = applTask.GetMenus(JenisAplikasiConstant.FrontendWeb, "default", out string oMessage);
            return new ResponseModel<List<ApplTaskModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetHome", Name = "GetHome")]
        public ResponseModel<WebHomeModel> GetHome()
        {

            var ret = webService.GetHome(out string oMessage);
            return new ResponseModel<WebHomeModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetAbout", Name = "GetAbout")]
        public ResponseModel<WebAboutModel> GetAbout()
        {

            var ret = webService.GetAbout(out string oMessage);
            return new ResponseModel<WebAboutModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetPropen", Name = "GetPropen")]
        public ResponseModel<WebPropenModel> GetPropen(int IdUnit)
        {

            var ret = webService.GetPropen(IdUnit, out string oMessage);
            return new ResponseModel<WebPropenModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetPpdbInfo", Name = "GetPpdbInfo")]
        public ResponseModel<WebPpdbListInfoModel> GetPpdbInfo()
        {

            var ret = webService.GetPpdbInfo(out string oMessage);
            return new ResponseModel<WebPpdbListInfoModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetSiswaRapors", Name = "GetSiswaRapors")]
        public ResponseModel<List<SiswaRaporModel>> GetSiswaRapors(string Nisn)
        {
            var ret = webService.GetSiswaRapors(Nisn, out string oMessage);
            return new ResponseModel<List<SiswaRaporModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetSiswaRapor", Name = "GetSiswaRapor")]
        public ResponseModel<SiswaRaporModel> GetSiswaRapor(int IdSiswa, int IdJenisRapor)
        {
            var ret = webService.GetSiswaRapor(IdSiswa, IdJenisRapor, out string oMessage);
            return new ResponseModel<SiswaRaporModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetAgamas", Name = "GetAgamas")]
        public ResponseModel<List<ComboModel>> GetAgamas()
        {
            var ret = schService.GetAgamas();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetJenisWargaNegara", Name = "GetJenisWargaNegara")]
        public ResponseModel<List<ComboModel>> GetJenisWargaNegara()
        {
            var ret = schService.GetJenisWargaNegara();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetTinggalSiswa", Name = "GetTinggalSiswa")]
        public ResponseModel<List<ComboModel>> GetTinggalSiswa()
        {
            var ret = schService.GetTinggalSiswa();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetJenisTransportasi", Name = "GetJenisTransportasi")]
        public ResponseModel<List<ComboModel>> GetJenisTransportasi()
        {
            var ret = schService.GetJenisTransportasi();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetJenisBahasa", Name = "GetJenisBahasa")]
        public ResponseModel<List<ComboModel>> GetJenisBahasa()
        {
            var ret = schService.GetJenisBahasa();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }
        [HttpGet("GetPpdbSeragams", Name = "GetPpdbSeragams")]
        public ResponseModel<List<PpdbSeragamListModel>> GetPpdbSeragams()
        {
            var ret = ppdbService.GetPpdbSeragams(1, out string oMessage);
            return new ResponseModel<List<PpdbSeragamListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        #endregion

        #region Ppdb

        [HttpGet("GetKategoriDaftars", Name = "GetKategoriDaftars")]
        public ResponseModel<List<ComboModel>> GetKategoriDaftars()
        {

            var ret = schService.GetKategoriDaftars();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }
        [HttpGet("GetUnits", Name = "GetUnits")]
        public ResponseModel<List<ComboModel>> GetUnits()
        {

            var ret = schService.GetUnits();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetKelass", Name = "GetKelass")]
        public ResponseModel<List<KelasModel>> GetKelass(int IdUnit)
        {

            var ret = kelasService.GetKelass(IdUnit, out string oMessage);
            return new ResponseModel<List<KelasModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetKelasParalels", Name = "GetKelasParalels")]
        public ResponseModel<List<KelasParalelModel>> GetKelasParalels(int IdKelas)
        {

            var ret = kelasService.GetKelasParalels(IdKelas, out string oMessage);
            return new ResponseModel<List<KelasParalelModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetJenisPendaftarans", Name = "GetJenisPendaftarans")]
        public ResponseModel<List<ComboModel>> GetJenisPendaftarans()
        {

            var ret = schService.GetJenisPendaftarans();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetJalurPendaftarans", Name = "GetJalurPendaftarans")]
        public ResponseModel<List<ComboModel>> GetJalurPendaftarans()
        {

            var ret = schService.GetJalurPendaftarans();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetJenisPrestasis", Name = "GetJenisPrestasis")]
        public ResponseModel<List<ComboModel>> GetJenisPrestasis()
        {

            var ret = schService.GetJenisPrestasis();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetJenisPekerjaans", Name = "GetJenisPekerjaans")]
        public ResponseModel<List<ComboModel>> GetJenisPekerjaans()
        {

            var ret = schService.GetJenisPekerjaans(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }


        [HttpGet("GetTingkatPrestasiSiswas", Name = "GetTingkatPrestasiSiswas")]
        public ResponseModel<List<ComboModel>> GetTingkatPrestasiSiswas()
        {

            var ret = schService.GetTingkatPrestasiSiswas();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetSiswaByNis", Name = "GetSiswaByNis")]
        public ResponseModel<SiswaModel> GetSiswaByNis(string Nis)
        {
            var ret = webService.GetSiswaByNis(Nis, out string oMessage);
            return new ResponseModel<SiswaModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetKelasPpdb", Name = "GetKelasPpdb")]
        public ResponseModel<List<ComboModel>> GetKelasPpdb(int IdUnit)
        {

            var ret = schService.GetKelasPpdb(IdUnit, out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetKelasPpdbs", Name = "GetKelasPpdbs")]
        public ResponseModel<List<ComboModel>> GetKelasPpdbs()
        {

            var ret = schService.GetKelasPpdbs(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetPpdbDaftar", Name = "GetPpdbDaftar")]
        public ResponseModel<PpdbDaftarModel> GetPpdbDaftar(int IdPpdbDaftar)
        {

            var ret = ppdbService.GetPpdbDaftar(IdPpdbDaftar, out string oMessage);
            return new ResponseModel<PpdbDaftarModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetPpdbDaftarByPin", Name = "GetPpdbDaftarByPin")]
        public ResponseModel<PpdbDaftarModel> GetPpdbDaftarByPin(int IdPpdbDaftar, string KodePin)
        {
            var ret = ppdbService.GetPpdbDaftar(IdPpdbDaftar, KodePin, out string oMessage);
            return new ResponseModel<PpdbDaftarModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetPpdbSiswa", Name = "GetPpdbSiswa")]
        public ResponseModel<PpdbSiswaModel> GetPpdbSiswa(int IdPpdbDaftar)
        {

            var ret = ppdbService.GetPpdbSiswa(IdPpdbDaftar, out string oMessage);
            return new ResponseModel<PpdbSiswaModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetPpdbSeragamByUnit", Name = "GetPpdbSeragamByUnit")]
        public ResponseModel<List<PpdbSeragamListModel>> GetPpdbSeragamByUnit(int IdUnit)
        {

            var ret = ppdbService.GetPpdbSeragamByUnit(IdUnit, out string oMessage);
            return new ResponseModel<List<PpdbSeragamListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("UploadFileAkteKk", Name = "UploadFileAkteKk")]
        public ResponseModel<string> UploadFileAkteKk(int IdPpdbDaftar, IFormFile FileAkte, IFormFile FileKK)
        {
            var ret = ppdbService.UploadFileAkteKk(IdPpdbDaftar, FileAkte, FileKK);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        [HttpPost("Registrasi", Name = "Registrasi")]
        public ResponseModel<string> Registrasi(PpdbDaftarAddModel Data)
        {
            var ret = schService.Registrasi(Data, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("InputFormulir", Name = "InputFormulir")]
        public ResponseModel<string> InputFormulir(
            PpdbSiswaAddModel Siswa,
            List<PpdbSiswaOrtuAddModel> Ortus,
            List<PpdbSiswaRaporAddModel> Rapors,
            List<PpdbSiswaPrestasiAddModel> Prestasis,
            List<PpdbKuisionerJawabanAddModel> Kuisioners,
            List<PpdbBiayaSiswaDetilAddModel> Biayas,
            IFormFile FileFoto,
            IFormFile FileKtpIbu,
            IFormFile FileKtpAyah,
            IFormFile FileKk,
            IFormFile FileAkte,
            IFormFile FileSuratKesehatan,
            IFormFile FileBebasNarkoba,
            List<IFormFile> FileRapors,
            List<IFormFile> FilePrestasis,
            PpdbSiswaKkModel SiswaKk
        )
        {
            int IdUser = 1; //iduser public lihat di table public.tb_user
            var ret = schService.InputFormulir(
                IdUser,
                Siswa,
                Ortus,
                Rapors,
                Prestasis,
                Kuisioners,
                Biayas,
                FileFoto,
                FileKtpIbu,
                FileKtpAyah,
                FileKk,
                FileAkte,
                FileSuratKesehatan,
                FileBebasNarkoba,
                FileRapors,
                FilePrestasis,
                SiswaKk);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        [HttpGet("GetPertanyaanKuisioner", Name = "GetPertanyaanKuisioner")]
        public ResponseModel<List<PpdbKuisionerModel>> GetPertanyaanKuisioner(int IdUnit, int IdJenisPendaftaran, int idJalurPendaftaran, int IdJenisPertanyaan)
        {

            var ret = ppdbService.GetPertanyaanKuisioner(IdUnit, IdJenisPendaftaran, idJalurPendaftaran, IdJenisPertanyaan, out string oMessage);
            return new ResponseModel<List<PpdbKuisionerModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetPpdbBiayaByUnit", Name = "GetPpdbBiayaByUnit")]
        public ResponseModel<List<PpdbBiayaModel>> GetPpdbBiayaByUnit(int IdPpdbKelas)
        {

            var ret = ppdbService.GetPpdbBiayaByUnit(IdPpdbKelas, out string oMessage);
            return new ResponseModel<List<PpdbBiayaModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetListJenjangPendidikan", Name = "GetListJenjangPendidikan")]
        public ResponseModel<List<ComboModel>> GetListJenjangPendidikan()
        {

            var ret = pegawaiService.GetListJenjangPendidikan(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        /*
        [HttpPost("Registrasi", Name = "Registrasi")]
        public ResponseModel<string> Registrasi(
            PpdbDaftarAddModel Data,
            List<PpdbPrestasiSiswaModel> Prestasis,
            List<PpdbRaportModel> FileRaports,
            IFormFile FileFoto,
            IFormFile FileKtpIbu,
            IFormFile FileKtpAyah,
            IFormFile FileKk,
            IFormFile FileAkte,
            IFormFile FileSuratKesehatan,
            IFormFile FileBebasNarkoba)
        {
            int IdUser = 1; //iduser public lihat di table public.tb_user
            var ret = schService.Registrasi(
                IdUser,
                Data,
                Prestasis,
                FileRaports,
                FileFoto,
                FileKtpIbu,
                FileKtpAyah,
                FileKk,
                FileAkte,
                FileSuratKesehatan,
                FileBebasNarkoba, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        
        [HttpGet("GetPpdbByEmail", Name = "GetPpdbByEmail")]
        public ResponseModel<List<PpdbDaftarListModel>> GetPpdbByEmail(string Email)
        {

            var ret = schService.GetPpdbByEmail(Email, out string oMessage);
            return new ResponseModel<List<PpdbDaftarListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetPpdbSeragam", Name = "GetPpdbSeragam")]
        public ResponseModel<List<PpdbSeragamListModel>> GetPpdbSeragam(int IdUnit)
        {

            var ret = schService.GetPpdbSeragamWeb(IdUnit, out string oMessage);
            return new ResponseModel<List<PpdbSeragamListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetPpdbDaftar", Name = "GetPpdbDaftar")]
        public ResponseModel<PpdbDaftarModel> GetPpdbDaftar(int IdPpdbDaftar)
        {
            var ret = schService.GetPpdbDaftar(IdPpdbDaftar, out string oMessage);
            return new ResponseModel<PpdbDaftarModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        */

        #endregion

        [HttpPost("DaftarPeminatPpdb", Name = "DaftarPeminatPpdb")]
        public ResponseModel<string> DaftarPeminatPpdb(AddPpdbDaftarPeminat Data)
        {

            var ret = ppdbService.DaftarPeminatPpdb(Data, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("UploadLembarKomitmen", Name = "UploadLembarKomitmen")]
        public ResponseModel<string> UploadLembarKomitmen(int IdPpdbDaftar, string KodePin, IFormFile FileLembarKomitmen)
        {
            var ret = ppdbService.UploadLembarKomitmen(IdPpdbDaftar, KodePin, FileLembarKomitmen, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetBanners", Name = "GetBanners")]
        public ResponseModel<List<BannerModel>> GetBanners()
        {
            List<BannerModel> ret = schService.GetBanners("web", out string oMessage);
            return new ResponseModel<List<BannerModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetProvinsi", Name = "GetProvinsi" + "Webs")]
        public ResponseModel<List<ComboModel>> GetProvinsi()
        {
            List<ComboModel> ret = schService.GetProvinsi(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKabkot", Name = "GetKabkot" + "Webs")]
        public ResponseModel<List<ComboModel>> GetKabkot(int IdProvinsi)
        {
            List<ComboModel> ret = schService.GetKabkot(IdProvinsi, out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKecamatan", Name = "GetKecamatan" + "Webs")]
        public ResponseModel<List<ComboModel>> GetKecamatan(int IdKabkot)
        {
            List<ComboModel> ret = schService.GetKecamatan(IdKabkot, out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetDesa", Name = "GetDesa" + "Webs")]
        public ResponseModel<List<ComboModel>> GetDesa(int IdKecamatan)
        {
            List<ComboModel> ret = schService.GetDesa(IdKecamatan, out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetJenisHobis", Name = "GetJenisHobis")]
        public ResponseModel<List<ComboModel>> GetJenisHobis()
        {
            var ret = schService.GetJenisHobis(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetJenisCita2s", Name = "GetJenisCita2s")]
        public ResponseModel<List<ComboModel>> GetJenisCita2s()
        {
            var ret = schService.GetJenisCita2s(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
    }
}
