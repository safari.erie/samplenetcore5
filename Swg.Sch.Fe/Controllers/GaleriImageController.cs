using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swg.Sch.Fe.Models;
using Swg.Sch.Shared.Interfaces;

namespace Swg.Sch.Fe.Controllers
{
    public class GaleriImageController : Controller
    {
        private readonly ILogger<GaleriImageController> _logger;
        private readonly IWebService webService;

        public GaleriImageController(ILogger<GaleriImageController> logger, IWebService WebService)
        {
            _logger = logger;
            webService = WebService;
        }

        public IActionResult Index()
        {
            return RedirectToAction("index", "home");
        }

        public IActionResult Tkit()
        {
            var ret1 = webService.GetPropen(1, out string oMessage1);
            if (string.IsNullOrEmpty(oMessage1))
            {
                ViewBag.ImageList = ret1.GaleriImage;
            }

            return View();
        }
        public IActionResult Sdit()
        {
            var ret1 = webService.GetPropen(2, out string oMessage1);
            if (string.IsNullOrEmpty(oMessage1))
            {
                ViewBag.ImageList = ret1.GaleriImage;
            }

            return View();
        }
        public IActionResult Smpit()
        {
            var ret1 = webService.GetPropen(3, out string oMessage1);
            if (string.IsNullOrEmpty(oMessage1))
            {
                ViewBag.ImageList = ret1.GaleriImage;
            }
            return View();
        }
        public IActionResult Smait()
        {
            var ret1 = webService.GetPropen(4, out string oMessage1);
            if (string.IsNullOrEmpty(oMessage1))
            {
                ViewBag.ImageList = ret1.GaleriImage;
            }
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
