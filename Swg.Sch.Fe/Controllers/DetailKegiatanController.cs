using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swg.Sch.Fe.Models;
using Swg.Sch.Shared.Interfaces;

namespace Swg.Sch.Fe.Controllers
{
    public class DetailKegiatanController : Controller
    {
        private readonly ILogger<DetailKegiatanController> _logger;
        private readonly IWebService webService;

        public DetailKegiatanController(ILogger<DetailKegiatanController> logger, IWebService WebService)
        {
            _logger = logger;
            webService = WebService;
        }

        public IActionResult Index()
        {
            return RedirectToAction("index", "home");
        }

        public IActionResult Tkit(string Read = null)
        {
            if (Read != null)
            {
                var ret = webService.GetKegiatanByUrl(Read, out string oMessage);
                if (string.IsNullOrEmpty(oMessage)) ViewBag.KegiatanByUrl = ret.KegiatanByUrl;
                var ret1 = webService.GetPropen(1, out string oMessage1);
                if (string.IsNullOrEmpty(oMessage1))
                {
                    ViewBag.KegiatanList = ret1.Kegiatan.Take(3);
                    ViewBag.ImageList = ret1.GaleriImage.Take(8);
                    ViewBag.VideoList = ret1.GaleriVideo.Take(8);
                }

                else
                {
                    return RedirectToAction("index", "home");
                }
            }
            else
            {
                return RedirectToAction("index", "home");
            }
            return View();
        }
        // public IActionResult Sdit(string Read = null)
        // {
        //     if (Read != null)
        //     {
        //         var ret = webService.GetKegiatanByUrl(Read, out string oMessage);
        //         if (string.IsNullOrEmpty(oMessage)) ViewBag.KegiatanByUrl = ret.KegiatanByUrl;
        //         var ret1 = webService.GetPropen(2, out string oMessage1);
        //         if (string.IsNullOrEmpty(oMessage1))
        //         {
        //             ViewBag.KegiatanList = ret1.Kegiatan.Take(3);
        //             ViewBag.ImageList = ret1.GaleriImage.Take(8);
        //             ViewBag.VideoList = ret1.GaleriVideo.Take(8);
        //         }

        //         else
        //         {
        //             return RedirectToAction("index", "home");
        //         }
        //     }
        //     else
        //     {
        //         return RedirectToAction("index", "home");
        //     }
        //     return View();
        // }
        // public IActionResult Smpit(string Read = null)
        // {
        //     if (Read != null)
        //     {
        //         var ret = webService.GetKegiatanByUrl(Read, out string oMessage);
        //         if (string.IsNullOrEmpty(oMessage)) ViewBag.KegiatanByUrl = ret.KegiatanByUrl;
        //         var ret1 = webService.GetPropen(3, out string oMessage1);
        //         if (string.IsNullOrEmpty(oMessage1))
        //         {
        //             ViewBag.KegiatanList = ret1.Kegiatan.Take(3);
        //             ViewBag.ImageList = ret1.GaleriImage.Take(8);
        //             ViewBag.VideoList = ret1.GaleriVideo.Take(8);
        //         }

        //         else
        //         {
        //             return RedirectToAction("index", "home");
        //         }
        //     }
        //     else
        //     {
        //         return RedirectToAction("index", "home");
        //     }
        //     return View();
        // }
        public IActionResult Smait(string Read = null)
        {
            if (Read != null)
            {
                var ret = webService.GetKegiatanByUrl(Read, out string oMessage);
                if (string.IsNullOrEmpty(oMessage)) ViewBag.KegiatanByUrl = ret.KegiatanByUrl;
                var ret1 = webService.GetPropen(3, out string oMessage1);
                if (string.IsNullOrEmpty(oMessage1))
                {
                    ViewBag.KegiatanList = ret1.Kegiatan.Take(3);
                    ViewBag.ImageList = ret1.GaleriImage.Take(8);
                    ViewBag.VideoList = ret1.GaleriVideo.Take(8);
                }

                else
                {
                    return RedirectToAction("index", "home");
                }
            }
            else
            {
                return RedirectToAction("index", "home");
            }
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
