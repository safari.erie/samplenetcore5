using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swg.Sch.Fe.Models;
using Swg.Sch.Shared.Interfaces;

namespace Swg.Sch.Fe.Controllers
{
    public class TentangController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IWebService webService;

        public TentangController(ILogger<HomeController> logger, IWebService WebService)
        {
            _logger = logger;
            webService = WebService;
        }

        public IActionResult Fasilitas()
        {
            var ret = webService.GetSekolahFasilitas(out string oMessage);

            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Data = ret;
            }
            return View();
        }
        public IActionResult KenapaAttaufiq()
        {
            return View();
        }
        public IActionResult Kurikulum()
        {
            var ret = webService.GetProfileSekolah(out string oMessage);

            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Data = ret;
            }
            return View();
        }
        public IActionResult Prestasi()
        {
            var retGambar = webService.GetSekolahPrestasi(1, out string oMessage);
            var retVideo = webService.GetSekolahPrestasi(2, out oMessage);

            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.DataGambar = retGambar;
                ViewBag.DataVideo = retVideo;
            }

            return View();
        }
        public IActionResult ProfilSekolah()
        {
            var ret = webService.GetProfileSekolah(out string oMessage);

            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Data = ret;
            }
            return View();
        }
        public IActionResult VisiMisi()
        {
            var ret = webService.GetProfileSekolah(out string oMessage);

            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Data = ret;
            }
            return View();
        }

        public IActionResult SambutanDirekturEksekutif()
        {
            var ret = webService.GetPegawaiByUsername("irm101", out string oMessage);

            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Data = ret;
            }
            return View();
        }

        public IActionResult SambutanKetuaYatib()
        {
            var ret = webService.GetPegawaiByUsername("sya251", out string oMessage);

            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Data = ret;
            }
            return View();
        }

        public IActionResult SambutanManajerPendidikan()
        {
            var ret = webService.GetPegawaiByUsername("nur155", out string oMessage);

            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Data = ret;
            }
            return View();
        }

        public IActionResult SambutanKepsekTkit()
        {
            var ret = webService.GetPropen(1, out string oMessage);

            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Tkit = ret.Tkit;
            }
            return View();
        }
        // public IActionResult SambutanKepsekSdit()
        // {
        //     var ret = webService.GetPropen(2, out string oMessage);

        //     if (string.IsNullOrEmpty(oMessage))
        //     {
        //         ViewBag.Sdit = ret.Sdit;
        //     }
        //     return View();
        // }
        // public IActionResult SambutanKepsekSmpit()
        // {
        //     var ret = webService.GetPropen(3, out string oMessage);

        //     if (string.IsNullOrEmpty(oMessage))
        //     {
        //         ViewBag.Smpit = ret.Smpit;
        //     }
        //     return View();
        // }
        public IActionResult SambutanKepsekSmait()
        {
            var ret = webService.GetPropen(4, out string oMessage);

            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Smait = ret.Smait;
            }
            return View();
        }

        public IActionResult KalenderPendidikan()
        {
            var semes1 = webService.GetKalenderPendidikan(1, out string oMessage);
            var semes2 = webService.GetKalenderPendidikan(2, out oMessage);

            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Semester1 = semes1;
                ViewBag.Semester2 = semes2;
            }
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
