const UrlService = {
  GetPpdbDaftar: base_url + "/api/webs/GetPpdbDaftar",
  GetPpdbSiswa: base_url + "/api/webs/GetPpdbSiswa",
  GetPpdbDaftarByPin: base_url + "/api/webs/GetPpdbDaftarByPin",
};

const GetPpdbDaftar = () => {
  ProgressBar("wait");
  var IdPpdbDaftar = $("#NoPendaftaran").val();
  var KodePin = $("#KodePin").val();
  var res = initAjax(
    `${UrlService.GetPpdbDaftarByPin}?IdPpdbDaftar=${IdPpdbDaftar}&KodePin=${KodePin}`
  );
  if (res) ProgressBar("success");
  if (res.IsSuccess) {
    var data = res.Data;
    if (data.Pin != null) {
      $("#modalPin").modal("show");
      $("#ValidatePin").val(data.Pin);

      var res = initAjax(
        `${UrlService.GetPpdbSiswa}?IdPpdbDaftar=${IdPpdbDaftar}`
      );
      if (res.IsSuccess) {
        GetPpdbSiswa(res.Data);
      } else {
        Swal.fire({
          icon: "error",
          title: "Gagal",
          text: "Anda belum mengisi formulir, silahkan isi formulir terlebih dahulu !",
        }).then(function () {
          window.location.href = base_url + "/ppdb/formulir";
        });
      }
    } else {
      Swal.fire({
        icon: "error",
        title: "Gagal",
        text: "Silahkan melakukan pembayaran pendaftaran terlebih dahulu",
      });
    }
  } else {
    Swal.fire({
      icon: "error",
      title: "Gagal",
      text: res.ReturnMessage,
    });
  }
};

const GetPpdbSiswa = (data) => {
  $("#modalCekStatus").modal("show");
  $("span#_status_siswa").html(data.StatusProses);
  $("#InfoStatusProses").removeAttr("class");
  if (data.StatusProses == "Diterima") {
    $("#InfoStatusProses").addClass("alert alert-success");
    $("#JudulStatusProses").html("Selamat Ananda Diterima !");
  } else if (data.StatusProses == "Tidak Diterima") {
    $("#InfoStatusProses").addClass("alert alert-danger");
    $("#JudulStatusProses").html("Anda Dinyatakan Tidak Diterima !");
  } else if (data.StatusProses == "Dalam Proses") {
    $("#InfoStatusProses").addClass("alert alert-info");
    $("#JudulStatusProses").html("Status Dalam Proses !");
  } else if (data.StatusProses == "Daftar Tunggu") {
    $("#InfoStatusProses").addClass("alert alert-warning");
    $("#JudulStatusProses").html("Anda Masuk Daftar Tunggu !");
  } else if (data.StatusProses == "Mengundurkan Diri") {
    $("#InfoStatusProses").addClass("alert alert-danger");
    $("#JudulStatusProses").html("Mengundurkan Diri !");
  } else if (data.StatusProses == "Ditangguhkan") {
    $("#InfoStatusProses").addClass("alert alert-danger");
    $("#JudulStatusProses").html("Status Anda Ditangguhkan !");
  }

  $("#InfoStatus").html(data.Status);

  //siswa
  $("#IdPpdbDaftar").val(data.IdPpdbDaftar);
  $("#JenisKategoriPendaftaran").val(data.JenisKategoriPendaftaran);
  $("#JenisPendaftaran").val(data.JenisPendaftaran);
  $("#JalurPendaftaran").val(data.JalurPendaftaran);
  $("#Unit").val(data.Unit);
  $("#Kelas").val(data.Kelas);
  if (data.Unit == "SMAIT AT Taufiq")
    $("#Kelas").val(data.Kelas + " - " + data.Peminatan);
  $("#TanggalLahir").val(data.TanggalLahir);

  $("#IdPpdbDaftar").val(data.IdPpdbDaftar);
  $("#JenisKategoriPendaftaran").val(data.JenisKategoriPendaftaran);
  $("#JenisPendaftaran").val(data.JenisPendaftaran);
  $("#JalurPendaftaran").val(data.JalurPendaftaran);
  $("#Unit").val(data.Unit);
  $("#Kelas").val(data.Kelas);
  if (data.Unit == "SMAIT AT Taufiq")
    $("#Kelas").val(data.Kelas + " - " + data.Peminatan);
  $("#TanggalLahir").val(data.TanggalLahir);

  $("#IdPpdbDaftar").val(data.IdPpdbDaftar);
  ComboGetSeragam(function (obj) {
    $("select[name='IdPpdbSeragam']").html(obj);
  }, data.IdPpdbSeragam);
  ComboGetAgama(function (obj) {
    $("select[name='IdAgama']").html(obj);
  }, data.IdAgama);
  ComboGetJenisWargaNegara(function (obj) {
    $("select[name='IdKewarganegaraan']").html(obj);
  }, data.IdKewarganegaraan);
  ComboGetTinggalSiswa(function (obj) {
    $("select[name='IdTinggal']").html(obj);
  }, data.IdTinggal);
  ComboGetJenisTransportasi(function (obj) {
    $("select[name='IdTransportasi']").html(obj);
  }, data.IdTransportasi);
  ComboGetJenisBahasa(function (obj) {
    $("select[name='IdBahasa']").html(obj);
  }, data.IdBahasa);
  $("#Nik").val(data.Nik);
  $("#Nisn").val(data.Nisn);
  $("#Nama").val(data.Nama);
  $("#NamaPanggilan").val(data.NamaPanggilan);
  $("#TempatLahir").val(data.TempatLahir);
  $("#KdJenisKelamin").val(data.KdJenisKelamin);
  $("#KdGolonganDarah").val(data.KdGolonganDarah);
  $("#AlamatTinggal").val(data.AlamatTinggal);
  $("#Email").val(data.Email);
  $("#NoHandphone").val(data.NoHandphone);
  $("#NoDarurat").val(data.NoDarurat);
  $("#TinggiBadan").val(data.TinggiBadan);
  $("#BeratBadan").val(data.BeratBadan);
  $("#AnakKe").val(data.AnakKe);
  $("#JumlahSodaraKandung").val(data.JumlahSodaraKandung);
  $("#JumlahSodaraTiri").val(data.JumlahSodaraTiri);
  $("#JumlahSodaraAngkat").val(data.JumlahSodaraAngkat);
  $("#JarakKeSekolah").val(data.JarakKeSekolah);
  $("#WaktuTempuh").val(data.WaktuTempuh);
  $("#PenyakitDiderita").val(data.PenyakitDiderita);
  $("#KelainanJasmani").val(data.KelainanJasmani);
  $("#SekolahAsal").val(data.SekolahAsal);
  $("#AlamatSekolahAsal").val(data.AlamatSekolahAsal);
  $("#NspnSekolahAsal").val(data.NspnSekolahAsal);

  //ortu
  $("#DivSudahTiadaIbu").hide();
  if (data.DataOrtu[0].IdStatusHidup == 0) {
    $("#DivSudahTiadaIbu").fadeIn();
  }
  $("#IdTipeIbu").val(data.DataOrtu[0].IdTipe);
  $("#NamaIbu").val(data.DataOrtu[0].Nama);
  $("#AlamatIbu").val(data.DataOrtu[0].Alamat);
  $("#NikIbu").val(data.DataOrtu[0].Nik);
  $("#NamaInstansiIbu").val(data.DataOrtu[0].NamaInstansi);
  $("#JabatanIbu").val(data.DataOrtu[0].Jabatan);
  $("#EmailIbu").val(data.DataOrtu[0].Email);
  $("#NoHandphoneIbu").val(data.DataOrtu[0].NoHandphone);
  $("#NoTelpRumahIbu").val(data.DataOrtu[0].NoTelpRumah);
  ComboGetJenisPekerjaan(function (obj) {
    $("#IdJenisPekerjaanIbu").html(obj);
  }, data.DataOrtu[0].IdJenisPekerjaan);
  ComboGetAgama(function (obj) {
    $("#IdAgamaIbu").html(obj);
  }, data.DataOrtu[0].IdAgama);
  ComboGetJenisWargaNegara(function (obj) {
    $("#IdKewarganegaraanIbu").html(obj);
  }, data.DataOrtu[0].IdKewarganegaraan);
  ComboGetListJenjangPendidikan(function (obj) {
    $("#IdJenjangPendidikanIbu").html(obj);
  }, data.DataOrtu[0].IdJenjangPendidikan);
  $("#IdStatusPenikahanIbu").val(data.DataOrtu[0].IdStatusPenikahan);
  $("#TempatLahirIbu").val(data.DataOrtu[0].TempatLahir);
  $("#TanggalLahirIbu").val(data.DataOrtu[0].TanggalLahir);
  $("#IdStatusHidupIbu").val(data.DataOrtu[0].IdStatusHidup);
  $("#TanggalMeninggalIbu").val(data.DataOrtu[0].TanggalMeninggal);

  $("#DivSudahTiadaAyah").hide();
  if (data.DataOrtu[1].IdStatusHidup == 0) {
    $("#DivSudahTiadaAyah").fadeIn();
  }
  $("#IdTipeAyah").val(data.DataOrtu[1].IdTipe);
  $("#NamaAyah").val(data.DataOrtu[1].Nama);
  $("#AlamatAyah").val(data.DataOrtu[1].Alamat);
  $("#NikAyah").val(data.DataOrtu[1].Nik);
  $("#NamaInstansiAyah").val(data.DataOrtu[1].NamaInstansi);
  $("#JabatanAyah").val(data.DataOrtu[1].Jabatan);
  $("#EmailAyah").val(data.DataOrtu[1].Email);
  $("#NoHandphoneAyah").val(data.DataOrtu[1].NoHandphone);
  $("#NoTelpRumahAyah").val(data.DataOrtu[1].NoTelpRumah);
  ComboGetJenisPekerjaan(function (obj) {
    $("#IdJenisPekerjaanAyah").html(obj);
  }, data.DataOrtu[1].IdJenisPekerjaan);
  ComboGetAgama(function (obj) {
    $("#IdAgamaAyah").html(obj);
  }, data.DataOrtu[1].IdAgama);
  ComboGetJenisWargaNegara(function (obj) {
    $("#IdKewarganegaraanAyah").html(obj);
  }, data.DataOrtu[1].IdKewarganegaraan);
  ComboGetListJenjangPendidikan(function (obj) {
    $("#IdJenjangPendidikanAyah").html(obj);
  }, data.DataOrtu[0].IdJenjangPendidikan);
  $("#IdStatusPenikahanAyah").val(data.DataOrtu[1].IdStatusPenikahan);
  $("#TempatLahirAyah").val(data.DataOrtu[1].TempatLahir);
  $("#TanggalLahirAyah").val(data.DataOrtu[1].TanggalLahir);
  $("#IdStatusHidupAyah").val(data.DataOrtu[1].IdStatusHidup);
  $("#TanggalMeninggalAyah").val(data.DataOrtu[1].TanggalMeninggal);

  //berkas
  $("#DivSuratKesehatan").hide();
  $("#DivBebasNarkoba").hide();
  if (data.Unit == "SMAIT AT Taufiq") {
    $("#DivSuratKesehatan").fadeIn();
    $("#DivBebasNarkoba").fadeIn();
  }
  $("#ImgFileFoto").attr("src", base_url + "/FileBerkas/" + data.Foto);
  $("#ImgFileKtpIbu").attr("src", base_url + "/FileBerkas/" + data.FileKtpIbu);
  $("#ImgFileKtpAyah").attr(
    "src",
    base_url + "/FileBerkas/" + data.FileKtpAyah
  );

  $("#ImgFileKk").attr(
    "src",
    base_url + "/FileBerkas/" + data.FileKartuKeluarga
  );

  $("#InfoAkteKkNull").hide();
  if (data.FileKartuKeluarga == null) {
    $("#InfoAkteKkNull").fadeIn();
    $("#ImgFileKk").attr("src", base_url + "/assets/img/dummy/no-image.png");
  }

  $("#ImgFileAkte").attr("src", base_url + "/FileBerkas/" + data.FileAkte);
  if (data.FileAkte == null) {
    $("#InfoAkteKkNull").fadeIn();
    $("#ImgFileAkte").attr("src", base_url + "/assets/img/dummy/no-image.png");
  }

  $("#ImgFileSuratKesehatan").attr(
    "src",
    base_url + "/FileBerkas/" + data.FileSuratKesehatan
  );
  $("#ImgFileBebasNarkoba").attr(
    "src",
    base_url + "/FileBerkas/" + data.FileBebasNarkoba
  );
};

// const GetPpdbDaftar = () => {
//     var IdPpdbDaftar = $('#NoPendaftaran').val();
//     var res = initAjax(`${UrlService.GetPpdbDaftar}?IdPpdbDaftar=${IdPpdbDaftar}`);
//     if (res) ProgressBar("success");
//     if (res.IsSuccess) {
//         $('#imageEmpty').hide();
//         $('#secTabelStatus').fadeIn();
//         $("#InfoTidakDiterima").alert('close');
//         const {
//             IdPpdbDaftar,
//             IdJenisKategoriPendaftaran,
//             IdJenisPendaftaran,
//             IdJalurPendaftaran,
//             Agama,
//             AsalSekolah,
//             AlamatOrtu,
//             AlamatTinggal,
//             Email,
//             EmailOrtu,
//             IdAgama,
//             IdJenisPekerjaanAyah,
//             IdJenisPekerjaanIbu,
//             IdJenisPrestasi,
//             IdKelas,
//             IdKelasParalel,
//             IdSiswa,
//             IdUnit,
//             JenisKelamin,
//             JenisPekerjaanAyah,
//             JenisPekerjaanIbu,
//             JalurPendaftaran,
//             JenisKategoriPendaftaran,
//             JenisPendaftaran,
//             KdJenisKelamin,
//             Kelas,
//             KelasParalel,
//             Nama,
//             NamaAyah,
//             NamaIbu,
//             NamaInstansiAyah,
//             NamaInstansiIbu,
//             NamaPanggilan,
//             NikSiswa,
//             NikAyah,
//             NikIbu,
//             Nisn,
//             NoDarurat,
//             NoHandphoneOrtu,
//             Status,
//             StatusBerkas,
//             StatusBayarPendaftaran,
//             StatusBayarPendidikan,
//             StatusObservasi,
//             StatusProsesPpdb,
//             TanggalLahir,
//             TempatLahir,
//             Unit,
//             Prestasis,
//             Foto,
//             FileKtpIbu,
//             FileKtpAyah,
//             FileKartuKeluarga,
//             FileSuratKesehatan,
//             FileBebasNarkoba,
//             FileRaport,
//             FileKartuPeserta,
//             FilePerjanjianCicil,
//             IdPpdbSeragam,
//         } = res.Data;
//         if (IdJalurPendaftaran == 2)
//             $('#sertifikatPrestasi').hide();
//         if (IdJalurPendaftaran == 1)
//             $('#sertifikatPrestasi').show();
//         GetKategoriDaftars(IdJenisKategoriPendaftaran)
//         GetJenisPendaftarans(IdJenisPendaftaran)
//         GetJalurPendaftarans(IdJalurPendaftaran)
//         // ComboGetUnit(function (obj) {
//         //     $("select[name='IdUnit']").html(obj);
//         // }, IdUnit);

//         // ComboGetKelas(function (obj) {
//         //     $("select[name='IdKelas']").html(obj);
//         // }, IdUnit, IdKelas);

//         // ComboGetJenisKelamin(function (obj) {
//         //     $("select[name='KdJenisKelamin']").html(obj);
//         // }, KdJenisKelamin);

//         ComboGetJenisPekerjaan(function (obj) {
//             $("select[name='IdJenisPekerjaanIbu']").html(obj);
//         }, IdJenisPekerjaanIbu);

//         ComboGetJenisPekerjaan(function (obj) {
//             $("select[name='IdJenisPekerjaanAyah']").html(obj);
//         }, IdJenisPekerjaanAyah);

//         ComboGetSeragam(function (obj) {
//             $("select[name='Seragam']").html(obj);
//         }, IdPpdbSeragam);

//         $('span#id_ppdb_daftar').html('#' + IdPpdbDaftar)
//         $('input#IdPpdbDaftar').val(IdPpdbDaftar)
//         $('input#NikSiswa').val(NikSiswa)
//         $('input#Nama').val(Nama)
//         $('input#NamaPanggilan').val(NamaPanggilan)
//         $('input#KdJenisKelamin').val(KdJenisKelamin)
//         $('textarea#AlamatTinggal').val(AlamatTinggal)
//         $('select#IdAgama').val(IdAgama)
//         $('input#TempatLahir').val(TempatLahir)
//         $('input#TanggalLahir').val(TanggalLahir)
//         $('input#AsalSekolah').val(AsalSekolah)
//         $('input#NikIbu').val(NikIbu)
//         $('input#NamaIbu').val(NamaIbu)
//         $('select#IdJenisPekerjaanIbu').val(IdJenisPekerjaanIbu)
//         $('input#NamaInstansiIbu').val(NamaInstansiIbu)
//         $('input#NikAyah').val(NikAyah)
//         $('input#NamaAyah').val(NamaAyah)
//         $('select#IdJenisPekerjaanAyah').val(IdJenisPekerjaanAyah)
//         $('input#NamaInstansiAyah').val(NamaInstansiAyah)
//         $('input#AlamatOrtu').val(AlamatOrtu)
//         $('input#NoHandphoneOrtu').val(NoHandphoneOrtu)
//         $('input#EmailOrtu').val(EmailOrtu)

//         //$('span#htmlImageSiswa').html('<img alt="image" src="' + base_url + '/FileSiswa/' + IdPpdbDaftar + '.jpeg" class="rounded-circle author-box-picture">')
//         $('span#htmlNamaSiswa').html(Nama)
//         $('span#_kategori').html(JenisKategoriPendaftaran)
//         $('span#_jenjang_pendidikan').html(Unit)
//         if (KelasParalel != null) {
//             $('span#_kelas').html(KelasParalel)
//         } else {
//             $('span#_kelas').html(Kelas)
//         }
//         $('span#_jenis_pendaftaran').html(JenisPendaftaran)

//         $('span#_status_proses').html(Status != null ? Status : 'Dalam Proses')
//         $('span#_status_berkas').html(StatusBerkas != null ? StatusBerkas : 'Dalam Proses')
//         $('span#_status_bayar_daftar').html(StatusBayarPendaftaran != null ? StatusBayarPendaftaran : 'Dalam Proses')
//         $('span#_status_observasi').html(StatusObservasi != null ? StatusObservasi : 'Dalam Proses')
//         $('span#_status_bayar_pendidikan').html(StatusBayarPendidikan != null ? StatusBayarPendidikan : 'Dalam Proses')
//         $('span#_status_siswa').html(StatusProsesPpdb)
//         $('#InfoStatusProses').removeAttr('class');
//         if (StatusProsesPpdb == 'Diterima') {
//             $('#InfoStatusProses').addClass('alert alert-success');
//             $('#JudulStatusProses').html('Selamat Ananda Diterima !');
//         } else if (StatusProsesPpdb == 'Tidak Diterima') {
//             $('#InfoStatusProses').addClass('alert alert-danger');
//             $('#JudulStatusProses').html('Anda Dinyatakan Tidak Diterima !');
//         } else if (StatusProsesPpdb == 'Dalam Proses') {
//             $('#InfoStatusProses').addClass('alert alert-info');
//             $('#JudulStatusProses').html('Status Dalam Proses !');
//         } else if (StatusProsesPpdb == 'Daftar Tunggu') {
//             $('#InfoStatusProses').addClass('alert alert-warning');
//             $('#JudulStatusProses').html('Anda Masuk Daftar Tunggu !');
//         } else if (StatusProsesPpdb == 'Mengundurkan Diri') {
//             $('#InfoStatusProses').addClass('alert alert-danger');
//             $('#JudulStatusProses').html('Mengundurkan Diri !');
//         } else if (StatusProsesPpdb == 'Ditangguhkan') {
//             $('#InfoStatusProses').addClass('alert alert-danger');
//             $('#JudulStatusProses').html('Status Anda Ditangguhkan !');
//         }

//         if (IdJalurPendaftaran == 1)
//             $('small#infoPrestasi').show();
//         else
//             $('small#infoPrestasi').hide();

//         $.each(Prestasis, (i, v) => {
//             i = i + 1;
//             CreateFormPrestasiss();
//             setTimeout((e) => {
//                 GetJenisPrestasis(i, v.IdJenisPrestasi);
//                 GetTingkatPrestasiSiswas(i, v.IdJenisTingkatPrestasi);
//                 GetTahuns(i, v.Tahun);
//                 $('#IdPpdbDaftarPrestasi' + i).val(v.IdPpdbDaftarPrestasi);
//                 $('#NamaPenyelenggara' + i).val(v.NamaPenyelenggara);
//                 $('#Keterangan' + i).val(v.Keterangan);
//             }, 1000)
//         });

//     } else if (!res.IsSuccess) {
//         Swal.fire({
//             icon: 'error',
//             title: 'Gagal',
//             text: res.ReturnMessage,
//         })
//     }
// }

const ValidatePin = () => {
  var ValidatePin = $("#ValidatePin").val();
  var KodePin = $("#KodePin").val();
  if (ValidatePin == KodePin) {
    $("#secTabelStatus").fadeIn();
    $("#modalPin").modal("hide");
    $("div#imageEmpty").hide();
  } else {
    Swal.fire({
      icon: "error",
      title: "Gagal",
      text: "Kode PIN salah",
    });
    return;
  }
};

const ModalUploadFile = () => {
  $("#modalUploadFile").modal("show");
};

const BtnUploadFile = () => {
  var fd = new FormData($("#FormUploadFile")[0]);
  fd.append("FileKK", $('input[type="file"][name="FileKK"]')[0].files[0]);
  fd.append("FileAkte", $('input[type="file"][name="FileAkte"]')[0].files[0]);

  $.ajax({
    url:
      base_url +
      "/api/webs/UploadFileAkteKk?IdPpdbDaftar=" +
      $("#NoPendaftaran").val(),
    method: "POST",
    timeout: 0,
    processData: false,
    contentType: false,
    data: fd,
    dataType: "JSON",
    beforeSend: () => {
      ProgressBar("wait");
    },
    success: (res) => {
      ProgressBar("success");
      if (res.IsSuccess) {
        Swal.fire({
          icon: "success",
          title: "Berhasil",
          text: "Silahkan refresh halaman untuk cek status pendaftaran kembali",
        }).then(function () {
          window.location.href = `${base_url}/ppdb/cekstatus`;
        });
      } else if (!res.IsSuccess) {
        Swal.fire({
          icon: "error",
          title: "Gagal",
          text: res.ReturnMessage,
        });
      }
    },
    error: (err, a, e) => {
      ProgressBar("success");
      Swal.fire({
        icon: "error",
        title: "Terjadi Kesalahan",
        text: JSON.stringify(err),
      });
    },
  });
};

const ValidateFile = (Type) => {
  if (Type == "FileKK") {
    let file = document.querySelector("#FileKK");
    if (/\.(jpe?g|png)$/i.test(file.files[0].name) === false) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Format File Salah!",
      });
      $("input#FileKK").val("");
      $("#infoFileKK").hide();
    }
  } else if (Type == "FileAkte") {
    let file = document.querySelector("#FileAkte");
    if (/\.(jpe?g|png)$/i.test(file.files[0].name) === false) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Format File Salah!",
      });
      $("input#FileAkte").val("");
      $("#infoFileAkte").hide();
    }
  }
};

const readURL = (input, id) => {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      var idimg = "#img" + id;
      $(idimg).attr("src", e.target.result);

      var previewimg = "#Pv" + id;
      $(previewimg).attr("src", e.target.result);
    };

    reader.readAsDataURL(input.files[0]); // convert to base64 string
    var idinfo = "#info" + id;
    $(idinfo).fadeIn();
  }
};

$("#FileKK").change(function () {
  readURL(this, "FileKK");
});
$("#FileAkte").change(function () {
  readURL(this, "FileAkte");
});
