const UrlSvc = {
    GetPpdbDaftar: base_url + '/api/webs/GetPpdbDaftar',
    GetPertanyaanKuisioners: base_url + '/api/webs/GetPertanyaanKuisioners',
};

const initAjax = (service) => {
    var dataArray = $.ajax({
        type: "GET",
        async: false,
        beforeSend: function () {
            ProgressBar("wait");
        },
        url: service
    });

    var result = dataArray.responseJSON; //JSON.stringify

    return result;
}

const GetPpdbDaftar = () => {
    var IdPpdbDaftar = $('#NoPendaftaran').val();
    var res = initAjax(`${UrlSvc.GetPpdbDaftar}?IdPpdbDaftar=${IdPpdbDaftar}`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        $('#modalPin').modal('show');
        $('#PvNamaLengkap').html(res.Data.Nama);
        $('#PvJenisKategori').html(res.Data.JenisKategoriPendaftaran);
        $('#PvJenjangPendidikan').html(res.Data.Unit);
        $('#PvKelas').html(res.Data.Kelas);
        $('#PvJenisDaftar').html(res.Data.JenisPendaftaran);
        if (res.Data.IdUnit == 4) {
            $('#trKelasParalel').fadeIn();
            $('#PvKelasParalel').html(res.Data.KelasParalel);
        } else {
            $('#trKelasParalel').hide();
        }
    } else if (!res.IsSuccess) {
        Swal.fire({
            icon: 'error',
            title: 'Gagal',
            text: res.ReturnMessage,
        })
    }
}

const GetWawancara = () => {
    var KodePin = $('#KodePin').val();
    if (KodePin.length != 6) {
        Swal.fire({
            icon: 'error',
            title: 'Gagal',
            text: 'Kode PIN harus 6 digit',
        })
        return;
    }
    $('#modalPin').modal('hide');
    $('div#imageEmpty').hide();
    $('div#tablePpdb').fadeIn();
    DataKuisioner1();
    DataKuisioner2();
    DataKuisioner3();
    DataKuisioner4();

}

const BtnMulaiWawancara = () => {
    Swal.fire({
        title: 'Apakah Anda Yakin?',
        text: "Klik lanjutkan untuk memulai wawancara online!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Lanjutkan!'
    }).then((result) => {
        if (result.value) {
            $('body').css('pointer-events', 'none');

            let timerInterval;
            Swal.fire({
                title: 'Tunggu Sebentar!',
                html: 'Sedang memuat data...',
                timer: 3000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getContent()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                onClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                    $('body').css('pointer-events', 'unset');
                    $('div#tablePpdb').hide();
                    $('div#secKuisioner1').fadeIn();
                    $('#NoPendaftaran').attr('readonly', true);
                    $('#DivBtnCari').hide();
                    $('#DivBtnReload').show();
                }
            })
        }
    })
    
}

const DataKuisioner1 = (IdUnit = 0, IdJenisPendaftaran = 0) => {
    var res = initAjax(`${UrlSvc.GetPertanyaanKuisioners}?IdJenisPertanyaan=1&IdUnit=${IdUnit}&IdJenisPendaftaran=${IdJenisPendaftaran}`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        var html = '';
        $.each(res.Data, (i, v) => {
            html += '<div class="form-group">' +
                '    <label for="Kuisioner1"><b>' + (i + 1) + '. </b>' + v.Pertanyaan +
                '    <input type="hidden" name="IdPertanyaanKuisioner[' + i + ']" value="' + v.IdPertanyaan + '"/>' +
                '    <input type="hidden" name="Pertanyaan[' + i + ']" value="' + v.Pertanyaan + '"/>' +
                '    <input type="hidden" name="Jawaban[' + i + ']" value=""/>' +
                '    <br />' +
                '    <b>Jawab: &nbsp;</b>' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[' + i + ']" id="OpsiJawaban' + i + '-1" value="Ya" required>' +
                '        <label class="form-check-label" for="OpsiJawaban' + i + '-1">Ya</label>' +
                '    </div>' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[' + i + ']" id="OpsiJawaban' + i + '-2" value="Tidak" required>' +
                '        <label class="form-check-label" for="OpsiJawaban' + i + '-2">Tidak</label>' +
                '    </div>' +
                '    <div class="invalid-feedback">More example invalid feedback text</div>' +
                '</div>';
        });
        $('div#formKuisioner1').html(html);
    }  else if (!res.IsSuccess) {
        Swal.fire({
            icon: 'error',
            title: 'Gagal',
            text: res.ReturnMessage,
        })
    }
}

const DataKuisioner2 = (IdUnit = 0, IdJenisPendaftaran = 0) => {
    var res = initAjax(`${UrlSvc.GetPertanyaanKuisioners}?IdJenisPertanyaan=2&IdUnit=${IdUnit}&IdJenisPendaftaran=${IdJenisPendaftaran}`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        var html = '';
        $.each(res.Data, (i, v) => {
            html += '<div class="form-group">' +
                '    <label for="Kuisioner2"><b>' + (i + 1) + '. </b>' + v.Pertanyaan +
                '    <input type="hidden" name="IdPertanyaanKuisioner[2' + i + ']" value="' + v.IdPertanyaan + '"/>' +
                '    <input type="hidden" name="Pertanyaan[2' + i + ']" value="' + v.Pertanyaan + '"/>' +
                '    <input type="hidden" name="Jawaban[2' + i + ']" value=""/>' +
                '    <br />' +
                '    <b>Jawab: &nbsp;</b>' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[2' + i + ']" id="OpsiJawaban2' + i + '-1" value="Selalu" required>' +
                '        <label class="form-check-label" for="OpsiJawaban2' + i + '-1">Selalu</label>' +
                '    </div>' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[2' + i + ']" id="OpsiJawaban2' + i + '-2" value="Sering" required>' +
                '        <label class="form-check-label" for="OpsiJawaban2' + i + '-2">Sering</label>' +
                '    </div>' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[2' + i + ']" id="OpsiJawaban2' + i + '-3" value="Jarang" required>' +
                '        <label class="form-check-label" for="OpsiJawaban2' + i + '-3">Jarang</label>' +
                '    </div>' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[2' + i + ']" id="OpsiJawaban2' + i + '-4" value="Tidak Pernah" required>' +
                '        <label class="form-check-label" for="OpsiJawaban2' + i + '-4">Tidak Pernah</label>' +
                '    </div>' +
                '</div>';
        });
        $('div#formKuisioner2').html(html);
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Gagal',
            text: res.ReturnMessage,
        })
    }
}

const DataKuisioner3 = (IdUnit = 0, IdJenisPendaftaran = 0) => {
    var res = initAjax(`${UrlSvc.GetPertanyaanKuisioners}?IdJenisPertanyaan=3&IdUnit=${IdUnit}&IdJenisPendaftaran=${IdJenisPendaftaran}`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        var html = '';
        $.each(res.Data, (i, v) => {
            html += '<div class="form-group">' +
                '    <label for="Kuisioner3"><b>' + (i + 1) + '. </b>' + v.Pertanyaan +
                '    <input type="hidden" name="IdPertanyaanKuisioner[3' + i + ']" value="' + v.IdPertanyaan + '"/>' +
                '    <input type="hidden" name="Pertanyaan[3' + i + ']" value="' + v.Pertanyaan + '"/>' +
                //'    <input type="hidden" name="Jawaban[3' + i + ']" value=""/>' +
                '    <br />' +
                '    <b>Jawab: &nbsp;</b>' +
                '    <br />' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[3' + i + ']" id="OpsiJawaban3' + i + '-1" value="Ayah dan Ibu" onchange="Lainnya(this,'+i+');" required>' +
                '        <label class="form-check-label" for="OpsiJawaban3' + i + '-1">Ayah dan Ibu</label>' +
                '    </div>' +
                '    <br />' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[3' + i + ']" id="OpsiJawaban3' + i + '-2" value="Ayah/Ibu saja (Faktor Pekerjaan)" onchange="Lainnya(this,'+i+');" required>' +
                '        <label class="form-check-label" for="OpsiJawaban3' + i + '-2">Ayah/Ibu saja (Faktor Pekerjaan)</label>' +
                '    </div>' +
                '    <br />' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[3' + i + ']" id="OpsiJawaban3' + i + '-3" value="Ayah/Ibu saja (Faktor Perceraian)" onchange="Lainnya(this,'+i+');" required>' +
                '        <label class="form-check-label" for="OpsiJawaban3' + i + '-3">Ayah/Ibu saja (Faktor Perceraian)</label>' +
                '    </div>' +
                '    <br />' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[3' + i + ']" id="OpsiJawaban3' + i + '-4" value="Keluarga selain ayah dan ibu kandung (kakek-nenek/ paman-bibi)" onchange="Lainnya(this,'+i+');" required>' +
                '        <label class="form-check-label" for="OpsiJawaban3' + i + '-4">Keluarga selain ayah dan ibu kandung (kakek-nenek/ paman-bibi)</label>' +
                '    </div>' +
                '    <br />' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[3' + i + ']" id="OpsiJawaban3' + i + '-5" value="Lainnya" onchange="Lainnya(this,'+i+');" data-input="Lainnya" required>' +
                '        <label class="form-check-label" for="OpsiJawaban3' + i + '-5">Lainnya *Sebutkan</label> <br />' +
                '        <div id="LainnyaField'+i+'" style="display:none;"> <input id="Jawaban3' + i + '" type="text" name="Jawaban[3' + i + ']" class="form-control" placeholder="Silahkan jawab pertanyaan ini" label="LainnyaField'+`${i}`+'" required> </div>' +
                '    </div>' +
                '</div>';
        });
        $('div#formKuisioner3').html(html);
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Gagal',
            text: res.ReturnMessage,
        })
    }
}

const DataKuisioner4 = (IdUnit = 0, IdJenisPendaftaran = 0) => {
    var res = initAjax(`${UrlSvc.GetPertanyaanKuisioners}?IdJenisPertanyaan=4&IdUnit=${IdUnit}&IdJenisPendaftaran=${IdJenisPendaftaran}`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        var html = '';
        $.each(res.Data, (i, v) => {
            html += '<div class="form-group">' +
                '    <label for="Kuisioner4"><b>' + (i + 1) + '. </b>' + v.Pertanyaan +
                '    <input type="hidden" name="IdPertanyaanKuisioner[4' + i + ']" value="' + v.IdPertanyaan + '"/>' +
                '    <input type="hidden" name="Pertanyaan[4' + i + ']" value="' + v.Pertanyaan + '"/>' +
                '    <input class="form-check-input" type="hidden" name="OpsiJawaban[4' + i + ']" id="OpsiJawaban' + i + '-1" value="" required>' +
                '    <br />' +
                '    <b>Jawab: &nbsp;</b>' +
                '    <input id="Jawaban4' + i + '" type="text" name="Jawaban[4' + i + ']" class="form-control" placeholder="Silahkan jawab pertanyaan ini" required>' +
                '</div>';
        });
        $('div#formKuisioner4').html(html);
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Gagal',
            text: res.ReturnMessage,
        })
    }
}

const DataRegister = (Proses, Sec) => {

    if (Proses == 0) {
        var TypeInput = true;
        $('[data-input="wajib"]').each(function (i, v) {
            if ($(this).val() == "") {
                TypeInput = false;
                $('#FormWawancara').addClass('was-validated');
                $('#FormDataOrtu').addClass('was-validated');
                $('#FormBerkas').addClass('was-validated');
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal',
                    text: 'Kolom ' + $(this).attr("label") + ' Wajib Diisi',
                })
                return i < 0;
            }
        })

        if (TypeInput) {
            Swal.fire({
                title: 'Apakah Anda Yakin?',
                text: "Jika data yang dimasukan sudah benar silahkan klik tombol lanjutkan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Lanjutkan!'
            }).then((result) => {
                if (result.value) {
                    $('body').css('pointer-events', 'none');

                    let timerInterval;
                    Swal.fire({
                        title: 'Tunggu Sebentar!',
                        // html: 'Anda akan diarahkan kehalaman kuisioner.',
                        html: 'Sedang memuat berkas anda.',
                        timer: 3000,
                        timerProgressBar: true,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                                const content = Swal.getContent()
                                if (content) {
                                    const b = content.querySelector('b')
                                    if (b) {
                                        b.textContent = Swal.getTimerLeft()
                                    }
                                }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then((result) => {
                        /* Read more about handling dismissals below */
                        if (result.dismiss === Swal.DismissReason.timer) {

                            $('body').css('pointer-events', 'unset');
                            /* cek getsiswabyemail */
                            var cekGetPpdbByEmail = GetPpdbByEmail($('#EmailOrtu').val());
                        }
                    })
                }
            })

        }
    } else if (Proses == 1) {
        var TypeInput = true;
        $('label[for="Kuisioner1"]').each(function (i, v) {
            if (!$("input[name='OpsiJawaban[" + i + "]']").is(':checked')) {
                TypeInput = false;
                $('#FormWawancara').addClass('was-validated');
            }
        })
        if (!TypeInput) {
            $('div#secKuisioner1').hide();
            $('div#secKuisioner2').fadeIn();
            scrollToBottom('secKuisioner2');
            $('#FormWawancara').removeClass('was-validated');
        }
    } else if (Proses == 2) {
        var TypeInput = true;
        $('label[for="Kuisioner2"]').each(function (i, v) {
            if (!$("input[name='OpsiJawaban[2" + i + "]']").is(':checked')) {
                TypeInput = false;
                $('#FormWawancara').addClass('was-validated');
            }
        })
        if (!TypeInput) {
            $('div#secKuisioner2').hide();
            $('div#secKuisioner3').fadeIn();
            scrollToBottom('secKuisioner3');
            $('#FormWawancara').removeClass('was-validated');
        }
    } else if (Proses == 3) {
        var TypeInput = true;
        $('label[for="Kuisioner3"]').each(function (i, v) {
            if (!$("input[name='OpsiJawaban[3" + i + "]']").is(':checked')) {
                TypeInput = false;
                $('#FormWawancara').addClass('was-validated');
            }
        })
        $('[data-input="wajib"]').each(function (i, v) {
            if ($(this).val() == "") {
                TypeInput = false;
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal',
                    text: 'Kolom Lainnya Nomor ' + $(this).attr("label") + ' Wajib Diisi',
                })
                return i < 0;
            }
        })
        if (!TypeInput) {
            $('div#secKuisioner3').hide();
            $('div#secKuisioner4').fadeIn();
            scrollToBottom('secKuisioner4');
            $('#FormWawancara').removeClass('was-validated');
        }
    } else if (Proses == 4) {
        var TypeInput = true;
        $('label[for="Kuisioner4"]').each(function (i, v) {
            if ($("input[name='Jawaban[4" + i + "]']").val() == "") {
                TypeInput = false;
                $('#FormWawancara').addClass('was-validated');
            }
        })
        if (!TypeInput) {
            $('div#secKuisioner4').hide();
            $('div#secKuisioner5').fadeIn();
            scrollToBottom('secKuisioner5');
            $('#FormWawancara').removeClass('was-validated');
        }
    } else if (Proses == 5) {
        var TypeInput = true;
        $('label[for="Kuisioner4"]').each(function (i, v) {
            if ($("input[name='Jawaban[4" + i + "]']").val() == "") {
                TypeInput = false;
                $('#FormWawancara').addClass('was-validated');
            }
        })
        if (TypeInput) {
            Swal.fire({
                title: 'Apakah Anda Yakin?',
                text: "Jika data yang dimasukan sudah benar silahkan klik tombol lanjutkan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Lanjutkan!'
            }).then((result) => {
                if (result.value) {
                    $('body').css('pointer-events', 'none');

                    let timerInterval;
                    Swal.fire({
                        title: 'Tunggu Sebentar!',
                        html: 'Sedang mengambil data...',
                        timer: 3000,
                        timerProgressBar: true,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                                const content = Swal.getContent()
                                if (content) {
                                    const b = content.querySelector('b')
                                    if (b) {
                                        b.textContent = Swal.getTimerLeft()
                                    }
                                }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then((result) => {
                        /* Read more about handling dismissals below */
                        if (result.dismiss === Swal.DismissReason.timer) {
                            $('body').css('pointer-events', 'unset');
                            DataPreviewForm();
                        }
                    })
                }
            })
        }
    } else if (Proses = -1) {
        if (Sec == 'secKuisioner5') {
            Swal.fire({
                title: 'Apakah Anda Yakin?',
                text: "Anda akan kembali kehalaman awal, data tidak akan direset!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Kembali Awal!'
            }).then((result) => {
                if (result.value) {
                    $('#' + Sec).hide();
                    $('#secKuisioner1').fadeIn();
                }
            })
        } else {
            $('#' + Sec).hide();
            $('#secKuisioner1').fadeIn();
        }
    }
}

const Lainnya = (val,index) => {
    if (val.value == "Lainnya") {
        $(`div#LainnyaField${index}`).fadeIn();
        $(`input[label="LainnyaField${index}"]`).attr({
            'data-input': 'wajib',
            'required': true,
        });
        $(`input[label="LainnyaField${index}"]`).val('');
    } else {
        $(`div#LainnyaField${index}`).fadeOut();
        $(`input[label="LainnyaField${index}"]`).removeAttr('data-input', 'required');
        $(`input[label="LainnyaField${index}"]`).val('');
    }
}

const scrollToBottom = (id) => {
    $('html, body').animate({
        scrollTop: $(`#${id}`).offset().top
    }, 0)
}