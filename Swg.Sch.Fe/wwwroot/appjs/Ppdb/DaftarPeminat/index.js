$(document).ready((e) => {
  $("select[name='Kelas']").change((e) => {
    if (e.target.value == "Pindahan") {
      $("#DivPindahanKelas").fadeIn();
      $("#PindahanKelas").attr({
        "data-input": "wajib",
        required: true,
      });
    } else {
      $("#DivPindahanKelas").fadeOut();
      $("#PindahanKelas").removeAttr("data-input", "required");
    }
  });
});
const UrlService = {
  DaftarPeminatPpdb: `${base_url}/api/webs/DaftarPeminatPpdb`,
};
function SaveData() {
  var NoPanitiaPpdb = $("#NoPanitiaPpdb").val(),
    NamaLengkap = $("#NamaLengkap").val(),
    NamaAyah = $("#NamaAyah").val(),
    NamaIbu = $("#NamaIbu").val(),
    NomorHp = $("#NomorHp").val(),
    Kelas = $("#Kelas").val(),
    Email = $("#Email").val();

  var fd = new FormData();
  if (Kelas === "Pindahan") {
    Kelas = $("#PindahanKelas").val();
    alert($("#PindahanKelas").val());
  }

  fd.append("NamaLengkap", NamaLengkap);
  fd.append("Kelas", Kelas);
  fd.append("NamaAyah", NamaAyah);
  fd.append("NamaIbu", NamaIbu);
  fd.append("NomorHp", NomorHp);
  fd.append("Email", Email);

  var TypeInput = true;
  $('[data-input="wajib"]').each(function (i, v) {
    if ($(this).val() == "") {
      TypeInput = false;
      $("#FormDaftarPeminat").addClass("was-validated");

      Swal.fire({
        icon: "error",
        title: "Gagal",
        text: "Kolom " + $(this).attr("label") + " Wajib Diisi",
      });
      return i < 0;
    }
  });

  if (TypeInput) {
    $.ajax({
      url: UrlService.DaftarPeminatPpdb,
      type: "POST",
      processData: false,
      contentType: false,
      data: fd,
      dataType: "JSON",
      beforeSend: () => {
        ProgressBar("wait");
      },
      success: (res) => {
        ProgressBar("success");
        if (res.IsSuccess) {
          Swal.fire({
            icon: "success",
            title: "Berhasil",
            text: "Terima Kasih telah mengisi Buku Tamu Peminat, In Syaa-a Allah kami akan segera menghubungi Ayah/Bunda melalui Email jika pendaftaran telah dibuka",
          }).then(function () {
            window.location.href = `${base_url}/ppdb/daftarpeminat`;
          });
        } else {
          Swal.fire({
            icon: "error",
            title: "Gagal",
            text: res.ReturnMessage,
          });
        }
      },
      error: (err, a, msg) => {
        ProgressBar("success");
        Swal.fire({
          icon: "error",
          title: "Terjadi Kesalahan",
          text: `${err.status} ${msg}`,
          footer:
            'silahkan hubungi &nbsp;<a href="http://wa.me/' +
            NoPanitiaPpdb +
            '">  panitia ppdb </a>',
        });
      },
    });
  }
}
