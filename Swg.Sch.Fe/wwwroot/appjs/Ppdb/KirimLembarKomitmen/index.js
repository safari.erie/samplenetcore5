const UrlService = {
  UploadLembarKomitmen: `${base_url}/api/webs/UploadLembarKomitmen`,
};
function KirimLembarKomitmen() {
  var NoPanitiaPpdb = $("#NoPanitiaPpdb").val();
  var KodePin = $("#KodePin").val();
  if (KodePin == "") {
    Swal.fire({
      icon: "error",
      title: "Gagal",
      text: "Kode Pin wajib diisi!",
    });
    return;
  }
  var fd = new FormData($("#FormLembarKomitmen")[0]);

  $.ajax({
    url: UrlService.UploadLembarKomitmen,
    type: "POST",
    processData: false,
    contentType: false,
    data: fd,
    dataType: "JSON",
    beforeSend: () => {
      ProgressBar("wait");
    },
    success: (res) => {
      ProgressBar("success");
      if (res.IsSuccess) {
        Swal.fire({
          icon: "success",
          title: "Berhasil",
          text: "Terimakasih anda telah mengirimkan lembar komitmen",
        }).then(function () {
          window.location.href = `${base_url}/`;
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Gagal",
          text: res.ReturnMessage,
        });
      }
    },
    error: (err, a, msg) => {
      ProgressBar("success");
      Swal.fire({
        icon: "error",
        title: "Terjadi Kesalahan",
        text: `${err.status} ${msg}`,
        footer:
          'silahkan hubungi &nbsp;<a href="http://wa.me/' +
          NoPanitiaPpdb +
          '">  panitia ppdb </a>',
      });
    },
  });
}
