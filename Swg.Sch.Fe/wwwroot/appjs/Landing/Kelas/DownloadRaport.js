﻿$(document).ready(function () {

})

function DownloadRaports(File = null) {
    if (!File) {
        var nisn = $('input#nisn').val();
        $('#TabelDownladRaports').DataTable({
            "ajax": {
                "url": base_url + "/api/webs/GetSiswaRapors?nisn=" + nisn,
                "type": "GET",
                "dataSrc": function (json) {
                    if (json.Data == null) {
                        alert(json.ReturnMessage)
                        return json;
                    } else {
                        $('#modalDownloadRaport').modal('show');
                        return json.Data;
                    }
                }
            },

            "responsive": true,
            "scrollX": false,
            "processing": true,
            "paging": true,
            "searching": true,
            "pageLength": 10,

            "columns": [
                { "data": "JenisRapor", "name": "JenisRapor", "autoWidth": true },
                { "data": "FileRapor", "name": "FileRapor", "autoWidth": true },
                {
                    "render": function (data, type, full, meta) {
                        var param = "'" + full.FileRapor + "'";
                        var aksi = 'onClick="DownloadRaports(' + param + ');"';
                        data = '<button type="button" class="btn btn-theme btn-sm" ' + aksi + '> Download</button> &nbsp; ';
                        return data;
                    }
                },

            ],
            "bDestroy": true
        });
    } else {
        window.open(base_url + "/File/Kelas/Raport/" + File, '_self');
    }
    
}