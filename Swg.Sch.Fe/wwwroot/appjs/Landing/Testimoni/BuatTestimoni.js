const UrlService = {
    AddTestimoni : `${base_url}/api/webs/AddTestimoni`,
}

$('#Sebagai').change((e) => {
    if (e.target.value === 'Lainnya') {
        $('#DivSebagaiLainnya').fadeIn();
    } else {
        $('#DivSebagaiLainnya').fadeOut();
        $('#SebagaiLainnya').val('');
    }
})
$('#FotoUser').change((e) => {
    FUNCValidateUploadFileSize(e.target, 2048, '2MB')
    FUNCValidateUploadFileExtension(e.target, [".jpg", ".jpeg", ".png"])
})

const KirimTestimoni = () => {
    var Nama = $('#Nama').val(),
        Sebagai = $('#Sebagai option:selected').val(),
        SebagaiLainnya = $('#SebagaiLainnya').val(),
        Url = $('#Url').val(),
        Pesan = $('#Pesan').val();
    
    if (Nama === '') {
        Swal.fire('Gagal', `Nama harus diisi!`, 'error');
        return;
    }
    if (Sebagai === '') {
        Swal.fire('Gagal', `Sebagai harus diisi!`, 'error');
        return;
    }
    if (Sebagai === 'Lainnya') {
        if (SebagaiLainnya === '') {
            Swal.fire('Gagal', `Sebagai lainnya harus diisi!`, 'error');
            return;
        }
    }
    if (Pesan === '') {
        Swal.fire('Gagal', `Pesan Testimoni harus diisi!`, 'error');
        return;
    }
    
    var fd = new FormData();
    fd.append('Nama', Nama)
    if (Sebagai === 'Lainnya') {
        fd.append('Sebagai', SebagaiLainnya)
    } else {
        fd.append('Sebagai', Sebagai)
    }
    fd.append('Url', Url)
    fd.append('Pesan', Pesan)
    fd.append('FotoUser', $('input[type="file"][name="FotoUser"]')[0].files[0])

    $.ajax({
        url: UrlService.AddTestimoni,
        method: "POST",
        timeout: 60000,
        processData: false,
        contentType: false,
        data: fd,
        dataType: "JSON",
        beforeSend: () => {
            ProgressBar("wait");
        },
        success: (res) => {
            ProgressBar("success");
            if (res.IsSuccess) {
                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil',
                    text: 'Selamat testimoni telah dikirim!',
                }).then(function () {
                    window.location.reload()
                });
            } else if (!res.IsSuccess) {
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal',
                    text: res.ReturnMessage,
                })

            }
        },
        "error": (err, a, e) => {
            ProgressBar("success");
            Swal.fire({
                icon: 'error',
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(err),
            })
        }
    })
}

const FUNCValidateUploadFileSize = (fi, maxSize = 2048, strMaxSize = '2MB') => {
    if (fi.files.length > 0) {
        for (var i = 0; i <= fi.files.length - 1; i++) {
            const fsize = fi.files.item(i).size;
            const file = Math.round((fsize / 1024));
            if (file >= maxSize) {
                Swal.fire('Gagal', `Ukuran file terlalu besar, batas ukuran ${strMaxSize}`, 'error');
                fi.value = "";
                return null;
            }
        }
    }
}

const FUNCValidateUploadFileExtension = (oInput, _validFileExtensions = [".jpg", ".jpeg", ".pdf", ".png"]) => {
    var sFileName = oInput.value;
    if (sFileName.length > 0) {
        var blnValid = false;
        var msgExtension = "";
        for (var j = 0; j < _validFileExtensions.length; j++) {
            msgExtension += _validFileExtensions[j] + " ";
            var sCurExtension = _validFileExtensions[j];
            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                blnValid = true;
                break;
            }
        }
            
        if (!blnValid) {
            Swal.fire('Gagal', `Ekstensi file tidak didukung! <br /> format harus ${msgExtension}`, 'error');
            oInput.value = "";
            return false;
        }
    }
}