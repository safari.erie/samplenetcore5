﻿const BtnPpdbDaftarHome = () => {
    var NamaSiswa = $('#NamaSiswaPpdbHome', '#FormPpdbDaftarHome').val(),
        Email = $('#EmailPpdbHome', '#FormPpdbDaftarHome').val();
    
    if (NamaSiswa === '') {
        Swal.fire({
            icon: 'info',
            title: 'Gagal',
            text: 'Masukan nama siswa',
        })
        return;
    }
    if (Email === '') {
        Swal.fire({
            icon: 'info',
            title: 'Gagal',
            text: 'Masukan email',
        })
        return;
    }
    window.location.replace(`/ppdb/daftar?nama=${NamaSiswa}&email=${Email}`)
 }