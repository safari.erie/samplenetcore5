﻿using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Shared.Interfaces;
using Swg.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Swg.Sch.Web.Components
{
    public class Footer : ViewComponent
    {
        private readonly IWebService webService;
        public Footer(IWebService WebService)
        {
            webService = WebService;
        }
        public IViewComponentResult Invoke()
        {
            var ret = webService.GetHome(out string oMessage);

            var ret1 = webService.GetPropen(1, out string oMessage1);
            var ret2 = webService.GetPropen(2, out string oMessage2);
            var ret3 = webService.GetPropen(3, out string oMessage3);
            var ret4 = webService.GetPropen(4, out string oMessage4);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Kontak = ret.Kontak;
            }
            if (string.IsNullOrEmpty(oMessage1))
            {
                if(ret1.Kegiatan != null && ret1.Kegiatan.Count > 0)
                {
                    ViewBag.KegiatanTkit = ret1.Kegiatan[0];
                }
            }
            if (string.IsNullOrEmpty(oMessage2))
            {
                if (ret2.Kegiatan != null && ret2.Kegiatan.Count > 0)
                {
                    ViewBag.KegiatanSdit = ret2.Kegiatan[0];
                }
            }
            if (string.IsNullOrEmpty(oMessage3))
            {
                if (ret3.Kegiatan != null && ret3.Kegiatan.Count > 0)
                {
                    ViewBag.KegiatanSmpit = ret3.Kegiatan[0];
                }
            }
            if (string.IsNullOrEmpty(oMessage4))
            {
                if (ret4.Kegiatan != null && ret4.Kegiatan.Count > 0)
                {
                    ViewBag.KegiatanSmait = ret4.Kegiatan[0];
                }
            }
            var webnopanitiappdb = webService.GetApplSetting("webnopanitiappdb", out string oMsg_webnopanitiappdb);
            if (string.IsNullOrEmpty(oMsg_webnopanitiappdb)) ViewBag.webnopanitiappdb = webnopanitiappdb.ValueString;   
            return View();
        }
    }
}
