﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;
using Swg.Models;
using Swg.Models.Constants;
using Swg.Services;

namespace Swg.Sch.Web.Components
{
    public class MenuApps : ViewComponent
    {
        public const string OPEN_LIST_TAG = "<ul>";
        public const string CLOSE_LIST_TAG = "</ul>";
        public const string OPEN_LIST_ITEM_TAG = "<li>";
        public const string CLOSE_LIST_ITEM_TAG = "</li>";
        public const string OPEN_LIST_ITEM_TAG_CLASS = "<li class='nav-item'>";
        public const string OPEN_LIST_UL_CLASS_DROPDOWN = "<ul class='dropdown-menu'>";

        private readonly IUserAppService applTask;
        IList<ApplTaskModel> allMenuItems;
        // Constructor
        public MenuApps(IUserAppService ApplTask)
        {
            applTask = ApplTask;
        }

        public IList<ApplTaskModel> GetMenus()
        {
            var ret = applTask.GetMenus(JenisAplikasiConstant.FrontendWeb, "default", out string oMessage);
            //if (ret.Count() == 0)
            //    return null;
            allMenuItems = ret;

            return allMenuItems;
        }


        public IViewComponentResult Invoke()
        {
            var stb = new StringBuilder();
            var baseUrlMenu = $"{this.Request.Scheme}://{this.Request.Host.Value.ToString()}{this.Request.PathBase.Value.ToString()}";
            if ((GetMenus().Count() == 0))
            {
                //stb.Append("<li><a class='nav-link' href='" + baseUrlMenu + "/Home/Index'><i class='fas fa-home'></i> <span>Dashboard</span></a></li>");
                //stb.Append("<li><a class='nav-link' href='#' onClick='Logout();'><i class='fas fa-times'></i> <span>Logout</span></a></li>");
                //stb.Append("<ul> </ul>");
            }
            else
            {
                List<ApplTaskModel> parentItems = (from a in GetMenus() where a.IdApplTaskParent == null select a).ToList();
                //stb.Append("<ul class='navbar-nav'>");
                //stb.Append("<li class='nav-item'><a class='nav-link' href='" + baseUrlMenu + "/Home/Index'><i class='fas fa-home'></i> <span>Dashboard</span></a></li>");
                foreach (var parentcat in parentItems)
                {
                    stb.Append(OPEN_LIST_ITEM_TAG_CLASS);
                    List<ApplTaskModel> childItems = (from a in allMenuItems where a.IdApplTaskParent == parentcat.IdApplTask select a).ToList();
                    if (childItems.Count > 0)
                    {
                        //stb.Append(OPEN_LIST_UL_CLASS_DROPDOWN);
                        //stb.Append("<li class='nav-item'><a href='#' class='nav-link'>Manajemen Users</a></li>");
                        //stb.Append(CLOSE_LIST_TAG);
                        stb.Append("<a href='javascript:void(0)'  class='nav-link'> <span>" + parentcat.ApplTaskName + "</span> </a>");
                        AddChildItem(parentcat, stb);
                    }
                    else
                    {
                        stb.Append("<a href='" + baseUrlMenu + "/" + parentcat.ControllerName + "/" + parentcat.ActionName + "' class='nav-link'> <span>" + parentcat.ApplTaskName + "</span> </a>");

                    }

                    stb.Append(CLOSE_LIST_ITEM_TAG);
                }
                //stb.Append("<li><a class='nav-link' href='#' onClick='Logout();'><i class='fas fa-times'></i> <span>Logout</span></a></li>");
                //stb.Append(CLOSE_LIST_TAG);
            }


            ViewBag.html = stb.ToString();
            return View();
        }
        //public IViewComponentResult Invoke()
        //{
        //    return View();
        //}

        private void AddChildItem(ApplTaskModel childItem, StringBuilder strBuilder)
        {

            strBuilder.Append("<ul>");
            List<ApplTaskModel> childItems = (from a in allMenuItems where a.IdApplTaskParent == childItem.IdApplTask select a).ToList();
            foreach (ApplTaskModel cItem in childItems)
            {
                if (cItem.ControllerName != null)
                {
                    var baseUrl = $"{this.Request.Scheme}://{this.Request.Host.Value.ToString()}{this.Request.PathBase.Value.ToString()}";

                    strBuilder.Append("<li>");
                    strBuilder.Append("<a href='" + baseUrl + "/" + cItem.ControllerName + "/" + cItem.ActionName + "'> " + cItem.ApplTaskName + "  </a>");
                    List<ApplTaskModel> subChilds = (from a in allMenuItems where a.IdApplTaskParent == cItem.IdApplTask select a).ToList();
                    if (subChilds.Count > 0)
                    {
                        AddChildItem(cItem, strBuilder);
                    }

                    strBuilder.Append(CLOSE_LIST_ITEM_TAG);
                }
                else
                {
                    strBuilder.Append("<li>");
                    strBuilder.Append("<a href='javascript:void(0)'>  " + cItem.ApplTaskName + "  </a>");
                    List<ApplTaskModel> subChilds = (from a in allMenuItems where a.IdApplTaskParent == cItem.IdApplTask select a).ToList();
                    if (subChilds.Count > 0)
                    {
                        AddChildItem(cItem, strBuilder);
                    }

                    strBuilder.Append(CLOSE_LIST_ITEM_TAG);
                }

            }
            strBuilder.Append(CLOSE_LIST_TAG);
        }
    }
}