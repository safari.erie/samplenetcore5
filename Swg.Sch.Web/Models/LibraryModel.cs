﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Swg.Sch.Web.Models
{
    public class LibraryModel
    {

    }

    public class LibraryCssModel
    {

        public string NameCss { get; set; }
        public string TypeCss { get; set; }
        public string Path { get; set; }
    }

    public class LibraryJsModel
    {

        public string NameJs { get; set; }
        public string TypeJs { get; set; }
        public string Path { get; set; }
    }

    public class LibraryPartialModel
    {
        public string NamePage { get; set; }
        public string TypePage { get; set; }
        public string PathPage { get; set; }
    }


}
