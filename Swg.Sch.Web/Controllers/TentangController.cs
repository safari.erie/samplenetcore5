﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Web.Models;

namespace Swg.Sch.Web.Controllers
{
    public class TentangController : Controller
    {
        private readonly IWebService webService;
        public TentangController(IWebService WebService)
        {
            webService = WebService;
        }
        public IActionResult Index()
        {
            var ret = webService.GetHome(out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.VisiMisi = ret.VisiMisi;
            }
            return View("VisiMisi");
        }
        public IActionResult VisiMisi()
        {
            var ret = webService.GetProfileSekolah(out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.KeteranganVisiMisi = ret.Visi;
                ViewBag.Visi = ret.Visi;
                ViewBag.Misi = ret.Misi;
                ViewBag.DataSekolah = ret;
            }
            return View();
        }
        public IActionResult Direksi()
        {
            var ret = webService.GetJabatanPegawai(out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Direksi = ret;
            }
            return View();
        }
        public IActionResult Kurikulum()
        {
            var ret = webService.GetAbout(out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Kurikulum = ret.Kurikulum;
                ViewBag.VisiMisi = ret.VisiMisi;
            }
            return View();
        }
        public IActionResult Kontak()
        {
            var ret = webService.GetAbout(out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Kontak = ret.Kontak;
            }
            return View();
        }

        public IActionResult Profil()
        {
            var ret = webService.GetAbout(out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Kontak = ret.Kontak;
            }
            return View();
        }

        public List<LibraryJsModel> ModuleJs(string Type)
        {
            List<LibraryJsModel> js = new List<LibraryJsModel>();
            LibraryJsModel home = new LibraryJsModel
            {
                NameJs = "index.js",
                TypeJs = "Home",
                Path = "landingPage"
            };
            if(Type == "VisiMisi")
            {
                js.Add(home);
            }

            return js;
        }
    }
}