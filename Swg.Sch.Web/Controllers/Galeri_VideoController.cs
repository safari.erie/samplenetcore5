﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Shared.Constants;
using Swg.Sch.Shared.Interfaces;

namespace Swg.Sch.Web.Controllers
{
    public class Galeri_VideoController : Controller
    {
        private readonly IWebService webService;
        public Galeri_VideoController(IWebService WebService)
        {
            webService = WebService;
        }
        public IActionResult Index()
        {
            var ret = webService.GetPropen(1, out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.GaleriVideo = ret.GaleriVideo;
            }
            return View("Tkit");
        }
        public IActionResult Tkit()
        {
            var ret = webService.GetPropen(1, out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.GaleriVideo = ret.GaleriVideo;
            }
            return View();
        }
        public IActionResult Sdit()
        {
            var ret = webService.GetPropen(2, out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.GaleriVideo = ret.GaleriVideo;
            }
            return View();
        }
        public IActionResult Smpit()
        {
            var ret = webService.GetPropen(3, out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.GaleriVideo = ret.GaleriVideo;
            }
            return View();
        }
        public IActionResult Smait()
        {
            var ret = webService.GetPropen(4, out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.GaleriVideo = ret.GaleriVideo;
            }
            return View();
        }

        public IActionResult Sekolah()
        {
            var ret = webService.GetGaleriSekolah(JenisGaleriConstant.Video,out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Galeri = ret;
            }
            return View();
        }
    }
}