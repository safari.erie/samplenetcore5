﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Web.Models;

namespace Swg.Sch.Web.Controllers
{
    public class PpdbController : Controller
    {
        private readonly IWebService webService;
        private readonly IPpdbService ppdbService;
        private readonly ISchService schService;
        public PpdbController(
            IWebService WebService,
            ISchService SchService)
        {
            webService = WebService;
            schService = SchService;
        }
        public IActionResult Index()
        {
            var webnopanitiappdb = webService.GetApplSetting("webnopanitiappdb", out string oMsg_webnopanitiappdb);

            if (string.IsNullOrEmpty(oMsg_webnopanitiappdb)) ViewBag.webnopanitiappdb = webnopanitiappdb.ValueString;    
            var ret = schService.GetJenisPrestasis();
            ViewBag.GetJenisPrestasis = ret;
            ViewBag.ModuleJs = ModuleJs("ppdb"); return View();
        }
        public IActionResult Pendaftaran()
        {
            ViewBag.ModuleJs = ModuleJs("ppdb");
            return View("Index");
        }
        public IActionResult Aturan()
        {
            var ret = webService.GetPpdbInfo(out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Aturan = ret.Aturan;
            }
            return View();
        }
        public IActionResult Alur()
        {
            var ret = webService.GetPpdbInfo(out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Alur = ret.Alur;
            }
            return View();
        }
        public IActionResult Biaya()
        {
            var ret = webService.GetPpdbInfo(out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Biaya = ret.Biaya;
            }
            return View();
        }
        public IActionResult Pengumuman()
        {
            var ret = webService.GetPpdbInfo(out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Pengumuman = ret.Pengumuman;
            }
            return View();
        }
        public IActionResult CekStatus()
        {
            ViewBag.ModuleJs = ModuleJs("cekstatus"); return View();
        }
        public IActionResult Wawancara()
        {
            ViewBag.ModuleJs = ModuleJs("wawancara"); return View();
        }
        public IActionResult Formulir()
        {
            var webnopanitiappdb = webService.GetApplSetting("webnopanitiappdb", out string oMsg_webnopanitiappdb);

            if (string.IsNullOrEmpty(oMsg_webnopanitiappdb)) ViewBag.webnopanitiappdb = webnopanitiappdb.ValueString;    
            ViewBag.ModuleJs = ModuleJs("formulir"); return View();
        }

        public List<LibraryJsModel> ModuleJs(string Module)
        {
            List<LibraryJsModel> js = new List<LibraryJsModel>();
            LibraryJsModel ppdb = new LibraryJsModel
            {
                NameJs = "index.js",
                TypeJs = "Register",
                Path = "Ppdb"
            };
            LibraryJsModel cekstatus = new LibraryJsModel
            {
                NameJs = "index.js",
                TypeJs = "CekStatus",
                Path = "Ppdb"
            };
            LibraryJsModel wawancara = new LibraryJsModel
            {
                NameJs = "index.js",
                TypeJs = "Wawancara",
                Path = "Ppdb"
            };
            LibraryJsModel formulir = new LibraryJsModel
            {
                NameJs = "index.js",
                TypeJs = "Formulir",
                Path = "Ppdb"
            };
            if (Module == "ppdb")
            {
                js.Add(ppdb);
            }
            if (Module == "cekstatus")
            {
                js.Add(cekstatus);
            }
            if (Module == "wawancara")
            {
                js.Add(wawancara);
            }
            if (Module == "formulir")
            {
                js.Add(formulir);
            }
            return js;
        }
    }
}