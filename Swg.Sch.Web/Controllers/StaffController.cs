﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Shared.Interfaces;

namespace Swg.Sch.Web.Controllers
{
    public class StaffController : Controller
    {
        private readonly IWebService webService;
        public StaffController(IWebService WebService)
        {
            webService = WebService;
        }
        public IActionResult Index()
        {
            var ret = webService.GetPropen(1, out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Staff = ret.Staff;
            }
            return View("Tkit");
        }
        public IActionResult Tkit()
        {
            var ret = webService.GetPropen(1, out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Staff = ret.Staff;
            }
            return View();
        }
        public IActionResult Sdit()
        {
            var ret = webService.GetPropen(2, out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Staff = ret.Staff;
            }
            return View();
        }
        public IActionResult Smpit()
        {
            var ret = webService.GetPropen(3, out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Staff = ret.Staff;
            }
            return View();
        }
        public IActionResult Smait()
        {
            var ret = webService.GetPropen(4, out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Staff = ret.Staff;
            }
            return View();
        }
    }
}