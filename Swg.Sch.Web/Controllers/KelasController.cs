﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Web.Models;

namespace Swg.Sch.Web.Controllers
{
    public class KelasController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult CekTagihan()
        {
            var jss = ModuleJs("cekTagihan");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult DownloadRaport()
        {
            var jss = ModuleJs("downloadRaport");
            ViewBag.ModuleJs = jss;
            return View();
        }

        public List<LibraryJsModel> ModuleJs(string Module)
        {
            List<LibraryJsModel> js = new List<LibraryJsModel>();
            LibraryJsModel downloadRaport = new LibraryJsModel
            {
                NameJs = "DownloadRaport.js",
                TypeJs = "Kelas",
                Path = "Landing"
            };
            LibraryJsModel cekTagihan = new LibraryJsModel
            {
                NameJs = "CekTagihan.js",
                TypeJs = "Kelas",
                Path = "Landing"
            };

            if (Module == "downloadRaport")
            {
                js.Add(downloadRaport);
            } 
            else if (Module == "cekTagihan")
            {
                js.Add(cekTagihan);
            }


            return js;
        }
    }
}