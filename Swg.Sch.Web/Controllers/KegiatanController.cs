﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Shared.Interfaces;

namespace Swg.Sch.Web.Controllers
{
    public class KegiatanController : Controller
    {
        private readonly IWebService webService;
        public KegiatanController(IWebService WebService)
        {
            webService = WebService;
        }
        public IActionResult Index()
        {
            var ret = webService.GetPropen(1, out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Kegiatan = ret.Kegiatan;
            }
            return View("Tkit");
        }
        public IActionResult Tkit(string Read = null)
        {
            if (Read != null)
            {
                var ret = webService.GetKegiatanByUrl(Read, out string oMessage);
                if (string.IsNullOrEmpty(oMessage))
                {
                    ViewBag.KegiatanByUrl = ret.KegiatanByUrl;
                } else
                {
                    return RedirectToAction("index", "home");
                }
            }
            else
            {
                var ret = webService.GetPropen(1, out string oMessage);
                if (string.IsNullOrEmpty(oMessage))
                {
                    ViewBag.Kegiatan = ret.Kegiatan;
                }
            }
            return View();
        }
        public IActionResult Sdit(string Read = null)
        {
            if (Read != null)
            {
                var ret = webService.GetKegiatanByUrl(Read, out string oMessage);
                if (string.IsNullOrEmpty(oMessage))
                {
                    ViewBag.KegiatanByUrl = ret.KegiatanByUrl;
                }
                else
                {
                    return RedirectToAction("index", "home");
                }
            }
            else
            {
                var ret = webService.GetPropen(2, out string oMessage);
                if (string.IsNullOrEmpty(oMessage))
                {
                    ViewBag.Kegiatan = ret.Kegiatan;
                }
            }
            return View();
        }
        public IActionResult Smpit(string Read = null)
        {
            if (Read != null)
            {
                var ret = webService.GetKegiatanByUrl(Read, out string oMessage);
                if (string.IsNullOrEmpty(oMessage))
                {
                    ViewBag.KegiatanByUrl = ret.KegiatanByUrl;
                }
                else
                {
                    return RedirectToAction("index", "home");
                }
            }
            else
            {
                var ret = webService.GetPropen(3, out string oMessage);
                if (string.IsNullOrEmpty(oMessage))
                {
                    ViewBag.Kegiatan = ret.Kegiatan;
                }
            }
            return View();
        }
        public IActionResult Smait(string Read = null)
        {
            if (Read != null)
            {
                var ret = webService.GetKegiatanByUrl(Read, out string oMessage);
                if (string.IsNullOrEmpty(oMessage))
                {
                    ViewBag.KegiatanByUrl = ret.KegiatanByUrl;
                }
                else
                {
                    return RedirectToAction("index", "home");
                }
            }
            else
            {
                var ret = webService.GetPropen(4, out string oMessage);
                if (string.IsNullOrEmpty(oMessage))
                {
                    ViewBag.Kegiatan = ret.Kegiatan;
                }
            }
            return View();
        }

        public IActionResult Sekolah()
        {
            var ret = webService.GetKegiatanSekolah(out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Kegiatan = ret;
            }
            return View();
        }
    }
}