﻿using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Shared.Interfaces;
using Swg.Services;

namespace Swg.Sch.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IWebService webService;
        public HomeController(IWebService WebService,
            IUserAppService ApplSettingService)
        {
            webService = WebService;
        }
        public IActionResult Index()
        {
            var ret = webService.GetHome(out string oMessage);
            var ret1 = webService.GetPropen(1, out string oMessage1);
            var ret2 = webService.GetPropen(2, out string oMessage2);
            var ret3 = webService.GetPropen(3, out string oMessage3);
            var ret4 = webService.GetPropen(4, out string oMessage4);
            var ApplSettingByCode = webService.GetApplSetting("pathuploadfile", out string oMessageSetting);
            if (string.IsNullOrEmpty(oMessageSetting))
            {
                ViewBag.ApplSettings = ApplSettingByCode;
            }
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Kontak = ret.Kontak;
            }
            if (string.IsNullOrEmpty(oMessage1))
            {
                if (ret1.GaleriImage != null && ret1.GaleriImage.Count > 0)
                {
                    ViewBag.GaleriImageTkit = ret1.GaleriImage[0];
                }
            }
            if (string.IsNullOrEmpty(oMessage2))
            {
                if (ret2.GaleriImage != null && ret2.GaleriImage.Count > 0)
                {
                    ViewBag.GaleriImageSdit = ret2.GaleriImage[0];
                }
            }
            if (string.IsNullOrEmpty(oMessage3))
            {
                if (ret3.GaleriImage != null && ret3.GaleriImage.Count > 0)
                {
                    ViewBag.GaleriImageSmpit = ret3.GaleriImage[0];
                }
            }
            if (string.IsNullOrEmpty(oMessage4))
            {
                if (ret4.GaleriImage != null && ret4.GaleriImage.Count > 0)
                {
                    ViewBag.GaleriImageSmait = ret4.GaleriImage[0];
                }
            }

            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Icon = ret.Icon;
                ViewBag.Logo = ret.Logo;
                ViewBag.FotoSekolah = ret.FotoSekolah;
                ViewBag.Nama = ret.Nama;
                ViewBag.Moto = ret.Moto;
                ViewBag.VisiMisi = ret.VisiMisi;
                ViewBag.MotoUnit = ret.MotoUnit;
                ViewBag.Kontak = ret.Kontak;
            }

            var webbanner = webService.GetApplSetting("webbanner", out string oMsg_webbanner);
            var webbgvideo = webService.GetApplSetting("webbgvideo", out string oMsg_webbgvideo);
            var webjudulutama = webService.GetApplSetting("webjudulutama", out string oMsg_webjudulutama);
            var webmotoutama = webService.GetApplSetting("webmotoutama", out string oMsg_webmotoutama);
            var webboxppdb = webService.GetApplSetting("webboxppdb", out string oMsg_webboxppdb);
            var weburlbanner = webService.GetApplSetting("weburlbanner", out string oMsg_weburlbanner);

            if (string.IsNullOrEmpty(oMsg_webbanner)) ViewBag.webbanner = webbanner.ValueString;         
            if (string.IsNullOrEmpty(oMsg_webbgvideo)) ViewBag.webbgvideo = webbgvideo.ValueString;         
            if (string.IsNullOrEmpty(oMsg_webjudulutama)) ViewBag.webjudulutama = webjudulutama.ValueString;         
            if (string.IsNullOrEmpty(oMsg_webmotoutama)) ViewBag.webmotoutama = webmotoutama.ValueString;         
            if (string.IsNullOrEmpty(oMsg_webboxppdb)) ViewBag.webboxppdb = webboxppdb.ValueString;         
            if (string.IsNullOrEmpty(oMsg_weburlbanner)) ViewBag.weburlbanner = weburlbanner.ValueString;         

            return View();
        }
        public IActionResult Index1()
        {
            var ret = webService.GetHome(out string oMessage);
            var ret1 = webService.GetPropen(1, out string oMessage1);
            var ret2 = webService.GetPropen(2, out string oMessage2);
            var ret3 = webService.GetPropen(3, out string oMessage3);
            var ret4 = webService.GetPropen(4, out string oMessage4);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Kontak = ret.Kontak;
            }
            if (string.IsNullOrEmpty(oMessage1))
            {
                if (ret1.GaleriImage != null && ret1.GaleriImage.Count > 0)
                {
                    ViewBag.GaleriImageTkit = ret1.GaleriImage[0];
                }
            }
            if (string.IsNullOrEmpty(oMessage2))
            {
                if (ret2.GaleriImage != null && ret2.GaleriImage.Count > 0)
                {
                    ViewBag.GaleriImageSdit = ret2.GaleriImage[0];
                }
            }
            if (string.IsNullOrEmpty(oMessage3))
            {
                if (ret3.GaleriImage != null && ret3.GaleriImage.Count > 0)
                {
                    ViewBag.GaleriImageSmpit = ret3.GaleriImage[0];
                }
            }
            if (string.IsNullOrEmpty(oMessage4))
            {
                if (ret4.GaleriImage != null && ret4.GaleriImage.Count > 0)
                {
                    ViewBag.GaleriImageSmait = ret4.GaleriImage[0];
                }
            }

            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Icon        = ret.Icon;
                ViewBag.Logo        = ret.Logo;
                ViewBag.FotoSekolah = ret.FotoSekolah;
                ViewBag.Nama        = ret.Nama;
                ViewBag.Moto        = ret.Moto;
                ViewBag.VisiMisi    = ret.VisiMisi;
                ViewBag.MotoUnit    = ret.MotoUnit;
                ViewBag.Kontak      = ret.Kontak;
            }

            return View();
        }


    }
}
