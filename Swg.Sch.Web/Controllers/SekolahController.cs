﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Shared.Interfaces;

namespace Swg.Sch.Web.Controllers
{
    public class SekolahController : Controller
    {
        private readonly IWebService _webService;
        public SekolahController(IWebService WebService)
        {
            _webService = WebService;
        }
        public IActionResult Index()
        {
            var ret = _webService.GetProfSekolah(out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.ProfilSekolah = ret;
            }
            return View();
        }
    }
}