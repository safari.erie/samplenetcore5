﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Shared.Interfaces;

namespace Swg.Sch.Web.Controllers
{
    public class DkmController : Controller
    {
        private readonly IWebService webService;
        public DkmController(IWebService WebService)
        {
            webService = WebService;
        }
        public IActionResult Index()
        {
            var ret = webService.GetPropenDkm(out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Profile = ret.Profile;
            }
            return View("Profile");
        }
        public IActionResult Profile()
        {
            var ret = webService.GetPropenDkm(out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Profile = ret.Profile;
            }
            return View();
        }
        public IActionResult Kegiatan(string Read = null)
        {
            if (Read != null)
            {
                var ret = webService.GetKegiatanByUrl(Read, out string oMessage);
                if (string.IsNullOrEmpty(oMessage))
                {
                    ViewBag.KegiatanByUrl = ret.KegiatanByUrl;
                }
                else
                {
                    return RedirectToAction("index", "home");
                }
            }
            else
            {
                var ret = webService.GetPropenDkm(out string oMessage);
                if (string.IsNullOrEmpty(oMessage))
                {
                    ViewBag.Kegiatan = ret.Kegiatan;
                }
            }
            return View();
        }
        public IActionResult Staff()
        {
            var ret = webService.GetPropenDkm(out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.Staff = ret.Staff;
            }
            return View();
        }
        public IActionResult Galeri_Image()
        {
            var ret = webService.GetPropenDkm(out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.GaleriImage = ret.GaleriImage;
            }
            return View();
        }
        public IActionResult Galeri_Video()
        {
            var ret = webService.GetPropenDkm(out string oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ViewBag.GaleriVideo = ret.GaleriVideo;
            }
            return View();
        }
    }
}