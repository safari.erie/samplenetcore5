$(document).ready((e) => {
    var i = 0;
    GetJenisPrestasis(i);
    GetTingkatPrestasiSiswas(i);
    GetTahuns(i);
    GetKategoriDaftars();
    GetJenisPekerjaans();

    ComboGetAgama(function (obj) {
        $("#IdAgamaIbu").html(obj);
        $("#IdAgamaAyah").html(obj);
        $("#IdAgamaWali").html(obj);
    });
   
    validate_type_file();
    validate_max_file();
    $('#TanggalLahir').mask('00-00-0000');
    $('#TanggalLahirAyah').mask('00-00-0000');
    $('#TanggalLahirIbu').mask('00-00-0000');
    $('#TanggalMeninggalAyah').mask('00-00-0000');
    $('#TanggalMeninggalIbu').mask('00-00-0000');
    $('#TanggalMeninggalWali').mask('00-00-0000');

    ComboGetListJenjangPendidikan(function (obj) {
        $('select#IdJenjangPendidikanIbu').html(obj);
        $('select#IdJenjangPendidikanAyah').html(obj);
        $('select#IdJenjangPendidikanWali').html(obj);
    });
    
    $("select[name='IdTinggal']").change((e) => {
        if (e.target.value == "2") {
            $('#DivDataWali').fadeIn();
        } else {
            $('#DivDataWali').fadeOut();
        }
    });
    $("select[name='AlatTransportasi']").change((e) => {
        if (e.target.value == "Lainnya") {
            $('#DivLainnyaAlatTransportasi').fadeIn();
            $('#AlatTransportasiLainnya').attr({
                'data-input': 'wajib',
                'required': true,
            });
        } else {
            $('#AlatTransportasiLainnya').val('');
            $('#DivLainnyaAlatTransportasi').fadeOut();
            $('#AlatTransportasiLainnya').removeAttr('data-input', 'required');
        }
    });
    $("select[name='PenyakitYangPernahDiderita']").change((e) => {
        if (e.target.value == "Lainnya") {
            $('#DivLainnyaPenyakitYangPernahDiderita').fadeIn();
            $('#PenyakitYangPernahDideritaLainnya').attr({
                'data-input': 'wajib',
                'required': true,
            });
        } else {
            $('#PenyakitYangPernahDideritaLainnya').val('');
            $('#DivLainnyaPenyakitYangPernahDiderita').fadeOut();
            $('#PenyakitYangPernahDideritaLainnya').removeAttr('data-input', 'required');
        }
    });
    $("#IdStatusHidupIbu").change((e) => {
            if (e.target.value == "0") {
            $('#DivSudahTiadaIbu').fadeIn();
            $('#TanggalMeninggalIbu').attr({
                'data-input': 'wajib',
                'required': true,
            });
        } else {
            $('#TanggalMeninggalIbu').val('');
            $('#DivSudahTiadaIbu').fadeOut();
            $('#TanggalMeninggalIbu').removeAttr('data-input', 'required');
        }
    });
    $("#IdStatusHidupAyah").change((e) => {
            if (e.target.value == "0") {
            $('#DivSudahTiadaAyah').fadeIn();
            $('#TanggalMeninggalAyah').attr({
                'data-input': 'wajib',
                'required': true,
            });
        } else {
            $('#TanggalMeninggalAyah').val('');
            $('#DivSudahTiadaAyah').fadeOut();
            $('#TanggalMeninggalAyah').removeAttr('data-input', 'required');
        }
    });
    $("#IdStatusHidupWali").change((e) => {
            if (e.target.value == "0") {
            $('#DivSudahTiadaWali').fadeIn();
            $('#TanggalMeninggalWali').attr({
                'data-input': 'wajib',
                'required': true,
            });
        } else {
            $('#TanggalMeninggalWali').val('');
            $('#DivSudahTiadaWali').fadeOut();
            $('#TanggalMeninggalWali').removeAttr('data-input', 'required');
        }
    });

    /* ADD KELAS PARALEL ONCHANGE */
    // $("select[name='IdKelasParalel']").change((e) => {
    //     alert(e.target.text)
    // })
    /* END KELAS PARALEL ONCHANGE */


    // $("select[name='IdJenisPendaftaran']").change((e) => {
    //     let Id = e.target.value;
    //     $('#divIdJalurPendaftaran').fadeIn();
    //     $('#divIdSiswaSaudara').fadeIn();
    //     GetJalurPendaftarans();

    //     if (Id == 1) {
    //         var html = '<div class="form-group row">' +
    //             '    <label class="col-sm-4 col-form-label">NIS Peserta Didik</label>' +
    //             '    <div class="col-sm-8">' +
    //             '        <div class="input-group">' +
    //             '            <input id="Nisn" type="number" name="Nisn" class="form-control" placeholder="Ketik NIS Peserta Didik..." data-input="wajib" required>' +
    //             '            <div class="input-group-append">' +
    //             '                <button id="BtnCariNisn" onClick="GetSiswaByNisn();" class="btn btn-theme" type="button"> <div id="htmlBtnCariNisn"><i class="fa fa-search"></i> Cari</div></button>' +
    //             '            </div>' +
    //             '        </div>' +
    //             '        <div class="invalid-feedback">' +
    //             '           Kolom Wajib diisi.' +
    //             '        </div>' +
    //             '    </div>' +
    //             '</div>';
    //         $('#divNisn').html(html);
    //         GetJenisPrestasis();
    //         setTimeout(() => {
    //             ModalSaudaraKandung(true);
    //         }, 1000)
    //         $('button#BtnRegister0').attr('disabled', true);
    //     }

    //     if (Id == 2) {
    //         var html = '<div class="form-group row">' +
    //             '    <label class="col-sm-4 col-form-label">NISN Calon Peserta Didik</label>' +
    //             '    <div class="col-sm-8">' +
    //             '        <input id="Nisn" type="number" name="Nisn" class="form-control" placeholder="Ketik NISN Calon Peserta Didik..." data-input="wajib" required>' +
    //             '           <small>Wajib cek terlebih dahulu NISN anda <a href="https://referensi.data.kemdikbud.go.id/nisn/" target="_blank">disini</a></small>' +
    //             '        <div class="invalid-feedback">' +
    //             '           Kolom Wajib diisi.' +
    //             '        </div>' +
    //             '    </div>' +
    //             '</div>';
    //         $('#divNisn').html(html);
    //         $('#divNisn').fadeIn();
    //         GetJenisPrestasis();
    //         setTimeout(() => {
    //             ModalSaudaraKandung(true);
    //         }, 1000)
    //     }
    // })
    // $("select[name='IdJalurPendaftaran']").change((e) => {
    //     let Id = e.target.value;
    //     if (Id == 1) {
    //         ModalPrestasis(true);
    //     } else {
    //         ModalPrestasis(false);
    //         $('#FormPrestasis')[0].reset();
    //     }

    //     $('#divSeragam').fadeIn();
    //     $('#divWakaf').fadeIn();
    // })

})

const UrlService = {
    GetPpdbDaftar: base_url + '/api/webs/GetPpdbDaftar',
    GetPpdbSiswa: base_url + '/api/webs/GetPpdbSiswa',
    GetPertanyaanKuisioners: base_url + '/api/webs/GetPertanyaanKuisioners',
    GetPpdbBiayaByUnit: base_url + '/api/webs/GetPpdbBiayaByUnit',
}; 

jQuery.fn.serializeObject = function () {
    var arrayData, objectData;
    arrayData = this.serializeArray();
    objectData = {};

    $.each(arrayData, function () {
        var value;

        if (this.value != null) {
            value = this.value;
        } else {
            value = '';
        }

        if (objectData[this.name] != null) {
            if (!objectData[this.name].push) {
                objectData[this.name] = [objectData[this.name]];
            }

            objectData[this.name].push(value);
        } else {
            objectData[this.name] = value;
        }
    });

    return objectData;
};

var i = 0;

// GetKategoriDaftars = () => {
//     $.getJSON(base_url + "/api/webs/GetKategoriDaftars", (res) => {
//         let html = '<option value="">Select One</option>';
//         if (res.IsSuccess) {
//             $.each(res.Data, (i, v) => {
//                 html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
//             });
//         } else console.log(res)
//         $('select[name="IdJenisKategoriPendaftaran"]').html(html);
//     });
// }

GetUnits = () => {
    $.getJSON(base_url + "/api/webs/GetUnits", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
                return i < 3;
            });
        } else console.log(res)
        $('select[name="IdUnit"]').html(html);
    });
}

// GetKelass = (IdUnit) => {
//     // var Url = base_url + "/api/webs/GetKelass?IdUnit=" + IdUnit;
//     // $.getJSON(Url, (res) => {
//     //     let html = '<option value="">Select One</option>';
//     //     if (res.IsSuccess) {
//     //         $.each(res.Data, (i, v) => {
//     //             html += '<option value="' + v.IdKelas + '">' + v.Nama + '</option>';
//     //         });
//     //     } else console.log(res)
//     //     $('select[name="IdKelas"]').html(html);
//     // });
//     var Url = base_url + "/api/webs/GetKelasPpdbs";
//     $.getJSON(Url, (res) => {
//         let html = '<option value="">Select One</option>';
//         if (res.IsSuccess) {
//             $.each(res.Data, (i, v) => {
//                 html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
//             });
//         } else console.log(res)
//         $('select[name="IdPpdbKelas"]').html(html);
//     });
// }

GetKelasParalels = (IdKelas) => {
    var Url = base_url + "/api/webs/GetKelasParalels?IdKelas=" + IdKelas;
    $.getJSON(Url, (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                html += '<option value="' + v.IdKelasParalel + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('select[name="IdKelasParalel"]').html(html);
    });
}

// GetJenisPendaftarans = () => {
//     $.getJSON(base_url + "/api/webs/GetJenisPendaftarans", (res) => {
//         let html = '<option value="">Select One</option>';
//         if (res.IsSuccess) {
//             $.each(res.Data, (i, v) => {
//                 html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
//             });
//         } else console.log(res)
//         $('select[name="IdJenisPendaftaran"]').html(html);
//     });
// }

// GetJalurPendaftarans = () => {
//     $.getJSON(base_url + "/api/webs/GetJalurPendaftarans", (res) => {
//         let html = '<option value="">Select One</option>';
//         if (res.IsSuccess) {
//             $.each(res.Data, (i, v) => {
//                 html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
//             });
//         } else console.log(res)
//         $('select[name="IdJalurPendaftaran"]').html(html);
//     });
// }

GetJenisPekerjaans = () => {
    $.getJSON(base_url + "/api/webs/GetJenisPekerjaans", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('#IdJenisPekerjaanIbu').html(html);
        $('#IdJenisPekerjaanAyah').html(html);
        $('#IdJenisPekerjaanWali').html(html);
    });
}

const GetSiswaByNisn = (Nisn) => {
    $.getJSON(base_url + "/api/webs/GetSiswaByNis?nis=" + Nisn, (res) => {

        if (res.IsSuccess) {
            const {
                Agama,
                AlamatOrtu,
                AlamatTinggal,
                Email,
                EmailOrtu,
                IdAgama,
                IdJenisPekerjaanAyah,
                IdJenisPekerjaanIbu,
                IdJenisPrestasi,
                IdKelas,
                IdKelasParalel,
                IdSiswa,
                IdUnit,
                JenisKelamin,
                JenisPekerjaanAyah,
                JenisPekerjaanIbu,
                KdJenisKelamin,
                Kelas,
                KelasParalel,
                Nama,
                NamaAyah,
                NamaIbu,
                NamaInstansiAyah,
                NamaInstansiIbu,
                NamaPanggilan,
                Nik,
                NikAyah,
                NikIbu,
                Nisn,
                NoDarurat,
                NoHandphone,
                Status,
                StrStatus,
                TanggalLahir,
                TempatLahir,
                Unit,
            } = res.Data;

            $('#Nisn').val(Nisn);
            $('#NikSiswa').val(Nik);
            $('#NamaPanggilan').val(NamaPanggilan);
            $('#IdAgama').val(IdAgama);
            $('#Nisn').val(Nisn);
            $('#Nisn').val(Nisn);

            $('input#NikIbu').val(NikIbu);
            $('input#NamaIbu').val(NamaIbu);
            $('select#IdJenisPekerjaanIbu').val(IdJenisPekerjaanIbu)
            $('input#NamaInstansiIbu').val(NamaInstansiIbu)
            $('input#NikAyah').val(NikAyah);
            $('input#NamaAyah').val(NamaAyah);
            $('select#IdJenisPekerjaanAyah').val(IdJenisPekerjaanAyah)
            $('input#NamaInstansiAyah').val(NamaInstansiAyah)
            $('input#AlamatIbu').val(AlamatOrtu);
            $('input#AlamatAyah').val(AlamatOrtu);
            $('input#NoHandphoneIbu').val(NoHandphone);
            $('input#NoHandphoneAyah').val(NoHandphone);
            $('input#EmailIbu').val(EmailOrtu);
            $('input#EmailAyah').val(EmailOrtu);

            $('#BtnRegister0').attr('disabled', false);
                
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Gagal',
                text: 'NISN Tidak Ditemukan!'
            })
        }
    });
}

GetJenisPrestasis = (i) => {
    $.getJSON(base_url + "/api/webs/GetJenisPrestasis", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('select[name="IdJenisPrestasi[' + i + ']"]').html(html);
    });
}

GetTingkatPrestasiSiswas = (i) => {
    $.getJSON(base_url + "/api/webs/GetTingkatPrestasiSiswas", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('select[name="IdJenisTingkatPrestasi[' + i + ']"]').html(html);
    });
}

GetTahuns = (i) => {
    var StartDate = (new Date).getFullYear() - 4;
    var EndDate = (new Date).getFullYear();
    var HtmlCombo = "";
    HtmlCombo += "<option value=''>Select One</option>";
    for (var ix = StartDate; ix <= EndDate; ix++) {
        if (ix == EndDate) {
            HtmlCombo += "<option value='" + ix + "'>" + ix + "</option>"; //jadikan selected
        } else {
            HtmlCombo += "<option value='" + ix + "'>" + ix + "</option>";
        }
    }
    $('select[name="Tahun[' + i + ']"]').html(HtmlCombo)
}



ModalPrestasis = (bool) => {
    $('#FormPrestasis').removeClass('was-validated');
    if (bool) {
        $('small#infoPrestasi').fadeIn();
        $('#modalPrestasis').modal('show');
    } else if (!bool) {
        $('small#infoPrestasi').fadeOut();
        $('#modalPrestasis').modal('hide');
    }
}

ModalSaudaraKandung = (bool) => {
    if (bool) {
        $('#FormSaudaraKandung').fadeOut();
        $('#modalSaudaraKandung').modal('show');
        $('#htmlPertanyaanSK').show();
        $('#htmlBtnMemilikiSk').html('Ya, Saya Memiliki');
        $('#BtnMemilikiSK').show();
        $('#BtnMemilikiSK').attr('onclick', 'DataSaudaraKandung(false)');
    } else if (!bool) {
        $('#FormSaudaraKandung').fadeIn();
        $('#htmlPertanyaanSK').hide();
        $('#htmlBtnMemilikiSk').html('Cari Sekarang !');
        $('#BtnMemilikiSK').attr('onclick', 'GetSiswaByNisn(true)');
    }
}

ModalSaudaraKandungInfo = () => {
    $('#FormSaudaraKandung').fadeIn();
    $('#modalSaudaraKandung').modal('show');
    $('#htmlPertanyaanSK').hide();
    $('#htmlBtnMemilikiSk').html('Simpan');
    $('#BtnMemilikiSK').show();
    $('#BtnMemilikiSK').attr('onclick', 'DataSaudaraKandung(true)');
}

ModalBatalkanPrestasis = () => {
    ModalPrestasis(false);
    $('select#IdJalurPendaftaran').val('');
    $('#FormPrestasis')[0].reset();
}

ModalTempatTinggal = () => {
    $('#modalKetTempatTinggal').modal('show');
}

ModalKesehatan = () => {
    $('#modalKetKesehatan').modal('show');
}

ModalSekolahAsal = () => {
    $('#modalKetSekolahAsal').modal('show');
}

ModalDataOrtu = () => {
    $('#modalDataOrtu').modal('show');
}

ModalBerkas = () => {
    $('#modalBerkas').modal('show');
    $('#nav-tab a[href="#nav-contoh-berkas"]').tab('show');
}

DataPrestasis = () => {
    var TypeInput = true;
    $('[data-input="wajib-prestasis"]').each(function () {
        if ($(this).val() == "") {
            TypeInput = false;
            $('#FormPrestasis').addClass('was-validated');
        }
    })
    if (TypeInput) {
        $('#modalPrestasis').modal('hide');
    }
}

DataSaudaraKandung = (bool = false) => {
    if (!bool) {
        ModalSaudaraKandung(false);
    } else {
        $('#htmlInfoSK').fadeIn();
        $('#modalSaudaraKandung').modal('hide');
    }
}

DataPrestasisReset = () => {
    $('div#formPrestasis').empty();
}

DataSaudaraKandungReset = () => {
    $('input#NisnSaudara', '#FormSaudaraKandung').attr('readonly', false);
    $('input#NisnSaudara', '#FormSaudaraKandung').val('');
    $('input#IdSiswaSaudara', '#FormSaudaraKandung').val('');
    $('#modalSaudaraKandung').modal('hide');
    $('div#alertSK').fadeOut();
    $('small#htmlInfoSK').fadeOut();
    $('input#IdSiswaSaudara').val('');
}

DataNisnReset = () => {
    $('#FormRegister')[0].reset();
    $('input#Nisn', '#FormRegister').attr('readonly', false);
    $('div#htmlBtnCariNisn').html('<i class="fa fa-search"></i> Cari Ulang');
    $('button#BtnCariNisn').attr('onclick', 'GetSiswaByNisn()');
    $('button#BtnRegister0').attr('disabled', true);
}

GetPpdbByEmail = (Email) => {
    $('div#secRegister').hide();
    $('div#secKuisioner1').fadeIn();
    scrollToBottom('secKuisioner1');
    $('#FormRegister').removeClass('was-validated');
    $('#FormDataOrtu').removeClass('was-validated');
    $('#FormBerkas').removeClass('was-validated');
    return;
    
    $.ajax({
        url: base_url + '/api/webs/GetPpdbByEmail?email=' + Email,
        type: 'GET',
        dataType: 'JSON',
        beforeSend: (xhr) => {

        },
        success: (res) => {
            if (res.IsSuccess) {
                var htmlSiswaTerdaftar = '';
                $.each(res.Data, (i, v) => {
                    htmlSiswaTerdaftar += `
                    <div class="effect1" style="border-radius:15px;background:white;margin:9px">
                        <div class="row">
                        <div class="col-4" style="padding:0px">
                            <div class="text-center mb-2">
                                <img onerror="this.onerror=null; this.src='${base_url}/assets/img/dummy/no-image.png'" src="${base_url}/FileBerkas/${v.Foto}" width="100" class="mt-2 img-fluid effect1" style="border-radius:5px;" />
                            </div>
                        </div>
                        <div class="col-8">
                            <p class="mt-3">
                                <b>Nama</b> : ${v.Nama}<br />
                                <b>Kelas</b> : ${v.Kelas}<br />
                                <b>Tanggal Daftar</b> : ${indoDate(v.TanggalDaftar)}
                            </p>
                        </div>
                        </div>
                    </div>
                    `;
                });
                $('#htmlSiswaTerdaftar').html(htmlSiswaTerdaftar)
                $('#modalGetPpdbByEmail').modal('show');
            } else {
                // $('div#secRegister').hide();
                // $('div#secKuisioner1').fadeIn();
                // scrollToBottom('secKuisioner1');
                // $('#FormRegister').removeClass('was-validated');
                // $('#FormDataOrtu').removeClass('was-validated');
                // $('#FormBerkas').removeClass('was-validated');
                DataPreviewForm();
            }
        },
        "error": (err, a, e) => {
            Swal.fire({
                icon: 'error',
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(err),
                // footer: '<a href>Bukan saya yang daftar</a>'
            })
        }
    })
}

BtnLanjutkanTerdaftar = () => {
    $('#modalGetPpdbByEmail').modal('hide');
    // $('div#secRegister').hide();
    // $('div#secKuisioner1').fadeIn();
    // scrollToBottom('secKuisioner1');
    $('#FormRegister').removeClass('was-validated');
    $('#FormDataOrtu').removeClass('was-validated');
    $('#FormBerkas').removeClass('was-validated');
    DataPreviewForm();
}

BtnSimpanKetTempatTinggal = () => {
    $('#modalKetTempatTinggal').modal('hide');
    var TypeInput = true;
    $('[data-input="wajib"]', '#FormKetTempatTinggal').each(function (i, v) {
        if ($(this).val() == "") {
            TypeInput = false;
            $('#FormKetTempatTinggal').addClass('was-validated');
        }
    })
    if (!TypeInput) {
        $('#infoKetTempatTinggal').fadeIn();
    } else {
        $('#infoKetTempatTinggal').hide();
    }
}

BtnSimpanKetKesehatan = () => {
    $('#modalKetKesehatan').modal('hide');
    var TypeInput = true;
    $('[data-input="wajib"]', '#FormKetKesehatan').each(function (i, v) {
        if ($(this).val() == "") {
        TypeInput = false;
            $('#FormKetKesehatan').addClass('was-validated');
        }
    })
    if (!TypeInput) {
        $('#infoKetKesehatan').fadeIn();
    } else {
        $('#infoKetKesehatan').hide();
    }
}

BtnSimpanKetSekolahAsal = () => {
    $('#modalKetSekolahAsal').modal('hide');
    var TypeInput = true;
    $('[data-input="wajib"]', '#FormKetSekolahAsal').each(function (i, v) {
        if ($(this).val() == "") {
            TypeInput = false;
            $('#FormKetSekolahAsal').addClass('was-validated');
        }
    })
    if (!TypeInput) {
        $('#infoKetSekolahAsal').fadeIn();
    } else {
        $('#infoKetSekolahAsal').hide();
    }
}

BtnSimpanDataOrtu = () => {
    $('#modalDataOrtu').modal('hide');
    var TypeInput = true;
    $('[data-input="wajib"]', '#FormDataOrtu').each(function (i, v) {
        if ($(this).val() == "") {
            TypeInput = false;
            $('#FormDataOrtu').addClass('was-validated');
        }
    })
    if (!TypeInput) {
        $('#infoDataOrtu').fadeIn();
    } else {
        $('#infoDataOrtu').hide();
    }
}

const BtnSimpanBerkas = () => {
    $('#modalBerkas').modal('hide');
    var TypeInput = true;
    $('[data-input="wajib"]', '#FormBerkas').each(function (i, v) {
        if ($(this).val() == "") {
            TypeInput = false;
            $('#FormBerkas').addClass('was-validated');
        }
    })

    // var IdUnit = $("select[name='IdUnit'] option:selected").val();
    // if (IdUnit == 3 || IdUnit == 4) {
    //     $('[data-input="wajib"]', '#FormRaport').each(function (i, v) {
    //         if ($(this).val() == "") {
    //             TypeInput = false;
    //             $('#FormRaport').addClass('was-validated');
    //         }
    //     })
    // }
    
    if (!TypeInput) {
        $('#infoBerkas').fadeIn();
    } else {
        $('#infoBerkas').hide();
    }
}

const ValidateRaportByUnit = (IdUnit) => {
    var i = 0;
    if (IdUnit == 3) { //smp
        $('#FormRaport').addClass('was-validated');
        while (i < 5) {
            i++
            $('#Matematika'+i).attr({
                'data-input': 'wajib',
                'required': true,
            });
            $('#Bindo'+i).attr({
                'data-input': 'wajib',
                'required': true,
            });
            $('#Ipa'+i).attr({
                'data-input': 'wajib',
                'required': true,
            });
            $('#Binggris'+i).removeAttr('data-input', 'required');
            $('#Ips'+i).removeAttr('data-input', 'required');
            $('#FileRaportS'+i).attr({
                'data-input': 'wajib',
                'required': true,
            });
        }
    } else if (IdUnit == 4) { //sma
        $('#FormRaport').addClass('was-validated');
        while (i < 5) {
            i++
            $('#Matematika'+i).attr({
                'data-input': 'wajib',
                'required': true,
            });
            $('#Bindo'+i).attr({
                'data-input': 'wajib',
                'required': true,
            });
            $('#Ipa'+i).attr({
                'data-input': 'wajib',
                'required': true,
            });
            $('#Binggris'+i).attr({
                'data-input': 'wajib',
                'required': true,
            });
            $('#Ips'+i).attr({
                'data-input': 'wajib',
                'required': true,
            });
            $('#FileRaportS'+i).attr({
                'data-input': 'wajib',
                'required': true,
            });
        }
    } else {
        while (i < 5) {
            i++
            $('#Matematika'+i).removeAttr('data-input', 'required');
            $('#Bindo'+i).removeAttr('data-input', 'required');
            $('#Ipa'+i).removeAttr('data-input', 'required');
            $('#Binggris'+i).removeAttr('data-input', 'required');
            $('#Ips'+i).removeAttr('data-input', 'required');
            $('#FileRaportS'+i).removeAttr('data-input', 'required');
        }
    }
        
}

const ValidateAsalSekolah = (IdUnit) => {
    if (IdUnit == 1) { //tk
        $('#FormKetSekolahAsal').removeClass('was-validated');
        $('#SekolahAsal').removeAttr('data-input', 'required');
        $('#AlamatSekolahAsal').removeAttr('data-input', 'required');
        $('#NspnSekolahAsal').removeAttr('data-input', 'required');
    } else {
        $('#SekolahAsal').attr({
            'data-input': 'wajib',
            'required': true,
        });
        $('#AlamatSekolahAsal').attr({
            'data-input': 'wajib',
            'required': true,
        });
        $('#NspnSekolahAsal').attr({
            'data-input': 'wajib',
            'required': true,
        });
    }
}

const DataRegister = (Proses, Sec) => {
    // GetPpdbBiayaByUnit()
    // return;
    if (Proses == 0) {
        var TypeInput = true;
        var IdUnit = $("select[name='IdUnit'] option:selected").val();
        ValidateRaportByUnit(IdUnit);
        ValidateAsalSekolah(IdUnit);
        $('[data-input="wajib"]').each(function (i, v) {
            if ($(this).val() == "") {
                TypeInput = false;
                $('#FormRegister').addClass('was-validated');
                $('#FormDataOrtu').addClass('was-validated');
                $('#FormBerkas').addClass('was-validated');
                
                // var IdUnit = $("select[name='IdUnit'] option:selected").val();
                // ValidateRaportByUnit(IdUnit);
                // ValidateAsalSekolah(IdUnit);
                    
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal',
                    text: 'Kolom ' + $(this).attr("label") + ' Wajib Diisi',
                })
                return i < 0;
            }
        })

        if (TypeInput) {
            Swal.fire({
                title: 'Apakah Anda Yakin?',
                text: "Jika data yang dimasukan sudah benar silahkan klik tombol lanjutkan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Lanjutkan!'
            }).then((result) => {
                if (result.value) {
                    $('body').css('pointer-events', 'none');

                    let timerInterval;
                    Swal.fire({
                        title: 'Tunggu Sebentar!',
                        // html: 'Anda akan diarahkan kehalaman kuisioner.',
                        html: 'Sedang memuat berkas anda.',
                        timer: 3000,
                        timerProgressBar: true,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                                const content = Swal.getContent()
                                if (content) {
                                    const b = content.querySelector('b')
                                    if (b) {
                                        b.textContent = Swal.getTimerLeft()
                                    }
                                }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then((result) => {
                        /* Read more about handling dismissals below */
                        if (result.dismiss === Swal.DismissReason.timer) {

                            $('body').css('pointer-events', 'unset');
                            /* cek getsiswabyemail */
                            var cekGetPpdbByEmail = GetPpdbByEmail($('#EmailOrtu').val());
                        }
                    })
                }
            })

        }
    } else if (Proses == 1) {
        var TypeInput = true;
        $('label[for="Kuisioner1"]').each(function (i, v) {
            if (!$("input[name='OpsiJawaban[" + i + "]']").is(':checked')) {
                TypeInput = false;
                $('#FormRegister').addClass('was-validated');
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal',
                    text: 'Pertanyaan ' + $(this).attr("kuis") + ' Wajib Diisi',
                })
                return i < 0;
            }
        })
        if (TypeInput) {
            $('div#secKuisioner1').hide();
            $('div#secKuisioner2').fadeIn();
            scrollToBottom('secKuisioner2');
            $('#FormRegister').removeClass('was-validated');
        }
    } else if (Proses == 2) {
        var TypeInput = true;
        $('label[for="Kuisioner2"]').each(function (i, v) {
            if (!$("input[name='OpsiJawaban[2" + i + "]']").is(':checked')) {
                TypeInput = false;
                $('#FormRegister').addClass('was-validated');
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal',
                    text: 'Pertanyaan ' + $(this).attr("kuis") + ' Wajib Diisi',
                })
                return i < 0;
            }
        })
        if (TypeInput) {
            $('div#secKuisioner2').hide();
            $('div#secKuisioner3').fadeIn();
            scrollToBottom('secKuisioner3');
            $('#FormRegister').removeClass('was-validated');
        }
    } else if (Proses == 3) {
        var TypeInput = true;
        $('label[for="Kuisioner3"]').each(function (i, v) {
            if (!$("input[name='OpsiJawaban[3" + i + "]']").is(':checked')) {
                TypeInput = false;
                $('#FormRegister').addClass('was-validated');
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal',
                    text: 'Pertanyaan ' + $(this).attr("kuis") + ' Wajib Diisi',
                })
                return i < 0;
            }
        });
        $('[data-input="wajib_lainnya"]').each(function (i, v) {
            if ($(this).val() == "") {
                TypeInput = false;
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal',
                    text: 'Kolom Lainnya Pertanyaan : ' + $(this).attr("label") + ' Wajib Diisi',
                })
                return i < 0;
            }
        });
        if (TypeInput) {
            $('div#secKuisioner3').hide();
            $('div#secKuisioner4').fadeIn();
            scrollToBottom('secKuisioner4');
            $('#FormRegister').removeClass('was-validated');
        }
    } else if (Proses == 4) {
        var TypeInput = true;
        $('label[for="Kuisioner4"]').each(function (i, v) {
            if ($("input[name='Jawaban[4" + i + "]']").val() == "") {
                TypeInput = false;
                $('#FormRegister').addClass('was-validated');
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal',
                    text: 'Pertanyaan ' + $(this).attr("kuis") + ' Wajib Diisi',
                })
                return i < 0;
            }
        })
        if (TypeInput) {
            $('div#secKuisioner4').hide();
            $('div#secKuisioner5').fadeIn();
            scrollToBottom('secKuisioner5');
            $('#FormRegister').removeClass('was-validated');
        }
    } else if (Proses == 5) {
        var TypeInput = true;
        $('label[for="Kuisioner5"]').each(function (i, v) {
            if (!$("input[name='OpsiJawaban[5" + i + "]']").is(':checked')) {
                TypeInput = false;
                $('#FormRegister').addClass('was-validated');
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal',
                    text: 'Pertanyaan Nomor ' + $(this).attr("kuis") + ' Wajib Diisi',
                })
                return i < 0;
            }
        })
        if (TypeInput) {
            Swal.fire({
                title: 'Apakah Anda Yakin?',
                text: "Jika data yang dimasukan sudah benar silahkan klik tombol lanjutkan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Lanjutkan!',
                cancelButtonText: 'Batalkan',
                allowOutsideClick: false
            }).then((result) => {
                if (result.value) {
                    $('body').css('pointer-events', 'none');

                    let timerInterval;
                    Swal.fire({
                        title: 'Tunggu Sebentar!',
                        html: 'Sedang mengambil data...',
                        timer: 3000,
                        timerProgressBar: true,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                                const content = Swal.getContent()
                                if (content) {
                                    const b = content.querySelector('b')
                                    if (b) {
                                        b.textContent = Swal.getTimerLeft()
                                    }
                                }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then((result) => {
                        /* Read more about handling dismissals below */
                        if (result.dismiss === Swal.DismissReason.timer) {
                            $('body').css('pointer-events', 'unset');
                            DataPreviewForm();
                        }
                    })
                }
            })
            // Swal.fire({
            //     title: 'Selesaikan?',
            //     text: "Anda akan selesaikan registrasi ini!",
            //     icon: 'warning',
            //     showCancelButton: true,
            //     confirmButtonColor: '#3085d6',
            //     cancelButtonColor: '#d33',
            //     confirmButtonText: 'Ya, Selesaikan!'
            // }).then((result) => {
            //     if (result.value) {
            //         DataKuisioner();
            //         $('#FormRegister').removeClass('was-validated');
            //     }
            // })
        }
    } else if (Proses = -1) {
        if (Sec == 'secKuisioner5') {
            Swal.fire({
                title: 'Apakah Anda Yakin?',
                text: "Anda akan kembali kehalaman awal, data tidak akan direset!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Kembali Awal!'
            }).then((result) => {
                if (result.value) {
                    $('#' + Sec).hide();
                    $('#secRegister').fadeIn();
                }
            })
        } else {
            $('#' + Sec).hide();
            $('#secRegister').fadeIn();
        }
    }
}

const DataPreviewForm = () => {
    $('#modalPreviewData').modal('show');

    var regis = $('#FormRegister').serializeObject(),
        tmptTgl = $('#FormKetTempatTinggal').serializeObject(),
        ketSehat = $('#FormKetKesehatan').serializeObject(),
        ketAsalSklh = $('#FormKetSekolahAsal').serializeObject();

    
    $('#InfoEmailPendaftar').text(regis.Email);
    
    $('#PvLastJenisKategori').text($('select[name="IdJenisKategoriPendaftaran"] option:selected').text());
    $('#PvLastJenjangPendidikan').text($('select[name="IdUnit"] option:selected').text());
    $('#PvLastKelas').text($('select[name="IdPpdbKelas"] option:selected').text());
    $('#PvLastJenisDaftar').text($('select[name="IdJenisPendaftaran"] option:selected').text());
    $('#PvLastJalurDaftar').text($('select[name="IdJalurPendaftaran"] option:selected').text());

    $('#PvIdPpdbDaftar').text($('#NoPendaftaran').val());
    $('#PvNisn').text(regis.Nisn);
    $('#PvNikSiswa').text(regis.NikSiswa);
    $('#PvNamaPeserta').text(regis.Nama);
    $('#PvNamaPanggilan').text(regis.NamaPanggilan);
    $('#PvJenisKelamin').text(regis.KdJenisKelamin);
    $('#PvAgama').text($('select[name="IdAgama"] option:selected').text());
    $('#PvKewarganegaraan').text($('select[name="IdKewarganegaraan"] option:selected').text());
    $('#PvLastTempatLahir').text(regis.TempatLahir);
    $('#PvLastTanggalLahir').text(indoDate(regis.TanggalLahir));
    $('#PvAnakKe').text(regis.AnakKe);
    $('#PvJmlSaudaraKandung').text(regis.JumlahSodaraKandung);
    $('#PvJmlSaudaraTiri').text(regis.JumlahSodaraTiri);
    $('#PvJmlSaudaraAngkat').text(regis.JumlahSodaraAngkat);
    $('#PvBahasaDirumah').text($('select[name="IdBahasa"] option:selected').text());
    $('#PvAlamatTinggal').text(tmptTgl.AlamatTinggal);

    $('#PvTlpnDarurat').text(tmptTgl.NoDarurat);
    $('#PvJarakKeSekolah').text(tmptTgl.JarakKeSekolah);
    $('#PvTransportasi').text($('select[name="IdTransportasi"] option:selected').text());
    $('#PvWaktuTempuh').text(tmptTgl.WaktuTempuh);
    $('#PvGolonganDarah').text(ketSehat.KdGolonganDarah);
    $('#PvPenyakitDiderita').text(ketSehat.PenyakitDiderita);
    $('#PvTinggiBadan').text(ketSehat.TinggiBadan);
    $('#PvBeratBadan').text(ketSehat.BeratBadan);
    $('#PvKelainan').text(ketSehat.KelainanJasmani);
    $('#PvAsalSekolah').text(ketAsalSklh.SekolahAsal);
    $('#PvAlamatSekolah').text(ketAsalSklh.AlamatSekolahAsal);
    $('#PvNpsn').text(ketAsalSklh.NspnSekolahAsal);
    
    $('#PvNikIbu').text($('#NikIbu').val());
    $('#PvNamaIbu').text($('#NamaIbu').val());
    $('#PvTanggalLahirIbu').text($('#TanggalLahirIbu').val());
    $('#PvTempatLahirIbu').text($('#TempatLahirIbu').val());
    $('#PvAlamatIbu').text($('#AlamatIbu').val());
    $('#PvNoHpIbu').text($('#NoHandphoneIbu').val());
    $('#PvNoTlpRumahIbu').text($('#NoTelpRumahIbu').val());
    $('#PvEmailIbu').text($('#EmailIbu').val());
    $('#PvAgamaIbu').text($('#IdAgamaIbu option:selected').text());
    $('#PvKewarganegaraanIbu').text($('#IdKewarganegaraanIbu option:selected').text());
    $('#PvPendidikanTerakhirIbu').text($('#IdJenjangPendidikanIbu option:selected').text());
    $('#PvPekerjaanIbu').text($('#IdJenisPekerjaanIbu option:selected').text());
    $('#PvInstansiIbu').text($('#NamaInstansiIbu').val());
    $('#PvJabatanIbu').text($('#JabatanIbu').val());
    $('#PvStatusPernikahanIbu').text($('#IdStatusPenikahanIbu option:selected').text());
    var TanggalMeninggalIbu = ' - Meninggal Tahun : ' + $('#TanggalMeninggalIbu').val();
    var StatusHidupIbu = $('#IdStatusHidupIbu option:selected').text();
        StatusHidupIbu += + $('#IdStatusHidupIbu option:selected').val() == 0 ? TanggalMeninggalIbu : '';
    $('#PvStatusHidupIbu').text(StatusHidupIbu);

    $('#PvNikAyah').text($('#NikAyah').val());
    $('#PvNamaAyah').text($('#NamaAyah').val());
    $('#PvTanggalLahirAyah').text($('#TanggalLahirAyah').val());
    $('#PvTempatLahirAyah').text($('#TempatLahirAyah').val());
    $('#PvAlamatAyah').text($('#AlamatAyah').val());
    $('#PvNoHpAyah').text($('#NoHandphoneAyah').val());
    $('#PvNoTlpRumahAyah').text($('#NoTelpRumahAyah').val());
    $('#PvEmailAyah').text($('#EmailAyah').val());
    $('#PvAgamaAyah').text($('#IdAgamaAyah option:selected').text());
    $('#PvKewarganegaraanAyah').text($('#IdKewarganegaraanAyah option:selected').text());
    $('#PvPendidikanTerakhirAyah').text($('#IdJenjangPendidikanAyah option:selected').text());
    $('#PvPekerjaanAyah').text($('#IdJenisPekerjaanAyah option:selected').text());
    $('#PvInstansiAyah').text($('#NamaInstansiAyah').val());
    $('#PvJabatanAyah').text($('#JabatanAyah').val());
    $('#PvStatusPernikahanAyah').text($('#IdStatusPenikahanAyah option:selected').text());
    var TanggalMeninggalAyah = ' - Meninggal Tahun : ' + $('#TanggalMeninggalAyah').val();
    var StatusHidupAyah = $('#IdStatusHidupAyah option:selected').text();
        StatusHidupAyah += + $('#IdStatusHidupAyah option:selected').val() == 0 ? TanggalMeninggalAyah : '';
    $('#PvStatusHidupAyah').text(StatusHidupAyah);

    var IdTinggal = $('#IdTinggal option:selected').val();
    $('#DivPreviewWali').hide();
    if (IdTinggal == 2)
        $('#DivPreviewWali').fadeIn();
    
    $('#PvNikWali').text($('#NikWali').val());
    $('#PvNamaWali').text($('#NamaWali').val());
    $('#PvTanggalLahirWali').text($('#TanggalLahirWali').val());
    $('#PvTempatLahirWali').text($('#TempatLahirWali').val());
    $('#PvAlamatWali').text($('#AlamatWali').val());
    $('#PvNoHpWali').text($('#NoHandphoneWali').val());
    $('#PvNoTlpRumahWali').text($('#NoTelpRumahWali').val());
    $('#PvEmailWali').text($('#EmailWali').val());
    $('#PvAgamaWali').text($('#IdAgamaWali option:selected').text());
    $('#PvKewarganegaraanWali').text($('#IdKewarganegaraanWali option:selected').text());
    $('#PvPendidikanTerakhirWali').text($('#IdJenjangPendidikanWali option:selected').text());
    $('#PvPekerjaanWali').text($('#IdJenisPekerjaanWali option:selected').text());
    $('#PvInstansiWali').text($('#NamaInstansiWali').val());
    $('#PvJabatanWali').text($('#JabatanWali').val());
    $('#PvStatusPernikahanWali').text($('#IdStatusPenikahanWali option:selected').text());
    var TanggalMeninggalWali = ' - Meninggal Tahun : ' + $('#TanggalMeninggalWali').val();
    var StatusHidupWali = $('#IdStatusHidupWali option:selected').text();
        StatusHidupWali += + $('#IdStatusHidupWali option:selected').val() == 0 ? TanggalMeninggalWali : '';
    $('#PvStatusHidupWali').text(StatusHidupWali);

    // if (regis.AsalSekolah == "") {
    //     $('#trAsalSekolah').hide();
    // } else {
    //     $('#trAsalSekolah').show();
    // }
    // if (regis.Wakaf == "Y") {
    //     $('#PvWakaf').text("Ya");
    // } else {
    //     $('#PvWakaf').text("Tidak");
    // }

}

const BtnPreviewForm = () => {
    $('#modalPreviewData').modal('hide');

    Swal.fire({
        title: 'Apakah Anda Yakin?',
        text: "Jika data yang dimasukan sudah benar silahkan klik tombol lanjutkan lagi!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Lanjutkan!',
        cancelButtonText: 'Batalkan',
        allowOutsideClick: false
    }).then((result) => {
        if (result.value) {
            $('body').css('pointer-events', 'none');

            let timerInterval;
            Swal.fire({
                title: 'Tunggu Sebentar!',
                html: 'Sedang memuat data berkas anda...',
                timer: 3000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getContent()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                onClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                    $('body').css('pointer-events', 'unset');
                    DataPreviewBerkas();
                }
            })
        }
    })
}

const DataPreviewBerkas = () => {
    $('#modalPreviewBerkas').modal('show');
}

const BtnPreviewBerkas = () => {
    $('#modalPreviewBerkas').modal('hide');
    Swal.fire({
        title: 'Apakah Anda Yakin?',
        text: "Jika berkas yang anda upload sudah benar silahkan klik Lanjutkan !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Lanjutkan!',
        cancelButtonText: 'Batalkan',
        allowOutsideClick: false
    }).then((result) => {
        if (result.value) {
            $('body').css('pointer-events', 'none');

            let timerInterval;
            Swal.fire({
                title: 'Tunggu Sebentar!',
                html: 'Sedang memuat biaya pendidikan...',
                timer: 3000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getContent()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                onClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                    $('body').css('pointer-events', 'unset');
                    // DataKuisioner();
                    GetPpdbBiayaByUnit();
                }
            })
        }
    })
}

const BtnSimpanBiaya = () => {
    var RpSppAsli = parseInt($('#RpSppAsli').val());
    var RpSpp = parseInt($('#RpSpp').val());
    var RpWakafAsli = parseInt($('#RpWakafAsli').val());
    var RpWakaf = parseInt($('#RpWakaf').val());

    var isTrue = false;

    if(RpSppAsli > RpSpp) {
        isTrue = false;
    } else {
        isTrue = true;
        if(RpWakafAsli > RpWakaf) {
            isTrue = false;
        } else {
            isTrue = true;
        }
    }

    if(isTrue) {
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Jika biaya yang anda tetapkan sudah yakin silahkan klik Selesai !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Selesai!',
            cancelButtonText: 'Batalkan',
            allowOutsideClick: false
        }).then((result) => {
            if (result.value) {
                $('body').css('pointer-events', 'none');
    
                let timerInterval;
                Swal.fire({
                    title: 'Tunggu Sebentar!',
                    html: 'Data anda sedang dalam proses insert ke database kami...',
                    timer: 3000,
                    timerProgressBar: true,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                        timerInterval = setInterval(() => {
                            const content = Swal.getContent()
                            if (content) {
                                const b = content.querySelector('b')
                                if (b) {
                                    b.textContent = Swal.getTimerLeft()
                                }
                            }
                        }, 100)
                    },
                    onClose: () => {
                        clearInterval(timerInterval)
                    }
                }).then((result) => {
                    /* Read more about handling dismissals below */
                    if (result.dismiss === Swal.DismissReason.timer) {
                        $('body').css('pointer-events', 'unset');
                        DataKuisioner();
                    }
                })
            }
        })
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Gagal',
            text: 'Nominal harus diatas minimal',
        })
    }
    
}



const DataKuisioner = () => {
    var NoPanitiaPpdb = $('#NoPanitiaPpdb').val();
    var fd = new FormData($('#FormRegister')[0]);
    var objRegister = $('#FormRegister').serializeObject();
    var objTempatTinggal = $('#FormKetTempatTinggal').serializeObject();
    var objKesehatan = $('#FormKetKesehatan').serializeObject();
    var objSekolahAsal = $('#FormKetSekolahAsal').serializeObject();
    var objDataOrtu = $('#FormDataOrtu').serializeObject();
    var objRaport = $('#FormRaport').serializeObject();
    var objSaudaraKandung = $('#FormSaudaraKandung').serializeObject();
    var IdUnit = $("select[name='IdUnit'] option:selected").val();

    if ($('select[name="IdJenisPrestasi[0]"]').val() != "") {
        $('select[name^="IdJenisPrestasi"]').each(function (i, v) {
            fd.append('Prestasis[' + i + '].' + 'IdJenisPrestasi', v.value);
        });
        $('select[name^="IdJenisTingkatPrestasi"]').each(function (i, v) {
            fd.append('Prestasis[' + i + '].' + 'IdJenisTingkatPrestasi', v.value);
        });
        $('select[name^="Tahun"]').each(function (i, v) {
            fd.append('Prestasis[' + i + '].' + 'Tahun', v.value);
        });
        $('input[name^="NamaPenyelenggara"]').each(function (i, v) {
            fd.append('Prestasis[' + i + '].' + 'NamaPenyelenggara', v.value);
        });
        $('input[name^="Keterangan"]').each(function (i, v) {
            fd.append('Prestasis[' + i + '].' + 'Keterangan', v.value);
        });
        $('input[name="FilePrestasi[]"][type="file"]').each(function (i, v) {
            fd.append('FilePrestasis', $('input[type="file"][name="FilePrestasi[]"]')[i].files[0]);
        });
    }

    fd.append("Siswa.IdPpdbDaftar", $('#NoPendaftaran').val());
    fd.append("Siswa.IdSiswa", objRegister.IdSiswa);
    fd.append("Siswa.IdSiswaSaudara", objSaudaraKandung.IdSiswaSaudara);
    fd.append("Siswa.IdPpdbKelas", objRegister.IdPpdbKelas);
    fd.append("Siswa.IdJenisKategoriPendaftaran", objRegister.IdJenisKategoriPendaftaran);
    fd.append("Siswa.IdJenisPendaftaran", objRegister.IdJenisPendaftaran);
    fd.append("Siswa.IdJalurPendaftaran", objRegister.IdJalurPendaftaran);
    fd.append("Siswa.Nik", objRegister.NikSiswa);
    fd.append("Siswa.Nisn", objRegister.Nisn);
    fd.append("Siswa.Nama", objRegister.Nama);
    fd.append("Siswa.NamaPanggilan", objRegister.NamaPanggilan);
    fd.append("Siswa.TempatLahir", objRegister.TempatLahir);
    fd.append("Siswa.TanggalLahir", objRegister.TanggalLahir);
    fd.append("Siswa.IdAgama", objRegister.IdAgama);
    fd.append("Siswa.IdKewarganegaraan", objRegister.IdKewarganegaraan);
    fd.append("Siswa.KdJenisKelamin", objRegister.KdJenisKelamin);
    fd.append("Siswa.KdGolonganDarah", objKesehatan.KdGolonganDarah);
    fd.append("Siswa.AlamatTinggal", objTempatTinggal.AlamatTinggal);
    fd.append("Siswa.IdTinggal", objDataOrtu.IdTinggal);
    fd.append("Siswa.Email", objRegister.Email);
    fd.append("Siswa.NoHandphone", objRegister.NoHandphone);
    fd.append("Siswa.NoDarurat", objTempatTinggal.NoDarurat);
    fd.append("Siswa.IdBahasa", objRegister.IdBahasa);
    fd.append("Siswa.TinggiBadan", objKesehatan.TinggiBadan);
    fd.append("Siswa.BeratBadan", objKesehatan.BeratBadan);
    fd.append("Siswa.AnakKe", objRegister.AnakKe);
    fd.append("Siswa.JumlahSodaraKandung", objRegister.JumlahSodaraKandung);
    fd.append("Siswa.JumlahSodaraTiri", objRegister.JumlahSodaraTiri);
    fd.append("Siswa.JumlahSodaraAngkat", objRegister.JumlahSodaraAngkat);
    fd.append("Siswa.IdTransportasi", objTempatTinggal.IdTransportasi);
    fd.append("Siswa.JarakKeSekolah", objTempatTinggal.JarakKeSekolah);
    fd.append("Siswa.WaktuTempuh", objTempatTinggal.WaktuTempuh);
    fd.append("Siswa.PenyakitDiderita", objKesehatan.PenyakitDiderita);
    fd.append("Siswa.KelainanJasmani", objKesehatan.KelainanJasmani);
    fd.append("Siswa.SekolahAsal", objSekolahAsal.SekolahAsal);
    fd.append("Siswa.AlamatSekolahAsal", objSekolahAsal.AlamatSekolahAsal);
    fd.append("Siswa.NspnSekolahAsal", objSekolahAsal.NspnSekolahAsal);
    fd.append("Siswa.IdPpdbSeragam", objRegister.IdPpdbSeragam);

    if ($('select[name="IdTipe[0]"]').val() != "") {
        
       
        $('input[name^="IdTipe[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'IdTipe', v.value);
            if ($("select[name='TinggalDengan']").val() != "2") 
                return i < 1;
        });

        $('input[name^="Nama[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'Nama[]', v.value);
            if ($("select[name='TinggalDengan']").val() != "2") 
                return i < 1;
        });
        $('input[name^="Alamat[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'Alamat', v.value);
            console.log(i)
            if ($("select[name='TinggalDengan']").val() != "2") 
                return i < 1;
        });
        $('input[name^="Nik[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'Nik', v.value);
            if ($("select[name='TinggalDengan']").val() != "2") 
                return i < 1;
        });
        $('select[name^="IdJenisPekerjaan[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'IdJenisPekerjaan', v.value);
            if ($("select[name='TinggalDengan']").val() != "2") 
                return i < 1;
        });
        $('input[name^="NamaInstansi[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'NamaInstansi', v.value);
            if ($("select[name='TinggalDengan']").val() != "2") 
                return i < 1;
        });
        $('input[name^="Jabatan[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'Jabatan', v.value);
            if ($("select[name='TinggalDengan']").val() != "2") 
                return i < 1;
        });
        $('input[name^="Email[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'Email', v.value);
            if ($("select[name='TinggalDengan']").val() != "2") 
                return i < 1;
        });
        $('input[name^="NoHandphone[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'NoHandphone', v.value);
            if ($("select[name='TinggalDengan']").val() != "2") 
                return i < 1;
        });
        $('input[name^="NoTelpRumah[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'NoTelpRumah', v.value);
            if ($("select[name='TinggalDengan']").val() != "2") 
                return i < 1;
        });
        $('select[name^="IdAgama[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'IdAgama', v.value);
            if ($("select[name='TinggalDengan']").val() != "2") 
                return i < 1;
        });
        $('select[name^="IdKewarganegaraan[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'IdKewarganegaraan', v.value);
            if ($("select[name='TinggalDengan']").val() != "2") 
                return i < 1;
        });
        $('select[name^="IdJenjangPendidikan[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'IdJenjangPendidikan', v.value);
            if ($("select[name='TinggalDengan']").val() != "2") 
                return i < 1;
        });
        $('select[name^="IdStatusPenikahan[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'IdStatusPenikahan', v.value);
            if ($("select[name='TinggalDengan']").val() != "2") 
                return i < 1;
        });
        $('input[name^="TanggalLahir[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'TanggalLahir', v.value);
            if ($("select[name='TinggalDengan']").val() != "2") 
                return i < 1;
        });
        $('input[name^="TempatLahir[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'TempatLahir', v.value);
            if ($("select[name='TinggalDengan']").val() != "2") 
                return i < 1;
        });
        $('select[name^="IdStatusHidup[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'IdStatusHidup', v.value);
            if ($("select[name='TinggalDengan']").val() != "2") 
                return i < 1;
        });
        $('input[name^="TanggalMeninggal[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'TanggalMeninggal', v.value);
            if ($("select[name='TinggalDengan']").val() != "2") 
                return i < 1;
        });

    }

    if (IdUnit == 3) {
        fd.append('Rapors[0].Mapel', 'Matematika');
        fd.append('Rapors[1].Mapel', 'B. Indonesia');
        fd.append('Rapors[2].Mapel', 'IPA');
        fd.append('Rapors[0].Nilai', objRaport.Matematika1);
        fd.append('Rapors[1].Nilai', objRaport.Bindo1);
        fd.append('Rapors[2].Nilai', objRaport.Ipa1);
        fd.append('Rapors[0].Semester', 1);
        fd.append('Rapors[1].Semester', 1);
        fd.append('Rapors[2].Semester', 1);
        // fd.append('FileRapors[0]', $('input[type="file"][name="FileRaportS1"]')[0].files[0]);
        // fd.append('FileRapors[1]', $('input[type="file"][name="FileRaportS1"]')[0].files[0]);
        // fd.append('FileRapors[2]', $('input[type="file"][name="FileRaportS1"]')[0].files[0]);
    
        fd.append('Rapors[3].Mapel', 'Matematika');
        fd.append('Rapors[4].Mapel', 'B. Indonesia');
        fd.append('Rapors[5].Mapel', 'IPA');
        fd.append('Rapors[3].Nilai', objRaport.Matematika2);
        fd.append('Rapors[4].Nilai', objRaport.Bindo2);
        fd.append('Rapors[5].Nilai', objRaport.Ipa2);
        fd.append('Rapors[3].Semester', 2);
        fd.append('Rapors[4].Semester', 2);
        fd.append('Rapors[5].Semester', 2);
        // fd.append('FileRapors[3]', $('input[type="file"][name="FileRaportS2"]')[0].files[0]);
        // fd.append('FileRapors[4]', $('input[type="file"][name="FileRaportS2"]')[0].files[0]);
        // fd.append('FileRapors[5]', $('input[type="file"][name="FileRaportS2"]')[0].files[0]);
    
        fd.append('Rapors[6].Mapel', 'Matematika');
        fd.append('Rapors[7].Mapel', 'B. Indonesia');
        fd.append('Rapors[8].Mapel', 'IPA');
        fd.append('Rapors[6].Nilai', objRaport.Matematika3);
        fd.append('Rapors[7].Nilai', objRaport.Bindo3);
        fd.append('Rapors[8].Nilai', objRaport.Ipa3);
        fd.append('Rapors[6].Semester', 3);
        fd.append('Rapors[7].Semester', 3);
        fd.append('Rapors[8].Semester', 3);
        // fd.append('FileRapors[6]', $('input[type="file"][name="FileRaportS3"]')[0].files[0]);
        // fd.append('FileRapors[7]', $('input[type="file"][name="FileRaportS3"]')[0].files[0]);
        // fd.append('FileRapors[8]', $('input[type="file"][name="FileRaportS3"]')[0].files[0]);
    
        fd.append('Rapors[9].Mapel', 'Matematika');
        fd.append('Rapors[10].Mapel', 'B. Indonesia');
        fd.append('Rapors[11].Mapel', 'IPA');
        fd.append('Rapors[9].Nilai', objRaport.Matematika4);
        fd.append('Rapors[10].Nilai', objRaport.Bindo4);
        fd.append('Rapors[11].Nilai', objRaport.Ipa4);
        fd.append('Rapors[9].Semester', 4);
        fd.append('Rapors[10].Semester', 4);
        fd.append('Rapors[11].Semester', 4);
        // fd.append('FileRapors[9]', $('input[type="file"][name="FileRaportS4"]')[0].files[0]);
        // fd.append('FileRapors[10]', $('input[type="file"][name="FileRaportS4"]')[0].files[0]);
        // fd.append('FileRapors[11]', $('input[type="file"][name="FileRaportS4"]')[0].files[0]);
    
        // fd.append('Rapors[12].Mapel', 'Matematika');
        // fd.append('Rapors[13].Mapel', 'B. Indonesia');
        // fd.append('Rapors[14].Mapel', 'IPA');
        // fd.append('Rapors[12].Nilai', objRaport.Matematika5);
        // fd.append('Rapors[13].Nilai', objRaport.Bindo5);
        // fd.append('Rapors[14].Nilai', objRaport.Ipa5);
        // fd.append('Rapors[12].Semester', 5);
        // fd.append('Rapors[13].Semester', 5);
        // fd.append('Rapors[14].Semester', 5);
        // fd.append('FileRapors[12]', $('input[type="file"][name="FileRaportS5"]')[0].files[0]);
        // fd.append('FileRapors[13]', $('input[type="file"][name="FileRaportS5"]')[0].files[0]);
        // fd.append('FileRapors[14]', $('input[type="file"][name="FileRaportS5"]')[0].files[0]);

        fd.append('FileRapors', $('input[type="file"][name="FileRaportS1"]')[0].files[0]);
        fd.append('FileRapors', $('input[type="file"][name="FileRaportS2"]')[0].files[0]);
        fd.append('FileRapors', $('input[type="file"][name="FileRaportS3"]')[0].files[0]);
        fd.append('FileRapors', $('input[type="file"][name="FileRaportS4"]')[0].files[0]);
        // fd.append('FileRapors', $('input[type="file"][name="FileRaportS5"]')[0].files[0]);
    } else if (IdUnit == 4) {
        fd.append('Rapors[0].Mapel', 'Matematika');
        fd.append('Rapors[1].Mapel', 'B. Indonesia');
        fd.append('Rapors[2].Mapel', 'IPA');
        fd.append('Rapors[3].Mapel', 'B. Inggris');
        fd.append('Rapors[4].Mapel', 'IPS');
        fd.append('Rapors[0].Nilai', objRaport.Matematika1);
        fd.append('Rapors[1].Nilai', objRaport.Bindo1);
        fd.append('Rapors[2].Nilai', objRaport.Ipa1);
        fd.append('Rapors[3].Nilai', objRaport.Binggris1);
        fd.append('Rapors[4].Nilai', objRaport.Ips1);
        fd.append('Rapors[0].Semester', 1);
        fd.append('Rapors[1].Semester', 1);
        fd.append('Rapors[2].Semester', 1);
        fd.append('Rapors[3].Semester', 1);
        fd.append('Rapors[4].Semester', 1);
        // fd.append('FileRapors[0]', $('input[type="file"][name="FileRaportS1"]')[0].files[0]);
        // fd.append('FileRapors[1]', $('input[type="file"][name="FileRaportS1"]')[0].files[0]);
        // fd.append('FileRapors[2]', $('input[type="file"][name="FileRaportS1"]')[0].files[0]);
        // fd.append('FileRapors[3]', $('input[type="file"][name="FileRaportS1"]')[0].files[0]);
        // fd.append('FileRapors[4]', $('input[type="file"][name="FileRaportS1"]')[0].files[0]);
    
        fd.append('Rapors[5].Mapel', 'Matematika');
        fd.append('Rapors[6].Mapel', 'B. Indonesia');
        fd.append('Rapors[7].Mapel', 'IPA');
        fd.append('Rapors[8].Mapel', 'B. Inggris');
        fd.append('Rapors[9].Mapel', 'IPS');
        fd.append('Rapors[5].Nilai', objRaport.Matematika2);
        fd.append('Rapors[6].Nilai', objRaport.Bindo2);
        fd.append('Rapors[7].Nilai', objRaport.Ipa2);
        fd.append('Rapors[8].Nilai', objRaport.Binggris2);
        fd.append('Rapors[9].Nilai', objRaport.Ips2);
        fd.append('Rapors[5].Semester', 2);
        fd.append('Rapors[6].Semester', 2);
        fd.append('Rapors[7].Semester', 2);
        fd.append('Rapors[8].Semester', 2);
        fd.append('Rapors[9].Semester', 2);
        // fd.append('FileRapors[5]', $('input[type="file"][name="FileRaportS2"]')[0].files[0]);
        // fd.append('FileRapors[6]', $('input[type="file"][name="FileRaportS2"]')[0].files[0]);
        // fd.append('FileRapors[7]', $('input[type="file"][name="FileRaportS2"]')[0].files[0]);
        // fd.append('FileRapors[8]', $('input[type="file"][name="FileRaportS2"]')[0].files[0]);
        // fd.append('FileRapors[9]', $('input[type="file"][name="FileRaportS2"]')[0].files[0]);
    
        fd.append('Rapors[10].Mapel', 'Matematika');
        fd.append('Rapors[11].Mapel', 'B. Indonesia');
        fd.append('Rapors[12].Mapel', 'IPA');
        fd.append('Rapors[13].Mapel', 'B. Inggris');
        fd.append('Rapors[14].Mapel', 'IPS');
        fd.append('Rapors[10].Nilai', objRaport.Matematika3);
        fd.append('Rapors[11].Nilai', objRaport.Bindo3);
        fd.append('Rapors[12].Nilai', objRaport.Ipa3);
        fd.append('Rapors[13].Nilai', objRaport.Binggris3);
        fd.append('Rapors[14].Nilai', objRaport.Ips3);
        fd.append('Rapors[10].Semester', 3);
        fd.append('Rapors[11].Semester', 3);
        fd.append('Rapors[12].Semester', 3);
        fd.append('Rapors[13].Semester', 3);
        fd.append('Rapors[14].Semester', 3);
        // fd.append('FileRapors[10]', $('input[type="file"][name="FileRaportS3"]')[0].files[0]);
        // fd.append('FileRapors[11]', $('input[type="file"][name="FileRaportS3"]')[0].files[0]);
        // fd.append('FileRapors[12]', $('input[type="file"][name="FileRaportS3"]')[0].files[0]);
        // fd.append('FileRapors[13]', $('input[type="file"][name="FileRaportS3"]')[0].files[0]);
        // fd.append('FileRapors[14]', $('input[type="file"][name="FileRaportS3"]')[0].files[0]);
    
        fd.append('Rapors[15].Mapel', 'Matematika');
        fd.append('Rapors[16].Mapel', 'B. Indonesia');
        fd.append('Rapors[17].Mapel', 'IPA');
        fd.append('Rapors[18].Mapel', 'B. Inggris');
        fd.append('Rapors[19].Mapel', 'IPS');
        fd.append('Rapors[15].Nilai', objRaport.Matematika4);
        fd.append('Rapors[16].Nilai', objRaport.Bindo4);
        fd.append('Rapors[17].Nilai', objRaport.Ipa4);
        fd.append('Rapors[18].Nilai', objRaport.Binggris4);
        fd.append('Rapors[19].Nilai', objRaport.Ips4);
        fd.append('Rapors[15].Semester', 4);
        fd.append('Rapors[16].Semester', 4);
        fd.append('Rapors[17].Semester', 4);
        fd.append('Rapors[18].Semester', 4);
        fd.append('Rapors[19].Semester', 4);
        // fd.append('FileRapors[15]', $('input[type="file"][name="FileRaportS4"]')[0].files[0]);
        // fd.append('FileRapors[16]', $('input[type="file"][name="FileRaportS4"]')[0].files[0]);
        // fd.append('FileRapors[17]', $('input[type="file"][name="FileRaportS4"]')[0].files[0]);
        // fd.append('FileRapors[18]', $('input[type="file"][name="FileRaportS4"]')[0].files[0]);
        // fd.append('FileRapors[19]', $('input[type="file"][name="FileRaportS4"]')[0].files[0]);
    
        // fd.append('Rapors[20].Mapel', 'Matematika');
        // fd.append('Rapors[21].Mapel', 'B. Indonesia');
        // fd.append('Rapors[22].Mapel', 'IPA');
        // fd.append('Rapors[23].Mapel', 'B. Inggris');
        // fd.append('Rapors[24].Mapel', 'IPS');
        // fd.append('Rapors[20].Nilai', objRaport.Matematika5);
        // fd.append('Rapors[21].Nilai', objRaport.Bindo5);
        // fd.append('Rapors[22].Nilai', objRaport.Ipa5);
        // fd.append('Rapors[23].Nilai', objRaport.Binggris5);
        // fd.append('Rapors[24].Nilai', objRaport.Ips5);
        // fd.append('Rapors[20].Semester', 5);
        // fd.append('Rapors[21].Semester', 5);
        // fd.append('Rapors[22].Semester', 5);
        // fd.append('Rapors[23].Semester', 5);
        // fd.append('Rapors[24].Semester', 5);
        // fd.append('FileRapors[20]', $('input[type="file"][name="FileRaportS5"]')[0].files[0]);
        // fd.append('FileRapors[21]', $('input[type="file"][name="FileRaportS5"]')[0].files[0]);
        // fd.append('FileRapors[22]', $('input[type="file"][name="FileRaportS5"]')[0].files[0]);
        // fd.append('FileRapors[23]', $('input[type="file"][name="FileRaportS5"]')[0].files[0]);
        // fd.append('FileRapors[24]', $('input[type="file"][name="FileRaportS5"]')[0].files[0]);

        fd.append('FileRapors', $('input[type="file"][name="FileRaportS1"]')[0].files[0]);
        fd.append('FileRapors', $('input[type="file"][name="FileRaportS2"]')[0].files[0]);
        fd.append('FileRapors', $('input[type="file"][name="FileRaportS3"]')[0].files[0]);
        fd.append('FileRapors', $('input[type="file"][name="FileRaportS4"]')[0].files[0]);
        // fd.append('FileRapors', $('input[type="file"][name="FileRaportS5"]')[0].files[0]);

    }

    fd.append('FileFoto', $('input[type="file"][name="FileFoto"]')[0].files[0]);
    fd.append('FileKtpIbu', $('input[type="file"][name="FileKtpIbu"]')[0].files[0]);
    fd.append('FileKtpAyah', $('input[type="file"][name="FileKtpAyah"]')[0].files[0]);
    fd.append('FileKK', $('input[type="file"][name="FileKK"]')[0].files[0]);
    fd.append('FileAkte', $('input[type="file"][name="FileAkte"]')[0].files[0]);
    fd.append('FileSuratKesehatan', $('input[type="file"][name="FileSuratKesehatan"]')[0].files[0]);
    fd.append('FileBebasNarkoba', $('input[type="file"][name="FileBebasNarkoba"]')[0].files[0]);
    
    $('input[name^="IdPertanyaanKuisioner"]').each(function (i, v) {
        fd.append('Kuisioners[' + i + '].' + 'IdPpdbKuisionerPertanyaan', v.value);
    });
    $('input[name^="OpsiJawaban"]:checked').each(function (i, v) {
        fd.append('Kuisioners[' + i + '].' + 'OpsiJawaban', v.value);
    });
    $('input[name^="Jawaban"]').each(function (i, v) {
        
        fd.append('Kuisioners[' + i + '].' + 'Jawaban', v.value);
    });

    $('input[name^="IdJenisBiayaPpdb[]"]').each(function (i, v) {
        fd.append('Biayas[' + i + '].' + 'IdJenisBiayaPpdb', v.value);
    });

    $('input[name^="Rp[]"]').each(function (i, v) {
        fd.append('Biayas[' + i + '].' + 'Rp', v.value);
    });

    // var AnakYatim = $("select[name='AnakYatim']").val();
    // if (AnakYatim == "Yatim" || AnakYatim == "Yatim Piatu") {
    //     fd.append('YatimPiatu', AnakYatim+'~'+$('#NamaWali').val());
    // } else {
    //     fd.append('YatimPiatu', AnakYatim);
    // }

    // var IdTinggal = $("select[name='IdTinggal']").val();
    // if (IdTinggal == "Lainnya") {
    //     fd.append('IdTinggal', $('#IdTinggalLainnya').val());
    // } else {
    //     fd.append('IdTinggal', IdTinggal);
    // }
    // var AlatTransportasi = $("select[name='AlatTransportasi']").val();
    // if (AlatTransportasi == "Lainnya") {
    //     fd.append('AlatTransportasi', $('#AlatTransportasiLainnya').val());
    // } else {
    //     fd.append('AlatTransportasi', AlatTransportasi);
    // }
    // var PenyakitYangPernahDiderita = $("select[name='PenyakitYangPernahDiderita']").val();
    // if (PenyakitYangPernahDiderita == "Lainnya") {
    //     fd.append('PenyakitYangPernahDiderita', $('#PenyakitYangPernahDideritaLainnya').val());
    // } else {
    //     fd.append('PenyakitYangPernahDiderita', PenyakitYangPernahDiderita);
    // }

    

    // fd.append('IdSiswaSaudara', parseInt($('input#IdSiswaSaudara', '#FormSaudaraKandung').val()));

    // fd.append('NikIbu', objDataOrtu.NikIbu);
    // fd.append('NamaIbu', objDataOrtu.NamaIbu);
    // fd.append('TahunLahirIbu', objDataOrtu.TahunLahirIbu);
    // fd.append('IdAgamaIbu', objDataOrtu.IdAgamaIbu);
    // fd.append('KewarganegaraanIbu', objDataOrtu.KewarganegaraanIbu);
    // fd.append('PendidikanTerakhirIbu', objDataOrtu.PendidikanTerakhirIbu);
    // fd.append('IdJenisPekerjaanIbu', objDataOrtu.IdJenisPekerjaanIbu);
    // fd.append('NamaInstansiIbu', objDataOrtu.NamaInstansiIbu);
    
    // fd.append('NikAyah', objDataOrtu.NikAyah);
    // fd.append('NamaAyah', objDataOrtu.NamaAyah);
    // fd.append('TahunLahirAyah', objDataOrtu.TahunLahirAyah);
    // fd.append('IdAgamaAyah', objDataOrtu.IdAgamaAyah);
    // fd.append('PendidikanTerakhirAyah', objDataOrtu.PendidikanTerakhirAyah);
    // fd.append('KewarganegaraanAyah', objDataOrtu.KewarganegaraanAyah);
    // fd.append('IdJenisPekerjaanAyah', objDataOrtu.IdJenisPekerjaanAyah);
    // fd.append('NamaInstansiAyah', objDataOrtu.NamaInstansiAyah);

    // fd.append('AlamatOrtu', objDataOrtu.AlamatOrtu);
    // fd.append('NoHandphoneOrtu', objDataOrtu.NoHandphoneOrtu);
    // fd.append('EmailOrtu', objDataOrtu.EmailOrtu);
    // fd.append('OrtuMeninggalDuniaTahun', objDataOrtu.OrtuMeninggalDuniaTahun);

    // fd.append('AlamatTinggal', objTempatTinggal.AlamatTinggal);
    // fd.append('Kelurahan', objTempatTinggal.Kelurahan);
    // fd.append('Kecamatan', objTempatTinggal.Kecamatan);
    // fd.append('Kota', objTempatTinggal.Kota);
    // fd.append('KodePos', objTempatTinggal.KodePos);
    // fd.append('NoTlpRumah', objTempatTinggal.NoTlpRumah);
    // fd.append('NoTlpnDarurat', objTempatTinggal.NoTlpnDarurat);
    // fd.append('TinggalDengan', objTempatTinggal.TinggalDengan);
    // fd.append('JarakTempatTinggalKeAttaufiq', objTempatTinggal.JarakTempatTinggalKeAttaufiq);
    // fd.append('AlatTransportasi', objTempatTinggal.AlatTransportasi);
    // fd.append('WaktuTempuhKeSekolah', objTempatTinggal.WaktuTempuhKeSekolah);

    // fd.append('GolonganDarah', objKesehatan.GolonganDarah);
    // // fd.append('PenyakitYangPernahDiderita', objKesehatan.PenyakitYangPernahDiderita);
    // fd.append('KeadaanHariIni', objKesehatan.KeadaanHariIni);
    // fd.append('TinggiBadan', objKesehatan.TinggiBadan);
    // fd.append('BeratBadan', objKesehatan.BeratBadan);
    // fd.append('KelainanFisik', objKesehatan.KelainanFisik);
    
    // fd.append('AsalSekolah', objSekolahAsal.AsalSekolah);
    // fd.append('AlamatSekolahAsal', objSekolahAsal.AlamatSekolahAsal);
    // fd.append('NPSN', objSekolahAsal.NPSN);
    // fd.append('AlasanPindah', objSekolahAsal.AlasanPindah);

    
    $.ajax({
        "url": base_url + '/api/webs/InputFormulir',
        "method": "POST",
        "timeout": 0,
        "processData": false,
        "contentType": false,
        "data": fd,
        "dataType": "JSON",
        "beforeSend": () => {
            ProgressBar("wait");
        },
        "success": (res) => {
            ProgressBar("success");
            if (res.IsSuccess) {
                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil',
                    text: 'Untuk informasi selanjutnya, silahkan cek email anda yang terdaftar',
                }).then(function () {
                    window.location.href = `${base_url}/ppdb`
                });
            } else if (!res.IsSuccess) {
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    footer: 'silahkan hubungi &nbsp;<a href="http://wa.me/' + NoPanitiaPpdb + '">  panitia ppdb </a>'
                })

            }
        },
        "error": (err, a, e) => {
            ProgressBar("success");
            Swal.fire({
                icon: 'error',
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(err),
                footer: 'silahkan hubungi &nbsp;<a href="http://wa.me/' + NoPanitiaPpdb + '">  panitia ppdb </a>'
            })
        }
    })
}

const DataKuisioner1 = () => {
    var NoPanitiaPpdb = $('#NoPanitiaPpdb').val();
    var IdUnit = $("select[name='IdUnit'] option:selected").val();
    var IdJenisPendaftaran = $("select[name='IdJenisPendaftaran'] option:selected").val();
    var IdJalurPendaftaran = $("select[name='IdJalurPendaftaran'] option:selected").val();
    var Redirect = `${base_url}/ppdb/formulir`;
    var res = initAjax(`${base_url}/api/webs/GetPertanyaanKuisioner?IdUnit=${IdUnit}&IdJenisPendaftaran=${IdJenisPendaftaran}&IdJalurPendaftaran=${IdJalurPendaftaran}&IdJenisPertanyaan=1`,
                        Redirect);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        var html = '';
        $.each(res.Data, (i, v) => {
            html += '<div class="form-group">' +
                '    <label for="Kuisioner1" kuis="' + v.Pertanyaan + '"><b>' + (i + 1) + '. </b>' + v.Pertanyaan +
                '    <input type="hidden" name="IdPertanyaanKuisioner[' + i + ']" value="' + v.IdPpdbKuisioner + '"/>' +
                '    <input type="hidden" name="Pertanyaan[' + i + ']" value="' + v.Pertanyaan + '"/>' +
                '    <input type="hidden" name="Jawaban[' + i + ']" value=""/>' +
                '    <br />' +
                '    <b>Jawab: &nbsp;</b>' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[' + i + ']" id="OpsiJawaban' + i + '-1" value="Ya" required>' +
                '        <label class="form-check-label" for="OpsiJawaban' + i + '-1">Ya</label>' +
                '    </div>' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[' + i + ']" id="OpsiJawaban' + i + '-2" value="Tidak" required>' +
                '        <label class="form-check-label" for="OpsiJawaban' + i + '-2">Tidak</label>' +
                '    </div>' +
                '    <div class="invalid-feedback">More example invalid feedback text</div>' +
                '</div>';
        });
        $('div#formKuisioner1').html(html);
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Gagal',
            text: res.ReturnMessage,
            footer: 'silahkan hubungi &nbsp;<a href="http://wa.me/' + NoPanitiaPpdb + '">  panitia ppdb </a>'
        }).then(function () {
            window.location.href = Redirect;
        });
    }
}

const DataKuisioner2 = () => {
    var NoPanitiaPpdb = $('#NoPanitiaPpdb').val();
    var IdUnit = $("select[name='IdUnit'] option:selected").val();
    var IdJenisPendaftaran = $("select[name='IdJenisPendaftaran'] option:selected").val();
    var IdJalurPendaftaran = $("select[name='IdJalurPendaftaran'] option:selected").val();
    var Redirect = `${base_url}/ppdb/formulir`;
    var res = initAjax(`${base_url}/api/webs/GetPertanyaanKuisioner?IdUnit=${IdUnit}&IdJenisPendaftaran=${IdJenisPendaftaran}&IdJalurPendaftaran=${IdJalurPendaftaran}&IdJenisPertanyaan=2`,
                        Redirect);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        var html = '';
        $.each(res.Data, (i, v) => {
            html += '<div class="form-group">' +
                '    <label for="Kuisioner2" kuis="' + v.Pertanyaan + '"><b>' + (i + 1) + '. </b>' + v.Pertanyaan +
                '    <input type="hidden" name="IdPertanyaanKuisioner[2' + i + ']" value="' + v.IdPpdbKuisioner + '"/>' +
                '    <input type="hidden" name="Pertanyaan[2' + i + ']" value="' + v.Pertanyaan + '"/>' +
                '    <input type="hidden" name="Jawaban[2' + i + ']" value=""/>' +
                '    <br />' +
                '    <b>Jawab: &nbsp;</b>' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[2' + i + ']" id="OpsiJawaban2' + i + '-1" value="Selalu" required>' +
                '        <label class="form-check-label" for="OpsiJawaban2' + i + '-1">Selalu</label>' +
                '    </div>' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[2' + i + ']" id="OpsiJawaban2' + i + '-2" value="Sering" required>' +
                '        <label class="form-check-label" for="OpsiJawaban2' + i + '-2">Sering</label>' +
                '    </div>' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[2' + i + ']" id="OpsiJawaban2' + i + '-3" value="Jarang" required>' +
                '        <label class="form-check-label" for="OpsiJawaban2' + i + '-3">Jarang</label>' +
                '    </div>' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[2' + i + ']" id="OpsiJawaban2' + i + '-4" value="Tidak Pernah" required>' +
                '        <label class="form-check-label" for="OpsiJawaban2' + i + '-4">Tidak Pernah</label>' +
                '    </div>' +
                '</div>';
        });
        $('div#formKuisioner2').html(html);
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Gagal',
            text: res.ReturnMessage,
            footer: 'silahkan hubungi &nbsp;<a href="http://wa.me/' + NoPanitiaPpdb + '">  panitia ppdb </a>'
        }).then(function () {
            window.location.href = Redirect;
        });
    }
}

const DataKuisioner3 = () => {
    var NoPanitiaPpdb = $('#NoPanitiaPpdb').val();
    var IdUnit = $("select[name='IdUnit'] option:selected").val();
    var IdJenisPendaftaran = $("select[name='IdJenisPendaftaran'] option:selected").val();
    var IdJalurPendaftaran = $("select[name='IdJalurPendaftaran'] option:selected").val();
    var Redirect = `${base_url}/ppdb/formulir`;
    var res = initAjax(`${base_url}/api/webs/GetPertanyaanKuisioner?IdUnit=${IdUnit}&IdJenisPendaftaran=${IdJenisPendaftaran}&IdJalurPendaftaran=${IdJalurPendaftaran}&IdJenisPertanyaan=3`,
                        Redirect);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        var html = '';
        $.each(res.Data, (i, v) => {
            html += '<div class="form-group">' +
                '    <label for="Kuisioner3" kuis="' + v.Pertanyaan + '"><b>' + (i + 1) + '. </b>' + v.Pertanyaan +
                '    <input type="hidden" name="IdPertanyaanKuisioner[3' + i + ']" value="' + v.IdPpdbKuisioner + '"/>' +
                '    <input type="hidden" name="Pertanyaan[3' + i + ']" value="' + v.Pertanyaan + '"/>' +
                '    <br />' +
                '    <b>Jawab: &nbsp;</b>' +
                '    <br />' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[3' + i + ']" id="OpsiJawaban3' + i + '-1" value="Ayah dan Ibu" onchange="Lainnya(this,'+i+');" required>' +
                '        <label class="form-check-label" for="OpsiJawaban3' + i + '-1">Ayah dan Ibu</label>' +
                '    </div>' +
                '    <br />' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[3' + i + ']" id="OpsiJawaban3' + i + '-2" value="Ayah/Ibu saja (Faktor Pekerjaan)" onchange="Lainnya(this,'+i+');" required>' +
                '        <label class="form-check-label" for="OpsiJawaban3' + i + '-2">Ayah/Ibu saja (Faktor Pekerjaan)</label>' +
                '    </div>' +
                '    <br />' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[3' + i + ']" id="OpsiJawaban3' + i + '-3" value="Ayah/Ibu saja (Faktor Perceraian)" onchange="Lainnya(this,'+i+');" required>' +
                '        <label class="form-check-label" for="OpsiJawaban3' + i + '-3">Ayah/Ibu saja (Faktor Perceraian)</label>' +
                '    </div>' +
                '    <br />' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[3' + i + ']" id="OpsiJawaban3' + i + '-4" value="Keluarga selain ayah dan ibu kandung (kakek-nenek/ paman-bibi)" onchange="Lainnya(this,'+i+');" required>' +
                '        <label class="form-check-label" for="OpsiJawaban3' + i + '-4">Keluarga selain ayah dan ibu kandung (kakek-nenek/ paman-bibi)</label>' +
                '    </div>' +
                '    <br />' +
                '    <div class="form-check form-check-inline">' +
                '        <input class="form-check-input" type="radio" name="OpsiJawaban[3' + i + ']" id="OpsiJawaban3' + i + '-5" value="Lainnya" onchange="Lainnya(this,'+i+');" data-input="Lainnya" required>' +
                '        <label class="form-check-label" for="OpsiJawaban3' + i + '-5">Lainnya *Sebutkan</label> <br />' +
                '        <div id="LainnyaField'+i+'" style="display:none;"> <input id="Jawaban3' + i + '" type="text" name="Jawaban[3' + i + ']" class="form-control" placeholder="Silahkan jawab pertanyaan ini" label="' + v.Pertanyaan +'" required> </div>' +
                '    </div>' +
                '</div>';
        });
        $('div#formKuisioner3').html(html);
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Gagal',
            text: res.ReturnMessage,
            footer: 'silahkan hubungi &nbsp;<a href="http://wa.me/' + NoPanitiaPpdb + '">  panitia ppdb </a>'
        }).then(function () {
            window.location.href = Redirect;
        });
    }
}

const DataKuisioner4 = () => {
    var NoPanitiaPpdb = $('#NoPanitiaPpdb').val();
    var IdUnit = $("select[name='IdUnit'] option:selected").val();
    var IdJenisPendaftaran = $("select[name='IdJenisPendaftaran'] option:selected").val();
    var IdJalurPendaftaran = $("select[name='IdJalurPendaftaran'] option:selected").val();
    var Redirect = `${base_url}/ppdb/formulir`;
    var res = initAjax(`${base_url}/api/webs/GetPertanyaanKuisioner?IdUnit=${IdUnit}&IdJenisPendaftaran=${IdJenisPendaftaran}&IdJalurPendaftaran=${IdJalurPendaftaran}&IdJenisPertanyaan=4`,
                        Redirect);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        var html = '';
        $.each(res.Data, (i, v) => {
            html += '<div class="form-group">' +
                '    <label for="Kuisioner4" kuis="' + v.Pertanyaan + '"><b>' + (i + 1) + '. </b>' + v.Pertanyaan +
                '    <input type="hidden" name="IdPertanyaanKuisioner[4' + i + ']" value="' + v.IdPpdbKuisioner + '"/>' +
                '    <input type="hidden" name="Pertanyaan[4' + i + ']" value="' + v.Pertanyaan + '"/>' +
                '    <input type="checkbox" style="display:none" name="OpsiJawaban[4' + i + ']" id="OpsiJawaban4' + i + '-1" value="-" checked/>' +
                '    <br />' +
                '    <b>Jawab: &nbsp;</b>' +
                '    <input id="Jawaban4' + i + '" type="text" name="Jawaban[4' + i + ']" class="form-control" placeholder="Silahkan jawab pertanyaan ini" required>' +
                '</div>';
        });
        $('div#formKuisioner4').html(html);
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Gagal',
            text: res.ReturnMessage,
            footer: 'silahkan hubungi &nbsp;<a href="http://wa.me/' + NoPanitiaPpdb + '">  panitia ppdb </a>'
        }).then(function () {
            window.location.href = Redirect;
        });
    }
}

const DataKuisioner5 = () => {
    var NoPanitiaPpdb = $('#NoPanitiaPpdb').val();
    var IdUnit = $("select[name='IdUnit'] option:selected").val();
    var IdJenisPendaftaran = $("select[name='IdJenisPendaftaran'] option:selected").val();
    var IdJalurPendaftaran = $("select[name='IdJalurPendaftaran'] option:selected").val();
    var Redirect = `${base_url}/ppdb/formulir`;
    var res = initAjax(`${base_url}/api/webs/GetPertanyaanKuisioner?IdUnit=${IdUnit}&IdJenisPendaftaran=${IdJenisPendaftaran}&IdJalurPendaftaran=${IdJalurPendaftaran}&IdJenisPertanyaan=5`,
                        Redirect);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        var html = '';
        $.each(res.Data, (i, v) => {
            html += `<div class="form-group">
                        <label for="Kuisioner5" kuis="${i+1}"><b>${i + 1}. </b>${v.Pertanyaan}
                        <input type="hidden" name="IdPertanyaanKuisioner[${i}]" value="${v.IdPpdbKuisioner}"/>
                        <input type="hidden" name="Jawaban[${i}]" value=""/>
                        <br />
                        <b>Jawab: &nbsp;</b>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="OpsiJawaban[5${i}]" id="OpsiJawaban5${i}-1" value="Ya" required>
                            <label class="form-check-label" for="OpsiJawaban5${i}-1">Ya</label>
                        </div>
                        <div class="invalid-feedback">More example invalid feedback text</div>
                    </div>`;
        });
        $('div#formKuisioner5').html(html);
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Gagal',
            text: res.ReturnMessage,
            footer: 'silahkan hubungi &nbsp;<a href="http://wa.me/' + NoPanitiaPpdb + '">  panitia ppdb </a>'
        }).then(function () {
            window.location.href = Redirect;
        });
    }
}

const ValidateFile = (Type) => {
    if (Type == 'CalonSiswa') {
        let file = document.querySelector("#FileFoto");
        if (/\.(jpe?g|png)$/i.test(file.files[0].name) === false) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Format File Salah!'
            })
            $('input#FileFoto').val('');
        }
    } else if (Type == 'FilePrestasi') {
        let file = document.querySelector("#FilePrestasi");
        if (/\.(zip|pdf)$/i.test(file.files[0].name) === false) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Format File Salah!'
            })
            $('input#FilePrestasi').val('');
        }
    } else if (Type == 'FileFoto') {
        let file = document.querySelector("#FileFoto");
        if (/\.(jpe?g|png)$/i.test(file.files[0].name) === false) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Format File Salah!'
            })
            $('input#FileFoto').val('');
            $('#infoFileFoto').hide();
        }
    } else if (Type == 'FileKtpIbu') {
        let file = document.querySelector("#FileKtpIbu");
        if (/\.(jpe?g|png)$/i.test(file.files[0].name) === false) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Format File Salah!'
            })
            $('input#FileKtpIbu').val('');
            $('#infoFileKtpIbu').hide();
        }
    } else if (Type == 'FileKtpAyah') {
        let file = document.querySelector("#FileKtpAyah");
        if (/\.(jpe?g|png)$/i.test(file.files[0].name) === false) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Format File Salah!'
            })
            $('input#FileKtpAyah').val('');
            $('#infoFileKtpAyah').hide();
        }
    } else if (Type == 'FileKK') {
        let file = document.querySelector("#FileKK");
        if (/\.(jpe?g|png)$/i.test(file.files[0].name) === false) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Format File Salah!'
            })
            $('input#FileKK').val('');
            $('#infoFileKK').hide();
        }
    } else if (Type == 'FileAkte') {
        let file = document.querySelector("#FileAkte");
        if (/\.(jpe?g|png)$/i.test(file.files[0].name) === false) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Format File Salah!'
            })
            $('input#FileAkte').val('');
            $('#infoFileAkte').hide();
        }
    } else if (Type == 'FileSuratKesehatan') {
        let file = document.querySelector("#FileSuratKesehatan");
        if (/\.(jpe?g|png)$/i.test(file.files[0].name) === false) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Format File Salah!'
            })
            $('input#FileSuratKesehatan').val('');
            $('#infoFileSuratKesehatan').hide();
        }
    } else if (Type == 'FileBebasNarkoba') {
        let file = document.querySelector("#FileBebasNarkoba");
        if (/\.(jpe?g|png)$/i.test(file.files[0].name) === false) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Format File Salah!'
            })
            $('input#FileBebasNarkoba').val('');
            $('#infoFileBebasNarkoba').hide();
        }
    } else if (Type == 'FileRaport') {
        let file = document.querySelector("#FileRaport");
        if (/\.(jpe?g|png)$/i.test(file.files[0].name) === false) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Format File Salah!'
            })
            $('input#FileRaport').val('');
            $('#infoFileRaport').hide();
        }
    }
}

const scrollToBottom = (id) => {
    // div_height = $("#" + id).height();
    // div_offset = $("#" + id).offset().top;
    // window_height = $(window).height();
    // $('html,body').animate({
    //     scrollTop: div_offset - window_height + div_height
    // }, 'slow');
    $('html, body').animate({
        scrollTop: $(`#${id}`).offset().top
    }, 0)
}

const Lainnya = (val, i) => {
    if (val.value == "Lainnya") {
        $('div#LainnyaField' + i).fadeIn();
        $('input#Jawaban3' + i).attr({
            'data-input': 'wajib_lainnya',
            'required': true,
        });
    } else {
        $('div#LainnyaField' + i).fadeOut();
        $('input#Jawaban3' + i).val('');
        $('input#Jawaban3' + i).removeAttr('data-input', 'required');
    }
}

const isNumber = (evt) => {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

const readURL = (input, id) => {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var idimg = '#img' + id;
            $(idimg).attr('src', e.target.result);

            var previewimg = '#Pv' + id;
            $(previewimg).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]); // convert to base64 string
        var idinfo = '#info' + id;
        $(idinfo).fadeIn();
    }
}

const GetSeragam = (IdUnit) => {
    $.getJSON(base_url + "/api/webs/GetPpdbSeragamByUnit?IdUnit=" + IdUnit, (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                html += '<option value="' + v.IdPpdbSeragam + '">' + v.Nama + ' - ' + v.NamaSingkat + '</option>';
            });
        } else console.log(res)
        $('select[name="IdPpdbSeragam"]').html(html);
    });
}

$("#FileFoto").change(function () {
    readURL(this, 'FileFoto');
});
$("#FileKtpIbu").change(function () {
    readURL(this, 'FileKtpIbu');
});
$("#FileKtpAyah").change(function () {
    readURL(this, 'FileKtpAyah');
});
$("#FileKK").change(function () {
    readURL(this, 'FileKK');
});
$("#FileAkte").change(function () {
    readURL(this, 'FileAkte');
});
$("#FileSuratKesehatan").change(function () {
    readURL(this, 'FileSuratKesehatan');
});
$("#FileBebasNarkoba").change(function () {
    readURL(this, 'FileBebasNarkoba');
});
// $("#FileRaport").change(function () {
//     readURL(this, 'FileRaport');
// });
$("#FileRaportS1").change(function () {
    readURL(this, 'FileRaportS1');
});
$("#FileRaportS2").change(function () {
    readURL(this, 'FileRaportS2');
});
$("#FileRaportS3").change(function () {
    readURL(this, 'FileRaportS3');
});
$("#FileRaportS4").change(function () {
    readURL(this, 'FileRaportS4');
});
$("#FileRaportS5").change(function () {
    readURL(this, 'FileRaportS5');
});

const CreateFormPrestasisx = () => {
    i++
    let html = "";
    html += '<hr /><div class="form-row">' +
        '    <div class="form-group col-md-4">' +
        '        <label for="">Prestasi</label>' +
        '        <select class="form-control" name="IdJenisPrestasi[' + i + ']" id="validationCustom01" data-input="wajib-prestasis" required>' +
        '        </select>' +
        '        <div class="invalid-feedback">' +
        '            Kolom Wajib diisi.' +
        '        </div>' +
        '    </div>' +
        '    <div class="form-group col-md-4">' +
        '        <label for="">Tingkat Prestasi</label>' +
        '        <select class="form-control" name="IdJenisTingkatPrestasi[' + i + ']" id="validationCustom02" data-input="wajib-prestasis" required>' +
        '            <option value="">Select One</option>' +
        '        </select>' +
        '        <div class="invalid-feedback">' +
        '            Kolom Wajib diisi.' +
        '        </div>' +
        '    </div>' +
        '    <div class="form-group col-md-4">' +
        '        <label for="">Tahun</label>' +
        '        <select class="form-control" name="Tahun[' + i + ']" id="validationCustom03" data-input="wajib-prestasis" required>' +
        '            <option value="">Select One</option>' +
        '        </select>' +
        '        <div class="invalid-feedback">' +
        '            Kolom Wajib diisi.' +
        '        </div>' +
        '    </div>' +
        '    <div class="form-group col-md-6">' +
        '        <label for="">Nama Penyelenggara</label>' +
        '        <input type="text" class="form-control" name="NamaPenyelenggara[]" placeholder="Ketik Nama Penyelenggara..." data-input="wajib-prestasis" required />' +
        '        <div class="invalid-feedback">' +
        '            Kolom Wajib diisi.' +
        '        </div>' +
        '    </div>' +
        '    <div class="form-group col-md-6">' +
        '        <label for="">Keterangan</label> (optional)' +
        '        <input type="text" class="form-control" name="Keterangan[]" placeholder="Ketik Keterangan..." />' +
        '    </div>' +
        '</div>';
    $('div#formPrestasis').append(html);
    GetJenisPrestasis(i);
    GetTingkatPrestasiSiswas(i);
    GetTahuns(i);
}


const validate_max_file = () => {
    $("input[type='file']").on("change", function () {
        console.log(this.files[0].size)
        if (this.files[0].size > 5000000) {
            Swal.fire({
                icon: 'error',
                title: 'Gagal',
                text: 'Size file terlalu besar, maksimal 5MB',
            })
            $(this).val('');
        }
    });
}

const validate_type_file = () => {
    $("input[type='file']").change(function () {
        var fileExtension = ['jpeg', 'jpg', 'png'];
            console.log($(this).val().split('.').pop().toLowerCase())
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            Swal.fire({
                icon: 'error',
                title: 'Gagal',
                text: 'Only formats are allowed : ' + fileExtension.join(', '),
            })
            $(this).val('');
        }
    });
}
var xInputCicils = 1;

const CreateFormPrestasis = () => {
    var HtmlData = "";
    HtmlData += 
    `<div class="form-row" id="InputUrlVideo_${xInputCicils}">
        <div class="form-group col-md-4">
            <a href="javascript:void(0);" class="text-danger" onClick="DeleteFormCicils(${xInputCicils});"><i class="fa fa-times"></i></a> &nbsp;<label for="">Prestasi Ke - ${xInputCicils+1}</label>
            <select class="form-control" name="IdJenisPrestasi[${xInputCicils}]" id="validationCustom01" data-input="wajib-prestasis" required>
                <option value="">Select One</option>
            </select>
            <div class="invalid-feedback">
                Kolom Wajib diisi.
            </div>
        </div>
        <div class="form-group col-md-4">
            <label for="">Tingkat Prestasi</label>
            <select class="form-control" name="IdJenisTingkatPrestasi[${xInputCicils}]" id="validationCustom02" data-input="wajib-prestasis" required>
                <option value="">Select One</option>
            </select>
            <div class="invalid-feedback">
                Kolom Wajib diisi.
            </div>
        </div>
        <div class="form-group col-md-4">
            <label for="">Tahun</label>
            <select class="form-control" name="Tahun[${xInputCicils}]" id="validationCustom03" data-input="wajib-prestasis" required>
                <option value="">Select One</option>
            </select>
            <div class="invalid-feedback">
                Kolom Wajib diisi.
            </div>
        </div>
        <div class="form-group col-md-4">
            <label for="">Nama Penyelenggara</label>
            <input type="text" class="form-control" name="NamaPenyelenggara[]" placeholder="Ketik Nama Penyelenggara..." data-input="wajib-prestasis" required />
            <div class="invalid-feedback">
                Kolom Wajib diisi.
            </div>
        </div>
        <div class="form-group col-md-4">
            <label for="">Keterangan</label> (optional)
            <input type="text" class="form-control" name="Keterangan[]" placeholder="Ketik Keterangan..." />
        </div>
        <div class="form-group col-md-4">
            <label for="">File Prestasi</label>
            <input type="file" class="form-control" name="FilePrestasi[]" id="FilePrestasi" data-input="wajib-prestasis" required />
            <div class="invalid-feedback">
                Kolom Wajib diisi.
            </div>
        </div>
    </div>`;
    $('div#formPrestasis').append(HtmlData);
    GetJenisPrestasis(xInputCicils);
    GetTingkatPrestasiSiswas(xInputCicils);
    GetTahuns(xInputCicils);
    validate_type_file()
    validate_max_file()
    xInputCicils++;
}

const DeleteFormCicils = (IdBtn) => {
    $("#InputUrlVideo_" + IdBtn).remove();
    xInputCicils--;
}

const ModalDataPrestasi = () => {
    $('#modalPrestasis').modal('show');
}

/* renew js */

const CekUnitLogik = (IdUnit) => {
    let IdJenisKategoriPendaftaran = $("select[name='IdJenisKategoriPendaftaran'] option:selected").text();
    let IdUnits = $("select[name='IdUnit'] option:selected").text();
    const text = IdUnits;

    $('#DivNisn').fadeIn();
    $('#Nisn').attr({
        'data-input': 'wajib',
        'required': true,
    });
    $('#AsalSekolah').attr({
        'data-input': 'wajib',
        'required': true,
    });
    $('#AlamatSekolahAsal').attr({
        'data-input': 'wajib',
        'required': true,
    });
    $('#NPSN').attr({
        'data-input': 'wajib',
        'required': true,
    });
    $('#AlasanPindah').attr({
        'data-input': 'wajib',
        'required': true,
    });
    
    if (text == "TKIT AT Taufiq") {
        $('#DivNisn').hide();
        $('#Nisn').removeAttr('data-input', 'required');
        if (IdJenisKategoriPendaftaran == "Siswa Baru") {
            $('#DivKetSekolahAsal').hide();
            $('#AsalSekolah').removeAttr('data-input', 'required');
            $('#AlamatSekolahAsal').removeAttr('data-input', 'required');
            $('#NPSN').removeAttr('data-input', 'required');
            $('#AlasanPindah').removeAttr('data-input', 'required');
        }
        $('#nav-upload-raport-tab').hide();
    } else if (text == "SDIT AT Taufiq") {
        $('#DivNisn').hide();
        $('#Nisn').removeAttr('data-input', 'required');
        $('#DivKetSekolahAsal').fadeIn();
        $('#nav-upload-raport-tab').hide();
        
    } else if (text == "SMPIT AT Taufiq") {
        $('#DivKetSekolahAsal').fadeIn();
        $('#nav-upload-raport-tab').fadeIn();
        $('#TABRaportS1').html("Kelas 4 Semester 1");
        $('#TABRaportS2').html("Kelas 4 Semester 2");
        $('#TABRaportS3').html("Kelas 5 Semester 1");
        $('#TABRaportS4').html("Kelas 5 Semester 2");
        $('#TABRaportS5').html("Kelas 6 Semester 1");
        $('#DivS1Inggris').fadeOut();
        $('#DivS1IPS').fadeOut();
        $('#DivS2Inggris').fadeOut();
        $('#DivS2IPS').fadeOut();
        $('#DivS3Inggris').fadeOut();
        $('#DivS3IPS').fadeOut();
        $('#DivS4Inggris').fadeOut();
        $('#DivS4IPS').fadeOut();
        $('#DivS5Inggris').fadeOut();
        $('#DivS5IPS').fadeOut();

    } else if (text == "SMAIT AT Taufiq") {
        $('#divIdKelasParalel').fadeIn();
        $('#IdKelasParalel').attr({
            'data-input': 'wajib',
            'required': true,
        });

        $('#divFileSuratKesehatan').fadeIn();
        $('#FileSuratKesehatan').attr({
            'data-input': 'wajib',
            'required': true,
        });

        $('#divFileBebasNarkoba').fadeIn();
        $('#FileBebasNarkoba').attr({
            'data-input': 'wajib',
            'required': true,
        });

        $('#DivKetSekolahAsal').fadeIn();
        $('#nav-upload-raport-tab').fadeIn();
        $('#TABRaportS1').html("Kelas 7 Semester 1");
        $('#TABRaportS2').html("Kelas 7 Semester 2");
        $('#TABRaportS3').html("Kelas 8 Semester 1");
        $('#TABRaportS4').html("Kelas 8 Semester 2");
        $('#TABRaportS5').html("Kelas 9 Semester 1");

        $('#DivS1Inggris').fadeIn();
        $('#DivS1IPS').fadeIn();
        $('#DivS2Inggris').fadeIn();
        $('#DivS2IPS').fadeIn();
        $('#DivS3Inggris').fadeIn();
        $('#DivS3IPS').fadeIn();
        $('#DivS4Inggris').fadeIn();
        $('#DivS4IPS').fadeIn();
        $('#DivS5Inggris').fadeIn();
        $('#DivS5IPS').fadeIn();

        $('#trKelasParalel').fadeIn();
        $('#trFileSuratKesehatan').fadeIn();
    } else {
        
        $('#divIdKelasParalel').hide();
        $("select[name='IdKelasParalel']").val('');
        $('#IdKelasParalel').removeAttr('data-input', 'required');

        $('#divFileSuratKesehatan').hide();
        $("input[name='FileSuratKesehatan']").val('');
        $('#FileSuratKesehatan').removeAttr('data-input', 'required');

        $('#divFileBebasNarkoba').hide();
        $("input[name='FileBebasNarkoba']").val('');
        $('#FileBebasNarkoba').removeAttr('data-input', 'required');

        $('#trKelasParalel').hide();
        $('#trFileSuratKesehatan').hide();
    }
}

// const CekFormulir = () => {
//     var res = initAjax(`${UrlService.GetPpdbDaftar}?IdPpdbDaftar=${IdPpdbDaftar}`);
//     if (res) ProgressBar("success");
//     if (res.IsSuccess) {

//     } else {

//     }
// }
const GetPpdbDaftar = () => {
    var IdPpdbDaftar = $('#NoPendaftaran').val();
    var res = initAjax(`${UrlService.GetPpdbDaftar}?IdPpdbDaftar=${IdPpdbDaftar}`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        var data = res.Data;
        if (data.Pin != null) {

            var resx = initAjax(`${UrlService.GetPpdbSiswa}?IdPpdbDaftar=${IdPpdbDaftar}`);
            if (resx) 
            if (resx.IsSuccess) {
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal',
                    text: 'Sebelum nya anda sudah mengisi formulir, tidak boleh isi kembali !',
                })
            } else {
                $('#modalPin').modal('show');
                $('#PvNamaLengkap').html(data.Nama);
                $('#PvJenisKategori').html(data.JenisKategoriPendaftaran);
                $('#PvJenjangPendidikan').html(data.Unit);
                $('#PvKelas').html(data.Kelas);
                if (data.Unit == "SMAIT AT Taufiq")
                    $('#PvKelas').html(data.Kelas + ' - ' + data.Peminatan);
                $('#PvJenisDaftar').html(data.JenisPendaftaran);
                $('#PvJalurDaftar').html(data.JalurPendaftaran);
                $('#PvTanggalLahir').html(indoDate(data.TanggalLahir));
                $('#ValidatePin').val(data.Pin);
                if (data.IdUnit == 4) {
                    $('#trKelasParalel').fadeIn();
                    $('#PvKelasParalel').html(data.KelasParalel);
                } else {
                    $('#trKelasParalel').hide();
                }
    
                /* set formulir */
                GetKategoriDaftars(function (obj) {
                    $("select[name='IdJenisKategoriPendaftaran']").html(obj);
                }, data.IdJenisKategoriPendaftaran);
    
                GetKelasPpdbs(function (obj) {
                    $("select[name='IdPpdbKelas']").html(obj);
                }, data.IdPpdbKelas);
    
                GetJenisPendaftarans(function (obj) {
                    $("select[name='IdJenisPendaftaran']").html(obj);
                }, data.IdJenisPendaftaran);
    
                GetJalurPendaftarans(function (obj) {
                    $("select[name='IdJalurPendaftaran']").html(obj);
                }, data.IdJalurPendaftaran);
    
                ComboGetUnitSklh(function (obj) {
                    $("select[name='IdUnit']").html(obj);
                }, data.IdUnit);
    
                if (data.IdJenisPendaftaran == 1) {
                    GetSiswaByNisn(data.NisSiswa);
                }
    
                $('#Nama').val(data.Nama);
                $('#KdJenisKelamin').val(data.KdJenisKelamin);
                $('#TempatLahir').val(data.TempatLahir);
                $('#TanggalLahir').val(data.TanggalLahir);
                $('#Email').val(data.Email);
                $('#NoHandphone').val(data.NoHandphone);
                
                $('#DivDataPrestasi').hide();
                if (data.IdJalurPendaftaran == 1)
                    $('#DivDataPrestasi').fadeIn();
                
                GetSeragam(data.IdUnit);
            }
            
           

            
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Gagal',
                text: 'Silahkan melakukan pembayaran pendaftaran terlebih dahulu',
            })
        }
        
    } else if (!res.IsSuccess) {
        Swal.fire({
            icon: 'error',
            title: 'Gagal',
            text: res.ReturnMessage,
        })
    }
}

const ValidatePin = () => {
    var ValidatePin = $('#ValidatePin').val();
    var KodePin = $('#KodePin').val();
    if (ValidatePin == KodePin) {
        $('#modalPin').modal('hide');
        $('div#imageEmpty').hide();
        $('div#tablePpdb').fadeIn();
        $('#DivBtnCari').hide();
        $('#DivBtnReload').show();
        CekUnitLogik();
        DataKuisioner1();
        DataKuisioner2();
        DataKuisioner3();
        DataKuisioner4();
        DataKuisioner5();
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Gagal',
            text: 'Kode PIN salah',
        })
        return;
    }
}

const BtnMulaiFormulir = () => {
    Swal.fire({
        title: 'Siap Lanjutkan?',
        text: "Klik lanjutkan untuk mengisi formulir & kuisioner online!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Lanjutkan!',
        cancelButtonText: 'Batalkan',
        allowOutsideClick: false
    }).then((result) => {
        if (result.value) {
            $('body').css('pointer-events', 'none');

            let timerInterval;
            Swal.fire({
                title: 'Tunggu Sebentar!',
                html: 'Sedang memuat data...',
                timer: 3000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getContent()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                onClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                    $('body').css('pointer-events', 'unset');
                    $('div#imageEmpty').hide();
                    $('div#tablePpdb').hide();
                    $('div#DivFormFormulir').fadeIn();
                    $('#NoPendaftaran').attr('readonly', true);
                }
            })
        }
    })
}

const GetPpdbBiayaByUnit = () => {
    var IdPpdbKelas = $('#IdPpdbKelas option:selected').val();
    var res = initAjax(`${UrlService.GetPpdbBiayaByUnit}?IdPpdbKelas=${IdPpdbKelas}`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        $('#modalBiaya').modal('show');
        var data = res.Data;
        var htmlBiaya = ``;
        $.each(data, (i, v) => {
            if (v.JenisBiaya != "BIAYA PENDAFTARAN") {
                if (v.JenisBiaya == "INFAQ") {
                    // htmlBiaya += `
                    // <div class="form-group row">
                    //     <label class="col-sm-4 col-form-label">${v.JenisBiaya}</label>
                    //     <div class="col-sm-8">
                    //         <input id="IdJenisBiayaPpdb" type="hidden" name="IdJenisBiayaPpdb[]" class="form-control" value="${v.IdJenisBiaya}">
                    //         <div class="input-group mb-3">
                    //             <div class="input-group-prepend">
                    //                 <span class="input-group-text" id="label-Rp">Rp. </span>
                    //             </div>
                    //             <input type="number" class="form-control" id="Rp" name="Rp[]" style="border-color:#d4d0cf" value="${v.Rp}">
                    //         </div>
                    //     </div>
                    // </div>
                    // `;
                    // htmlBiaya += `
                    // <div class="form-group row">
                    //     <label class="col-sm-4 col-form-label">${v.JenisBiaya}</label>
                    //     <div class="col-sm-8">
                    //         <input id="IdJenisBiayaPpdb" type="hidden" name="IdJenisBiayaPpdb[]" class="form-control" value="${v.IdJenisBiaya}">
                    //         <input id="Rp" type="hidden" name="Rp[]" class="form-control" value="${v.Rp}" disabled>
                    //         <label class="col-form-label">Rp. ${formatRupiah(v.Rp)}</label>
                    //     </div>
                    // </div>
                    // `;
                } else if (v.JenisBiaya == "WAKAF") {
                    htmlBiaya += `
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">${v.JenisBiaya}</label>
                        <div class="col-sm-8">
                            <input type="hidden" class="form-control" id="RpWakafAsli" style="border-color:#d4d0cf" value="${v.Rp}">
                            <input id="IdJenisBiayaPpdb" type="hidden" name="IdJenisBiayaPpdb[]" class="form-control" value="${v.IdJenisBiaya}">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="label-Rp">Rp. </span>
                                </div>
                                <input type="number" class="form-control" id="RpWakaf" name="Rp[]" style="border-color:#d4d0cf" value="${v.Rp}">
                            </div>
                            <small class="text-danger">*Minimal Wakaf 1 M2 </small>
                        </div>
                    </div>
                    `;
                } else if (v.JenisBiaya == "SPP") {
                    htmlBiaya += `
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">${v.JenisBiaya}</label>
                        <div class="col-sm-8">
                            <input type="hidden" class="form-control" id="RpSppAsli" style="border-color:#d4d0cf" value="${v.Rp}">
                            <input id="IdJenisBiayaPpdb" type="hidden" name="IdJenisBiayaPpdb[]" class="form-control" value="${v.IdJenisBiaya}">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="label-Rp">Rp. </span>
                                </div>
                                <input type="number" class="form-control" id="RpSpp" name="Rp[]" style="border-color:#d4d0cf" value="${v.Rp}">
                            </div>
                            <small class="text-danger">*SPP (Rp ${formatRupiah(v.Rp)}) yang tertera adalah SPP minimal dan dapat ditambahkan sesuai keinginan </small>
                        </div>
                    </div>
                    `;
                } else if (v.JenisBiaya == "BOKS") {
                    htmlBiaya += `
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">${v.JenisBiaya}</label>
                        <div class="col-sm-8">
                            <input id="IdJenisBiayaPpdb" type="hidden" name="IdJenisBiayaPpdb[]" class="form-control" value="${v.IdJenisBiaya}">
                            <input id="Rp" type="hidden" name="Rp[]" class="form-control" value="${v.Rp}" disabled>
                            <label class="col-form-label">Rp. ${formatRupiah(v.Rp)}</label><br />
                            <small class="text-danger">*BOKS ditentukan kemudian dan dibayarkan Bulan Mei/Juni 2020 </small>
                        </div>
                    </div>
                    `;
                } else {
                    htmlBiaya += `
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">${v.JenisBiaya}</label>
                        <div class="col-sm-8">
                            <input id="IdJenisBiayaPpdb" type="hidden" name="IdJenisBiayaPpdb[]" class="form-control" value="${v.IdJenisBiaya}">
                            <input id="Rp" type="hidden" name="Rp[]" class="form-control" value="${v.Rp}" disabled>
                            <label class="col-form-label">Rp. ${formatRupiah(v.Rp)}</label>
                        </div>
                    </div>
                    `;
                }
            }

            
            // return i < 5;
        })
        $('#DivBiayaList').html(htmlBiaya);
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Gagal',
            text: res.ReturnMessage,
        })
    }
}

const CekNikSiswa = (e) => {
    let PanjangNik = (e.value).length;
    if(PanjangNik >= 16) {
        var nik = $('#NikSiswa').val().slice(0,16);
        $('#NikSiswa').val(nik);
        nikParse(nik, function(result) {
            console.log(result); // object
            if(result.status == "success") {
                $('#InfoNikSiswaSalah').hide();
                $('#InfoNikSiswaSukses').fadeIn();
                $('#BtnRegister0').attr('disabled', false);
            } else {
                $('#InfoNikSiswaSalah').fadeIn();
                $('#InfoNikSiswaSukses').hide();
            }
        });	
    } else {
        $('#InfoNikSiswaSukses').hide();
        $('#BtnRegister0').attr('disabled', true);
    }
}

const CekNikIbu = (e) => {
    let PanjangNik = (e.value).length;
    if(PanjangNik >= 16) {
        var nik = $('#NikIbu').val().slice(0,16);
        $('#NikIbu').val(nik);
        nikParse(nik, function(result) {
            console.log(result); // object
            if(result.status == "success") {
                $('#InfoNikIbuSalah').hide();
                $('#InfoNikIbuSukses').fadeIn();
                $('#BtnRegister0').attr('disabled', false);
            } else {
                $('#InfoNikIbuSalah').fadeIn();
                $('#InfoNikIbuSukses').hide();
            }
        });	
    } else {
        $('#InfoNikIbuSukses').hide();
        $('#BtnRegister0').attr('disabled', true);
    }
}

const CekNikAyah = (e) => {
    let PanjangNik = (e.value).length;
    if(PanjangNik >= 16) {
        var nik = $('#NikAyah').val().slice(0,16);
        $('#NikAyah').val(nik);
        nikParse(nik, function(result) {
            console.log(result); // object
            if(result.status == "success") {
                $('#InfoNikAyahSalah').hide();
                $('#InfoNikAyahSukses').fadeIn();
                $('#BtnRegister0').attr('disabled', false);
            } else {
                $('#InfoNikAyahSalah').fadeIn();
                $('#InfoNikAyahSukses').hide();
            }
        });	
    } else {
        $('#InfoNikAyahSukses').hide();
        $('#BtnRegister0').attr('disabled', true);
    }
}

const CekNikWali = (e) => {
    let PanjangNik = (e.value).length;
    if(PanjangNik >= 16) {
        var nik = $('#NikWali').val().slice(0,16);
        $('#NikWali').val(nik);
        nikParse(nik, function(result) {
            console.log(result); // object
            if(result.status == "success") {
                $('#InfoNikWaliSalah').hide();
                $('#InfoNikWaliSukses').fadeIn();
            } else {
                $('#InfoNikWaliSalah').fadeIn();
                $('#InfoNikWaliSukses').hide();
            }
        });	
    } else {
        $('#InfoNikWaliSukses').hide();
    }
}

const DetailNik = (val) => {
    var nik = '';
    if (val == 'siswa') {
        nik = $('#NikSiswa').val().slice(0,16);
    } else if (val == 'ibu') {
        nik = $('#NikIbu').val().slice(0,16);
    } else if (val == 'ayah') {
        nik = $('#NikAyah').val().slice(0,16);
    } else if (val == 'wali') {
        nik = $('#NikWali').val().slice(0,16);
    }
    nikParse(nik, function(result) {
        if(result.status == "success") {
            $('#modalNik').modal('show');
            $('#modalDataOrtu').modal('hide');
            $('#SWNik').html(result.data.nik);
            $('#SWJK').html(result.data.kelamin);
            $('#SWTL').html(result.data.lahir);
            $('#SWUsia').html(result.data.tambahan.usia);
            $('#SWUltah').html(result.data.tambahan.ultah);
        } else {
            $('#modalNik').modal('hide');
        }
    });
}
