﻿$(document).ready((e) => {
  // window.location.replace(base_url);

  $("#TanggalLahir").mask("00-00-0000");
  GetKategoriDaftars(function (obj) {
    $("select[name='IdJenisKategoriPendaftaran']").html(obj);
  });

  $("select[name='IdJenisKategoriPendaftaran']").change((e) => {
    $("#divIdUnit").fadeIn();
    $("#divIdKelas").hide();
    GetUnits();

    if (e.target.value == 2) $("#modalPindahan").modal("show");
  });

  $("select[name='IdUnit']").change((e) => {
    $("#divIdKelas").fadeIn();

    GetKelass(e.target.value);
  });

  $("select[name='IdPpdbKelas']").change((e) => {
    let Id = e.target.value;
    $("#divIdJenisPendaftaran").fadeIn();
    GetJenisPendaftaransLoc();
  });

  $("select[name='IdJenisPendaftaran']").change((e) => {
    let Id = e.target.value;
    $("#divIdJalurPendaftaran").fadeIn();
    $("#divIdSiswaSaudara").fadeIn();
    GetJalurPendaftaransLoc();

    if (Id == 1) {
      var html =
        '<div class="form-group row">' +
        '    <label class="col-sm-4 col-form-label">NIS Peserta Didik</label>' +
        '    <div class="col-sm-8">' +
        '        <div class="input-group">' +
        '            <input id="NisSiswa" type="number" name="NisSiswa" class="form-control" placeholder="Ketik NIS Peserta Didik..." data-input="wajib" required>' +
        '            <div class="input-group-append">' +
        '                <button id="BtnCariNisn" onClick="GetSiswaByNisn();" class="btn btn-theme" type="button"> <div id="htmlBtnCariNisn"><i class="fa fa-search"></i> Cari</div></button>' +
        "            </div>" +
        "        </div>" +
        '        <div class="invalid-feedback">' +
        "           Kolom Wajib diisi." +
        "        </div>" +
        "    </div>" +
        "</div>";
      $("#divNisn").html(html);
      setTimeout(() => {
        ModalSaudaraKandung(true);
      }, 1000);
    }

    if (Id == 2) {
      // var html = '<div class="form-group row">' +
      //     '    <label class="col-sm-4 col-form-label">NISN Calon Peserta Didik</label>' +
      //     '    <div class="col-sm-8">' +
      //     '        <input id="NisSiswa" type="number" name="NisSiswa" class="form-control" placeholder="Ketik NISN Calon Peserta Didik..." data-input="wajib" required>' +
      //     '           <small>Wajib cek terlebih dahulu NISN anda <a href="https://referensi.data.kemdikbud.go.id/nisn/" target="_blank">disini</a></small>' +
      //     '        <div class="invalid-feedback">' +
      //     '           Kolom Wajib diisi.' +
      //     '        </div>' +
      //     '    </div>' +
      //     '</div>';
      var html = "";
      $("#divNisn").html(html);
      $("#divNisn").fadeIn();
      setTimeout(() => {
        ModalSaudaraKandung(true);
      }, 1000);
    }
  });
  $("select[name='IdJalurPendaftaran']").change((e) => {
    let Id = e.target.value;
  });
});

const UrlService = {
  Registrasi: `${base_url}/api/webs/Registrasi`,
  GetSiswaByNis: `${base_url}/api/webs/GetSiswaByNis`,
  GetJenisPendaftarans: `${base_url}/api/webs/GetJenisPendaftarans`,
  GetJalurPendaftarans: `${base_url}/api/webs/GetJalurPendaftarans`,
};

const ModalPindahan = () => {
  $("#ModalPindahan").modal("show");
};

const allnumeric = (inputtxt) => {
  var numbers = /^[0-9]+$/;
  if (inputtxt.match(numbers)) {
    // alert('ok')
    return true;
  } else {
    Swal.fire({
      icon: "error",
      title: "Gagal",
      text: "Masukan Nomor HP dengan benar, contoh (08212234882)",
    });
    return false;
  }
};

const GetJenisPendaftaransLoc = () => {
  let IdUnit = $("select[name='IdUnit'] option:selected").val();
  $.getJSON(UrlService.GetJenisPendaftarans, (res) => {
    let html = '<option value="">Select One</option>';
    if (res.IsSuccess) {
      $.each(res.Data, (i, v) => {
        if (IdUnit == 1) {
          if (v.Id != 1) {
            html += '<option value="' + v.Id + '">' + v.Nama + "</option>";
          }
        } else {
          html += '<option value="' + v.Id + '">' + v.Nama + "</option>";
        }
      });
      // handleData(html);
      $('select[name="IdJenisPendaftaran"]').html(html);
    } else console.log(res);
  });
};

const GetJalurPendaftaransLoc = () => {
  let IdUnit = $("select[name='IdUnit'] option:selected").val();
  $.getJSON(UrlService.GetJalurPendaftarans, (res) => {
    let html = '<option value="">Select One</option>';
    if (res.IsSuccess) {
      $.each(res.Data, (i, v) => {
        if (IdUnit != 1 && IdUnit != 2) {
          html += '<option value="' + v.Id + '">' + v.Nama + "</option>";
        } else {
          if (v.Id != 1) {
            html += '<option value="' + v.Id + '">' + v.Nama + "</option>";
          }
        }
      });
      // handleData(html);
      $('select[name="IdJalurPendaftaran"]').html(html);
    } else console.log(res);
  });
};

const CekNik = (e) => {
  let PanjangNik = e.value.length;
  if (PanjangNik >= 16) {
    var nik = $("#NikOrtu").val().slice(0, 16);
    $("#NikOrtu").val(nik);
    nikParse(nik, function (result) {
      console.log(result); // object
      if (result.status == "success") {
        $("#InfoNikSalah").hide();
        $("#InfoNikSukses").fadeIn();
        $("#BtnRegister").attr("disabled", false);
      } else {
        $("#InfoNikSalah").fadeIn();
        $("#InfoNikSukses").hide();
      }
    });
  } else {
    $("#InfoNikSukses").hide();
    $("#BtnRegister").attr("disabled", true);
  }
};

const DetailNikOrtu = () => {
  var nik = $("#NikOrtu").val().slice(0, 16);
  nikParse(nik, function (result) {
    if (result.status == "success") {
      $("#modalNikOrtu").modal("show");
      $("#PvNik").html(result.data.nik);
      $("#PvJK").html(result.data.kelamin);
      $("#PvTL").html(result.data.lahir);
      $("#PvUsia").html(result.data.tambahan.usia);
      $("#PvUltah").html(result.data.tambahan.ultah);
    } else {
      $("#modalNikOrtu").modal("hide");
    }
  });
};

const GetUnits = () => {
  var IdKategori = $("#IdJenisKategoriPendaftaran option:selected").val();
  $.getJSON(base_url + "/api/webs/GetUnits", (res) => {
    let html = '<option value="">Select One</option>';
    if (res.IsSuccess) {
      $.each(res.Data, (i, v) => {
        if (IdKategori == 2) {
          if (v.Id != 1) {
            html += '<option value="' + v.Id + '">' + v.Nama + "</option>";
            return i < 3;
          }
        } else {
          html += '<option value="' + v.Id + '">' + v.Nama + "</option>";
          return i < 3;
        }
      });
    } else console.log(res);
    $('select[name="IdUnit"]').html(html);
  });
};

const GetKelass = (IdUnit) => {
  var IdKategori = $("#IdJenisKategoriPendaftaran option:selected").val();
  var Url = base_url + "/api/webs/GetKelasPpdb?IdUnit=" + IdUnit;
  $.getJSON(Url, (res) => {
    let html = '<option value="">Select One</option>';
    if (res.IsSuccess) {
      $.each(res.Data, (i, v) => {
        if (IdKategori == 2) {
          // pertama cek kategori

          if (IdUnit == 2) {
            if (v.Nama != "I" && v.Nama != "VI") {
              html += '<option value="' + v.Id + '">' + v.Nama + "</option>";
            }
          }
          if (IdUnit == 3) {
            if (v.Nama != "VII" && v.Nama != "IX") {
              html += '<option value="' + v.Id + '">' + v.Nama + "</option>";
            }
          }
          if (IdUnit == 4) {
            if (
              v.Nama != "X IPA" &&
              v.Nama != "X IPS" &&
              v.Nama != "XII IPA" &&
              v.Nama != "XII IPS"
            ) {
              html += '<option value="' + v.Id + '">' + v.Nama + "</option>";
            }
          }
        } else {
          if (IdUnit == 1) {
            html += '<option value="' + v.Id + '">' + v.Nama + "</option>";
            return i < 1;
          } else if (IdUnit != 4) {
            html += '<option value="' + v.Id + '">' + v.Nama + "</option>";
            return i < 0;
          } else {
            if (v.Nama == "X IPA" || v.Nama == "X IPS") {
              html += '<option value="' + v.Id + '">' + v.Nama + "</option>";
            }
          }
        }
      });
    } else console.log(res);
    $('select[name="IdPpdbKelas"]').html(html);
  });
};

const DataNisnReset = () => {
  $("#FormRegister")[0].reset();
  $("input#Nisn", "#FormRegister").attr("readonly", false);
  $("div#htmlBtnCariNisn").html('<i class="fa fa-search"></i> Cari Ulang');
  $("button#BtnCariNisn").attr("onclick", "GetSiswaByNisn()");
};

const ModalSaudaraKandung = (bool) => {
  if (bool) {
    $("#FormSaudaraKandung").fadeOut();
    $("#modalSaudaraKandung").modal("show");
    $("#htmlPertanyaanSK").show();
    $("#htmlBtnMemilikiSk").html("Ya, Saya Memiliki");
    $("#BtnMemilikiSK").show();
    $("#BtnMemilikiSK").attr("onclick", "DataSaudaraKandung(false)");
  } else if (!bool) {
    $("#FormSaudaraKandung").fadeIn();
    $("#htmlPertanyaanSK").hide();
    $("#htmlBtnMemilikiSk").html("Cari Sekarang !");
    $("#BtnMemilikiSK").attr("onclick", "GetSiswaByNisn(true)");
  }
};

const GetSiswaByNisn = (bool = false) => {
  var Nisn =
    bool == false
      ? $("input#NisSiswa", "#FormRegister").val()
      : $("input#NisnSaudara", "#FormSaudaraKandung").val();
  console.log(`${UrlService.GetSiswaByNis}?nis=${Nisn}`);
  $.getJSON(`${UrlService.GetSiswaByNis}?nis=${Nisn}`, (res) => {
    if (res.IsSuccess) {
      if (res.Data.TotalTagihan1 > 0) {
        // Swal.fire({
        //     icon: 'error',
        //     title: 'Oops...',
        //     text: 'Mohon maaf saat ini ananda belum bisa melakukan pendaftaran,  silahkan menghubungi bagian keuangan untuk menyelesaikan administrasi sekolah!'
        // }).then(function () {
        //     window.location.href = `${base_url}/ppdb`
        // });
        $("#modalTagihan").modal("show");
        $("input#NisSiswa", "#FormRegister").val("");
        $("input#NisnSaudara", "#FormSaudaraKandung").val("");
        $("#alertSKGagal").hide();
      } else {
        const {
          Agama,
          AlamatOrtu,
          AlamatTinggal,
          Email,
          EmailOrtu,
          IdAgama,
          IdJenisPekerjaanAyah,
          IdJenisPekerjaanIbu,
          IdJenisPrestasi,
          IdKelas,
          IdKelasParalel,
          IdSiswa,
          IdUnit,
          JenisKelamin,
          JenisPekerjaanAyah,
          JenisPekerjaanIbu,
          KdJenisKelamin,
          Kelas,
          KelasParalel,
          Nama,
          NamaAyah,
          NamaIbu,
          NamaInstansiAyah,
          NamaInstansiIbu,
          NamaPanggilan,
          Nik,
          NikAyah,
          NikIbu,
          Nisn,
          NoDarurat,
          NoHandphone,
          Status,
          StrStatus,
          TanggalLahir,
          TempatLahir,
          Unit,
        } = res.Data;
        if (bool == false) {
          if (
            $("input#Nisn", "#FormRegister").val() ==
            $("input#NisnSaudara", "#FormSaudaraKandung").val()
          ) {
            Swal.fire({
              icon: "error",
              title: "Oops...",
              text: "Nisn Sudah Ditambahkan Sebagai Saudara!",
            });
          } else {
            $("input#IdSiswa").val(IdSiswa);
            //IdUnit != 0 ? $('select#IdUnit').val(IdUnit) : $('select#IdUnit').val('') ;
            //IdKelas != 0 ? $('select#IdKelas').val(IdKelas) : $('select#IdKelas').val('') ;
            //if (IdJenisPrestasi.length != 0) {
            //    $('select#IdJalurPendaftaran').val('1')
            //    ModalPrestasis(true)
            //}
            $("input#Nisn", "#FormRegister").attr("readonly", true);
            $("div#htmlBtnCariNisn").html(
              '<i class="fa fa-times"></i> Batalkan'
            );
            $("button#BtnCariNisn").attr("onclick", "DataNisnReset()");
            $("input#NikSiswa").val(Nik);
            $("input#Nama", "#FormRegister").val(Nama);
            $("input#NamaPanggilan").val(NamaPanggilan);
            $("#KdJenisKelamin").val(KdJenisKelamin);
            $("select#IdAgama").val(IdAgama);
            $("input#TempatLahir", "#FormRegister").val(TempatLahir);
            $("input#TanggalLahir", "#FormRegister").val(TanggalLahir);
            $("input#NikIbu").val(NikIbu);
            $("input#NamaIbu").val(NamaIbu);
            $("select#IdJenisPekerjaanIbu").val(IdJenisPekerjaanIbu);
            $("input#NamaInstansiIbu").val(NamaInstansiIbu);
            $("input#NikAyah").val(NikAyah);
            $("input#NamaAyah").val(NamaAyah);
            $("select#IdJenisPekerjaanAyah").val(IdJenisPekerjaanAyah);
            $("input#NamaInstansiAyah").val(NamaInstansiAyah);
            $("input#AlamatIbu").val(AlamatOrtu);
            $("input#AlamatAyah").val(AlamatOrtu);
            $("input#NoHandphoneIbu").val(NoHandphone);
            $("input#NoHandphoneAyah").val(NoHandphone);
            $("input#EmailIbu").val(EmailOrtu);
            $("input#EmailAyah").val(EmailOrtu);

            if (res.Data.TotalTagihan.length < 0) {
              alert("gagal woi");
            }
            console.log("woi -> ", res.Data.TotalTagihan.length);
          }
          console.log("woi -> ", res.Data.TotalTagihan);
        } else {
          $("input#NisnSaudara", "#FormSaudaraKandung").attr("readonly", true);
          $("div#alertSKGagal").hide();
          $("div#alertSK").fadeIn();
          $("input#IdSiswaSaudara").val(IdSiswa);
          $("span#resNamaSK").html(Nama);
          $("span#resKelasSK").html(Kelas);
          $("#htmlBtnMemilikiSk").html("Ya, Ini Saudara Saya");
          $("#BtnMemilikiSK").attr("onclick", "DataSaudaraKandung(true)");
        }
      }

      //console.log(res)
    } else {
      if (bool == false) {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Nisn Tidak Ditemukan!",
        });
      } else {
        $("input#NisnSaudara", "#FormSaudaraKandung").attr("readonly", false);
        $("#htmlBtnMemilikiSk").html("Cari Ulang");
        $("div#alertSK").hide();
        $("div#alertSKGagal").fadeIn();
      }
    }
  });
};

const ModalSaudaraKandungInfo = () => {
  $("#FormSaudaraKandung").fadeIn();
  $("#modalSaudaraKandung").modal("show");
  $("#htmlPertanyaanSK").hide();
  $("#htmlBtnMemilikiSk").html("Simpan");
  $("#BtnMemilikiSK").show();
  $("#BtnMemilikiSK").attr("onclick", "DataSaudaraKandung(true)");
};

const DataSaudaraKandung = (bool = false) => {
  if (!bool) {
    ModalSaudaraKandung(false);
  } else {
    $("#htmlInfoSK").fadeIn();
    $("#modalSaudaraKandung").modal("hide");
  }
};

const DataSaudaraKandungReset = () => {
  $("input#NisnSaudara", "#FormSaudaraKandung").attr("readonly", false);
  $("input#NisnSaudara", "#FormSaudaraKandung").val("");
  $("input#IdSiswaSaudara", "#FormSaudaraKandung").val("");
  $("#modalSaudaraKandung").modal("hide");
  $("div#alertSK").fadeOut();
  $("small#htmlInfoSK").fadeOut();
  $("input#IdSiswaSaudara").val("");
};

const Register = () => {
  if (!allnumeric($("#NoHandphone").val())) return;
  var TypeInput = true;
  $('[data-input="wajib"]').each(function (i, v) {
    if ($(this).val() == "") {
      TypeInput = false;
      $("#FormRegister").addClass("was-validated");
      Swal.fire({
        icon: "error",
        title: "Gagal",
        text: "Kolom " + $(this).attr("label") + " Wajib Diisi",
      });
      return i < 0;
    }
  });

  if (TypeInput) {
    var fd = new FormData($("#FormRegister")[0]);
    var NisSodara = $("#NisnSaudara").val();
    if (NisSodara != "") fd.append("NisSodara", NisSodara);

    $.ajax({
      url: UrlService.Registrasi,
      type: "POST",
      processData: false,
      contentType: false,
      data: fd,
      dataType: "JSON",
      beforeSend: () => {
        ProgressBar("wait");
      },
      success: (res) => {
        ProgressBar("success");
        if (res.IsSuccess) {
          Swal.fire({
            icon: "success",
            title: "Berhasil",
            text: res.Data,
          }).then(function () {
            window.location.href = `${base_url}/ppdb`;
          });
          $("#BtnRegister").attr("disabled", true);
        } else {
          Swal.fire({
            icon: "error",
            title: "Gagal",
            text: res.ReturnMessage,
          });
        }
      },
      error: (err, a, e) => {
        ProgressBar("success");
        Swal.fire({
          icon: "error",
          title: "Terjadi Kesalahan",
          text: JSON.stringify(err),
          footer: "<a href>silahkan hubungi admin ppdb</a>",
        });
      },
    });
  }
};
