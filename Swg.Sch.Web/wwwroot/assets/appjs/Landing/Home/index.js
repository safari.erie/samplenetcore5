﻿var urlApi = base_url + "/api/homes/gethome";
$(document).ready(function () {
    loadData()
});

function loadData() {
    $.ajax({
        url: urlApi,
        type: "GET",
        beforeSend: function (before) {
            console.log(before)
            ProgressBar('wait')
        },
        success: function (r) {
            ProgressBar('success')
            var res = r.Data;
            if (r.IsSuccess == true) {
                $('span#Judul').html(res.Judul)
                $('span#Keterangan').html(res.Keterangan)

                dataVisiMisi(res.VisiMisi)
                dataMotoUnit(res.MotoUnit)
            } else if (r.IsSuccess == false) {
                alert('fire')
            }
        }, error: function (error, a, e) {
            alert(JSON.stringify(error) + " : " + e)
        }
    })
}

function dataVisiMisi(r) {
    var VisiMisi = "";
    for (var i = 0; i < (r).length; i++) {
        console.log(i)
        VisiMisi +=
            '<div class="item">' +
            '    <div class="featured-item style-2">' +
            '        <div class="featured-icon">' +
            '            <i class="flaticon-data"></i>' +
            '            <span class="rotateme"></span>' +
            '        </div>' +
            '        <div class="featured-title">' +
            '            <h5>' + r[i].Judul + '</h5>' +
            '        </div>' +
        '        <div class="featured-desc">' +
        '            <p>' + r[i].Keterangan + '.</p>' +
            '            <a class="icon-btn mt-4" href="#">' +
            '                <i class="la la-angle-right"></i>' +
            '            </a>' +
            '        </div>' +
            '    </div>' +
            '</div>';
    }
    $('div#VisiMisi').html(VisiMisi)
}
function dataMotoUnit(r) {
    $('p#MotoUnit_TKIT').html(r.Tk)
    $('p#MotoUnit_SDIT').html(r.Sd)
    $('p#MotoUnit_SMPIT').html(r.Smp)
    $('p#MotoUnit_SMAIT').html(r.Sma)
}