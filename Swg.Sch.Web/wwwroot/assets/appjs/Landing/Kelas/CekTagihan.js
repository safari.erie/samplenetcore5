﻿$(document).ready(function () {
    GetTahuns()
})

function CekTagihan() {
    var nisn = $('input#nisn').val();
    var bulan = $('select#bulan').val();
    var tahun = $('select#tahun').val();
    var url = base_url + "/api/webs/GetSiswaTagihan?nisn=" + nisn + "&bulan=" + bulan + "&tahun=" + tahun;
    console.log(url)
    $('#TabelCekTagihan').DataTable({
        "ajax": {
            "url": url,
            "type": "GET",
            "dataSrc": function (json) {
                if (json.Data == null) {
                    alert(json.ReturnMessage)
                    return json;
                } else {
                    $('#ModalCekTagihan').modal('show');
                    if ((json.Data.Tagihans).length == 0) {
                        return json.Data.Tagihans;
                    } else {
                    return json.Data.Tagihans;
                    }
                }
            }
        },

        "scrollX": false,
        "processing": true,
        "paging": true,
        "searching": true,
        "pageLength": 10,

        "columns": [
            { "data": "JenisTagihan", "name": "JenisTagihan", "autoWidth": true },
            { "data": "Rp", "name": "Rp", "autoWidth": true }

        ],
        "bDestroy": true
    });

}

function GetTahuns() {
    var StartDate = (new Date).getFullYear() - 4;
    var EndDate = (new Date).getFullYear();
    var HtmlCombo = "";
    HtmlCombo += "<option value=''>- Pilih Salah Satu -</option>";
    for (var i = StartDate; i <= EndDate; i++) {
        if (i == EndDate) {
            HtmlCombo += "<option value='" + i + "' selected>" + i + "</option>";
        } else {
            HtmlCombo += "<option value='" + i + "'>" + i + "</option>";
        }
    }
    $('select#tahun').html(HtmlCombo)
}