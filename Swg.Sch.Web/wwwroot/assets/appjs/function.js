function ComboGetJenisPekerjaan(handleData, IdJenisPekerjaan) {
    var Url = base_url + "/api/webs/GetJenisPekerjaans";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdJenisPekerjaan) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}


const initAjax = (service, redirect = null) => {
    var dataArray = $.ajax({
        type: "GET",
        async: false,
        beforeSend: function () {
            ProgressBar("wait");
        },
        url: service,
        error: function (err, a, e) {
            ProgressBar("success");
            if (redirect == null) {
                Swal.fire({
                    icon: 'error',
                    title: 'Terjadi Kesalahan',
                    text: JSON.stringify(err),
                });
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Terjadi Kesalahan',
                    text: JSON.stringify(err),
                }).then(function () {
                    window.location.href = redirect;
                });
            }
        }
    });

    var result = dataArray.responseJSON; //JSON.stringify

    return result;
}

const indoDate = (Date) => {
    var SplitTanggal = Date.split("-");
    var Hari = SplitTanggal[0];
    var Bulan = SplitTanggal[1];
    var Tahun = SplitTanggal[2];

    var ArrayBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    if (Bulan < 10) {
        Bulan = Bulan.replace('0', '');
    }

    return Hari + " " + ArrayBulan[Bulan - 1] + " " + Tahun;
}

const formatRupiah = (angka) => {
    var TipeData = jQuery.type(angka);
    if (TipeData == "number") {
        var bilangan = angka;
        var reverse = bilangan.toString().split('').reverse().join(''),
            ribuan = reverse.match(/\d{1,3}/g);
        ribuan = ribuan.join('.').split('').reverse().join('');
        return ribuan;
    }
    return "Tidak Ada";
}

const GetKategoriDaftars = (handleData, IdJenisKategoriPendaftaran) => {
    $.getJSON(base_url + "/api/webs/GetKategoriDaftars", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if(v.Id == IdJenisKategoriPendaftaran)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                
                html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
            handleData(html);
        } else console.log(res)
    });
}

const GetKelasPpdbs = (handleData, IdPpdbKelas) => {
    var Url = base_url + "/api/webs/GetKelasPpdbs";
    $.getJSON(Url, (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if(v.Id == IdPpdbKelas)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                
                html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
            handleData(html);
        } else console.log(res)
    });
}


const GetJenisPendaftarans = (handleData, IdJenisPendaftaran) => {
    $.getJSON(base_url + "/api/webs/GetJenisPendaftarans", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if(v.Id == IdJenisPendaftaran)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                
                html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
            handleData(html);
            // $('select[name="IdJenisPendaftaran"]').html(html);
        } else console.log(res)
    });
}

const GetJalurPendaftarans = (handleData, IdJalurPendaftaran) => {
    $.getJSON(base_url + "/api/webs/GetJalurPendaftarans", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if(v.Id == IdJalurPendaftaran)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                
                html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
            handleData(html);
            // $('select[name="IdJalurPendaftaran"]').html(html);
        } else console.log(res)
    });
}

const ComboGetUnitSklh = (handleData, IdUnit) => {
    var Url = base_url + "/api/webs/GetUnits";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < 4; i++) {
                    if (responsesave.Data[i].Id == IdUnit) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected="true">' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

const ComboGetListJenjangPendidikan = (handleData, IdJenisPengurus) => {
    var Url = base_url + "/api/webs/GetListJenjangPendidikan";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdJenisPengurus) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

const ComboGetAgama = (handleData, IdAgama) => {
    var Url = base_url + "/api/webs/GetAgamas";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdAgama) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

const ComboGetJenisWargaNegara = (handleData, IdKewarganegaraan) => {
    var Url = base_url + "/api/webs/GetJenisWargaNegara";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdKewarganegaraan) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

const ComboGetTinggalSiswa = (handleData, IdTinggal) => {
    var Url = base_url + "/api/webs/GetTinggalSiswa";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdTinggal) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

const ComboGetJenisTransportasi = (handleData, IdTransportasi) => {
    var Url = base_url + "/api/webs/GetJenisTransportasi";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdTransportasi) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

const ComboGetJenisBahasa = (handleData, IdBahasa) => {
    var Url = base_url + "/api/webs/GetJenisBahasa";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdBahasa) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

const ComboGetSeragam = (handleData, IdPpdbSeragam) => {
    $.getJSON(base_url + "/api/webs/GetPpdbSeragams", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if(v.IdPpdbSeragam == IdPpdbSeragam)
                    html += '<option value="' + v.IdPpdbSeragam + '" selected>'+v.NamaSingkat + ' - ' + v.Nama + '</option>';
                
                html += '<option value="' + v.IdPpdbSeragam + '">'+v.NamaSingkat + ' - ' + v.Nama + '</option>';
            });
            handleData(html);
        } else console.log(res)
    });
}