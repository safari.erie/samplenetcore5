﻿var Url = base_url + "/api/Webs/GetMenus";

$.ajax({
    url: Url,
    type: "GET",
    dataType: "json",
    beforeSend: function (response) {
        ProgressBar("wait");
    },
    success: function (responseGetMenu) {
        ProgressBar("success");
        if (responseGetMenu.IsSuccess == true) {
            //JUDUL
            //$('title').html(responseGetMenu.Data.Judul);

            //LOGO
            //$('#logo_detil').html("<img src='" + base_url + "/unit/images/" + responseGetMenu.Data.Logo + "' width='70px' class='img-responsive'>");
            //$('#logo_index').html("<img src='" + base_url + "/unit/images/" + responseGetMenu.Data.Logo + "' width='70px' class='img-responsive'>");

            /// MENUS INDEX
            var htmlMenus = "";
            
            var jml_menu = (responseGetMenu.Data).length;
            htmlMenus += "<li><a class='dropdown-item' href='" + base_url + '/' + responseGetMenu.Data[0].ControllerName + '/' + responseGetMenu.Data[0].ActionName + "'>" + responseGetMenu.Data[0].ApplTaskName + "</a></li>";
            for (var imenu = 0; imenu < jml_menu; imenu++) {
                if (responseGetMenu.Data[imenu].IdApplTaskParent == null && responseGetMenu.Data[imenu].ApplTaskName != "Utama") {
                    if (responseGetMenu.Data[imenu].ApplTaskName == "PPDB") { }    
                    var cekPpdb = responseGetMenu.Data[imenu].ApplTaskName == `PPDB` ? `style="background:red"` : ``;
                    console.log(cekPpdb)
                    htmlMenus += "<li class='dropdown' "+cekPpdb+">                                                                                 " +
                        "    <a href='#' class='nav-link dropdown-toggle' data-toggle='dropdown'>" + responseGetMenu.Data[imenu].ApplTaskName + " <b class='caret'></b></a> " +
                        "    <ul class='dropdown-menu'>                                                                        ";

                        var jml_submenu = (responseGetMenu.Data).length;
                        for (var isubmenu = 0; isubmenu < jml_submenu; isubmenu++) {
                            if (responseGetMenu.Data[isubmenu].IdApplTaskParent != null) {
                                if (responseGetMenu.Data[imenu].IdApplTask == responseGetMenu.Data[isubmenu].IdApplTaskParent) {
                                    htmlMenus += "<li><a class='dropdown-item' href='" + base_url + '/' + responseGetMenu.Data[isubmenu].ControllerName + '/' + responseGetMenu.Data[isubmenu].ActionName + "'>" + responseGetMenu.Data[isubmenu].ApplTaskName + "</a></li> ";
                                }
                            }
                        }


                    htmlMenus += "    </ul>                                                                                             " +
                            "</li>                                                                                                          ";
                }
            }
            //htmlMenus += "<li><a class='" + responseGetMenu.Data[4].ClassName + "' href='" + base_url + '/' + responseGetMenu.Data[4].ControllerName + '/' + responseGetMenu.Data[4].ActionName + "'>" + responseGetMenu.Data[4].ApplTaskName + "</a></li>";
            $("#Menus").html(htmlMenus);

            
        } else if (responseGetMenu.IsSuccess == true) {
        }
    }, error: function (response, a, e) {
        ProgressBar("success");
        swal({ title: 'Error :(', text: JSON.stringify(response) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
    }
});