﻿var getSesi = window.sessionStorage["banner"];
//$('#Banner').modal("show");
$.ajax({
    url: base_url + "/Webs/GetHome",
    method: "GET",
    dataType: "json",
    beforeSend: function () {
        ProgressBar("wait");
    },
    success: function (responseGetHome) {
        ProgressBar("success");
        if (responseGetHome.IsSuccess == true) {
            // BANNER
            $('#gambar_banner').html('<img src="' + base_url + '/unit/images/' + responseGetHome.Data.Banner + '" class="img-responsive" />');
            var geturl = window.location.pathname;
            if (geturl == folder + "/Home/Index") {
                if (responseGetHome.Data.Banner == "") {
                    $('#Banner').modal("hide");
                } else {
                    if (getSesi == undefined) {
                        //tampil 
                        $('#Banner').modal("show");
                    } else {
                        // create session        
                        $('#Banner').modal("hide");
                    }
                }
            } else if (geturl == folder + "/") {
                if (responseGetHome.Data.Banner == "") {
                    $('#Banner').modal("hide");
                } else {
                    if (getSesi == undefined) {
                        //tampil 
                        $('#Banner').modal("show");
                    } else {
                        // create session        
                        $('#Banner').modal("hide");
                    }
                }
            } else if (geturl == folder + "") {
                if (responseGetHome.Data.Banner == "") {
                    $('#Banner').modal("hide");
                } else {
                    if (getSesi == undefined) {
                        //tampil 
                        $('#Banner').modal("show");
                    } else {
                        // create session        
                        $('#Banner').modal("hide");
                    }
                }
            }

            //NAMA
            $('#NamaWeb').html(responseGetHome.Data.Nama);
            //MOTO
            $('#Moto').html(responseGetHome.Data.Moto);
            //VIDEOS
            $('#video_slide').html("<source src='" + base_url + "/unit/videos/" + responseGetHome.Data.Video + "' type='video/mp4'>");
            //WALPAPER MOBILE
            $('#image_mobile').html('<img src="' + base_url + '/unit/images/' + responseGetHome.Data.WebWallPaper + '" />');
            //PPDB ACTIVE/NOT
            if (responseGetHome.Data.IsPpdb == true) {
                $('#tentang-atq').css('display', 'none');
                $('#mobile_pendaftaran').removeAttr('style');
                $('#pendaftaran-online').removeAttr('style');

                $('#daftar_form_off').css('display', 'none');
                $('#daftar_form_on').removeAttr('style');
            } else if (responseGetHome.Data.IsPpdb == false) {
                $('#tentang-atq').removeAttr('style');
                $('#mobile_tentang').css('display', 'none');
                $('#pendaftaran-online').css('display', 'none');

                $('#daftar_form_off').removeAttr('style');
                $('#daftar_form_on').css('display', 'none');
            }
            //UNIT MOTTO
            $('#nama_tk').html(responseGetHome.Data.Units[0].Nama);
            $('#moto_tk').html(responseGetHome.Data.Units[0].Motto);
            $('#nama_sd').html(responseGetHome.Data.Units[1].Nama);
            $('#moto_sd').html(responseGetHome.Data.Units[1].Motto);
            $('#nama_smp').html(responseGetHome.Data.Units[2].Nama);
            $('#moto_smp').html(responseGetHome.Data.Units[2].Motto);
            $('#nama_sma').html(responseGetHome.Data.Units[3].Nama);
            $('#moto_sma').html(responseGetHome.Data.Units[3].Motto);
        }
        else if (responseGetHome.IsSuccess == true) {

        }
    }
});

function TutupBanner() {
    window.sessionStorage["banner"] = "createsesi_save_banner";
    $('#Banner').modal("hide");
}