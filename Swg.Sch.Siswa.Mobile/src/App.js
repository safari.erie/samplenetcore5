import React, {useEffect} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Router from './router';
import { Provider } from 'react-redux';
import { LogBox } from 'react-native';
import store from './config/redux/store';
import { navigationRef } from './router/RootNavigation';
import OneSignal from 'react-native-onesignal';
import SplashScreen from  "react-native-splash-screen";

const App = () => {
    LogBox.ignoreLogs([
        'Remote debugger is in a background tab which may cause apps to perform slowly',
        'Require cycle: node_modules/rn-fetch-blob/index.js',
        'Require cycle: node_modules/react-native/Libraries/Network/fetch.js'
    ]);
    
    useEffect(() => {
        OneSignal.init('AppId')
        OneSignal.inFocusDisplaying(2);
        SplashScreen.hide();
    }, []);

    const deppLinking = {
        prefixes: ['https://siswa.sekolahattaufiq.id'],
        config: {
            Masuk: 'user/login',
            DaftarMapel: 'Siswa/Learning',
            Mutabaah: 'Siswa/MutabaahSiswa',
            Qiroaty: 'Siswa/KelompokQiroaty',
            Rapor: 'Siswa/Raport',
            Checkout: {
                path: 'Checkout/:ok',
                params: {
                    ok: null,
                },
            },
        },
    };
    return (
        <>
        <Provider store={store}>
            <NavigationContainer ref={navigationRef} linking={deppLinking}>
            <Router />
            </NavigationContainer>
        </Provider>
        </>
    );
};

export default App;
