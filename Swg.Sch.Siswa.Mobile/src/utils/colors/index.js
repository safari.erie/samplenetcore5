const mainColors = {
    blue1: '#FF521F',
    blue2: '#003F88',
    blue3: '#00296B',
    yellow1: '#FA9E2C', //change
    yellow2: '#FDC500',
    dark1: '#737373',
    dark2: '#B9B9B9',
    dark3: '#737373AD',
    
    orange: '#f68104',
    orange1: '#FECD98',
    orange2: '#fdf1e8',
    darkmode1: '#2f2f2f',
    darkmode2: '#222526',
}


export const colors = {
    primary: mainColors.orange,
    secondary: mainColors.orange1,
    secondary2: mainColors.orange2,
    darkmode1: mainColors.darkmode1,
    darkmode2: mainColors.darkmode2,
    dark1: mainColors.dark1,
    dark2: mainColors.dark2,
    yellow1: mainColors.yellow1,
    yellow2: mainColors.yellow2,
    white: 'white',
    black: 'black',
    text: {
        primary: mainColors.dark1,
        secondary: mainColors.grey1,
        menuInactive: mainColors.dark2,
        menuActive: mainColors.yellow1,
    },
    button: {
        primary: {
            background: mainColors.yellow1,
            text: 'white',
        },
        secondary: {
            background: 'white',
            text: mainColors.yellow1,
        },
    },
    border: mainColors.blue1,
    cardLight: mainColors.blue2,
}