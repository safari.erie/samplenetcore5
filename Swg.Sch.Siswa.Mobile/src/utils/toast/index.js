import Tips from 'react-native-root-tips';
import { colors } from '../colors';

export const toast = (status, { ...obj }) => {
    if (status === "SUCCESS") {
        Tips.showSuccess(obj.msg, {
            duration: obj.duration === undefined ? 2000 : obj.duration,
            position: Tips.positions.CENTER,
            shadow: false,
            animation: true,
            hideOnPress: false,
            mask: true,
            maskColor: colors.black,
            delay: 0
        });
    } 
    if (status === "FAIL") {
        Tips.showFail(obj.msg, {
            duration: 2000,
            position: Tips.positions.CENTER,
            shadow: false,
            animation: true,
            hideOnPress: false,
            mask: true,
            maskColor: colors.black,
            delay: 0
        });
    } 
    if (status === "WARN") {
        Tips.showWarn(obj.msg, {
            duration: 2000,
            position: Tips.positions.CENTER,
            shadow: false,
            animation: true,
            hideOnPress: false,
            mask: true,
            maskColor: colors.black,
            delay: 0
        });
    } 
    if (status === "INFO") {
        Tips.showInfo(obj.msg, {
            duration: obj.duration === undefined ? 2000 : obj.duration,
            position: Tips.positions.CENTER,
            shadow: false,
            animation: true,
            hideOnPress: false,
            mask: true,
            maskColor: colors.black,
            delay: 0
        });
    } 
    if (status === "LOADING") {
        Tips.showLoading(obj.msg, {
            duration: obj.duration === undefined ? 2000 : obj.duration,
            position: Tips.positions.CENTER,
            shadow: false,
            animation: true,
            hideOnPress: false,
            mask: true,
            maskColor: colors.black,
            delay: 0
        });
    } 
   
}