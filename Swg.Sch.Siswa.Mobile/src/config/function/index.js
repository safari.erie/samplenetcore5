import { BackHandler, PermissionsAndroid } from 'react-native';
import Tips from 'react-native-root-tips';
import RNFetchBlob from 'rn-fetch-blob';
import { colors } from '../../utils/colors';

export const FUNCAngkaToRupiah = (num) => {
    var numx = num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
    return numx;
}

export const FUNCToast = (status, { ...obj }) => {
    if (status === "SUCCESS") {
        Tips.showSuccess(obj.msg, {
            duration: obj.duration === undefined ? 2000 : obj.duration,
            position: Tips.positions.CENTER,
            shadow: false,
            animation: true,
            hideOnPress: false,
            mask: true,
            maskColor: colors.black,
            delay: 0
        });
    } 
    if (status === "FAIL") {
        Tips.showFail(obj.msg, {
            duration: 2000,
            position: Tips.positions.CENTER,
            shadow: false,
            animation: true,
            hideOnPress: false,
            mask: true,
            maskColor: colors.black,
            delay: 0
        });
    } 
    if (status === "WARN") {
        Tips.showWarn(obj.msg, {
            duration: obj.duration === undefined ? 2000 : obj.duration,
            position: Tips.positions.CENTER,
            shadow: false,
            animation: true,
            hideOnPress: false,
            mask: true,
            maskColor: colors.black,
            delay: 0
        });
    } 
    if (status === "INFO") {
        Tips.showInfo(obj.msg, {
            duration: obj.duration === undefined ? 2000 : obj.duration,
            position: Tips.positions.CENTER,
            shadow: false,
            animation: true,
            hideOnPress: false,
            mask: true,
            maskColor: colors.black,
            delay: 0
        });
    } 
    if (status === "LOADING") {
        Tips.showLoading(obj.msg, {
            duration: obj.duration === undefined ? 30000 : obj.duration,
            position: Tips.positions.CENTER,
            shadow: false,
            animation: true,
            hideOnPress: false,
            mask: true,
            maskColor: colors.black,
            delay: 0
        });
    }
    if (status === "HIDE") {
        Tips.hide();
    }
   
}

export const FUNCDateToString = (date) => {
    var Date = date.getDate();
    var Month = date.getMonth() + 1; 
    var Year = date.getFullYear();
    if (Date < 10) Date = "0" + Date;
    if (Month < 10) Month = "0" + Month;
    return Date + "-" + Month + "-" + Year;
}

export const FUNCGetExtention = (fileUrl) => {
    return /[.]/.exec(fileUrl) ?
                /[^.]+$/.exec(fileUrl) : undefined;
}

export const FUNCGetExtentionYt = (fileUrl) => {
    return /[/]/.exec(fileUrl) ?
                /[^/]+$/.exec(fileUrl) : undefined;
}

export const FUNCCheckPermission = async (isAction) => {
    const granted = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
            title: 'Storage Permission Required',
            message:
                'Application needs access to your storage to Download File',
            buttonPositive: "OK"
        }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // console.log('Storage Permission Granted.');
    } else {
        FUNCToast('FAIL', { msg: 'Izinkan aplikasi untuk akses penyimpanan' })
    }
    var grantedx = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.CAMERA,
        {
            title: "Camera Permission Required",
            message:"Application needs access to your upload picture ",
            buttonPositive: "OK"
        }
    );
    if (grantedx === PermissionsAndroid.RESULTS.GRANTED) {
        // console.log('Camera Permission Granted.');
    } else {
        FUNCToast('FAIL', { msg: 'Izinkan aplikasi untuk akses kamera' })
    }
}

export const FUNCDownloadFile = async (fileUrl) => {
    try {
        FUNCToast('LOADING', {msg: 'memuat file...'})
        let date = new Date();
        let FILE_URL = fileUrl;    
        let file_ext = FUNCGetExtention(FILE_URL);
    
        file_ext = '.' + file_ext[0];
    
        const { config, fs } = RNFetchBlob;
        let RootDir = `${fs.dirs.PictureDir}`;
        let options = {
            fileCache: true,
            addAndroidDownloads: {
                path:
                RootDir+
                '/file_' + 
                Math.floor(date.getTime() + date.getSeconds() / 2) +
                file_ext,
                description: 'downloading file...',
                notification: true,
                useDownloadManager: true,   
            },
        };
        config(options)
        .fetch('GET', FILE_URL)
            .then(res => {
                FUNCToast('SUCCESS', { msg: `File Downloaded Successfully ${Platform.OS !== 'ios' ? '\n\n' + options.addAndroidDownloads.path : ''}` })
            });
    } catch (e) {
        console.log(`😱 Axios request failed: ${e}`);
    }
}

let backHandlerClickCount = 0;
export const FUNCBackHandleClose = () => {
    const shortToast = message => {
        Tips.show(message, {
            duration: 1000,
            position: Tips.positions.BOTTOM,
            shadow: false,
            animation: true,
            hideOnPress: true,
            delay: 0
        });
    }
    backHandlerClickCount += 1;
    if ((backHandlerClickCount < 2)) {
        shortToast('Tekan sekali lagi untuk keluar');
    } else {
        BackHandler.exitApp();
    }
    setTimeout(() => {
        backHandlerClickCount = 0;
    }, 1000);
    return true;
}