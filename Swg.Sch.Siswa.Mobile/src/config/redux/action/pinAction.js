import { FUNCToast } from '../../function';
import { BASEAPI, BASEURL, http } from '../../helpers';
import { setDataProfil } from './profilAction';
import { setDataTagihans } from './tagihanAction';

export const setDataPin = (pin, navigation, pinFor) => {
    FUNCToast('LOADING', {msg: 'sedang memeriksa...'})
    return async (dispatch) => {
        let isTrue = false;
        await http.get(`${BASEAPI}/Mobiles/ValidasiPin?Pin=${pin}`).then(res => {
            let data = res.data;
            if (data.IsSuccess) {
                FUNCToast('SUCCESS', { msg: 'PIN Valid!' });
                dispatch({ type: 'DATA_PIN', payload: '' });
                if (pinFor === 'tagihan')
                    dispatch(setDataTagihans(navigation))
                if (pinFor === 'profil')
                    dispatch(setDataProfil(navigation))
                if (pinFor === 'ubah pin')
                    navigation.navigate('UbahPin');
                if (pinFor === 'ubah password')
                    navigation.navigate('UbahPassword');
                if (pinFor === 'rapor') {
                    navigation.navigate('Rapor');
                    FUNCToast('WARN', { msg: 'Mohon maaf, Rapor belum dapat diakses, silakan menghubungi bagian Keuangan. Terimakasih!' });
                }
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
                dispatch({ type: 'DATA_PIN', payload: '' });
            }
        })
    }
}

export const apiResetPin = () => {
    FUNCToast('LOADING', {msg: 'sedang memuat...'})
    return async (dispatch) => {
        let isTrue = false;
        await http.get(`${BASEAPI}/Mobiles/ResetPin`).then(res => {
            let data = res.data;
            if (data.IsSuccess) {
                FUNCToast('SUCCESS', { msg: 'PIN Baru berhasil terkirim!' });
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        })
    }
}