import { FUNCToast } from '../../function';
import { BASEAPI, BASEURL, http } from '../../helpers';

export const setDataProfil = (navigation) => {
    return async (dispatch) => {
        await http.get(`${BASEAPI}/Mobiles/GetSiswa`).then(res => {
            let data = res.data;
            if (data.IsSuccess) {
                dispatch({ type: 'DATA_PROFIL', payload: data.Data });
                dispatch(setFormProfil('Email', data.Data.Email));
                dispatch(setFormProfil('EmailOrtu', data.Data.EmailOrtu));
                dispatch(setFormProfil('NoHandphone', data.Data.NoHandphone));
                dispatch(setFormProfil('FotoProfile', {
                    uri: data.Data.FotoProfile === null ? '' : `${BASEURL}/Asset/Files/Siswa/Foto/${data.Data.FotoProfile}`
                }));
                dispatch(setFormProfil('IsFotoProfile', false));
                navigation.navigate('Profil');
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        }).catch(err => {
            FUNCToast('FAIL', { msg: `${JSON.stringify(err)}` });
        })
        FUNCToast('HIDE');
    }
}

export const setFormProfil = (formType, formValue) => {
    return {type: 'FORM_PROFIL', formType, formValue}
}


export const apiEditProfilSiswa = (iData) => {
    return () => {
        console.log(iData)
        FUNCToast('LOADING', { msg: 'sedang memuat...' });
        var fd = new FormData();
        fd.append('Email', iData.Email);
        fd.append('EmailOrtu', iData.EmailOrtu);
        fd.append('NoHandphone', iData.NoHandphone);
        if(iData.IsFotoProfile === true)
            fd.append('FotoProfile', iData.FotoProfile);

        http.post(`${BASEAPI}/Mobiles/EditProfilSiswa`, fd, {
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        }).then(res => {
            let data = res.data;
            if (data.IsSuccess) {
                FUNCToast('SUCCESS', { msg: 'Berhasil perbarui data profil' });
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        })
    }
}