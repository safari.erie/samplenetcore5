import AsyncStorage from '@react-native-async-storage/async-storage';
import { FUNCToast } from '../../function';
import { BASEAPI, BASEURL, http } from '../../helpers';

export const setFormUbahPassword = (formType, formValue) => {
    return {type: 'FORM_UBAH_PASSWORD', formType, formValue}
}

export const apiGantiPassword = (iData, navigation) => {
    return async (dispatch) => {
        FUNCToast('LOADING', { msg: 'sedang memuat...' });
        await http.get(`${BASEAPI}/Mobiles/GantiPassword?OldPassword=${iData.OldPassword}&NewPassword1=${iData.NewPassword1}&NewPassword2=${iData.NewPassword2}`).then(res => {
            let data = res.data;
            if (data.IsSuccess) {
                FUNCToast('SUCCESS', { msg: 'Berhasil perbarui password' });
                let keys = ['@token', '@user'];
                AsyncStorage.multiRemove(keys, (err) => {
                    setTimeout(() => {
                        navigation.replace('Splash')
                    }, 1000);
                })
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        })
    }
}