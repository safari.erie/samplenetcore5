import { FUNCDateToString, FUNCToast } from '../../function';
import { BASEAPI, http } from '../../helpers';


export const setDataElearnings = (tanggal) => {
    return (dispatch) => {
        dispatch({ type: 'DATA_ELEARNINGS', payload: false });
        http.get(`${BASEAPI}/Mobiles/GetKbmBySiswas?Tanggal=${tanggal}`).then((res) => {
            let data = res.data;
            let senin = [];
            let selasa = [];
            let rabu = [];
            let kamis = [];
            let jumat = [];
            if (data.IsSuccess) {
               
                data.Data.map((v, i) => {
                    switch (v.Hari) {
                        case 'Senin':
                            senin.push(v);
                            break;
                        case 'Selasa':
                            selasa.push(v);
                            break;
                        case 'Rabu':
                            rabu.push(v);
                            break;
                        case 'Kamis':
                            kamis.push(v);
                            break;
                        case "Jum'at":
                            jumat.push(v);
                            break;
                        default:
                    }
                });
                
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }

            const dataElearnings = {
                senin : senin,
                selasa : selasa,
                rabu : rabu,
                kamis : kamis,
                jumat : jumat,
            }
            dispatch({ type: 'DATA_ELEARNINGS', payload: dataElearnings });

        })
    }
}

export const setDataElearning = (idKbmMateri) => {
    return (dispatch) => {
        dispatch({ type: 'DATA_ELEARNING', payload: false });
        http.get(`${BASEAPI}/Mobiles/GetKbmBySiswa?IdKbmMateri=${idKbmMateri}`).then((res) => {
            let data = res.data;
            if (data.IsSuccess) {
                dispatch({ type: 'DATA_ELEARNING', payload: data.Data });
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        })
    }
}

export const setDataElearningByDays = () => {
    return (dispatch) => {
        dispatch({ type: 'DATA_ELEARNING_BY_DAYS', payload: false });
        http.get(`${BASEAPI}/Mobiles/GetKbmBySiswas?Tanggal=${FUNCDateToString(new Date())}`).then((res) => {
        // http.get(`${BASEAPI}/Mobiles/GetKbmBySiswas?Tanggal=${'17-08-2020'}`).then((res) => {
            let data = res.data;
            let senin = [];
            let selasa = [];
            let rabu = [];
            let kamis = [];
            let jumat = [];
            if (data.IsSuccess) {
               
                data.Data.map((v, i) => {
                    switch (v.Hari) {
                        case 'Senin':
                            senin.push(v);
                            break;
                        case 'Selasa':
                            selasa.push(v);
                            break;
                        case 'Rabu':
                            rabu.push(v);
                            break;
                        case 'Kamis':
                            kamis.push(v);
                            break;
                        case "Jum'at":
                            jumat.push(v);
                            break;
                        default:
                    }
                });
                
            } else {
                // FUNCToast('WARN', { msg: data.ReturnMessage });
            }

            var dataElearningByDays = [];
            const dataElearnings = {
                senin : senin,
                selasa : selasa,
                rabu : rabu,
                kamis : kamis,
                jumat : jumat,
            }

            var days = ['Ahad', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
            var d = new Date();
            var dayName = days[d.getDay()];
            if (dayName === 'Ahad') dataElearningByDays = dataElearnings.senin;
            if (dayName === 'Senin') dataElearningByDays = dataElearnings.senin;
            if (dayName === 'Selasa') dataElearningByDays = dataElearnings.selasa;
            if (dayName === 'Rabu') dataElearningByDays = dataElearnings.rabu;
            if (dayName === 'Kamis') dataElearningByDays = dataElearnings.kamis;
            if (dayName === 'Jumat') dataElearningByDays = dataElearnings.jumat;
            if (dayName === 'Sabtu') dataElearningByDays = dataElearnings.senin;

            dispatch({ type: 'DATA_ELEARNING_BY_DAYS', payload: dataElearningByDays });

        })
    }
}

export const setDataJenisBantuanOrtus = () => {
    return (dispatch) => {
        http.get(`${BASEAPI}/Mobiles/GetJenisBantuan`).then(res => {
            let data = res.data;
            if (data.IsSuccess) {
                dispatch({ type: 'DATA_JENIS_BANTUAN_ORTUS', payload: data.Data });
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        })
    }
}

export const setFormJawaban = (formType, formValue) => {
    return {type: 'FORM_ELEARNING', formType, formValue}
}

export const apiUploadJawaban = (idKbmMateri, idJenisSoal, idJenisBantuanOrtu, namaUrl, fileUpload) => {
    return (dispatch) => {
        FUNCToast('LOADING', { msg: 'sedang memuat...' });

        var fd = new FormData();
        fd.append('IdKbmMateri', idKbmMateri);
        fd.append('IdJenisSoal', idJenisSoal);
        fd.append('IdJenisBantuanOrtu', idJenisBantuanOrtu);
        fd.append('NamaUrl', namaUrl);
        
        fileUpload.map((v, i) => {
            fd.append(`FileUpload`, v);
        });

        http.post(`${BASEAPI}/Mobiles/UploadJawabans`, fd, {
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        }).then(res => {
            let data = res.data;
            if (data.IsSuccess) {
                FUNCToast('SUCCESS', { msg: 'Berhasil menyerahkan tugas' });
                dispatch(setDataElearning(idKbmMateri));
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage, duration: 4000 });
            }
            dispatch(setFormJawaban('FileUpload', []));
        })
    }
}