import Axios from 'axios';
import { FUNCToast } from '../../function';
import { BASEAPI, BASEURL, http, VERSIAPP } from '../../helpers';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Alert, BackHandler, Linking } from "react-native";

export const setFormLogin = (formType, formValue) => {
    return {type: 'FORM_LOGIN', formType, formValue}
}

export const apiMobileLogin = (iData, navigation) => {
    return (dispatch) => {
        FUNCToast('LOADING', { msg: 'sedang memuat...' });
        Axios.get(`${BASEURL}/Auth/MobileLogin?Nis=${iData.Nis}&Password=${iData.Password}`).then(res => {
            let data = res.data;
            if (data.IsSuccess) {
                setToken(data.Data)
                dispatch(apiGetInfoUser(navigation))
                apiToken(iData.TokenNotif);
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        }).catch(err => {
            FUNCToast('FAIL', { msg: JSON.stringify(err.message) });
        })
    }
}

const apiToken = (token) => {
    http.get(`${BASEAPI}/Mobiles/UpdateTokenFcmSiswa?Token=${token}&UserAgent=`).then(res => {
        let data = res.data;
    })
}

export const apiLupaPass = (iData) => {
    return (dispatch) => {
        FUNCToast('LOADING', { msg: 'sedang memuat...' });
        Axios.get(`${BASEURL}/Auth/SiswaResetPassword?Nis=${iData.NisLupaPass}`).then(res => {
            let data = res.data;
            if (data.IsSuccess) {
                FUNCToast('SUCCESS', { msg: 'Berhasil reset password, silahkan cek email '+ data.Data });
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        }).catch(err => {
            FUNCToast('FAIL', { msg: JSON.stringify(err.message) });
        })
    }
}

export const apiGetInfoUser = (navigation) => {
    return (dispatch) => {
        FUNCToast('LOADING', { msg: 'sedang memuat...' });
        http.get(`${BASEAPI}/Mobiles/GetInfoUser`).then(res => {
            let data = res.data;
            if (data.IsSuccess) {
                dispatch({ type: 'DATA_USER', payload: data.Data });
                dispatch({ type: 'DATA_USER_PROFIL', payload: data.Data.FotoProfile === null ? '' : `${BASEURL}/Asset/Files/Siswa/Foto/${data.Data.FotoProfile}?n=${Math.random()}` });
                navigation.replace('MainApp');
                setInfoUser(JSON.stringify(data.Data));
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        })
    }
}

export const apiGetInfoUserHome = () => {
    return (dispatch) => {
        http.get(`${BASEAPI}/Mobiles/GetInfoUser`).then(res => {
            let data = res.data;
            if (data.IsSuccess) {
                dispatch({ type: 'DATA_USER', payload: data.Data });
                dispatch({ type: 'DATA_USER_PROFIL', payload: data.Data.FotoProfile === null ? '' : `${BASEURL}/Asset/Files/Siswa/Foto/${data.Data.FotoProfile}?n=${Math.random()}` });
                setInfoUser(JSON.stringify(data.Data));
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        })
    }
}

export const apiCekVersi = () => {
    return (dispatch) => {
        http.get(`${BASEAPI}/Mobiles/CekVersi?Versi=${VERSIAPP}`).then(res => {
            let data = res.data;
            if (!data.IsSuccess) {
                Alert.alert(
                "Pembaruan Tersedia",
                "Segera lakukan pembaruan aplikasi!",
                [
                    {
                        text: "TUTUP",
                        onPress: () => BackHandler.exitApp(),
                        style: "cancel"
                    },
                    { text: "UPDATE", onPress: () => [BackHandler.exitApp(), Linking.openURL("market://details?id=com.sekolahattaufiq.siswa")] }
                ]
                );
            } else {

            }
        })
    }
}

export const apiGetBanner = () => {
    return (dispatch) => {
        dispatch({ type: 'DATA_BANNER', payload: '' });
        http.get(`${BASEAPI}/Mobiles/GetBanners`).then(res => {
            let data = res.data;
            if (data.IsSuccess) {
                // ${BASEURL}/Asset/Files/Appl/File/${data.Data}?n=${Math.random()}
                dispatch({ type: 'DATA_BANNER', payload: data.Data });
            } else {
                dispatch({ type: 'DATA_BANNER', payload: false });
                // FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        })
    }
}

const setToken = async (token) => {
    try {
        await AsyncStorage.setItem('@token', token)
        FUNCToast('SUCCESS', { msg: 'Berhasil login akun!' });
    } catch (e) {
        FUNCToast('FAIL', { msg: e });
    }
}
const setInfoUser = async (user) => {
    try {
        await AsyncStorage.setItem('@user', user)
        FUNCToast('HIDE')
    } catch (e) {
        FUNCToast('FAIL', { msg: e });
    }
}

