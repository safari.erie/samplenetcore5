import AsyncStorage from '@react-native-async-storage/async-storage';
import { FUNCToast } from '../../function';
import { BASEAPI, BASEURL, http } from '../../helpers';

export const setFormUbahPin = (formType, formValue) => {
    return {type: 'FORM_UBAH_PIN', formType, formValue}
}

export const apiGantiPin = (iData, navigation) => {
    return (dispatch) => {
        FUNCToast('LOADING', { msg: 'sedang memuat...' });
        http.get(`${BASEAPI}/Mobiles/GantiPin?PinLama=${iData.PinLama}&PinBaru=${iData.PinBaru}`).then(res => {
            let data = res.data;
            dispatch(setFormUbahPin('PinLama', ''));
            dispatch(setFormUbahPin('PinBaru', ''));
            if (data.IsSuccess) {
                FUNCToast('SUCCESS', { msg: 'Berhasil perbarui pin' });
                let keys = ['@token', '@user'];
                AsyncStorage.multiRemove(keys, (err) => {
                    setTimeout(() => {
                        navigation.replace('Splash')
                    }, 1000);
                })
                
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        })
    }
}