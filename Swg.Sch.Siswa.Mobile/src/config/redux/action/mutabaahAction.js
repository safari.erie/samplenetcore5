import { FUNCToast } from '../../function';
import { BASEAPI, http } from '../../helpers';


export const setDataMutabaahs = (tanggal) => {
    return (dispatch) => {
        dispatch({ type: 'DATA_MUTABAAHS', payload: false });
        http.get(`${BASEAPI}/Mobiles/GetKbmEvalHarians?Tanggal=${tanggal}`).then((res) => {
            let data = res.data;
            
            if (data.IsSuccess) {
                let minggu = [];
                let senin = [];
                let selasa = [];
                let rabu = [];
                let kamis = [];
                let jumat = [];
                let sabtu = [];
                let mingguHome = [];
                let seninHome = [];
                let selasaHome = [];
                let rabuHome = [];
                let kamisHome = [];
                let jumatHome = [];
                let sabtuHome = [];
                var tglMulai = '';
                var tglSelesai = '';
                var tglAhad = '';
                var tglSenin = '';
                var tglSelasa = '';
                var tglRabu = '';
                var tglKamis = '';
                var tglJumat = '';
                var tglSabtu = '';
                data.Data.map((v, i) => {
                    tglMulai = v.HariKe1Tanggal;
                    tglSelesai = v.HariKe7Tanggal;

                    tglAhad = v.HariKe1Tanggal;
                    tglSenin = v.HariKe2Tanggal;
                    tglSelasa = v.HariKe3Tanggal;
                    tglRabu = v.HariKe4Tanggal;
                    tglKamis = v.HariKe5Tanggal;
                    tglJumat = v.HariKe6Tanggal;
                    tglSabtu = v.HariKe7Tanggal;

                    minggu.push({
                        idJenisEvaluasi: v.IdJenisEvaluasi,
                        jenisEvaluasi: v.JenisEvaluasi,
                        isDone: v.HariKe1,
                        tanggal: v.HariKe1Tanggal
                    })
                    senin.push({
                        idJenisEvaluasi: v.IdJenisEvaluasi,
                        jenisEvaluasi: v.JenisEvaluasi,
                        isDone: v.HariKe2,
                        tanggal: v.HariKe2Tanggal
                    })
                    selasa.push({
                        idJenisEvaluasi: v.IdJenisEvaluasi,
                        jenisEvaluasi: v.JenisEvaluasi,
                        isDone: v.HariKe3,
                        tanggal: v.HariKe3Tanggal
                    })
                    rabu.push({
                        idJenisEvaluasi: v.IdJenisEvaluasi,
                        jenisEvaluasi: v.JenisEvaluasi,
                        isDone: v.HariKe4,
                        tanggal: v.HariKe4Tanggal
                    })
                    kamis.push({
                        idJenisEvaluasi: v.IdJenisEvaluasi,
                        jenisEvaluasi: v.JenisEvaluasi,
                        isDone: v.HariKe5,
                        tanggal: v.HariKe5Tanggal
                    })
                    jumat.push({
                        idJenisEvaluasi: v.IdJenisEvaluasi,
                        jenisEvaluasi: v.JenisEvaluasi,
                        isDone: v.HariKe6,
                        tanggal: v.HariKe6Tanggal
                    })
                    sabtu.push({
                        idJenisEvaluasi: v.IdJenisEvaluasi,
                        jenisEvaluasi: v.JenisEvaluasi,
                        isDone: v.HariKe7,
                        tanggal: v.HariKe7Tanggal
                    })

                    if (v.HariKe1 !== "Y") {
                        mingguHome.push({
                            idJenisEvaluasi: v.IdJenisEvaluasi,
                            jenisEvaluasi: v.JenisEvaluasi,
                            isDone: v.HariKe1,
                            tanggal: v.HariKe1Tanggal
                        })
                    }
                    if (v.HariKe2 !== "Y") {
                        seninHome.push({
                            idJenisEvaluasi: v.IdJenisEvaluasi,
                            jenisEvaluasi: v.JenisEvaluasi,
                            isDone: v.HariKe2,
                            tanggal: v.HariKe2Tanggal
                        })
                    }
                    if (v.HariKe3 !== "Y") {
                        selasaHome.push({
                            idJenisEvaluasi: v.IdJenisEvaluasi,
                            jenisEvaluasi: v.JenisEvaluasi,
                            isDone: v.HariKe3,
                            tanggal: v.HariKe3Tanggal
                        })
                    }
                    if (v.HariKe4 !== "Y") {
                        rabuHome.push({
                            idJenisEvaluasi: v.IdJenisEvaluasi,
                            jenisEvaluasi: v.JenisEvaluasi,
                            isDone: v.HariKe4,
                            tanggal: v.HariKe4Tanggal
                        })
                    }
                    if (v.HariKe5 !== "Y") {
                        kamisHome.push({
                            idJenisEvaluasi: v.IdJenisEvaluasi,
                            jenisEvaluasi: v.JenisEvaluasi,
                            isDone: v.HariKe5,
                            tanggal: v.HariKe5Tanggal
                        })
                    }
                    if (v.HariKe6 !== "Y") {
                        jumatHome.push({
                            idJenisEvaluasi: v.IdJenisEvaluasi,
                            jenisEvaluasi: v.JenisEvaluasi,
                            isDone: v.HariKe6,
                            tanggal: v.HariKe6Tanggal
                        })
                    }
                    if (v.HariKe7 !== "Y") {
                        sabtuHome.push({
                            idJenisEvaluasi: v.IdJenisEvaluasi,
                            jenisEvaluasi: v.JenisEvaluasi,
                            isDone: v.HariKe7,
                            tanggal: v.HariKe7Tanggal
                        })
                    }
                });
                
                var byDay = [];
                var days = ['Ahad', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
                var d = new Date();
                var dayName = days[d.getDay()];
                if (dayName === 'Ahad') byDay = mingguHome;
                if (dayName === 'Senin') byDay = seninHome;
                if (dayName === 'Selasa') byDay = selasaHome;
                if (dayName === 'Rabu') byDay = rabuHome;
                if (dayName === 'Kamis') byDay = kamisHome;
                if (dayName === 'Jumat') byDay = jumatHome;
                if (dayName === 'Sabtu') byDay = sabtuHome;

                    
                const dataMutabaahs = {
                    tglMulai: tglMulai,
                    tglSelesai: tglSelesai,

                    tglAhad : tglAhad,
                    tglSenin : tglSenin,
                    tglSelasa : tglSelasa,
                    tglRabu : tglRabu,
                    tglKamis : tglKamis,
                    tglJumat : tglJumat,
                    tglSabtu : tglSabtu,
                    
                    minggu : minggu,
                    senin : senin,
                    selasa : selasa,
                    rabu : rabu,
                    kamis : kamis,
                    jumat : jumat,
                    sabtu: sabtu,
                    byDay: byDay,
                }
                dispatch({ type: 'DATA_MUTABAAHS', payload: dataMutabaahs });
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        })
    }
}

export const apiAddMutabaah = (iData,date) => {
    return (dispatch) => {
        FUNCToast('LOADING', { msg: 'sedang memuat...' });

        var fd = new FormData();
        iData.map((v, i) => {
            fd.append(`Data[${i}].IdJenisEvaluasiHarian`, v.idJenisEvaluasi);
            fd.append(`Data[${i}].Tanggal`, v.tanggal);
            fd.append(`Data[${i}].IsDone`, v.isDone || 'T');
        });
        http.post(`${BASEAPI}/Mobiles/EvaluasiHarianAdd`, fd, {
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        }).then(res => {
            let data = res.data;
            if (data.IsSuccess) {
                FUNCToast('SUCCESS', { msg: 'Berhasil simpan mutabaah' });
                dispatch(setDataMutabaahs(date))
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        })
    }
}