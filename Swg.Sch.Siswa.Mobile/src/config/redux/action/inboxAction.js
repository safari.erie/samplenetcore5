import { FUNCToast } from '../../function';
import { BASEAPI, http } from '../../helpers';

export const setDataInboxs = () => {
    return (dispatch) => {
        dispatch({ type: 'DATA_INBOXS', payload: false });
        http.get(`${BASEAPI}/Mobiles/GetInboxSiswas`).then((res) => {
            let data = res.data;
            if (data.IsSuccess) {
                dispatch({ type: 'DATA_INBOXS', payload: data.Data });

                let isRead = 0;
                data.Data.map((v, i) => {
                    if (!v.IsRead) isRead += 1;
                })
                if (isRead > 0) {
                    dispatch({ type: 'DATA_INBOX_NO_READ', payload: true });
                } else {
                    dispatch({ type: 'DATA_INBOX_NO_READ', payload: false });
                }
            } else {
                dispatch({ type: 'DATA_INBOXS', payload: [] });
            }
        })
    }
}

export const setDataInboxNoRead = () => {
    return (dispatch) => {
        dispatch({ type: 'DATA_INBOX_NO_READ', payload: false });
        http.get(`${BASEAPI}/Mobiles/GetInboxSiswas`).then((res) => {
            let data = res.data;
            if (data.IsSuccess) {
                let isRead = 0;
                data.Data.map((v, i) => {
                    if (!v.IsRead) isRead += 1;
                })
                if (isRead > 0) dispatch({ type: 'DATA_INBOX_NO_READ', payload: true });
            } else {
                dispatch({ type: 'DATA_INBOX_NO_READ', payload: false });
            }
        })
    }
}

export const apiReadInbox = (idSiswaInbox) => {
    return (dispatch) => {
        http.get(`${BASEAPI}/Mobiles/ReadInboxSiswa?IdSiswaInbox=${idSiswaInbox}`).then((res) => {
            let data = res.data;
            if (data.IsSuccess) {
                dispatch(setDataInboxs());
                // dispatch({ type: 'DATA_INBOXS', payload: data.Data });
            } else {
                // dispatch({ type: 'DATA_INBOXS', payload: [] });
            }
        })
    }
}