import { FUNCToast } from '../../function';
import { BASEAPI, BASEURL, http } from '../../helpers';

export const setDataTagihans = (navigation) => {
    return async (dispatch) => {
        await http.get(`${BASEAPI}/Mobiles/GetSiswaTagihan`).then(res => {
            let data = res.data;
            if (data.IsSuccess) {
                dispatch({ type: 'DATA_TAGIHANS', payload: data.Data });
                navigation.navigate('Tagihans');
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }

        }).catch(err => {
            FUNCToast('FAIL', { msg: `${JSON.stringify(err)}` });
        })
        FUNCToast('HIDE');
    }
}
export const setDataTagihansNoPin = () => {
    FUNCToast('LOADING', {msg: 'sedang memuat...'})
    return async (dispatch) => {
        await http.get(`${BASEAPI}/Mobiles/GetSiswaTagihan`).then(res => {
            let data = res.data;
            if (data.IsSuccess) {
                dispatch({ type: 'DATA_TAGIHANS', payload: data.Data });
                FUNCToast('HIDE');
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }

        }).catch(err => {
            FUNCToast('FAIL', { msg: `${JSON.stringify(err)}` });
        })
    }
}