import { FUNCToast } from '../../function';
import { BASEAPI, http } from '../../helpers';

export const setDataRapors = () => {
    return (dispatch) => {
        dispatch({ type: 'DATA_RAPORS', payload: false });
        http.get(`${BASEAPI}/Mobiles/GetSiswaRaporsBySiswa`).then((res) => {
            let data = res.data;
            if (data.IsSuccess) {
                dispatch({ type: 'DATA_RAPORS', payload: data.Data });
            } else {
                dispatch({ type: 'DATA_RAPORS', payload: [] });
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        })
    }
}
export const setDataRaporByTahunAjaran = (idTahunAjaran) => {
    return (dispatch) => {
        dispatch({ type: 'DATA_RAPORS', payload: false });
        http.get(`${BASEAPI}/Mobiles/GetSiswaRapors?IdTahunAjaran=${idTahunAjaran}`).then((res) => {
            let data = res.data;
            if (data.IsSuccess) {
                dispatch({ type: 'DATA_RAPORS', payload: data.Data });
            } else {
                dispatch({ type: 'DATA_RAPORS', payload: [] });
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        })
    }
}

export const setDataTahunAjarans = () => {
    return (dispatch) => {
        dispatch({ type: 'DATA_TAHUN_AJARANS', payload: false });
        http.get(`${BASEAPI}/Mobiles/GetTahunAjarans`).then((res) => {
            let data = res.data;
            if (data.IsSuccess) {
                dispatch({ type: 'DATA_TAHUN_AJARANS', payload: data.Data });
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        })
    }
}