import { FUNCToast } from '../../function';
import { BASEAPI, BASEURL, http } from '../../helpers';

export const setDataPenilaianSiswas = (idTahunAjaran = 2020, semester = 1) => {
    return async (dispatch) => {
        await http.get(`${BASEAPI}/Mobiles/GetPenilaianDiriSiswa?IdTahunAjaran=${idTahunAjaran}&Semester=${semester}`).then(res => {
            let data = res.data;
            if (data.IsSuccess) {
                dispatch({ type: 'DATA_PENILAIAN_SISWAS', payload: data.Data });
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        })
    }
}
export const setDataSemester = () => {
    return async (dispatch) => {
        await http.get(`${BASEAPI}/Mobiles/GetSemester`).then(res => {
            let data = res.data;
            if (data.IsSuccess) {
                dispatch({ type: 'DATA_SEMESTER', payload: data.Data });
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        })
    }
}

export const setFormPenilaianSiswa = (formType, formValue) => {
    return {type: 'FORM_PENILAIAN_SISWA', formType, formValue}
}

export const apiPenilaianDiriSiswaAdd = (idSiswa, idTahunAjaran, idSemester, iData) => {
    return (dispatch) => {
        // FUNCToast('LOADING', { msg: 'sedang memuat...' });


        var fd = new FormData();
        fd.append('Data.IdSiswa', idSiswa);
        fd.append('Data.IdTahunAjaran', idTahunAjaran);
        fd.append('Data.IdSemester', idSemester);
        iData.map((v, i) => {
            console.log(v)
            fd.append(`Data.PenilaianDiris[${i}].IdJenisPenilaianDiri`, v.IdJenisPenilaianDiri);
            fd.append(`Data.PenilaianDiris[${i}].Penilaian`, v.Penilaian);
            fd.append(`Data.PenilaianDiris[${i}].Catatan`, v.Catatan);
        });

        http.post(`${BASEAPI}/Mobiles/PenilaianDiriSiswaAdd`, fd).then(res => {
            let data = res.data;
            if (data.IsSuccess) {
                FUNCToast('SUCCESS', { msg: 'Berhasil simpan penilaian diri siswa' });
                // dispatch(setDataElearning(idKbmMateri));
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage, duration: 4000 });
            }
        })
    }
}