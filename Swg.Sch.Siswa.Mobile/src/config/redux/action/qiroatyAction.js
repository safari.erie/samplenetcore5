import { FUNCToast } from '../../function';
import { BASEAPI, http } from '../../helpers';

export const setDataQiroatys = (tanggal) => {
    return (dispatch) => {
        dispatch({ type: 'DATA_QIROATYS', payload: false });
        http.get(`${BASEAPI}/Mobiles/GetKelasQuranSiswa?Tanggal=${tanggal}`).then((res) => {
            let data = res.data;
            if (data.IsSuccess) {
                dispatch({ type: 'DATA_QIROATYS', payload: data.Data });
            } else {
                FUNCToast('WARN', { msg: data.ReturnMessage });
            }
        })
    }
}