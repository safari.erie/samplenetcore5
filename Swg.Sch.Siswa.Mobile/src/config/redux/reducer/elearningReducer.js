const initialState = {
    dataElearnings: false,
    dataElearning: false,
    dataElearningByDays: false,
    dataJenisBantuanOrtus: [],
    formJawaban: {
        NamaUrl: '',
        IdJenisBantuanOrtu: 0,
        FileUpload: []
    }
}

const elearningReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'DATA_ELEARNINGS':
            return { ...state, dataElearnings: action.payload }
        case 'DATA_ELEARNING':
            return { ...state, dataElearning: action.payload }
        case 'DATA_ELEARNING_BY_DAYS':
            return { ...state, dataElearningByDays: action.payload }
        case 'DATA_JENIS_BANTUAN_ORTUS':
            return { ...state, dataJenisBantuanOrtus: action.payload }
        case 'FORM_ELEARNING':
            return {
                ...state,
                formJawaban: {
                    ...state.formJawaban,
                    [action.formType]: action.formValue
                }
            }
        default:
            return state
    }
}

export default elearningReducer;