const initialState = {
    pin: '',
}

const pinReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'DATA_PIN':
            return { ...state, pin: action.payload }
        default:
            return state
    }
}

export default pinReducer;