const initialState = {
    dataProfil: false,
    formProfil: {
        Email: '',
        EmailOrtu: '',
        NoHandphone: '',
        FotoProfile: ''
    }
}

const profilReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'DATA_PROFIL':
            return { ...state, dataProfil: action.payload }
        case 'FORM_PROFIL':
            return {
                ...state,
                formProfil: {
                    ...state.formProfil,
                    [action.formType]: action.formValue
                }
            }
        default:
            return state
    }
}

export default profilReducer;