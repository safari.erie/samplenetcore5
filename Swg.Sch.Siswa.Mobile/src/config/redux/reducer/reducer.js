import { combineReducers } from 'redux';
import elearningReducer from './elearningReducer';
import authReducer from './authReducer';
import tagihanReducer from './tagihanReducer';
import mutabaahReducer from './mutabaahReducer';
import qiroatyReducer from './qiroatyReducer';
import raporReducer from './raporReducer';
import pinReducer from './pinReducer';
import profilReducer from './profilReducer';
import ubahPinReducer from './ubahPinReducer';
import ubahPasswordReducer from './ubahPasswordReducer';
import inboxReducer from './inboxReducer';
import penilaianSiswaReducer from './penilaianSiswaReducer';

const reducer = combineReducers({
    elearningReducer,
    authReducer,
    tagihanReducer,
    mutabaahReducer,
    qiroatyReducer,
    raporReducer,
    pinReducer,
    profilReducer,
    ubahPinReducer,
    ubahPasswordReducer,
    inboxReducer,
    penilaianSiswaReducer
});

export default reducer;
