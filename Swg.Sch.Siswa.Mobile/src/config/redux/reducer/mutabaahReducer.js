const initialState = {
    dataMutabaahs: false,
}

const mutabaahReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'DATA_MUTABAAHS':
            return { ...state, dataMutabaahs: action.payload }
        default:
            return state
    }
}

export default mutabaahReducer;