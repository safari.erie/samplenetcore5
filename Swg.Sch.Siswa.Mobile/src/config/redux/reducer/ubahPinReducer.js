const initialState = {
    formUbahPin: {
        PinLama: '',
        PinBaru: '',
    }
}

const ubahPinReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'FORM_UBAH_PIN':
            return {
                ...state,
                formUbahPin: {
                    ...state.formUbahPin,
                    [action.formType]: action.formValue
                }
            }
        default:
            return state
    }
}

export default ubahPinReducer;