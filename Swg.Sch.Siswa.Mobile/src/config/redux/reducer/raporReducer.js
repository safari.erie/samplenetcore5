const initialState = {
    dataRapors: false,
    dataTahunAjarans: []
}

const raporReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'DATA_RAPORS':
            return { ...state, dataRapors: action.payload }
        case 'DATA_TAHUN_AJARANS':
            return { ...state, dataTahunAjarans: action.payload }
        default:
            return state
    }
}

export default raporReducer;