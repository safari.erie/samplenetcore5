const initialState = {
    formUbahPassword: {
        OldPassword: '',
        NewPassword1: '',
        NewPassword2: '',
    }
}

const ubahPasswordReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'FORM_UBAH_PASSWORD':
            return {
                ...state,
                formUbahPassword: {
                    ...state.formUbahPassword,
                    [action.formType]: action.formValue
                }
            }
        default:
            return state
    }
}

export default ubahPasswordReducer;