const initialState = {
    pin: '',
    dataTagihans: false
}

const tagihanReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'DATA_TAGIHANS':
            return { ...state, dataTagihans: action.payload }
        case 'DATA_PIN':
            return { ...state, pin: action.payload }
        default:
            return state
    }
}

export default tagihanReducer;