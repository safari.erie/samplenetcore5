const initialState = {
    dataInboxs: false,
    dataInboxNoRead: false,
}

const inboxReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'DATA_INBOXS':
            return { ...state, dataInboxs: action.payload }
        case 'DATA_INBOX_NO_READ':
            return { ...state, dataInboxNoRead: action.payload }
        default:
            return state
    }
}

export default inboxReducer;