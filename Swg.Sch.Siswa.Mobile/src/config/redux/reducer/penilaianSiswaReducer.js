const initialState = {
    dataPenilaianSiswas: false,
    dataSemester: false,
    formPenilaianSiswa: {}
}

const penilaianSiswaReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'DATA_PENILAIAN_SISWAS':
            return { ...state, dataPenilaianSiswas: action.payload }
        case 'DATA_SEMESTER':
            return { ...state, dataSemester: action.payload }
        case 'FORM_PENILAIAN_SISWA':
            return {
                ...state,
                formPenilaianSiswa: {
                    ...state.formPenilaianSiswa,
                    [action.formType]: action.formValue
                }
            }
        default:
            return state
    }
}

export default penilaianSiswaReducer;