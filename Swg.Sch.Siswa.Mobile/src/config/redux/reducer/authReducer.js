const initialState = {
    formLogin: {
        Nis: '', //1621012087
        Password: '', //1621012087
        NisLupaPass: '',
        TokenNotif: ''
    },
    dataUser: {},
    fotoProfil: {},
    banner: ''
}

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'DATA_USER':
            return { ...state, dataUser: action.payload }
        case 'DATA_USER_PROFIL':
            return { ...state, fotoProfil: action.payload }
        case 'DATA_BANNER':
            return { ...state, banner: action.payload }
        case 'FORM_LOGIN':
            return {
                ...state,
                formLogin: {
                    ...state.formLogin,
                    [action.formType]: action.formValue
                }
            }
        default:
            return state
    }
}

export default authReducer;