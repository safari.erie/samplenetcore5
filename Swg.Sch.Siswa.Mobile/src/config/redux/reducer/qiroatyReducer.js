const initialState = {
    dataQiroatys: false,
}

const qiroatyReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'DATA_QIROATYS':
            return { ...state, dataQiroatys: action.payload }
        default:
            return state
    }
}

export default qiroatyReducer;