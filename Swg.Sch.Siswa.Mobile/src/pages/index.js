import Splash from './Splash';
import Welcome from './Welcome';
import Masuk from './Masuk';
import Home from './Home';
import Message from './Message';
import Inbox from './Inbox';
import User from './User';


import DaftarMapel from './DaftarMapel';
import RuangBelajar from './RuangBelajar';
import Materi from './Materi';
import DetilVidio from './DetilVidio';
import DetilGambar from './DetilGambar';
import DetilPdf from './DetilPdf';
import Pin from './Pin';
import Tagihan from './Tagihan';
import Qiroaty from './Qiroaty';
import Mutabaah from './Mutabaah';
import Rapor from './Rapor';
import Profil from './Profil';
import UbahPin from './UbahPin';
import UbahPassword from './UbahPassword';
import Version from './Version';
import PenilaianSiswa from './PenilaianSiswa';

export {
    Splash,
    Welcome,
    Masuk,
    Home,
    Message,
    Inbox,
    User,
   
    DaftarMapel,
    RuangBelajar,
    Materi,
    DetilVidio,
    DetilGambar,
    DetilPdf,
    Pin,
    Tagihan,
    Qiroaty,
    Mutabaah,
    Rapor,
    Profil,
    UbahPin,
    UbahPassword,
    Version,
    PenilaianSiswa
};
