import React from 'react'
import { Image, TouchableOpacity, StyleSheet, Dimensions, View } from 'react-native'
import { colors } from '../../utils'
import ImageZoom from 'react-native-image-pan-zoom';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Button } from '../../components';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { IconBack } from '../../assets';
import { FUNCDownloadFile } from '../../config/function';

const DetilGambar = ({ navigation, route }) => {
    const { link } = route.params;
    return (
        <View style={{ flex: 1, backgroundColor:colors.black }}>
            
            <View style={{flex:1,  backgroundColor:'transparent'}}>
                <ImageZoom cropWidth={Dimensions.get('window').width}
                    cropHeight={Dimensions.get('window').height}
                    imageWidth={wp('100%')}
                    imageHeight={hp('100%')}
                    style={{backgroundColor:'transparent', }}
                >
                    <Image style={{width:undefined, height:undefined, flex:1, resizeMode:'contain'}} source={{uri:link}}/>
                </ImageZoom>
                <View style={{width:wp('100%'),position:'absolute', top:0, padding:10, marginTop:getStatusBarHeight()}}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <IconBack />
                    </TouchableOpacity>
                </View>
                <View style={{width:wp('100%'),height:80, padding:20,backgroundColor: 'transparent',justifyContent:'center',position:'absolute', bottom:0}}>
                    <Button 
                    title="DOWNLOAD" 
                    onPress={() => FUNCDownloadFile(link)}/>
                </View>
                
            </View>
            
            
        </View>
    )
}

export default DetilGambar


const styles = StyleSheet.create({
  header_body: {
        flexDirection: 'row',
        paddingVertical:18,
        paddingHorizontal:10,
        alignItems: 'center',
        justifyContent: 'space-between'
    },  
})
