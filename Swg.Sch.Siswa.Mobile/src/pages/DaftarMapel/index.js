import React, { useState, useRef, useEffect } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Button,
    StyleSheet,
    Image,
    StatusBar,
    useWindowDimensions,
    ImageBackground,
    ScrollView,
    TextInput
} from 'react-native';
import { colors } from '../../utils/colors';
import { fonts } from '../../utils';
import { IconDateWhite, ILEbook, ILLoading, IconCoRight, IconRefreshDark, IconBack, IconRefresh, Loading, IconSearchWhite } from '../../assets';
import { Gap, HeaderCard, LoadingCard, MapelCard } from '../../components';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { useDispatch, useSelector } from 'react-redux';
import { setDataElearnings } from '../../config/redux/action';
import Animated from 'react-native-reanimated';
import UserAvatar from 'react-native-user-avatar';
import DateTimePicker from '@react-native-community/datetimepicker';
import { Modalize } from 'react-native-modalize';
import { FUNCDateToString } from '../../config/function';
import { useIsFocused } from '@react-navigation/core';

const Tab = createMaterialTopTabNavigator();
const DaftarMapel = () => {
    const modalizeRef = useRef(null);
    const [date, setDate] = useState(new Date());
    const [show, setShow] = useState(false);
    const { dataElearnings } = useSelector(state => state.elearningReducer)
    const dispatch = useDispatch();
    // const isFocused = useIsFocused();

    useEffect(() => {
        dispatch(setDataElearnings(FUNCDateToString(date)));
    }, [dispatch])

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };
    const btnPencarian = () => {
        dispatch(setDataElearnings(FUNCDateToString(date)));
        modalizeRef.current?.close()
    }
   
    return (
        <>
            <StatusBar translucent barStyle="light-content" backgroundColor="transparent" />
            <View style={styles.android_safearea}>

                <HeaderCard
                    statusBar={getStatusBarHeight()}
                    icon={<IconDateWhite />}
                    onPress={() => modalizeRef.current?.open()}
                    title={'Daftar Mapel'}
                />

                {
                    dataElearnings ?
                        <View style={styles.content}>
                            <Tab.Navigator tabBar={props => <TabNavigator {...props} />}>
                                <Tab.Screen name="Senin" component={Senin} initialParams={{ elearnings: dataElearnings.senin }} />
                                <Tab.Screen name="Selasa" component={Selasa} initialParams={{ elearnings: dataElearnings.selasa }} />
                                <Tab.Screen name="Rabu" component={Rabu} initialParams={{ elearnings: dataElearnings.rabu }} />
                                <Tab.Screen name="Kamis" component={Kamis} initialParams={{ elearnings: dataElearnings.kamis }} />
                                <Tab.Screen name="Jumat" component={Jumat} initialParams={{ elearnings: dataElearnings.jumat }} />
                            </Tab.Navigator>
                        </View>
                        : <LoadingCard />
                }
               
                 <Modalize
                    ref={modalizeRef}
                    modalHeight={hp('25%')}
                    handlePosition="inside"
                >
                    <View style={styles.modal_content}>
                        <Text style={styles.modal_title}>Pilih Tanggal</Text>
                        <Gap height={15} />
                        <TouchableOpacity onPress={() => setShow(true)} style={styles.modal_com_input}>
                            {Platform.OS !== 'ios' ?
                                <Text style={styles.modal_com_input_text}>{FUNCDateToString(date)}</Text>
                                :
                                null
                            }
                            {show && (
                                <DateTimePicker
                                    testID="dateTimePicker"
                                    value={date}
                                    mode={'date'}
                                    is24Hour={true}
                                    display="default"
                                    onChange={onChange}
                                    maximumDate={new Date()}
                                />
                            )}
                        </TouchableOpacity>
                        <Gap height={20} />
                        <TouchableOpacity onPress={() => btnPencarian()} style={styles.btn_primary}>
                            <IconSearchWhite />
                            <Text style={styles.btn_primary_text}>Pencarian</Text>
                        </TouchableOpacity>
                    </View>
                </Modalize>
            </View>
        </>
    )
}

const Senin = ({ navigation, route }) => {
    const { elearnings } = route.params;
    return (
        elearnings.length !== 0 ?
            <ScrollView>
                <View style={styles.mapel_content}>
                    {
                        elearnings.map((v, i) => {
                            return (
                                <MapelCard
                                    key={i}
                                    namaMapel={v.NamaMapel}
                                    hari={v.Hari}
                                    guru={v.Guru}
                                    jamMulai={v.JamMulai}
                                    jamSelesai={v.JamSelesai}
                                    judulMateri={v.JudulMateri}
                                    onPress={() => navigation.navigate('RuangBelajar', {idKbmMateri: v.IdKbmMateri})}
                                />    
                            )
                        })
                    }
                </View>
            </ScrollView>
            :
            <View style={styles.mapel_empty_content}>
                <View style={styles.mapel_empty_img_card}>
                    <Image source={ILEbook} style={styles.mapel_empty_image} />
                </View>
                <Gap height={50}/>
                <Text style={styles.mapel_empty_font}>Yah, materi tidak ada :(</Text>
            </View>
    )
}
const Selasa = ({ navigation, route }) => {
    const { elearnings } = route.params;
    return (
        elearnings.length !== 0 ?
           <ScrollView>
                <View style={styles.mapel_content}>
                    {
                        elearnings.map((v, i) => {
                            return (
                                <MapelCard
                                    key={i}
                                    namaMapel={v.NamaMapel}
                                    hari={v.Hari}
                                    guru={v.Guru}
                                    jamMulai={v.JamMulai}
                                    jamSelesai={v.JamSelesai}
                                    judulMateri={v.JudulMateri}
                                    onPress={() => navigation.navigate('RuangBelajar', {idKbmMateri: v.IdKbmMateri})}
                                />    
                            )
                        })
                    }
                </View>
            </ScrollView>
            :
            <View style={styles.mapel_empty_content}>
                <View style={styles.mapel_empty_img_card}>
                    <Image source={ILEbook} style={styles.mapel_empty_image} />
                </View>
                <Gap height={50}/>
                <Text style={styles.mapel_empty_font}>Yah, Materi tidak ada :(</Text>
            </View>
    )
}
const Rabu = ({ navigation, route }) => {
    const { elearnings } = route.params;
    return (
        elearnings.length !== 0 ?
            <ScrollView>
                <View style={styles.mapel_content}>
                    {
                        elearnings.map((v, i) => {
                            return (
                                <MapelCard
                                    key={i}
                                    namaMapel={v.NamaMapel}
                                    hari={v.Hari}
                                    guru={v.Guru}
                                    jamMulai={v.JamMulai}
                                    jamSelesai={v.JamSelesai}
                                    judulMateri={v.JudulMateri}
                                    onPress={() => navigation.navigate('RuangBelajar', {idKbmMateri: v.IdKbmMateri})}
                                />    
                            )
                        })
                    }
                </View>
            </ScrollView>
            :
            <View style={styles.mapel_empty_content}>
                <View style={styles.mapel_empty_img_card}>
                    <Image source={ILEbook} style={styles.mapel_empty_image} />
                </View>
                <Gap height={50}/>
                <Text style={styles.mapel_empty_font}>Yah, Materi tidak ada :(</Text>
            </View>
    )
}
const Kamis = ({ navigation, route }) => {
    const { elearnings } = route.params;
    return (
        elearnings.length !== 0 ?
            <ScrollView>
                <View style={styles.mapel_content}>
                    {
                        elearnings.map((v, i) => {
                            return (
                                <MapelCard
                                    key={i}
                                    namaMapel={v.NamaMapel}
                                    hari={v.Hari}
                                    guru={v.Guru}
                                    jamMulai={v.JamMulai}
                                    jamSelesai={v.JamSelesai}
                                    judulMateri={v.JudulMateri}
                                    onPress={() => navigation.navigate('RuangBelajar', {idKbmMateri: v.IdKbmMateri})}
                                />    
                            )
                        })
                    }
                </View>
            </ScrollView>
            :
            <View style={styles.mapel_empty_content}>
                <View style={styles.mapel_empty_img_card}>
                    <Image source={ILEbook} style={styles.mapel_empty_image} />
                </View>
                <Gap height={50}/>
                <Text style={styles.mapel_empty_font}>Yah, Materi tidak ada :(</Text>
            </View>
    )
}
const Jumat = ({ navigation, route }) => {
    const { elearnings } = route.params;
    return (
        elearnings.length !== 0 ?
            <ScrollView>
                <View style={styles.mapel_content}>
                    {
                        elearnings.map((v, i) => {
                            return (
                                <MapelCard
                                    key={i}
                                    namaMapel={v.NamaMapel}
                                    hari={v.Hari}
                                    guru={v.Guru}
                                    jamMulai={v.JamMulai}
                                    jamSelesai={v.JamSelesai}
                                    judulMateri={v.JudulMateri}
                                    onPress={() => navigation.navigate('RuangBelajar', {idKbmMateri: v.IdKbmMateri})}
                                />    
                            )
                        })
                    }
                </View>
            </ScrollView>
            :
            <View style={styles.mapel_empty_content}>
                <View style={styles.mapel_empty_img_card}>
                    <Image source={ILEbook} style={styles.mapel_empty_image} />
                </View>
                <Gap height={50}/>
                <Text style={styles.mapel_empty_font}>Yah, Materi tidak ada :(</Text>
            </View>
    )
}

const TabNavigator = ({ state, descriptors, navigation, position }) => {
    return (
        <View style={{ flexDirection: 'row',  borderBottomWidth: 0.3, borderBottomColor:colors.dark2, paddingLeft:10 }}>
            <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{}}>
                {state.routes.map((route, index) => {
                    const { options } = descriptors[route.key];
                    const label =
                        options.tabBarLabel !== undefined
                            ? options.tabBarLabel
                            : options.title !== undefined
                                ? options.title
                                : route.name;

                    const isFocused = state.index === index;

                    const onPress = () => {
                        const event = navigation.emit({
                            type: 'tabPress',
                            target: route.key,
                            canPreventDefault: true,
                        });

                        if (!isFocused && !event.defaultPrevented) {
                            navigation.navigate(route.name);
                        }
                    };


                    const onLongPress = () => {
                        navigation.emit({
                            type: 'tabLongPress',
                            target: route.key,
                        });
                    };

                    const inputRange = state.routes.map((_, i) => i);
                    const opacity = Animated.interpolate(position, {
                        inputRange,
                        outputRange: inputRange.map(i => (i === index ? 1 : 0)),
                    });
                    return (
                        <View key={index} style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Gap height={10} />
                            <TouchableOpacity
                                accessibilityRole="button"
                                accessibilityStates={isFocused ? ['selected'] : []}
                                accessibilityLabel={options.tabBarAccessibilityLabel}
                                testID={options.tabBarTestID}
                                onPress={onPress}
                                onLongPress={onLongPress}
                            >
                            
                            {
                                isFocused ? (
                                    <Animated.View opacity={opacity} style={{backgroundColor:colors.primary, marginHorizontal:5, paddingVertical:5, paddingHorizontal:15, borderRadius:20}}>
                                        <Text style={{color:colors.white, fontFamily:fonts.primary[800], fontSize:13}}>{label}</Text>
                                    </Animated.View>
                                ) : (
                                    <View style={{backgroundColor:colors.dark2, borderWidth:1, borderColor:colors.dark2, marginHorizontal:5, paddingVertical:5, paddingHorizontal:15, borderRadius:20}}>
                                        <Text style={{color:colors.white, fontFamily:fonts.primary[800], fontSize:13}}>{label}</Text>
                                    </View>
                                )
                                }
                                </TouchableOpacity>
                            <Gap height={10} />
                        </View>
                    );
                })}
            </ScrollView>
        </View>
    );
}


export default DaftarMapel

const styles = StyleSheet.create({
    android_safearea: {
        flex: 1,
        backgroundColor: colors.primary
    },
    modal_content: {
        padding: 10,
        top: 10
    },
    modal_title: {
        fontSize: 18, 
        color: colors.black, 
        fontFamily: fonts.primary[600]
    },
    modal_com_input: {
        width: '100%', 
        height:40, 
        backgroundColor: colors.white, 
        borderWidth:0.5, 
        borderColor:colors.dark2, 
        borderRadius: 5, 
        justifyContent:'center' 
    },
    modal_com_input_text: {
        fontSize: 13,
        color: colors.black,
        fontFamily: fonts.primary[400],
        padding: 10
    },
    btn_primary: {
        height: 40, 
        backgroundColor: colors.primary, 
        borderRadius: 7, 
        justifyContent: 'center', 
        alignItems: 'center',
        flexDirection:'row'
    },
    btn_primary_text: {
        fontFamily: fonts.primary[700],
        color: colors.white,
        fontSize: 15,
        marginLeft: 5
    },
    mapel_content: {
        flex: 1, 
        alignItems: 'center', 
        alignContent: 'center', 
        justifyContent: 'center',
        backgroundColor: '#F6F7FC',
        padding:10,
    },

    splashgif: {
        width: 50,
        height: 50
    },
    header_content_search: {
        position: 'relative',
        flex: 1,
        justifyContent: 'center',
    },
    header_content_search_font: {
        borderWidth: 1,
        borderColor: '#E8E8E8',
        borderRadius: 10,
        height: 40,
        fontSize: 13,
        paddingLeft: 45,
        paddingRight: 20,
        backgroundColor: '#f6f8fa',
        justifyContent: 'center'
    },
    content: {
        flex: 1,
        backgroundColor: '#F6F7FC',
        paddingBottom: 10,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },

    icon_body: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap'
    },

    detil_card: {
        flex: 1,
        backgroundColor: colors.white,
        borderRadius: 0,
        paddingVertical: 15,
    },
    // batas
    daftar_mapel_card: {
        backgroundColor: colors.white,
        borderRadius: 10,
        elevation: 2,
        paddingBottom: 10,
        marginBottom: 10,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2,
    },
    daftar_mapel_header: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 5,
        justifyContent: 'space-between'
    },
    daftar_mapel_header_mapel_font: {
        fontSize: 12,
        fontFamily: fonts.primary[600],
    },
    daftar_mapel_header_hari_font: {
        fontSize: 11,
        fontFamily: fonts.primary[400],
    },
    daftar_mapel_body: {
        padding: 10,
        flexDirection: 'row',
        alignItems:'center',
        borderTopWidth: 0.3,
        borderTopColor: colors.dark2
    },
    daftar_mapel_img_card: {
        width: wp('20%'),
        height: hp('10%'),
        backgroundColor: '#f27c56'
    },
    daftar_mapel_image: {
        width: undefined,
        height: undefined,
        flex: 1,
        resizeMode: 'contain'
    },
    daftar_mapel_desc_card: {
        marginHorizontal: 10,
        minWidth: wp('65%'),
    },
    daftar_mapel_desc_materi_font: {
        maxWidth: wp('65%'),
        fontSize: 14,
        fontFamily: fonts.primary[600]
    },
    daftar_mapel_desc_materi_rinci_font: {
        maxWidth: wp('60%'),
        fontSize: 11,
        fontFamily: fonts.primary[400]
    },
    btn_ruang_belajar: {
        flexDirection: 'row',
        backgroundColor: colors.white, 
        borderColor: colors.yellow1, 
        borderWidth: 0.9, 
        paddingVertical: 5, 
        paddingHorizontal: 10, 
        borderRadius: 10, 
        alignItems: 'center', 
    },
    btn_ruang_belajar_text: {
        color: colors.yellow1,
        fontFamily: fonts.primary[600],
        fontSize: 13,
        paddingHorizontal:5
    },
    daftar_mapel_desc_card_action_button: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    mapel_empty_content: {
        flex: 1, 
        alignItems: 'center', 
        alignContent: 'center', 
        justifyContent: 'center', 
        backgroundColor:colors.white
    },
    mapel_empty_img_card: {
        width: 200, height: 200, marginTop:-100
    },
    mapel_empty_image: {
        width: undefined, 
        height: undefined, 
        resizeMode: 'cover', 
        flex: 1, 
        borderRadius: 100
    },
    mapel_empty_font: {
        fontSize: 16, 
        color: colors.black, 
        fontFamily: fonts.primary[400]
    },

})
