import React, { useState, useEffect, useRef } from 'react';
import {
    Text,
    View,
    TextInput, 
    StatusBar, 
    StyleSheet, 
    ScrollView, 
    KeyboardAvoidingView, 
    TouchableOpacity,
    ImageBackground,
    Platform
} from 'react-native';
import { colors } from '../../utils/colors';
import { fonts } from '../../utils';
import { IconBack, IconBackDark, IconCloseWhite, IconFPaketData, IconHome, IconMore, IconMoreActive, IconRefresh, ILBg1, ILBg2 } from '../../assets';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { AlertHeader, Gap } from '../../components';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import { useDispatch, useSelector } from 'react-redux';
import { apiResetPin, setDataPin, setDataTagihans } from '../../config/redux/action';
import { Modalize } from 'react-native-modalize';

const Pin = ({ navigation, route }) => {
    const modalResetPinRef = useRef(null);
    const { pinFor } = route.params;
    const { pin } = useSelector(state => state.pinReducer);
    const dispatch = useDispatch();
    const onPin = (pin) => {
        dispatch(setDataPin(pin, navigation, pinFor))
    }
    const btnResetPin = () => {
        dispatch(apiResetPin());
        modalResetPinRef.current.close();
    }
    return (
        <>
            <StatusBar translucent barStyle="light-content" backgroundColor="transparent" />
             <KeyboardAvoidingView
                // behavior={Platform.OS === "ios" ? "padding" : "height"}
                style={{ flex: 1, justifyContent: 'center', backgroundColor: colors.white }}
            >
                <View style={{ flex: 1, justifyContent: 'center', backgroundColor: colors.white }}>
                    {/* <ScrollView> */}

                        <ImageBackground source={ILBg2} style={styles.content}>
                            <Gap height={20} />
                            <View style={{flex:1,justifyContent:'center', alignItems:'center'}}>
                                <Text style={{ fontSize: 17, color: colors.white, fontFamily: fonts.primary[800] }}>Masukan PIN untuk akses {pinFor}!</Text>
                                <Gap height={40} />
                                <SmoothPinCodeInput
                                    placeholder={<View style={{
                                        width: 10,
                                        height: 10,
                                        borderRadius: 25,
                                        opacity: 0.3,
                                        backgroundColor: colors.white,
                                    }}></View>}
                                    mask={<View style={{
                                        width: 10,
                                        height: 10,
                                        borderRadius: 25,
                                        backgroundColor: colors.white,
                                    }}></View>}
                                    maskDelay={1000}
                                    password={true}
                                    cellStyle={null}
                                    cellStyleFocused={null}
                                    value={pin}
                                    onTextChange={pin => dispatch({ type: 'DATA_PIN', payload: pin })}
                                    codeLength={6}
                                    autoFocus={true}
                                    onFulfill={(e) => onPin(e)}
                                />
                                <Gap height={hp('15%')} />
                                    
                                <TouchableOpacity onPress={() => modalResetPinRef.current.open()}>
                                    <Text style={{fontSize: 14, color: colors.white, fontFamily: fonts.primary[600]}}>Lupa PIN ?</Text>
                                </TouchableOpacity>
                                
                                {
                                    pinFor !== 'tagihan' ?
                                        <View style={{width:wp('100%'),position:'absolute', top:0, paddingHorizontal:10, marginTop:getStatusBarHeight()}}>
                                            <TouchableOpacity onPress={() => navigation.goBack()} style={{flexDirection:'row', alignItems:'center'}}>
                                                <IconBack />
                                                <Text style={{fontSize: 18, color: colors.white, fontFamily: fonts.primary[600], marginLeft:10}}>Kembali</Text>
                                            </TouchableOpacity>
                                        </View>
                                        : null
                                }
                            </View>
                        </ImageBackground>
                    {/* </ScrollView> */}
                </View>
            </KeyboardAvoidingView>

            <Modalize
                ref={modalResetPinRef}
                modalHeight={hp('40%')}
                handlePosition="inside"
            >
                <View style={{padding: 10, top: 10, height:hp('39%')}}>
                    <Text style={styles.modal_title}>Lupa PIN</Text>
                    <Gap height={15} />
                    <AlertHeader desc="PIN akan direset otomatis oleh system, untuk melihat pin silahkan cek email siswa atau email orang tua!" />
                    {
                        pinFor !== 'tagihan' ?
                            <View style={{position:'absolute', bottom:0 }}>
                                <TouchableOpacity onPress={() => btnResetPin()} style={{ backgroundColor: colors.primary, justifyContent: 'center', alignItems: 'center', width: wp('100%'), flexDirection: 'row', paddingVertical: Platform.OS === 'ios' ? 20 : 15, paddingBottom:25 }}>
                                    <IconCloseWhite />
                                    <Text style={{ color: colors.white, fontFamily: fonts.primary[700], fontSize: 15, marginLeft: 5 }}>Reset PIN</Text>
                                </TouchableOpacity>
                            </View>
                            :
                            <>
                                <Gap height={hp('15%')} />
                                <TouchableOpacity onPress={() => btnResetPin()} style={styles.btn_primary}>
                                    <IconCloseWhite />
                                    <Text style={styles.btn_primary_text}>Reset PIN</Text>
                                </TouchableOpacity>
                            </>
                    }

                    
                </View>
            </Modalize>
        </>
    )
}

export default Pin

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: colors.primary,
        alignItems: 'center',
        height: '100%'
    },
    container: {
        flexDirection: 'row',
        padding: 16,
        borderBottomWidth: 0.5,
        borderBottomColor: colors.dark2,
        alignItems: 'center',
    },
    avatar: {
        width: 46,
        height: 46,
        borderRadius: 46 / 2,
        marginRight: 12,
        backgroundColor: colors.primary
    },
    name: {
        fontSize: 16,
        fontFamily: fonts.primary.normal,
        color: colors.black,
    },
    desc: {
        fontSize: 12,
        fontFamily: fonts.primary[300],
        color: colors.text.secondary,
    },
    btn_primary: {
        height: 40,
        backgroundColor: colors.primary,
        borderRadius: 7,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    btn_primary_text: {
        fontFamily: fonts.primary[700],
        color: colors.white,
        fontSize: 15,
        marginLeft: 5
    },
});
