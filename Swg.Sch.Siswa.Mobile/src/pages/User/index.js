import React, { useState, useEffect, useRef } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Image,
    StyleSheet,
    ScrollView,
    StatusBar
} from 'react-native';
import { colors } from '../../utils/colors';
import { fonts } from '../../utils';
import { IconRight, IconBronze, IconPromo, IconMore, IconUbahProfile, IconBantuan, IconPanduan, ILNullPhoto, IconAffilate, IconSnK, IconScanQr, IconQrCode, IconPinUser, IconPasswordUser, IconVerified } from '../../assets';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Gap, HeaderCard, Avatar } from '../../components';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { FUNCToast } from '../../config/function';
import { useDispatch, useSelector } from 'react-redux';
import { apiGetInfoUserHome } from '../../config/redux/action';
import { VERSIAPP } from '../../config/helpers';

const User = ({ navigation }) => {
    const { dataUser, fotoProfil } = useSelector(state => state.authReducer);
    const dispatch = useDispatch();
    const btnKeluar = async () => {
        FUNCToast('LOADING', { msg: 'sedang mengahapus sesi...' });
        try {
            let keys = ['@token', '@user'];
                
            await AsyncStorage.multiRemove(keys, (err) => {
                setTimeout(() => {
                    navigation.replace('Splash')
                    FUNCToast('SUCCESS', { msg: 'Berhasil logout akun!' });
                }, 2000);
            })
        } catch(e) {
            FUNCToast('FAIL', { msg: e });
        }
    }
    // useEffect(() => {
    //     dispatch(apiGetInfoUserHome());
    // }, [])
    return (
        <>
            <StatusBar translucent barStyle="light-content" backgroundColor="transparent"  />
            <View style={styles.android_safearea}>
                <HeaderCard
                    statusBar={getStatusBarHeight()}
                    title={'Pengaturan'}
                />
               
                <View style={styles.container}>
                    <ScrollView>
                        <View style={styles.content}>
                            <View style={styles.card_user}>
                                <Avatar source={fotoProfil === '' ? ILNullPhoto : {uri : fotoProfil}} size={40} />
                                <View>
                                    <View style={{flexDirection:'row', alignItems:'center', maxWidth:'90%'}}>
                                        <Text style={styles.name}>{dataUser.Nama} </Text>
                                        <IconVerified />
                                    </View>
                                    <Text style={styles.desc}>Kelas {dataUser.KelasParalel}</Text>
                                </View>
                            </View>
                            
                            <Gap height={5} />
                            <View style={styles.card_list}>
                                <Text style={styles.list_title}>Akun</Text>
                                <Gap height={15} />
                                <TouchableOpacity onPress={() => navigation.navigate('PinProfil')} style={styles.list_content}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <View style={{marginRight:15}}>
                                        <IconUbahProfile /> 
                                        </View>
                                        <Text style={{fontFamily:fonts.primary[500], fontSize:15}}>Ubah Profil</Text>
                                    </View>
                                    <View>
                                        <Text style={{fontFamily:fonts.primary[300], fontSize:13}}> <IconRight /></Text>
                                    </View>
                                </TouchableOpacity>

                                <Gap height={15} />
                                <TouchableOpacity onPress={() => navigation.navigate('PinChange')} style={styles.list_content}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <View style={{marginRight:15}}>
                                        <IconPinUser /> 
                                        </View>
                                        <Text style={{fontFamily:fonts.primary[500], fontSize:15}}>Ubah PIN</Text>
                                    </View>
                                    <View>
                                        <Text style={{fontFamily:fonts.primary[300], fontSize:13}}> <IconRight /></Text>
                                    </View>
                                </TouchableOpacity>

                                <Gap height={15} />
                                <TouchableOpacity onPress={() => navigation.navigate('PinPassword')} style={styles.list_content}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <View style={{marginRight:15}}>
                                        <IconPasswordUser /> 
                                        </View>
                                        <Text style={{fontFamily:fonts.primary[500], fontSize:15}}>Ubah Password</Text>
                                    </View>
                                    <View>
                                        <Text style={{fontFamily:fonts.primary[300], fontSize:13}}> <IconRight /></Text>
                                    </View>
                                </TouchableOpacity>

                                {/* <Gap height={15} />
                                <View style={styles.list_content}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <View style={{marginRight:15}}>
                                        <IconSnK /> 
                                        </View>
                                        <Text style={{fontFamily:fonts.primary[500], fontSize:15}}>Syarat dan Ketentuan</Text>
                                    </View>
                                    <View>
                                        <Text style={{fontFamily:fonts.primary[300], fontSize:13}}> <IconRight /></Text>
                                    </View>
                                </View> */}

                                <Gap height={15} />
                                <TouchableOpacity onPress={() => navigation.navigate('Version')} style={styles.list_content}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <View style={{marginRight:15}}>
                                        <IconPanduan /> 
                                        </View>
                                        <Text style={{fontFamily:fonts.primary[500], fontSize:15}}>Version</Text>
                                    </View>
                                    <View>
                                        <Text style={{fontFamily:fonts.primary[300], fontSize:13}}> <IconRight /></Text>
                                    </View>
                                </TouchableOpacity>

                            </View>
                            <Gap height={5} />
                            <View style={{ marginHorizontal: 20, marginVertical: 10 }}>
                                <TouchableOpacity onPress={() => btnKeluar()} style={{ height: 40, backgroundColor: colors.primary, margin: 10, borderRadius: 10, justifyContent: 'center', alignItems: 'center'}}>
                                    <Text style={{fontFamily:fonts.primary[700], color:colors.white, fontSize: 16}}>Keluar</Text>
                                </TouchableOpacity>
                                <View style={{ alignItems: 'center' }}>
                                    <Text style={{ fontFamily: fonts.primary[600], color: colors.dark2, fontSize: 13 }}>#ATTAUFIQ {VERSIAPP}</Text>
                                </View>
                            </View>
                            
                            <Gap height={25} />
                        </View>
                    </ScrollView>
                </View>
            </View>
        </>
    )
}

export default User

const styles = StyleSheet.create({
    android_safearea: {
        flex: 1,
        backgroundColor: colors.primary
    },
    container: {
        flex:1,
        borderTopLeftRadius:20,
        borderTopRightRadius:20,
        backgroundColor:'white',
        marginTop:0  
    },
    content: {
        backgroundColor : colors.white,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        flex: 1,
        marginTop: 5,
    },
    card_user: {
        flexDirection: 'row',
        padding: 16,
        borderBottomWidth: 0.6,
        borderBottomColor: colors.dark2,
        alignItems: 'center',
    },
    avatar_content: {
        width: 46,
        height: 46,
        borderRadius: 100,
        marginRight: 12,
        backgroundColor: colors.yellow1,
        justifyContent: 'center',
        alignItems:'center'
    },
    avatar_body: {
        width: 40,
        height: 40,
        borderRadius: 100,
        backgroundColor: 'pink'
    },
    name: {
        fontSize: 16,
        fontFamily: fonts.primary[600],
        color: colors.black,
        marginRight: 5,
    },
    desc: {
        fontSize: 12,
        fontFamily: fonts.primary[300],
        color: colors.text.secondary,
    },
    card_list: {
        marginHorizontal: 20,
        marginVertical: 10
    },
    list_title: {
        fontFamily: fonts.primary[700],
        fontSize: 20
    },
    list_content: {
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems: 'center',
        borderBottomWidth: 0.6,
        borderBottomColor: colors.dark2,
        paddingBottom:20
    }
})
