import * as React from 'react';
import { Text, View, TouchableOpacity, StatusBar, StyleSheet, ScrollView,Dimensions, TextInput } from 'react-native';
import { colors } from '../../utils/colors';
import { fonts } from '../../utils';
import { IconFPaketData, IconHome, IconMore, IconMoreActive, IconRefresh, IconUploadWhite } from '../../assets';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Gap, HeaderCard } from '../../components';
import { useDispatch, useSelector } from 'react-redux';
import { apiGantiPin, setFormUbahPin } from '../../config/redux/action';
const UbahPin = ({ navigation }) => {
    const { formUbahPin } = useSelector(state => state.ubahPinReducer);
    const dispatch = useDispatch();

    const {
        PinLama,
        PinBaru
    } = formUbahPin;

    const btnUbahPin = () => dispatch(apiGantiPin(formUbahPin, navigation));
    return (
        <>
            <StatusBar translucent barStyle="light-content" backgroundColor="transparent"  />
            <View style={styles.android_safearea}>
              
                <HeaderCard
                    statusBar={getStatusBarHeight()}
                    title={'Ubah PIN'}
                    isBack={true}
                    onPressBack={() => navigation.goBack()}
                />
                
                <View style={styles.content}>
                    <ScrollView>
                        <View style={{ flex: 1, backgroundColor: colors.white, borderRadius: 20 }}>
                            <View style={{paddingHorizontal:15, marginTop:20}}>

                                <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[500]}}>PIN Lama</Text>
                                <View style={{alignItems:'center'}}>
                                    <TextInput
                                        style={{width:'100%', borderBottomWidth:1,borderBottomColor: colors.dark2,paddingVertical: 10, fontSize: 16,color: colors.black}} 
                                        placeholder={'Masukan PIN Lama'} 
                                        placeholderTextColor={colors.default}
                                        maxLength={6}
                                        keyboardType={'number-pad'}
                                        value={PinLama}
                                        onChangeText={(e) => dispatch(setFormUbahPin('PinLama', e))}
                                    />
                                </View>

                                <Gap height={20} />

                                <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[500]}}>PIN Baru</Text>
                                <View style={{alignItems:'center'}}>
                                    <TextInput 
                                        style={{width:'100%', borderBottomWidth:1,borderBottomColor: colors.dark2,paddingVertical: 10, fontSize: 16,color: colors.black}} 
                                        placeholder={'Masukan PIN Baru'} 
                                        placeholderTextColor={colors.default}
                                        maxLength={6}
                                        keyboardType={'number-pad'}
                                        value={PinBaru}
                                        onChangeText={(e) => dispatch(setFormUbahPin('PinBaru', e))}
                                    />
                                </View>
                                

                                <Gap height={40} />

                                <TouchableOpacity onPress={() => btnUbahPin()} style={styles.btn_primary}>
                                    <IconUploadWhite />
                                    <Text style={styles.btn_primary_text}>Perbarui PIN</Text>
                                </TouchableOpacity>
                                <Gap height={20} />
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </View>
        </>
    )
}

export default UbahPin

const styles = StyleSheet.create({
    android_safearea: {
        flex: 1,
        backgroundColor: colors.primary
    },
    content: {
        flex: 1,
        backgroundColor : colors.white,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    btn_primary: {
        height: 40, 
        backgroundColor: colors.primary, 
        borderRadius: 7, 
        justifyContent: 'center', 
        alignItems: 'center',
        flexDirection:'row'
    },
    btn_primary_text: {
        fontFamily: fonts.primary[700],
        color: colors.white,
        fontSize: 15,
        marginLeft: 5
    },
})
