import React, {useState, useRef, useEffect} from 'react';
import {
    Text,
    TextInput,
    View,
    TouchableOpacity,
    StatusBar,
    StyleSheet,
    ScrollView,
    Image,
    ImageBackground
} from 'react-native';
import { colors } from '../../utils/colors';
import { fonts } from '../../utils';
import { IconBack, IconHome, IconMore, IconMoreActive, IconRefresh, IconUploadWhite, IconEditWhite, ILBg2, ILNullPhoto, IconCameraWhite, IconImageWhite } from '../../assets';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Gap, Avatar } from '../../components';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { color } from 'react-native-reanimated';
import { useDispatch, useSelector } from 'react-redux';
import {
    setDataProfil,
    apiEditProfilSiswa,
    setFormProfil
} from '../../config/redux/action/profilAction';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { Modalize } from 'react-native-modalize';
import { FUNCToast } from '../../config/function';

const Profil = ({navigation}) => {
    const modalFotoProfilReff = useRef(null);
    const { dataProfil, formProfil } = useSelector(state => state.profilReducer);
    const dispatch = useDispatch();

    const {
        Email,
        EmailOrtu,
        NoHandphone,
        FotoProfile
    } = formProfil;

    const btnCamera = () => {
        FUNCToast('LOADING', { msg: 'sedang memuat...' })
        let options = {
            cameraType: 'front',
            mediaType: 'photo' ,
            quality: 1,
        };
        launchCamera(options, (response) => {
            FUNCToast('HIDE');
            dispatch(setFormProfil('FileUpload', ''));
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                FUNCToast('FAIL', { msg: response.error })
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else if (response.errorCode === 'camera_unavailable') {
                FUNCToast('FAIL', { msg: response.errorCode })
            } else {
                
                if (typeof response.errorCode === 'undefined') {
                    var array = [];
                    response.assets.map((v, i) => {
                        array.push({
                            uri: v.uri,
                            type: v.type,
                            name: v.fileName,
                        });
                    });
                    dispatch(setFormProfil('FotoProfile',
                        {
                            uri: response.assets[0].uri,
                            type: response.assets[0].type,
                            name: response.assets[0].fileName,
                        }
                    ));
                    dispatch(setFormProfil('IsFotoProfile', true));
                    modalFotoProfilReff.current.close();
                } else {
                    FUNCToast('FAIL', { msg: 'Izinkan kami untuk mengakses kamera!' })
                }
            }
        });
    }
    const btnGallery = () => {
        FUNCToast('LOADING', { msg: 'sedang memuat...' })
        let options = {
            cameraType: 'front',
            mediaType: 'photo' ,
        }; 
        launchImageLibrary(options, (response) => {
            FUNCToast('HIDE');
            dispatch(setFormProfil('FotoProfile', ''));
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                dispatch(setFormProfil('FotoProfile',
                    {
                        uri: response.assets[0].uri,
                        type: response.assets[0].type,
                        name: response.assets[0].fileName,
                    }
                ));
                dispatch(setFormProfil('IsFotoProfile', true));
                modalFotoProfilReff.current.close()
            }
        });
        
    }

    const btnPerbaruiData = () => dispatch(apiEditProfilSiswa(formProfil));
    return (
        <>
            <StatusBar translucent barStyle="light-content" backgroundColor="transparent"  />
            <View style={styles.android_safearea}>
              
                <View style={styles.content}>
                    {
                        dataProfil ?
                                    
                            <ScrollView>
                                <ImageBackground source={ILBg2} style={{ flex: 1, backgroundColor: '#ff8214',  paddingTop: 20, paddingBottom:30 }}>
                                    <View style={{ paddingHorizontal: 15,  marginTop:getStatusBarHeight() }}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                            <TouchableOpacity onPress={() => navigation.goBack()} style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                                <IconBack />
                                                <Text style={{ fontSize: 18, color: colors.white, fontFamily: fonts.primary[700], left:10 }}>Ubah Profil</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={{ flex: 1, alignItems: 'center', paddingTop: 20 }}>
                                        <Avatar source={FotoProfile.uri === '' ? ILNullPhoto : {uri : FotoProfile.uri}} size={135} />
                                        <TouchableOpacity onPress={() => modalFotoProfilReff.current.open()} style={{ flexDirection: 'row', alignItems: 'center', top:10 }}>
                                            <IconEditWhite />
                                            <Text style={{ fontSize: 14, color: colors.white, fontFamily: fonts.primary[700], marginLeft:5}}>Ganti Foto Profil</Text>
                                        </TouchableOpacity>
                                        <Gap height={20} />
                                    </View>
                                </ImageBackground>
                                <Gap height={20} />
                                <View style={{ flex: 1, marginTop: -35, backgroundColor: colors.white, borderRadius: 20 }}>
                                    <View style={{paddingHorizontal:15, marginTop:20}}>

                                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[500]}}>Email Siswa</Text>
                                        <View style={{alignItems:'center'}}>
                                            <TextInput 
                                                style={{width:'100%', borderBottomWidth:1,borderBottomColor: colors.dark2,paddingVertical: 10, fontSize: 16,color: colors.black}} 
                                                placeholder={'Masukan Email Siswa'} 
                                                placeholderTextColor={colors.default}
                                                value={Email}
                                                onChangeText={(e) => dispatch(setFormProfil('Email', e))}
                                            />
                                        </View>

                                        <Gap height={20} />

                                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[500]}}>Email Ortu</Text>
                                        <View style={{alignItems:'center'}}>
                                            <TextInput 
                                                style={{width:'100%', borderBottomWidth:1,borderBottomColor: colors.dark2,paddingVertical: 10, fontSize: 16,color: colors.black}} 
                                                placeholder={'Masukan Email Ortu'} 
                                                placeholderTextColor={colors.default}
                                                value={EmailOrtu}
                                                onChangeText={(e) => dispatch(setFormProfil('EmailOrtu', e))}
                                            />
                                        </View>
                                        
                                        <Gap height={20} />

                                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[500]}}>Nomor Handphone</Text>
                                        <View style={{alignItems:'center'}}>
                                            <TextInput 
                                                style={{width:'100%', borderBottomWidth:1,borderBottomColor: colors.dark2,paddingVertical: 10, fontSize: 16,color: colors.black}} 
                                                placeholder={'Masukan Nomor Handphone'} 
                                                placeholderTextColor={colors.default}
                                                value={NoHandphone}
                                                onChangeText={(e) => dispatch(setFormProfil('NoHandphone', e))}
                                            />
                                        </View>

                                        <Gap height={40} />

                                        <TouchableOpacity onPress={() => btnPerbaruiData()} style={styles.btn_primary}>
                                            <IconUploadWhite />
                                            <Text style={styles.btn_primary_text}>Perbarui Data</Text>
                                        </TouchableOpacity>
                                        <Gap height={20} />
                                    </View>
                                </View>
                            </ScrollView>
                            : null
                        
                    }
                    
                </View>
            </View>
            <Modalize
                ref={modalFotoProfilReff}
                modalHeight={hp('25%')}
                handlePosition="inside"
            >
                <View style={{ paddingHorizontal: 10, paddingVertical: 20, height: hp('24%')}}>
                    <Text style={{ color: colors.black, fontFamily: fonts.primary[700], fontSize: 18 }}>Pilih File</Text>
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginHorizontal: wp('10%'), top:20 }}>
                        <View style={{ justifyContent: 'space-between', flexDirection: 'row', width: '100%', flexWrap: 'wrap' }}>
                            <TouchableOpacity onPress={() => btnCamera()} style={{ width: '50%', alignItems: 'center', marginBottom: 5 }}>
                                <View style={{ width: 58, height: 58, borderWidth: 1, backgroundColor: '#03a9f4', borderColor: '#fbfbfb', borderRadius: 100, justifyContent: 'center', alignItems: 'center' }}>
                                    <IconCameraWhite />
                                </View>
                                <Text style={{ fontSize: 12, fontFamily: fonts.primary[600], textAlign: 'center', marginTop: 2 }}>Kamera</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => btnGallery()} style={{ width: '50%', alignItems: 'center', marginBottom: 5 }}>
                                <View style={{ width: 58, height: 58, borderWidth: 1, backgroundColor: colors.yellow1, borderColor: '#fbfbfb', borderRadius: 100, justifyContent: 'center', alignItems: 'center' }}>
                                    <IconImageWhite />
                                </View>
                                <Text style={{ fontSize: 12, fontFamily: fonts.primary[600], textAlign: 'center', marginTop: 2 }}>Galeri</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modalize>
        </>
    )
}

export default Profil

const styles = StyleSheet.create({
    android_safearea: {
        flex: 1,
        backgroundColor: colors.primary
    },
    content: {
        flex: 1,
        backgroundColor : colors.white,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    btn_primary: {
        height: 40, 
        backgroundColor: colors.primary, 
        borderRadius: 7, 
        justifyContent: 'center', 
        alignItems: 'center',
        flexDirection:'row'
    },
    btn_primary_text: {
        fontFamily: fonts.primary[700],
        color: colors.white,
        fontSize: 15,
        marginLeft: 5
    },
})
