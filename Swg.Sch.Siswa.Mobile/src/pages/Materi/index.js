import React, {useState, useRef, useEffect, Fragment} from 'react';
import { Text,
    View,
    TouchableOpacity,
    Image,
    StyleSheet,
    ScrollView,
    StatusBar,
    useWindowDimensions,
    PermissionsAndroid,
    Animated
} from 'react-native';
import { colors } from '../../utils/colors';
import { fonts } from '../../utils';
import {
    IconRefreshDark,
    IconBackDark,
    ILBg2,
    IconMenuTutorial,
    IconRight,
    IconMenuLearning,
    IconInfoResiAuto,
    ILEbook,
    IconChatProduk,
    IconKeranjangAdd,
    IconMenuPembayaran,
    IconBack,
    IconMore,
    IconMenuIklan,
    ILNilaiKosong,
    IconMateriVideo,
    IconMateriImage,
    IconMateriFile
} from '../../assets';
import { Gap } from '../../components';
import { ParallaxImage } from 'react-native-snap-carousel';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import HTML from "react-native-render-html";
import { WebView } from 'react-native-webview';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import RBSheet from "react-native-raw-bottom-sheet";
import { Modalize } from 'react-native-modalize';
import { SwipeablePanel } from 'rn-swipeable-panel';
import YouTube, { YouTubeStandaloneIOS, YouTubeStandaloneAndroid } from 'react-native-youtube';
import YoutubePlayer from "react-native-youtube-iframe";
import RNFetchBlob from 'rn-fetch-blob';
import { FUNCDownloadFile, FUNCGetExtention, FUNCGetExtentionYt, FUNCToast } from '../../config/function';
import { BASEURL } from '../../config/helpers';


const Materi = ({navigation, route}) => {
    const { dataElearning } = route.params;
    const contentWidth = useWindowDimensions().width;
    const modalizeRef = useRef(null);
    return (
        <>
            <StatusBar translucent barStyle="light-content" backgroundColor="transparent"  />
            <View style={styles.androidSafeArea}>
                <Gap height={getStatusBarHeight()} />
                <View style={styles.header_body}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <TouchableOpacity onPress={() => navigation.goBack()}>
                                <IconBack />
                            </TouchableOpacity>
                            <Text numberOfLines={1} style={{ fontSize: 18, color: colors.white, fontFamily: fonts.primary[700], left: 10, maxWidth:wp('70%') }} numberOfLines={1}>Materi :: {dataElearning.NamaMapel}</Text>
                        </View>
                        <View>
                        </View>
                </View>

                <ScrollView>
                    <View style={{ paddingHorizontal: 10 }}>
                        <View style={styles.cardMenu}>
                            <View style={{ padding: 10 }}>
                                <Text numberOfLines={1} style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Judul : </Text>
                                <HTML defaultTextProps={{selectable:true}} source={{ html: dataElearning.JudulMateri || '<p>tidak ada materi</p>' }} contentWidth={contentWidth} />
                            </View>
                        </View>

                        <View style={styles.cardMenu}>
                            <View style={{ padding: 10 }}>
                                <Text numberOfLines={1} style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Penjelasan : </Text>
                                <HTML defaultTextProps={{selectable:true}} source={{ html: dataElearning.Deskripsi || '<p>tidak ada materi</p>' }} contentWidth={contentWidth} />
                            </View>
                        </View>
                        <Gap height={hp('13%')}/>
                    </View>
                </ScrollView>
                
            </View>
            <Modalize
                ref={modalizeRef}
                alwaysOpen={hp('13%')}
                handlePosition="inside"
            >
                <ScrollView>
                    <View style={{ padding: 10 }}>
                        
                        {
                            dataElearning.NamaFileMateri !== null ?
                                <>
                                    <Text numberOfLines={1} style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>File : </Text>
                                    <Gap height={10} />
                                    <TouchableOpacity onPress={()=> FUNCDownloadFile(`${BASEURL}/Asset/Files/Elearning/${dataElearning.NamaFileMateri}`)} style={styles.cardMenu}>
                                        <View style={{ flexDirection: 'row', padding: 10, alignItems: 'center', justifyContent: 'space-between' }}>
                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                    <View style={{ padding: 8 }}>
                                                        <IconMateriFile />
                                                    </View>
                                                </View>
                                                <View style={{left:10}}>
                                                    <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>File Materi</Text>
                                                </View>
                                            </View>
                                            <View>
                                                <IconRight />
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </>
                                : null
                        }

                        {
                            dataElearning.FileKbmMedias.length !== 0 ?
                                dataElearning.FileKbmMedias.map((v, i) => {
                                    return (
                                        v.IdJenisPathFile === 2 ?
                                            <Fragment key={i}>
                                                {
                                                    i === 0 && <Text numberOfLines={1} style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Video : </Text>
                                                }
                                                
                                                <Gap height={10} />
                                                <TouchableOpacity onPress={() => navigation.navigate('DetilVidio', {videoId : FUNCGetExtentionYt(v.PathFileUrl)[0]})} style={styles.cardMenu}>
                                                    <View style={{ flexDirection: 'row', padding: 10, alignItems: 'center', justifyContent: 'space-between' }}>
                                                        <View style={{flexDirection:'row', alignItems:'center'}}>
                                                            <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                                <View style={{ padding: 8 }}>
                                                                    <IconMateriVideo />
                                                                </View>
                                                            </View>
                                                            <View style={{left:10}}>
                                                                <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Video {i+1}</Text>
                                                            </View>
                                                        </View>
                                                        <View>
                                                            <IconRight />
                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                                
                                            </Fragment>
                                            : null
                                                
                                    )
                                    
                                })
                                : null
                            
                        }

                        {
                            dataElearning.FileKbmMedias.length !== 0 ?
                                dataElearning.FileKbmMedias.map((v, i) => {
                                    return (
                                        v.IdJenisPathFile === 1 ?
                                            <Fragment key={i}>
                                                {
                                                    i === 0 && <Text numberOfLines={1} style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Gambar : </Text>
                                                }
                                                <Gap height={10} />
                                                <TouchableOpacity onPress={() => navigation.navigate('DetilGambar', {link : `${BASEURL}/Asset/Files/Elearning/${v.PathFileUrl}`})} style={styles.cardMenu}>
                                                    <View style={{ flexDirection: 'row', padding: 10, alignItems: 'center', justifyContent: 'space-between' }}>
                                                        <View style={{flexDirection:'row', alignItems:'center'}}>
                                                            <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                                <View style={{ padding: 8 }}>
                                                                    <IconMateriImage />
                                                                </View>
                                                            </View>
                                                            <View style={{left:10}}>
                                                                <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Gambar {i}</Text>
                                                            </View>
                                                        </View>
                                                        <View>
                                                            <IconRight />
                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                                
                                            </Fragment>
                                            : null
                                                
                                    )
                                    
                                })
                                : null
                            
                        }

                        {
                            dataElearning.TotFileKbmUrl === 0 && dataElearning.TotFileKbmFile === 0 && dataElearning.NamaFileMateri === null ?
                                <View style={{ flex: 1, alignItems: 'center', alignContent: 'center', justifyContent: 'center', backgroundColor:colors.white, borderTopLeftRadius: 20, borderTopRightRadius:20, height:hp('90%') }}>
                                    <View style={{width: 200, height: 200, marginTop:-100}}>
                                        <Image source={ILNilaiKosong} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                                    </View>
                                    <Gap height={50}/>
                                    <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[400] }}>Yey, tidak ada file maupun vidio</Text>
                                </View>
                                : null
                        }
                        

                        
                       
                    </View>
                </ScrollView>
            </Modalize>
            
        </>
    )
}

export default Materi

const styles = StyleSheet.create({
    androidSafeArea: {
        flex: 1,
        backgroundColor: colors.primary
    },
    cardMenu: {
        backgroundColor: colors.white,
        borderRadius:10,
        elevation:2,
        marginBottom: 10,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2,
    },
    header_body: {
        flexDirection: 'row',
        paddingVertical:18,
        paddingHorizontal:10,
        alignItems: 'center',
        justifyContent: 'space-between'
    },  
    content: {
        flex: 1,
    },

    icon_body: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap'
    },

    detil_card: {
        flex: 1,
        backgroundColor: colors.white,
        borderRadius: 0,
        paddingVertical: 15,
    },
    notice_card: {
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#F2DB73',
        // paddingHorizontal: 10,
        paddingVertical: 10
    },
    notice_icon: {
        width: wp('20%'),
        alignItems: 'center'
    },
    notice_desc: {
        // maxWidth: wp('90%'),
        // paddingRight: 10
    },
    notice_desc_font: {
        fontSize:13,
        fontFamily:fonts.primary[400],
        color: colors.black,
        maxWidth:wp('70%')
    },
    bottomCheckout: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
        paddingHorizontal: 15,
        paddingVertical: 10,
        backgroundColor: colors.white,
        borderWidth: 0.6,
        borderColor: '#dcdcdc',
        alignItems: 'center',
    },

})
