import React, {useState, useRef, useEffect} from 'react';
import {
    Text, 
    View, 
    Image, 
    StatusBar, 
    StyleSheet, 
    ScrollView, 
    TouchableOpacity, 
    Platform,
    ImageBackground
} from 'react-native';
import { colors } from '../../utils/colors';
import { fonts } from '../../utils';
import { IconBack, IconFPaketData, IconHome, ILTagihanBerjalan, ILTagihanSebelum, IconRefresh, ILMutabaah, IconLogin, IconCoRight, IconUploadWhite, ILLoading, Loading, IconDateWhite, ILBg2, ILLogo, IconSearchWhite, IconMutabaahList } from '../../assets';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Gap, HeaderCard, LoadingCard } from '../../components';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Modalize } from 'react-native-modalize';
import { useDispatch, useSelector } from 'react-redux';
import BouncyCheckbox from "react-native-bouncy-checkbox";
import { apiAddMutabaah, setDataMutabaahs } from '../../config/redux/action';
import { useIsFocused } from '@react-navigation/core';
import { FUNCDateToString } from '../../config/function';
import DateTimePicker from '@react-native-community/datetimepicker';

const Mutabaah = ({navigation}) => {
    const modalDetailRef = useRef(null);
    const modalDateRef = useRef(null);
    const { dataMutabaahs } = useSelector(state => state.mutabaahReducer);
    const dispatch = useDispatch();
    const [dataMut, setDataMut] = useState([]);
    const [hari, setHari] = useState('memuat...');
    const [date, setDate] = useState(new Date());
    const [show, setShow] = useState(false);

    const isFocused = useIsFocused();
    useEffect(() => {
        dispatch(setDataMutabaahs(FUNCDateToString(date)))
    }, [dispatch, isFocused])
    
    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };

    const btnOpenModal = (data, hari) => {
        modalDetailRef.current?.open()
        console.log('dataMutabaahs : ', data);
        setDataMut(data)
        setHari(hari)
    }
    const btnSimpanMutabaah = () => {
        dispatch(apiAddMutabaah(dataMut, FUNCDateToString(date)));
        // console.log('saveMut : ', dataMut)
    }
    const btnReload = () => {
        dispatch(setDataMutabaahs(FUNCDateToString(date)));
        modalDateRef.current.close()
    }

    const onCheck = (idJenisEvaluasi, isDone ) => {
        let newArr = dataMut.map((item, i) => {
            if (idJenisEvaluasi == item.idJenisEvaluasi) {
                return { ...item, ["isDone"]:  isDone ? 'Y':'T'};
            } else {
                return item;
            }
        });
        setDataMut(newArr)
    }
   
    return (
        <>
            <StatusBar translucent barStyle="light-content" backgroundColor="transparent"  />
            <View style={{ flex: 1, backgroundColor: colors.primary }}>
                <HeaderCard
                    statusBar={getStatusBarHeight()}
                    icon={<IconDateWhite />}
                    onPress={() => modalDateRef.current?.open()}
                    title={'Mutabaah'}
                    isBack={true}
                    onPressBack={() => navigation.goBack()}
                />
                
                <View style={styles.content}>
                    
                    {
                        dataMutabaahs ?
                            <ScrollView>
                                <View style={{ paddingHorizontal: 10, paddingTop: 15 }}>
                                    <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10, backgroundColor:colors.primary, borderRadius:10}}>
                                        <View style={{paddingVertical:15, paddingHorizontal:5}}>
                                            <Text style={{ fontSize: 18, color: colors.white, fontFamily: fonts.primary[700], maxWidth: wp('55%') }}>Mutabah dimulai tanggal sampai tanggal</Text>
                                            <Gap height={hp('3%')} />
                                            <Text style={{ fontSize: 14, color: colors.white, fontFamily: fonts.primary[700], maxWidth: wp('60%') }}>{dataMutabaahs.tglMulai} s/d {dataMutabaahs.tglSelesai}</Text>
                                        </View>
                                        <View>
                                            <View style={{width: 120, height: 110}}>
                                                <Image source={ILMutabaah} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1}} />
                                            </View> 
                                        </View>
                                    </View>
                                    {/* <ImageBackground source={{uri: 'https://plongsite.files.wordpress.com/2019/09/20190913_1029028179281658937593014.jpg'}} imageStyle={{ borderRadius: 10, opacity:0.7}}  style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10, backgroundColor:colors.black, borderRadius:10}}>
                                        <View style={{paddingVertical:15, paddingHorizontal:5}}>
                                            <Text style={{ fontSize: 20, color: colors.white, fontFamily: fonts.primary[700], maxWidth: wp('55%') }}>Mutabah dimulai tanggal sampai tanggal</Text>
                                            <Gap height={hp('3%')} />
                                            <Text style={{ fontSize: 15, color: colors.white, fontFamily: fonts.primary[700], maxWidth: wp('60%') }}>{dataMutabaahs.tglMulai} s/d {dataMutabaahs.tglSelesai}</Text>
                                        </View>
                                        <View>
                                            <View style={{width: 100, height: 100}}>
                                                <Image source={ILLogo} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                                            </View> 
                                        </View>
                                    </ImageBackground> */}
                                </View>
                                <View style={{ paddingHorizontal: 10, paddingVertical:15 }}>
                                    <TouchableOpacity onPress={() => btnOpenModal(dataMutabaahs.minggu, 'Ahad')} style={styles.cardMenu}>
                                        <View style={{ flexDirection: 'row', paddingHorizontal: 10, paddingVertical: 10, justifyContent: 'space-between', alignItems: 'center' }}>
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                    <View style={{ padding: 8 }}>
                                                        <IconMutabaahList />
                                                    </View>
                                                </View>
                                                <View style={{marginLeft:10}}>
                                                    <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Ahad</Text>
                                                    <Gap height={5} />
                                                    <Text style={{ fontSize: 13, color: colors.dark1, fontFamily: fonts.primary[400] }}>{dataMutabaahs.tglAhad}</Text>
                                                </View>
                                            </View>
                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <Text style={{ fontSize: 13, color: colors.primary, fontFamily: fonts.primary[600]}}>Lihat </Text>
                                                <IconCoRight />
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => btnOpenModal(dataMutabaahs.senin, 'Senin')} style={styles.cardMenu}>
                                        <View style={{flexDirection:'row', paddingHorizontal:10, paddingVertical:10, justifyContent:'space-between', alignItems:'center'}}>
                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                    <View style={{ padding: 8 }}>
                                                        <IconMutabaahList />
                                                    </View>
                                                </View>
                                                <View style={{marginLeft:10}}>
                                                    <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Senin</Text>
                                                    <Gap height={5} />
                                                    <Text style={{ fontSize: 13, color: colors.dark1, fontFamily: fonts.primary[400] }}>{dataMutabaahs.tglSenin}</Text>
                                                </View>
                                            </View>
                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <Text style={{ fontSize: 13, color: colors.primary, fontFamily: fonts.primary[600]}}>Lihat </Text>
                                                <IconCoRight />
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => btnOpenModal(dataMutabaahs.selasa, 'Selasa')} style={styles.cardMenu}>
                                        <View style={{flexDirection:'row', paddingHorizontal:10, paddingVertical:10, justifyContent:'space-between', alignItems:'center'}}>
                                           <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                    <View style={{ padding: 8 }}>
                                                        <IconMutabaahList />
                                                    </View>
                                                </View>
                                                <View style={{marginLeft:10}}>
                                                    <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Selasa</Text>
                                                    <Gap height={5} />
                                                    <Text style={{ fontSize: 13, color: colors.dark1, fontFamily: fonts.primary[400] }}>{dataMutabaahs.tglSelasa}</Text>
                                                </View>
                                            </View>
                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <Text style={{ fontSize: 13, color: colors.primary, fontFamily: fonts.primary[600]}}>Lihat </Text>
                                                <IconCoRight />
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => btnOpenModal(dataMutabaahs.rabu, 'Rabu')} style={styles.cardMenu}>
                                        <View style={{flexDirection:'row', paddingHorizontal:10, paddingVertical:10, justifyContent:'space-between', alignItems:'center'}}>
                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                    <View style={{ padding: 8 }}>
                                                        <IconMutabaahList />
                                                    </View>
                                                </View>
                                                <View style={{marginLeft:10}}>
                                                    <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Rabu</Text>
                                                    <Gap height={5} />
                                                    <Text style={{ fontSize: 13, color: colors.dark1, fontFamily: fonts.primary[400] }}>{dataMutabaahs.tglRabu}</Text>
                                                </View>
                                            </View>
                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <Text style={{ fontSize: 13, color: colors.primary, fontFamily: fonts.primary[600]}}>Lihat </Text>
                                                <IconCoRight />
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => btnOpenModal(dataMutabaahs.kamis, 'Kamis')} style={styles.cardMenu}>
                                        <View style={{flexDirection:'row', paddingHorizontal:10, paddingVertical:10, justifyContent:'space-between', alignItems:'center'}}>
                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                    <View style={{ padding: 8 }}>
                                                        <IconMutabaahList />
                                                    </View>
                                                </View>
                                                <View style={{marginLeft:10}}>
                                                    <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Kamis</Text>
                                                    <Gap height={5} />
                                                    <Text style={{ fontSize: 13, color: colors.dark1, fontFamily: fonts.primary[400] }}>{dataMutabaahs.tglKamis}</Text>
                                                </View>
                                            </View>
                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <Text style={{ fontSize: 13, color: colors.primary, fontFamily: fonts.primary[600]}}>Lihat </Text>
                                                <IconCoRight />
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => btnOpenModal(dataMutabaahs.jumat, 'Jumat')} style={styles.cardMenu}>
                                        <View style={{flexDirection:'row', paddingHorizontal:10, paddingVertical:10, justifyContent:'space-between', alignItems:'center'}}>
                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                    <View style={{ padding: 8 }}>
                                                        <IconMutabaahList />
                                                    </View>
                                                </View>
                                                <View style={{marginLeft:10}}>
                                                    <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Jum'at</Text>
                                                    <Gap height={5} />
                                                    <Text style={{ fontSize: 13, color: colors.dark1, fontFamily: fonts.primary[400] }}>{dataMutabaahs.tglJumat}</Text>
                                                </View>
                                            </View>
                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <Text style={{ fontSize: 13, color: colors.primary, fontFamily: fonts.primary[600]}}>Lihat </Text>
                                                <IconCoRight />
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => btnOpenModal(dataMutabaahs.sabtu, 'Sabtu')} style={styles.cardMenu}>
                                        <View style={{flexDirection:'row', paddingHorizontal:10, paddingVertical:10, justifyContent:'space-between', alignItems:'center'}}>
                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                    <View style={{ padding: 8 }}>
                                                        <IconMutabaahList />
                                                    </View>
                                                </View>
                                                <View style={{marginLeft:10}}>
                                                    <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Sabtu</Text>
                                                    <Gap height={5} />
                                                    <Text style={{ fontSize: 13, color: colors.dark1, fontFamily: fonts.primary[400] }}>{dataMutabaahs.tglSabtu}</Text>
                                                </View>
                                            </View>
                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                <Text style={{ fontSize: 13, color: colors.primary, fontFamily: fonts.primary[600]}}>Lihat </Text>
                                                <IconCoRight />
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </ScrollView>

                                
                            : <LoadingCard />
                    }
                </View>

            </View>
            {
                dataMutabaahs ?
                    <Modalize
                        ref={modalDetailRef}
                        modalHeight={hp('90%')}
                        handlePosition="inside"
                        snapPoint={hp('30%')}
                    >
                        <View style={styles.modal_content}>
                            <Text style={styles.modal_title}>Daftar Mutabaah ({hari})</Text>
                            <Gap height={15} />
                            {
                                dataMut.length !== 0 ?
                                    <ScrollView showsVerticalScrollIndicator={false}>
                                        {
                                            dataMut.map((v, i) => {
                                                return (
                                                    <View key={i} style={{padding:5}}>
                                                        <View  style={{ flexDirection: 'row', justifyContent: 'space-between',  borderBottomWidth:0.2, borderBottomColor:colors.dark2, paddingBottom:10, alignItems:'center' }}>
                                                            <Text style={{ fontSize: 14, color: colors.black, fontFamily: fonts.primary[500], maxWidth: wp('70%') }}>{v.jenisEvaluasi}</Text>
                                                            <BouncyCheckbox onPress={(isChecked) => onCheck(v.idJenisEvaluasi, isChecked)} isChecked={v.isDone === "Y" ? true : false}/>
                                                        </View>
                                                    </View>
                                                )
                                            })
                                        }
                                        
                                    
                                    <Gap height={hp('7%')}/>
                                    </ScrollView>
                                    : null
                            }
                            
                            <View style={{position:'absolute', bottom:0, width:wp('100%') }}>
                                <TouchableOpacity onPress={() => btnSimpanMutabaah()} style={{
                                    backgroundColor: colors.primary,
                                    justifyContent: 'center', 
                                    alignItems: 'center', 
                                    width: wp('100%'), 
                                    flexDirection: 'row', 
                                    paddingVertical: Platform.OS === 'ios' ? 20 : 15, 
                                    paddingBottom: 25
                                }}>
                                    <IconUploadWhite />
                                    <Text style={{ color: colors.white, fontFamily: fonts.primary[700], fontSize: 15, marginLeft: 5 }}>Simpan Mutabaah</Text>
                                </TouchableOpacity>
                            </View>

                        </View>
                    </Modalize>
                    :null
            }

            <Modalize
                ref={modalDateRef}
                modalHeight={hp('30%')}
                handlePosition="inside"
            >
                <View style={{padding: 10, top: 10, height:hp('29%')}}>
                    <Text style={styles.modal_title}>Pilih Tanggal</Text>
                    <Gap height={15} />
                    <TouchableOpacity onPress={() => setShow(true)} style={styles.modal_com_input}>
                        {Platform.OS !== 'ios' ?
                            <Text style={styles.modal_com_input_text}>{FUNCDateToString(date)}</Text>
                            :
                            null
                        }
                        {show && (
                            <DateTimePicker
                                testID="dateTimePicker"
                                value={date}
                                mode={'date'}
                                is24Hour={true}
                                display="default"
                                onChange={onChange}
                                maximumDate={new Date()}
                            />
                        )}
                    </TouchableOpacity>
                    <Gap height={20} />
                    <View style={{position:'absolute', bottom:0 }}>
                        <TouchableOpacity onPress={() => btnReload()} style={{ backgroundColor: colors.primary, justifyContent: 'center', alignItems: 'center', width: wp('100%'), flexDirection: 'row', paddingVertical: Platform.OS === 'ios' ? 20 : 15, paddingBottom:25 }}>
                            <IconSearchWhite />
                            <Text style={{ color: colors.white, fontFamily: fonts.primary[700], fontSize: 15, left: 5 }}>Pencarian</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modalize>
        </>
    )
}

export default Mutabaah

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor : colors.white,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    modal_content: {
        padding: 10,
        top: 10,
        height:hp('90%')
    },
    modal_title: {
        fontSize: 18, 
        color: colors.black, 
        fontFamily: fonts.primary[600]
    },
    modal_com_input: {
        width: '100%', 
        height:40, 
        backgroundColor: colors.white, 
        borderWidth:0.5, 
        borderColor:colors.dark2, 
        borderRadius: 5, 
        justifyContent:'center' 
    },
    modal_com_input_text: {
        fontSize: 13,
        color: colors.black,
        fontFamily: fonts.primary[400],
        padding: 10
    },
    cardMenu: {
        backgroundColor: colors.white,
        borderRadius:10,
        elevation:2,
        marginBottom: 10,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2,
    },
    container: {
        flexDirection: 'row',
        padding: 16,
        borderBottomWidth: 0.5,
        borderBottomColor: colors.dark2,
        alignItems: 'center',
    },
    avatar: {
        width: 46,
        height: 46,
        borderRadius: 46/2,
        marginRight: 12,
        backgroundColor: colors.primary
    },
    name: {
        fontSize: 16,
        fontFamily: fonts.primary.normal,
        color: colors.black,
    },
    desc: {
        fontSize: 12,
        fontFamily: fonts.primary[300],
        color: colors.text.secondary,
    }
})
