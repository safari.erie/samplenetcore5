import * as React from 'react';
import { Text, View, TouchableOpacity, StatusBar, StyleSheet, ScrollView,Dimensions } from 'react-native';
import { colors } from '../../utils/colors';
import { fonts } from '../../utils';
import { IconFPaketData, IconHome, IconMore, IconMoreActive, IconRefresh, ILLogo } from '../../assets';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Gap, HeaderCard, Avatar } from '../../components';
import { VERSIAPP } from '../../config/helpers';
const Version = ({navigation}) => {
    return (
        <>
            <StatusBar translucent barStyle="light-content" backgroundColor="transparent"  />
            <View style={styles.android_safearea}>
              
                <HeaderCard
                    statusBar={getStatusBarHeight()}
                    title={'Version'}
                    isBack={true}
                    onPressBack={() => navigation.goBack()}
                />
                
                <View style={styles.content}>
                    <View style={{flex:1, justifyContent:'center', alignItems:'center', marginTop:-30}}>
                        <Avatar source={ILLogo} size={100} />
                        <Gap height={40} />
                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[700] }}>#ATTAUFIQ {VERSIAPP}</Text>
                        <Gap height={10} />
                        <Text style={{ fontSize: 13, color: colors.dark1, fontFamily: fonts.primary[500]}}>MADE WITH SEKOLAH ATTAUFIQ</Text>
                    </View>
                </View>
            </View>
        </>
    )
}

export default Version

const styles = StyleSheet.create({
    android_safearea: {
        flex: 1,
        backgroundColor: colors.primary
    },
    content: {
        flex: 1,
        backgroundColor : colors.white,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
})
