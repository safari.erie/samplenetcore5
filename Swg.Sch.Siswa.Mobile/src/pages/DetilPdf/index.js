import React from 'react'
import { StatusBar, TouchableOpacity, StyleSheet, Dimensions, View } from 'react-native'
import { colors } from '../../utils'
import ImageZoom from 'react-native-image-pan-zoom';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Button } from '../../components';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Gap, HeaderCard } from '../../components';
import { IconBack, IconDownloadWhite } from '../../assets';
import { FUNCDownloadFile } from '../../config/function';
import Pdf from 'react-native-pdf';


const DetilPdf = ({ navigation, route }) => {
    const { source } = route.params;
    return (
        <>
            <StatusBar translucent barStyle="light-content" backgroundColor="transparent"  />
            <View style={{ flex: 1, backgroundColor: colors.black }}>
                <HeaderCard
                    statusBar={getStatusBarHeight()}
                    title={'Kembali'}
                    icon={<IconDownloadWhite />}
                    onPress={() => FUNCDownloadFile(source.uri)}
                    isBack={true}
                    onPressBack={() => navigation.goBack()}
                />
                <Pdf
                    source={source}
                    onLoadComplete={(numberOfPages,filePath)=>{
                        // console.log(`number of pages: ${numberOfPages}`);
                    }}
                    onPageChanged={(page,numberOfPages)=>{
                        // console.log(`current page: ${page}`);
                    }}
                    onError={(error)=>{
                        // console.log(error);
                    }}
                    onPressLink={(uri)=>{
                        // console.log(`Link presse: ${uri}`)
                    }}
                    style={styles.pdf}
                />            
            </View>
        </>
    )
}

export default DetilPdf


const styles = StyleSheet.create({
    header_body: {
        flexDirection: 'row',
        paddingVertical:18,
        paddingHorizontal:10,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
    },
})
