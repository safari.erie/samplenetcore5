import React, { useState, useEffect, useRef } from 'react';
import {
    Dimensions,
    StyleSheet,
    ScrollView,
    View,
    Text,
    SafeAreaView,
    Platform,
    TouchableOpacity,
    Image,
    ImageBackground,
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import { colors } from '../../utils/colors';
import { fonts } from '../../utils';
import { Gap } from '../../components';
import { IconNotifikasi,
    ILHospitalBG,
    IconStar,
    IconPromo,
    IconBronze,
    IconRefresh,
    ILBg1,
    ILBg2,
    IconMenuKeranjang,
    IconMenuPembayaran,
    IconMenuProduk,
    IconMenuPoin,
    IconMenuLearning,
    IconMenuTutorial,
    IconMenuIklan,
    IconMenuPembelian,
} from '../../assets';
import OneSignal from 'react-native-onesignal';
import Geolocation from '@react-native-community/geolocation';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Carousel, { ParallaxImage, Pagination } from 'react-native-snap-carousel';
import Axios from 'axios';
import { URLService } from '../../services';
import Tips from 'react-native-root-tips';


const ENTRIES1 = [
    {
      title: 'Beautiful and dramatic Antelope Canyon',
      subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
      illustration: 'https://dropshipedia.id//assets/images/banner/Image.jpg',
    },
    {
      title: 'Earlier this morning, NYC',
      subtitle: 'Lorem ipsum dolor sit amet',
      illustration: 'https://dropshipedia.id//assets/images/banner/Image1.jpg',
    },
    {
      title: 'White Pocket Sunset',
      subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
      illustration: 'https://dropshipedia.id//assets/images/banner/Image2.jpg',
    },
];
const {width: screenWidth} = Dimensions.get('window');
  

const Home = ({ navigation }) => {
    const [entries, setEntries] = useState([]);
    const [produk, setProduk] = useState([]);
    const [activeSlide, setActiveSlide] = useState(0);
    const carouselRef = useRef(null);
    const {width,height} = Dimensions.get('window')

    useEffect(() => {
        console.log(height/2.5)
        setEntries(ENTRIES1);

        getData()
        Geolocation.getCurrentPosition(info => console.log('loc -> ', info));
        OneSignal.init('AppId')
        OneSignal.addEventListener('ids', onIds)
        return () => { 
            OneSignal.removeEventListener('ids', onIds)
        }
        
    }, [])

    const renderItem = ({item}, parallaxProps) => {
        return (
          <TouchableOpacity style={styles.item}>
            <ParallaxImage
              source={{uri: item.illustration}}
              containerStyle={styles.imageContainer}
              style={styles.image}
              parallaxFactor={0.4}
              {...parallaxProps}
            />
            
          </TouchableOpacity>
        );
    };

    const onIds = (device) => {
        console.log('device Id -> ',device)
    }

    const getData = async () => {
        try {
            const value = await AsyncStorage.getItem('@token')
            if (value !== null) {
                Axios.get(`${URLService}/Mobile/ProdukTayang/`, { headers: { 'token': value } }).then((res) => {
                    let data = res.data;
                    if (data.IsSuccess) {
                        setProduk(data.Data);
                    } else {
                        Tips.showWarn(data.ReturnMessage, {
                            duration: 2000,
                            position: Tips.positions.CENTER,
                            shadow: false,
                            animation: true,
                            hideOnPress: false,
                            mask: true,
                            maskColor: colors.black,
                            delay: 0
                        });
                    }
                }).catch((err) => {
                    JSON.stringify(err.message);
                    Tips.showWarn(err.message, {
                        duration: 2000,
                        position: Tips.positions.CENTER,
                        shadow: false,
                        animation: true,
                        hideOnPress: false,
                        mask: true,
                        maskColor: colors.black,
                        delay: 0
                    });
                });
            //   const value = await AsyncStorage.removeItem('@token')
            console.log('token -> ', value)
            } else {
                // navigation.replace('Welcome');
            }
        } catch(e) {
            Tips.showWarn('err get data', {
                duration: 2000,
                position: Tips.positions.CENTER,
                shadow: false,
                animation: true,
                hideOnPress: false,
                mask: true,
                maskColor: colors.black,
                delay: 0
            });
        }
    }




  return (
    <>
        <SafeAreaView style={styles.androidSafeArea}>
            <ImageBackground source={ILBg1} imageStyle={{height:'70%'}} style={styles.page}>
               
                <ScrollView>
                    <View style={styles.header_card}>
                        <Carousel
                            layout={'default'}
                            loop={true}
                            ref={carouselRef}
                            sliderWidth={screenWidth}
                            sliderHeight={screenWidth}
                            itemWidth={wp('100%')}
                            data={entries}
                            renderItem={renderItem}
                            onSnapToItem={index => setActiveSlide(index)}
                            hasParallaxImages={true}
                          />
                           <View style={styles.header_body}>
                                <View style={styles.header_content_search}>
                                    <TouchableOpacity onPress={() => alert('ok')} style={styles.header_content_search_font}>
                                        <Text style={{fontSize:15,color:colors.dark1, fontFamily:fonts.primary[400]}}> Pencarian</Text>
                                    </TouchableOpacity>
                                    {/* <TextInput  style={styles.header_content_search_font} placeholder="Mau cari apa?" /> */}
                                    <IconPromo style={{position: 'absolute', left: 12}} />
                                </View>
                                <View style={styles.header_content_icon}>
                                    <IconNotifikasi style={{right:10}} />
                                    <IconRefresh style={{left:0}}/>
                                </View>
                            </View>
                        <Pagination
                            dotsLength={entries.length}
                            activeDotIndex={activeSlide}
                            containerStyle={{  paddingVertical:5, justifyContent:'flex-start', marginTop:-20}}
                            dotStyle={{
                                width: 10,
                                height: 10,
                                borderRadius: 5,
                                marginHorizontal: 2,
                                backgroundColor: 'rgba(255, 255, 255, 0.92)'
                            }}
                            inactiveDotStyle={{
                                // Define styles for inactive dots here
                            }}
                            inactiveDotOpacity={0.4}
                            inactiveDotScale={0.6}
                        />
                    </View>
                   
                    <View style={styles.content}>
                        
                        <View style={styles.icon_card}>
                              <View style={styles.icon_body}>
                                  <View style={styles.icon_content_left}>  
                                    <IconBronze />
                                    <Text style={styles.icon_content_font}>Poin : 14 </Text>
                                </View>
                                <View style={styles.icon_content_right}>  
                                    <IconPromo />
                                    <Text style={styles.icon_content_font}>Kupon : 1 </Text>
                                </View>
                            </View>
                        </View>
                          
                        <Gap height={20} /> 
                          
                        <View style={styles.menu_card}>
                            <View style={styles.menu_body}>
                                <View style={styles.menu_content}>
                                    <TouchableOpacity onPress={() => navigation.navigate('DetilProduk')} style={styles.menu_content_icon}>
                                        <IconMenuProduk />
                                    </TouchableOpacity>
                                    <Text style={styles.menu_content_font}>Produk</Text>
                                </View>
                                <View style={styles.menu_content}>
                                    <TouchableOpacity onPress={() => navigation.navigate('Pulsa')} style={styles.menu_content_icon}>
                                        <IconMenuPembelian />
                                    </TouchableOpacity>
                                    <Text style={styles.menu_content_font}>Pembelian</Text>
                                </View>
                                <View style={styles.menu_content}>
                                    <TouchableOpacity onPress={() => navigation.navigate('Pulsa')} style={styles.menu_content_icon}>
                                        <IconMenuPembayaran />
                                    </TouchableOpacity>
                                    <Text style={styles.menu_content_font}>Pembayaran</Text>
                                </View>
                                <View style={styles.menu_content}>
                                    <TouchableOpacity onPress={() => navigation.navigate('Pulsa')} style={styles.menu_content_icon}>
                                        <IconMenuKeranjang />
                                    </TouchableOpacity>
                                    <Text style={styles.menu_content_font}>Keranjang</Text>
                                </View>
                                <View style={styles.menu_content}>
                                    <TouchableOpacity onPress={() => navigation.navigate('Pulsa')} style={styles.menu_content_icon}>
                                        <IconMenuPoin />
                                    </TouchableOpacity>
                                    <Text style={styles.menu_content_font}>Poin</Text>
                                </View>
                                <View style={styles.menu_content}>
                                    <TouchableOpacity onPress={() => navigation.navigate('Pulsa')} style={styles.menu_content_icon}>
                                        <IconMenuLearning />
                                    </TouchableOpacity>
                                    <Text style={styles.menu_content_font}>E-Learning</Text>
                                </View>
                                <View style={styles.menu_content}>
                                    <TouchableOpacity onPress={() => navigation.navigate('Pulsa')} style={styles.menu_content_icon}>
                                        <IconMenuIklan />
                                    </TouchableOpacity>
                                    <Text style={styles.menu_content_font}>Iklan</Text>
                                </View>
                                <View style={styles.menu_content}>
                                    <TouchableOpacity onPress={() => navigation.navigate('Pulsa')} style={styles.menu_content_icon}>
                                        <IconMenuTutorial />
                                    </TouchableOpacity>
                                    <Text style={styles.menu_content_font}>Tutorial</Text>
                                </View>
                                <View style={styles.menu_content}>
                                    <TouchableOpacity onPress={() => navigation.navigate('Pulsa')} style={styles.menu_content_icon}>
                                        <IconMenuPembelian />
                                    </TouchableOpacity>
                                    <Text style={styles.menu_content_font}>Pembelian</Text>
                                </View>
                                <View style={styles.menu_content}>
                                    <TouchableOpacity onPress={() => navigation.navigate('Pulsa')} style={styles.menu_content_icon}>
                                        <IconMenuPembayaran />
                                    </TouchableOpacity>
                                    <Text style={styles.menu_content_font}>Pembayaran</Text>
                                </View>
                            </View>
                        </View>
                        
                        <ImageBackground source={ILBg2} imageStyle={{ borderRadius: 20}} style={styles.produk_terlaris_card}>
                            <View style={styles.section_header}>
                                <Text style={styles.section_header_font_left_white}>Produk Terlaris</Text>
                                <Text style={styles.section_header_font_right_white}>Lihat Semua</Text>
                            </View>
                            <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{ flexDirection: 'row' }}>
                                    
                                <View style={styles.produk_terlaris_content}>
                                    <View style={styles.produk_card}>
                                        <View style={styles.produk_card_image}>
                                            <Image source={ILHospitalBG} style={styles.produk_image} />
                                        </View>
                                        <View style={styles.produk_card_desc}>
                                            <Text numberOfLines={2} style={styles.produk_font_judul}>Sepatu Cewe 1211</Text>
                                              <Text style={styles.produk_font_harga}>RP 129.000,00</Text>
                                              <View style={{ flexDirection: 'row', alignItems:'center' }} >
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <Text style={{fontSize: 13, fontFamily:fonts.primary[400], color:colors.dark1, maxWidth:150}}> (10)</Text>
                                              </View>
                                            <Text style={styles.produk_font_lokasi}>Jakarta Barat</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.produk_terlaris_content}>
                                    <View style={styles.produk_card}>
                                        <View style={styles.produk_card_image}>
                                            <Image source={ILHospitalBG} style={styles.produk_image} />
                                        </View>
                                        <View style={styles.produk_card_desc}>
                                            <Text numberOfLines={2} style={styles.produk_font_judul}>Sepatu Cewe 1211</Text>
                                              <Text style={styles.produk_font_harga}>RP 129.000,00</Text>
                                              <View style={{ flexDirection: 'row', alignItems:'center' }} >
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <Text style={{fontSize: 13, fontFamily:fonts.primary[400], color:colors.dark1, maxWidth:150}}> (10)</Text>
                                              </View>
                                            <Text style={styles.produk_font_lokasi}>Jakarta Barat</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.produk_terlaris_content}>
                                    <View style={styles.produk_card}>
                                        <View style={styles.produk_card_image}>
                                            <Image source={ILHospitalBG} style={styles.produk_image} />
                                        </View>
                                        <View style={styles.produk_card_desc}>
                                            <Text numberOfLines={2} style={styles.produk_font_judul}>Sepatu Cewe 1211</Text>
                                              <Text style={styles.produk_font_harga}>RP 129.000,00</Text>
                                              <View style={{ flexDirection: 'row', alignItems:'center' }} >
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <Text style={{fontSize: 13, fontFamily:fonts.primary[400], color:colors.dark1, maxWidth:150}}> (10)</Text>
                                              </View>
                                            <Text style={styles.produk_font_lokasi}>Jakarta Barat</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.produk_terlaris_content}>
                                    <View style={styles.produk_card}>
                                        <View style={styles.produk_card_image}>
                                            <Image source={ILHospitalBG} style={styles.produk_image} />
                                        </View>
                                        <View style={styles.produk_card_desc}>
                                            <Text numberOfLines={2} style={styles.produk_font_judul}>Sepatu Cewe 1211</Text>
                                              <Text style={styles.produk_font_harga}>RP 129.000,00</Text>
                                              <View style={{ flexDirection: 'row', alignItems:'center' }} >
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <Text style={{fontSize: 13, fontFamily:fonts.primary[400], color:colors.dark1, maxWidth:150}}> (10)</Text>
                                              </View>
                                            <Text style={styles.produk_font_lokasi}>Jakarta Barat</Text>
                                        </View>
                                    </View>
                                </View>
                            </ScrollView>
                            <Gap height={10} />   
                        </ImageBackground>
                        
                        <Gap height={20} />   
                        
                        <View style={styles.section_header}>
                            <Text style={styles.section_header_font_left_black}>Produk Terbaru</Text>
                            <Text style={styles.section_header_font_right_black}>Lihat Semua</Text>
                        </View>
                        <View style={styles.produk_terbaru_card}>
                            <View style={styles.produk_terbaru_body}>
                                  {
                                      produk.map(item => {
                                          return (
                                            <View key={item.id_produk} style={styles.produk_terbaru_content}>  
                                                <View style={styles.produk_card}>
                                                    <View style={styles.produk_card_image}>
                                                        <Image source={{uri: `${URLService}/upload/produk/${item.gambar}`}} style={styles.produk_image} />
                                                    </View>
                                                    <View style={styles.produk_card_desc}>
                                                          <Text numberOfLines={2} style={styles.produk_font_judul}>{item.nama}</Text>
                                                          <Text style={styles.produk_font_harga}>RP {item.harga}</Text>
                                                        <View style={{ flexDirection: 'row', alignItems:'center' }} >
                                                            <IconStar />
                                                            <IconStar />
                                                            <IconStar />
                                                            <IconStar />
                                                            <IconStar />
                                                              <Text style={{ fontSize: 13, fontFamily: fonts.primary[400], color: colors.dark1, maxWidth: 150 }}> ({item.terjual})</Text>
                                                        </View>
                                                          <Text style={styles.produk_font_lokasi}>{item.dikirim}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                          )
                                      })
                                      
                                  }
                                
                                <View style={styles.produk_terbaru_content}>  
                                    <View style={styles.produk_card}>
                                        <View style={styles.produk_card_image}>
                                            <Image source={ILHospitalBG} style={styles.produk_image} />
                                        </View>
                                        <View style={styles.produk_card_desc}>
                                            <Text numberOfLines={2} style={styles.produk_font_judul}>Sepatu Cewe 1211Sepatu Cewe 1211Sepatu Cewe 1211</Text>
                                              <Text style={styles.produk_font_harga}>RP 129.000,00</Text>
                                              <View style={{ flexDirection: 'row', alignItems:'center' }} >
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <Text style={{fontSize: 13, fontFamily:fonts.primary[400], color:colors.dark1, maxWidth:150}}> (10)</Text>
                                              </View>
                                            <Text style={styles.produk_font_lokasi}>Jakarta Barat</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.produk_terbaru_content}>  
                                    <View style={styles.produk_card}>
                                        <View style={styles.produk_card_image}>
                                            <Image source={ILHospitalBG} style={styles.produk_image} />
                                        </View>
                                        <View style={styles.produk_card_desc}>
                                            <Text numberOfLines={2} style={styles.produk_font_judul}>Sepatu Cewe 1211</Text>
                                              <Text style={styles.produk_font_harga}>RP 129.000,00</Text>
                                              <View style={{ flexDirection: 'row', alignItems:'center' }} >
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <Text style={{fontSize: 13, fontFamily:fonts.primary[400], color:colors.dark1, maxWidth:150}}> (10)</Text>
                                              </View>
                                            <Text style={styles.produk_font_lokasi}>Jakarta Barat</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.produk_terbaru_content}>  
                                    <View style={styles.produk_card}>
                                        <View style={styles.produk_card_image}>
                                            <Image source={ILHospitalBG} style={styles.produk_image} />
                                        </View>
                                        <View style={styles.produk_card_desc}>
                                            <Text numberOfLines={2} style={styles.produk_font_judul}>Sepatu Cewe 1211</Text>
                                              <Text style={styles.produk_font_harga}>RP 129.000,00</Text>
                                              <View style={{ flexDirection: 'row', alignItems:'center' }} >
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <IconStar />
                                                <Text style={{fontSize: 13, fontFamily:fonts.primary[400], color:colors.dark1, maxWidth:150}}> (10)</Text>
                                              </View>
                                            <Text style={styles.produk_font_lokasi}>Jakarta Barat</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                
                    <Gap height={20} />
                    </View>
                </ScrollView>
              </ImageBackground>
              
        </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
    androidSafeArea: {
        flex: 1,
        backgroundColor: colors.yellow1,
        // paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
    },
    page: {
        flex: 1,
    },
    section_header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10,
        paddingHorizontal: 16,
        alignItems: 'center'
    },
    section_header_font_left_white: {
        color:colors.white,
        fontFamily:fonts.primary[800],
        fontSize: 17
    },
    section_header_font_right_white: {
        color:colors.white,
        fontFamily:fonts.primary[800],
        fontSize: 15
    },
    section_header_font_left_black: {
        color:colors.black,
        fontFamily:fonts.primary[800],
        fontSize: 17
    },
    section_header_font_right_black: {
        color:colors.black,
        fontFamily:fonts.primary[600],
        fontSize: 15
    },

    header_card: {
        backgroundColor:'#ff000000',
        flex: 1,
        // paddingTop: 5,
        // paddingBottom: 30
    },
    header_body: {
        flexDirection: 'row',
        marginTop: 10,
        paddingHorizontal:10,
        paddingBottom: 10,
        position:'absolute'
        // opacity: 0.1
    },  
    header_content_search: {
        position: 'relative',
        flex: 1,
        justifyContent:'center'
    },
    header_content_search_font: {
        borderWidth: 1,
        borderColor: '#E8E8E8',
        borderRadius: 5,
        height: 40,
        fontSize: 13,
        paddingLeft: 45,
        paddingRight: 20,
        backgroundColor: 'white',
        marginRight: 18,
        justifyContent: 'center'
    },
    header_content_icon: {
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems:'center', 
    },
    content: {
        flex:1,
        backgroundColor : colors.white,
        paddingTop: 15,
    },
    icon_card: {
        marginHorizontal: 10,
        shadowColor: colors.black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 5,
        elevation: 4,
        borderRadius: 10,
        // marginTop: -40,
        backgroundColor: colors.white
    },
    icon_body: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap'
    },
    icon_content_left: {
        flexDirection:'row',
        width: '50%',
        alignItems: 'center',
        justifyContent:'center',
        marginVertical: 10
    },
    icon_content_right: {
        flexDirection:'row',
        width: '50%',
        alignItems: 'center',
        justifyContent:'center',
        marginVertical: 10,
        borderLeftWidth: 1,
        borderLeftColor: colors.dark1
    },
    icon_content_font: {
        color: colors.dark1,
        fontFamily: fonts.primary[600],
        fontSize: 17,
        paddingLeft:6
    },
    
    menu_card: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginHorizontal: 0 
    },
    menu_body: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap'
    },
    menu_content: {
        width: '20%',
        alignItems: 'center',
        marginBottom: 18
    },
    menu_content_icon: {
        width: 58,
        height: 58,
        borderWidth: 1,
        backgroundColor: '#fff',
        borderColor: '#fff',
        borderRadius: 18,
        justifyContent: 'center',
        alignItems: 'center'
    },
    menu_content_font: {
        fontSize:11,
        fontFamily: fonts.primary[600],
        textAlign:'center',
        marginTop: 6
    },

    produk_terlaris_card: {
        paddingVertical: 20,
        borderRadius: 20
    },

    produk_terlaris_content: {
        marginRight: 5,
        padding: 5,
        borderRadius:10
    },

    produk_terbaru_card: {
    },
    produk_terbaru_body: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
        position: 'relative',
        paddingHorizontal:5
    },
    produk_terbaru_content: {
        width: '50%',
        alignItems: 'center',
        marginBottom: 10,
        // backgroundColor:'red'
    },

    produk_card: {
        width: wp('45%'),
        borderRadius:10,
        elevation:2,
        backgroundColor:colors.white,
        borderWidth:0.5,
        borderColor:colors.dark1,
        // height: hp('40%'),
    },
    produk_card_image: {
        width: wp('45%'),
        height: wp('45%'),
        // width: 180,
        // height: 180,
        borderRadius: 10,
        backgroundColor: 'white'
    },
    produk_image: {
        width: undefined,
        height: undefined,
        resizeMode: 'cover',
        flex: 1,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },
    produk_card_desc: {
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    produk_font_judul: {
        fontSize: 14,
        fontFamily:fonts.primary[400],
        color:colors.black,
        maxWidth:150
    },
    produk_font_harga: {
        fontSize: 16,
        fontFamily:fonts.primary[700],
        color:'#FA591D',
        maxWidth:150
    },
    produk_font_lokasi: {
        fontSize: 13,
        fontFamily:fonts.primary[400],
        color:colors.dark1,
        maxWidth:150
    },    
    
    item: {
        width: wp('100%'),
        height: hp('30%'),
    },
    imageContainer: {
        flex: 1,
        marginBottom: Platform.select({ios: 0,
            android: 1}), // Prevent a random Android rendering issue
        backgroundColor: 'transparent',
        borderRadius: 8,
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'contain',
    },
    
  
    
});

export default Home;

