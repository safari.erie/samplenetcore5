import React, { useState, useEffect, useRef } from 'react';
import {
    StatusBar,
    Text, 
    View, 
    StyleSheet,
    TouchableOpacity,
    Image,
    ScrollView,
    ImageBackground,
    RefreshControl,
    Dimensions
} from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {
    IconNotifBlack,
    IconNotifWhite,
    IconCoRight,
    ILNullPhoto,
    IconRightPrimary,
    IMGMenuBelajar,
    IMGMenuQiroaty,
    IMGMenuRapor,
    IMGMenuMore,
    IMGMenuMutabaah,
    ILBg3,
    ILLogo,
    IMGMenuPenilaian
} from '../../assets';
import { AlertHeader, Gap, Avatar } from '../../components';
import { fonts } from '../../utils';
import { colors } from '../../utils';
import { Modalize } from 'react-native-modalize';
import { useDispatch, useSelector } from 'react-redux';
import { FUNCCheckPermission, FUNCDateToString, FUNCDownloadFile } from '../../config/function';
import { apiCekVersi, apiGetBanner, apiGetInfoUserHome, setDataElearningByDays, setDataInboxNoRead, setDataMutabaahs } from '../../config/redux/action';
import UserAvatar from 'react-native-user-avatar';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import Carousel, { Pagination, ParallaxImage } from 'react-native-snap-carousel';
import { BASEURL } from '../../config/helpers';


const HEADER_MAX_HEIGHT = 400;
const HEADER_MIN_HEIGHT = hp('10%');
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const { width: screenWidth } = Dimensions.get('window');

const wait = (timeout) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}
const Home = ({ navigation }) => {
    const modalMenuRef = useRef(null);
    const [refreshing, setRefreshing] = useState(false);
    const [selamat, setSelamat] = useState('');
    const [isDarkMode, setIsDarkMode] = useState(false);
    const { dataUser, fotoProfil, banner } = useSelector(state => state.authReducer);
    const { dataElearningByDays } = useSelector(state => state.elearningReducer);
    const { dataMutabaahs } = useSelector(state => state.mutabaahReducer);
    const { dataInboxNoRead } = useSelector(state => state.inboxReducer);
    const dispatch = useDispatch();
    
    const carouselRef = useRef(null);
    const [entries, setEntries] = useState([]);
    const [activeSlide, setActiveSlide] = useState(0);

    useEffect(() => {
        if (Platform.OS !== 'ios') {
            FUNCCheckPermission()
        }
        dispatch(setDataElearningByDays());
        dispatch(apiGetBanner());
        dispatch(setDataInboxNoRead());
        dispatch(setDataMutabaahs(FUNCDateToString(new Date())))
        selamatHari();
        dispatch(apiGetInfoUserHome());
        dispatch(apiCekVersi());
    }, [dispatch])

    const renderItem = ({ item }, parallaxProps) => {
        return (
            <TouchableOpacity onPress={() => navigation.navigate('DetilGambar', {link: `${BASEURL}/Asset/Files/Appl/File/${item.FileImage}`})} style={styles.parax_item}>
                <View style={styles.parax_image}>
                    <Image
                        source={{uri: `${BASEURL}/Asset/Files/Appl/File/${item.FileImage}`}}
                        containerStyle={styles.parallax_image_body}
                        style={{width:undefined, height:undefined, flex:1, resizeMode:'contain', borderRadius:5}}
                        parallaxFactor={0.4}
                        {...parallaxProps}
                    />
                </View>
            </TouchableOpacity>
        );
    };

    const onRefresh = React.useCallback(() => {
        dispatch(setDataMutabaahs(FUNCDateToString(new Date())))
        dispatch(setDataElearningByDays());
        dispatch(apiGetInfoUserHome());
        selamatHari();
        dispatch(apiGetBanner());
        dispatch(setDataInboxNoRead());
        setRefreshing(true);
        dispatch(apiCekVersi());
        wait(2000).then(() => setRefreshing(false));
    }, []);

    const selamatHari = () => {
        var b = new Date();
        var c = b.getHours();
        if (c > 0 && c < 10) {
            setSelamat("Selamat pagi");
            setIsDarkMode(false);
        }
        else if (c >= 0 && c < 10) {
            setSelamat("Selamat pagi");
            setIsDarkMode(false);
        }
        else if (c >= 10 && c < 16) {
            setSelamat("Selamat siang");
            setIsDarkMode(false);
        }
        else if (c >= 16 && c < 18) {
            setSelamat("Udah sore siap² istirahat");
            setIsDarkMode(false);
        }
        else if (c >= 18 && c < 19) {
            setSelamat("Sebentar lagi magrib");
            setIsDarkMode(false);
        }
        else if (c > 19 && c < 24) {
            setSelamat("Selamat malam");
            setIsDarkMode(false);
        }
    }




    return (
        <>
            <StatusBar translucent barStyle={isDarkMode ? "light-content" : "dark-content"} backgroundColor="transparent"  />
            <View style={styles.android_safearea(isDarkMode)}>
                <Gap height={getStatusBarHeight()} />
                <View style={{ paddingHorizontal: 15, paddingTop: 15 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <View style={{width: 30, height: 30}}>
                                <Image source={ILLogo} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                            </View> 
                            <Text style={{ fontSize: 18, color: isDarkMode ? colors.white : colors.black, fontFamily: fonts.primary[700], left:5 }}>ATTAUFIQ</Text>
                        </View>
                        <TouchableOpacity onPress={() => navigation.navigate('Inbox')}>
                            {
                                dataInboxNoRead && <View style={{ width: 10, height: 10, backgroundColor: 'red', borderRadius: 100, zIndex:999, marginBottom:-11 }} />
                            }
                            
                            <View>
                                {
                                    isDarkMode ? <IconNotifWhite /> : <IconNotifBlack />
                                }
                            </View>
                            
                        </TouchableOpacity>
                    </View>
                </View>
                <Gap height={10} />

                {
                    fotoProfil === '' && <AlertHeader desc="[PERINGATAN] Anda belum memiliki foto profil, silahkan perbarui foto profil anda dengan ukuran standar 250x250" />
                }
                
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefresh}
                        />
                    }
                >
                    <View style={{ paddingHorizontal: 10, paddingVertical:10}}>
                        <ImageBackground source={ILBg3} imageStyle={{ borderRadius: 10}}  style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10, backgroundColor:colors.primary, borderRadius:10}}>
                            <View style={{paddingVertical:10, paddingHorizontal:5}}>
                                <Text style={{ fontSize: 20, color: colors.white, fontFamily: fonts.primary[700], maxWidth: wp('55%') }}>Hai, {selamat}!</Text>
                                <Gap height={25} />
                                <Text style={{ fontSize: 16, color: colors.white, fontFamily: fonts.primary[700], maxWidth:wp('55%')}}>{dataUser.Nama}</Text>
                                <Text style={{ fontSize: 15, color: colors.white, fontFamily: fonts.primary[700], maxWidth: '80%' }}>Kelas : {dataUser.KelasParalel}</Text>
                            </View>
                            <View style={{ paddingVertical: 10 }}>
                                <Avatar source={fotoProfil === '' ? ILNullPhoto : {uri : fotoProfil}} size={100} />
                            </View>
                        </ImageBackground>
                    </View>
    
                    <Gap height={10} />
                    

                    <View style={{ paddingHorizontal: 10}}>
                        <View style={styles.card_menu(isDarkMode)}>
                            <View style={{paddingHorizontal:10, paddingTop:10 }}>
                                <Text style={{ fontSize: 16, color: isDarkMode ? colors.white : colors.black, fontFamily: fonts.primary[600] }}>Menu</Text>
                                <Text style={{ fontSize: 12, color: isDarkMode ? colors.white : colors.black, fontFamily: fonts.primary[400], }}>Fitur Aplikasi Siswa</Text>
                            </View>
                            <Gap height={10} />
                            <View>
                                <View style={{flexDirection: 'row', flexWrap: 'wrap', marginHorizontal: 0}}>
                                    <View style={{justifyContent: 'space-between', flexDirection: 'row', width: '100%', flexWrap: 'wrap'}}>
                                        <View style={{width: '25%', alignItems: 'center', marginBottom: 5}}>  
                                            <TouchableOpacity onPress={() => navigation.navigate('Ruang Belajar')} style={{width: 58, height: 58, backgroundColor: isDarkMode ? colors.darkmode1 : colors.white, borderRadius: 18, justifyContent: 'center' , alignItems: 'center'}}>
                                                <View style={{width: 60, height: 60}}>
                                                    <Image source={IMGMenuBelajar} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                                                </View> 
                                            </TouchableOpacity>
                                            <Text style={{fontSize:12, color:isDarkMode ? colors.white : colors.black, fontFamily: fonts.primary[600], textAlign:'center', marginTop: 0}}>Belajar</Text>
                                        </View>
                                        <View style={{width: '25%', alignItems: 'center', marginBottom: 5}}>  
                                            <TouchableOpacity onPress={() => navigation.navigate('Qiroaty')} style={{width: 58, height: 58, backgroundColor: isDarkMode ? colors.darkmode1 : colors.white, borderRadius: 18, justifyContent: 'center' , alignItems: 'center'}}>
                                                <View style={{width: 60, height: 60}}>
                                                    <Image source={IMGMenuQiroaty} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                                                </View> 
                                            </TouchableOpacity>
                                            <Text style={{fontSize:12, color:isDarkMode ? colors.white : colors.black, fontFamily: fonts.primary[600], textAlign:'center', marginTop: 0}}>Qiroaty</Text>
                                        </View>
                                        <View style={{ width: '25%', alignItems: 'center', marginBottom: 5 }}>
                                             <TouchableOpacity onPress={() => navigation.navigate('Mutabaah')} style={{width: 58, height: 58, backgroundColor: isDarkMode ? colors.darkmode1 : colors.white, borderRadius: 18, justifyContent: 'center' , alignItems: 'center'}}>
                                                <View style={{width: 60, height: 60}}>
                                                    <Image source={IMGMenuMutabaah} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                                                </View> 
                                            </TouchableOpacity>
                                            <Text style={{fontSize:12, color:isDarkMode ? colors.white : colors.black, fontFamily: fonts.primary[600], textAlign:'center', marginTop: 0}}>Mutabaah</Text>
                                        </View>
                                        <View style={{width: '25%', alignItems: 'center', marginBottom: 5}}>  
                                            <TouchableOpacity onPress={() => modalMenuRef.current?.open()} style={{width: 58, height: 58, backgroundColor: isDarkMode ? colors.darkmode1 : colors.white, borderRadius: 18, justifyContent: 'center' , alignItems: 'center'}}>
                                                <View style={{width: 60, height: 60}}>
                                                    <Image source={IMGMenuMore} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                                                </View> 
                                            </TouchableOpacity>
                                            <Text style={{fontSize:12, color:isDarkMode ? colors.white : colors.black, fontFamily: fonts.primary[600], textAlign:'center', marginTop: 0}}>Lainnya</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <Gap height={10} />

                        </View>
                    </View>

                    {
                        dataMutabaahs ?
                            dataMutabaahs.byDay.length !== 0 ?
                                <>
                                    <Gap height={10} />
                                    <View style={{ backgroundColor: 'transparent'}}>
                                        <View style={{ paddingHorizontal: 15, flexDirection:'row', justifyContent:'space-between', alignItems:'center' }}>
                                            <Text style={{ fontSize: 16, color: isDarkMode ? colors.white : colors.black, fontFamily: fonts.primary[600] }}>Mutabaah</Text>
                                            <TouchableOpacity onPress={() => navigation.navigate('Mutabaah')} style={{ flexDirection:'row', justifyContent:'space-between', alignItems:'center' }}>
                                                <Text style={{ fontSize: 13, color: colors.primary, fontFamily: fonts.primary[600], marginRight:10 }}>Lihat Lainnya</Text>
                                                <IconRightPrimary />
                                            </TouchableOpacity>
                                        </View>
                                        <Gap height={5} />
                                        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                                            <View style={{flexDirection:'row', alignItems:'center', paddingHorizontal:10}}>

                                                {
                                                    dataMutabaahs.byDay.map((v, i) => {
                                                        return (
                                                            v.isDone !== 'Y' ?
                                                                <TouchableOpacity onPress={() => navigation.navigate('Mutabaah')} key={i} style={{backgroundColor:'#E4A730', borderRadius:20, margin:5}}>
                                                                    <Text style={{ fontSize: 13, color: colors.white, fontFamily: fonts.primary[700], paddingVertical: 5, paddingHorizontal: 10 }}>{v.jenisEvaluasi} ? </Text>
                                                                </TouchableOpacity>
                                                                : null
                                                        )
                                                    })
                                                }
                                            </View>
                                        
                                        </ScrollView>
                                    </View>
                                </>
                                
                                : null
                            
                            :
                            <>
                                <Gap height={10} />
                                <SkeletonPlaceholder backgroundColor={isDarkMode ? colors.darkmode2 : '#E1E9EE'}> 
                                    <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" marginLeft={10}>
                                        <SkeletonPlaceholder.Item width={120} height={25} borderRadius={10} />
                                        <SkeletonPlaceholder.Item width={120} height={25} borderRadius={10} marginLeft={10} />
                                        <SkeletonPlaceholder.Item width={120} height={25} borderRadius={10} marginLeft={10} />
                                        <SkeletonPlaceholder.Item width={120} height={25} borderRadius={10} marginLeft={10} />
                                    </SkeletonPlaceholder.Item>
                                </SkeletonPlaceholder>
                            </>
                    }

                    
                    
                    {
                        banner !== '' ?
                            banner !== false ?
                                <View style={{ paddingVertical: 15 }}>
                                    <Carousel
                                        ref={carouselRef}
                                        sliderWidth={screenWidth}
                                        sliderHeight={screenWidth}
                                        itemWidth={screenWidth - 60}
                                        data={banner}
                                        renderItem={renderItem}
                                        hasParallaxImages={true}
                                        onSnapToItem={index => setActiveSlide(index)}
                                        loop={true}
                                        autoplay={true}
                                    />
                                    <Pagination
                                        dotsLength={banner.length}
                                        activeDotIndex={activeSlide}
                                        containerStyle={{  paddingVertical:10, justifyContent:'flex-start' }}
                                        dotStyle={{
                                            width: 10,
                                            // height: 10,
                                            borderRadius: 5,
                                            backgroundColor: colors.primary
                                        }}
                                        inactiveDotStyle={{
                                            // Define styles for inactive dots here
                                        }}
                                        inactiveDotOpacity={0.4}
                                        inactiveDotScale={0.6}
                                    />
                                </View>
                                : null
                            :
                            <>
                                <Gap height={15} />
                                <SkeletonPlaceholder backgroundColor={isDarkMode ? colors.darkmode2 : '#E1E9EE'}>
                                    <SkeletonPlaceholder.Item paddingHorizontal={10}>
                                        <SkeletonPlaceholder.Item width={'100%'} height={hp('22%')} borderRadius={10} />
                                    </SkeletonPlaceholder.Item>
                                </SkeletonPlaceholder>
                                <Gap height={15} />
                            </>
                    }



                    {
                        dataElearningByDays ?
                            dataElearningByDays.length !== 0 ?
                                <View style={{ backgroundColor: 'transparent' }}>
                                    <View style={{ paddingHorizontal: 15, flexDirection:'row', justifyContent:'space-between', alignItems:'center' }}>
                                        <Text style={{fontSize: 16, color: isDarkMode ? colors.white : colors.black, fontFamily: fonts.primary[600]}}>Materi Hari Ini</Text>
                                        <TouchableOpacity onPress={() => navigation.navigate('Ruang Belajar')} style={{ flexDirection:'row', justifyContent:'space-between', alignItems:'center' }}>
                                            <Text style={{ fontSize: 13, color: colors.primary, fontFamily: fonts.primary[600], marginRight:10 }}>Lihat Lainnya</Text>
                                            <IconRightPrimary />
                                        </TouchableOpacity>
                                    </View>
                                    <Gap height={5} />
                                    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                                        <View style={{ flexDirection: 'row', paddingHorizontal: 10, alignItems:'center' }}>
                                            {
                                                dataElearningByDays.map((v, i) => {
                                                    return (
                                                        <View key={i} style={{ margin: 5, paddingRight: 5 }}>
                                                            <View style={styles.daftar_pesanan_card}>
                                                                <View style={styles.daftar_pesanan_header}>
                                                                    <Text style={styles.daftar_pesanan_header_font} numberOfLines={1}>
                                                                        Mapel : {v.NamaMapel}
                                                                    </Text>
                                                                    <Text style={styles.daftar_pesanan_header_tgl_font}>
                                                                        {v.Hari}
                                                                    </Text>
                                                                </View>
                                                
                                                                <View style={styles.daftar_pesanan_body}>
                                                                    <View style={styles.daftar_mapel_img_card}>
                                                                        <UserAvatar size={100} name={v.NamaMapel} borderRadius={0} bgColor="#f27c56" style={styles.daftar_mapel_image}/>
                                                                    </View>
                                                                    <View style={styles.daftar_pesanan_desc_card}>
                                                                        <Gap height={3} />
                                                                        <Text style={styles.daftar_pesanan_desc_produk_font} numberOfLines={1}>Materi : {v.JudulMateri || 'Tidak ada materi'}</Text>
                                                                        <Gap height={3} />
                                                                        <Text style={styles.daftar_pesanan_desc_rincian_font} numberOfLines={1}>Guru : {v.Guru}</Text>
                                                                        <Gap height={3} />
                                                                        <Text style={styles.daftar_pesanan_desc_rincian_font} numberOfLines={1}>Waktu : {v.JamMulai} s/d {v.JamSelesai}</Text>
                                                                        <Gap height={3} />
                                                                        <View style={styles.detil_keranjang_produk_card_action_button}>
                                                                            <TouchableOpacity onPress={() => navigation.navigate('RuangBelajar', { idKbmMateri: v.IdKbmMateri })} style={{ backgroundColor: colors.white, borderColor: colors.yellow1, borderWidth: 0.9, paddingVertical: 5, paddingHorizontal: 10, borderRadius: 10, alignItems: 'center', flexDirection: 'row' }}>
                                                                                <Text style={{ color: colors.yellow1, fontFamily: fonts.primary[600], fontSize: 13 }}>Masuk Ruang Belajar</Text>
                                                                                <IconCoRight style={{ left: 5 }} />
                                                                            </TouchableOpacity>
                                                                        </View>
                                                                    </View>
                                                                </View>
                                                            </View>
                                                        </View>  
                                                    )
                                                })
                                            }
                                        </View>
                                    </ScrollView>
                                </View>
                                : null
                            :
                            <>
                                <SkeletonPlaceholder backgroundColor={isDarkMode ? colors.darkmode2 : '#E1E9EE'}> 
                                    <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" marginLeft={10}>
                                        <SkeletonPlaceholder.Item width={250} height={120} borderRadius={10} />
                                        <SkeletonPlaceholder.Item width={250} height={120} borderRadius={10} marginLeft={10} />
                                        <SkeletonPlaceholder.Item width={250} height={120} borderRadius={10} marginLeft={10} />
                                    </SkeletonPlaceholder.Item>
                                </SkeletonPlaceholder>
                            </>
                    }
                <Gap height={20} />
                </ScrollView>
                    
            </View>

             <Modalize
                ref={modalMenuRef}
                snapPoint={350}
                modalHeight={hp('80%')}
                handlePosition="inside"
                modalStyle={{backgroundColor:isDarkMode ? colors.darkmode1 : colors.white}}
            >
                <ScrollView>
                    <Gap height={10} />
                    <View style={{padding:10}}>
                        <Text style={{ fontSize: 18, color: isDarkMode ? colors.white : colors.darkmode1, fontFamily: fonts.primary[600]}}>Fitur</Text>
                        <Gap height={10} />
                        <View style={{flexDirection: 'row', flexWrap: 'wrap', marginHorizontal: 0}}>
                            {/* <View style={{justifyContent: 'space-between', flexDirection: 'row', width: '100%', flexWrap: 'wrap'}}> */}
                            <View style={{justifyContent: 'flex-start', flexDirection: 'row', width: '100%', flexWrap: 'wrap'}}>
                                <View style={{width: '25%', alignItems: 'center', marginBottom: 5}}>  
                                    <TouchableOpacity onPress={() => navigation.navigate('Rapor')} style={{width: 58, height: 58, backgroundColor: isDarkMode ? colors.darkmode1 : colors.white, borderRadius: 18, justifyContent: 'center' , alignItems: 'center'}}>
                                        <View style={{width: 60, height: 60}}>
                                            <Image source={IMGMenuRapor} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                                        </View> 
                                    </TouchableOpacity>
                                    <Text style={{fontSize:12, color:isDarkMode ? colors.white : colors.black, fontFamily: fonts.primary[600], textAlign:'center', marginTop: 0}}>Cari Rapor</Text>
                                </View>
                                <View style={{width: '25%', alignItems: 'center', marginBottom: 5}}>  
                                    <TouchableOpacity onPress={() => navigation.navigate('PenilaianSiswa')} style={{width: 58, height: 58, backgroundColor: isDarkMode ? colors.darkmode1 : colors.white, borderRadius: 18, justifyContent: 'center' , alignItems: 'center'}}>
                                        <View style={{width: 60, height: 60}}>
                                            <Image source={IMGMenuPenilaian} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                                        </View> 
                                    </TouchableOpacity>
                                    <Text style={{fontSize:12, color:isDarkMode ? colors.white : colors.black, fontFamily: fonts.primary[600], textAlign:'center', marginTop: 0}}>Penilaian Siswa</Text>
                                </View>
                            </View>
                        </View>
                    </View>

                </ScrollView>
            </Modalize>
        </>
    )
}
const styles = StyleSheet.create({
    android_safearea: (isDarkMode) => ({
        flex: 1,
        backgroundColor: isDarkMode ? colors.darkmode2 : '#F6F7FC',
    }),
    card_menu: (isDarkMode) => ({
        backgroundColor: isDarkMode ? colors.darkmode2 : colors.white,
        borderRadius:10,
        elevation:2,
        marginBottom: 10,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2,
    }),
    daftar_pesanan_card: {
        backgroundColor: colors.white,
        borderRadius: 10,
        elevation: 2,
        paddingBottom: 10,
        marginBottom: 10,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2,
    },
    daftar_pesanan_header: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 5,
        justifyContent: 'space-between'
    },
    daftar_pesanan_header_font: {
        fontSize: 12,
        fontFamily: fonts.primary[600],
        maxWidth:wp('50%')
    },
    daftar_pesanan_header_tgl_font: {
        fontSize: 11,
        fontFamily: fonts.primary[400],
    },
    daftar_pesanan_header_status_pending: {
        fontSize: 11,
        fontFamily: fonts.primary[400],
        backgroundColor: colors.yellow2,
        paddingVertical: 4,
        paddingHorizontal: 8,
        borderRadius: 5,
        alignItems: 'center'
    },
    daftar_pesanan_header_status_dibayar: {
        fontSize: 11,
        fontFamily: fonts.primary[400],
        backgroundColor: 'green',
        paddingVertical: 4,
        paddingHorizontal: 8,
        borderRadius: 5,
        alignItems: 'center'
    },
    daftar_pesanan_header_status_font: {
        fontSize: 10,
        fontFamily: fonts.primary[800],
        color: colors.white
    },
    daftar_pesanan_body: {
        backgroundColor: colors.white,
        padding: 10,
        flexDirection: 'row',
        alignItems:'center',
        justifyContent: 'space-between',
        borderTopWidth: 0.3,
        borderTopColor: colors.dark2
    },
    daftar_mapel_img_card: {
        width: wp('20%'),
        height: hp('10%'),
        backgroundColor: '#f27c56'
    },
    daftar_mapel_image: {
        width: undefined,
        height: undefined,
        flex: 1,
        resizeMode: 'contain'
    },
    daftar_pesanan_desc_harga_produk_font: {
        fontSize: 12,
        fontFamily: fonts.primary[400],
    },
    daftar_pesanan_desc_rincian_font: {
        maxWidth: wp('60%'),
        fontSize: 11,
        fontFamily: fonts.primary[400]
    },
    daftar_pesanan_desc_card: {
        marginHorizontal: 10,
        minWidth: wp('50%'),
    },
    detil_keranjang_produk_card_action_button: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    daftar_pesanan_desc_produk_font: {
        maxWidth: wp('50%'),
        fontSize: 14,
        fontFamily: fonts.primary[600]
    },
    parallax_image_card: {
        width: '100%',
        height: 180,
        // marginHorizontal:80
    },
    parallax_image_body: {
        marginBottom: Platform.select({ios: 0,
            android: 1}), // Prevent a random Android rendering issue
        backgroundColor: 'blue',
    },

    parax_item: {
        width: '100%',
        height: 180,
    },
    imageContainer: {
        flex: 1,
        marginBottom: Platform.select({ios: 0, android: 1}), // Prevent a random Android rendering issue
        backgroundColor: 'white',
        borderRadius: 8,
    },
    parax_image: {
        ...StyleSheet.absoluteFillObject,
    },
});
export default Home;