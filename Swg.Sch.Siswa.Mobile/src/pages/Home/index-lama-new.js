import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import {
    Dimensions,
    StyleSheet,
    ScrollView,
    View,
    Text,
    PixelRatio,
    Platform,
    TouchableOpacity,
    Image,
    ImageBackground,
    Animated,
    useWindowDimensions,
    StatusBar,
    LogBox
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { colors } from '../../utils/colors';
import { fonts } from '../../utils';
import { Gap } from '../../components';
import { IconNotifikasi,
    ILHospitalBG,
    IconStar,
    IconPromo,
    IconSearch,
    ILBg1,
    ILBg2,
    IconMenuKeranjang,
    IconMenuPembayaran,
    IconMenuProduk,
    IconMenuPoin,
    IconMenuLearning,
    IconMenuTutorial,
    IconMenuIklan,
    IconMenuPembelian,
    IconPlayVidio,
    IconKeranjang
} from '../../assets';
import OneSignal from 'react-native-onesignal';
import Geolocation from '@react-native-community/geolocation';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Carousel, { ParallaxImage, Pagination } from 'react-native-snap-carousel';
import Axios from 'axios';
import { URLService } from '../../services';
import Tips from 'react-native-root-tips';
import WebView from 'react-native-webview';
import { CommonActions, useIsFocused } from '@react-navigation/native';
import YouTube, { YouTubeStandaloneIOS, YouTubeStandaloneAndroid } from 'react-native-youtube';
GLOBAL = require('../../services/global_state');


const HEADER_MAX_HEIGHT = 400;
const HEADER_MIN_HEIGHT = 399;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;


const ENTRIES1 = [
    {
      title: 'Beautiful and dramatic Antelope Canyon',
      subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
      illustration: 'https://ecs7-p.tokopedia.net/img/cache/800/VxWOnu/2020/12/17/ff0028a6-172c-4923-99bb-25c8cceb00d9.jpg.webp',
    },
    {
      title: 'Earlier this morning, NYC',
      subtitle: 'Lorem ipsum dolor sit amet',
      illustration: 'https://ecs7-p.tokopedia.net/img/cache/800/VxWOnu/2020/12/18/755f7450-f6a2-4bef-a39b-d1e0b6efdb70.jpg.webp',
    },
    {
      title: 'White Pocket Sunset',
      subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
      illustration: 'https://ecs7-p.tokopedia.net/img/cache/800/VxWOnu/2020/12/18/50d85630-dea8-405e-9e35-8b388f294c91.jpg.webp',
    },
    {
      title: 'White Pocket Sunset',
      subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
      illustration: 'https://ecs7-p.tokopedia.net/img/cache/800/VxWOnu/2020/12/18/20ca77f4-eea0-4039-bf06-22354aab0ba2.jpg.webp',
    },
    {
      title: 'White Pocket Sunset',
      subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
      illustration: 'https://ecs7-p.tokopedia.net/img/cache/800/VxWOnu/2020/12/18/ae8f0774-cbca-4389-9fbf-936e2b28c78c.jpg.webp',
    },
    {
      title: 'White Pocket Sunset',
      subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
      illustration: 'https://ecs7-p.tokopedia.net/img/cache/800/VxWOnu/2020/12/18/c3e0375c-cbc7-49f3-9b35-f7a766e487c6.jpg.webp',
    },
];
const {width: screenWidth} = Dimensions.get('window');
  
const Home = ({ navigation }) => {
    const isMountedComponent = useRef(true);   

    const [entries, setEntries] = useState([]);
    const [produk, setProduk] = useState([]);
    const [activeSlide, setActiveSlide] = useState(0);
    const carouselRef = useRef(null);
    const {width,height} = Dimensions.get('window')

    useEffect(() => {
        setEntries(ENTRIES1);
        Geolocation.getCurrentPosition(info => console.log('loc -> ', info));
        OneSignal.init('AppId')
        OneSignal.inFocusDisplaying(2);

        OneSignal.addEventListener('ids', onIds)
        return () => { 
            OneSignal.removeEventListener('ids', onIds)
        }
    }, [])

    const renderItem = ({ item }, parallaxProps) => {
        return (
          <TouchableOpacity onPress={() => alert(item.illustration)} style={styles.carousel_item}>
            <Image
              source={{uri: item.illustration}}
              containerStyle={styles.carousel_imageContainer}
              style={styles.carousel_image}
              parallaxFactor={0.4}
              {...parallaxProps}
            />
            
          </TouchableOpacity>
        );
    };

    const animatedScrollYValue = useRef(new Animated.Value(0)).current;
    const contentWidth = useWindowDimensions().width;

    const headerHeight = animatedScrollYValue.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 1],
        extrapolate: 'clamp',
    });

    const onIds = (device) => {
        console.log('device Id -> ',device)
    }

    useEffect(() => {
        getData()
    }, [])

    const getData = () => {
        Axios.get(`${URLService}/Mobile/ProdukList/`, { headers: { 'token': GLOBAL.TOKEN } }).then((res) => {
            let data = res.data;
            if (data.IsSuccess) {
                setProduk(data.Data);
            } else {
                Tips.showWarn(data.ReturnMessage, {
                    duration: 2000,
                    position: Tips.positions.CENTER,
                    shadow: false,
                    animation: true,
                    hideOnPress: false,
                    mask: true,
                    maskColor: colors.black,
                    delay: 0
                });
            }
        }).catch((err) => {
            let status = err.response.status;
            if (status === 401) {
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            { name: 'Welcome' },
                        ],
                    })
                );
            } else {
                JSON.stringify(err.message);
                Tips.showWarn(err.message, {
                    duration: 2000,
                    position: Tips.positions.CENTER,
                    shadow: false,
                    animation: true,
                    hideOnPress: false,
                    mask: true,
                    maskColor: colors.black,
                    delay: 0
                });
            }
        });
    }
    
    LogBox.ignoreLogs([
            'Sending \`onAnimatedValueUpdate\` with no listeners registered.',
        ]
    )

    return (
        <>
        <StatusBar translucent barStyle="light-content" backgroundColor="transparent" />
        <View style={styles.animated_fill}>
            <Animated.ScrollView
            style={styles.animated_fill}
            contentContainerStyle={styles.animated_scrollViewContent}
            scrollEventThrottle={16}
            onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: animatedScrollYValue } } }], {useNativeDriver: true})}
            >
                <View style={styles.animated_scrollViewContent}>
                
                    <ScrollView style={{ flex: 1, backgroundColor: colors.white }}>
                        <View style={styles.header_card}>
                            <Carousel
                                layout={'default'}
                                ref={carouselRef}
                                sliderWidth={screenWidth}
                                sliderHeight={screenWidth}
                                itemWidth={wp('100%')}
                                data={entries}
                                renderItem={renderItem}
                                onSnapToItem={index => setActiveSlide(index)}
                                hasParallaxImages={true}
                                autoplay={true}
                                />
                                <View style={styles.header_body}>
                                    <View style={styles.header_content_search}>
                                        <TouchableOpacity onPress={() => alert('ok')} style={styles.header_content_search_font}>
                                            <Text style={{fontSize:15,color:colors.dark1, fontFamily:fonts.primary[400]}}> Pencarian</Text>
                                        </TouchableOpacity>
                                        <IconSearch style={{position: 'absolute', left: 12}} />
                                    </View>
                                    <View style={styles.header_content_icon}>
                                        <IconNotifikasi style={{right:10}} />
                                        <IconKeranjang style={{left:0}}/>
                                    </View>
                                </View>
                            <Pagination
                                dotsLength={entries.length}
                                activeDotIndex={activeSlide}
                                containerStyle={{  paddingVertical:5, justifyContent:'flex-start', marginTop:-40}}
                                dotStyle={{
                                    width: 10,
                                    height: 10,
                                    borderRadius: 5,
                                    marginHorizontal: 2,
                                    backgroundColor: 'rgba(255, 255, 255, 0.92)'
                                }}
                                inactiveDotStyle={{
                                    // Define styles for inactive dots here
                                }}
                                inactiveDotOpacity={0.4}
                                inactiveDotScale={0.6}
                            />
                        </View>
                        
                        <ImageBackground imageStyle={{borderRadius:20}} source={ILBg1} style={styles.content}>
                            
                            {/* <View style={styles.icon_card}>
                                    <View style={styles.icon_body}>
                                        <View style={styles.icon_content_left}>  
                                        <IconBronze />
                                        <Text style={styles.icon_content_font}>Poin : 14 </Text>
                                    </View>
                                    <View style={styles.icon_content_right}>  
                                        <IconSearch />
                                        <Text style={styles.icon_content_font}>Kupon : 1 </Text>
                                    </View>
                                </View>
                            </View> */}
                                
                            {/* <Gap height={20} />  */}
                                
                                <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{ flexDirection: 'row' }}>
                                    <View style={styles.menu_card}>
                                        <View style={styles.menu_body}>
                                            <View style={styles.menu_content}>
                                                <TouchableOpacity onPress={() => navigation.navigate('DaftarProduk')} style={styles.menu_content_icon}>
                                                    <IconMenuProduk />
                                                </TouchableOpacity>
                                                <Text numberOfLines={1} style={styles.menu_content_font}>Produk</Text>
                                            </View>
                                            <View style={styles.menu_content}>
                                                <TouchableOpacity onPress={() => navigation.navigate('DaftarPesanan')} style={styles.menu_content_icon}>
                                                    <IconMenuPembelian />
                                                </TouchableOpacity>
                                                <Text numberOfLines={1} style={styles.menu_content_font}>Pembelian</Text>
                                            </View>
                                            <View style={styles.menu_content}>
                                                <TouchableOpacity onPress={() => navigation.navigate('Pulsa')} style={styles.menu_content_icon}>
                                                    <IconMenuPembayaran />
                                                </TouchableOpacity>
                                                <Text numberOfLines={1} style={styles.menu_content_font}>Pembayaran</Text>
                                            </View>
                                            <View style={styles.menu_content}>
                                                <TouchableOpacity onPress={() => navigation.navigate('Pulsa')} style={styles.menu_content_icon}>
                                                    <IconMenuKeranjang />
                                                </TouchableOpacity>
                                                <Text numberOfLines={1} style={styles.menu_content_font}>Keranjang</Text>
                                            </View>
                                            <View style={styles.menu_content}>
                                                <TouchableOpacity onPress={() => navigation.navigate('Pulsa')} style={styles.menu_content_icon}>
                                                    <IconMenuPoin />
                                                </TouchableOpacity>
                                                <Text numberOfLines={1} style={styles.menu_content_font}>Poin</Text>
                                            </View>
                                            <View style={styles.menu_content}>
                                                <TouchableOpacity onPress={() => navigation.navigate('DetilProduk')} style={styles.menu_content_icon}>
                                                    <IconMenuProduk />
                                                </TouchableOpacity>
                                                <Text numberOfLines={1} style={styles.menu_content_font}>Produk</Text>
                                            </View>
                                            <View style={styles.menu_content}>
                                                <TouchableOpacity onPress={() => navigation.navigate('Pulsa')} style={styles.menu_content_icon}>
                                                    <IconMenuPembelian />
                                                </TouchableOpacity>
                                                <Text numberOfLines={1} style={styles.menu_content_font}>Pembelian</Text>
                                            </View>
                                            <View style={styles.menu_content}>
                                                <TouchableOpacity onPress={() => navigation.navigate('Pulsa')} style={styles.menu_content_icon}>
                                                    <IconMenuPembayaran />
                                                </TouchableOpacity>
                                                <Text numberOfLines={1} style={styles.menu_content_font}>Pembayaran</Text>
                                            </View>
                                           
                                        </View>
                                    </View>
                                    
                                </ScrollView>
                        </ImageBackground>
                        <View style={styles.content1}>
                            <View style={styles.section_header}>
                                <Text style={styles.section_header_font_left_black}>Hai, apakabarmu hari ini?</Text>
                            </View>
                                <TouchableOpacity onPress={() => navigation.navigate('DetilVidio', {videoId : 'xuCn8ux2gbs'})} style={styles.vidutama_card}>
                                    <View style={styles.vidutama_body}>
                                        <Image source={{uri: 'https://ecs7-p.tokopedia.net/img/cache/800/VxWOnu/2020/12/18/50d85630-dea8-405e-9e35-8b388f294c91.jpg.webp'}}  style={styles.vidutama_cover} />
                                        <View style={styles.vidutama_playbutton}>
                                            <IconPlayVidio />
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            {/* <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{ flexDirection: 'row' }}>
                                
                                <View style={{
                                    marginHorizontal: 10,
                                    marginBottom: 10,
                                    backgroundColor:colors.white,
                                    borderRadius:20,
                                    shadowColor: colors.black,
                                    shadowOffset: {
                                        width: 0,
                                        height: 2,
                                    },
                                    shadowOpacity: 0.23,
                                    shadowRadius: 5,
                                    width:wp('65%'),
                                    elevation:3
                                }}>
                                    <View style={{alignItems:'center', borderRadius:20}}>
                                        <View style={{overflow:'hidden',borderTopLeftRadius:20, borderTopRightRadius:20}}>
                                            <YouTube
                                                apiKey="AIzaSyAZirAjBTitnWrkpdqxuW2ih8UhwLad87Y"
                                                videoId="xuCn8ux2gbs"
                                                play={false}
                                                loop={false}
                                                fullscreen={false}
                                                controls={1}
                                                style={{ width: wp('65%'), height: hp('15%')}}
                                            />
                                        </View>
                                    </View>
                                    <View style={{paddingVertical:10, paddingHorizontal:20}}>
                                        <Text style={{fontSize:15, fontFamily: fonts.primary[700]}}>Belajar Mandi Sendiri</Text>
                                        <Text style={{fontSize:13, fontFamily: fonts.primary[400], maxWidth:wp('80%')}} numberOfLines={2}>Wwkawkakwkawk land wkakwakwka land wkakwakwka landwkakwakwka landwkakwakwka landwkakwakwka land</Text>
                                    </View>
                                    <View style={{paddingBottom:10, paddingHorizontal:20, alignItems:'flex-end'}}>
                                        <Text style={{fontSize:15, fontFamily: fonts.primary[700], color:colors.yellow1}}>Selengkapnya</Text>
                                    </View>
                                </View>
                                <View style={{
                                    marginHorizontal: 10,
                                    marginBottom: 10,
                                    backgroundColor:colors.white,
                                    borderRadius:20,
                                    shadowColor: colors.black,
                                    shadowOffset: {
                                        width: 0,
                                        height: 2,
                                    },
                                    shadowOpacity: 0.23,
                                    shadowRadius: 5,
                                    width:wp('65%'),
                                    elevation:3
                                }}>
                                    <View style={{alignItems:'center', borderRadius:20}}>
                                        <View style={{overflow:'hidden',borderTopLeftRadius:20, borderTopRightRadius:20}}>
                                            <YouTube
                                                apiKey="AIzaSyAZirAjBTitnWrkpdqxuW2ih8UhwLad87Y"
                                                videoId="xuCn8ux2gbs"
                                                play={false}
                                                loop={false}
                                                fullscreen={false}
                                                controls={1}
                                                style={{ width: wp('65%'), height: hp('15%')}}
                                            />
                                        </View>
                                    </View>
                                    <View style={{paddingVertical:10, paddingHorizontal:20}}>
                                        <Text style={{fontSize:15, fontFamily: fonts.primary[700]}}>Belajar Mandi Sendiri</Text>
                                        <Text style={{fontSize:13, fontFamily: fonts.primary[400], maxWidth:wp('80%')}} numberOfLines={2}>Wwkawkakwkawk land wkakwakwka land wkakwakwka landwkakwakwka landwkakwakwka landwkakwakwka land</Text>
                                    </View>
                                    <View style={{paddingBottom:10, paddingHorizontal:20, alignItems:'flex-end'}}>
                                        <Text style={{fontSize:15, fontFamily: fonts.primary[700], color:colors.yellow1}}>Selengkapnya</Text>
                                    </View>
                                </View>
                                      
                            </ScrollView> */}
                            <Gap height={10} />  
                            
                            <View style={styles.section_header}>
                                <Text style={styles.section_header_font_left_black}>Menu Pembelajaran</Text>
                            </View>
                            <View style={styles.belajar_card}>
                                <View style={styles.belajar_body}>
                                    <View style={styles.belajar_content}>
                                        <IconMenuPoin />
                                        <Gap height={5} />
                                        <Text style={styles.belajar_judul_font}>Poin</Text>
                                        <Text style={styles.belajar_desc_font} numberOfLines={1}>Wkwkwkw land wkwk land wkwk land</Text>
                                    </View>
                                </View>
                                <View style={styles.belajar_body}>
                                    <View style={styles.belajar_content}>
                                        <IconMenuIklan />
                                        <Gap height={5} />
                                        <Text style={styles.belajar_judul_font}>Iklan</Text>
                                        <Text style={styles.belajar_desc_font} numberOfLines={1}>Wkwkwkw land wkwk land wkwk land</Text>
                                    </View>
                                </View>
                                <View style={styles.belajar_body}>
                                    <View style={styles.belajar_content}>
                                        <IconMenuLearning />
                                        <Gap height={5} />
                                        <Text style={styles.belajar_judul_font}>Ecourse</Text>
                                        <Text style={styles.belajar_desc_font} numberOfLines={1}>Wkwkwkw land wkwk land wkwk land</Text>
                                    </View>
                                </View>
                                <View style={styles.belajar_body}>
                                    <View style={styles.belajar_content}>
                                        <IconMenuTutorial />
                                        <Gap height={5} />
                                        <Text style={styles.belajar_judul_font}>Tutorial</Text>
                                        <Text style={styles.belajar_desc_font} numberOfLines={1}>Wkwkwkw land wkwk land wkwk land</Text>
                                    </View>
                                </View>
                            </View>
                            <Gap height={10} />  
                            <View style={styles.section_header}>
                                <Text style={styles.section_header_font_left_black}>Tutorial Singkat</Text>
                                <Text style={styles.section_header_font_right_black}>Lihat Semua</Text>
                            </View>
                            <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{ flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => navigation.navigate('DetilVidio', {videoId : 'xuCn8ux2gbs'})} style={styles.tutorial_card}>
                                    <Image source={{uri: 'https://dropshipedia.id//assets/images/banner/Image.jpg'}}  style={styles.tutorial_cover} />
                                    <View style={styles.tutorial_playbutton}>
                                        <IconPlayVidio />
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => navigation.navigate('DetilVidio', {videoId : 'xuCn8ux2gbs'})} style={styles.tutorial_card}>
                                    <Image source={{uri: 'https://dropshipedia.id//assets/images/banner/Image1.jpg'}}  style={styles.tutorial_cover} />
                                    <View style={styles.tutorial_playbutton}>
                                        <IconPlayVidio />
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => navigation.navigate('DetilVidio', {videoId : 'xuCn8ux2gbs'})} style={styles.tutorial_card}>
                                    <Image source={{uri: 'https://dropshipedia.id//assets/images/banner/Image2.jpg'}}  style={styles.tutorial_cover} />
                                    <View style={styles.tutorial_playbutton}>
                                        <IconPlayVidio />
                                    </View>
                                </TouchableOpacity>
                               
                                
                                    
                            </ScrollView>
                                
                            <Gap height={10} />   
                            
                            <ImageBackground source={ILBg2} imageStyle={{ borderRadius: 20}} style={styles.produk_terlaris_card}>
                                <View style={styles.section_header}>
                                    <Text style={styles.section_header_font_left_white}>Produk Terlaris</Text>
                                    <Text style={styles.section_header_font_right_white}>Lihat Semua</Text>
                                </View>
                                <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{ flexDirection: 'row' }}>
                                        
                                    <View style={styles.produk_terlaris_content}>
                                        <View style={styles.produk_card}>
                                            <View style={styles.produk_card_image}>
                                                <Image source={ILHospitalBG} style={styles.produk_image} />
                                            </View>
                                            <View style={styles.produk_card_desc}>
                                                <Text numberOfLines={1} style={styles.produk_font_judul}>Sepatu Cewe 1211</Text>
                                                    <Text style={styles.produk_font_harga}>RP 129.000,00</Text>
                                                    <View style={{ flexDirection: 'row', alignItems:'center' }} >
                                                    <IconStar />
                                                    <Text style={{fontSize: 13, fontFamily:fonts.primary[400], color:colors.dark1, maxWidth:150}}> (4.8) | Terjual : 109</Text>
                                                    </View>
                                                <Text style={styles.produk_font_lokasi}>Jakarta Barat</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.produk_terlaris_content}>
                                        <View style={styles.produk_card}>
                                            <View style={styles.produk_card_image}>
                                                <Image source={ILHospitalBG} style={styles.produk_image} />
                                            </View>
                                            <View style={styles.produk_card_desc}>
                                               <Text numberOfLines={1} style={styles.produk_font_judul}>Sepatu Cewe 1211</Text>
                                                    <Text style={styles.produk_font_harga}>RP 129.000,00</Text>
                                                    <View style={{ flexDirection: 'row', alignItems:'center' }} >
                                                    <IconStar />
                                                    <Text style={{fontSize: 13, fontFamily:fonts.primary[400], color:colors.dark1, maxWidth:150}}> (4.8) | Terjual : 109</Text>
                                                    </View>
                                                <Text style={styles.produk_font_lokasi}>Jakarta Barat</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.produk_terlaris_content}>
                                        <View style={styles.produk_card}>
                                            <View style={styles.produk_card_image}>
                                                <Image source={ILHospitalBG} style={styles.produk_image} />
                                            </View>
                                            <View style={styles.produk_card_desc}>
                                               <Text numberOfLines={1} style={styles.produk_font_judul}>Sepatu Cewe 1211</Text>
                                                    <Text style={styles.produk_font_harga}>RP 129.000,00</Text>
                                                    <View style={{ flexDirection: 'row', alignItems:'center' }} >
                                                    <IconStar />
                                                    <Text style={{fontSize: 13, fontFamily:fonts.primary[400], color:colors.dark1, maxWidth:150}}> (4.8) | Terjual : 109</Text>
                                                    </View>
                                                <Text style={styles.produk_font_lokasi}>Jakarta Barat</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.produk_terlaris_content}>
                                        <View style={styles.produk_card}>
                                            <View style={styles.produk_card_image}>
                                                <Image source={ILHospitalBG} style={styles.produk_image} />
                                            </View>
                                            <View style={styles.produk_card_desc}>
                                               <Text numberOfLines={1} style={styles.produk_font_judul}>Sepatu Cewe 1211</Text>
                                                    <Text style={styles.produk_font_harga}>RP 129.000,00</Text>
                                                    <View style={{ flexDirection: 'row', alignItems:'center' }} >
                                                    <IconStar />
                                                    <Text style={{fontSize: 13, fontFamily:fonts.primary[400], color:colors.dark1, maxWidth:150}}> (4.8) | Terjual : 109</Text>
                                                    </View>
                                                <Text style={styles.produk_font_lokasi}>Jakarta Barat</Text>
                                            </View>
                                        </View>
                                    </View>
                                </ScrollView>
                                <Gap height={10} />   
                            </ImageBackground>
                            
                            <Gap height={20} />   
                            
                            <View style={styles.section_header}>
                                <Text style={styles.section_header_font_left_black}>Produk Terbaru</Text>
                                <Text style={styles.section_header_font_right_black}>Lihat Semua</Text>
                            </View>
                            <View style={styles.produk_terbaru_card}>
                                <View style={styles.produk_terbaru_body}>
                                        {
                                            produk.map(item => {
                                                return (
                                                <TouchableOpacity onPress={() => alert(item.nama)} key={item.id_produk} style={styles.produk_terbaru_content}>  
                                                    <View style={styles.produk_card}>
                                                        <View style={styles.produk_card_image}>
                                                            <Image source={{uri: `${URLService}/upload/produk/${item.gambar}`}} style={styles.produk_image} />
                                                        </View>
                                                        <View style={{backgroundColor:'#fb8500', width:wp('28%'), alignItems:'center', borderRadius:0, marginHorizontal:0, position:'absolute'}}>
                                                            <Text style={{color:colors.white, fontFamily: fonts.primary[800]}}>BEST SELLER</Text>
                                                        </View>
                                                        <View style={styles.produk_card_desc}>
                                                                <Text numberOfLines={1} style={styles.produk_font_judul}>{item.nama}</Text>
                                                                <Text style={styles.produk_font_harga}>RP {item.harga}</Text>
                                                            <View style={{ flexDirection: 'row', alignItems:'center' }} >
                                                                <IconStar />
                                                                    <Text style={{ fontSize: 13, fontFamily: fonts.primary[400], color: colors.dark1, maxWidth: 150 }}> ({item.terjual}) | Terjual : {item.terjual}</Text>
                                                            </View>
                                                                <Text style={styles.produk_font_lokasi}>{item.dikirim}</Text>
                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                                )
                                            })
                                            
                                        }
                                    
                                    <View style={styles.produk_terbaru_content}>  
                                        <View style={styles.produk_card}>
                                            <View style={styles.produk_card_image}>
                                                <Image source={ILHospitalBG} style={styles.produk_image} />
                                            </View>
                                            <View style={styles.produk_card_desc}>
                                                <Text numberOfLines={1} style={styles.produk_font_judul}>Sepatu Cewe 1211Sepatu Cewe 1211Sepatu Cewe 1211</Text>
                                                    <Text style={styles.produk_font_harga}>RP 129.000,00</Text>
                                                    <View style={{ flexDirection: 'row', alignItems:'center' }} >
                                                    <IconStar />
                                                    <IconStar />
                                                    <IconStar />
                                                    <IconStar />
                                                    <IconStar />
                                                    <Text style={{fontSize: 13, fontFamily:fonts.primary[400], color:colors.dark1, maxWidth:150}}> (10)</Text>
                                                    </View>
                                                <Text style={styles.produk_font_lokasi}>Jakarta Barat</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <TouchableOpacity onPress={() => navigation.navigate('DetilProduk')} style={styles.produk_terbaru_content}>  
                                        <View style={styles.produk_card}>
                                            <View style={styles.produk_card_image}>
                                                <Image source={{uri: 'https://dropshipedia.id/uploads/564685c70040c8fd9e944f7747145924.jpg'}} style={styles.produk_image} />
                                            </View>
                                            <View style={styles.produk_card_desc}>
                                                <Text numberOfLines={1} style={styles.produk_font_judul}>Jam tangan casual (FOSIL)- Termurah sejagad raya</Text>
                                                    <Text style={styles.produk_font_harga}>RP 105.000,00</Text>
                                                    <View style={{ flexDirection: 'row', alignItems:'center' }} >
                                                    <IconStar />
                                                    <IconStar />
                                                    <IconStar />
                                                    <IconStar />
                                                    <IconStar />
                                                    <Text style={{fontSize: 13, fontFamily:fonts.primary[400], color:colors.dark1, maxWidth:150}}> (10)</Text>
                                                    </View>
                                                <Text style={styles.produk_font_lokasi}>Jakarta Barat</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                    <View style={styles.produk_terbaru_content}>  
                                        <View style={styles.produk_card}>
                                            <View style={styles.produk_card_image}>
                                                <Image source={ILHospitalBG} style={styles.produk_image} />
                                            </View>
                                            <View style={styles.produk_card_desc}>
                                               <Text numberOfLines={1} style={styles.produk_font_judul}>Sepatu Cewe 1211</Text>
                                                    <Text style={styles.produk_font_harga}>RP 129.000,00</Text>
                                                    <View style={{ flexDirection: 'row', alignItems:'center' }} >
                                                    <IconStar />
                                                    <Text style={{fontSize: 13, fontFamily:fonts.primary[400], color:colors.dark1, maxWidth:150}}> (4.8) | Terjual : 109</Text>
                                                    </View>
                                                <Text style={styles.produk_font_lokasi}>Jakarta Barat</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </View>
                    
                        <Gap height={20} />
                        </View>
        
                    </ScrollView>
             
                
                </View>
                
            </Animated.ScrollView>
            
            
            <Animated.View opacity={headerHeight} style={[styles.animated_header]}>
                <ImageBackground source={ILBg2} style={{width:wp('100%')}}>
                    <Gap height={getStatusBarHeight()} />
                        <View style={styles.header_body_animated} >
                            <View style={styles.header_content_search}>
                                <TouchableOpacity onPress={() => alert('ok')} style={styles.header_content_search_font}>
                                    <Text style={{fontSize:15,color:colors.dark1, fontFamily:fonts.primary[400]}}> Pencarian</Text>
                                </TouchableOpacity>
                                {/* <TextInput  style={styles.header_content_search_font} placeholder="Mau cari apa?" /> */}
                                <IconSearch style={{position: 'absolute', left: 12}} />
                            </View>
                            <View style={styles.header_content_icon}>
                                <IconNotifikasi style={{ right: 10 }} />
                                <TouchableOpacity onPress={() => navigation.navigate('Keranjang')}>
                                    <IconKeranjang style={{left:0}}/>
                                </TouchableOpacity>
                            </View>
                        </View>
                </ImageBackground>
            </Animated.View>
        </View>
        </>
    );
};

Home.propTypes = {
    navigation: PropTypes.shape({
        navigate: PropTypes.func.isRequired,
    }).isRequired,
};


const styles = StyleSheet.create({
    animated_fill: {
        flex: 1,
    },
    animated_header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: colors.white,
        overflow: 'hidden',
    },
    animated_scrollViewContent: {
        marginTop: 0,
        backgroundColor: colors.yellow1,
    },
    androidSafeArea: {
        flex: 1,
        backgroundColor: colors.yellow1,
    },
    page: {
        flex: 1,
    },
    section_header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10,
        paddingHorizontal: 16,
        alignItems: 'center'
    },
    section_header_font_left_white: {
        color:colors.white,
        fontFamily:fonts.primary[800],
        fontSize: 17
    },
    section_header_font_right_white: {
        color:colors.white,
        fontFamily:fonts.primary[800],
        fontSize: 15
    },
    section_header_font_left_black: {
        color:colors.black,
        fontFamily:fonts.primary[800],
        fontSize: 17
    },
    section_header_font_right_black: {
        color:colors.yellow1,
        fontFamily:fonts.primary[600],
        fontSize: 15
    },

    header_card: {
        backgroundColor:'#ff000000',
        flex: 1,
    },
    header_body: {
        flexDirection: 'row',
        marginTop: getStatusBarHeight(),
        paddingHorizontal:10,
        paddingVertical: 10,
        position:'absolute',
    },  
    header_body_animated: {
        flexDirection: 'row',
        paddingTop:10,
        paddingHorizontal:10,
        paddingBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "transparent",
        borderBottomWidth: 0.3,
        borderColor:colors.dark1,
        marginTop: Platform.OS === 'ios' ? 0 : 0,
    },  
    header_content_search: {
        position: 'relative',
        flex: 1,
        justifyContent: 'center',
    },
    header_content_search_font: {
        borderWidth: 1,
        borderColor: '#E8E8E8',
        borderRadius: 5,
        height: 40,
        fontSize: 13,
        paddingLeft: 45,
        paddingRight: 20,
        backgroundColor:'#f6f8fa',
        marginRight: 18,
        justifyContent: 'center'
    },
    header_content_icon: {
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems:'center', 
    },
    content: {
        flex:1,
        backgroundColor : colors.yellow1,
        paddingTop: 15,
        height:150,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    content1: {
        flex:1,
        backgroundColor : colors.white,
        paddingTop: 15,
        top:-45,
        borderRadius: 25
    },
    vidutama_card: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    vidutama_body: {
        marginBottom: 10,
        borderRadius:20,
        shadowColor: colors.black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 5,
        width:wp('95%'),
        height:hp('25%'),
        elevation:3
    },
    vidutama_cover: {
        width: undefined,
        height:undefined,
        flex:1,
        resizeMode:'stretch',
        borderRadius:20
    },
    vidutama_playbutton: {
        flex:1,
        position:'absolute',
        width:wp('95%'),
        height:hp('25%'),
        backgroundColor:'#00000042',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:17
    },
    belajar_card: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
        position: 'relative',
        paddingHorizontal:5
    },
    belajar_body: {
        width:'50%',
        marginBottom:10,
        paddingHorizontal:5
    },
    belajar_content: {
        backgroundColor:'#edebec',
        borderRadius: 10,
        padding:10
    },  
    belajar_judul_font: {
        fontSize: 13,
        fontFamily: fonts.primary[700]
    },
    belajar_desc_font: {
        fontSize:12,
        fontFamily: fonts.primary[400],
        maxWidth:wp('40%')
    },
    tutorial_card: {
        marginHorizontal: 10,
        marginBottom: 20,
        backgroundColor:colors.white,
        borderRadius:20,
        shadowColor: colors.black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 5,
        width:wp('60%'),
        height:hp('17%'),
        elevation:3
    },
    tutorial_cover: {
        width: undefined,
        height:undefined,
        flex:1,
        resizeMode:'stretch',
        borderRadius:20
    },
    tutorial_cover: {
        width: undefined,
        height:undefined,
        flex:1,
        resizeMode:'stretch',
        borderRadius:20
    },
    tutorial_playbutton: {
        flex: 1,
        position: 'absolute',
        width: wp('60%'),
        height: hp('17%'),
        backgroundColor: '#00000042',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 17
    },
    icon_card: {
        marginHorizontal: 10,
        shadowColor: colors.black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 5,
        elevation: 2,
        borderRadius: 5,
        backgroundColor: colors.white
    },
    icon_body: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap'
    },
    icon_content_left: {
        flexDirection:'row',
        width: '50%',
        alignItems: 'center',
        justifyContent:'center',
        marginVertical: 10
    },
    icon_content_right: {
        flexDirection:'row',
        width: '50%',
        alignItems: 'center',
        justifyContent:'center',
        marginVertical: 10,
        borderLeftWidth: 0.5,
        borderLeftColor: colors.dark1
    },
    icon_content_font: {
        color: colors.black,
        fontFamily: fonts.primary[400],
        fontSize: 14,
        paddingLeft:6
    },
    
    menu_card: {
        flexDirection: 'row',
        // flexWrap: 'wrap',
        marginHorizontal: 0 
    },
    menu_body: {
        // justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        // flexWrap: 'wrap'
    },
    menu_content: {
        // width: '20%',
        alignItems: 'center',
        marginBottom: 18,
        paddingLeft: 20,
    },
    menu_content_icon: {
        width: 50,
        height: 50,
        borderWidth: 1,
        backgroundColor: '#fff',
        borderColor: '#fff',
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 3,
        shadowColor: colors.black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 5,
    },
    menu_content_font: {
        fontSize:11,
        fontFamily: fonts.primary[600],
        textAlign:'center',
        marginTop: 6,
        maxWidth: 60,
        color: colors.white
    },

    produk_terlaris_card: {
        paddingVertical: 20,
        borderRadius: 20
    },

    produk_terlaris_content: {
        left: 10,
        padding: 5,
        borderRadius:10
    },

    produk_terbaru_card: {
    },
    produk_terbaru_body: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
        position: 'relative',
        paddingHorizontal:5
    },
    produk_terbaru_content: {
        width: '50%',
        alignItems: 'center',
        marginBottom: 10,
    },
    produk_card: {
        width: wp('45%'),
        borderRadius:10,
        elevation:2,
        backgroundColor:colors.white,
        // borderWidth:0.5,
        borderColor: colors.dark1,
        shadowColor: colors.black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 3,
    },
    produk_card_image: {
        width: wp('45%'),
        height: wp('45%'),
        borderRadius: 10,
        backgroundColor: 'white'
    },
    produk_image: {
        width: undefined,
        height: undefined,
        resizeMode: 'cover',
        flex: 1,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },
    produk_card_desc: {
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    produk_font_judul: {
        fontSize: 14,
        fontFamily:fonts.primary[400],
        color:colors.black,
        maxWidth:150
    },
    produk_font_harga: {
        fontSize: 16,
        fontFamily:fonts.primary[700],
        color:'#FA591D',
        maxWidth:150
    },
    produk_font_lokasi: {
        fontSize: 13,
        fontFamily:fonts.primary[400],
        color:colors.dark1,
        maxWidth:150
    },    
    
    carousel_item: {
        width: wp('100%'),
        height: hp('38%'),
    },
    carousel_imageContainer: {
        flex: 1,
        marginBottom: Platform.select({ios: 0,
            android: 1}), 
        backgroundColor: 'transparent',
    },
    carousel_image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'cover',
    },
    player: {
        alignSelf: 'stretch',
        // marginVertical: 10,
      },
   
});

export default Home;
    

