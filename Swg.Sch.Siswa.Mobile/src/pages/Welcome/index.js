import React, { useEffect } from 'react'
import { StyleSheet, Image, View, Text, ImageBackground } from 'react-native'
import { ILLogin, ILBg1, ILBg2 } from '../../assets'
import { Button, Gap } from '../../components'
import { colors, fonts } from '../../utils'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const Welcome = ({ navigation }) => {
  
  return (
    <ImageBackground source={ILBg2} style={styles.page}>
      <View style={{alignItems:'center'}}>
        <Text style={styles.title}>Selamat datang di ayamayam, buka toko mu sekarang !</Text>    
        <View style={{ width: wp('90%'), height: wp('90%')}}>
          <Image source={ILLogin} style={{width: undefined, height: undefined, resizeMode: 'contain', flex: 1}} />
        </View>
      </View>
      <View>
          <Button 
              type="secondary" 
              title="Registrasi" 
              onPress={() => navigation.navigate('Register')}/>
          <Gap height={16} />
          <Button 
              title="Masuk" 
              onPress={() => navigation.navigate('Masuk')}/>
      </View>
    </ImageBackground>
  )
}

export default Welcome

const styles = StyleSheet.create({
  page : {
      padding: 30, 
      justifyContent: 'space-between', 
      backgroundColor: colors.yellow1, 
      flex: 1
  },
  title: {
      fontSize: 28, 
      color: colors.white, 
      marginTop: wp('10%'),
      fontFamily: fonts.primary[600]
  },
})