import React, {useState, useRef, useEffect} from 'react';
import {
    Text, 
    View, 
    Image, 
    StatusBar, 
    StyleSheet, 
    ScrollView, 
    TouchableOpacity, 
    Platform,
    Dimensions
} from 'react-native';
import { colors } from '../../utils/colors';
import { fonts } from '../../utils';
import { IconDownloadColor, IconDateWhite, ILRapor, IconSearchWhite, ILNilaiKosong, IconCoRight } from '../../assets';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { AlertHeader, Gap, HeaderCard, LoadingCard } from '../../components';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Modalize } from 'react-native-modalize';
import { useDispatch, useSelector } from 'react-redux';
import BouncyCheckbox from "react-native-bouncy-checkbox";
import { setDataRaporByTahunAjaran, setDataRapors, setDataTagihansNoPin, setDataTahunAjarans } from '../../config/redux/action';
import { FUNCDownloadFile, FUNCToast } from '../../config/function';
import { Picker } from '@react-native-picker/picker';
import { BASEURL } from '../../config/helpers';

const Rapor = ({navigation}) => {
    const modalTahunAjaranRef = useRef(null);
    const { dataTagihans } = useSelector(state => state.tagihanReducer);
    const { dataRapors, dataTahunAjarans } = useSelector(state => state.raporReducer);
    const dispatch = useDispatch();
    const [tp, setTp] = useState(0);

    useEffect(() => {
        dispatch(setDataTahunAjarans());
        dispatch(setDataRapors());
        dispatch(setDataTagihansNoPin());
        setTp(0);
    }, [dispatch])
    
    const btnReload = () => {
        dispatch(setDataRaporByTahunAjaran(tp))
        modalTahunAjaranRef.current.close()
    }

    const btnDownload = (file) => {
        if (dataTagihans.RpSebelumnya > 0) {
            navigation.navigate('PinRapor');
            return;
        }

        // FUNCDownloadFile(`${BASEURL}/File/Raport/${file}`) lama
        // FUNCDownloadFile(`${BASEURL}/Asset/Files/Siswa/Rapor/${file}`) baru
        navigation.navigate('DetilPdf', {source : {uri : `${BASEURL}/Asset/Files/Siswa/Rapor/${file}`, cache: true}})
            
    }
   
    return (
        <>
            <StatusBar translucent barStyle="light-content" backgroundColor="transparent"  />
            <View style={{ flex: 1, backgroundColor: colors.primary }}>
                <HeaderCard
                    statusBar={getStatusBarHeight()}
                    icon={<IconDateWhite />}
                    onPress={() => modalTahunAjaranRef.current?.open()}
                    title={'Rapor'}
                    isBack={true}
                    onPressBack={() => navigation.goBack()}
                />
                
                <View style={styles.content}>
                    
                            
                    {
                        dataRapors ?
                            dataRapors.length !== 0 ?
                                <>
                                    <ScrollView>
                                        <View style={{ paddingHorizontal: 10, paddingTop: 10 }}>
                                            <AlertHeader desc="[Disclaimer] Dokumen raport asli yang sudah ditandatangan Kepala Sekolah disimpan di sekolah" />
                                            <Gap height={10} />
                                            <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10, backgroundColor:colors.primary, borderRadius:10, marginBottom:10}}>
                                                <View style={{padding:5}}>
                                                    <Text style={{ fontSize: 18, color: colors.white, fontFamily: fonts.primary[700], maxWidth: wp('45%') }}>Unduh Rapor </Text>
                                                    {
                                                        tp !== 0 ? <Text style={{ fontSize: 18, color: colors.white, fontFamily: fonts.primary[600], maxWidth:wp('45%')}}>(TA {tp}) </Text> : null
                                                    }
                                                    
                                                    <Gap height={20} />
                                                    <Text style={{ fontSize: 14, color: colors.white, fontFamily: fonts.primary[600], maxWidth:wp('45%')}}>Tersedia : </Text>
                                                    <Text style={{ fontSize: 16, color: colors.white, fontFamily: fonts.primary[700], maxWidth: wp('45%') }}>{dataRapors.length} File Rapor </Text>
                                                </View>
                                                <View>
                                                    <View style={{width: 210, height: 110}}>
                                                        <Image source={ILRapor} style={{width: undefined, height: undefined, resizeMode: 'stretch', flex: 1}} />
                                                    </View> 
                                                </View>
                                            </View>
                                            {
                                                dataRapors.map((v, i) => {
                                                    return (
                                                        <TouchableOpacity onPress={() => btnDownload(`${v.FileRapor}`)} key={i} style={styles.cardMenu}>
                                                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 10, paddingHorizontal: 10 }}>
                                                                <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                                    <View style={{ padding: 8 }}>
                                                                        <IconDownloadColor />
                                                                    </View>
                                                                </View>
                                                                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                                                    <View>
                                                                        <Text style={{ fontSize: 15, fontFamily: fonts.primary[500], marginLeft: 10 }}>{v.JenisRapor}</Text>
                                                                        {/* <Text style={{ fontSize: 13, fontFamily: fonts.primary[500], color:colors.dark1, marginLeft: 10, maxWidth:wp('80%') }}>Kode Rapor : 10</Text> */}
                                                                    </View>
                                                                    <IconCoRight />
                                                                </View>
                                                            </View>
                                                        </TouchableOpacity>
                                                    )
                                                })
                                            }
                                        </View>
                                    </ScrollView>
                                </>
                                :
                                <View style={{ flex: 1, alignItems: 'center', alignContent: 'center', justifyContent: 'center', backgroundColor:colors.white,borderTopLeftRadius: 20, borderTopRightRadius:20 }}>
                                    <View style={{width: 300, height: 200, marginTop:-100}}>
                                        <Image source={ILNilaiKosong} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 0}} />
                                    </View>
                                    <Gap height={50}/>
                                    <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[400] }}>Yah, tidak ada rapor :(</Text>
                                </View>
                            : <LoadingCard />
                    }

                    
                            
                        
                    
                </View>

            </View>
          

            <Modalize
                ref={modalTahunAjaranRef}
                modalHeight={hp('30%')}
                handlePosition="inside"
            >
                <View style={{padding: 10, top: 10, height:hp('29%')}}>
                    <Text style={styles.modal_title}>Pilih Tahun Ajaran</Text>
                    <Gap height={15} />
                    <View style={Platform.OS !== 'ios' ? {borderWidth:0.5, borderColor:colors.dark2, borderRadius:7,  backgroundColor:colors.white} : {marginTop:-30}}>
                        <Picker
                            style={{ width: wp('90%') }}
                            selectedValue={tp}
                            onValueChange={(itemValue, itemIndex) =>
                                setTp(itemValue)
                            }
                            mode="dialog"
                        >
                            {
                                dataTahunAjarans ?
                                    dataTahunAjarans.length !== 0 ?
                                        dataTahunAjarans.map((v, i) => {
                                            return (
                                                <Picker.Item key={i} label={`${v.Nama} - ${v.IdTahunAjaran}`} value={v.IdTahunAjaran} />
                                            )
                                        })
                                        : null
                                    : null
                                
                            }
                        </Picker>
                    </View>
                    <Gap height={20} />
                    <View style={{position:'absolute', bottom:0 }}>
                        <TouchableOpacity onPress={() => btnReload()} style={{ backgroundColor: colors.primary, justifyContent: 'center', alignItems: 'center', width: wp('100%'), flexDirection: 'row', paddingVertical: Platform.OS === 'ios' ? 20 : 15, paddingBottom:25 }}>
                            <IconSearchWhite />
                            <Text style={{ color: colors.white, fontFamily: fonts.primary[700], fontSize: 15, marginLeft: 5 }}>Pencarian</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modalize>
        </>
    )
}

export default Rapor

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor : colors.white,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    cardMenu: {
        backgroundColor: colors.white,
        borderRadius:10,
        elevation:2,
        marginBottom: 10,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2,
    },
    modal_content: {
        padding: 10,
        top: 10,
        height:hp('90%')
    },
    modal_title: {
        fontSize: 18, 
        color: colors.black, 
        fontFamily: fonts.primary[600]
    },
    modal_com_input: {
        width: '100%', 
        height:40, 
        backgroundColor: colors.white, 
        borderWidth:0.5, 
        borderColor:colors.dark2, 
        borderRadius: 5, 
        justifyContent:'center' 
    },
    modal_com_input_text: {
        fontSize: 13,
        color: colors.black,
        fontFamily: fonts.primary[400],
        padding: 10
    },
    cardMenu: {
        backgroundColor: colors.white,
        borderRadius:10,
        elevation:2,
        marginBottom: 10,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2,
    },
    container: {
        flexDirection: 'row',
        padding: 16,
        borderBottomWidth: 0.5,
        borderBottomColor: colors.dark2,
        alignItems: 'center',
    },
    avatar: {
        width: 46,
        height: 46,
        borderRadius: 46/2,
        marginRight: 12,
        backgroundColor: colors.primary
    },
    name: {
        fontSize: 16,
        fontFamily: fonts.primary.normal,
        color: colors.black,
    },
    desc: {
        fontSize: 12,
        fontFamily: fonts.primary[300],
        color: colors.text.secondary,
    }
})
