import React from 'react'
import { SafeAreaView, StyleSheet, TouchableOpacity, View } from 'react-native'
import YouTube, { YouTubeStandaloneIOS, YouTubeStandaloneAndroid } from 'react-native-youtube';
import { colors } from '../../utils';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { IconBack } from '../../assets';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Gap } from '../../components';

const DetilVidio = ({ navigation, route }) => {
  const { videoId } = route.params;
  return (
    <View style={{ flex: 1, backgroundColor: colors.black }}>
      <Gap height={getStatusBarHeight()} />
      <View style={styles.header_body}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <TouchableOpacity onPress={() => navigation.goBack()}>
                  <IconBack />
              </TouchableOpacity>
          </View>
      </View>
      <View style={{marginTop:-getStatusBarHeight()}} />
      <View style={{ flex:1, alignItems: 'center', justifyContent:'center' }}>
        <YouTube
            apiKey="AIzaSyAZirAjBTitnWrkpdqxuW2ih8UhwLad87Y"
            videoId={videoId}
            play={true}
            loop={false}
            fullscreen={true}
            controls={3}
            style={{ width: wp('100%%'), height: hp('30%')}}
        />
      </View>
    </View>
  )
}

export default DetilVidio

const styles = StyleSheet.create({
  header_body: {
        flexDirection: 'row',
        paddingVertical:18,
        paddingHorizontal:10,
        alignItems: 'center',
        justifyContent: 'space-between'
    },  
})
