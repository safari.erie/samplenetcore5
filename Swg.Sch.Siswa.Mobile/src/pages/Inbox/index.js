import React, {useState, useRef, useEffect} from 'react';
import { Text, View, TouchableOpacity, StatusBar, StyleSheet, ScrollView, Image } from 'react-native';
import { colors } from '../../utils/colors';
import { fonts } from '../../utils';
import { IconRefresh, ILNilaiKosong } from '../../assets';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Gap, HeaderCard } from '../../components';
import { useDispatch, useSelector } from 'react-redux';
import { apiReadInbox, setDataInboxs } from '../../config/redux/action';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import { Modalize } from 'react-native-modalize';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const Inbox = () => {
    const modalReadRef = useRef(null);
    const { dataInboxs } = useSelector(state => state.inboxReducer);
    const dispatch = useDispatch();
    const [idInbox, setIdInbox] = useState(null)
    const [judul, setJudul] = useState(null)
    const [deskripsi, setDeskripsi] = useState(null)

    useEffect(() => {
        dispatch(setDataInboxs());
    }, [dispatch])
    
    const btnReload = () => {
        dispatch(setDataInboxs());
    }

    const btnDetail = (id, judul, deskripsi) => {
        modalReadRef.current.open();
        setIdInbox(id);
        setJudul(judul);
        setDeskripsi(deskripsi);
    }
    return (
        <>
            <StatusBar translucent barStyle="light-content" backgroundColor="transparent"  />
            <View style={styles.android_safearea}>
              
                <HeaderCard
                    statusBar={getStatusBarHeight()}
                    title={'Inbox'}
                    icon={<IconRefresh />}
                    onPress={() => btnReload()}
                />
                
                <View style={styles.content}>
                        {
                            dataInboxs ?
                                dataInboxs.length !== 0 ?
                                <ScrollView>
                                    {
                                        dataInboxs.map((v, i) => {
                                            return (
                                                <TouchableOpacity onPress={() => btnDetail(v.IdSiswaInbox, v.Judul, v.Deskripsi)} key={i} style={{ paddingHorizontal: 15, backgroundColor: v.IsRead ? colors.white : colors.secondary2 }}>
                                                    <View style={{ borderBottomWidth: 0.5, borderBottomColor: colors.dark2, paddingVertical: 10 }}>
                                                        <Text style={{ fontSize: 10, color: colors.dark1, fontFamily: fonts.primary[400] }}>Info {v.StrTipe} · {v.CreatedDate}</Text>
                                                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[600] }}>{v.Judul}</Text>
                                                        <Text style={{ fontSize: 13, color: colors.dark1, fontFamily: fonts.primary[500] }}>{v.Deskripsi}</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            )
                                        })
                                    }
                                </ScrollView>
                                :
                                <View style={{ flex: 1, alignItems: 'center', alignContent: 'center', justifyContent: 'center', backgroundColor:colors.white,borderTopLeftRadius: 20, borderTopRightRadius:20 }}>
                                    <View style={{width: 300, height: 200}}>
                                        <Image source={ILNilaiKosong} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 0}} />
                                    </View>
                                    <Gap height={50}/>
                                    <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[400] }}>Tidak ada notifikasi!</Text>
                                </View>
                            :
                            <SkeletonPlaceholder> 
                                <SkeletonPlaceholder.Item paddingHorizontal={5} paddingVertical={5}>
                                    <SkeletonPlaceholder.Item width={'100%'} height={100} marginBottom={5} />
                                    <SkeletonPlaceholder.Item width={'100%'} height={100} marginBottom={5} />
                                    <SkeletonPlaceholder.Item width={'100%'} height={100} marginBottom={5} />
                                    <SkeletonPlaceholder.Item width={'100%'} height={100} marginBottom={5} />
                                    <SkeletonPlaceholder.Item width={'100%'} height={100} marginBottom={5} />
                                    <SkeletonPlaceholder.Item width={'100%'} height={100} marginBottom={5} />
                                </SkeletonPlaceholder.Item>
                            </SkeletonPlaceholder>
                        }
                        
                        

                    
                </View>
            </View>

            <Modalize
                ref={modalReadRef}
                snapPoint={250}
                modalHeight={hp('80%')}
                handlePosition="inside"
                onClose={() => dispatch(apiReadInbox(idInbox))}
            >
                <ScrollView>
                    <Gap height={10} />
                    <View style={{padding:10}}>
                        <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600]}}>{judul}</Text>
                        <Text style={{ fontSize: 14, color: colors.dark1, fontFamily: fonts.primary[500]}}>{deskripsi}</Text>
                    </View>
                </ScrollView>
            </Modalize>
        </>
    )
}

export default Inbox

const styles = StyleSheet.create({
    android_safearea: {
        flex: 1,
        backgroundColor: colors.primary
    },
    content: {
        flex: 1,
        backgroundColor : colors.white,
        // borderTopLeftRadius: 20,
        // borderTopRightRadius: 20,
    },
})
