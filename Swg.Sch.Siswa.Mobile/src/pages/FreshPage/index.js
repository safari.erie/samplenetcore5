import * as React from 'react';
import { Text, View, TouchableOpacity, StatusBar, StyleSheet, ScrollView,Dimensions } from 'react-native';
import { colors } from '../../utils/colors';
import { fonts } from '../../utils';
import { IconFPaketData, IconHome, IconMore, IconMoreActive, IconRefresh } from '../../assets';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Gap, HeaderCard } from '../../components';
const FreshPage = () => {
    return (
        <>
            <StatusBar translucent barStyle="light-content" backgroundColor="transparent"  />
            <View style={styles.android_safearea}>
              
                <Gap height={getStatusBarHeight()} />
                <HeaderCard
                    title={'Inbox'}
                    icon={<IconRefresh />}
                    onPress={() => alert('oi')}
                />
                
                <View style={styles.content}>
                    <ScrollView>
                        
                    </ScrollView>
                </View>
            </View>
        </>
    )
}

export default FreshPage

const styles = StyleSheet.create({
    android_safearea: {
        flex: 1,
        backgroundColor: colors.primary
    },
    content: {
        flex: 1,
        backgroundColor : colors.white,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
})
