import React, {useState, useRef, useEffect} from 'react';
import { Text, View, Image, StatusBar, StyleSheet, ScrollView,TouchableOpacity } from 'react-native';
import { colors } from '../../utils/colors';
import { fonts } from '../../utils';
import { IconBack, IconFPaketData, IconHome, ILTagihanBerjalan, ILTagihanSebelum, IconRefresh, ILEbook } from '../../assets';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Gap, HeaderCard } from '../../components';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Modalize } from 'react-native-modalize';
import { useDispatch, useSelector } from 'react-redux';
import { setDataTagihansNoPin } from '../../config/redux/action/tagihanAction';
import { FUNCAngkaToRupiah } from '../../config/function';
const Tagihan = ({navigation}) => {
    const modalTagihanBerjalanRef = useRef(null);
    const modalTagihanSebelumRef = useRef(null);
    const { dataTagihans } = useSelector(state => state.tagihanReducer);
    const dispatch = useDispatch();
    const btnReload = () => dispatch(setDataTagihansNoPin());
   
    return (
        <>
            <StatusBar translucent barStyle="light-content" backgroundColor="transparent"  />
            <View style={{ flex: 1, backgroundColor: colors.primary }}>
                <HeaderCard
                    statusBar={getStatusBarHeight()}
                    icon={<IconRefresh />}
                    onPress={() => btnReload()}
                    title={'Tagihan'}
                    isBack={true}
                    onPressBack={() => navigation.goBack()}
                />
                
                <View style={styles.content}>
                    <ScrollView>
                        <View style={{ paddingHorizontal: 10 }}>
                            <Gap height={10} />
                            <TouchableOpacity onPress={() => modalTagihanBerjalanRef.current?.open()} style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10, backgroundColor:colors.cardLight, borderRadius:10, marginBottom:10}}>
                                <View style={{padding:5}}>
                                    <Text style={{ fontSize: 20, color: colors.white, fontFamily: fonts.primary[700], maxWidth: wp('45%') }}>Tahun Berjalan</Text>
                                    <Gap height={30} />
                                    <Text style={{ fontSize: 15, color: colors.white, fontFamily: fonts.primary[600], maxWidth:wp('45%')}}>Total Tagihan : </Text>
                                    <Text style={{ fontSize: 18, color: colors.white, fontFamily: fonts.primary[700], maxWidth: wp('45%') }}>RP {FUNCAngkaToRupiah(dataTagihans.RpBerjalan)}</Text>
                                </View>
                                <View>
                                    <View style={{width: 130, height: 130}}>
                                        <Image source={ILTagihanBerjalan} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 0}} />
                                    </View> 
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => modalTagihanSebelumRef.current?.open()} style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10, backgroundColor:'#4caf50', borderRadius:10, marginBottom:10}}>
                                <View style={{padding:5}}>
                                    <Text style={{ fontSize: 20, color: colors.white, fontFamily: fonts.primary[700], maxWidth: wp('45%') }}>Tahun Sebelum</Text>
                                    <Gap height={30} />
                                    <Text style={{ fontSize: 15, color: colors.white, fontFamily: fonts.primary[600], maxWidth:wp('45%')}}>Total Tagihan : </Text>
                                    <Text style={{ fontSize: 18, color: colors.white, fontFamily: fonts.primary[700], maxWidth: wp('45%') }}>RP {FUNCAngkaToRupiah(dataTagihans.RpSebelumnya)}</Text>
                                </View>
                                <View>
                                    <View style={{width: 130, height: 130}}>
                                        <Image source={ILTagihanSebelum} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                                    </View> 
                                </View>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
            </View>


            <Modalize
                ref={modalTagihanBerjalanRef}
                handlePosition="inside"
            >
                <ScrollView>
                    <View style={{ paddingHorizontal: 15, paddingVertical: 20 }}>
                        <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10, backgroundColor:colors.cardLight, borderRadius:10, marginBottom:10}}>
                            <View style={{padding:5}}>
                                <Text style={{ fontSize: 20, color: colors.white, fontFamily: fonts.primary[700], maxWidth: wp('45%') }}>Tahun Berjalan</Text>
                                <Gap height={30} />
                                <Text style={{ fontSize: 15, color: colors.white, fontFamily: fonts.primary[600], maxWidth:wp('45%')}}>Total Tagihan : </Text>
                                <Text style={{ fontSize: 18, color: colors.white, fontFamily: fonts.primary[700], maxWidth: wp('45%') }}>RP {FUNCAngkaToRupiah(dataTagihans.RpBerjalan)}</Text>
                            </View>
                            <View>
                                <View style={{width: 130, height: 130}}>
                                    <Image source={ILTagihanBerjalan} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 0}} />
                                </View> 
                            </View>
                        </View>
                        <Text style={{ fontSize: 18, color: colors.black, fontFamily: fonts.primary[700] }}>Rincian Tagihan</Text>
                        <Gap height={10} />
                        {
                            dataTagihans.TagihanBerjalan.map((v, i) => {
                                return (
                                    <View key={i} style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom:5, borderBottomWidth:0.5, borderBottomColor:colors.dark2, paddingBottom:5 }}>
                                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[500] }}>{v.JenisTagihan}</Text>
                                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[600] }}>{FUNCAngkaToRupiah(v.Rp)}</Text>
                                    </View>
                                )
                            })
                        }
                    </View>
                </ScrollView>
            </Modalize>
            <Modalize
                ref={modalTagihanSebelumRef}
                handlePosition="inside"
            >
                <ScrollView>
                    <View style={{ paddingHorizontal: 15, paddingVertical: 20 }}>
                        <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10, backgroundColor:'#4caf50', borderRadius:10, marginBottom:10}}>
                            <View style={{padding:5}}>
                                <Text style={{ fontSize: 20, color: colors.white, fontFamily: fonts.primary[700], maxWidth: wp('45%') }}>Tahun Sebelum</Text>
                                <Gap height={30} />
                                <Text style={{ fontSize: 15, color: colors.white, fontFamily: fonts.primary[600], maxWidth:wp('45%')}}>Total Tagihan : </Text>
                                <Text style={{ fontSize: 18, color: colors.white, fontFamily: fonts.primary[700], maxWidth:wp('45%')}}>RP {FUNCAngkaToRupiah(dataTagihans.RpSebelumnya)}</Text>
                            </View>
                            <View>
                                <View style={{width: 130, height: 130}}>
                                    <Image source={ILTagihanSebelum} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                                </View> 
                            </View>
                        </View>
                        <Text style={{ fontSize: 18, color: colors.black, fontFamily: fonts.primary[700] }}>Rincian Tagihan</Text>
                        <Gap height={10} />
                        {
                            dataTagihans.TagihanSebelumnya.map((v, i) => {
                                return (
                                    <View key={i} style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom:5, borderBottomWidth:0.5, borderBottomColor:colors.dark2, paddingBottom:5 }}>
                                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[500] }}>{v.JenisTagihan}</Text>
                                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[600] }}>{FUNCAngkaToRupiah(v.Rp)}</Text>
                                    </View>
                                )
                            })
                        }
                    </View>
                </ScrollView>
            </Modalize>
        </>
    )
}

export default Tagihan

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor : colors.white,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    container: {
        flexDirection: 'row',
        padding: 16,
        borderBottomWidth: 0.5,
        borderBottomColor: colors.dark2,
        alignItems: 'center',
    },
    avatar: {
        width: 46,
        height: 46,
        borderRadius: 46/2,
        marginRight: 12,
        backgroundColor: colors.primary
    },
    name: {
        fontSize: 16,
        fontFamily: fonts.primary.normal,
        color: colors.black,
    },
    desc: {
        fontSize: 12,
        fontFamily: fonts.primary[300],
        color: colors.text.secondary,
    }
})
