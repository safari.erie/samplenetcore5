import React, {useState, useRef, useEffect} from 'react';
import { Text,
    View,
    TouchableOpacity,
    Image,
    StyleSheet,
    ScrollView,
    StatusBar,
    useWindowDimensions,
    Platform,
    TextInput,
    Linking
} from 'react-native';
import { colors } from '../../utils/colors';
import { fonts } from '../../utils';
import {
    IconRefreshDark,
    IconBackDark,
    IconMenuTutorial,
    IconRight,
    IconMenuLearning,
    ILEbook,
    IconSkorPrimary,
    IconUploadWhite,
    IconMenuPembayaran,
    IconMenuIklan,
    IconImageWhite,
    IconCameraWhite,
    IconFileWhite,
    ILLoading,
    ILNilaiKosong,
    Loading,
    IconDelete,
    ILNullPhoto,
    IconDownloadColor,
    IconRuangBelajarMateri,
    IconRuangBelajarDeskripsi,
    IconRuangBelajarAnggota,
    IconRuangBelajarLink,
    IconRuangBelajarDownload,
    IconCatatanOrange,
    IconRemedialOrange
} from '../../assets';
import { Gap, AlertHeader, LoadingCard, Avatar } from '../../components';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Modalize } from 'react-native-modalize';
import HTML from "react-native-render-html";
import { useDispatch, useSelector } from 'react-redux';
import {
    apiUploadJawaban,
    setDataElearning,
    setDataJenisBantuanOrtus,
    setFormJawaban
} from '../../config/redux/action';
import { FUNCDownloadFile, FUNCToast } from '../../config/function';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import DocumentPicker from "react-native-document-picker";
import { Picker } from '@react-native-picker/picker';
import { BASEURL } from '../../config/helpers';


const RuangBelajar = ({ navigation, route }) => {
    const { idKbmMateri } = route.params;
    const modalSoalRegulerRef = useRef(null);
    const modalSoalRemedialRef = useRef(null);
    const modalAnggotaRef = useRef(null);
    const modalKirimTugasRef = useRef(null);
    const modalSkorRef = useRef(null);
    const modalCatatanRef = useRef(null);
    const contentWidth = useWindowDimensions().width;

    const { dataElearning, dataJenisBantuanOrtus, formJawaban } = useSelector(state => state.elearningReducer)
    const dispatch = useDispatch();
    // const isFocused = useIsFocused();

    const {
        IdJenisBantuanOrtu,
        NamaUrl,
        FileUpload
    } = formJawaban;

    useEffect(() => {
        dispatch(setDataElearning(idKbmMateri));
        dispatch(setFormJawaban('IdJenisBantuanOrtu', 0));
        dispatch(setFormJawaban('NamaUrl', ''));
        dispatch(setFormJawaban('FileUpload', []));
        dispatch(setDataJenisBantuanOrtus());
    }, [dispatch])

    
    const btnCamera = () => {
        FUNCToast('LOADING', { msg: 'sedang memuat...' })
        let options = {
            cameraType: 'front',
            mediaType: 'photo' ,
            quality: 1,
            saveToPhotos: true
        };
        launchCamera(options, (response) => {
            FUNCToast('HIDE');
            dispatch(setFormJawaban('FileUpload', []));
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                FUNCToast('FAIL', { msg: response.error })
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else if (response.errorCode === 'camera_unavailable') {
                FUNCToast('FAIL', { msg: response.errorCode })
            } else {
                
                if (typeof response.errorCode === 'undefined') {
                    var array = [];
                    response.assets.map((v, i) => {
                        array.push({
                            uri: v.uri,
                            type: v.type,
                            name: v.fileName,
                        });
                    });
                    dispatch(setFormJawaban('FileUpload', array));
                } else {
                    FUNCToast('FAIL', { msg: 'Izinkan kami untuk mengakses kamera!' })
                }
            }
        });
    }

    const btnGallery = () => {
        FUNCToast('LOADING', { msg: 'sedang memuat...' })
        let options = {
            cameraType: 'front',
            mediaType: 'photo' ,
            selectionLimit: 5
        }; 
        launchImageLibrary(options, (response) => {
            FUNCToast('HIDE');
            dispatch(setFormJawaban('FileUpload', []));
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                var array = [];
                response.assets.map((v, i) => {
                    array.push({
                        uri: v.uri,
                        type: v.type,
                        name: v.fileName,
                    });
                });
                dispatch(setFormJawaban('FileUpload', array));
            }
        });
        
    }

    const btnDocument = async () => {
        FUNCToast('LOADING', {msg: 'sedang memuat...'});
        try {
            const response = await DocumentPicker.pickMultiple({
                type: [DocumentPicker.types.allFiles],
            });
            var array = [];
            response.map((v, i) => {
                array.push({
                    uri: v.uri,
                    type: v.type,
                    name: v.name,
                });
            });
            dispatch(setFormJawaban('FileUpload', array));
            FUNCToast('HIDE');
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
                FUNCToast('HIDE');
            } else {
                FUNCToast('HIDE');
                throw err;
            }
        }
    }

    const btnSerahTugas = () => {
        let nameUrl = dataElearning.IdJenisSoal === 0 ?
            dataElearning.SoalReguler.IdJenisJawaban === 3 ? "Ya" : NamaUrl
            : dataElearning.SoalRemedial.IdJenisJawaban === 3 ? "Ya" : NamaUrl;
        dispatch(apiUploadJawaban(
            dataElearning.IdKbmMateri,
            dataElearning.IdJenisSoal === 0 ? 1 : dataElearning.IdJenisSoal === 1 ? 2 : 1 ,
            IdJenisBantuanOrtu,
            nameUrl,
            FileUpload
        ))
    }

    const btnResetFileUpload = () => {
        FUNCToast('LOADING', {msg: 'sedang memuat...', duration: 500});
        dispatch(setFormJawaban('FileUpload', []));
    }


    return (
        <>
            <StatusBar translucent barStyle="dark-content" backgroundColor="transparent"  />
            <View style={styles.androidSafeArea}>
                <Gap height={getStatusBarHeight()} />
                <View style={styles.header_body}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => navigation.goBack()}>
                            <IconBackDark />
                        </TouchableOpacity>
                    </View>
                    <View>
                        <Text numberOfLines={1} style={{ fontSize: 18, color: colors.black, fontFamily: fonts.primary[700]}}>Ruang Belajar</Text>
                    </View>
                    <TouchableOpacity onPress={() => dispatch(setDataElearning(idKbmMateri))}>
                        <IconRefreshDark />
                    </TouchableOpacity>
                </View>
                {
                    dataElearning ?
                        dataElearning.SoalReguler !== null ?
                            <>
                                <ScrollView>
                                    {
                                        dataElearning.IsRemedial == 1 ?
                                            <AlertHeader desc="Anda belum mengerjakan soal remedial, lakukan segera supaya guru anda tau bahwa anda sudah mengerjakan!" />
                                            : dataElearning.PathFileUrl === null ?
                                                dataElearning.JawabanSusulan !== null ?
                                                    null
                                                    : dataElearning.NilaiAngka === 0 || dataElearning.NilaiAngka === null ?
                                                        dataElearning.NilaiHuruf === null ?
                                                            <AlertHeader desc="Anda belum mengerjakan soal reguler, lakukan segera supaya guru anda tau bahwa anda sudah mengerjakan!" />
                                                            :
                                                            null
                                                        : null
                                                :
                                                null
                                    }
                                    
                                    
                                    <View style={{ paddingHorizontal: 10 }}>
                                            <Gap height={10} />
                                            <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10, backgroundColor:colors.primary, borderRadius:10}}>
                                                <View style={{padding:5}}>
                                                        <Text style={{ fontSize: 18, color: colors.white, fontFamily: fonts.primary[700], maxWidth: wp('45%') }}>{dataElearning.NamaMapel}</Text>
                                                    <Gap height={30} />
                                                        <Text style={{ fontSize: 15, color: colors.white, fontFamily: fonts.primary[700], maxWidth: wp('45%') }}>Kelas {dataElearning.NamaKelasParalel}</Text>
                                                        <Text style={{ fontSize: 13, color: colors.white, fontFamily: fonts.primary[700], maxWidth: wp('45%') }}>{dataElearning.JamMulai} s/d {dataElearning.JamSelesai}</Text>
                                                </View>
                                                <View>
                                                    <View style={{width: 130, height: 130}}>
                                                        <Image source={ILEbook} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                                                    </View> 
                                                </View>
                                            </View>
                                            <Gap height={10} />
                                        <TouchableOpacity onPress={() => navigation.navigate('Materi', {
                                            dataElearning: dataElearning,
                                        })} style={styles.cardMenu}>
                                            <View style={{ flexDirection: 'row', padding: 10, alignItems: 'center', justifyContent: 'space-between' }}>
                                                <View style={{flexDirection:'row', alignItems:'center'}}>
                                                    <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                        <View style={{ padding: 8 }}>
                                                            <IconRuangBelajarMateri />
                                                        </View>
                                                    </View>
                                                    <View style={{left:10}}>
                                                        <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600], maxWidth:wp('70%') }} numberOfLines={1}>Materi {dataElearning.NamaMapel}</Text>
                                                        {
                                                            dataElearning.NamaFileMateri !== null ? 
                                                                <Text style={{ fontSize: 14, color: colors.primary, fontFamily: fonts.primary[400] }}>+1 file</Text>
                                                                : null
                                                        }
                                                        {
                                                            dataElearning.TotFileKbmFile !== 0 ?
                                                                <Text style={{ fontSize: 14, color: colors.primary, fontFamily: fonts.primary[400] }}>+{dataElearning.TotFileKbmFile} gambar</Text>
                                                                : null
                                                        }
                                                        {
                                                            dataElearning.TotFileKbmUrl !== 0 ?
                                                                <Text style={{ fontSize: 14, color: colors.primary, fontFamily: fonts.primary[400]}}>+{dataElearning.TotFileKbmUrl} video</Text>
                                                                : null
                                                        }

                                                    </View>
                                                </View>
                                                <View>
                                                    <IconRight />
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                            
                                            
                                            <TouchableOpacity onPress={() => modalSoalRegulerRef.current?.open()} style={styles.cardMenu}>
                                                <View style={{ flexDirection: 'row', padding: 10, alignItems: 'center', justifyContent: 'space-between' }}>
                                                    <View style={{flexDirection:'row', alignItems:'center'}}>
                                                        <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                            <View style={{ padding: 8 }}>
                                                                <IconRuangBelajarDeskripsi />
                                                            </View>
                                                        </View>
                                                        <View style={{left:10}}>
                                                                <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600]}}>Deskripsi Soal Reguler</Text>
                                                                {
                                                                    dataElearning.SoalReguler.NamaFile !== null ?
                                                                        <Text style={{ fontSize: 14, color: colors.primary, fontFamily: fonts.primary[400]}}>+1 file</Text>
                                                                        :
                                                                        null
                                                                }
                                                                {
                                                                    dataElearning.SoalReguler.NamaUrl !== null ?
                                                                        <Text style={{ fontSize: 14, color: colors.primary, fontFamily: fonts.primary[400]}}>+1 Link</Text>
                                                                        :
                                                                        null
                                                                }
                                                        </View>
                                                    </View>
                                                    <View>
                                                        <IconRight />
                                                    </View>
                                                </View>
                                            </TouchableOpacity>
                                            
                                            {
                                                dataElearning.IsRemedial === 1 ?
                                                    <TouchableOpacity onPress={() => modalSoalRemedialRef.current?.open()} style={styles.cardMenu}>
                                                        <View style={{ flexDirection: 'row', padding: 10, alignItems: 'center', justifyContent: 'space-between' }}>
                                                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                                                <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                                    <View style={{ padding: 8 }}>
                                                                        <IconRemedialOrange />
                                                                    </View>
                                                                </View>
                                                                <View style={{left:10}}>
                                                                        <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Deskripsi Soal Remedial</Text>
                                                                        {
                                                                            dataElearning.SoalRemedial.NamaFile !== null ?
                                                                                <Text style={{ fontSize: 14, color: colors.primary, fontFamily: fonts.primary[400]}}>+1 file</Text>
                                                                                :
                                                                                null
                                                                        }
                                                                        {
                                                                            dataElearning.SoalRemedial.NamaUrl !== null ?
                                                                                <Text style={{ fontSize: 14, color: colors.primary, fontFamily: fonts.primary[400]}}>+1 Link</Text>
                                                                                :
                                                                                null
                                                                        }
                                                                </View>
                                                            </View>
                                                            <View>
                                                                <IconRight />
                                                            </View>
                                                        </View>
                                                    </TouchableOpacity>
                                                    :
                                                    null
                                            }
                                            
                                        {
                                            dataElearning.Catatan !== null ?
                                                <TouchableOpacity onPress={() => modalCatatanRef.current?.open()} style={styles.cardMenu}>
                                                    <View style={{ flexDirection: 'row', padding: 10, alignItems: 'center', justifyContent: 'space-between' }}>
                                                        <View style={{flexDirection:'row', alignItems:'center'}}>
                                                            <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                                <View style={{ padding: 8 }}>
                                                                    <IconCatatanOrange />
                                                                </View>
                                                            </View>
                                                            <View style={{left:10}}>
                                                                <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600]}}>Catatan Guru</Text>
                                                            </View>
                                                        </View>
                                                        <View>
                                                            <IconRight />
                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                                : null
                                        }
                                           
                                            
                                            <TouchableOpacity onPress={() => modalAnggotaRef.current?.open()} style={styles.cardMenu}>
                                                <View style={{ flexDirection: 'row', padding: 10, alignItems: 'center', justifyContent: 'space-between' }}>
                                                    <View style={{flexDirection:'row', alignItems:'center'}}>
                                                        <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                            <View style={{ padding: 8 }}>
                                                                <IconRuangBelajarAnggota />
                                                            </View>
                                                        </View>
                                                        <View style={{left:10}}>
                                                            <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600]}}>Anggota Kelas</Text>
                                                            <Text style={{ fontSize: 14, color: colors.primary, fontFamily: fonts.primary[400]}}>{dataElearning.TugasSiswas.length} anggota</Text>
                                                        </View>
                                                    </View>
                                                    <View>
                                                        <IconRight />
                                                    </View>
                                                </View>
                                            </TouchableOpacity>
                                        
                                    </View>
                                </ScrollView>
                                <View style={styles.bottomCheckout}>
                                    <TouchableOpacity onPress={() => modalKirimTugasRef.current.open()} style={{ backgroundColor: colors.primary, borderRadius: 7, justifyContent:'center', alignItems:'center',width:'65%', flexDirection:'row', paddingVertical: 10 }}>
                                        <IconUploadWhite />
                                        <Text style={{color:colors.white, fontFamily:fonts.primary[700], fontSize:14, left: 5}}>Serahkan Tugas</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => modalSkorRef.current.open()}  style={{ backgroundColor: colors.white, borderRadius: 7, justifyContent:'center', alignItems:'center',width:'30%', borderWidth:1, borderColor:colors.primary, flexDirection:'row', paddingVertical: 10 }}>
                                        <IconSkorPrimary />
                                        <Text style={{color:'#FF6721', fontFamily:fonts.primary[700], fontSize:14, left:5}}>Skor</Text>
                                    </TouchableOpacity>
                                </View>
                            </>
                            :
                            <View style={{ flex: 1, alignItems: 'center', alignContent: 'center', justifyContent: 'center', backgroundColor:colors.white,borderTopLeftRadius: 20, borderTopRightRadius:20 }}>
                                <View style={{width: 300, height: 200, marginTop:-100}}>
                                    <Image source={ILNilaiKosong} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 0}} />
                                </View>
                                <Gap height={50}/>
                                <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[400] }}>Yey, tidak ada materi!</Text>
                            </View>
                        :
                        <LoadingCard />
                }
            </View>

            {
                dataElearning ?
                        dataElearning.SoalReguler !== null ?
                        <>
                            <Modalize
                                ref={modalSoalRegulerRef}
                                snapPoint={hp('30%')}
                                handlePosition="inside"
                            >
                                <View style={styles.modal_content}>
                                    <Text style={styles.modal_title}>Deskripsi Soal Reguler</Text>
                                    <Gap height={15} />
                                    <ScrollView>
                                            <HTML source={{ html: dataElearning.DeskripsiTugas || '<p>tidak ada deskripsi</p>' }} contentWidth={contentWidth} />
                                            <Gap height={20} />
                                            {dataElearning.SoalReguler.NamaFile !== null ?
                                                <TouchableOpacity onPress={() => FUNCDownloadFile(`${BASEURL}/Asset/Files/Elearning/${dataElearning.SoalReguler.NamaFile}`)} style={styles.cardMenu}>
                                                    <View style={{ flexDirection: 'row', padding: 10, alignItems: 'center', justifyContent: 'space-between' }}>
                                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                            <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                                <View style={{ padding: 8 }}>
                                                                    <IconRuangBelajarDownload />
                                                                </View>
                                                            </View>
                                                            <View style={{ left: 10 }}>
                                                                <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Download File (Soal Reguler)</Text>
                                                            </View>
                                                        </View>
                                                        <View>
                                                            <IconRight />
                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                                :
                                                null
                                            }
                                            {dataElearning.SoalReguler.NamaUrl !== null ?
                                                <TouchableOpacity onPress={() => Linking.openURL(`${dataElearning.SoalReguler.NamaUrl}`)} style={styles.cardMenu}>
                                                    <View style={{ flexDirection: 'row', margin: 10, alignItems: 'center', justifyContent: 'space-between' }}>
                                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                           <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                                <View style={{ padding: 8 }}>
                                                                    <IconRuangBelajarLink />
                                                                </View>
                                                            </View>
                                                            <View style={{ left: 10 }}>
                                                                <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Link (Soal Reguler)</Text>
                                                            </View>
                                                        </View>
                                                        <View>
                                                            <IconRight />
                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                                :
                                                null
                                            }
                                    </ScrollView>
                                </View>
                            </Modalize>

                            {
                                dataElearning.SoalRemedial !== null ?
                                    <Modalize
                                        ref={modalSoalRemedialRef}
                                        snapPoint={hp('30%')}
                                        handlePosition="inside"
                                    >
                                        <View style={styles.modal_content}>
                                            <Text style={styles.modal_title}>Deskripsi Soal Remedial</Text>
                                            <Gap height={15} />
                                            <ScrollView>
                                                    <HTML source={{ html: dataElearning.DeskripsiTugas || '<p>tidak ada deskripsi</p>' }} contentWidth={contentWidth} />
                                
                                                    <Gap height={20} />
                                                    {dataElearning.SoalRemedial.NamaFile !== null ?
                                                        <TouchableOpacity onPress={() => FUNCDownloadFile(`${BASEURL}/Asset/Files/Elearning/${dataElearning.SoalRemedial.NamaFile}`)} style={styles.cardMenu}>
                                                            <View style={{ flexDirection: 'row', padding: 10, alignItems: 'center', justifyContent: 'space-between' }}>
                                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                                    <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                                        <View style={{ padding: 8 }}>
                                                                            <IconRuangBelajarDownload />
                                                                        </View>
                                                                    </View>
                                                                    <View style={{ left: 10 }}>
                                                                        <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Download File (Soal Remedial)</Text>
                                                                    </View>
                                                                </View>
                                                                <View>
                                                                    <IconRight />
                                                                </View>
                                                            </View>
                                                        </TouchableOpacity>
                                                        :
                                                        null
                                                    }
                                                    {dataElearning.SoalRemedial.NamaUrl !== null ?
                                                        <TouchableOpacity onPress={() => Linking.openURL(`${dataElearning.SoalRemedial.NamaUrl}`)} style={styles.cardMenu}>
                                                            <View style={{ flexDirection: 'row', padding: 10, alignItems: 'center', justifyContent: 'space-between' }}>
                                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                                    <View style={{ backgroundColor: colors.secondary, borderRadius: 10, alignItems: 'center' }}>
                                                                        <View style={{ padding: 8 }}>
                                                                            <IconRuangBelajarLink />
                                                                        </View>
                                                                    </View>
                                                                    <View style={{ left: 10 }}>
                                                                        <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Link (Soal Remedial)</Text>
                                                                    </View>
                                                                </View>
                                                                <View>
                                                                    <IconRight />
                                                                </View>
                                                            </View>
                                                        </TouchableOpacity>
                                                        :
                                                        null
                                                    }
                                            </ScrollView>
                                        </View>
                                    </Modalize>
                                    : null
                            }
                            <Modalize
                                ref={modalAnggotaRef}
                                snapPoint={hp('30%')}
                                handlePosition="inside"
                            >
                                <View style={styles.modal_content}>
                                    <Text style={styles.modal_title}>Anggota Kelas</Text>
                                    <Gap height={15} />
                                    <ScrollView showsVerticalScrollIndicator={false}>
                                        {
                                            dataElearning.TugasSiswas.map((v, i) => {
                                                return (
                                                    <View key={i} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingVertical: 10, borderBottomWidth: 0.4, borderBottomColor: colors.dark2 }}>
                                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                            <View>
                                                                <Avatar source={ILNullPhoto} size={30} />
                                                            </View>
                                                            <View>
                                                                <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[400], maxWidth:wp('80%') }} numberOfLines={1}>{i + 1}. {v.NamaSiswa}</Text>
                                                            </View>
                                                        </View>
                                                        <View>
                                                            {/* <IconMoreActive /> */}
                                                        </View>
                                                    </View>
                                                )
                                            })
                                        }
                                    </ScrollView>
                                </View>
                            </Modalize>

                            {/* cek nilai
                            cek isRemedial */}
                            <Modalize
                                ref={modalKirimTugasRef}
                                modalHeight={dataElearning.IdUnit === 1 ? hp('50%') : hp('38%')}
                                handlePosition="inside"
                                keyboardAvoidingOffset={1}
                            >
                                <View style={{ paddingHorizontal: 10, paddingVertical: 20, height: dataElearning.IdUnit === 1 ? hp('48%') : hp('36%'), alignItems:'center' }}>
                                    <Text style={styles.modal_title}>Kirim Tugas</Text>
                                    {/* <Text style={{ color: colors.black, fontFamily: fonts.primary[700], fontSize: 18 }}>Kirim Tugas</Text> */}
                                
                                    {
                                        dataElearning.IsRemedial === 0 ?
                                            dataElearning.SoalReguler.IdJenisJawaban === 1 ?
                                                FileUpload.length === 0 ?
                                                    <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginHorizontal: wp('10%'), top:20 }}>
                                                        <View style={{ justifyContent: 'space-between', flexDirection: 'row', width: '100%', flexWrap: 'wrap' }}>
                                                            <TouchableOpacity onPress={() => btnDocument()} style={{ width: '25%', alignItems: 'center', marginBottom: 5 }}>
                                                                <View style={{ width: 58, height: 58, borderWidth: 1, backgroundColor: '#009688', borderColor: '#fbfbfb', borderRadius: 100, justifyContent: 'center', alignItems: 'center' }}>
                                                                    <IconFileWhite />
                                                                </View>
                                                                <Text style={{ fontSize: 12, fontFamily: fonts.primary[600], textAlign: 'center', marginTop: 2 }}>Dokumen</Text>
                                                            </TouchableOpacity>
                                                            <TouchableOpacity onPress={() => btnCamera()} style={{ width: '25%', alignItems: 'center', marginBottom: 5 }}>
                                                                <View style={{ width: 58, height: 58, borderWidth: 1, backgroundColor: '#03a9f4', borderColor: '#fbfbfb', borderRadius: 100, justifyContent: 'center', alignItems: 'center' }}>
                                                                    <IconCameraWhite />
                                                                </View>
                                                                <Text style={{ fontSize: 12, fontFamily: fonts.primary[600], textAlign: 'center', marginTop: 2 }}>Kamera</Text>
                                                            </TouchableOpacity>
                                                            <TouchableOpacity onPress={() => btnGallery()} style={{ width: '25%', alignItems: 'center', marginBottom: 5 }}>
                                                                <View style={{ width: 58, height: 58, borderWidth: 1, backgroundColor: colors.yellow1, borderColor: '#fbfbfb', borderRadius: 100, justifyContent: 'center', alignItems: 'center' }}>
                                                                    <IconImageWhite />
                                                                </View>
                                                                <Text style={{ fontSize: 12, fontFamily: fonts.primary[600], textAlign: 'center', marginTop: 2 }}>Galeri</Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    </View>
                                                    :
                                                    <View style={{flexDirection:'row', backgroundColor:colors.secondary, padding:5, borderRadius:10, width:wp('90%'), justifyContent:'space-between', alignItems:'center', top:hp('3%')}}>
                                                        <View style={{ flexDirection: 'row', alignItems:'center' }}>
                                                            <View style={styles.avatar}>
                                                                <View style={{width: 40, height: 40, borderRadius: 100, backgroundColor: 'pink'}}>
                                                                    <Image source={{uri: FileUpload[0].uri}} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                                                                </View> 
                                                            </View>
                                                            {
                                                                FileUpload.length >= 2 ?
                                                                    <View style={styles.avatar1}>
                                                                        <View style={{width: 20, height: 20, borderRadius: 100, backgroundColor: 'pink'}}>
                                                                            <Image source={{uri: FileUpload[1].uri}} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                                                                        </View> 
                                                                    </View>
                                                                    :
                                                                    null
                                                            }
                                                            
                                                            
                                                            <Text style={{ fontSize: 14, fontFamily: fonts.primary[400], color:colors.dark1, textAlign: 'center', marginTop: 0, left:5 }}>+{FileUpload.length} file dipilih</Text>
                                                            
                                                        </View>
                                                        <TouchableOpacity onPress={() => btnResetFileUpload()}>
                                                            <IconDelete />
                                                        </TouchableOpacity>
                                                    </View>

                                                : dataElearning.SoalReguler.IdJenisJawaban === 2 ?
                                                    <View style={{marginTop:hp('1%')}}>
                                                        <TextInput 
                                                            style={{width:wp('90%'), borderBottomWidth:1,borderBottomColor: colors.dark1,paddingVertical: 10,paddingHorizontal: 5,fontSize: 14,color: colors.black}} 
                                                            placeholder={'Masukan URL/Link'} 
                                                            placeholderTextColor={colors.default}
                                                            onChangeText={(e) => dispatch(setFormJawaban('NamaUrl', e))}
                                                        />
                                                    </View>
                                                    : dataElearning.SoalReguler.IdJenisJawaban === 3 ?
                                                        <Text style={{ marginTop:hp('4%'), fontSize: 14, fontFamily: fonts.primary[500], color: colors.dark1, textAlign: 'center', }}>
                                                            Klik serahkan tugas, tidak ada upload berkas!
                                                        </Text>
                                                        : null
                                            : null
                                    }

                                    {
                                        dataElearning.IsRemedial === 1 ?
                                            dataElearning.SoalRemedial.IdJenisJawaban === 1 ?
                                                FileUpload.length === 0 ?
                                                    <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginHorizontal: wp('10%'), top:20 }}>
                                                        <View style={{ justifyContent: 'space-between', flexDirection: 'row', width: '100%', flexWrap: 'wrap' }}>
                                                            <TouchableOpacity onPress={() => btnDocument()} style={{ width: '25%', alignItems: 'center', marginBottom: 5 }}>
                                                                <View style={{ width: 58, height: 58, borderWidth: 1, backgroundColor: '#009688', borderColor: '#fbfbfb', borderRadius: 100, justifyContent: 'center', alignItems: 'center' }}>
                                                                    <IconFileWhite />
                                                                </View>
                                                                <Text style={{ fontSize: 12, fontFamily: fonts.primary[600], textAlign: 'center', marginTop: 2 }}>Dokumen</Text>
                                                            </TouchableOpacity>
                                                            <TouchableOpacity onPress={() => btnCamera()} style={{ width: '25%', alignItems: 'center', marginBottom: 5 }}>
                                                                <View style={{ width: 58, height: 58, borderWidth: 1, backgroundColor: '#03a9f4', borderColor: '#fbfbfb', borderRadius: 100, justifyContent: 'center', alignItems: 'center' }}>
                                                                    <IconCameraWhite />
                                                                </View>
                                                                <Text style={{ fontSize: 12, fontFamily: fonts.primary[600], textAlign: 'center', marginTop: 2 }}>Kamera</Text>
                                                            </TouchableOpacity>
                                                            <TouchableOpacity onPress={() => btnGallery()} style={{ width: '25%', alignItems: 'center', marginBottom: 5 }}>
                                                                <View style={{ width: 58, height: 58, borderWidth: 1, backgroundColor: colors.yellow1, borderColor: '#fbfbfb', borderRadius: 100, justifyContent: 'center', alignItems: 'center' }}>
                                                                    <IconImageWhite />
                                                                </View>
                                                                <Text style={{ fontSize: 12, fontFamily: fonts.primary[600], textAlign: 'center', marginTop: 2 }}>Galeri</Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    </View>
                                                    :
                                                    <View style={{flexDirection:'row', backgroundColor:colors.secondary, padding:5, borderRadius:10, width:wp('90%'), justifyContent:'space-between', alignItems:'center', top:hp('3%')}}>
                                                        <View style={{ flexDirection: 'row', alignItems:'center' }}>
                                                            <View style={styles.avatar}>
                                                                <View style={{width: 40, height: 40, borderRadius: 100, backgroundColor: 'pink'}}>
                                                                    <Image source={{uri: FileUpload[0].uri}} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                                                                </View> 
                                                            </View>
                                                            {
                                                                FileUpload.length >= 2 ?
                                                                    <View style={styles.avatar1}>
                                                                        <View style={{width: 20, height: 20, borderRadius: 100, backgroundColor: 'pink'}}>
                                                                            <Image source={{uri: FileUpload[1].uri}} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                                                                        </View> 
                                                                    </View>
                                                                    :
                                                                    null
                                                            }
                                                            
                                                            
                                                            <Text style={{ fontSize: 14, fontFamily: fonts.primary[400], color:colors.dark1, textAlign: 'center', marginTop: 0, left:5 }}>+{FileUpload.length} file dipilih</Text>
                                                            
                                                        </View>
                                                        <TouchableOpacity onPress={() => btnResetFileUpload()}>
                                                            <IconDelete />
                                                        </TouchableOpacity>
                                                    </View>

                                                : dataElearning.SoalRemedial.IdJenisJawaban === 2 ?
                                                    <View style={{marginTop:hp('1%')}}>
                                                        <TextInput 
                                                            style={{width:wp('90%'), borderBottomWidth:1,borderBottomColor: colors.dark1,paddingVertical: 10,paddingHorizontal: 5,fontSize: 14,color: colors.black}} 
                                                            placeholder={'Masukan URL/Link'} 
                                                            placeholderTextColor={colors.default}
                                                            onChangeText={(e) => dispatch(setFormJawaban('NamaUrl', e))}
                                                        />
                                                    </View>
                                                    : dataElearning.SoalRemedial.IdJenisJawaban === 3 ?
                                                        <Text style={{ marginTop:hp('4%'), fontSize: 14, fontFamily: fonts.primary[500], color: colors.dark1, textAlign: 'center', }}>
                                                            Klik serahkan tugas, tidak ada upload berkas!
                                                        </Text>
                                                        : null
                                            : null
                                    }

                                    {/* CEK JIKA UNIT = 1 MUNCULIN */}
                                    {
                                        dataElearning.IdUnit === 1 ?
                                            <>
                                                <Gap height={Platform.OS !== 'ios' ? hp('4%') : 1} />
                                                <View style={Platform.OS !== 'ios' ? {backgroundColor:'#F2DB73', alignItems:'center', paddingBottom:14, borderRadius:20} : {}}>
                                                    {
                                                        Platform.OS !== 'ios' ? <Text style={{ fontSize: 14, fontFamily: fonts.primary[500], color: colors.black, textAlign: 'center', paddingVertical:4 }}>Pilih Jenis Bantuan Ortu</Text> : null
                                                    }
                                                    <View style={Platform.OS !== 'ios' ? {borderWidth:0.5, borderRadius:20, marginHorizontal:10, backgroundColor:colors.white} : {}}>
                                                        <Picker
                                                            style={{width:wp('90%')}}
                                                            selectedValue={IdJenisBantuanOrtu}
                                                            onValueChange={(itemValue, itemIndex) =>
                                                                dispatch(setFormJawaban('IdJenisBantuanOrtu', itemValue))
                                                            }
                                                            mode="dropdown"
                                                        >
                                                            {
                                                                dataJenisBantuanOrtus.map((v, i) => {
                                                                    return (
                                                                        <Picker.Item key={i} label={v.Nama} value={v.Id} />
                                                                    )
                                                                })
                                                            }
                                                        </Picker>
                                                    </View>
                                                </View>
                                                
                                            </>
                                            :
                                            null
                                    }
                                    

                                    
                                    
                                    {/* <Text style={{ marginTop:hp('4%'), fontSize: 14, fontFamily: fonts.primary[500], color: colors.dark1, textAlign: 'center', }}>
                                        Tidak ada berkas yang harus diserahkan
                                    </Text> */}
                                
                                    
                                    {/* <Gap height={hp('3%')} />
                                    <View style={{flexDirection:'row', backgroundColor:colors.secondary, padding:5, borderRadius:10, width:wp('90%'), alignItems:'center', justifyContent:'space-between'}}>
                                        <View style={{ flexDirection: 'row', alignItems:'center' }}>
                                            <View style={styles.avatar}>
                                                <View style={{width: 40, height: 40, borderRadius: 100, backgroundColor: 'pink'}}>
                                                    <Image source={{uri: test}} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                                                </View> 
                                            </View>
                                            <Text style={{ fontSize: 14, fontFamily: fonts.primary[400], textAlign: 'center', marginTop: 0 }}>dasfasfasga.jpg</Text>
                                        </View>
                                        <IconDelete />
                                    </View> */}
                                    <TouchableOpacity onPress={() => btnSerahTugas()} style={{ backgroundColor: colors.primary, borderRadius: 7, justifyContent: 'center', alignItems: 'center', width: wp('90%'), flexDirection: 'row', paddingVertical: 10,position:'absolute', bottom:0 }}>
                                        <IconUploadWhite />
                                        <Text style={{ color: colors.white, fontFamily: fonts.primary[800], fontSize: 13, left: 5 }}>Serahkan Tugas</Text>
                                    </TouchableOpacity>
                                </View>
                            </Modalize>

                            <Modalize
                                ref={modalSkorRef}
                                // snapPoint={hp('35%')}
                                handlePosition="inside"
                            >
                                <View style={{ flex: 1, alignItems: 'center', alignContent: 'center', justifyContent: 'center', height: hp('90%') }}>
                                    {
                                        dataElearning.NilaiAngka !== null ?
                                            <Text style={{ fontSize: 28, color: colors.black, fontFamily: fonts.primary[400] }}>Skor : {dataElearning.NilaiAngka}</Text>
                                            : null
                                    }
                                    {
                                        dataElearning.NilaiHuruf !== null ?
                                            <Text style={{ fontSize: 28, color: colors.black, fontFamily: fonts.primary[400] }}>Skor : {dataElearning.NilaiHuruf}</Text>
                                            : null
                                    }
                                    {
                                        dataElearning.NilaiAngka === null && dataElearning.NilaiHuruf === null ?
                                            <View style={{ flex: 1, alignItems: 'center', alignContent: 'center', justifyContent: 'center', backgroundColor:colors.white,borderTopLeftRadius: 20, borderTopRightRadius:20 }}>
                                                <View style={{width: 200, height: 200, marginTop:-100}}>
                                                    <Image source={ILNilaiKosong} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                                                </View>
                                                <Gap height={50}/>
                                                <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[400] }}>Yah, belum ada nilai :(</Text>
                                            </View>
                                            : null
                                    }
                                    {/* <Text style={{ fontSize: 28, color: colors.black, fontFamily: fonts.primary[400]}}></Text> */}
                                </View>
                            </Modalize>


                            <Modalize
                                ref={modalCatatanRef}
                                snapPoint={hp('30%')}
                                handlePosition="inside"
                            >
                                <View style={styles.modal_content}>
                                    <Text style={styles.modal_title}>Catatan Guru</Text>
                                    <Gap height={15} />
                                    <ScrollView>
                                            <HTML source={{ html: dataElearning.Catatan || '<p>tidak ada deskripsi</p>' }} contentWidth={contentWidth} />
                                            <Gap height={20} />
                                    </ScrollView>
                                </View>
                            </Modalize>
                        </>
                        : null
                    : null
            }
        </>
    )
}


export default RuangBelajar

const styles = StyleSheet.create({
    androidSafeArea: {
        flex: 1,
        backgroundColor: colors.white
    },
    splashgif: {
        width: 50,
        height: 50
    },
    avatar: {
        width: 46,
        height: 46,
        borderRadius: 100,
        backgroundColor: colors.yellow1,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex:10
    },
    avatar1: {
        width: 25,
        height: 25,
        borderRadius: 100,
        backgroundColor: colors.yellow1,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: -13,
        zIndex:1
    },
    cardMenu: {
        backgroundColor: colors.white,
        borderRadius:10,
        elevation:2,
        marginBottom: 10,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2,
    },
    header_body: {
        flexDirection: 'row',
        paddingVertical:18,
        paddingHorizontal:10,
        alignItems: 'center',
        justifyContent: 'space-between'
    },  
    content: {
        flex: 1,
    },

    icon_body: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap'
    },

    detil_card: {
        flex: 1,
        backgroundColor: colors.white,
        borderRadius: 0,
        paddingVertical: 15,
    },
    notice_card: {
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#F2DB73',
        // paddingHorizontal: 10,
        paddingVertical: 10
    },
    notice_icon: {
        width: wp('20%'),
        alignItems: 'center'
    },
    notice_desc: {
        // maxWidth: wp('90%'),
        // paddingRight: 10
    },
    notice_desc_font: {
        fontSize:13,
        fontFamily:fonts.primary[400],
        color: colors.black,
        maxWidth:wp('70%')
    },
    bottomCheckout: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
        paddingHorizontal: 15,
        paddingVertical: 10,
        backgroundColor: colors.white,
        borderWidth: 0.6,
        borderColor: '#dcdcdc',
        alignItems: 'center',
    },
    modal_content: {
        padding: 10,
        top: 10,
        height: hp('90%'),
    },
    modal_title: {
        fontSize: 18, 
        color: colors.black, 
        fontFamily: fonts.primary[600]
    },
})
