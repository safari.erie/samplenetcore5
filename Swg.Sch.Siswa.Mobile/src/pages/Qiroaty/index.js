import React, {useState, useRef, useEffect} from 'react';
import { Text, View, Image, StatusBar, StyleSheet, ScrollView,TouchableOpacity } from 'react-native';
import { colors } from '../../utils/colors';
import { fonts } from '../../utils';
import {
    IconSearchWhite,
    IconDateWhite,
    IconCloseWhite,
    ILNilaiKosong
} from '../../assets';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Gap, HeaderCard } from '../../components';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Modalize } from 'react-native-modalize';
import { useDispatch, useSelector } from 'react-redux';
import { setDataTagihansNoPin } from '../../config/redux/action/tagihanAction';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Animated from 'react-native-reanimated';
import {DaftarQiroatyCard, LoadingCard, TesQiroatyCard} from '../../components/molecules';
import { setDataQiroatys } from '../../config/redux/action/qiroatyAction';
import { FUNCDateToString } from '../../config/function';
import DateTimePicker from '@react-native-community/datetimepicker';

const Tab = createMaterialTopTabNavigator();
const Qiroaty = ({navigation}) => {
    const modalDateRef = useRef(null);
    const [date, setDate] = useState(new Date());
    const [show, setShow] = useState(false);
    const { dataQiroatys } = useSelector(state => state.qiroatyReducer);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(setDataQiroatys(FUNCDateToString(date)))
    }, [dispatch])

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };

    const btnReload = () => {
        dispatch(setDataQiroatys(FUNCDateToString(date)));
        modalDateRef.current.close()
    }

    
   
    return (
        <>
            <StatusBar translucent barStyle="light-content" backgroundColor="transparent"  />
            <View style={{ flex: 1, backgroundColor: colors.primary }}>
                <HeaderCard
                    statusBar={getStatusBarHeight()}
                    icon={<IconDateWhite />}
                    onPress={() => modalDateRef.current?.open()}
                    title={'Qiroaty'}
                    isBack={true}
                    onPressBack={() => navigation.goBack()}
                />
                
                    {
                        dataQiroatys ?
                            <View style={styles.content}>
                            <Tab.Navigator
                                // tabBar={props => <TabNavigator {...props} />}
                                tabBarOptions={{
                                    labelStyle: { fontSize: 17, color: colors.primary, fontFamily:fonts.primary[700], textTransform: 'none', marginVertical:7 },
                                    style: { backgroundColor: colors.white, borderTopLeftRadius:20, borderTopRightRadius:20 },
                                    indicatorStyle: {
                                        backgroundColor: colors.primary
                                    },
                                }}
                            >
                                    <Tab.Screen name="Daftar Qiroaty" component={DaftarQiroaty} initialParams={{ qiroatys: dataQiroatys.Progress }} />
                                    <Tab.Screen name="Hasil Test" component={TesQiroaty} initialParams={{ qiroatys: dataQiroatys.Ujians }} />
                                </Tab.Navigator>
                            </View>
                            : <LoadingCard />
                    }
                     
            </View>

            <Modalize
                ref={modalDateRef}
                modalHeight={hp('30%')}
                handlePosition="inside"
            >
                <View style={{padding: 10, top: 10, height:hp('29%')}}>
                    <Text style={styles.modal_title}>Pilih Tanggal</Text>
                    <Gap height={15} />
                    <TouchableOpacity onPress={() => setShow(true)} style={styles.modal_com_input}>
                        {Platform.OS !== 'ios' ?
                            <Text style={styles.modal_com_input_text}>{FUNCDateToString(date)}</Text>
                            :
                            null
                        }
                        {show && (
                            <DateTimePicker
                                testID="dateTimePicker"
                                value={date}
                                mode={'date'}
                                is24Hour={true}
                                display="default"
                                onChange={onChange}
                                maximumDate={new Date()}
                            />
                        )}
                    </TouchableOpacity>
                    <Gap height={20} />
                    <View style={{position:'absolute', bottom:0 }}>
                        <TouchableOpacity onPress={() => btnReload()} style={{ backgroundColor: colors.primary, justifyContent: 'center', alignItems: 'center', width: wp('100%'), flexDirection: 'row', paddingVertical: Platform.OS === 'ios' ? 20 : 15, paddingBottom:25 }}>
                            <IconSearchWhite />
                            <Text style={{ color: colors.white, fontFamily: fonts.primary[700], fontSize: 15, marginLeft: 5 }}>Pencarian</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modalize>
        </>
    )
}

const DaftarQiroaty = ({ route }) => {
    const { qiroatys } = route.params;
    return (
        <View style={{flex:1, backgroundColor:colors.white}}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ padding: 10 }}>
                    {
                        qiroatys.length !== 0 ?
                            qiroatys.map((v, i) => {
                                return (
                                    <DaftarQiroatyCard
                                        key={i}
                                        hari={v.Hari === 'Minggu' ? 'Ahad' : v.Hari}
                                        tanggal={v.Tanggal}
                                        guru={v.NamaGuru}
                                        juz={v.Jilid}
                                        halaman={v.Halaman}
                                        kelompok={v.Kelompok}
                                    />
                                )
                            })
                            :
                            <View style={{ flex: 1, alignItems: 'center', alignContent: 'center', justifyContent: 'center', backgroundColor:colors.white,borderTopLeftRadius: 20, borderTopRightRadius:20 }}>
                                <View style={{width: 300, height: 200, marginTop:-100}}>
                                    <Image source={ILNilaiKosong} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 0}} />
                                </View>
                                <Gap height={50}/>
                                <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[400] }}>Yey, tidak ada daftar qiroaty!</Text>
                            </View>
                    }
                </View>
            </ScrollView>
        </View>
    )
}
const TesQiroaty = ({ route }) => {
    const modalDetailHasilRef = useRef(null);
    const [dataDetil, setDataDetil] = useState({
        hasil:'',
        hari:'',
        tanggal:'',
        juz:'',
        nilai: '',
        catatan: ''
    })
    const { qiroatys } = route.params;

    const btnDetail = (hasil, hari, tanggal, juz, nilai, catatan) => {
        modalDetailHasilRef.current.open()
        setDataDetil({
            hasil,
            hari,
            tanggal,
            juz,
            nilai,
            catatan
        });
    }
    return (
        <View style={{flex:1, backgroundColor:colors.white}}>
            {
                qiroatys.length !== 0 ?
                    
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{ padding: 10 }}>

                            {
                                qiroatys.map((v, i) => {
                                    return (
                                        <TesQiroatyCard
                                            hasil={v.Hasil}
                                            hari={v.Hari}
                                            tanggal={v.Tanggal}
                                            juz={v.Jilid}
                                            nilai={v.Nilai}
                                            onPress={() => btnDetail(v.Hasil, v.Hari, v.Tanggal, v.Juz, v.Nilai, v.Catatan)}
                                        />
                                    )
                                })
                            }
                        </View>
                    </ScrollView>
                    :
                    <View style={{ flex: 1, alignItems: 'center', alignContent: 'center', justifyContent: 'center', backgroundColor:colors.white,borderTopLeftRadius: 20, borderTopRightRadius:20 }}>
                        <View style={{width: 300, height: 200, marginTop:-100}}>
                            <Image source={ILNilaiKosong} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 0}} />
                        </View>
                        <Gap height={50}/>
                        <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[400] }}>Yah, belum ada hasil :(</Text>
                    </View>
            }
            <Modalize
                ref={modalDetailHasilRef}
                modalHeight={hp('50%')}
                handlePosition="inside"
                overlayStyle={{backgroundColor:'#908e9247'}}
            >
                <View style={{padding: 10, top: 10, height:hp('50%')}}>
                    <Text style={styles.modal_title}>Detail Hasil</Text>
                    <Gap height={15} />
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom:5, borderBottomWidth:0.2, borderBottomColor:colors.dark2, paddingBottom:5 }}>
                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[500] }}>Hari</Text>
                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[600] }}>{dataDetil.hari}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom:5, borderBottomWidth:0.2, borderBottomColor:colors.dark2, paddingBottom:5 }}>
                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[500] }}>Tanggal</Text>
                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[600] }}>{dataDetil.tanggal}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom:5, borderBottomWidth:0.2, borderBottomColor:colors.dark2, paddingBottom:5 }}>
                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[500] }}>Juz / Jilid</Text>
                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[600] }}>{dataDetil.juz}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom:5, borderBottomWidth:0.2, borderBottomColor:colors.dark2, paddingBottom:5 }}>
                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[500] }}>Hasil</Text>
                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[600] }}>{dataDetil.hasil}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom:5, borderBottomWidth:0.2, borderBottomColor:colors.dark2, paddingBottom:5 }}>
                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[500] }}>Nilai</Text>
                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[600] }}>{dataDetil.nilai}</Text>
                    </View>
                    <Gap height={10} />
                    <View style={styles.cardMenu}>
                        <View style={{padding:10}}>
                            <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[600] }}>Catatan :</Text>
                            <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[400] }}>{dataDetil.catatan}</Text>
                        </View>
                    </View>
                    <View style={{position:'absolute', bottom:0 }}>
                        <TouchableOpacity onPress={() => modalDetailHasilRef.current.close()} style={{ backgroundColor: colors.primary, justifyContent: 'center', alignItems: 'center', width: wp('100%'), flexDirection: 'row', paddingVertical: Platform.OS === 'ios' ? 20 : 15, paddingBottom:25 }}>
                            <IconCloseWhite />
                            <Text style={{ color: colors.white, fontFamily: fonts.primary[700], fontSize: 15, marginLeft: 5 }}>Tutup</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modalize>
        </View>

        
        
    )
}

const TabNavigator = ({ state, descriptors, navigation, position }) => {
    return (
        <View style={{ flexDirection: 'row', borderBottomColor:colors.dark2, justifyContent:'center' }}>
                {state.routes.map((route, index) => {
                    const { options } = descriptors[route.key];
                    const label =
                        options.tabBarLabel !== undefined
                            ? options.tabBarLabel
                            : options.title !== undefined
                                ? options.title
                                : route.name;

                    const isFocused = state.index === index;

                    const onPress = () => {
                        const event = navigation.emit({
                            type: 'tabPress',
                            target: route.key,
                            canPreventDefault: true,
                        });

                        if (!isFocused && !event.defaultPrevented) {
                            navigation.navigate(route.name);
                        }
                    };


                    const onLongPress = () => {
                        navigation.emit({
                            type: 'tabLongPress',
                            target: route.key,
                        });
                    };

                    const inputRange = state.routes.map((_, i) => i);
                    const opacity = Animated.interpolate(position, {
                        inputRange,
                        outputRange: inputRange.map(i => (i === index ? 1 : 0)),
                    });
                    return (
                        <View key={index} style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Gap height={10} />
                            <TouchableOpacity
                                accessibilityRole="button"
                                accessibilityStates={isFocused ? ['selected'] : []}
                                accessibilityLabel={options.tabBarAccessibilityLabel}
                                testID={options.tabBarTestID}
                                onPress={onPress}
                                onLongPress={onLongPress}
                            >
                            
                            {
                                isFocused ? (
                                    <Animated.View opacity={opacity} style={{backgroundColor:colors.white, width:wp('50%'), alignItems:'center', borderBottomColor:colors.primary, borderBottomWidth:1.5, paddingBottom:10, paddingTop:5, borderTopLeftRadius:10, borderTopRightRadius:10}}>
                                        <Text style={{color:colors.primary, fontFamily:fonts.primary[600], fontSize:16}}>{label}</Text>
                                    </Animated.View>
                                ) : (
                                    <View style={{backgroundColor:colors.white, borderColor:colors.dark2, width:wp('50%'),alignItems:'center', borderBottomColor:colors.dark2, borderBottomWidth:1, paddingBottom:10, paddingTop:5, borderTopLeftRadius:10, borderTopRightRadius:10}}>
                                        <Text style={{color:colors.dark1, fontFamily:fonts.primary[600], fontSize:16}}>{label}</Text>
                                    </View>
                                )
                            }

                                </TouchableOpacity>
                        </View>
                    );
                })}
        </View>
    );
}

export default Qiroaty

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor : colors.white,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    avatar: {
        width: 46,
        height: 46,
        borderRadius: 100,
        marginRight: 12,
        backgroundColor: colors.yellow1,
        justifyContent: 'center',
        alignItems:'center'
    },
    cardMenu: {
        backgroundColor: colors.white,
        borderRadius:10,
        elevation:2,
        marginBottom: 10,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2,
    },
    container: {
        flexDirection: 'row',
        padding: 16,
        borderBottomWidth: 0.5,
        borderBottomColor: colors.dark2,
        alignItems: 'center',
    },
    avatar: {
        width: 36,
        height: 36,
        borderRadius: 46/2,
        marginRight: 12,
        backgroundColor: colors.primary,
        alignItems: 'center',
        justifyContent:'center'
    },
    name: {
        fontSize: 16,
        fontFamily: fonts.primary.normal,
        color: colors.black,
    },
    desc: {
        fontSize: 12,
        fontFamily: fonts.primary[300],
        color: colors.text.secondary,
    },
    modal_content: {
        padding: 10,
        top: 10,
        height:hp('90%')
    },
    modal_title: {
        fontSize: 18, 
        color: colors.black, 
        fontFamily: fonts.primary[600]
    },
    modal_com_input: {
        width: '100%', 
        height:40, 
        backgroundColor: colors.white, 
        borderWidth:0.5, 
        borderColor:colors.dark2, 
        borderRadius: 5, 
        justifyContent:'center' 
    },
    modal_com_input_text: {
        fontSize: 13,
        color: colors.black,
        fontFamily: fonts.primary[400],
        padding: 10
    },
})
