import React, {useEffect, useRef} from 'react'
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    StatusBar,
    TextInput,
    Image,
    Platform
} from 'react-native'
import { colors, fonts } from '../../utils'
import {  Gap } from '../../components'
import { IconLogin, ILLogo } from '../../assets'
import { getStatusBarHeight } from 'react-native-status-bar-height';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { useDispatch, useSelector } from 'react-redux'
import { apiMobileLogin, setFormLogin, apiLupaPass } from '../../config/redux/action'
import { FUNCToast } from '../../config/function'
import { Modalize } from 'react-native-modalize';
import OneSignal from 'react-native-onesignal';

const Masuk = ({ navigation }) => {
    const modalLupaPassword = useRef(null);
    const { formLogin } = useSelector(state => state.authReducer);
    const dispatch = useDispatch();
    const {
        Nis,
        Password,
        NisLupaPass
    } = formLogin;
    const btnLogin = () => dispatch(apiMobileLogin(formLogin, navigation))
    const btnLupaPass = () => dispatch(apiLupaPass(formLogin))

    useEffect(() => {
        OneSignal.init('AppId');
        OneSignal.addEventListener('ids', onIds)
        return () => { 
            OneSignal.removeEventListener('ids', onIds)
        }
    }, [])

    const onIds = (device) => dispatch(setFormLogin('TokenNotif', device.userId))
    
    
    return (
        <>
            <StatusBar translucent barStyle="dark-content" backgroundColor="transparent"  />
            <View style={styles.android_safearea}>
                <Gap height={getStatusBarHeight()+20} />
                <View style={{ paddingHorizontal: 20, paddingTop: 15 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <View style={{width: 40, height: 40}}>
                                <Image source={ILLogo} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                            </View> 
                            <Text style={{ fontSize: 22, color: colors.black, fontFamily: fonts.primary[600], left:10 }}>ATTAUFIQ</Text>
                        </View>
                    </View>
                </View>
                {/* <Gap height={25} /> */}
                <View style={{ flex: 1, paddingHorizontal:15, marginTop:30}}>
                    {/* <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[500], left: 5 }}>NIS</Text> */}
                    <View style={{alignItems:'center'}}>
                        <TextInput 
                            style={{width:wp('90%'), borderBottomWidth:1,borderBottomColor: colors.dark2,paddingVertical: 10, fontSize: 16,color: colors.black}} 
                            placeholder={'Masukan NIS'} 
                            placeholderTextColor={colors.default}
                            onChangeText={(e) => dispatch(setFormLogin('Nis', e))}
                            value={Nis}
                        />
                    </View>
                    <Gap height={30} />
                    {/* <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[500], left: 5 }}>Password</Text> */}
                    <View style={{alignItems:'center'}}>
                        <TextInput 
                            style={{ width: wp('90%'), borderBottomWidth: 1, borderBottomColor: colors.dark2, paddingVertical: 10, fontSize: 16, color: colors.black }}
                            placeholder={'Masukan Password'} 
                            placeholderTextColor={colors.default}
                            onChangeText={(e) => dispatch(setFormLogin('Password', e))}
                            value={Password}
                            secureTextEntry={true} 
                        />
                    </View>
                    <Gap height={20} />
                    
                    <View style={{ position: 'absolute', bottom: 0, alignItems: 'center', width:wp('100%') }}>
                        <TouchableOpacity onPress={() => modalLupaPassword.current.open()}>
                            <Text style={{ fontSize: 15, color: colors.dark1, fontFamily: fonts.primary[500], left: 5 }}>Lupa Password ?</Text>
                        </TouchableOpacity>
                        <Gap height={hp('5%')} />
                        <TouchableOpacity onPress={() => btnLogin()} style={{ backgroundColor: colors.primary, justifyContent: 'center', alignItems: 'center', width: '100%', flexDirection: 'row', paddingVertical: Platform.OS === 'ios' ? 20 : 15 }}>
                            <IconLogin />
                            <Text style={{color:colors.white, fontFamily:fonts.primary[600], fontSize:19, marginLeft: 10}}>Masuk</Text>
                        </TouchableOpacity>
                    </View>
                </View>
               
            </View>
            <Modalize
                ref={modalLupaPassword}
                modalHeight={hp('50%')}
                handlePosition="inside"
            >
                <Gap height={10} />
                <View style={{ flex:1,paddingHorizontal: 20, paddingVertical: 20, height: hp('49%') }}>
                    <Text style={{ fontSize: 18, color: colors.black, fontFamily: fonts.primary[600]}}>Lupa Password</Text>
                    <Gap height={10} />
                    <View style={{alignItems:'center'}}>
                        <TextInput 
                            style={{width:wp('90%'), borderBottomWidth:1,borderBottomColor: colors.dark2,paddingVertical: 10, fontSize: 16,color: colors.black}} 
                            placeholder={'Masukan NIS'} 
                            placeholderTextColor={colors.default}
                            onChangeText={(e) => dispatch(setFormLogin('NisLupaPass', e))}
                            value={NisLupaPass}
                        />
                    </View>
                    <View style={{position:'absolute', bottom:0 }}>
                        
                     <TouchableOpacity onPress={() => btnLupaPass()} style={{ backgroundColor: colors.primary, justifyContent: 'center', alignItems: 'center', width: wp('100%'), flexDirection: 'row', paddingVertical: Platform.OS === 'ios' ? 20 : 15 }}>
                        <Text style={{color:colors.white, fontFamily:fonts.primary[600], fontSize:19, marginRight: 10}}>Reset Password</Text>
                        <IconLogin />
                    </TouchableOpacity>
                    </View>
                </View>

            </Modalize>
        </>
    )
}

export default Masuk

const styles = StyleSheet.create({
    android_safearea: {
        flex: 1,
        backgroundColor: colors.white
    },
})
