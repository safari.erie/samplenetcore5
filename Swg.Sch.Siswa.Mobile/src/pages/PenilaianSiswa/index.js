import React, { useEffect, useRef, useState } from 'react';
import { Text, View, TouchableOpacity, StatusBar, StyleSheet, ScrollView,Dimensions, Platform } from 'react-native';
import { colors } from '../../utils/colors';
import { fonts } from '../../utils';
import { IconDateWhite, IconFPaketData, IconHome, IconMore, IconMoreActive, IconRefresh, IconSearchWhite } from '../../assets';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Gap, HeaderCard } from '../../components';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { color } from 'react-native-reanimated';
import { setDataPenilaianSiswas, apiPenilaianDiriSiswaAdd, setDataTahunAjarans, setDataSemester } from '../../config/redux/action';
import { useDispatch, useSelector } from 'react-redux';
import { Modalize } from 'react-native-modalize';
import { FUNCDateToString } from '../../config/function';
import { Picker } from '@react-native-picker/picker';
import DateTimePicker from '@react-native-community/datetimepicker';

const PenilaianSiswa = ({navigation}) => {
    const { dataUser } = useSelector(state => state.authReducer);
    const { dataPenilaianSiswas, dataSemester } = useSelector(state => state.penilaianSiswaReducer)
    const { dataTahunAjarans } = useSelector(state => state.raporReducer);
    const dispatch = useDispatch()
    const modalFilterRef = useRef()
    const [tp, setTp] = useState(0);
    const [semester, setSemester] = useState(1);

    useEffect(() => {
        dispatch(setDataPenilaianSiswas())
        dispatch(setDataTahunAjarans());
        dispatch(setDataSemester());
    }, [dispatch])

    const btnReload = () => {
        dispatch(setDataPenilaianSiswas(tp, semester));
        modalFilterRef.current.close()
    }

    const setRadio = (id, radio) => {
        let newArr = dataPenilaianSiswas.map((item) => {
            if (item.IdJenisPenilaianDiri == id) {
                return { ...item, ["Penilaian"]:  radio};
            } else {
                return item;
            }
        });
        dispatch({ type: 'DATA_PENILAIAN_SISWAS', payload: newArr });
    }

    const btnSimpan = () => {
        dispatch(apiPenilaianDiriSiswaAdd(dataUser.IdSiswa, 2020, 1, dataPenilaianSiswas))
    }

    return (
        <>
            <StatusBar translucent barStyle="light-content" backgroundColor="transparent"  />
            <View style={styles.android_safearea}>
              
                <HeaderCard
                    statusBar={getStatusBarHeight()}
                    icon={<IconDateWhite />}
                    onPress={() => modalFilterRef.current?.open()}
                    title={'Penilaian Siswa'}
                    isBack={true}
                    onPressBack={() => navigation.goBack()}
                />
                
                <View style={styles.content}>
                    <ScrollView>
                        <View style={{ paddingHorizontal: 10, paddingTop: 15 }}>
                            {
                                dataPenilaianSiswas ?
                                    dataPenilaianSiswas.map((v, i) => {
                                        return (
                                            <View key={i} style={styles.card}>
                                                <View style={{ flex:1, borderRadius: 10, padding:10, flexDirection: 'column', justifyContent: 'space-between' }}>
                                                    <View>
                                                        <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[600] }}>{v.IdJenisPenilaianDiri}. {v.JenisPenilaianDiri}</Text>
                                                    </View>
                                                    <View style={{flexDirection: 'row', flexWrap: 'wrap', marginHorizontal: 0, paddingTop:10}}>
                                                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row', width: '100%', flexWrap: 'wrap' }}>
                                                            {
                                                                // v.IdJenisPenilaianDiri == idJenisPenilaianDiri ?
                                                                    <>
                                                                    {
                                                                        v.Penilaian == 1 ?
                                                                            <View style={{width: '10%', alignItems: 'center'}}>  
                                                                                <View style={{ padding:2, borderRadius: 100, alignItems:'center', justifyContent:'center', backgroundColor:colors.primary }}>
                                                                                    <Text style={{ fontSize: 25 }}>😀</Text>
                                                                                </View>
                                                                            </View>
                                                                            :
                                                                            <TouchableOpacity onPress={() => setRadio(v.IdJenisPenilaianDiri, 1)} style={{width: '10%', alignItems: 'center'}}>  
                                                                                <View style={{ padding:2, borderRadius: 100,  borderColor:colors.dark2, alignItems:'center', justifyContent:'center' }}>
                                                                                    <Text style={{ fontSize: 20 }}>😀</Text>
                                                                                </View>
                                                                            </TouchableOpacity>
                                                                    }
                                                                    {
                                                                        v.Penilaian == 2 ?
                                                                            <View style={{width: '10%', alignItems: 'center'}}>  
                                                                                <View style={{ padding:2, borderRadius: 100, alignItems:'center', justifyContent:'center', backgroundColor:colors.primary }}>
                                                                                    <Text style={{ fontSize: 25 }}>😊</Text>
                                                                                </View>
                                                                            </View>
                                                                            :
                                                                            <TouchableOpacity onPress={() => setRadio(v.IdJenisPenilaianDiri, 2)} style={{width: '10%', alignItems: 'center'}}>  
                                                                                <View style={{ padding:2, borderRadius: 100,  borderColor:colors.dark2, alignItems:'center', justifyContent:'center' }}>
                                                                                    <Text style={{ fontSize: 20 }}>😊</Text>
                                                                                </View>
                                                                            </TouchableOpacity>
                                                                    }
                                                                    {
                                                                        v.Penilaian == 3 ?
                                                                            <View style={{width: '10%', alignItems: 'center'}}>  
                                                                                <View style={{ padding:2, borderRadius: 100, alignItems:'center', justifyContent:'center', backgroundColor:colors.primary }}>
                                                                                    <Text style={{ fontSize: 25 }}>😟</Text>
                                                                                </View>
                                                                            </View>
                                                                            :
                                                                            <TouchableOpacity onPress={() => setRadio(v.IdJenisPenilaianDiri, 3)} style={{width: '10%', alignItems: 'center'}}>  
                                                                                <View style={{ padding:2, borderRadius: 100,  borderColor:colors.dark2, alignItems:'center', justifyContent:'center' }}>
                                                                                    <Text style={{ fontSize: 20 }}>😟</Text>
                                                                                </View>
                                                                            </TouchableOpacity>
                                                                    }
                                                                    </>
                                                            }
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                        )
                                    })
                                    : null
                            }
                           

                            <Gap height={15} />
                            <TouchableOpacity onPress={() => btnSimpan()} style={{ height: 40, backgroundColor: colors.primary,  borderRadius: 10, justifyContent: 'center', alignItems: 'center'}}>
                                <Text style={{fontFamily:fonts.primary[700], color:colors.white, fontSize: 16}}>Simpan</Text>
                            </TouchableOpacity>
                            <Gap height={15} />
                        </View>
                        
                    </ScrollView>
                </View>
            </View>
            <Modalize
                ref={modalFilterRef}
                modalHeight={hp('40%')}
                handlePosition="inside"
            >
                <View style={{padding: 10, top: 10, height:hp('40%')}}>
                    <Text style={styles.modal_title}>Pilih Tahun Ajaran</Text>
                    <Gap height={15} />
                    <View style={Platform.OS !== 'ios' ? {borderWidth:0.5, borderColor:colors.dark2, borderRadius:7,  backgroundColor:colors.white} : {marginTop:-30}}>
                        <Picker
                            style={{ width: wp('90%') }}
                            selectedValue={tp}
                            onValueChange={(itemValue) =>
                                setTp(itemValue)
                            }
                            mode="dialog"
                        >
                            {
                                dataTahunAjarans ?
                                    dataTahunAjarans.length !== 0 ?
                                        dataTahunAjarans.map((v, i) => {
                                            return (
                                                <Picker.Item key={i} label={`${v.Nama}`} value={v.IdTahunAjaran} />
                                            )
                                        })
                                        : null
                                    : null
                                
                            }
                        </Picker>
                    </View>
                    <Gap height={20} />
                    <Text style={styles.modal_title}>Pilih Semester</Text>
                    <Gap height={15} />
                    <View style={Platform.OS !== 'ios' ? {borderWidth:0.5, borderColor:colors.dark2, borderRadius:7,  backgroundColor:colors.white} : {marginTop:-30}}>
                        <Picker
                            style={{ width: wp('90%') }}
                            selectedValue={semester}
                            onValueChange={(itemValue) =>
                                setSemester(itemValue)
                            }
                            mode="dialog"
                        >
                            <Picker.Item label={`${dataSemester.Semester}`} value={dataSemester.IdSemester} />
                        </Picker>
                    </View>
                    <Gap height={20} />
                    <View style={{position:'absolute', bottom:0 }}>
                        <TouchableOpacity onPress={() => btnReload()} style={{ backgroundColor: colors.primary, justifyContent: 'center', alignItems: 'center', width: wp('100%'), flexDirection: 'row', paddingVertical: Platform.OS === 'ios' ? 20 : 15, paddingBottom:25 }}>
                            <IconSearchWhite />
                            <Text style={{ color: colors.white, fontFamily: fonts.primary[700], fontSize: 15, left: 5 }}>Pencarian</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modalize>
        </>
    )
}

export default PenilaianSiswa

const styles = StyleSheet.create({
    android_safearea: {
        flex: 1,
        backgroundColor: colors.primary
    },
    content: {
        flex: 1,
        backgroundColor : '#F6F7FC',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    card: {
        backgroundColor: colors.white,
        borderRadius: 10,
        elevation: 2,
        paddingBottom: 10,
        marginBottom: 10,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2,
    },
    modal_content: {
        padding: 10,
        top: 10,
        height:hp('90%')
    },
    modal_title: {
        fontSize: 18, 
        color: colors.black, 
        fontFamily: fonts.primary[600]
    },
    modal_com_input: {
        width: '100%', 
        height:50, 
        backgroundColor: colors.white, 
        borderWidth:0.5, 
        borderColor:colors.dark2, 
        borderRadius: 5, 
        justifyContent:'center' 
    },
    modal_com_input_text: {
        fontSize: 15,
        color: colors.black,
        fontFamily: fonts.primary[400],
        // padding: 10
        paddingHorizontal:15
    },
})
