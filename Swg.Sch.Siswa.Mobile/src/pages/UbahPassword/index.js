import * as React from 'react';
import { Text, View, TouchableOpacity, StatusBar, StyleSheet, ScrollView,Dimensions, TextInput } from 'react-native';
import { colors } from '../../utils/colors';
import { fonts } from '../../utils';
import { IconFPaketData, IconHome, IconMore, IconMoreActive, IconRefresh, IconUploadWhite } from '../../assets';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Gap, HeaderCard } from '../../components';
import { apiGantiPassword, setFormUbahPassword } from '../../config/redux/action';
import { useDispatch, useSelector } from 'react-redux';

const UbahPassword = ({ navigation }) => {
    const { formUbahPassword } = useSelector(state => state.ubahPasswordReducer);
    const dispatch = useDispatch();

    const {
        OldPassword,
        NewPassword1,
        NewPassword2
    } = formUbahPassword;

    const btnUbahPassword = () => dispatch(apiGantiPassword(formUbahPassword, navigation));
    return (
        <>
            <StatusBar translucent barStyle="light-content" backgroundColor="transparent"  />
            <View style={styles.android_safearea}>
              
                <HeaderCard
                    statusBar={getStatusBarHeight()}
                    title={'Ubah Password'}
                    isBack={true}
                    onPressBack={() => navigation.goBack()}
                />
                
                <View style={styles.content}>
                    <ScrollView>
                        <View style={{ flex: 1, backgroundColor: colors.white, borderRadius: 20 }}>
                            <View style={{paddingHorizontal:15, marginTop:20}}>

                                <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[500]}}>Password Lama</Text>
                                <View style={{alignItems:'center'}}>
                                    <TextInput
                                        style={{width:'100%', borderBottomWidth:1,borderBottomColor: colors.dark2,paddingVertical: 10, fontSize: 16,color: colors.black}} 
                                        placeholder={'Masukan Password Lama'} 
                                        placeholderTextColor={colors.default}
                                        value={OldPassword}
                                        onChangeText={(e) => dispatch(setFormUbahPassword('OldPassword', e))}
                                    />
                                </View>

                                <Gap height={20} />

                                <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[500]}}>Password Baru</Text>
                                <View style={{alignItems:'center'}}>
                                    <TextInput 
                                        style={{width:'100%', borderBottomWidth:1,borderBottomColor: colors.dark2,paddingVertical: 10, fontSize: 16,color: colors.black}} 
                                        placeholder={'Masukan Password Baru'} 
                                        placeholderTextColor={colors.default}
                                        value={NewPassword1}
                                        onChangeText={(e) => dispatch(setFormUbahPassword('NewPassword1', e))}
                                    />
                                </View>
                                
                                <Gap height={20} />

                                <Text style={{ fontSize: 15, color: colors.black, fontFamily: fonts.primary[500]}}>Konfirmasi Password Baru</Text>
                                <View style={{alignItems:'center'}}>
                                    <TextInput 
                                        style={{width:'100%', borderBottomWidth:1,borderBottomColor: colors.dark2,paddingVertical: 10, fontSize: 16,color: colors.black}} 
                                        placeholder={'Masukan Konfirmasi Password Baru'} 
                                        placeholderTextColor={colors.default}
                                        value={NewPassword2}
                                        onChangeText={(e) => dispatch(setFormUbahPassword('NewPassword2', e))}
                                    />
                                </View>

                                <Gap height={40} />

                                <TouchableOpacity onPress={() => btnUbahPassword()} style={styles.btn_primary}>
                                    <IconUploadWhite />
                                    <Text style={styles.btn_primary_text}>Perbarui Password</Text>
                                </TouchableOpacity>
                                <Gap height={20} />
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </View>
        </>
    )
}

export default UbahPassword

const styles = StyleSheet.create({
    android_safearea: {
        flex: 1,
        backgroundColor: colors.primary
    },
    content: {
        flex: 1,
        backgroundColor : colors.white,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    btn_primary: {
        height: 40, 
        backgroundColor: colors.primary, 
        borderRadius: 7, 
        justifyContent: 'center', 
        alignItems: 'center',
        flexDirection:'row'
    },
    btn_primary_text: {
        fontFamily: fonts.primary[700],
        color: colors.white,
        fontSize: 15,
        marginLeft: 5
    },
})
