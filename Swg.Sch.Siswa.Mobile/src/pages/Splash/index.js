import React, {useState, useEffect} from 'react'
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    StatusBar,
    TextInput,
    Image,
    Platform
} from 'react-native'
import { colors, fonts } from '../../utils'
import {  Gap } from '../../components'
import {  ILLogo, LoadingSplash } from '../../assets'
import { getStatusBarHeight } from 'react-native-status-bar-height';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { apiGetInfoUser } from '../../config/redux/action'
import { FUNCToast } from '../../config/function'
import { useDispatch } from 'react-redux'
import { VERSIAPP } from '../../config/helpers'

const Splash = ({ navigation }) => {
    const dispatch = useDispatch();
    useEffect(() => {
        setTimeout(() => {
            getToken()
        }, 3000);
    }, [])
    const getToken = async () => {
        try {
            const token = await AsyncStorage.getItem('@token');
            if (token === null) navigation.replace('Masuk');
            if (token !== null) dispatch(apiGetInfoUser(navigation));
        } catch(e) {
            FUNCToast('FAIL', { msg: e });
        }
    }
    return (
        <>
            <StatusBar translucent barStyle="light-content" backgroundColor="transparent"  />
            <View style={styles.android_safearea}>
                <Gap height={getStatusBarHeight()} />
                <View style={{flex:1, justifyContent:'center', backgroundColor:colors.primary, alignItems:'center'}}>
                    <View style={{width: 100, height: 100}}>
                        <Image source={ILLogo} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                    </View>
                    <Gap height={25} />
                    <Text style={{ fontSize: 20, color: colors.white, fontFamily: fonts.primary[800], left: 5 }}>ATTAUFIQ</Text>
                    <Gap height={40} />
                    <Image source={LoadingSplash} style={styles.splashgif} />
                    <View style={{position:'absolute', bottom:0, alignItems:'center'}}>
                        <Text style={{ fontSize: 15, color: colors.white, fontFamily: fonts.primary[500] }}>#ATTAUFIQ {VERSIAPP}</Text>
                        <Gap height={20} />
                    </View>
                </View>
            </View>
        </>
    )
}

export default Splash

const styles = StyleSheet.create({
    android_safearea: {
        flex: 1,
        backgroundColor: colors.primary
    },
    splashgif: {
        width: 50,
        height: 50
    }
})
