import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { colors } from '../../../utils';

const Input = ({placeholder, ...rest}) => {
    return (
        <TextInput 
            style={styles.input} 
            placeholder={placeholder} 
            placeholderTextColor={colors.default}
            {...rest}
        />
    )
}

export default Input;

const styles = StyleSheet.create({
    input: {
        borderBottomWidth:1,
        borderBottomColor: colors.dark2,
        // borderColor: colors.white,
        // borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 5,
        fontSize: 14,
        color: colors.black
    }
})