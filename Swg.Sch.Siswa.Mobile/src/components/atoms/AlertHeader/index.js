import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { colors, fonts } from '../../../utils';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { IconInfoResiAuto } from '../../../assets';

const AlertHeader = ({tipe, desc}) => {
    return (
        <View style={styles.notice_card}>
            <View style={styles.notice_icon}>
                <IconInfoResiAuto />
            </View>
            <View style={styles.notice_desc}>
                <Text style={styles.notice_desc_font}>{desc}</Text>
            </View>
        </View>
    )
}

export default AlertHeader;

const styles = StyleSheet.create({
    notice_card: {
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#F2DB73',
        paddingVertical: 10
    },
    notice_icon: {
        width: wp('20%'),
        alignItems: 'center'
    },
    notice_desc: {
        // maxWidth: wp('90%'),
        // paddingRight: 10
    },
    notice_desc_font: {
        fontSize:13,
        fontFamily:fonts.primary[400],
        color: colors.black,
        maxWidth:wp('70%')
    },
})