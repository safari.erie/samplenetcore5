import React from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { colors } from '../../../utils'

const Avatar = ({source, size}) => {
    return (
        <View style={styles.avatar_content(size)}>
            <View style={styles.avatar_body(size)}>
                <Image source={source} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
            </View> 
        </View> 
    )
}

export default Avatar

const styles = StyleSheet.create({
    avatar_content: (size) => ({
        width: size+6,
        height: size+6,
        borderRadius: 100,
        marginRight: 12,
        backgroundColor: colors.yellow1,
        justifyContent: 'center',
        alignItems:'center'
    }),
    avatar_body: (size) => ({
        width: size,
        height: size,
        borderRadius: 100,
        backgroundColor: 'pink'
    }),
})
