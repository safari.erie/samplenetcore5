import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import { IconBack, ILBg1, ILBg3 } from '../../../assets';
import { colors, fonts } from '../../../utils';
import Gap from '../Gap'

const HeaderCard = ({statusBar, title, onPress, icon, titleColor, isBack, onPressBack}) => {
    return (
        <>
            <ImageBackground source={ILBg3} style={{ width: '100%' }}>
                <Gap height={statusBar} />
                <View style={styles.header_card}>
                    {
                        isBack ?
                            <TouchableOpacity onPress={onPressBack} style={styles.button_back}>
                                <IconBack />
                                <Text style={styles.title_text}>{title}</Text>
                            </TouchableOpacity>
                            :
                            <View>
                                <Text numberOfLines={1} style={styles.header_title(titleColor)}>{title}</Text>
                            </View>
                    }
                    <TouchableOpacity onPress={onPress}>
                        {icon}
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        </>
    )
}

export default HeaderCard;

const styles = StyleSheet.create({
    header_card: {
        flexDirection: 'row',
        paddingVertical: 18,
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    header_title: (titleColor) => ({
        fontSize: 18, 
        color: titleColor || colors.white, 
        fontFamily: fonts.primary[700]
    }),
    button_back: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    title_text: {
        fontSize: 18, 
        color: colors.white, 
        fontFamily: fonts.primary[700], 
        left: 10
    }
})