import React from 'react'
import {
    StyleSheet, 
    Text, 
    TouchableOpacity, 
    View,
    ImageBackground
} from 'react-native'
import { 
    IconHomeActive,
    IconHomeNonActive,
    IconTagihanActive,
    IconTagihanNonActive,
    IconNotifActive,
    IconNotifNonActive,
    IconSettingActive,
    IconSettingNonActive,

    IconRuangBelajar,
    IconRuangBelajar1,
    ILBg1,
  
} from '../../../assets'
import { colors, fonts } from '../../../utils'

const TabItem = ({ title, active, onPress, onLongPress }) => {
    const Icon = () => {
        if (title === 'Home') {
            return active ? <IconHomeActive /> : <IconHomeNonActive />;
        }
        if (title === 'Tagihan') {
            return active ? <IconTagihanActive /> : <IconTagihanNonActive />;
        }
        if (title === 'Ruang Belajar') {
            return active ? <IconRuangBelajar1 /> : <IconRuangBelajar />;
        }
        if (title === 'Inbox') {
            return active ? <IconNotifActive /> : <IconNotifNonActive />;
        }
        if (title === 'User') {
            return active ? <IconSettingActive /> : <IconSettingNonActive />;
        }
        
        return <IconRuangBelajar />;
    }
    return (
        
        <>
            {title == 'Ruang Belajar' ? 
                <>
                    <View style={{alignItems: 'center', width: '20%'}}>
                        <TouchableOpacity onPress={onPress} style={{alignItems: 'center', width:60, height:60, backgroundColor:'white', borderRadius:100, paddingTop:5}}>
                            <ImageBackground source={ILBg1} imageStyle={{borderRadius:100}} style={{width:50,height:50}}>
                                {/* <TouchableOpacity style={{ backgroundColor: colors.primary, borderRadius:100, borderColor:'transparent',borderWidth:4}} onPress={onPress} onLongPress={onLongPress}> */}
                                    <View style={{alignItems:'center',  height:50, width:50,justifyContent:'center'}}>
                                        <Icon />
                                    </View>
                                {/* </TouchableOpacity> */}
                            </ImageBackground>
                        </TouchableOpacity>
                        <Text style={{fontSize:11,  fontFamily: fonts.primary[800], color:colors.dark2}}>Belajar</Text>
                    </View>

                </>
            :
                <TouchableOpacity style={styles.container} onPress={onPress} onLongPress={onLongPress}>
                    <Icon />
                    <Text style={styles.text(active)}>{title}</Text>
                </TouchableOpacity>
            }
        </>
    )
}

export default TabItem

const styles = StyleSheet.create({
    container: {
        width: '15%',
        alignItems: 'center',
    },
    text: (active) => ({
        fontSize : 11,
        color: active ? colors.text.menuActive : colors.text.menuInactive,
        fontFamily: fonts.primary[800],
        marginTop: 2
    }),
})
