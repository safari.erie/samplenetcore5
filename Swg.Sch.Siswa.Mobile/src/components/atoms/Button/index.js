import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import { colors, fonts } from '../../../utils'

const Button = ({type, title, onPress, icon}) => {
    return (
        <TouchableOpacity style={styles.container(type)} onPress={onPress}>
            <Text style={styles.text(type)}>{title}</Text>
        </TouchableOpacity>
    )
}

export default Button

const styles = StyleSheet.create({
    container: (type) => ({
        backgroundColor: type === 'secondary' ? colors.secondary : colors.primary,
        paddingVertical: 10,
        borderRadius: 10,
        // borderWidth: 1,
        // borderColor: type === 'secondary' ? colors.yellow1 : colors.white,
    }),
    text: (type) => ({
        fontSize: 18,
        fontFamily: fonts.primary[600],
        textAlign: 'center',
        color: type === 'secondary' ? colors.button.secondary.text : colors.button.primary.text,
    })
})
