import * as React from 'react';
import { Text, View, TouchableOpacity, ScrollView } from 'react-native';
import Animated from 'react-native-reanimated';
import { colors, fonts } from '../../../utils';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Gap } from '../../atoms';

const TabNavigator = ({ state, descriptors, navigation, position }) => {
    return (
        <View style={{ flexDirection: 'row',  borderBottomWidth: 0.3, borderBottomColor:colors.dark2 }}>
            <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{}}>
                {state.routes.map((route, index) => {
                    const { options } = descriptors[route.key];
                    const label =
                        options.tabBarLabel !== undefined
                            ? options.tabBarLabel
                            : options.title !== undefined
                                ? options.title
                                : route.name;

                    const isFocused = state.index === index;

                    const onPress = () => {
                        const event = navigation.emit({
                            type: 'tabPress',
                            target: route.key,
                            canPreventDefault: true,
                        });

                        if (!isFocused && !event.defaultPrevented) {
                            navigation.navigate(route.name);
                        }
                    };


                    const onLongPress = () => {
                        navigation.emit({
                            type: 'tabLongPress',
                            target: route.key,
                        });
                    };

                    const inputRange = state.routes.map((_, i) => i);
                    const opacity = Animated.interpolate(position, {
                        inputRange,
                        outputRange: inputRange.map(i => (i === index ? 1 : 0)),
                    });
                    return (
                        <View key={index} style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Gap height={10} />
                            <TouchableOpacity
                                accessibilityRole="button"
                                accessibilityStates={isFocused ? ['selected'] : []}
                                accessibilityLabel={options.tabBarAccessibilityLabel}
                                testID={options.tabBarTestID}
                                onPress={onPress}
                                onLongPress={onLongPress}
                            >
                            
                            {
                                isFocused ? (
                                    <Animated.View opacity={opacity} style={{backgroundColor:colors.primary, marginHorizontal:5, paddingVertical:5, paddingHorizontal:10, borderRadius:20}}>
                                        <Text style={{color:colors.white, fontFamily:fonts.primary[800], fontSize:13}}>{label}</Text>
                                    </Animated.View>
                                ) : (
                                    <View style={{backgroundColor:colors.dark2, borderWidth:1, borderColor:colors.dark2, marginHorizontal:5, paddingVertical:5, paddingHorizontal:10, borderRadius:20}}>
                                        <Text style={{color:colors.white, fontFamily:fonts.primary[800], fontSize:13}}>{label}</Text>
                                    </View>
                                )
                                }
                                </TouchableOpacity>
                            <Gap height={10} />
                        </View>
                    );
                })}
            </ScrollView>
        </View>
    );
}

export default TabNavigator

// const styles = StyleSheet.create({

// })
