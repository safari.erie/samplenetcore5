import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { colors, fonts } from '../../../utils';
import { IconCheckColor, IconRightColor, IconTimesColor } from '../../../assets';
import { Gap } from '../../atoms';

const TesQiroatyCard = ({hasil, hari, tanggal, juz, nilai, onPress}) => {
    return (
        <View style={styles.cardMenu}>
            <View style={{ flexDirection: 'row' }}>
                <View style={{backgroundColor:colors.secondary, borderTopLeftRadius:10, borderBottomLeftRadius:10, justifyContent:'center'}}>
                    <View style={{ paddingHorizontal: 10 }}>
                        {
                            hasil === 'Tuntas' ? <IconCheckColor /> : <IconTimesColor />
                        }
                    </View>
                </View>
                <View style={{ flex: 1, flexDirection:'row', paddingVertical: 10, paddingHorizontal:10 }}>
                    <View style={{flex:1}}>
                        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                            <Text style={{ fontSize: 18, color: colors.black, fontFamily: fonts.primary[600]}}>{hari}</Text>
                            <Text style={{ fontSize: 16, color: colors.dark2, fontFamily: fonts.primary[400] }}>{tanggal}</Text>
                        </View>
                        <Gap height={10} />
                        <Text style={{ fontSize: 16, color: colors.dark1, fontFamily: fonts.primary[500]}}>Juz / Jilid : {juz || '-'}</Text>
                        <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                            <Text style={{ fontSize: 16, color: colors.dark1, fontFamily: fonts.primary[500] }}>Nilai : {nilai || '-'}</Text>
                            <TouchableOpacity onPress={onPress} style={{flexDirection:'row', alignItems:'center'}}>
                                <Text style={{ fontSize: 13, color: colors.primary, fontFamily: fonts.primary[500] }}>Lihat detail </Text>
                                <IconRightColor />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    )
    
}

export default TesQiroatyCard;

const styles = StyleSheet.create({
    cardMenu: {
        backgroundColor: colors.white,
        borderRadius:10,
        elevation:2,
        marginBottom: 10,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2,
    },
    splashgif: {
        width: 50,
        height: 50
    },
})