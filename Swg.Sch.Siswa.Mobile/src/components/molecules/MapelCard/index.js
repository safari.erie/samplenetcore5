import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { colors,fonts } from '../../../utils';
import UserAvatar from 'react-native-user-avatar';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Gap } from '../../atoms';
import { IconCoRight } from '../../../assets';

const MapelCard = ({namaMapel, hari, judulMateri, guru, jamMulai, jamSelesai, onPress}) => {
    return (
        <View style={styles.daftar_mapel_card}>
            <View style={styles.daftar_mapel_header}>
                <Text style={styles.daftar_mapel_header_mapel_font}>
                    Mapel : {namaMapel}
                </Text>
                <Text style={styles.daftar_mapel_header_hari_font}>
                    {hari}
                </Text>
            </View>
            <View style={styles.daftar_mapel_body}>
                <View style={styles.daftar_mapel_img_card}>
                    <UserAvatar size={100} name={namaMapel} borderRadius={0} bgColor="#f27c56" style={styles.daftar_mapel_image}/>
                </View>
                <View style={styles.daftar_mapel_desc_card}>
                    <Gap height={3} />
                    <Text style={styles.daftar_mapel_desc_materi_font} numberOfLines={1}>Materi : {judulMateri || 'Tidak ada materi'}</Text>
                    <Gap height={3} />
                    <Text style={styles.daftar_mapel_desc_materi_rinci_font} numberOfLines={1}>Guru : {guru}</Text>
                    <Gap height={3} />
                    <Text style={styles.daftar_mapel_desc_materi_rinci_font} numberOfLines={1}>Waktu : {jamMulai} s/d {jamSelesai}</Text>
                    <Gap height={3} />
                    <View style={styles.daftar_mapel_desc_card_action_button}>
                        <TouchableOpacity onPress={onPress} style={styles.btn_ruang_belajar}>
                            <Text style={styles.btn_ruang_belajar_text}>Masuk Ruang Belajar</Text>
                            <IconCoRight />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default MapelCard;

const styles = StyleSheet.create({
    daftar_mapel_card: {
        backgroundColor: colors.white,
        borderRadius: 10,
        elevation: 2,
        paddingBottom: 10,
        marginBottom: 10,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2,
    },
    daftar_mapel_header: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 5,
        justifyContent: 'space-between'
    },
    daftar_mapel_header_mapel_font: {
        fontSize: 12,
        fontFamily: fonts.primary[600],
    },
    daftar_mapel_header_hari_font: {
        fontSize: 11,
        fontFamily: fonts.primary[400],
    },
    daftar_mapel_body: {
        padding: 10,
        flexDirection: 'row',
        alignItems:'center',
        borderTopWidth: 0.3,
        borderTopColor: colors.dark2
    },
    daftar_mapel_img_card: {
        width: wp('20%'),
        height: hp('10%'),
        backgroundColor: '#f27c56'
    },
    daftar_mapel_image: {
        width: undefined,
        height: undefined,
        flex: 1,
        resizeMode: 'contain'
    },
    daftar_mapel_desc_card: {
        marginHorizontal: 10,
        minWidth: wp('65%'),
    },
    daftar_mapel_desc_materi_font: {
        maxWidth: wp('65%'),
        fontSize: 14,
        fontFamily: fonts.primary[600]
    },
    daftar_mapel_desc_materi_rinci_font: {
        maxWidth: wp('60%'),
        fontSize: 11,
        fontFamily: fonts.primary[400]
    },
    btn_ruang_belajar: {
        flexDirection: 'row',
        backgroundColor: colors.white, 
        borderColor: colors.yellow1, 
        borderWidth: 0.9, 
        paddingVertical: 5, 
        paddingHorizontal: 10, 
        borderRadius: 10, 
        alignItems: 'center', 
    },
    btn_ruang_belajar_text: {
        color: colors.yellow1,
        fontFamily: fonts.primary[600],
        fontSize: 13,
        paddingHorizontal:5
    },
    daftar_mapel_desc_card_action_button: {
        flexDirection: 'row',
        alignItems: 'center'
    },
})