import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import { colors, fonts } from '../../../utils';
import {ILNullPhoto } from '../../../assets';

const DaftarQiroatyCard = ({hari, tanggal, juz, halaman, guru, kelompok}) => {
    return (
        <View style={styles.cardMenu}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 5 }}>
            <View style={{ paddingHorizontal: 20,   marginBottom: -5, backgroundColor: colors.primary, borderBottomRightRadius: 20, borderTopLeftRadius:10 }}>
                    <Text style={{ fontSize: 18, color: colors.white, fontFamily: fonts.primary[600], paddingVertical: 5 }}>{hari || '-'}</Text>
                </View>
                <View style={{paddingHorizontal:10, paddingTop:10, marginBottom:-10}}>
                    <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[400] }}>Juz / Jilid : {juz || '-'}</Text>
                </View>
            </View>
            <View style={{padding:10}}>
                <View style={{flexDirection:'row', justifyContent:'space-between', paddingBottom:15, borderBottomColor:colors.dark2, borderBottomWidth:0.3}}>
                    <Text style={{ fontSize: 16, color: colors.dark1, fontFamily: fonts.primary[400] }}>{tanggal}</Text>
                    <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[400] }}>Halaman : {halaman || '-'}</Text>
                </View>
                <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', paddingTop:10}}>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <View style={styles.avatar}>
                            <View style={{width: 30, height: 30, borderRadius: 100, backgroundColor: 'pink'}}>
                                <Image source={ILNullPhoto} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
                            </View> 
                        </View>
                        
                        <Text style={{ fontSize: 13, color: colors.black, fontFamily: fonts.primary[400] }}>{guru || '-'}</Text>
                    </View> 
                    <Text style={{ fontSize: 13, color: colors.black, fontFamily: fonts.primary[400]}}>Kel. {kelompok || '-'}.</Text>
                </View>
            </View>
        </View>
        
    )
    
}

export default DaftarQiroatyCard;

const styles = StyleSheet.create({
    cardMenu: {
        backgroundColor: colors.white,
        borderRadius:10,
        elevation:2,
        marginBottom: 10,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2,
    },
    avatar: {
        width: 36,
        height: 36,
        borderRadius: 100,
        marginRight: 12,
        backgroundColor: colors.yellow1,
        justifyContent: 'center',
        alignItems:'center'
    },
    splashgif: {
        width: 50,
        height: 50
    },
})