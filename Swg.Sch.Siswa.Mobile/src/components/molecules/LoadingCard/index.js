import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import { colors,fonts } from '../../../utils';
import UserAvatar from 'react-native-user-avatar';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Gap } from '../../atoms';
import { IconCoRight, ILLoading, Loading } from '../../../assets';

const LoadingCard = () => {
    return (
        <View style={{ flex: 1, alignItems: 'center', alignContent: 'center', justifyContent: 'center', backgroundColor:colors.white,borderTopLeftRadius: 20, borderTopRightRadius:20 }}>
            <View style={{width: 200, height: 200, marginTop:-100}}>
                <Image source={ILLoading} style={{width: undefined, height: undefined, resizeMode: 'cover', flex: 1, borderRadius: 100}} />
            </View>
            <Gap height={50}/>
            <Text style={{ fontSize: 16, color: colors.black, fontFamily: fonts.primary[400] }}>Sedang Memuat!</Text>
            <Image source={Loading} style={styles.splashgif} />
        </View>
    )
    
}

export default LoadingCard;

const styles = StyleSheet.create({
    splashgif: {
        width: 50,
        height: 50
    },
})