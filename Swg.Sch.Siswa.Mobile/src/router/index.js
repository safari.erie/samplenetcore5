import React from 'react';
import { CardStyleInterpolators, createStackNavigator } from '@react-navigation/stack';
import {
    Splash,
    Welcome,
    Home,
    Inbox,
    User,
    Masuk,
    DaftarMapel,
    RuangBelajar,
    Materi,
    DetilVidio,
    DetilGambar,
    DetilPdf,
    Pin,
    Tagihan,
    Qiroaty,
    Mutabaah,
    Rapor,
    Profil,
    UbahPin,
    UbahPassword,
    Version,
    PenilaianSiswa
} from '../pages';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { BottomNavigator } from '../components';
import { BackHandler } from 'react-native';
import { FUNCBackHandleClose } from '../config/function';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
    return (
        <Tab.Navigator tabBar={props => <BottomNavigator {...props} />}>
            <Tab.Screen
                name="Home"
                component={Home}
                listeners={{
                    focus: () => BackHandler.addEventListener('hardwareBackPress', FUNCBackHandleClose),
                    blur: () => BackHandler.removeEventListener('hardwareBackPress', FUNCBackHandleClose)
                }}
            />
            <Tab.Screen
                name="Tagihan"
                initialParams={{ pinFor: 'tagihan' }}
                component={Pin}
            />
            <Tab.Screen
                name="Ruang Belajar"
                component={DaftarMapel}
                listeners={{
                    focus: () => BackHandler.addEventListener('hardwareBackPress', FUNCBackHandleClose),
                    blur: () => BackHandler.removeEventListener('hardwareBackPress', FUNCBackHandleClose)
                }}
            />
            <Tab.Screen
                name="Inbox"
                component={Inbox}
                listeners={{
                    focus: () => BackHandler.addEventListener('hardwareBackPress', FUNCBackHandleClose),
                    blur: () => BackHandler.removeEventListener('hardwareBackPress', FUNCBackHandleClose)
                }}
            />
            <Tab.Screen
                name="User"
                component={User}
                listeners={{
                    focus: () => BackHandler.addEventListener('hardwareBackPress', FUNCBackHandleClose),
                    blur: () => BackHandler.removeEventListener('hardwareBackPress', FUNCBackHandleClose)
                }}
            />
        </Tab.Navigator>
    )
}

const config = {
    animation: 'spring',
    config: {
        stiffness: 1000,
        damping: 500,
        mass: 3,
        overshootClamping: true,
        restDisplacementThreshold: 0.01,
        restSpeedThreshold: 0.01,
    },
};

const Router = () => {
    return (
        <Stack.Navigator initialRouteName="Splash"
            screenOptions={{
                // gestureEnabled: false,
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                transitionSpec: {
                    open: config,
                    close: config,
                },
            }}
        >
            <Stack.Screen
                name="Splash"
                component={Splash}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="Welcome"
                component={Welcome}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="Home"
                component={Home}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="Masuk"
                component={Masuk}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="MainApp"
                component={MainApp}
                options={{ headerShown: false }} />
            
            <Stack.Screen
                name="DaftarMapel"
                component={DaftarMapel}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="RuangBelajar"
                component={RuangBelajar}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="Materi"
                component={Materi}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="DetilVidio"
                component={DetilVidio}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="DetilGambar"
                component={DetilGambar}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="DetilPdf"
                component={DetilPdf}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="PinTagihan"
                component={Pin}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="Tagihans"
                component={Tagihan}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="Qiroaty"
                component={Qiroaty}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="Mutabaah"
                component={Mutabaah}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="Rapor"
                component={Rapor}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="PinProfil"
                component={Pin}
                initialParams={{pinFor: 'profil'}}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="PinChange"
                component={Pin}
                initialParams={{pinFor: 'ubah pin'}}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="PinPassword"
                component={Pin}
                initialParams={{pinFor: 'ubah password'}}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="PinRapor"
                component={Pin}
                initialParams={{pinFor: 'rapor'}}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="Profil"
                component={Profil}
                initialParams={{pinFor: 'profil'}}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="UbahPin"
                component={UbahPin}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="UbahPassword"
                component={UbahPassword}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="Version"
                component={Version}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="PenilaianSiswa"
                component={PenilaianSiswa}
                options={{ headerShown: false }} />
                
        </Stack.Navigator>
    )
}

export default Router;