
import IconMessages from './ic-messages.svg';
import IconMessagesActive from './ic-messages-active.svg';
import IconInbox from './ic-inbox.svg';
import IconInboxActive from './ic-inbox-active.svg';
import IconUser from './ic-user.svg';
import IconUserActive from './ic-user-active.svg';

import IconMore from './ic-more.svg';
import IconMoreActive from './ic-more-active.svg';
import IconScan from './ic-scan.svg';
import IconTopup from './ic-topup.svg';
import IconRefresh from './ic-refresh.svg';
import IconBack from './ic-back.svg';
import IconRight from './ic-right.svg';
import IconCheck from './ic-check.svg';
import IconMasuk from './ic-masuk.svg';
import IconDaftar from './ic-daftar.svg';
import IconLupaPassword from './ic-lupa-pass.svg';
import IconUbahProfile from './ic-ubah-profile.svg';
import IconBantuan from './ic-bantuan.svg';
import IconAffilate from './ic-affilate.svg';
import IconSnK from './ic-snk.svg';
import IconPanduan from './ic-panduan.svg';
import IconPromo from './ic-promo.svg';
import IconScanQr from './ic-scan-qr.svg';
import IconTransfer from './ic-transfer.svg';
import IconKupon from './ic-kupon.svg';
import IconNotifikasi from './ic-notif.svg';
import IconWithdraw from './ic-withdraw.svg';
import IconLainnya from './ic-lainnya.svg';
import IconQrCode from './ic-qr-code.svg';
import IconBronze from './ic-bronze.svg';

import IconFPulsa from './ic-f-pulsa.svg';
import IconFPaketData from './ic-f-paketdata.svg';

//ICON MENU MAIN APP
import IconHome from './ic-home.svg';
import IconTribe from './ic-tribe.svg';
import IconTribeActive from './ic-tribe-active.svg';
import IconProdukActive from './ic-produk-active.svg';
import IconProduk from './ic-produk.svg';
// END

//ICON MIX NEW
import IconKeranjang from './ic-keranjang.svg';
import IconKeranjangAdd from './ic-keranjang-add.svg';
import IconSearch from './ic-search.svg';
import IconChatProduk from './ic-chat-produk.svg';
import IconHeart from './ic-heart.svg';
import IconShare from './ic-share.svg';
import IconCoin from './ic-coin.svg';
import IconLokasiToko from './ic-lokasi-toko.svg';
import IconDelete from './ic-delete.svg';
import IconPlus from './ic-plus.svg';
import IconMin from './ic-min.svg';
import IconInfoResiAuto from './ic-info-resiauto.svg';
import IconCoResiAuto from './ic-co-resiauto.svg';
import IconCoRight from './ic-co-right.svg';
import IconCoAlamat from './ic-co-alamat.svg';
import IconCoPesanan from './ic-co-pesanan.svg';
import IconCoLokasiToko from './ic-co-lokasitoko.svg';
import IconCoRincian from './ic-co-rincian.svg';
import IconBackYellow from './ic-back-yellow.svg';
import IconBackDark from './ic-back-dark.svg';
import IconRefreshDark from './ic-refresh-dark.svg';


// END

import IconMenuKeranjang from './ic-dp-keranjang.svg';
import IconMenuPembayaran from './ic-dp-pembayaran.svg';
import IconMenuProduk from './ic-dp-produk.svg';
import IconMenuPoin from './ic-dp-poin.svg';
import IconMenuLearning from './ic-dp-learning.svg';
import IconMenuTutorial from './ic-dp-tutorial.svg';
import IconMenuIklan from './ic-dp-iklan.svg';
import IconMenuPembelian from './ic-dp-pembelian.svg';

import IconStar from './ic-dp-star.svg';
import IconPlayVidio from './ic-dp-playvidio.svg';


///NEWWWWWWWWWW

import IconHomeActive from './ic-atq-home-active.svg';
import IconHomeNonActive from './ic-atq-home-nonactive.svg';
import IconTagihanActive from './ic-atq-tagihan-active.svg';
import IconTagihanNonActive from './ic-atq-tagihan-nonactive.svg';
import IconNotifActive from './ic-atq-notif-active.svg';
import IconNotifNonActive from './ic-atq-notif-nonactive.svg';
import IconSettingActive from './ic-atq-setting-active.svg';
import IconSettingNonActive from './ic-atq-setting-nonactive.svg';

import IconRuangBelajar from './ic-atq-ruangbelajar.svg';
import IconRuangBelajar1 from './ic-atq-ruangbelajar1.svg';
import IconDateWhite from './ic-atq-date-white.svg';
import IconNotifBlack from './ic-atq-notif-black.svg';
import IconNotifWhite from './ic-atq-notif-white.svg';
import IconUploadWhite from './ic-atq-upload-white.svg';
import IconSkorPrimary from './ic-atq-skor-primary.svg';
import IconFileWhite from './ic-atq-file-white.svg';
import IconImageWhite from './ic-atq-image-white.svg';
import IconCameraWhite from './ic-atq-camera-white.svg';
import IconLogin from './ic-atq-login-white.svg';
import IconCheckColor from './ic-atq-check.svg';
import IconRightColor from './ic-atq-right-color.svg';
import IconDownloadColor from './ic-atq-download-color.svg';
import IconDownloadWhite from './ic-atq-download-white.svg';
import IconTimesColor from './ic-atq-times-color.svg';
import IconCloseWhite from './ic-atq-close-white.svg';
import IconSearchWhite from './ic-atq-search-white.svg';
import IconEditWhite from './ic-atq-edit-white.svg';
import IconPinUser from './ic-atq-pin-user.svg';
import IconPasswordUser from './ic-atq-password-user.svg';
import IconVerified from './ic-atq-verified-blue.svg';
import IconRightPrimary from './ic-atq-right-primary.svg';
import IconRuangBelajarMateri from './ic-atq-ruang-belajar-materi.svg';
import IconRuangBelajarDeskripsi from './ic-atq-ruang-belajar-deskripsi.svg';
import IconRuangBelajarAnggota from './ic-atq-ruang-belajar-anggota.svg';
import IconMutabaahList from './ic-atq-mutabaah-list.svg';
import IconRuangBelajarLink from './ic-atq-ruang-belajar-link.svg';
import IconRuangBelajarDownload from './ic-atq-ruang-belajar-download.svg';
import IconMateriVideo from './ic-atq-materi-video.svg';
import IconMateriFile from './ic-atq-materi-file.svg';
import IconMateriImage from './ic-atq-materi-image.svg';
import IconCatatanOrange from './ic-atq-catatan-orange.svg';
import IconRemedialOrange from './ic-atq-remedial-orange.svg';


export {
    ////neeeewwwwww
    IconHomeActive,
    IconHomeNonActive,
    IconTagihanActive,
    IconTagihanNonActive,
    IconNotifActive,
    IconNotifNonActive,
    IconSettingActive,
    IconSettingNonActive,

    IconRuangBelajar,
    IconRuangBelajar1,
    IconDateWhite,
    IconNotifBlack,
    IconNotifWhite,
    IconUploadWhite,
    IconSkorPrimary,
    IconFileWhite,
    IconImageWhite,
    IconCameraWhite,
    IconLogin,
    IconCheckColor,
    IconRightColor,
    IconDownloadColor,
    IconDownloadWhite,
    IconTimesColor,
    IconCloseWhite,
    IconSearchWhite,
    IconEditWhite,
    IconPinUser,
    IconPasswordUser,
    IconVerified,
    IconRightPrimary,
    IconRuangBelajarMateri,
    IconRuangBelajarDeskripsi,
    IconRuangBelajarAnggota,
    IconMutabaahList,
    IconRuangBelajarLink,
    IconRuangBelajarDownload,
    IconMateriVideo,
    IconMateriFile,
    IconMateriImage,
    IconCatatanOrange,
    IconRemedialOrange,
   

    ////end neeeewwwwww
    
    IconMessages,
    IconMessagesActive,
    IconInbox,
    IconInboxActive,
    IconUser,
    IconUserActive,
    IconProduk,
    IconProdukActive,

    IconKeranjang,
    IconKeranjangAdd,
    IconSearch,
    IconChatProduk,
    IconHeart,
    IconShare,
    IconCoin,
    IconLokasiToko,
    IconDelete,
    IconPlus,
    IconMin,
    IconInfoResiAuto,
    IconCoResiAuto,
    IconCoRight,
    IconCoAlamat,
    IconCoPesanan,
    IconCoLokasiToko,
    IconCoRincian,
    IconBackYellow,
    IconBackDark,
    IconRefreshDark,
    
    IconMore,
    IconMoreActive,
    IconScan,
    IconTopup,
    IconRefresh,
    IconBack,
    IconRight,
    IconCheck,
    IconMasuk,
    IconDaftar,
    IconLupaPassword,
    IconUbahProfile,
    IconBantuan,
    IconAffilate,
    IconSnK,
    IconPanduan,
    IconPromo,
    IconScanQr,
    IconTransfer,
    IconKupon,
    IconNotifikasi,
    IconWithdraw,
    IconLainnya,
    IconQrCode,
    IconBronze,

    IconFPulsa,
    IconFPaketData,

    IconHome,
    IconTribe,
    IconTribeActive,

    IconMenuKeranjang,
    IconMenuPembayaran,
    IconMenuProduk,
    IconMenuPoin,
    IconMenuLearning,
    IconMenuTutorial,
    IconMenuIklan,
    IconMenuPembelian,

    IconStar,
    IconPlayVidio,
};