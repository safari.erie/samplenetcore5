import DummyUser from './user.png';
import DummyDoctor1 from './doctor1.png';
import DummyDoctor2 from './doctor2.png';
import DummyDoctor3 from './doctor3.png';
import DummyNews1 from './news1.png';
import DummyNews2 from './news2.png';
import DummyNews3 from './news3.png';
import DummyHospital1 from './hospital1.png';
import DummyHospital2 from './hospital2.png';
import DummyHospital3 from './hospital3.png';
import PaymentGopay from './payment-gopay.png';
import PaymentOvo from './payment-ovo.png';
import PaymentBca from './payment-bca.png';
import PaymentMandiri from './payment-mandiri.png';
import PaymentBni from './payment-bni.png';
import PaymentAbupay from './payment-abupay.png';
import Loading from './loading.gif';
import GIFResultSuccess from './dp-result-success.gif';
import LoadingSplash from './loading-splash.gif';


////NEW FOR MENU
import IMGMenuBelajar from './img-atq-menu-belajar.png';
import IMGMenuQiroaty from './img-atq-menu-qiroaty.png';
import IMGMenuMutabaah from './img-atq-menu-mutabaah.png';
import IMGMenuMore from './img-atq-menu-more.png';
import IMGMenuRapor from './img-atq-menu-rapor.png';
import IMGMenuPenilaian from './img-atq-menu-penilaian.png';

export {
    IMGMenuBelajar,
    IMGMenuQiroaty,
    IMGMenuMutabaah,
    IMGMenuMore,
    IMGMenuRapor,
    IMGMenuPenilaian,

    DummyUser,
    DummyDoctor1,
    DummyDoctor2,
    DummyDoctor3,
    DummyNews1,
    DummyNews2,
    DummyNews3,
    DummyHospital1,
    DummyHospital2,
    DummyHospital3,
    PaymentGopay,
    PaymentOvo,
    PaymentBca,
    PaymentMandiri,
    PaymentBni,
    PaymentAbupay,
    Loading,
    GIFResultSuccess,
    LoadingSplash
}