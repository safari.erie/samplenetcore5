// import ILLogo from './logo.svg';
import ILGetStarted from './get-started.png';
import ILNullPhoto from './null-photo.png';
import ILCatUmum from './cat-dok-umum.svg';
import ILCatPsikater from './cat-dok-psikater.svg';
import ILCatObat from './cat-dok-obat.svg';
import ILHospitalBG from './hospitals-background.png';
import ILHeader from './header.png';

import ILLogin from './dp-login.png';
import ILBg1 from './dp-ilus1.jpg';
import ILBg2 from './dp-ilus2.jpg';
import ILBg3 from './dp-ilus3.jpg';

///new
import ILLogo from './atq-logo.png';
import ILEbook from './atq-ilus-ebook.png';
import ILLoading from './atq-ilus-loading.png';
import ILTagihanSebelum from './atq-ilus-tagihan-sebelum.png';
import ILTagihanBerjalan from './atq-ilus-tagihan-berjalan.png';
import ILNilaiKosong from './atq-ilus-nilai-kosong.png';
import ILRapor from './atq-ilus-rapor.png';
import ILMutabaah from './atq-ilus-mutabaah.png';

export {
    ILLogo,
    ILEbook,
    ILLoading,
    ILTagihanSebelum,
    ILTagihanBerjalan,
    ILNilaiKosong,
    ILRapor,
    ILMutabaah,
    
    ILGetStarted, 
    ILNullPhoto,
    ILCatUmum,
    ILCatPsikater,
    ILCatObat,
    ILHospitalBG,
    ILHeader,
    ILLogin,
    ILBg1,
    ILBg2,
    ILBg3
};
