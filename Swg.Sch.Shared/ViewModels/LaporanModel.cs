﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class ElTanggalModel
    {
        public string TanggalAwal { get; set; }
        public string TanggalAkhir { get; set; }
    }
    public class ElDashboardModel
    {
        public int IdUnit { get; set; }
        public string NamaUnit { get; set; }
        public double PstGuruIsiMateri { get; set; }
        public double PstGuruIsiNilaiSiswa { get; set; }
        public double PstSiswaTugasSelesai { get; set; }
        public double PstSiswaTidakHadir { get; set; }
        public double PstSiswaIsiMutabaah { get; set; }
    }
    #region Materi dan tugas
    public class ElMateriTugasModel
    {
        public ElTanggalModel Periode { get; set; }
        public List<ElMateriTugasUnitModel> Units { get; set; }
    }
    public class ElMateriTugasUnitModel
    {
        public int IdUnit { get; set; }
        public string Unit { get; set; }
        public int JmlTotal { get; set; }
        public int JmlInput { get; set; }
        public int JmlNoInput { get; set; }
    }
    public class ElMateriTugasKelasModel : ElMateriTugasUnitModel
    {
        public int IdKelas { get; set; }
        public string Kelas { get; set; }
    }
    public class ElMateriTugasKelasParalelModel : ElMateriTugasKelasModel
    {
        public int IdKelasParalel { get; set; }
        public string KelasParalel { get; set; }
    }
    public class ElMateriTugasListModel
    {
        public string InfoJudul { get; set; }
        public List<ElMateriTugasMapelModel> Input { get; set; }
        public List<ElMateriTugasMapelModel> NotInput { get; set; }
    }
    public class ElMateriTugasMapelModel
    {
        public string NamaGuru { get; set; }
        public string MataPelajaran { get; set; }
        public string Hari { get; set; }
        public string JamMulai { get; set; }
        public string JamSelesai { get; set; }
        public string JudulMateri { get; set; }
        public string FileMateri { get; set; }
    }
    #endregion
    #region kbm siswa
    public class ElKbmSiswaModel
    {
        public ElTanggalModel Periode { get; set; }
        public List<ElKbmSiswaUnitModel> Units { get; set; }
    }
    public class ElKbmSiswaUnitModel
    {
        public int IdUnit { get; set; }
        public string Unit { get; set; }
        public int JmlTotal { get; set; }
        public int JmlJawab { get; set; }
        public int JmlNoJawab { get; set; }
    }
    public class ElKbmSiswaKelasModel : ElKbmSiswaUnitModel
    {
        public int IdKelas { get; set; }
        public string Kelas { get; set; }
    }
    public class ElKbmSiswaKelasParalelModel : ElKbmSiswaKelasModel
    {
        public int IdKelasParalel { get; set; }
        public string KelasParalel { get; set; }
    }
    public class ElKbmSiswaListModel
    {
        public string InfoJudul { get; set; }
        public List<ElKbmSiswaMapelModel> Jawab { get; set; }
        public List<ElKbmSiswaMapelModel> NoJawab { get; set; }
    }
    public class ElKbmSiswaMapelModel
    {
        public string Nis { get; set; }
        public string NamaSiswa { get; set; }
        public string MataPelajaran { get; set; }
        public string Hari { get; set; }
        public string JamMulai { get; set; }
        public string JamSelesai { get; set; }
        public string FileJawaban { get; set; }
        public string Nilai { get; set; }
    }
    #endregion
    #region kehadiran siswa
    public class ElAbsenSiswaModel
    {
        public ElTanggalModel Periode { get; set; }
        public List<ElAbsenSiswaUnitModel> Units { get; set; }
    }
    public class ElAbsenSiswaUnitModel
    {
        public int IdUnit { get; set; }
        public string Unit { get; set; }
        public int JmlTotal { get; set; }
        public int JmlHadir { get; set; }
        public int JmlNoHadir { get; set; }
    }
    public class ElAbsenSiswaKelasModel : ElAbsenSiswaUnitModel
    {
        public int IdKelas { get; set; }
        public string Kelas { get; set; }
    }
    public class ElAbsenSiswaKelasParalelModel : ElAbsenSiswaKelasModel
    {
        public int IdKelasParalel { get; set; }
        public string KelasParalel { get; set; }
    }

    public class ElAbsenSiswaMapelModel : ElAbsenSiswaKelasParalelModel
    {
        public int IdMapel { get; set; }
        public string NamaMapel { get; set; }
    }
    public class ElAbsenSiswaMapelListModel
    {
        public string InfoJudul { get; set; }
        public List<ElAbsenSiswaListModel> Hadir { get; set; }
        public List<ElAbsenSiswaListModel> NoHadir { get; set; }
    }
    public class ElAbsenSiswaListModel
    {
        public string Nis { get; set; }
        public string NamaSiswa { get; set; }
        public string MataPelajaran { get; set; }
        public string Hari { get; set; }
        public string JamMulai { get; set; }
        public string JamSelesai { get; set; }
    }
    #endregion
    #region evaluasi harian siswa
    public class ElEvalHarianModel
    {
        public ElTanggalModel Periode { get; set; }
        public List<ElEvalHarianUnitModel> Units { get; set; }
    }
    public class ElEvalHarianUnitModel
    {
        public int IdUnit { get; set; }
        public string Unit { get; set; }
        public int JmlTotal { get; set; }
        public int JmlIsi { get; set; }
        public int JmlNoIsi { get; set; }
    }
    public class ElEvalHarianKelasModel : ElEvalHarianUnitModel
    {
        public int IdKelas { get; set; }
        public string Kelas { get; set; }
    }
    public class ElEvalHarianKelasParalelModel : ElEvalHarianKelasModel
    {
        public int IdKelasParalel { get; set; }
        public string KelasParalel { get; set; }
    }

    public class ElEvalHarianListModel
    {
        public string Unit { get; set; }
        public string Kelas { get; set; }
        public string KelasParalel { get; set; }
        public int IdSiswa { get; set; }
        public string Nis { get; set; }
        public string NamaSiswa { get; set; }
        public string Minggu { get; set; }
        public string Senin { get; set; }
        public string Selasa { get; set; }
        public string Rabu { get; set; }
        public string Kamis { get; set; }
        public string Jumat { get; set; }
        public string Sabtu { get; set; }
    }
    public class ElEvalHarianSiswaModel
    {
        public string InfoJudul { get; set; }
        public List<ElEvalHarianSiswaListModel> Data { get; set; }
    }
    public class ElEvalHarianSiswaListModel
    {
        public string NamaMutabaah { get; set; }
        public string YaTidak { get; set; }
        public string Catatan { get; set; }
    }
    #endregion
    #region kelas quran
    public class ElKelasQuranModel
    {
        public ElTanggalModel Periode { get; set; }
        public List<ElKelasQuranUnitModel> Units { get; set; }
    }
    public class ElKelasQuranUnitModel
    {
        public int IdUnit { get; set; }
        public string Unit { get; set; }
        public int JmlTotal { get; set; }
        public int JmlBaca { get; set; }
        public int JmlTes { get; set; }
    }
    public class ElKelasQuranListModel
    {
        public int IdKelasQuran { get; set; }
        public string Kelompok { get; set; }
        public int JmlTotal { get; set; }
        public int JmlBaca { get; set; }
        public int JmlTes { get; set; }
    }
    public class ElKelasQuranBacaModel
    {
        public int IdKelasQuran { get; set; }
        public string Kelompok { get; set; }
        public int JmlTotal { get; set; }
        public int JmlBaca { get; set; }
        public int JmlTes { get; set; }
    }
    public class ElKelasQuranTesModel
    {
        public int IdKelasQuran { get; set; }
        public string Kelompok { get; set; }
        public int JmlTotal { get; set; }
        public int JmlBaca { get; set; }
        public int JmlTes { get; set; }
    }
    #endregion
    #region laporan gaji
    public class GajiPegawaiBulanModel
    {
        public string Periode { get; set; }
        public double GT_Total { get; set; }
        public double GTT_Total { get; set; }
        public double Pot_Total { get; set; }
        public double Tam_Total { get; set; }
        public double Total { get; set; }
    }
    public class GajiPegawaiUnitModel : GajiPegawaiBulanModel
    {
        public int IdUnit { get; set; }
        public string Unit { get; set; }
    }
    #endregion

    #region laporan elarning excel
    public class SheetLaporanElearningProgressPegawai
    {
        public string UnitKerja { get; set; }
        public string Nip { get; set; }
        public string NamaPegawai { get; set; }
        public double Materi { get; set; }
        public double Soal { get; set; }
        public double Menilai { get; set; }
        public double KehadiranSiswa { get; set; }
        public string Keterangan { get; set; }
    }
    #endregion
}
