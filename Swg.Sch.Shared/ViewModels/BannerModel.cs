using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace Swg.Sch.Shared.ViewModels
{
    public class BannerDefaultModel
    {
        public int IdBanner { get; set; }
        public string Nama { get; set; }
        public string Tipe { get; set; }
    }
    public class BannerModel : BannerDefaultModel
    {
        public string FileImage { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedDate { get; set; }
    }
    public class BannerAddEditModel : BannerDefaultModel
    {
        public IFormFile FileImage { get; set; }
    }

}
