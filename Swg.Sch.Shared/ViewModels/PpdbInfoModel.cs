﻿using System;
using Microsoft.AspNetCore.Http;

namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbInfoModel
    {
        public int IdPpdbInfo { get; set; }
        public int IdPpdb { get; set; }
        public int IdJenisPpdbInfo { get; set; }
        public string Judul { get; set; }
        public string Konten { get; set; }
        public string Url { get; set; }
    }

    public class PpdbInfoModelRead : PpdbInfoModel
    {
        public string NamaTipe { get; set; }
        public string FileImage { get; set; }
    }
    public class PpdbInfoModelAddEdit : PpdbInfoModel
    {
        public IFormFile FileImage { get; set; }
    }
}
