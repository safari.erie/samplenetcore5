using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbInputObservasiSheetModel
    {
        public int IdPpdbDaftar { get; set; }
        public string StatusHasilObservasi { get; set; }
        public string Catatan { get; set; }
        public List<PpdbBiayaSiswaDetilAddModel> Biayas { get; set; }
        public List<PpdbBiayaSiswaCicilAddModel> Cicils { get; set; }
    }
}