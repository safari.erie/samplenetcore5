﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.ViewModels
{
    public class ReportModel
    {
    }

    public class UnitReportModel
    {
        public string NamaUnit { get; set; }
        public int Jumlah { get; set; }
        public int SudahInput { get; set; }
        public int BelumInput { get; set; }
    }

    public class UnitReportData : List<UnitReportModel>
    {
        public UnitReportData()
        {
            Add(new UnitReportModel()
            {
                NamaUnit = "TK",
                Jumlah = 10,
                SudahInput = 11,
                BelumInput = 12
          
            });

            Add(new UnitReportModel()
            {
                NamaUnit = "SD",
                Jumlah = 20,
                SudahInput = 21,
                BelumInput = 22

            });
        }
    }
}
