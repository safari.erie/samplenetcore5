using System;

namespace Swg.Sch.Shared.ViewModels
{
    public class PartnerRequestModel : PartnerRequestDetilModel
    {
        public string Nis { get; set; }
        public string KdPartner { get; set; }
        public string MethodName { get; set; }
        public DateTime InDate { get; set; }
        public string ResponseStatus { get; set; }
        public string ResponseMessage { get; set; }
        public string ResponseResult { get; set; }
        public string IpAddress { get; set; }
    }
    public class PartnerRequestDetilModel
    {
        public string ParamName { get; set; }
        public string ParamValue { get; set; }
    }
}