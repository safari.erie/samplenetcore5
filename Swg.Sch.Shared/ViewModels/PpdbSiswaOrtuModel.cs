﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbSiswaOrtuModel : PpdbSiswaOrtuAddModel
    {
        public int IdPpdbDaftar { get; set; }
    }
    public class PpdbSiswaOrtuAddModel
    {
        public int IdTipe { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public string Nik { get; set; }
        public int IdJenisPekerjaan { get; set; }
        public string NamaInstansi { get; set; }
        public string Jabatan { get; set; }
        public string Email { get; set; }
        public string NoHandphone { get; set; }
        public string NoTelpRumah { get; set; }
        public int IdAgama { get; set; }
        public int IdKewarganegaraan { get; set; }
        public int IdJenjangPendidikan { get; set; }
        public int IdStatusPenikahan { get; set; }
        public string TempatLahir { get; set; }
        public string TanggalLahir { get; set; }
        public int IdStatusHidup { get; set; }
        public string TanggalMeninggal { get; set; }
    }
}
