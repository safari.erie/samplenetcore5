﻿using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class KelasParalelModel
    {
        public int IdKelasParalel { get; set; }
        public int IdKelas { get; set; }
        public int IdUnit { get; set; }
        public string Nama { get; set; }
        public string NamaSingkat { get; set; }
    }
    public class KelasParalelPegawaiModel : KelasParalelModel
    {
        public int IdPegawai { get; set; }
        public string NamaPegawai { get; set; }
        public int IdJenisPegawaiKelasParalel { get; set; }
        public string JenisPegawaiKelasParalel { get; set; }
    }
}
