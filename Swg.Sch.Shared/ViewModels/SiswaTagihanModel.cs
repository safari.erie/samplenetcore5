using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class SiswaTagihanModel
    {
        public int IdSiswa { get; set; }
        public string Periode { get; set; }
        public double RpTotal { get; set; }
        public double RpBerjalan { get; set; }
        public double RpSebelumnya { get; set; }
        public string Status { get; set; }
        public string TanggalLunas { get; set; }
        public string RefNo { get; set; }
        public DateTime Min { get; set; }
        public DateTime Max { get; set; }

        public List<SiswaTagihanDetilModel> TagihanBerjalan { get; set; }
        public List<SiswaTagihanDetilModel> TagihanSebelumnya { get; set; }
    }
    public class SiswaTagihanDetilModel
    {
        public int IdJenisTagihanSiswa { get; set; }
        public string JenisTagihan { get; set; }
        public double Rp { get; set; }
        public string Periode { get; set; }

    }
}
