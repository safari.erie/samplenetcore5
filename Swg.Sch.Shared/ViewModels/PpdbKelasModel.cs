﻿using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbKelasModel
    {
        public int IdKelas { get; set; }
        public int? IdJenisPeminatan { get; set; }
        public int KuotaDaftarP { get; set; }
        public int KuotaDaftarL { get; set; }
        public int KuotaDaftarPrestasi { get; set; }
        public int KuotaTerimaP { get; set; }
        public int KuotaTerimaL { get; set; }
        public string MaxTanggalLahir { get; set; }
    }
    public class PpdbKelasViewModel : PpdbKelasEditModel
    {
        public int IdUnit { get; set; }
        public string Kelas { get; set; }
    }
    public class PpdbKelasEditModel : PpdbKelasModel
    {
        public int IdPpdbKelas { get; set; }
    }
}
