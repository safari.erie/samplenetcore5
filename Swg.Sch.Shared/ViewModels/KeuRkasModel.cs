﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class RkasReffDownloadModel
    {
        public string KodeFile { get; set; }
        public string NamaUnit { get; set; }
        public string TahunAjaran { get; set; }
        public List<string> KodeCoas { get; set; }
        public List<string> JenisSatuans { get; set; }
        public List<string> JenisVolumes { get; set; }
    }
    public class RkasUploadModel
    {
        public string NamaUnit { get; set; }
        public string TahunAjaran { get; set; }
        public string Nomor { get; set; }
        public string StrCode { get; set; }
        public List<RkasCoaModel> DataRkas { get; set; }
    }
    public class RkasCoaModel
    {
        public string Kode { get; set; }
        public List<RkasDetilModel> Detil { get; set; }
    }
    public class RkasDetilModel
    {
        public string Tanggal { get; set; }
        public string Uraian { get; set; }
        public double JumlahSatuan { get; set; }
        public string Satuan { get; set; }
        public double JumlahVolume { get; set; }
        public string Volume { get; set; }
        public double HargaSatuan { get; set; }
        public double TotalRp { get; set; }
        public string Catatan { get; set; }
    }
    public class RkasListModel
    {
        public int IdRkas { get; set; }
        public string TahunAjaran { get; set; }
        public string NamaUnit { get; set; }
        public string Nomor { get; set; }
        public string Status { get; set; }
        public string StatusProses { get; set; }
        public string Catatan { get; set; }
        public double TotalRp { get; set; }
        public List<string> Aksis { get; set; }
    }
    public class RkasCoaListModel
    {
        public int IdRkasCoa { get; set; }
        public int IdRkas { get; set; }
        public string KodeCoa { get; set; }
        public string NamaCoa { get; set; }
        public double TotalRp { get; set; }
        public List<string> Aksis { get; set; }
    }
    public class RkasDetilListModel : RkasDetilEditModel
    {
        public List<string> Aksis { get; set; }
    }
    public class RkasDetilEditModel : RkasDetilAddModel
    {
        public int IdRkasDetil { get; set; }
    }
    public class RkasDetilAddModel : RkasDetilModel
    {
        public int IdRkasCoa { get; set; }
        public int IdJenisSatuan { get; set; }
        public int IdJenisVolume { get; set; }
    }

    public class RkasDownloadModel
    {
        public string NamaUnit { get; set; }
        public string TahunAjaran { get; set; }
        public string Nomor { get; set; }
        public double TotalRp { get; set; }
        public List<RkasDetDownloadModel> Det { get; set; }
    }
    public class RkasDetDownloadModel
    {
        public int IdCoa { get; set; }
        public int? IdParentCoa { get; set; }
        public string KodeCoa { get; set; }
        public string NamaCoa { get; set; }
        public double TotalRp { get; set; }

        public string Tanggal { get; set; }
        public string Uraian { get; set; }
        public double JumlahSatuan { get; set; }
        public string Satuan { get; set; }
        public double JumlahVolume { get; set; }
        public string Volume { get; set; }
        public double HargaSatuan { get; set; }
        public string Catatan { get; set; }
        public List<RkasDetDownloadModel> Det { get; set; }
    }

}
