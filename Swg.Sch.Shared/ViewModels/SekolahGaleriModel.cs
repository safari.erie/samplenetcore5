﻿using Microsoft.AspNetCore.Http;
using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class SekolahGaleriModel 
    {
        public int IdSekolahGaleri { get; set; }
        public int IdSekolah { get; set; }
        public int IdJenisGaleri { get; set; }
        public string Judul { get; set; }
        public string Keterangan { get; set; }
        public string JenisGaleri { get; set; }
        public string UrlVideo { get; set; }
    }
    public class SekolahGaleriReadModel : SekolahGaleriModel
    {
        public string FileUrl { get; set; }
    }
    public class SekolahGaleriAddEditModel : SekolahGaleriModel
    {
        public IFormFile FileUrl { get; set; }
    }
}
