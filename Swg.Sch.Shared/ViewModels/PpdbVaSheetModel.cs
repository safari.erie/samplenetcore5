﻿using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbVaSheetModel
    {
        public int IdPpdbVa { get; set; }
        public string Unit { get; set; }
        public string Status { get; set; }
    }
}
