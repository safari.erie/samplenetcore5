﻿using System;
using Microsoft.AspNetCore.Http;
namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbModel
    {
        public int IdPpdb { get; set; }
        public string TahunAjaran { get; set; }
        public string TanggalAwal { get; set; }
        public string TanggalAkhir { get; set; }
        public int IdKetua { get; set; }
        public int MinNoSk { get; set; }
        public string NamaKetua { get; set; }
    }
    public class PpdbModelRead : PpdbModel
    {
        public string FilePetunjuk { get; set; }
    }

    public class PpdbModelAddEdit : PpdbModel
    {
        public IFormFile FilePetunjuk { get; set; }
    }
}
