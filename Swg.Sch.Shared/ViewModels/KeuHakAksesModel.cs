﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Sch.Shared.ViewModels
{
    public class KeuHakAksesModel
    {
        public bool CanRead { get; set; }
        public bool CanDownload { get; set; }
        public bool CanAdd { get; set; }
        public bool CanEdit { get; set; }
        public bool CanDelete { get; set; }
        public bool CanApprove { get; set; }
        public bool CanRelease { get; set; }

    }
}
