﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace Swg.Sch.Shared.ViewModels
{

    public class KbmListModel
    {
        public int IdKbmMateri { get; set; }
        public string Tanggal { get; set; }
        public string Guru { get; set; }
        public string NamaKelasParalel { get; set; }
        public string NamaMapel { get; set; }
        public string Hari { get; set; }
        public string JamMulai { get; set; }
        public string JamSelesai { get; set; }
        public string JudulMateri { get; set; }

    }
    public class KbmSiswaListModel : KbmListModel
    {
        public int IdSiswa { get; set; }
        public string Nis { get; set; }
        public string NamaSiswa { get; set; }
        public int IsRemedial { get; set; }
        public int IdJenisSoal { get; set; }
        public double? NilaiAngka { get; set; }
        public string NilaiHuruf { get; set; }
        public string Catatan { get; set; }
        public int IdJenisPathFile { get; set; }
        public string PathFileUrl { get; set; }
        public string StatusMeeting { get; set; }
        public string NamaUrlMeeting { get; set; }
        public KbmJawabanSusulanModel JawabanSusulan { get; set; }
    }
    public class KbmSiswaModel : KbmSiswaListModel
    {
        public int IdUnit { get; set; }
        public string FotoGuru { get; set; }
        public string Deskripsi { get; set; }
        public string DeskripsiTugas { get; set; }
        public string NamaFileMateri { get; set; }
        public KbmSoalModel SoalReguler { get; set; }
        public KbmSoalModel SoalRemedial { get; set; }
        public KbmSoalModel SoalSusulan { get; set; }
        public List<KbmMediaModel> FileKbmMedias { get; set; }
        public int TotFileKbmUrl { get; set; }
        public int TotFileKbmFile { get; set; }
        public List<TugasSiswaList> TugasSiswas { get; set; }
        // public List<KbmMediaModel> FileKbmMediaImages { get; set; }
        // public List<KbmMediaModel> FileKbmMediaVideos { get; set; }

    }
    public class KbmMediaModel
    {
        public int IdKbmMedia { get; set; }
        public int IdJenisPathFile { get; set; }
        public string PathFileUrl { get; set; }
    }
    public class TugasSiswaList
    {
        public string NamaSiswa { get; set; }
        public string TugasStatus { get; set; }
    }
    public class KbmGuruListModel : KbmListModel
    {
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedDate { get; set; }
    }
    public class KbmGuruModel : KbmListModel
    {
        public string Deskripsi { get; set; }
        public string DeskripsiTugas { get; set; }
        public string NamaFileMateri { get; set; }
        public string NamaUrlMeeting { get; set; }
        public KbmSoalModel SoalReguler { get; set; }
        public KbmSoalModel SoalRemedial { get; set; }
        public KbmSoalModel SoalSusulan { get; set; }
        public List<KbmMediaModel> FileKbmMedias { get; set; }
    }
    public class KbmSoalModel
    {
        public int IdJenisSoal { get; set; }
        public string JenisSoal { get; set; }
        public int IdJenisJawaban { get; set; }
        public string JenisJawaban { get; set; }
        public string NamaFile { get; set; }
        public string NamaUrl { get; set; }
    }

    #region Evaluasi Harian Siswa
    public class EvalHarianUnitModel
    {
        public int IdUnit { get; set; }
        public string NamaUnit { get; set; }
        public List<EvalHarianModel> AssignEvals { get; set; }
        public List<EvalHarianModel> NoAssignEvals { get; set; }
    }
    public class EvalHarianModel
    {
        public int IdJenisEvaluasiHarian { get; set; }
        public string Nama { get; set; }
        public string NamaSingkat { get; set; }
    }
    public class KbmEvalHarianAddModel
    {
        public int IdSiswa { get; set; }
        public int IdJenisEvaluasiHarian { get; set; }
        public string Tanggal { get; set; }
        public string IsDone { get; set; }
        public string Catatan { get; set; }
    }
    public class KbmEvalHarianListModel
    {
        public int IdJenisEvaluasi { get; set; }
        public string JenisEvaluasi { get; set; }
        public string HariKe1 { get; set; }
        public string HariKe1Tanggal { get; set; }
        public string HariKe2 { get; set; }
        public string HariKe2Tanggal { get; set; }
        public string HariKe3 { get; set; }
        public string HariKe3Tanggal { get; set; }
        public string HariKe4 { get; set; }
        public string HariKe4Tanggal { get; set; }
        public string HariKe5 { get; set; }
        public string HariKe5Tanggal { get; set; }
        public string HariKe6 { get; set; }
        public string HariKe6Tanggal { get; set; }
        public string HariKe7 { get; set; }
        public string HariKe7Tanggal { get; set; }
    }
    public class KbmEvalHarianModel
    {
        public string IsDone { get; set; }
        public string Catatan { get; set; }
    }
    public class KbmEvalHarianTempMpdel
    {
        public DateTime Tanggal { get; set; }
        public int IdJenisEvaluasiHarian { get; set; }
        public string Nama { get; set; }
        public string IsDone { get; set; }
        public string Catatan { get; set; }
    }
    #endregion Evaluasi Harian Siswa

    #region Evaluasi Harian Pegawai
    public class EvalHarianUnitPegawaiModel
    {
        public int IdUnit { get; set; }
        public string NamaUnit { get; set; }
        public List<EvalHarianPegawaiModel> AssignEvals { get; set; }
        public List<EvalHarianPegawaiModel> NoAssignEvals { get; set; }
    }
    public class EvalHarianPegawaiModel
    {
        public int IdJenisEvaluasiHarianPegawai { get; set; }
        public string Nama { get; set; }
        public string NamaSingkat { get; set; }
    }
    public class KbmEvalHarianAddPegawaiModel
    {
        public int IdSiswa { get; set; }
        public int IdJenisEvaluasiHarianPegawai { get; set; }
        public string Tanggal { get; set; }
        public string IsDone { get; set; }
        public string Catatan { get; set; }
    }
    public class KbmEvalHarianListPegawaiModel
    {
        public int IdJenisEvaluasiPegawai { get; set; }
        public string JenisEvaluasiPegawai { get; set; }
        public string HariKe1 { get; set; }
        public string HariKe1Tanggal { get; set; }
        public string HariKe2 { get; set; }
        public string HariKe2Tanggal { get; set; }
        public string HariKe3 { get; set; }
        public string HariKe3Tanggal { get; set; }
        public string HariKe4 { get; set; }
        public string HariKe4Tanggal { get; set; }
        public string HariKe5 { get; set; }
        public string HariKe5Tanggal { get; set; }
        public string HariKe6 { get; set; }
        public string HariKe6Tanggal { get; set; }
        public string HariKe7 { get; set; }
        public string HariKe7Tanggal { get; set; }
    }
    public class KbmEvalHarianPegawaiModel
    {
        public string IsDone { get; set; }
        public string Catatan { get; set; }
    }
    public class KbmEvalHarianTempPegawaiMpdel
    {
        public DateTime Tanggal { get; set; }
        public int IdJenisEvaluasiHarianPegawai { get; set; }
        public string Nama { get; set; }
        public string IsDone { get; set; }
        public string Catatan { get; set; }
    }
    #endregion Evaluasi Harian Pegawai

    //proses update
    public class KbmUpdateMateriModel
    {
        public string Judul { get; set; }
        public string Deskripsi { get; set; }
        public string DeskripsiTugas { get; set; }
        // public List<KbmUpdateSoalModel> FileSoals { get; set; }
        // public List<KbmUpdateMateriMediaModel> FileMedias { get; set; }
    }
    public class KbmUpdateSoalModel
    {
        public int IdJenisSoal { get; set; }
        public int IdJenisPathFile { get; set; }
        public string NamaUrl { get; set; }
        public IFormFile FileSoal { get; set; }
    }
    public class KbmUpdateMateriMediaModel
    {
        public int IdJenisPathFile { get; set; }
        public string NamaUrl { get; set; }
        public IFormFile FileMedia { get; set; }
    }

    public class KbmJawabanSusulanModel
    {
        public int IdJenisPathFile { get; set; }
        public string NamaUrl { get; set; }
        public string NamaFile { get; set; }
        public string Catatan { get; set; }
    }

    public class KbmListJadwalModel
    {
        public int IdKbmMateri { get; set; }
        public int IdUnit { get; set; }
        public int IdKelas { get; set; }
        public int IdKelasParalel { get; set; }
        public string NamaKelasParalel { get; set; }
        public int IdMapel { get; set; }
        public string NamaMapel { get; set; }
        public int IdPegawai { get; set; }
        public string NamaGuru { get; set; }
        public string FotoGuru { get; set; }
        public int IdKbmJadwal { get; set; }
        public int HariKe { get; set; }
        public string Hari { get; set; }
        public string JamMulai { get; set; }
        public string JamSelesai { get; set; }
        public string Tanggal { get; set; }
    }


    public class KelasQuranModel
    {
        public int IdKelasQuran { get; set; }
        public string Kode { get; set; }
        public string Nama { get; set; }
        public string NamaSingkat { get; set; }
        public int IdPegawai { get; set; }
        public int HariKe { get; set; }
        public string JamMulai { get; set; }
        public string JamSelesai { get; set; }
        public string NamaGuru { get; set; }
        public string Hari { get; set; }
    }

    public class KelompokQuranSiswaModel
    {
        public int IdSiswa { get; set; }
        public int IdKelasQuran { get; set; }
        public string Nama { get; set; }
        public string NamaSiswa { get; set; }
    }


    public class KelasQuranExcelModel
    {
        public string Kode { get; set; }
        public string NamaKelompok { get; set; }
        public string NipGuru { get; set; }
        public string NamaGuru { get; set; }
    }
    public class KelasQuranJadwalExcelModel
    {
        public string Kode { get; set; }
        public string Hari { get; set; }
        public string JamMulai { get; set; }
        public string JamSelesai { get; set; }
    }
    public class KelasQuranSiswaExcelModel
    {
        public string Kode { get; set; }
        public string Nis { get; set; }
    }
    public class KelasQuranAllExcelModel
    {
        public List<KelasQuranExcelModel> KelasQuran { get; set; }
        public List<KelasQuranJadwalExcelModel> KelasQuranJadwal { get; set; }
        public List<KelasQuranSiswaExcelModel> KelasQuranSiswa { get; set; }
    }


    public class KbmJadwalUnitModel
    {
        public string Periode { get; set; }
        public List<KbmJadwalUnitListModel> Jadwals { get; set; }
    }
    public class KbmJadwalUnitListModel
    {
        public int IdKelasParalel { get; set; }
        public string Kelas { get; set; }
        public string KelasParalel { get; set; }
        public int? Kelompok { get; set; }
        public string Setting { get; set; }
    }

    public class PenilaianDiriSiswaAddModel
    {
        public int IdSiswa { get; set; }
        public int IdTahunAjaran { get; set; }
        public int IdSemester { get; set; }
        public List<PenilaianDiriSiswaModel> PenilaianDiris { get; set; }
    }
    public class PenilaianDiriSiswaModel
    {
        public int IdJenisPenilaianDiri { get; set; }
        public int Penilaian { get; set; }
        public string Catatan { get; set; }
    }
    public class PenilaianDiriSiswaListModel : PenilaianDiriSiswaModel
    {
        public string JenisPenilaianDiri { get; set; }
    }
}
