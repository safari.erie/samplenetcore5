﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbLaporanModel
    {
        public List<PpdbLaporanRekapModel> Rekap { get; set; }
        public List<PpdbLaporanListModel> Tk { get; set; }
        public List<PpdbLaporanListModel> Sd { get; set; }
        public List<PpdbLaporanListModel> Smp { get; set; }
        public List<PpdbLaporanListModel> Sma { get; set; }
    }
    public class PpdbLaporanRekapModel
    {
        public string Unit { get; set; }
        public int KuotaL { get; set; }
        public int KuotaP { get; set; }
        public int DaftarL { get; set; }
        public int DaftarP { get; set; }
        public int DaftarTungguL { get; set; }
        public int DaftarTungguP { get; set; }
        public int TidakBayarDaftarL { get; set; }
        public int TidakBayarDaftarP { get; set; }
        public int BayarDaftarL { get; set; }
        public int BayarDaftarP { get; set; }
        public int TidakInputFormulirL { get; set; }
        public int TidakInputFormulirP { get; set; }
        public int IkutObservasiL { get; set; }
        public int IkutObservasiP { get; set; }
        public int TidakDirekomendasikanObservasiL { get; set; }
        public int TidakDirekomendasikanObservasiP { get; set; }
        public int DirekomendasikanObservasiL { get; set; }
        public int DirekomendasikanObservasiP { get; set; }
        public int VerifHasilObservasiDiterimaL { get; set; }
        public int VerifHasilObservasiDiterimaP { get; set; }
        public int VerifHasilObservasiTidakDiterimaL { get; set; }
        public int VerifHasilObservasiTidakDiterimaP { get; set; }
        public int VerifHasilObservasiCadanganL { get; set; }
        public int VerifHasilObservasiCadanganP { get; set; }
        public int VerifTidakBayarPendidikanL { get; set; }
        public int VerifTidakBayarPendidikanP { get; set; }
        public int PengesahanL { get; set; }
        public int PengesahanP { get; set; }



        public int LulusObservasiL { get; set; }
        public int LulusObservasiP { get; set; }
        public int TidakLulusObservasiL { get; set; }
        public int TidakLulusObservasiP { get; set; }
        public int CadanganL { get; set; }
        public int CadanganP { get; set; }
        public int TidakBayarL { get; set; }
        public int TidakBayarP { get; set; }
        public int TerimaL { get; set; }
        public int TerimaP { get; set; }
    }
    public class PpdbLaporanListModel
    {
        public string IdPpdbDaftar { get; set; }
        public string JenjangPendidikan { get; set; }
        public string JurusanPendidikan { get; set; }
        public string JalurPendaftaran { get; set; }
        public string Nis { get; set; }
        public string Nama { get; set; }
        public string JenisKelamin { get; set; }
        public string TempatLahir { get; set; }
        public string TanggalLahir { get; set; }
        public string Alamat { get; set; }
        public string NamaAyah { get; set; }
        public string NoHpAyah { get; set; }
        public string EmailAyah { get; set; }
        public string NamaIbu { get; set; }
        public string NoHpIbu { get; set; }
        public string EmailIbu { get; set; }
        public string NamaPewakaf { get; set; }
        public string HubunganSiswa { get; set; }
        public string StatusProses { get; set; }
        public string StatusPpdb { get; set; }
        public List<StatusPpdbModel> StatusProsess { get; set; }
    }
    public class StatusPpdbModel
    {
        public string Status { get; set; }
        public string StatusProses { get; set; }
        public string Tanggal { get; set; }
    }
}
