﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbBiayaDaftarModel : PpdbDaftarModel
    {
        public double RpTotal { get; set; }

        public string NoVa { get; set; }

        public string StatusLunas { get; set; }
        public string RefNo { get; set; }
        public string TanggalLunas { get; set; }
        public string Catatan { get; set; }
        public int? CilKe { get; set; }
        public List<PpdbBiayaDaftarDetilModel> TagPpdb { get; set; }
    }
    public class PpdbBiayaDaftarDetilModel
    {
        public string JenisTagihan { get; set; }
        public double Rp { get; set; }
    }
}
