﻿using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbRedaksiEmailModel
    {
        public int IdPpdbRedaksiEmail { get; set; }
        public int IdPpdb { get; set; }
        public int IdJenisRedaksiEmailPpdb { get; set; }
        public string MessageBody1 { get; set; }
        public string MessageBody2 { get; set; }
        public string JenisRedaksiEmailPpdb { get; set; }
    }
}
