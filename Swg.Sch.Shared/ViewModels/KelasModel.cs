﻿using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class KelasModel : KelasUnitModel
    {
        public int IdKelas { get; set; }
        public string Nama { get; set; }
        public string NamaSingkat { get; set; }
    }
    public class KelasUnitModel
    {
        public int IdUnit { get; set; }
        public string NamaUnit { get; set; }
    }
}
