﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.ViewModels
{
    public class WebSekolahModel
    {
        public string NamaSekolah { get; set; }
        public string NamaSekolahSingkat { get; set; }
        public string Alamat { get; set; }
        public string KodePos { get; set; }
        public string NoTelp { get; set; }
        public string NoFax { get; set; }
        public string EmailSekolah { get; set; }
        public string WebSekolah { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public string Youtube { get; set; }
        public string Profile { get; set; }
        public string Video { get; set; }

        public List<WebSekolahGaleriModel> GaleriSekolah { get; set; }

        public List<WebSekolahKegiatanModel> KegiatanSekolah { get; set; }

        public List<WebVisiMisiModel> VisiMisi { get; set; }
    }

    public class WebSekolahGaleriModel
    {

        public string IdJenisGaleri { get; set; }
        public string Judul { get; set; }
        public string Keterangan { get; set; }

    }

    public class WebSekolahKegiatanModel
    {
        public int IdSekolahKegiatan { get; set; }
        public int IdSekolah { get; set; }
        public string Judul { get; set; }
        public string Keterangan { get; set; }
        public string Url { get; set; }
        public string FileImage { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }



    }

    public class WebSekolahPengurusModel : PegawaiModel
    {
        public string JenisPengurus { get; set; }

    }

    public class WebSekolahVisiMisi : WebSekolahModel
    {
        public WebVisiMisiModel StrKurikulum { get; set; }
        public WebVisiMisiModel StrVisi { get; set; }
        public WebVisiMisiModel StrMisi { get; set; }
        public List<WebVisiMisiModel> Visi { get; set; }
        public List<WebVisiMisiModel> Misi { get; set; }
    }

}
