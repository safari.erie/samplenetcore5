using Microsoft.AspNetCore.Http;

namespace Swg.Sch.Shared.ViewModels
{
    public class SekolahFasilitasDefaultModel
    {
        public int IdSekolahFasilitas { get; set; }
        public string Judul { get; set; }
        public string Deskripsi { get; set; }
    }
    public class SekolahFasilitasModel : SekolahFasilitasDefaultModel
    {
        public string FileImage { get; set; }
    }
    public class SekolahFasilitasAddEditModel : SekolahFasilitasDefaultModel
    {
        public IFormFile FileImage { get; set; }
    }
}
