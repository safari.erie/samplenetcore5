using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace Swg.Sch.Shared.ViewModels
{
    public class SiswaInboxDefaultModel
    {
        public int IdSiswaInbox { get; set; }
        public int IdSiswa { get; set; }
        public int Tipe { get; set; }
        public string StrTipe { get; set; }
        public string Judul { get; set; }
        public string Deskripsi { get; set; }
        public bool IsRead { get; set; }
        public int CreatedBy { get; set; }
    }
    public class SiswaInboxModel : SiswaInboxDefaultModel
    {
        public string CreatedDate { get; set; }
        public string UpdatedDate { get; set; }
    }

    public class SiswaInboxAddEditModel : SiswaInboxDefaultModel
    {
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
