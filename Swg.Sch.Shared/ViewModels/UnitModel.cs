﻿using Microsoft.AspNetCore.Http;
using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class UnitModel
    {
        public int IdUnit { get; set; }
        public int IdSekolah { get; set; }
        public string Nspn { get; set; }
        public string Nama { get; set; }
        public string NamaSingkat { get; set; }
        public string Alamat { get; set; }
        public string KodePos { get; set; }
        public string Desa { get; set; }
        public string Kecamatan { get; set; }
        public string Kabupaten { get; set; }
        public string Provinsi { get; set; }
        public string NoTelepon { get; set; }
        public string NoFaximili { get; set; }
        public string Email { get; set; }
        public string Web { get; set; }
        public string StatusUnit { get; set; }
        public string WaktuPenyelenggaraan { get; set; }
        public string Kurikulum { get; set; }
        public string Naungan { get; set; }
        public string NoSkPendirian { get; set; }
        public string TanggalSkPendirian { get; set; }
        public string NoSkOperasional { get; set; }
        public string TanggalSkOperasional { get; set; }
        public string JenisAkreditasi { get; set; }
        public string NoSkAkreditasi { get; set; }
        public string TanggalSkAkreditasi { get; set; }
        public string NoSertifikatIso { get; set; }
        public string NamaYayasan { get; set; }
        public double LuasTanah { get; set; }
        public double LuasBangunan { get; set; }
        public string ProviderInternet { get; set; }
        public int BandwidthInternet { get; set; }
        public string SumberListrik { get; set; }
        public int DayaListrik { get; set; }
        public string Moto { get; set; }
        public string SliderJudul { get; set; }
        public string SliderKeterangan { get; set; }
        public string Profile { get; set; }
        public string VisiMisi { get; set; }
    }
    public class UnitReadModel : UnitModel
    {
        public string Logo { get; set; }
        public string SliderBanner { get; set; }
        public string SliderImage { get; set; }
        public string Thumbnail { get; set; }
        public string ThumbnailBesar { get; set; }
        public string StrukturOrganisasi { get; set; }
    }
    public class UnitAddEditModel : UnitModel
    {
        public IFormFile Logo { get; set; }
        public IFormFile SliderBanner { get; set; }
        public IFormFile SliderImage { get; set; }
        public IFormFile Thumbnail { get; set; }
        public IFormFile ThumbnailBesar { get; set; }
        public IFormFile StrukturOrganisasi { get; set; }
    }


}
