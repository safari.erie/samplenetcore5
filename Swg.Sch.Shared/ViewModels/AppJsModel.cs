﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.ViewModels
{
    /// <summary>
    /// CreatedBy :Eri Safari
    /// CreatedDt :20200306
    /// TODO
    /// Model Load Module Javascript
    /// 
    /// </summary>

    public class AppJsModel
    {
        public string NameJs { get; set; }
        public string TypeJs { get; set; }
        public string Path { get; set; }
    }
}
