using Microsoft.AspNetCore.Http;

namespace Swg.Sch.Shared.ViewModels
{
    public class SekolahPrestasiDefaultModel
    {
        public int IdSekolahPrestasi { get; set; }
        public int IdJenisGaleri { get; set; }
        public string JenisGaleri { get; set; }
        public string Judul { get; set; }
        public string Keterangan { get; set; }
        public string FileUrl { get; set; }
    }
    public class SekolahPrestasiModel : SekolahPrestasiDefaultModel
    {
        // public string FileImage { get; set; }
    }
    public class SekolahPrestasiAddEditModel : SekolahPrestasiDefaultModel
    {
        public IFormFile FileImage { get; set; }
    }
}
