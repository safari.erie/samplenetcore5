﻿using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class MapelModel
    {
        public int IdMapel { get; set; }
        public string Kode { get; set; }
        public string Nama { get; set; }
    }
}
