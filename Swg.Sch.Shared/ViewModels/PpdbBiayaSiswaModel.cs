﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbBiayaSiswaDetilModel : PpdbBiayaSiswaDetilAddModel
    {
        public string JenisBiayaPpdb { get; set; }
    }
    public class PpdbBiayaSiswaDetilAddModel
    {
        public int IdJenisBiayaPpdb { get; set; }
        public double Rp { get; set; }
        public string Catatan { get; set; }
    }
    public class PpdbBiayaSiswaCicilModel : PpdbBiayaSiswaCicilAddModel
    {
        public string Status { get; set; }
        public string TanggalLunas { get; set; }
    }
    public class PpdbBiayaSiswaCicilAddModel
    {
        public int CilKe { get; set; }
        public double RpCilKe { get; set; }
        public string TanggalCilKe { get; set; }
    }
}
