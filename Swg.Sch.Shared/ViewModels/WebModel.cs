﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace Swg.Sch.Shared.ViewModels
{
    public class WebHomeModel
    {
        public string Icon { get; set; }
        public string Logo { get; set; }
        public string FotoSekolah { get; set; }
        public string Nama { get; set; }
        public string Moto { get; set; }
        public List<WebVisiMisiModel> VisiMisi { get; set; }
        public WebMotoUnitModel MotoUnit { get; set; }
        public List<WebKegiatanModel> Kegiatan { get; set; }
        public WebKontakModel Kontak { get; set; }
    }
    public class WebAboutModel
    {
        public List<WebVisiMisiModel> VisiMisi { get; set; }
        public List<WebPegawaiModel> Direksi { get; set; }
        public List<WebKurikulumModel> Kurikulum { get; set; }
        public WebKontakModel Kontak { get; set; }
    }
    public class WebPropenModel
    {
        public List<WebProfileUnitModel> Profile { get; set; }
        public List<WebKegiatanModel> Kegiatan { get; set; }
        public List<WebPegawaiModel> Staff { get; set; }
        public List<WebGaleriImageModel> GaleriImage { get; set; }
        public List<WebGaleriVideoModel> GaleriVideo { get; set; }
        public List<WebKegiatanModel> Ekskul { get; set; }
        public WebUnitModel Tkit { get; set; }
        public WebUnitModel Sdit { get; set; }
        public WebUnitModel Smpit { get; set; }
        public WebUnitModel Smait { get; set; }
        public int TotAlumni { get; set; }
        public int TotSiswaPrestasi { get; set; }
    }
    public class WebUnitModel
    {
        public UnitReadModel Detail { get; set; }
        public int TotSiswa { get; set; }
        public string FileImageLogo { get; set; }
        public string FileImageOrang { get; set; }
        public string FileImageBanner { get; set; }
        public WebUnitKepsekModel Kepsek { get; set; }
    }
    public class WebUnitKepsekModel
    {
        public string Nama { get; set; }
        public string Jabatan { get; set; }
        public string FileImage { get; set; }
        public string Sambutan { get; set; }
        public string Gelar { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public string Youtube { get; set; }
    }
    public class WebKegiatanByUrlModel
    {
        public WebKegiatanModel KegiatanByUrl { get; set; }

    }
    public class WebPpdbInfoModel
    {
        public WebPpdbListInfoModel Ppdb { get; set; }
        public WebPpdbListInfoImageModel PpdbImage { get; set; }
    }

    public class WebVisiMisiModel
    {
        public string Ikon { get; set; }
        public string Judul { get; set; }
        public string Keterangan { get; set; }
    }
    public class WebMotoUnitModel
    {
        public string Tk { get; set; }
        public string Sd { get; set; }
        public string Smp { get; set; }
        public string Sma { get; set; }
    }
    public class WebKegiatanModel
    {
        public int Id { get; set; }
        public string Judul { get; set; }
        public string KeteranganSingkat { get; set; }
        public string KeteranganSingkatBener { get; set; }
        public string Keterangan { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedYear { get; set; }
        public string CreatedBy { get; set; }
        public string FileImage { get; set; }
        public string Url { get; set; }
        public string Unit { get; set; }

    }
    public class WebKontakModel
    {
        public string Alamat { get; set; }
        public string KodePos { get; set; }
        public string Desa { get; set; }
        public string Kecamatan { get; set; }
        public string Kabupaten { get; set; }
        public string Provinsi { get; set; }
        public string NoTelepon { get; set; }
        public string NoFaximili { get; set; }
        public string Email { get; set; }
    }
    public class WebPegawaiModel
    {
        public string FileFoto { get; set; }
        public string Nama { get; set; }
        public string Jabatan { get; set; }
        public string Gelar { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public string Youtube { get; set; }
    }
    public class WebKurikulumModel
    {
        public int IdUnit { get; set; }
        public string NamaSekolah { get; set; }
        public string Keterangan { get; set; }
    }
    public class WebProfileUnitModel
    {
        public string Judul { get; set; }
        public string Keterangan { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string FileImage { get; set; }
        public string Url { get; set; }
        public string Unit { get; set; }
    }
    public class WebGaleriImageModel
    {
        public int IdGaleri { get; set; }
        public string Judul { get; set; }
        public string Konten { get; set; }
        public string FileGaleri { get; set; }
        public string CreatedDate { get; set; }
    }
    public class WebGaleriVideoModel
    {
        public int IdGaleri { get; set; }
        public string Judul { get; set; }
        public string Konten { get; set; }
        public string UrlGaleri { get; set; }
        public string CreatedDate { get; set; }

    }
    public class WebKontenModel
    {
        public int IdKonten { get; set; }
        public string Judul { get; set; }
        public string Konten { get; set; }
        public string FileImage { get; set; }
    }
    public class WebKontenImageModel
    {
        public int IdKonten { get; set; }
        public string NamaImage { get; set; }
    }
    public class WebPpdbListInfoModel
    {
        public List<WebKontenModel> Aturan { get; set; }
        public List<WebKontenModel> Alur { get; set; }
        public List<WebKontenModel> Biaya { get; set; }
        public List<WebKontenModel> Pengumuman { get; set; }
    }
    public class WebPpdbListInfoImageModel
    {
        public List<WebKontenImageModel> Aturan { get; set; }
        public List<WebKontenImageModel> Alur { get; set; }
        public List<WebKontenImageModel> Biaya { get; set; }
        public List<WebKontenImageModel> Pengumuman { get; set; }
    }
}
