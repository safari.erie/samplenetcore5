﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace Swg.Sch.Shared.ViewModels
{
    public class HariLiburModel
    {
        public int IdHariLibur { get; set; }
        public string Tanggal { get; set; }
        public string TanggalEnd { get; set; }
        public string Nama { get; set; }
    }
    public class HariLiburUnitModel
    {
        public int IdUnit { get; set; }
        public string NamaUnit { get; set; }
        public List<HariLiburModel> AssignLiburs { get; set; }
        public List<HariLiburModel> NoAssignLiburs { get; set; }
    }
}
