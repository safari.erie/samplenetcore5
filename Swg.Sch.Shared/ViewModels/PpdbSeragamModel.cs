using System;
using System.Collections.Generic;
namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbSeragamModel
    {
        public int IdPpdbSeragam { get; set; }
        public int IdUnit { get; set; }
        public string Nama { get; set; }
        public string NamaSingkat { get; set; }
    }

    public class PpdbSeragamListModel : PpdbSeragamModel
    {
        public string Unit { get; set; }
    }
}
