﻿using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class SekolahVisiMisiModel : SekolahVisiMisiEditModel
    {
        public string JenisVisiMisi { get; set; }
    }
    public class SekolahVisiMisiAddModel
    {
        public int IdJenisVisiMisi { get; set; }
        public int IdSekolah { get; set; }
        public string FileIkon { get; set; }
        public string Judul { get; set; }
        public string Konten { get; set; }
    }
    public class SekolahVisiMisiEditModel : SekolahVisiMisiAddModel
    {
        public int IdSekolahVisiMisi { get; set; }
    }
}
