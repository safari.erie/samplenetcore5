using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace Swg.Sch.Shared.ViewModels
{
    public class TestimoniDefaultModel
    {
        public int IdTestimoni { get; set; }
        public string Nama { get; set; }
        public string Sebagai { get; set; }
        public string Pesan { get; set; }
        public string FotoUser { get; set; }
        public string Url { get; set; }
        public int Status { get; set; }
        public string CreatedDate { get; set; }
    }
    public class TestimoniModel : TestimoniDefaultModel
    {
        public string StrStatus { get; set; }
    }
    // public class TestimoniAddEditModel : TestimoniDefaultModel
    // {
    //     public IFormFile FileImage { get; set; }
    // }

    public class WebTestimoniAddModel
    {
        public string Nama { get; set; }
        public string Sebagai { get; set; }
        public string Pesan { get; set; }
        public IFormFile FotoUser { get; set; }
        public string Url { get; set; }
    }

}
