using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class UnitBannerModel
    {
        public int IdUnitBanner { get; set; }
        public int IdUnit { get; set; }
    }
    public class UnitBannerReadModel : UnitBannerModel
    {
        public string Unit { get; set; }
        public string FileImage { get; set; }
    }
    public class UnitBannerAddEditModel : UnitBannerModel
    {
        public IFormFile FileImage { get; set; }
    }
}
