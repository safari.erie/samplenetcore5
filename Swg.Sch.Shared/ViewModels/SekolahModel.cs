﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class SekolahModel
    {
        public int IdSekolah { get; set; }
        public string Nama { get; set; }
        public string NamaSingkat { get; set; }
        public string Alamat { get; set; }
        public string KodePos { get; set; }
        public string Desa { get; set; }
        public string Kecamatan { get; set; }
        public string Kabupaten { get; set; }
        public string Provinsi { get; set; }
        public string NoTelepon { get; set; }
        public string NoFaximili { get; set; }
        public string Email { get; set; }
        public string Web { get; set; }
        public string Moto { get; set; }

        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public string Youtube { get; set; }
        public string Profile { get; set; }
        public string Video { get; set; }


        public static implicit operator List<object>(SekolahModel v)
        {
            throw new NotImplementedException();
        }
    }

    public class SekolahReadModel : SekolahModel
    {
        public string Logo { get; set; }
        public string Icon { get; set; }
        public string FotoWeb { get; set; }
    }
    public class SekolahAddEditModel : SekolahModel
    {
        public IFormFile Logo { get; set; }
        public IFormFile Icon { get; set; }
        public IFormFile FotoWeb { get; set; }

    }
}
