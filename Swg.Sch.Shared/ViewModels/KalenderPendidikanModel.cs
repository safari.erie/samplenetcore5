using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace Swg.Sch.Shared.ViewModels
{
    public class KalenderPendidikanDefaultModel
    {
        public int IdKalenderPendidikan { get; set; }
        public int Semester { get; set; }
        public string Keterangan { get; set; }
    }
    public class KalenderPendidikanModel : KalenderPendidikanDefaultModel
    {
        public string FileImage { get; set; }
    }
    public class KalenderPendidikanAddEditModel : KalenderPendidikanDefaultModel
    {
        public IFormFile FileImage { get; set; }
    }
}
