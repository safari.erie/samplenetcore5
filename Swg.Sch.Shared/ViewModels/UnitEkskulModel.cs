using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class UnitEkskulModel
    {
        public int IdUnitEkskul { get; set; }
        public int IdUnit { get; set; }
        public string Judul { get; set; }
        public string Deskripsi { get; set; }
    }
    public class UnitEkskulReadModel : UnitEkskulModel
    {

        public string FileImage { get; set; }
        public string Status { get; set; }
        public string Unit { get; set; }
        public List<ActionButtonModel> ActionButton { get; set; }
    }
    public class UnitEkskulAddEditModel : UnitEkskulModel
    {
        public IFormFile FileImage { get; set; }
    }
}
