﻿using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class SiswaRaporModel
    {
        public int IdSiswa { get; set; }
        public string NamaSiswa { get; set; }
        public int IdJenisRapor { get; set; }
        public string FileRapor { get; set; }
        public string JenisRapor { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
    }
    public class SiswaRaporAddModel
    {
        public int IdSiswa { get; set; }
        public int IdJenisRapor { get; set; }
        public string FileRapor { get; set; }

    }

    public class JenisRaporModel
    {
        public int IdJenisRapor { get; set; }
        public string Nama { get; set; }
        public string NamaSingkat { get; set; }
    }

    public class TaSiswaRaporModel
    {
        public int IdTahunAjaran { get; set; }
        public string Nis { get; set; }
        public string JenisRapor { get; set; }
        public string FileRapor { get; set; }
    }
}
