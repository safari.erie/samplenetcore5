using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbDaftarPeminat
    {
        public string NamaLengkap { get; set; }
        public string Kelas { get; set; }
        public string NamaAyah { get; set; }
        public string NamaIbu { get; set; }
        public string Email { get; set; }
        public string NomorHp { get; set; }
        public string CreatedDate { get; set; }
    }
    public class AddPpdbDaftarPeminat
    {
        [Required]
        public string NamaLengkap { get; set; }
        [Required]
        public string Kelas { get; set; }
        [Required]
        public string NamaAyah { get; set; }
        [Required]
        public string NamaIbu { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string NomorHp { get; set; }
    }
}