﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.ViewModels
{
    public class HakAksesModel
    {
        public int IdHakAkses { get; set; }
        public string NamaHakAkses { get; set; }
    }
    public class JabatanHakAksesModel
    {
        public int IdJabatan { get; set; }
        public string NamaJabatan { get; set; }
        public List<HakAksesModel> HakAksess { get; set; }
        public List<HakAksesModel> NoHakAksess { get; set; }
    }
}
