﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbDaftarModel : PpdbDaftarAddModel
    {
        public int IdPpdbDaftar { get; set; }
        public int IdUnit { get; set; }
        public string Pin { get; set; }
        public string Status { get; set; }
        public string Sekolah { get; set; }
        public string Unit { get; set; }
        public string Kelas { get; set; }
        public string Peminatan { get; set; }
        public string JenisKategoriPendaftaran { get; set; }
        public string JenisPendaftaran { get; set; }
        public string JalurPendaftaran { get; set; }
    }
    public class PpdbDaftarAddModel
    {
        public int IdPpdbKelas { get; set; }
        public int IdJenisKategoriPendaftaran { get; set; }
        public int IdJenisPendaftaran { get; set; }
        public int IdJalurPendaftaran { get; set; }
        public string NisSiswa { get; set; }
        public string NisSodara { get; set; }
        public string NikOrtu { get; set; }
        public string Nama { get; set; }
        public string KdJenisKelamin { get; set; }
        public string TempatLahir { get; set; }
        public string TanggalLahir { get; set; }
        public string Email { get; set; }
        public string NoHandphone { get; set; }
    }
}
