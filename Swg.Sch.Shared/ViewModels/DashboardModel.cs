﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace Swg.Sch.Shared.ViewModels
{
    public class DashboardModel
    {
        public int IdUnit { get; set; }
        public string NamaUnit { get; set; }
        public int PstGuruIsiMateri { get; set; }
        public int PstGuruIsiNilaiSiswa { get; set; }
        public int PstSiswaTugasSelesai { get; set; }
        public int PstSiswaTidakHadir { get; set; }
        public int PstSiswaIsiMutabaah { get; set; }
    }
    #region ppdb
    public class DashboardPppdbModel
    {
        public string TahunAjaran { get; set; }
        public string LastUpdate { get; set; }
        public List<DashboardPpdbKelasModel> Daftars { get; set; }
        public List<DashboardPpdbKelasModel> Vas { get; set; }
        public List<DashboardPpdbKelasModel> Observasis { get; set; }
        public List<DashboardPpdbKelasModel> Bayars { get; set; }
    }
    public class DashboardPpdbKelasModel
    {
        public string Judul { get; set; }
        public List<DashboardSiswaModel> Infos { get; set; }
    }
    public class DashboardSiswaModel
    {
        public string Unit { get; set; }
        public string Kelas { get; set; }
        public int Total { get; set; }
        public int JumlahL { get; set; }
        public int JumlahP { get; set; }
    }
    #endregion
}
