﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Swg.Sch.Shared.ViewModels
{
    public class KeuPtgjwbListModel : KeuPengajuanListModel
    {
        public string TanggalDilaporkan { get; set; }
        public double TotalRpDilaporkan { get; set; }
    }
    public class KeuPtgjwbCoaListModel
    {
        public int IdPtgjwbCoa { get; set; }
        public int IdCoa { get; set; }
        public int IdJenisSatuan { get; set; }
        public int IdJenisVolume { get; set; }
        public string KodeCoa { get; set; }
        public string NamaCoa { get; set; }
        public double JumlahSatuan { get; set; }
        public string Satuan { get; set; }
        public double JumlahVolume { get; set; }
        public string Volume { get; set; }
        public double HargaSatuan { get; set; }
        public double Rp { get; set; }
        public string Catatan { get; set; }
        public string UrlFileBukti { get; set; }
        public List<string> Aksis { get; set; }
    }

    public class KeuPtgjwbCoaAddModel
    {
        public int IdPengajuan { get; set; }
        public int IdCoa { get; set; }
        public int IdJenisSatuan { get; set; }
        public double JumlahSatuan { get; set; }
        public int IdJenisVolume { get; set; }
        public double JumlahVolume { get; set; }
        public string Tanggal { get; set; }
        public double HargaSatuan { get; set; }
        public string Catatan { get; set; }
        public IFormFile FileUpload { get; set; }
    }
    public class KeuPtgjwbCoaEditModel
    {
        public int IdPtgjwbCoa { get; set; }
        public int IdJenisSatuan { get; set; }
        public double JumlahSatuan { get; set; }
        public int IdJenisVolume { get; set; }
        public double JumlahVolume { get; set; }
        public string Tanggal { get; set; }
        public double HargaSatuan { get; set; }
        public string Catatan { get; set; }
        public IFormFile FileUpload { get; set; }
    }
    public class KeuPtgjwbDownloadModel
    {
        public string NamaUnit { get; set; }
        public string TahunAjaran { get; set; }
        public string Program { get; set; }
        public string Kegiatan { get; set; }
        public string Nomor { get; set; }
        public string Tanggal { get; set; }
        public double TotalRp { get; set; }
        public string Status { get; set; }
        public List<KeuPtgjwbDetDownloadModel> Det { get; set; }
    }
    public class KeuPtgjwbDetDownloadModel
    {
        public int IdCoa { get; set; }
        public int? IdParentCoa { get; set; }
        public string Kode { get; set; }
        public string Nama { get; set; }
        public double JumlahSatuan { get; set; }
        public string Satuan { get; set; }
        public double JumlahVolume { get; set; }
        public string Volume { get; set; }
        public double HargaSatuan { get; set; }
        public double TotalRp { get; set; }
        public string Catatan { get; set; }
        public string UrlFileBukti { get; set; }
    }
}
