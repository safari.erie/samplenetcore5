﻿using Microsoft.AspNetCore.Http;
using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class UnitGaleriModel
    {
        public int IdUnitGaleri { get; set; }
        public string JenisGaleri { get; set; }
        public int Jenis { get; set; }
        public string NamaJenis { get; set; }
        public int Status { get; set; }
        public string NamaStatus { get; set; }
        public string UrlVideo { get; set; }
        public int IdUnit { get; set; }
        public string Unit { get; set; }
        public int IdJenisGaleri { get; set; }
        public string Judul { get; set; }
        public string Keterangan { get; set; }
    }
    public class UnitGaleriReadModel : UnitGaleriModel
    {
        public string FileUrl { get; set; }
    }
    public class UnitGaleriAddEditModel : UnitGaleriModel
    {
        public IFormFile FileUrl { get; set; }
    }
}
