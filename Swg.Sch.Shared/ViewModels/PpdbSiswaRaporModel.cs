﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbSiswaRaporModel : PpdbSiswaRaporAddModel
    {
        public int IdPpdbDaftar { get; set; }
        public string FileRapor { get; set; }
    }
    public class PpdbSiswaRaporAddModel
    {
        public int Semester { get; set; }
        public string Mapel { get; set; }
        public double Nilai { get; set; }
    }
}
