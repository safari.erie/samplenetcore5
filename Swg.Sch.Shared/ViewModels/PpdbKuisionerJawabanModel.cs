﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbKuisionerJawabanModel : PpdbKuisionerJawabanAddModel
    {
        public int IdPpdbDaftar { get; set; }
        public string JenisPertanyaan { get; set; }
        public string Pertanyaan { get; set; }
    }

    public class PpdbKuisionerJawabanAddModel
    {
        public int IdPpdbKuisionerPertanyaan { get; set; }
        public string OpsiJawaban { get; set; }
        public string Jawaban { get; set; }

    }
    public class PpdbKuisionerExcelModel
    {
        public int IdPpdbDaftar { get; set; }
        public string Nama { get; set; }
        public string NamaAyah { get; set; }
        public string NamaIbu { get; set; }
        public string AsalSekolah { get; set; }
        public string NamaUnit { get; set; }
        public string Peminatan { get; set; }
        public string NamaKelas { get; set; }
        public int? IdPegawai { get; set; }
        public string JenisKategoriPendaftaran { get; set; }
        public List<PpdbKuisionerJawabanModel> Jawabans { get; set; }
    }
}
