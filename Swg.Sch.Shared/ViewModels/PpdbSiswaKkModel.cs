using System;
public class PpdbSiswaKkModel
{
    public int IdPpdbDaftar { get; set; }
    public string NoKk { get; set; }
    public string Alamat { get; set; }
    public string Rt { get; set; }
    public string Rw { get; set; }
    public string Dusun { get; set; }
    public Int64 IdWilDesa { get; set; }
    public string KdPos { get; set; }
    public string NoTelp { get; set; }
    public double Longitude { get; set; }
    public double Latitude { get; set; }
}