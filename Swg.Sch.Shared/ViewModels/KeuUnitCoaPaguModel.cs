﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Sch.Shared.ViewModels
{
    public class KeuUnitCoaPaguModel
    {
        public int IdUnitCoaPagu { get; set; }
        public int IdUnit { get; set; }
        public int IdTahunAjaran { get; set; }
        public int IdCoaRenPen { get; set; }
        public double RpPagu { get; set; }
        public double RpPaguUsed { get; set; }
        public string Unit { get; set; }
        public string TahunAjaran { get; set; }
        public string NamaCoaRenPen { get; set; }
    }
}
