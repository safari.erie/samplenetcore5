﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Swg.Models;

namespace Swg.Sch.Shared.ViewModels
{
    public class SiswaListModel
    {
        public string Nis { get; set; }
        public string Nisn { get; set; }
        public string Nik { get; set; }
        public string Nama { get; set; }
        public string NamaPanggilan { get; set; }
        public string TempatLahir { get; set; }
        public string TanggalLahir { get; set; }
        public string Agama { get; set; }
        public string JenisKelamin { get; set; }
        public string NoHandphone { get; set; }
        public string NoDarurat { get; set; }
        public string Email { get; set; }
        public string AlamatTinggal { get; set; }
        public string AlamatOrtu { get; set; }
        public string NikIbu { get; set; }
        public string NamaIbu { get; set; }
        public string PekerjaanIbu { get; set; }
        public string NamaInstansiIbu { get; set; }
        public string NikAyah { get; set; }
        public string NamaAyah { get; set; }
        public string PekerjaanAyah { get; set; }
        public string NamaInstansiAyah { get; set; }
        public string EmailOrtu { get; set; }
        public string Unit { get; set; }
        public string Kelas { get; set; }
        public string KelasParalel { get; set; }
        public int Status { get; set; }
        public string StrStatus { get; set; }
        public int IdSiswa { get; set; }
        public int IdUnit { get; set; }
        public int IdKelas { get; set; }
        public int IdKelasParalel { get; set; }
        public string NisLama { get; set; }
        public string Password { get; set; }
        public string Pin { get; set; }
    }
    public class SiswaModel : SiswaEditModel
    {
        public string FotoProfile { get; set; }
        public string NisLama { get; set; }
        public int Status { get; set; }
        public string Agama { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisPekerjaanIbu { get; set; }
        public string JenisPekerjaanAyah { get; set; }
        public string StrStatus { get; set; }
        public string KelasParalel { get; set; }
        public string Kelas { get; set; }
        public string Unit { get; set; }
        public double TotalTagihan2 { get; set; }
        public double TotalTagihan1 { get; set; }
        public double TotalTagihan { get; set; }
        public List<SiswaTagihanDetilModel> Tagihans2 { get; set; }
        public List<SiswaTagihanDetilModel> Tagihans1 { get; set; }
        public List<SiswaRaporModel> Rapors { get; set; }
        public KelasQuranSiswaModel KelasQuran { get; set; }

    }
    public class SiswaAddModel
    {
        public string Nis { get; set; }
        public string Nisn { get; set; }
        public string Nik { get; set; }
        public string Nama { get; set; }
        public string NamaPanggilan { get; set; }
        public string TempatLahir { get; set; }
        public string TanggalLahir { get; set; }
        public int? IdJenisPrestasi { get; set; }
        public int IdAgama { get; set; }
        public string KdJenisKelamin { get; set; }
        public string NoHandphone { get; set; }
        public string NoDarurat { get; set; }
        public string Email { get; set; }
        public string AlamatTinggal { get; set; }
        public string AlamatOrtu { get; set; }
        public string NikIbu { get; set; }
        public string NamaIbu { get; set; }
        public int IdJenisPekerjaanIbu { get; set; }
        public string NamaInstansiIbu { get; set; }
        public string NikAyah { get; set; }
        public string NamaAyah { get; set; }
        public int IdJenisPekerjaanAyah { get; set; }
        public string NamaInstansiAyah { get; set; }
        public string EmailOrtu { get; set; }
        public int IdKelasParalel { get; set; }
        public int IdKelas { get; set; }
        public int IdUnit { get; set; }
    }
    public class SiswaEditModel : SiswaAddModel
    {
        public int IdSiswa { get; set; }
    }

    public class SiswaAkunModel
    {
        public string Nama { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Pin { get; set; }
    }
    public class SiswaEditProfileModel
    {
        public string Nisn { get; set; }
        public string Nik { get; set; }
        public string NamaPanggilan { get; set; }
        public string TempatLahir { get; set; }
        public int IdAgama { get; set; }
        public string KdJenisKelamin { get; set; }
        public string NoHandphone { get; set; }
        public string NoDarurat { get; set; }
        public string Email { get; set; }
        public string AlamatTinggal { get; set; }
        public string AlamatOrtu { get; set; }
        public string NikIbu { get; set; }
        public string NamaIbu { get; set; }
        public int IdJenisPekerjaanIbu { get; set; }
        public string NamaInstansiIbu { get; set; }
        public string NikAyah { get; set; }
        public string NamaAyah { get; set; }
        public int IdJenisPekerjaanAyah { get; set; }
        public string NamaInstansiAyah { get; set; }
        public string EmailOrtu { get; set; }
        public IFormFile FotoProfile { get; set; }
        public string NoAktaLahir { get; set; }
        public IFormFile FileKk { get; set; }
        public IFormFile FileAktaLahir { get; set; }
        public string NoKip { get; set; }
        public IFormFile FileKip { get; set; }
        public string KdGolonganDarah { get; set; }
        public int IdKewarganegaraan { get; set; }
        public int IdBahasa { get; set; }
        public int TinggiBadan { get; set; }
        public int BeratBadan { get; set; }
        public double LingkarKepala { get; set; }
        public int AnakKe { get; set; }
        public int JumlahSodaraKandung { get; set; }
        public int JumlahSodaraTiri { get; set; }
        public int JumlahSodaraAngkat { get; set; }
        public int IdTransportasi { get; set; }
        public int JarakKeSekolah { get; set; }
        public double WaktuTempuh { get; set; }
        public string PenyakitDiderita { get; set; }
        public string KelainanJasmani { get; set; }
        public string SekolahAsal { get; set; }
        public string AlamatSekolahAsal { get; set; }
        public string NspnSekolahAsal { get; set; }
        public bool PernahTkFormal { get; set; }
        public bool PernahTlInformal { get; set; }
        public int IdHobi { get; set; }
        public int IdCita2 { get; set; }
        public string NoUjian { get; set; }
        public string NoIjazah { get; set; }
        public IFormFile FileIjazah { get; set; }
        public string NoShusbn { get; set; }
        public IFormFile FileShusbn { get; set; }
        public string KelasDapodik { get; set; }
        public string BerkebutuhanKhusus { get; set; }
        public string AlamatKk { get; set; }
        public string PunyaWali { get; set; }
        public string TanggalLahirAyah { get; set; }
        public string BerkebutuhanKhususAyah { get; set; }
        public string PendidikanTerakhirAyah { get; set; }
        public string PenghasilanAyah { get; set; }
        public string NomorHpAyah { get; set; }
        public string TanggalLahirIbu { get; set; }
        public string BerkebutuhanKhususIbu { get; set; }
        public string PendidikanTerakhirIbu { get; set; }
        public string PenghasilanIbu { get; set; }
        public string NomorHpIbu { get; set; }
        public string NikWali { get; set; }
        public string NamaWali { get; set; }
        public string AlamatWali { get; set; }
        public int IdJenisPekerjaanWali { get; set; }
        public string NamaInstansiWali { get; set; }
        public string EmailWali { get; set; }
        public string NoHandphoneWali { get; set; }
        public bool TerimaKip { get; set; }
        public string AlasanKip { get; set; }
        public string NoKk { get; set; }
        public string Alamat { get; set; }
        public string Rt { get; set; }
        public string Rw { get; set; }
        public string Dusun { get; set; }
        public Int64 IdWilDesa { get; set; }
        public string KdPos { get; set; }
        public string NoTelp { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
    public class SiswaViewProfileModel
    {
        public int IdSiswa { get; set; }
        public string KelasParalel { get; set; }
        public string Nis { get; set; }
        public string Nisn { get; set; }
        public string Nik { get; set; }
        public string Nama { get; set; }
        public string NamaPanggilan { get; set; }
        public string TempatLahir { get; set; }
        public string TanggalLahir { get; set; }
        public ComboModel Agama { get; set; }
        public ComboModel JenisKelamin { get; set; }
        public string NoHandphone { get; set; }
        public string NoDarurat { get; set; }
        public string Email { get; set; }
        public string AlamatTinggal { get; set; }
        public string AlamatOrtu { get; set; }
        public string NikIbu { get; set; }
        public string NamaIbu { get; set; }
        public ComboModel JenisPekerjaanIbu { get; set; }
        public string NamaInstansiIbu { get; set; }
        public string NikAyah { get; set; }
        public string NamaAyah { get; set; }
        public ComboModel JenisPekerjaanAyah { get; set; }
        public string NamaInstansiAyah { get; set; }
        public string EmailOrtu { get; set; }
        public string FotoProfile { get; set; }

        public string NoAktaLahir { get; set; }
        public string FileKk { get; set; }
        public string FileAktaLahir { get; set; }
        public string NoKip { get; set; }
        public string FileKip { get; set; }
        public ComboModel GolonganDarah { get; set; }
        public ComboModel Kewarganegaraan { get; set; }
        public ComboModel Bahasa { get; set; }
        public int TinggiBadan { get; set; }
        public int BeratBadan { get; set; }
        public double LingkarKepala { get; set; }
        public int AnakKe { get; set; }
        public int JumlahSodaraKandung { get; set; }
        public int JumlahSodaraTiri { get; set; }
        public int JumlahSodaraAngkat { get; set; }
        public ComboModel Transportasi { get; set; }
        public int JarakKeSekolah { get; set; }
        public double WaktuTempuh { get; set; }
        public string PenyakitDiderita { get; set; }
        public string KelainanJasmani { get; set; }
        public string SekolahAsal { get; set; }
        public string AlamatSekolahAsal { get; set; }
        public string NspnSekolahAsal { get; set; }
        public bool PernahTkFormal { get; set; }
        public bool PernahTlInformal { get; set; }
        public ComboModel Hobi { get; set; }
        public ComboModel Cita2 { get; set; }
        public string NoUjian { get; set; }
        public string NoIjazah { get; set; }
        public string FileIjazah { get; set; }
        public string NoShusbn { get; set; }
        public string FileShusbn { get; set; }
        public string KelasDapodik { get; set; }
        public string BerkebutuhanKhusus { get; set; }
        public string AlamatKk { get; set; }
        public string PunyaWali { get; set; }
        public string TanggalLahirAyah { get; set; }
        public string BerkebutuhanKhususAyah { get; set; }
        public string PendidikanTerakhirAyah { get; set; }
        public string PenghasilanAyah { get; set; }
        public string NomorHpAyah { get; set; }
        public string TanggalLahirIbu { get; set; }
        public string BerkebutuhanKhususIbu { get; set; }
        public string PendidikanTerakhirIbu { get; set; }
        public string PenghasilanIbu { get; set; }
        public string NomorHpIbu { get; set; }
        public string NikWali { get; set; }
        public string NamaWali { get; set; }
        public string AlamatWali { get; set; }
        public int IdJenisPekerjaanWali { get; set; }
        public string NamaInstansiWali { get; set; }
        public string EmailWali { get; set; }
        public string NoHandphoneWali { get; set; }
        public bool TerimaKip { get; set; }
        public string AlasanKip { get; set; }
        public string NoKk { get; set; }
        public string Alamat { get; set; }
        public string Rt { get; set; }
        public string Rw { get; set; }
        public string Dusun { get; set; }
        public ComboModel WilProv { get; set; }
        public ComboModel WilKabkot { get; set; }
        public ComboModel WilKec { get; set; }
        public ComboModel WilDesa { get; set; }
        public string KdPos { get; set; }
        public string NoTelp { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
    public class SheetSiswaViewProfileModel
    {
        public string Judul { get; set; }
        public string DownloadOleh { get; set; }
        public string Unit { get; set; }
        public List<SiswaViewProfileModel> ListData { get; set; }
    }
}
