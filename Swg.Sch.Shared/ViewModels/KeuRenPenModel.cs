﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Sch.Shared.ViewModels
{
    public class KeuUnitRenPenModel
    {
        public int IdUnitRenPen { get; set; }
        public string Unit { get; set; }
        public string TahunAjaran { get; set; }
        public string Status { get; set; }
        public double Rp { get; set; }
    }
    public class KeuUnitRenPenListModel : KeuUnitRenPenModel
    {
        public List<string> Aksis { get; set; }
    }
    public class KeuUnitRenPenCoaModel
    {
        public int IdUnitRenPen { get; set; }
        public int IdCoa { get; set; }
        public string KodeCoa { get; set; }
        public string NamaCoa { get; set; }
        public double Rp { get; set; }
    }
    public class KeuUnitRenPenCoaListModel : KeuUnitRenPenCoaModel
    {
        public List<string> Aksis { get; set; }
    }
    public class KeuUnitRenPenDownloadModel
    {
        public string NamaUnit { get; set; }
        public string TahunAjaran { get; set; }
        public string Nomor { get; set; }
        public double TotalRp { get; set; }
        public string Status { get; set; }
        public List<KeuUnitRenPenDetDownloadModel> Det { get; set; }
    }
    public class KeuUnitRenPenDetDownloadModel
    {
        public int IdCoa { get; set; }
        public int? IdParentCoa { get; set; }
        public string KodeCoa { get; set; }
        public string NamaCoa { get; set; }
        public double Rp { get; set; }
        public List<KeuUnitRenPenDetDownloadModel> Det { get; set; }
    }
}
