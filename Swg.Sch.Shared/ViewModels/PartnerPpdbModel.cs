using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class PartnerPpdbModel
    {
        public string NoVa { get; set; }
        public string Nama { get; set; }
        public string TanggalLahir { get; set; }
        public string Unit { get; set; }
        public string Kelas { get; set; }
        public string Status { get; set; }
        public double RpTotal { get; set; }
        public string Catatan { get; set; }
        public List<PartnerTagPpdbModel> TagPpdb { get; set; }
    }
    public class PartnerTagPpdbModel
    {
        public string JenisTagihan { get; set; }
        public double Rp { get; set; }
    }
    public class PartnerPpdbTempModel
    {
        public string RefNo { get; set; }
    }
}
