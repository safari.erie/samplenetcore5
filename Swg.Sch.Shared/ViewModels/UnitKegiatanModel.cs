﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class UnitKegiatanModel
    {
        public int IdUnitKegiatan { get; set; }
        public int IdUnit { get; set; }
        public string Unit { get; set; }
        public string Judul { get; set; }
        public string Keterangan { get; set; }
        public string Url { get; set; }
        public int Status { get; set; }
        public string NamaStatus { get; set; }
        public int Jenis { get; set; }
        public string NamaJenis { get; set; }
    }
    public class UnitKegiatanReadModel : UnitKegiatanModel
    {
        public string FileImage { get; set; }
        public List<ActionButtonModel> ActionButton { get; set; }
    }
    public class ActionButtonModel 
    {
        public bool ButtonEdit { get; set; }
        public bool ButtonReject { get; set; }
        public bool ButtonDelete { get; set; }
    }
    
    public class UnitKegiatanAddEditModel : UnitKegiatanModel
    {
        public IFormFile FileImage { get; set; }
    }
}
