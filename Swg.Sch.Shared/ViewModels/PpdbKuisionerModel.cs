using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbKuisionerModel
    {
        public int IdPpdbKuisioner { get; set; }
        public int IdUnit { get; set; }
        public int IdJenisPendaftaran { get; set; }
        public int IdJalurPendaftaran { get; set; }
        public int IdJenisPertanyaan { get; set; }
        public string JenisPertanyaan { get; set; }
        public string Pertanyaan { get; set; }
    }
}