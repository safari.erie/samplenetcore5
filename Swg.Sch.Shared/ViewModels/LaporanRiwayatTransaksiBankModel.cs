using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class LaporanRiwayatTransaksiBankModel
    {
        public string KodeBayar { get; set; }
        public string Tipe { get; set; }
        public string Nama { get; set; }
        public string Kelas { get; set; }
        public double JumlahBayar { get; set; }
        public string Terbilang { get; set; }
        public string Catatan { get; set; }
        public string KeteranganGagal { get; set; }
        public string Status { get; set; }
        public string Tanggal { get; set; }

    }
}
