﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class KelasQuranListModel
    {
        public int IdKelasQuran { get; set; }
        public string Tanggal { get; set; }
        public string Hari { get; set; }
        public string Kelompok { get; set; }
        public int JumlahSiswa { get; set; }
        public int JumlahInput { get; set; }
        public int JumlahNoInput { get; set; }
    }
    public class KelasQuranSiswaListModel
    {
        public int IdKelasQuran { get; set; }
        public string Tanggal { get; set; }
        public int IdSiswa { get; set; }
        public string NamaSiswa { get; set; }
        public string NamaKelasParalel { get; set; }
        public int? IdJenisJilidQuran { get; set; }
        public string JilidQuran { get; set; }
        public int? Halaman { get; set; }
        public string Catatan { get; set; }
        public string StatusUjian { get; set; }
    }
    public class KelasQuranSiswaProgresModel
    {
        public string Tanggal { get; set; }
        public string Hari { get; set; }
        public int IdJenisJilidQuran { get; set; }
        public string JilidQuran { get; set; }
        public string NamaKelasQuran { get; set; }
        public string NamaGuru { get; set; }
        public int IdSiswa { get; set; }
        public string NamaSiswa { get; set; }
        public int Halaman { get; set; }
        public string Catatan { get; set; }
    }

    public class KelasQuranSiswaUjianModel
    {
        public int IdKelasQuran { get; set; }
        public string NamaKelasQuran { get; set; }
        public string NamaGuru { get; set; }
        public int IdSiswa { get; set; }
        public int IdJenisJilidQuran { get; set; }
        public string JilidQuran { get; set; }
        public string Tanggal { get; set; }
        public string Hasil { get; set; }
        public string Nilai { get; set; }
        public string Catatan { get; set; }
    }
    //belum dipakai
    public class KelasQuranSiswaModel
    {
        public string Nama { get; set; }
        public List<KelasQuranSiswaProgresListModel> Progress { get; set; }
        public List<KelasQuranSiswaUjianListModel> Ujians { get; set; }
    }
    public class KelasQuranSiswaProgresListModel
    {
        public string Tanggal { get; set; }
        public string Hari { get; set; }
        public string Jilid { get; set; }
        public int Halaman { get; set; }
        public string Kelompok { get; set; }
        public string NamaGuru { get; set; }
    }

    public class KelasQuranSiswaUjianListModel
    {
        public string Tanggal { get; set; }
        public string Hari { get; set; }
        public string Jilid { get; set; }
        public string Hasil { get; set; }
        public string Nilai { get; set; }
        public string Catatan { get; set; }
    }
}
