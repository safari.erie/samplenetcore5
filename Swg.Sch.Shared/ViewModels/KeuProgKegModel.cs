﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Sch.Shared.ViewModels
{
    public class KeuUnitModel
    {
        public int IdUnitProKeg { get; set; }
        public string Unit { get; set; }
        public string TahunAjaran { get; set; }
        public string Status { get; set; }
        public string StatusProses { get; set; }
        public string Catatan { get; set; }
        public double Rp { get; set; }
    }
    public class KeuUnitListModel : KeuUnitModel
    {
        public List<string> Aksis { get; set; }
    }
    public class KeuUnitProModel
    {
        public int IdUnitProKeg { get; set; }
        public int IdProgram { get; set; }
        public string Program { get; set; }
        public int JumlahKegiatanWajib { get; set; }
        public double RpKegiatanWajib { get; set; }
        public int JumlahKegiatanTambahan { get; set; }
        public double RpKegiatanTambahan { get; set; }
        public double RpTotal { get; set; }
    }
    public class KeuUnitProListModel : KeuUnitProModel
    {
        public List<string> Aksis { get; set; }
    }
    public class KeuUnitProKegModel
    {
        public int? IdUnitProkegWajib { get; set; }
        public int? IdUnitProkegTambahan { get; set; }
        public string Jenis { get; set; }
        public string Kegiatan { get; set; }
        public string Tanggal { get; set; }
        public double Rp { get; set; }
    }
    public class KeuUnitProKegListModel : KeuUnitProKegModel
    {
        public List<string> Aksis { get; set; }
    }

    public class KeuUnitProKegCoaListModel
    {
        public int IdCoa { get; set; }
        public int IdJenisSatuan { get; set; }
        public int IdJenisVolume { get; set; }
        public string KodeCoa { get; set; }
        public string NamaCoa { get; set; }
        public double JumlahSatuan { get; set; }
        public string Satuan { get; set; }
        public double JumlahVolume { get; set; }
        public string Volume { get; set; }
        public double HargaSatuan { get; set; }
        public double Rp { get; set; }
        public string Catatan { get; set; }
    }
    public class KeuUnitProKegWajibCoaListModel : KeuUnitProKegCoaListModel
    {
        public int IdUnitProkegWajibCoa { get; set; }
        public List<string> Aksis { get; set; }
    }
    public class KeuUnitProKegTambahanCoaListModel : KeuUnitProKegCoaListModel
    {
        public int IdUnitProkegTambahanCoa { get; set; }
        public List<string> Aksis { get; set; }
    }
    public class KeuUnitProKegCoaAddModel
    {
        public int IdCoa { get; set; }
        public int IdJenisSatuan { get; set; }
        public double JumlahSatuan { get; set; }
        public int IdJenisVolume { get; set; }
        public double JumlahVolume { get; set; }
        public double HargaSatuan { get; set; }
        public string Catatan { get; set; }
    }
    public class KeuUnitProKegCoaWajibAddModel : KeuUnitProKegCoaAddModel
    {
        public int IdUnitProkegWajib { get; set; }
    }
    public class KeuUnitProKegCoaWajibEditModel : KeuUnitProKegCoaWajibAddModel
    {
        public int IdUnitProkegWajibCoa { get; set; }
    }
    public class KeuUnitProKegCoaTambahanAddModel : KeuUnitProKegCoaAddModel
    {
        public int IdUnitProkegTambahan { get; set; }
    }
    public class KeuUnitProKegCoaTambahanEditModel : KeuUnitProKegCoaTambahanAddModel
    {
        public int IdUnitProkegTambahanCoa { get; set; }
    }
    public class KeuUnitProKegDownloadModel
    {
        public string NamaUnit { get; set; }
        public string TahunAjaran { get; set; }
        public string Kecamatan { get; set; }
        public string KabKot { get; set; }
        public string Provinsi { get; set; }
        public double TotalRp { get; set; }
        public string Status { get; set; }
        public List<KeuUnitProKegDetDownloadModel> Det { get; set; }
    }
    public class KeuUnitProKegDetDownloadModel
    {
        public int IdCoa { get; set; }
        public int? IdParentCoa { get; set; }
        public string Kode { get; set; }
        public string Nama { get; set; }
        public double TotalRp { get; set; }

        public string Tanggal { get; set; }
        public double JumlahSatuan { get; set; }
        public string Satuan { get; set; }
        public double JumlahVolume { get; set; }
        public string Volume { get; set; }
        public double HargaSatuan { get; set; }
        public string Catatan { get; set; }
        public List<KeuUnitProKegDetDownloadModel> Det { get; set; }
    }
}
