﻿using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class TahunAjaranModel : TahunAjaranEditModel
    {
        public int Status { get; set; }
        public string StrStatus { get; set; }
    }
    public class TahunAjaranAddModel
    {
        public string Nama { get; set; }
        public string NamaSingkat { get; set; }
        public string TanggalMulai { get; set; }
        public string TanggalSelesai { get; set; }
    }
    public class TahunAjaranEditModel : TahunAjaranAddModel
    {
        public int IdTahunAjaran { get; set; }
    }
    public class SemesterModel
    {
        public int IdSemester { get; set; }
        public string Semester { get; set; }
    }
}
