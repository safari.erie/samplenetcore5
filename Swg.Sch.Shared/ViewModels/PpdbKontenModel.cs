using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbKontenListModel
    {
        public int IdPpdbKonten { get; set; }
        public string JenisPpdbKonten { get; set; }
        public string Deskripsi { get; set; }
    }
    public class PpdbKontenByModel
    {
        public int IdPpdbKonten { get; set; }
        public int IdJenisPpdbKonten { get; set; }
        public string Deskripsi { get; set; }
    }
}