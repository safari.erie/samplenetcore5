﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class SiswaTagihanAddModel
    {
        public string Bulan { get; set; }
        public string KodeBayar { get; set; }
        public string Nis { get; set; }
        public string Nama { get; set; }
        public double Spp1 { get; set; }
        public double Boks1 { get; set; }
        public double Jemputan1 { get; set; }
        public double Katering1 { get; set; }
        public double Ppdb1 { get; set; }
        public double Komite1 { get; set; }
        public double Spp2 { get; set; }
        public double Boks2 { get; set; }
        public double Jemputan2 { get; set; }
        public double Katering2 { get; set; }
        public double Ppdb2 { get; set; }
        public double Komite2 { get; set; }
        public string Catatan { get; set; }
        public string Min { get; set; }
        public string Max { get; set; }
    }
    public class ResumeSiswaTagihanModel : SiswaTagihanAddModel
    {
        public string NamaSiswa { get; set; }
        public string Status { get; set; }
        public string RefNo { get; set; }
        public string Periode { get; set; }
    }
    public class ResumeListSiswaTagihanModel
    {
        public double TotSpp1 { get; set; }
        public double TotBoks1 { get; set; }
        public double TotJemputan1 { get; set; }
        public double TotKatering1 { get; set; }
        public double TotPpdb1 { get; set; }
        public double TotKomite1 { get; set; }
        public double TotAll1 { get; set; }
        public double TotSpp2 { get; set; }
        public double TotBoks2 { get; set; }
        public double TotJemputan2 { get; set; }
        public double TotKatering2 { get; set; }
        public double TotPpdb2 { get; set; }
        public double TotKomite2 { get; set; }
        public double TotAll2 { get; set; }
        public double TotalAll12 { get; set; }
        // public List<ResumeSiswaTagihanModel> SiswaTagihans { get; set; }
    }
}
