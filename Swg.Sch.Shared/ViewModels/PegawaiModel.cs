﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class PegawaiModel : PegawaiEditModel
    {
        public string Unit { get; set; }
        public string JenisPegawai { get; set; }
        public string Email { get; set; }
        public string Nama { get; set; }
        public string JenisKelamin { get; set; }
        public string Alamat { get; set; }
        public string NoTelpon { get; set; }
        public string NoHandphone { get; set; }
        public string Jabatan { get; set; }
        public string Agama { get; set; }
        public string JenjangPendidikan { get; set; }
        public string JabatanFungsional { get; set; }
        public string Status { get; set; }
        public string GolonganPegawai { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string TokenFcm { get; set; }
    }

    public class PegawaiKelasParalelModel
    {
        public int IdKelasParalel { get; set; }
        public string KelasParalel { get; set; }
    }

    public class PegawaiEditModel : PegawaiAddModel
    {
    }
    public class PegawaiAddModel
    {
        public int IdPegawai { get; set; }
        public int IdJabatan { get; set; }
        public string Nip { get; set; }
        public string Nik { get; set; }
        public string TempatLahir { get; set; }
        public string TanggalLahir { get; set; }
        public string KdJenisKelamin { get; set; }
        public int IdAgama { get; set; }
        public int IdJenjangPendidikan { get; set; }
        public string NamaInstitusiPendidikan { get; set; }
        public string NamaPasangan { get; set; }
        public string NoDarurat { get; set; }
        public int IdJenisPegawai { get; set; }
        public string TanggalMasuk { get; set; }
        public int IdGolonganPegawai { get; set; }
        public int IdUnit { get; set; }
        public string NoKk { get; set; }
        public string JurusanPendidikan { get; set; }
        public string AktifitasDakwah { get; set; }
        public string OrganisasiMasyarakat { get; set; }
        public string PengalamanKerja { get; set; }
        public List<PegawaiKelasParalelModel> KelasDiampu { get; set; }
        public List<PegawaiKompetensiModel> Kompetensi { get; set; }
        public string Npwp { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public string Youtube { get; set; }
        public string Sambutan { get; set; }
        public int StatusKawin { get; set; }
        public string StrStatusKawin { get; set; }
        public int JumlahAnak { get; set; }
        public string Gelar { get; set; }
    }
    public class PegawaiKompetensiModel
    {
        public string IdKompetensi { get; set; }
        public string Kompetensi { get; set; }
    }
    public class PegawaiJenisModel
    {
        public int IdPegawai { get; set; }
    }

    public class PegawaiUnitModel
    {
        public int IdPegawai { get; set; }
        public List<PegawaiJenisUnitModel> Unit { get; set; }
    }
    public class PegawaiJenisUnitModel
    {
        public int IdUnit { get; set; }
        public string NamaUnit { get; set; }
    }
    public class PegawaiGajiListModel
    {
        public string Periode { get; set; }
        public int IdPegawai { get; set; }
        public string Nip { get; set; }
        public string Nama { get; set; }
        public double GT_Total { get; set; }
        public double GTT_Total { get; set; }
        public double Pot_Total { get; set; }
        public double Tam_Total { get; set; }
        public double Total { get; set; }
    }
    public class SlipGajiModel
    {
        public string Bulan { get; set; }
        public string Unit { get; set; }
        public string Nip { get; set; }
        public string Nama { get; set; }
        public string PendidikanTerakhir { get; set; }
        public string TanggalMasuk { get; set; }
        public string Golongan { get; set; }
        public string JenisPegawai { get; set; }
        public int Mkg { get; set; }
        public double TakeHomePay { get; set; }
        public string Terbilang { get; set; }
        public List<SlipGajiDetilModel> GajiTetaps { get; set; }
        public List<SlipGajiDetilModel> GajiTidakTetaps { get; set; }
        public List<SlipGajiDetilModel> Potongans { get; set; }
        public List<SlipGajiDetilModel> Tambahans { get; set; }
    }
    public class SlipGajiDetilModel
    {
        public int NoUrut { get; set; }
        public string Keterangan { get; set; }
        public double Rp { get; set; }
    }
}
