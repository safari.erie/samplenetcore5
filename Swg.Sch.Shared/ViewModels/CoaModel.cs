﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.ViewModels
{
    public class CoaModel : CoaAddModel
    {
        public string JenisAkun { get; set; }
        public string JenisTransaksi { get; set; }
    }
    public class CoaAddModel
    {
        public int IdCoa { get; set; }
        public int? IdParentCoa { get; set; }
        public int IdJenisAkun { get; set; }
        public int IdJenisTransaksi { get; set; }
        public string Kode { get; set; }
        public string Nama { get; set; }
    }
}
