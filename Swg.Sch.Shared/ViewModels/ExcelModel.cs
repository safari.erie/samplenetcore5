﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class ExcelSekolahModel
    {
        public string Nama { get; set; }
        public string NamaSingkat { get; set; }
        public string Alamat { get; set; }
        public string KodePos { get; set; }
        public string Desa { get; set; }
        public string Kecamatan { get; set; }
        public string Kabupaten { get; set; }
        public string Provinsi { get; set; }
        public string NoTelepon { get; set; }
        public string NoFaximili { get; set; }
        public string Email { get; set; }
        public string Web { get; set; }
        public string Moto { get; set; }
        public List<SheetUnitModel> XlsUnits { get; set; }
        public List<SheetPegawaiModel> XlsPegawais { get; set; }
        public List<SheetKelasModel> XlsKelas { get; set; }
        public List<SheetMapelModel> XlsMapels { get; set; }
        public List<SheetJadwalKbmModel> XlsKelasMapels { get; set; }
        public List<SheetSiswaModel> XlsSiswas { get; set; }
        public List<SheetJenisEvaluasiHarianModel> XlsJenisEvaluasiHarian { get; set; }
        public List<SheetJenisJilidQuranModel> XlsJenisJilidQuran { get; set; }
        public List<SheetJadwalQuranModel> XlsKelasQurans { get; set; }
        public List<SheetKelQuranSiswaModel> XlsKelasQuranSiswas { get; set; }
    }
    public class SheetUnitModel
    {
        public string Nspn { get; set; }
        public string Nama { get; set; }
        public string NamaSingkat { get; set; }
        public string Alamat { get; set; }
        public string KodePos { get; set; }
        public string Desa { get; set; }
        public string Kecamatan { get; set; }
        public string Kabupaten { get; set; }
        public string Provinsi { get; set; }
        public string NoTelepon { get; set; }
        public string NoFaximili { get; set; }
        public string Email { get; set; }
        public string Web { get; set; }
        public string StatusUnit { get; set; }
        public string WaktuPenyelenggaraan { get; set; }
        public string Kurikulum { get; set; }
        public string Naungan { get; set; }
        public string NoSkPendirian { get; set; }
        public string TanggalSkPendirian { get; set; }
        public string NoSkOperasional { get; set; }
        public string TanggalSkOperasional { get; set; }
        public string JenisAkreditasi { get; set; }
        public string NoSkAkreditasi { get; set; }
        public string TanggalSkAkreditasi { get; set; }
        public string NoSertifikatIso { get; set; }
        public string NamaYayasan { get; set; }
        public double LuasTanah { get; set; }
        public double LuasBangunan { get; set; }
        public string ProviderInternet { get; set; }
        public int BandwidthInternet { get; set; }
        public string SumberListrik { get; set; }
        public int DayaListrik { get; set; }
        public string Moto { get; set; }
    }
    public class SheetSiswaModel
    {
        public string Nis { get; set; }
        public string Nisn { get; set; }
        public string Nik { get; set; }
        public string Nama { get; set; }
        public string NamaPanggilan { get; set; }
        public string TempatLahir { get; set; }
        public string TanggalLahir { get; set; }
        public string Agama { get; set; }
        public string JenisKelamin { get; set; }
        public string NoHandphone { get; set; }
        public string NoDarurat { get; set; }
        public string Email { get; set; }
        public string AlamatTinggal { get; set; }
        public string AlamatOrtu { get; set; }
        public string NikIbu { get; set; }
        public string NamaIbu { get; set; }
        public string JenisPekerjaanIbu { get; set; }
        public string NamaInstansiIbu { get; set; }
        public string NikAyah { get; set; }
        public string NamaAyah { get; set; }
        public string JenisPekerjaanAyah { get; set; }
        public string NamaInstansiAyah { get; set; }
        public string EmailOrtu { get; set; }
        public string Unit { get; set; }
        public string Kelas { get; set; }
        public string KelasParalel { get; set; }
        public string KelasQuran { get; set; }
        public string NisLama { get; set; }
    }
    public class SheetTagihanSiswaModel
    {
        public string Bulan { get; set; }
        public string Nis { get; set; }
        public double Spp1 { get; set; }
        public double Boks1 { get; set; }
        public double Jemputan1 { get; set; }
        public double Katering1 { get; set; }
        public double Ppdb1 { get; set; }
        public double Komite1 { get; set; }
        public double Spp2 { get; set; }
        public double Boks2 { get; set; }
        public double Jemputan2 { get; set; }
        public double Katering2 { get; set; }
        public double Ppdb2 { get; set; }
        public double Komite2 { get; set; }
    }
    public class SheetKelasModel
    {
        public string Unit { get; set; }
        public string Kelas { get; set; }
        public string KelasParalel { get; set; }
        public string WaliKelas { get; set; }
        public string WakilWaliKelas1 { get; set; }
        public string WakilWaliKelas2 { get; set; }
        public string WakilWaliKelas3 { get; set; }
    }
    public class SheetMapelModel
    {
        public string Kode { get; set; }
        public string NamaMapel { get; set; }
    }
    public class SheetJadwalKbmModel
    {
        public string KelasParalel { get; set; }
        public string Hari { get; set; }
        public string JamMulai { get; set; }
        public string JamSelesai { get; set; }
        public string NamaMapel { get; set; }
        public string NipGuru { get; set; }
        public string NamaGuru { get; set; }
        public int Kelompok { get; set; }
    }
    public class SheetKelQuranModel
    {
        public string NamaKelasQuran { get; set; }
        public string NamaGuru { get; set; }

    }
    public class SheetJadwalQuranModel
    {
        public string NamaKelasQuran { get; set; }
        public string NamaGuru { get; set; }
        public string Hari { get; set; }
        public string JamMulai { get; set; }
        public string JamSelesai { get; set; }
    }
    public class SheetKelQuranSiswaModel
    {
        public string NamaKelasQuran { get; set; }
        public string Nis { get; set; }
        public string NamaSiswa { get; set; }
        public string KelasParalel { get; set; }
    }
    public class SheetJenisEvaluasiHarianModel
    {
        public string Kode { get; set; }
        public string Nama { get; set; }
        public string Target { get; set; }
    }
    public class SheetJenisJilidQuranModel
    {
        public string Kode { get; set; }
        public string Nama { get; set; }
    }

    public class SheetGajiPegawaiPreviousModel : SheetGajiPegawaiModel
    {
        public string JenisPegawai { get; set; }
        public string Jabatan { get; set; }
        public string NamaInstitusiPendidikan { get; set; }
        public string Agama { get; set; }
    }
    public class SheetGajiPegawaiModel
    {
        public string Bulan { get; set; }
        public string Unit { get; set; }
        public string Nip { get; set; }
        public string Nama { get; set; }
        public string Pendidikan { get; set; }
        public string TanggalMasuk { get; set; }
        public string Golongan { get; set; }
        public string Status { get; set; }
        public int Mkg { get; set; }
        public double GT_Pokok { get; set; }
        public double GT_Operasional { get; set; }
        public double GT_Jabatan { get; set; }
        public double GT_Keluarga { get; set; }
        public double GTT_Lembur { get; set; }
        public double GTT_Anomali { get; set; }
        public double GTT_Performa { get; set; }
        public double GTT_Bpjs { get; set; }
        public double GTT_Pensiun { get; set; }
        public double GTT_Thr { get; set; }
        public double Pot_Koreksi { get; set; }
        public double Pot_Icat { get; set; }
        public double Pot_DayCare { get; set; }
        public double Pot_Koperasi { get; set; }
        public double Pot_Absensi { get; set; }
        public double Pot_Lembur { get; set; }
        public double Pot_Bpjs { get; set; }
        public double Pot_Peserta { get; set; }
        public double Pot_Pensiun { get; set; }
        public double Pot_Lainnya { get; set; }
        public double Tam_Koreksi { get; set; }
        public double Tam_Lembur { get; set; }
        public double Tam_InvalGuru { get; set; }
        public double Tam_OverTime { get; set; }
        public double Tam_Backup { get; set; }
        public double Tam_LemburHari { get; set; }
        public double Tam_LemburJam { get; set; }
        public double Tam_Tunjangan { get; set; }
        public double Tam_Bpap { get; set; }
        public double Tam_Lainnya { get; set; }
    }
    public class SheetPegawaiModel
    {
        public int IdPegawai { get; set; }
        public string Unit { get; set; }
        public string Nip { get; set; }
        public string Nama { get; set; }
        public string Pendidikan { get; set; }
        public string TanggalMasuk { get; set; }
        public string Golongan { get; set; }
        public string Status { get; set; }
        public string Nik { get; set; }
        public string TempatLahir { get; set; }
        public string TanggalLahir { get; set; }
        public string JenisKelamin { get; set; }
        public string Agama { get; set; }
        public string NamaInstitusiPendidikan { get; set; }
        public string NamaPasangan { get; set; }
        public string NoDarurat { get; set; }
        public string Jabatan { get; set; }
        public string NoHandphone { get; set; }
        public string Email { get; set; }
        public string Alamat { get; set; }
        public string NoTelpon { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class SheetNilaiSiswa
    {
        public int IdKBmMateri { get; set; }
        public string JenisNilai { get; set; }
        public string NamaUrlMeeting { get; set; }
        public string Nis { get; set; }
        public string NamaSiswa { get; set; }
        public string Hadir { get; set; }
        public double NilaiAngka { get; set; }
        public string NilaiHuruf { get; set; }
        public string Hasil { get; set; }
        public string Catatan { get; set; }
    }

    public class SheetPpdbDaftarSiswaModel
    {
        public string Judul { get; set; }
        public string DownloadOleh { get; set; }
        public string IdPpdbDaftar { get; set; }
        public string JenisKategoriPendaftaran { get; set; }
        public string JalurPendaftaran { get; set; }
        public string JenisPendaftaran { get; set; }
        public string Kelas { get; set; }
        public string Nik { get; set; }
        public string Nisn { get; set; }
        public string Nama { get; set; }
        public string NamaPanggilan { get; set; }
        public string TempatLahir { get; set; }
        public string TanggalLahir { get; set; }
        public string IdAgama { get; set; }
        public string IdKewarganegaraan { get; set; }
        public string KdJenisKelamin { get; set; }
        public string KdGolonganDarah { get; set; }
        public string AlamatTinggal { get; set; }
        public string IdTinggal { get; set; }
        public string Email { get; set; }
        public string NoHandphone { get; set; }
        public string NoDarurat { get; set; }
        public string Bahasa { get; set; }
        public int TinggiBadan { get; set; }
        public int BeratBadan { get; set; }
        public int AnakKe { get; set; }
        public int JumlahSodaraKandung { get; set; }
        public int JumlahSodaraTiri { get; set; }
        public int JumlahSodaraAngkat { get; set; }
        public string Transportasi { get; set; }
        public int JarakKeSekolah { get; set; }
        public double WaktuTempuh { get; set; }
        public string PenyakitDiderita { get; set; }
        public string KelainanJasmani { get; set; }
        public string SekolahAsal { get; set; }
        public string AlamatSekolahAsal { get; set; }
        public string NspnSekolahAsal { get; set; }
        public string IbuNama { get; set; }
        public string IbuAlamat { get; set; }
        public string IbuNik { get; set; }
        public string IbuJenisPekerjaan { get; set; }
        public string IbuNamaInstansi { get; set; }
        public string IbuJabatan { get; set; }
        public string IbuEmail { get; set; }
        public string IbuNoHandphone { get; set; }
        public string IbuNoTelpRumah { get; set; }
        public string IbuAgama { get; set; }
        public string IbuKewarganegaraan { get; set; }
        public string IbuJenjangPendidikan { get; set; }
        public string IbuIdStatusPenikahan { get; set; }
        public string IbuTempatLahir { get; set; }
        public string IbuTanggalLahir { get; set; }
        public string IbuIdStatusHidup { get; set; }
        public string IbuTanggalMeninggal { get; set; }
        public string AyahNama { get; set; }
        public string AyahAlamat { get; set; }
        public string AyahNik { get; set; }
        public string AyahJenisPekerjaan { get; set; }
        public string AyahNamaInstansi { get; set; }
        public string AyahJabatan { get; set; }
        public string AyahEmail { get; set; }
        public string AyahNoHandphone { get; set; }
        public string AyahNoTelpRumah { get; set; }
        public string AyahAgama { get; set; }
        public string AyahKewarganegaraan { get; set; }
        public string AyahJenjangPendidikan { get; set; }
        public string AyahIdStatusPenikahan { get; set; }
        public string AyahTempatLahir { get; set; }
        public string AyahTanggalLahir { get; set; }
        public string AyahIdStatusHidup { get; set; }
        public string AyahTanggalMeninggal { get; set; }
        public string WaliNama { get; set; }
        public string WaliAlamat { get; set; }
        public string WaliNik { get; set; }
        public string WaliJenisPekerjaan { get; set; }
        public string WaliNamaInstansi { get; set; }
        public string WaliJabatan { get; set; }
        public string WaliEmail { get; set; }
        public string WaliNoHandphone { get; set; }
        public string WaliNoTelpRumah { get; set; }
        public string WaliAgama { get; set; }
        public string WaliKewarganegaraan { get; set; }
        public string WaliJenjangPendidikan { get; set; }
        public string WaliIdStatusPenikahan { get; set; }
        public string WaliTempatLahir { get; set; }
        public string WaliTanggalLahir { get; set; }
        public string WaliIdStatusHidup { get; set; }
        public string WaliTanggalMeninggal { get; set; }
        public string RaporMatematika { get; set; }
        public string RaporIndo { get; set; }
        public string RaporIpa { get; set; }
        public string RaporEng { get; set; }
        public string RaporIps { get; set; }
        public string JenisPrestasi { get; set; }
        public string JenisTingkatPrestasi { get; set; }
        public string Tahun { get; set; }
        public string NamaPenyelenggara { get; set; }
        public string Keterangan { get; set; }
        public string UkuranSeragam { get; set; }
        public List<string> Jawabans { get; set; }
        public List<string> Komitmens { get; set; }
        public double UangPangkal { get; set; }
        public double Boks { get; set; }
        public double Spp { get; set; }
        public double Infaq { get; set; }
        public double Seragam { get; set; }
        public double Wakaf { get; set; }
        public string IsCicil { get; set; }
        public string LastStatus { get; set; }
        public string LastStatusProses { get; set; }
        public string LastCatatan { get; set; }
        public string TanggalDaftar { get; set; }
        public string TanggalBayarDaftar { get; set; }
        public string TanggalInputFormulir { get; set; }
        public string TanggalWawancara { get; set; }
        public string TanggalTestObservasi { get; set; }
        public string TanggalInputObservasi { get; set; }
        public string TanggalVerifikasiObservasi { get; set; }
        public string TanggalBayarPendidikan { get; set; }
        public string IsAnakPegawai { get; set; }
        public string NisSiswa { get; set; }
        public string NisSodara { get; set; }
    }
}
