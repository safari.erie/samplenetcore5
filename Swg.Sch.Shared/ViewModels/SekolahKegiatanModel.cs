﻿using Microsoft.AspNetCore.Http;
using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class SekolahKegiatanModel
    {
        public int IdSekolahKegiatan { get; set; }
        public int IdSekolah { get; set; }
        public string Judul { get; set; }
        public string Keterangan { get; set; }
        public string Url { get; set; }
    }
    public class SekolahKegiatanReadModel : SekolahKegiatanModel
    {
        public string FileImage { get; set; }
    }
    public class SekolahKegiatanAddEditModel : SekolahKegiatanModel
    {
        public IFormFile FileImage { get; set; }
    }
}
