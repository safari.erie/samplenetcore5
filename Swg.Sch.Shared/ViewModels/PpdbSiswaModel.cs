﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbSiswaModel : PpdbSiswaAddModel
    {
        public int IdPpdbKelas { get; set; }
        public int IdJenisKategoriPendaftaran { get; set; }
        public int IdJenisPendaftaran { get; set; }
        public int IdJalurPendaftaran { get; set; }
        public string TanggalLahir { get; set; }
        public int? IdSiswa { get; set; }
        public int? IdPegawai { get; set; }
        public int? IdSiswaSodara { get; set; }
        public string Unit { get; set; }
        public string Kelas { get; set; }
        public string Peminatan { get; set; }
        public string JenisKategoriPendaftaran { get; set; }
        public string JenisPendaftaran { get; set; }
        public string JalurPendaftaran { get; set; }
        public string NamaPegawai { get; set; }
        public string NamaSodara { get; set; }
        public string Status { get; set; }
        public string StatusProses { get; set; }
        public string CatatanProses { get; set; }
        public int IsCicil { get; set; }
        public double RpBiayaTotal { get; set; }
        public string StatusBiaya { get; set; }
        public string CatatanBiaya { get; set; }
        public string RefNo { get; set; }
        public string TanggalLunas { get; set; }
        public List<PpdbBiayaSiswaDetilModel> TagPpdb { get; set; }
        public List<PpdbBiayaSiswaCicilModel> TagCicil { get; set; }
        public string Foto { get; set; }
        public string FileKtpIbu { get; set; }
        public string FileKtpAyah { get; set; }
        public string FileKartuKeluarga { get; set; }
        public string FileAkte { get; set; }
        public string FileSuratKesehatan { get; set; }
        public string FileBebasNarkoba { get; set; }
        public string FileKartuPeserta { get; set; }
        public string FilePerjanjianCicil { get; set; }
        public List<PpdbSiswaOrtuAddModel> DataOrtu { get; set; }
        public List<PpdbSiswaPrestasiModel> Prestasi { get; set; }
        public List<PpdbSiswaRaporModel> Rapor { get; set; }
        public string FileLembarKomitmen { get; set; }
    }
    public class PpdbSiswaAddModel
    {
        public int IdPpdbDaftar { get; set; }
        public int IdPpdbSeragam { get; set; }
        public string Nik { get; set; }
        public string Nisn { get; set; }
        public string Nama { get; set; }
        public string NamaPanggilan { get; set; }
        public string TempatLahir { get; set; }
        public int IdAgama { get; set; }
        public int IdKewarganegaraan { get; set; }
        public string KdJenisKelamin { get; set; }
        public string KdGolonganDarah { get; set; }
        public string AlamatTinggal { get; set; }
        public int IdTinggal { get; set; }
        public string Email { get; set; }
        public string NoHandphone { get; set; }
        public string NoDarurat { get; set; }
        public int IdBahasa { get; set; }
        public int TinggiBadan { get; set; }
        public int BeratBadan { get; set; }
        public int AnakKe { get; set; }
        public int JumlahSodaraKandung { get; set; }
        public int JumlahSodaraTiri { get; set; }
        public int JumlahSodaraAngkat { get; set; }
        public int IdTransportasi { get; set; }
        public int JarakKeSekolah { get; set; }
        public double WaktuTempuh { get; set; }
        public string PenyakitDiderita { get; set; }
        public string KelainanJasmani { get; set; }
        public string SekolahAsal { get; set; }
        public string AlamatSekolahAsal { get; set; }
        public string NspnSekolahAsal { get; set; }
        public bool TerimaKip { get; set; }
        public string AlasanKip { get; set; }
        public int IdCita2 { get; set; }
        public int IdHobi { get; set; }
    }

    public class PpdbSiswaListModel
    {
        public int IdPpdbDaftar { get; set; }
        public string Unit { get; set; }
        public string Kelas { get; set; }
        public string Peminatan { get; set; }
        public string JenisKategoriPendaftaran { get; set; }
        public string JenisPendaftaran { get; set; }
        public string JalurPendaftaran { get; set; }
        public string Nama { get; set; }
        public string TanggalLahir { get; set; }
        public string NamaPegawai { get; set; }
        public string NamaSodara { get; set; }
        public string SekolahAsal { get; set; }
        public string Status { get; set; }
        public string StatusProses { get; set; }
        public string CatatanProses { get; set; }
    }

    public class PpdbSiswaObservasiWawancara : PpdbSiswaListModel
    {
        public string NoHandphoneSiswa { get; set; }
        public string NamaPetugasObservasi { get; set; }
        public string NamaPetugasWawancara { get; set; }
        public string EmailPetugasObservasi { get; set; }
        public string EmailPetugasWawancara { get; set; }
        public string NoHpPetugasObservasi { get; set; }
        public string NoHpPetugasWawancara { get; set; }
        public string TanggalObservasi { get; set; }
        public string TanggalWawancara { get; set; }
        public string JamObservasi { get; set; }
        public string JamWawancara { get; set; }
    }


    ///sheeet laporan siswa gelombang ppdb
    public class SheetLaporanSiswaGelombangPpdbModel
    {
        public string IdPpdbDaftar { get; set; }
        public string JenjangPendidikan { get; set; }
        public string JurusanPendidikan { get; set; }
        public string JalurPendaftaran { get; set; }
        public string Nis { get; set; }
        public string Nama { get; set; }
        public string JenisKelamin { get; set; }
        public string TempatLahir { get; set; }
        public string TanggalLahir { get; set; }
        public string Alamat { get; set; }
        public string NamaAyah { get; set; }
        public string NoHpAyah { get; set; }
        public string EmailAyah { get; set; }
        public string NamaIbu { get; set; }
        public string NoHpIbu { get; set; }
        public string EmailIbu { get; set; }
        public string NamaPewakaf { get; set; }
        public string HubunganSiswa { get; set; }
        public string StatusProses { get; set; }
        public string StatusPpdb { get; set; }
    }

    public class SheetLaporanJkPpdbModel
    {
        public int TotL { get; set; }
        public int TotP { get; set; }
    }

    public class SheetLaporanStatikGelombangPpdbModel
    {
        public SheetLaporanJkPpdbModel KuotaPpdbTkA { get; set; }
        public SheetLaporanJkPpdbModel KuotaPpdbTkB { get; set; }
        public SheetLaporanJkPpdbModel KuotaPpdbSd { get; set; }
        public SheetLaporanJkPpdbModel KuotaPpdbSmp { get; set; }
        public SheetLaporanJkPpdbModel KuotaPpdbSmaIpa { get; set; }
        public SheetLaporanJkPpdbModel KuotaPpdbSmaIps { get; set; }

        public SheetLaporanJkPpdbModel KuotaDaftarTkA { get; set; }
        public SheetLaporanJkPpdbModel KuotaDaftarTkB { get; set; }
        public SheetLaporanJkPpdbModel KuotaDaftarSd { get; set; }
        public SheetLaporanJkPpdbModel KuotaDaftarSmp { get; set; }
        public SheetLaporanJkPpdbModel KuotaDaftarSmaIpa { get; set; }
        public SheetLaporanJkPpdbModel KuotaDaftarSmaIps { get; set; }

        public SheetLaporanJkPpdbModel KuotaDaftarTungguTkA { get; set; }
        public SheetLaporanJkPpdbModel KuotaDaftarTungguTkB { get; set; }
        public SheetLaporanJkPpdbModel KuotaDaftarTungguSd { get; set; }
        public SheetLaporanJkPpdbModel KuotaDaftarTungguSmp { get; set; }
        public SheetLaporanJkPpdbModel KuotaDaftarTungguSmaIpa { get; set; }
        public SheetLaporanJkPpdbModel KuotaDaftarTungguSmaIps { get; set; }

        public SheetLaporanJkPpdbModel KuotaLulusObservasiTkA { get; set; }
        public SheetLaporanJkPpdbModel KuotaLulusObservasiTkB { get; set; }
        public SheetLaporanJkPpdbModel KuotaLulusObservasiSd { get; set; }
        public SheetLaporanJkPpdbModel KuotaLulusObservasiSmp { get; set; }
        public SheetLaporanJkPpdbModel KuotaLulusObservasiSmaIpa { get; set; }
        public SheetLaporanJkPpdbModel KuotaLulusObservasiSmaIps { get; set; }

        public SheetLaporanJkPpdbModel KuotaTidakLulusObservasiTkA { get; set; }
        public SheetLaporanJkPpdbModel KuotaTidakLulusObservasiTkB { get; set; }
        public SheetLaporanJkPpdbModel KuotaTidakLulusObservasiSd { get; set; }
        public SheetLaporanJkPpdbModel KuotaTidakLulusObservasiSmp { get; set; }
        public SheetLaporanJkPpdbModel KuotaTidakLulusObservasiSmaIpa { get; set; }
        public SheetLaporanJkPpdbModel KuotaTidakLulusObservasiSmaIps { get; set; }

        public SheetLaporanJkPpdbModel KuotaCadanganObservasiTkA { get; set; }
        public SheetLaporanJkPpdbModel KuotaCadanganObservasiTkB { get; set; }
        public SheetLaporanJkPpdbModel KuotaCadanganObservasiSd { get; set; }
        public SheetLaporanJkPpdbModel KuotaCadanganObservasiSmp { get; set; }
        public SheetLaporanJkPpdbModel KuotaCadanganObservasiSmaIpa { get; set; }
        public SheetLaporanJkPpdbModel KuotaCadanganObservasiSmaIps { get; set; }

        public SheetLaporanJkPpdbModel KuotaBiayaPendidikanTkA { get; set; }
        public SheetLaporanJkPpdbModel KuotaBiayaPendidikanTkB { get; set; }
        public SheetLaporanJkPpdbModel KuotaBiayaPendidikanSd { get; set; }
        public SheetLaporanJkPpdbModel KuotaBiayaPendidikanSmp { get; set; }
        public SheetLaporanJkPpdbModel KuotaBiayaPendidikanSmaIpa { get; set; }
        public SheetLaporanJkPpdbModel KuotaBiayaPendidikanSmaIps { get; set; }

    }

    public class SheetLaporanGelombangPpdbModel
    {
        public SheetLaporanStatikGelombangPpdbModel StatikTotalPpdb { get; set; }
        public List<SheetLaporanSiswaGelombangPpdbModel> LapSiswaTk { get; set; }
        public List<SheetLaporanSiswaGelombangPpdbModel> LapSiswaSd { get; set; }
        public List<SheetLaporanSiswaGelombangPpdbModel> LapSiswaSmp { get; set; }
        public List<SheetLaporanSiswaGelombangPpdbModel> LapSiswaSma { get; set; }
    }

}
