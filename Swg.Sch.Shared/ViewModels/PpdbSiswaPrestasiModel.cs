﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbSiswaPrestasiModel : PpdbSiswaPrestasiAddModel
    {
        public int IdPpdbSiswaPrestasi { get; set; }
        public int IdPpdbDaftar { get; set; }
        public string JenisTingkatPrestasi { get; set; }
        public string JenisPrestasi { get; set; }
        public string FileSertifikat { get; set; }
    }
    public class PpdbSiswaPrestasiAddModel
    {
        public int IdJenisPrestasi { get; set; }
        public int Tahun { get; set; }
        public int IdJenisTingkatPrestasi { get; set; }
        public string NamaPenyelenggara { get; set; }
        public string Keterangan { get; set; }
    }
}
