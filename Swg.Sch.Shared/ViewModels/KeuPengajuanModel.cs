﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class KeuPengajuanListModel
    {
        public int IdPengajuan { get; set; }
        public string TahunAjaran { get; set; }
        public int IdUnit { get; set; }
        public string NamaUnit { get; set; }
        public string Nomor { get; set; }
        public string Program { get; set; }
        public string Kegiatan { get; set; }
        public string TanggalButuh { get; set; }
        public string Status { get; set; }
        public string StatusProses { get; set; }
        public string Catatan { get; set; }
        public double TotalRp { get; set; }
        public double TotalRealisasiRp { get; set; }
        public List<string> Aksis { get; set; }
    }
    public class KeuPengajuanCoaListModel
    {
        public int IdPengajuanCoa { get; set; }
        public int IdCoa { get; set; }
        public int IdJenisSatuan { get; set; }
        public int IdJenisVolume { get; set; }
        public string KodeCoa { get; set; }
        public string NamaCoa { get; set; }
        public double JumlahSatuan { get; set; }
        public string Satuan { get; set; }
        public double JumlahVolume { get; set; }
        public string Volume { get; set; }
        public double HargaSatuan { get; set; }
        public double Rp { get; set; }
        public string Catatan { get; set; }
        public string Tanggal { get; set; }
        public List<string> Aksis { get; set; }
    }
    public class KeuPengajuanCoaAddModel
    {
        public int IdPengajuan { get; set; }
        public int IdCoa { get; set; }
        public int IdJenisSatuan { get; set; }
        public double JumlahSatuan { get; set; }
        public int IdJenisVolume { get; set; }
        public double JumlahVolume { get; set; }
        public string Tanggal { get; set; }
        public double HargaSatuan { get; set; }
        public string Catatan { get; set; }
    }
    public class KeuPengajuanCoaEditModel
    {
        public int IdPengajuanCoa { get; set; }
        public int IdJenisSatuan { get; set; }
        public double JumlahSatuan { get; set; }
        public int IdJenisVolume { get; set; }
        public double JumlahVolume { get; set; }
        public string Tanggal { get; set; }
        public double HargaSatuan { get; set; }
        public string Catatan { get; set; }
    }

    public class KeuPengajuanDownloadModel
    {
        public string NamaUnit { get; set; }
        public string TahunAjaran { get; set; }
        public string Program { get; set; }
        public string Kegiatan { get; set; }
        public string Nomor { get; set; }
        public string Tanggal { get; set; }
        public double TotalRp { get; set; }
        public string Status { get; set; }
        public List<KeuPengajuanDetDownloadModel> Det { get; set; }
    }
    public class KeuPengajuanDetDownloadModel
    {
        public int IdCoa { get; set; }
        public int? IdParentCoa { get; set; }
        public string Kode { get; set; }
        public string Nama { get; set; }
        public double JumlahSatuan { get; set; }
        public string Satuan { get; set; }
        public double JumlahVolume { get; set; }
        public string Volume { get; set; }
        public double HargaSatuan { get; set; }
        public double TotalRp { get; set; }
        public string Catatan { get; set; }
    }
}
