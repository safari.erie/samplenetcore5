﻿using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class SiswaPrestasiModel : SiswaPrestasiEditModel
    {
        public string FileSertifikat { get; set; }
        public string JenisPrestasi { get; set; }
        public string JenisTingkatPrestasi { get; set; }
    }
    public class SiswaPrestasiAddModel
    {
        public int IdSiswa { get; set; }
        public int IdJenisPrestasi { get; set; }
        public int Tahun { get; set; }
        public int IdJenisTingkatPrestasi { get; set; }
        public string NamaPenyelenggara { get; set; }
        public string Keterangan { get; set; }
    }
    public class SiswaPrestasiEditModel : SiswaPrestasiAddModel
    {
        public int IdSiswaPrestasi { get; set; }
    }
}
