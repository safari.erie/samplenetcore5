﻿using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbBiayaModel : PpdbBiayaAddEditModel
    {
        public string Unit { get; set; }
        public string Kelas { get; set; }
        public string JenisBiaya { get; set; }
    }
    public class PpdbBiayaAddEditModel
    {
        public int IdPpdbBiaya { get; set; }
        public int IdUnit { get; set; }
        public int IdPpdbKelas { get; set; }

        public int IdPpdb { get; set; }
        public int IdJenisBiaya { get; set; }
        public double Rp { get; set; }
    }
}
