﻿using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbDaftarKartuModel : PpdbDaftarModel
    {
        public string NamaPanggilan { get; set; }
        public string Foto { get; set; }
    }
}
