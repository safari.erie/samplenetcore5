﻿using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbJadwalModel
    {
        public int IdUnit { get; set; }
        public int IdJenisGelombangPpdb { get; set; }
        public int IdJenisPendaftaran { get; set; }
        public int IdJalurPendaftaran { get; set; }
        public string TanggalAwalDaftar { get; set; }
        public string TanggalAkhirDaftar { get; set; }
        public string TanggalAwalBayarDaftar { get; set; }
        public string TanggalAkhirBayarDaftar { get; set; }
        public string TanggalAwalObservasi { get; set; }
        public string TanggalAkhirObservasi { get; set; }
        public string TanggalAwalBayarPendidikan { get; set; }
        public string TanggalAkhirBayarPendidikan { get; set; }
        public string TanggalKonfirmasiCadangan { get; set; }
    }
    public class PpdbJadwalViewModel : PpdbJadwalModel
    {
        public string Unit { get; set; }
        public string JenisGelombangPpdb { get; set; }
        public string JenisPendaftaran { get; set; }
        public string JalurPendaftaran { get; set; }
    }
    public class PpdbJadwalEditModel : PpdbJadwalModel
    {

    }
}
