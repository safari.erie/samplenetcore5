﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbObservasiModel
    {
        public int IdUser { get; set; }
        public int IdPpdbDaftar { get; set; }
        public int IdStatusObservasi { get; set; }
        public List<PpdbObservasiBiayaModel> Biayas { get; set; }
        public string CatatanObservasi { get; set; }
        public string CatatanWawancara { get; set; }
        public int Status { get; set; }
        public string StatusObservasi { get; set; }
    }

    public class PpdbObservasiBiayaModel : PpdbObservasiModel
    {
        public int IdJenisBiayaPpdb { get; set; }
        public string JenisBiayaPpdb { get; set; }
        public double Rp { get; set; }
    }

    public class PpdbObservasiBiayaModelRead
    {
        public string JenisBiayaPpdb { get; set; }
        public double Rp { get; set; }
        public string Status { get; set; }
    }

    public class PpdbObservasiSheetModel
    {
        public string IdPpdbDaftar { get; set; }
        public string CatatanObservasi { get; set; }
        public string CatatanWawancara { get; set; }
        public string RpUp { get; set; }
        public string RpBoks { get; set; }
        public string RpSpp { get; set; }
        public string RpInfaq { get; set; }
        public string RpSeragam { get; set; }
        public string RpWakaf { get; set; }
        public string CatatanWakaf { get; set; }
        public string Status { get; set; }
    }

}
