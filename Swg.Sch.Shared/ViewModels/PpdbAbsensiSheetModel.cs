using System;
namespace Swg.Sch.Shared.ViewModels
{
    public class PpdbAbsensiSheetModel
    {
        public int IdPpdbDaftar { get; set; }
        public string Status { get; set; }
        public string Catatan { get; set; }
    }
}
