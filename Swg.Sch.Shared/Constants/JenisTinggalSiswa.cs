﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.Constants
{
    public class JenisTinggalSiswaConstant
    {
        public static Dictionary<int, string> DictJenisTinggalSiswa = new Dictionary<int, string>()
        {
            {Ortu,"Dengan Orang Tua" },
            {Wali,"Dengan Wali Siswa" },
            {Kos,"Tempat Kos/Kontrakan" },
            {Pesantren, "Pesantren" },
            {Lainnya, "Lainnya" },
        };
        public const int Ortu = 1;
        public const int Wali = 2;
        public const int Kos = 3;
        public const int Pesantren = 4;
        public const int Lainnya = 5;
    }
}
