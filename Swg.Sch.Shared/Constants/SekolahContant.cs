﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.Constants
{
    public class SekolahConstant
    {
        public static Dictionary<int, string> DictSekolah = new Dictionary<int, string>()
        {
            {At, "Sekolah AT Taufiq Bogor" },
            {Mh, "Sekolah Mutiara Hati Bekasi" }
        };
        public const int At = 1;
        public const int Mh = 2;
    }
}
