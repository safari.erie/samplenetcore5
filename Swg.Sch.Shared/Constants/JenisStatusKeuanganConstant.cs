﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisStatusRenPenConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>();
        public static int Initial;
        public static int Baru;
        public static int DisetujuiManKeu;
        public static int DisetujuiDirektur;
        public static int Disahkan;
    }
    public class JenisStatusProgKegConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>();
        public static int Initial;
        public static int Baru;
        public static int DisetujuiKaUnit;
        public static int DisetujuiManUnit;
        public static int DisetujuiManKeu;
        public static int DisetujuiDirektur;
        public static int Disahkan;
    }
    public class JenisStatusPengajuanConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>();
        public static int Baru;
        public static int DisetujuiKaUnit;
        public static int DisetujuiManUnit;
        public static int DisetujuiManKeu;
        public static int DisetujuiDirektur;
        public static int Direalisasikan;
        public static int Dilaporkan;
        public static int InitDilaporkan;
        public static int Dibukukan;
    }
}
