﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisPpdbInfoConstant
    {
        public static Dictionary<int, string> DictJenisPpdbInfo = new Dictionary<int, string>()
        {
            {Aturan,"Aturan" },
            {Alur,"Alur" },
            {Biaya,"Biaya" },
            {Pengumuman,"Pengumuman" }
        };

        public const int Aturan = 1;
        public const int Alur = 2;
        public const int Biaya = 3;
        public const int Pengumuman = 4;
    }
}
