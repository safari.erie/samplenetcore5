﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class NamaFileUploadConstant
    {
        public static Dictionary<int, string> DictNamaFileUpload = new Dictionary<int, string>()
        {
            {Akte,"_01"}, //akte
            {KartuKeluarga,"_02"}, //kartu keluarga
            {KtpIbu,"_03"}, //ktp ibu
            {KtpAyah,"_04"}, //ktp ayah
            {SuratKesehatan,"_05"}, //surat kesehatan
            {Nisn,"_06"}, //NISN
            {KetBebasNarkoba,"_07"}, //keterangan bebas narkoba
            {PasFoto,"_08"}, //pas foto
            {BuktiPrestasi,"_09"}, //bukti prestasi
            {BuktiBayarDaftar,"_10"}, //bukti bayar pendaftara
            {BuktiBayarPendidikan,"_11" }, //bukti bayar pendidikan
            {SuratPerjanjianCicilan,"_12"}, //surat perjanjian cicil
            {Raport,"_13"}, //file raport
            {KartuPeserta,"_14"}, //kartu peserta
            // {RaportNilai,"_15"}, //file raport nilai

            {LogoSekolah,"_logo_sekolah"},
            {LogoUnit,"_logo_unit"},
            {IconSekolah,"_icon_sekolah"},
            {FotoWebSekolah,"_foto_web_sekolah"},
            {UnitSliderBanner,"_unit_slider_banner"},
            {UnitSliderImage,"_unit_slider_image"},
            {UnitThumbnail,"_unit_thumbnail"},
            {UnitThumbnailBesar,"_unit_thumbnail_besar"},
            {UnitStrukturOrganisasi,"_unit_struktur_organisasi"},
            {UnitEkskul,"_unit_ekskul"},
            {KalenderPendidikan,"_kalender_pendidikan"},
            {FasilitasSekolah,"_fasilitas_sekolah"},
            {PrestasiSekolah,"_prestasi_sekolah"},

        };

        public const int Akte = 1;
        public const int KartuKeluarga = 2;
        public const int KtpIbu = 3;
        public const int KtpAyah = 4;
        public const int SuratKesehatan = 5;
        public const int Nisn = 6;
        public const int KetBebasNarkoba = 7;
        public const int PasFoto = 8;
        public const int BuktiPrestasi = 9;
        public const int BuktiBayarDaftar = 10;
        public const int BuktiBayarPendidikan = 11;
        public const int SuratPerjanjianCicilan = 12;
        public const int Raport = 13;
        public const int KartuPeserta = 14;
        // public const int RaportNilai = 15;

        public const int LogoSekolah = 100;
        public const int LogoUnit = 101;
        public const int IconSekolah = 102;
        public const int FotoWebSekolah = 103;
        public const int UnitSliderBanner = 104;
        public const int UnitSliderImage = 105;
        public const int UnitThumbnail = 106;
        public const int UnitThumbnailBesar = 107;
        public const int UnitStrukturOrganisasi = 108;
        public const int UnitEkskul = 109;
        public const int KalenderPendidikan = 110;
        public const int FasilitasSekolah = 111;
        public const int PrestasiSekolah = 112;
    }
}
