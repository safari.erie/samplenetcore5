﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class PpdbJenisPeminatan
    {
        public PpdbJenisPeminatan()
        {
        }
    }
    public class JenisPeminatanPpdbConstant
    {
        public static Dictionary<int, string> DictJenisPeminatanPpdb = new Dictionary<int, string>()
        {
            {IPA,"IPA" },
            {IPS,"IPS" }
        };

        public const int IPA = 1;
        public const int IPS = 2;
    }

}
