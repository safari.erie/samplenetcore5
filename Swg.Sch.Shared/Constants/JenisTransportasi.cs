﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.Constants
{
    public class JenisTransportasiConstant
    {
        public static Dictionary<int, string> DictJenisTransportasi = new Dictionary<int, string>()
        {
            {Pribadi2,"Kendaraan Pribadi R-2" },
            {Pribadi4,"Kendaraan Pribadi R-4" },
            {Jemputan, "Kendaraan Jemputan" },
            {Umum, "Kendaraan Umum" }
        };

        public const int Pribadi2 = 1;
        public const int Pribadi4 = 2;
        public const int Jemputan = 3;
        public const int Umum = 4;
    }
    class JenisTransportasi
    {
    }
}
