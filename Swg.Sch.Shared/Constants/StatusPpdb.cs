﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class StatusPpdbConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>()
        {
            {Registrasi, "Registrasi" },
            {TestObservasi,"Proses Test/Observasi" },
            {Wawancara,"Proses Wawancara" },
            {TestObservasiWawancara,"Test Observasi Wawancara" },
            {InputObservasi, "Input Hasil Observasi" },
            {VerifikasiObservasi, "Verifikasi Hasil Observasi" },
            {PelunasanBiayaPendidikan,"Pelunasan Biaya Pendidikan" },
            {Pengesahan, "Pengesahan PPDB" }
        };
        public const int Registrasi = 1;
        public const int TestObservasi = 20;
        public const int Wawancara = 21;
        public const int TestObservasiWawancara = 22;
        public const int InputObservasi = 3;
        public const int VerifikasiObservasi = 4;
        public const int PelunasanBiayaPendidikan = 5;
        public const int Pengesahan = 6;
    }
    public class StatusPpdbTestObservasiConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>()
        {
            {DalamProses, "Belum/Tidak Hadir" },
            {Hadir, "Hadir" }
        };
        public const int DalamProses = 0;
        public const int Hadir = 1;
    }
    public class StatusPpdbWawancaraConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>()
        {
            {DalamProses, "Belum/Tidak Hadir" },
            {Hadir, "Hadir" }
        };
        public const int DalamProses = 0;
        public const int Hadir = 1;
    }
    public class StatusPpdbTestObservasiWawancaraConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>()
        {
            {DalamProses, "Belum/Tidak Hadir" },
            {Hadir, "Sudah Observasi dan Wawancara" }
        };
        public const int DalamProses = 0;
        public const int Hadir = 1;
    }
    public class StatusPpdbHasilObservasiConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>()
        {
            {Direkomendasikan, "Direkomendasikan" },
            {TidakDirekomendasikan,"Tidak Direkomendasikan" }
        };
        public const int Direkomendasikan = 1;
        public const int TidakDirekomendasikan = -1;
    }
    public class StatusPpdbVerifikasiObservasiConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>()
        {
            {Diterima,"Diterima" },
            {UndurDiri, "Mengundurkan Diri" },
            {Ditangguhkan, "Ditangguhkan/Cadangan" },
            {TidakDiterima,"Tidak Diterima" }
        };
        public const int Diterima = 1;
        public const int UndurDiri = 2;
        public const int Ditangguhkan = 3;
        public const int TidakDiterima = -1;
    }

}
