﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.Constants
{
    public class StatusProsesKontenConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>()
        {
            {New, "Baru Dibuat" },
            {Approved, "Disetujui" },
            {Published, "Publish" },
            {InActive, "Non-Aktif" },

        };
        public const int New = 0;
        public const int Approved = 1;
        public const int Published = 2;
        public const int InActive = 99;
    }
}
