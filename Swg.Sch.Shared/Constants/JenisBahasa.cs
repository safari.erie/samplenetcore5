﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.Constants
{
    public class JenisBahasaConstant
    {
        public static Dictionary<int, string> DictJenisBahasa = new Dictionary<int, string>()
        {
            {Id,"Indonesia" },
            {En,"Inggris" },
            {Ar,"Arab" },
            {Lainnya, "Lainnya" },
        };
        public const int Id = 1;
        public const int En = 2;
        public const int Ar = 3;
        public const int Lainnya = 4;
    }
}
