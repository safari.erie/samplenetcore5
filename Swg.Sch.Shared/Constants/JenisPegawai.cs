﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisPegawaiConstant
    {
        public static Dictionary<int, string> DictJenisPegawai = new Dictionary<int, string>()
        {
            {Na,"N/A" },
            {Tetap,"Pegawai Tetap" },
            {Capeg,"Calon Pegawai Tetap" },
            {Kontrak, "Pegawai Kontrak" },
            {Honorer, "Pegawai Honorer" }
        };
        public const int Na = 0;
        public const int Tetap = 1;
        public const int Capeg = 2;
        public const int Kontrak = 3;
        public const int Honorer = 4;
    }
}
