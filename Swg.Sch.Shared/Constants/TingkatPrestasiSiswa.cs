﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class TingkatPrestasiSiswaConstant
    {
        public static Dictionary<int, string> DictTingkatPrestasiSiswa = new Dictionary<int, string>()
        {
            {Kec,"Kecamatan" },
            {Kab,"Kab/Kota" },
            {Prov,"Provinsi" },
            {Nas, "Nasional" },
            {Intr, "Internasional" }
        };
        public const int Kec = 1;
        public const int Kab = 2;
        public const int Prov = 3;
        public const int Nas = 4;
        public const int Intr = 5;
    }
}
