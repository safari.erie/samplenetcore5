using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class StatusSiswa
    {
        public static Dictionary<int, string> DictStatusSiswa = new Dictionary<int, string>()
        {
            {Aktif,"Aktif" },
            {NonAktif,"NonAktif" }
        };

        public const int Aktif = 1;
        public const int NonAktif = -1;
    }
}
