﻿using System;
using System.Collections.Generic;
namespace Swg.Sch.Shared.Constants
{
    public class StatusLunasConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>()
        {
            {BelumLunas,"BELUM LUNAS" },
            {Lunas,"LUNAS" }
        };
        public const int BelumLunas = 0;
        public const int Lunas = 1;
    }
}
