﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class StatusPpdbVaConstant
    {
        public static Dictionary<int, string> DictStatusBerkas = new Dictionary<int, string>()
        {
            {TidakAktif, "Tidak Aktif" },
            {Aktif,"Aktif" },
            {SudahDigunakan,"Sudah Digunakan" }
        };

        public const int TidakAktif = 0;
        public const int Aktif = 1;
        public const int SudahDigunakan = -1;
    }
}
