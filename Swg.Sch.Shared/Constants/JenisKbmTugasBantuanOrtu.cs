﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisKbmTugasBantuanOrtuConstant
    {
        public static Dictionary<int, string> DictJenisKbmTugasBantuanOrtu = new Dictionary<int, string>()
        {
            {Mandiri, "Mengerjakan sendiri" },
            {BimbinganOrtu, "Mengerjakan dengan sedikit bantuan" },
            {BantuanOrtu, "Mengerjakan dengan bantuan" },
            {PerluMotivasi, "Perlu Motivasi" }
        };
        public const int Mandiri = 1;
        public const int BimbinganOrtu = 2;
        public const int BantuanOrtu = 3;
        public const int PerluMotivasi = 4;
    }
}
