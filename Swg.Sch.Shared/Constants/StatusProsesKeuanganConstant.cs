﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class StatusProsesKeuanganConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>()
        {
            {New, "Dalam Proses" },
            {Approved, "Disetujui" },
            {Rejected, "Tidak Disetujui" }

        };
        public const int New = 0;
        public const int Approved = 1;
        public const int Rejected = 99;
    }
}
