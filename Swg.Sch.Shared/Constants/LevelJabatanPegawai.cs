﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class LevelJabatanPegawaiConstant
    {
        public static Dictionary<int, string> DictLevelJabatanPegawai = new Dictionary<int, string>()
        {
            {Administrator, "Administrator" },
            {Direksi,"Direksi" },
            {Manager, "Manager" },
            {KepalaUnit, "Kepala Unit" },
            {ManajemenUnit, "Wakil Kepala , Sekretaris, Bendahara" },
            {StafUnit, "Staf Unit" },
        };
        public const int Administrator = 99;
        public const int Direksi = 1;
        public const int Manager = 2;
        public const int KepalaUnit = 3;
        public const int ManajemenUnit = 4;
        public const int StafUnit = 5;
    }
}
