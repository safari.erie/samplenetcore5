﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisGaleriConstant
    {
        public static Dictionary<int, string> DictJenisGaleri = new Dictionary<int, string>()
        {
            {Image,"Image" },
            {Video,"Video" }
        };

        public const int Image = 1;
        public const int Video = 2;
    }
}
