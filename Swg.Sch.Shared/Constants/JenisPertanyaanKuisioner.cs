﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisPertanyaanKuisionerConstant
    {
        public static Dictionary<int, string> DictJenisPertanyaanKuisioner = new Dictionary<int, string>()
        {
            {Jenis1,"Jenis 1" },
            {Jenis2,"Jenis 2" },
            {Jenis3,"Jenis 3" },
            {Jenis4,"Jenis 4" },
            {Jenis5,"Jenis 5" },
        };

        public const int Jenis1 = 1;
        public const int Jenis2 = 2;
        public const int Jenis3 = 3;
        public const int Jenis4 = 4;
        public const int Jenis5 = 5;
    }
}
