using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.Constants
{
    public class TipePlatformConstant
    {
        public static Dictionary<int, string> DictTipePlatform = new Dictionary<int, string>()
        {
            {Web, "Website" },
            {Mobile, "Mobile" },
        };
        public const int Web = 1;
        public const int Mobile = 2;
    }
}
