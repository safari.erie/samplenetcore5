﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisTingkatPrestasiSiswaConstant
    {
        public static Dictionary<int, string> DictJenisTingkatPrestasiSiswa = new Dictionary<int, string>()
        {
            {Kecamatan,"Tahfidz Quran" },
            {Kabupaten,"Akademis" },
            {Provinsi,"Olah Raga" },
            {Nasional, "Lainnya" },
            {Internasional, "Internasional" },
            {Sekolah, "Sekolah" }
        };
        public const int Kecamatan = 1;
        public const int Kabupaten = 2;
        public const int Provinsi = 3;
        public const int Nasional = 4;
        public const int Internasional = 5;
        public const int Sekolah = 6;
    }
}
