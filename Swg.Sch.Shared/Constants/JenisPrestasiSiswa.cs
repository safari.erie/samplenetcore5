﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisPrestasiConstant
    {
        public static Dictionary<int, string> DictJenisPrestasi = new Dictionary<int, string>()
        {
            {Tahfidz,"Tahfidz Quran" },
            {Akademis,"Akademis" },
            {OlahRaga,"Olah Raga" },
            {Lain2, "Lainnya" }
        };
        public const int Tahfidz = 1;
        public const int Akademis = 2;
        public const int OlahRaga = 3;
        public const int Lain2 = 4;
    }
}
