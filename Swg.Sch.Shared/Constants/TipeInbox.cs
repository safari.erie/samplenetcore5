using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.Constants
{
    public class TipeInboxConstant
    {
        public static Dictionary<int, string> DictTipeInbox = new Dictionary<int, string>()
        {
            {RuangBelajar, "Ruang Belajar" },
            {Tagihan, "Tagihan" },
            {Rapor,"Rapor" },
            {Login,"Login" },
        };
        public const int RuangBelajar = 1;
        public const int Tagihan = 2;
        public const int Rapor = 3;
        public const int Login = 4;
    }

}
