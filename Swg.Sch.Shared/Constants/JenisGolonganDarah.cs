﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisGolonganDarahConstant
    {
        public static Dictionary<string, string> Dict = new Dictionary<string, string>()
        {
            {A,"A" },
            {AB,"AB" },
            {B,"B" },
            {O,"O" }
        };
        public const string A = "A";
        public const string AB = "AB";
        public const string B = "B";
        public const string O = "O";
    }
}
