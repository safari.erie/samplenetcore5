﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.Constants
{
    public class JenisDataKontenUnitConstant
    {
        public static Dictionary<int, string> DictJenisDataKontenUnit = new Dictionary<int, string>()
        {
            {Default, "Default" },
            {Profil, "Profil" },
        };
        public const int Default = 0;
        public const int Profil = 1;
    }
}
