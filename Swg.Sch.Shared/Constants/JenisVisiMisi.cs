﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisVisiMisiConstant
    {
        public static Dictionary<int, string> DictJenisVisiMisi = new Dictionary<int, string>()
        {
            {Visi,"Visi" },
            {Misi,"Misi" },
            {Kurikulum,"Kurikulum" }
        };

        public const int Visi = 1;
        public const int Misi = 2;
        public const int Kurikulum = 3;
    }
}
