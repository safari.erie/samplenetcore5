﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.Constants
{
    public class TipeOrtuSiswaConstant
    {
        public static Dictionary<int, string> DictTipeOrtuSiswa = new Dictionary<int, string>()
        {
            {Ayah, "Ayah" },
            {Ibu, "Ibu" },
            {Wali,"Wali" },
        };
        public const int Ayah = 1;
        public const int Ibu = 2;
        public const int Wali = 3;
    }
    class TipeOrtuSiswa
    {
    }
}
