using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisPpdbKontenConstant
    {
        public static Dictionary<int, string> DictJenisPpdbKonten = new Dictionary<int, string>()
        {
            {Aturan,"Aturan" },
            {Alur,"Alur Pendaftaran" },
            {PetujukTeknis,"Petunjuk dan Teknis" },
            {BrosurBiaya,"Brosur Biaya" },
            {BrosurSekolah,"Brosur Sekolah" },
            {Pengumuman,"Pengumuman" }
        };

        public const int Aturan = 1;
        public const int Alur = 2;
        public const int PetujukTeknis = 3;
        public const int BrosurBiaya = 4;
        public const int BrosurSekolah = 5;
        public const int Pengumuman = 6;
    }
}
