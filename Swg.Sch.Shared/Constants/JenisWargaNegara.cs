﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisWargaNegaraConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>()
        {
            {Wni,"Warga Negara Indonesia" },
            {Wna,"Warga Negara Asing" }
        };
        public const int Wni = 1;
        public const int Wna = 2;
    }
}
