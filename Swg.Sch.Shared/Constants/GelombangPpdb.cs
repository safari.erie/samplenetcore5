﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class GelombangPpdbConstant
    {
        public static Dictionary<int, string> DictGelombangPpdb = new Dictionary<int, string>()
        {
            {Gel1,"Gelombang 1" },
            {Gel2,"Gelombang 2" }
        };

        public const int Gel1 = 1;
        public const int Gel2 = 2;
    }
}
