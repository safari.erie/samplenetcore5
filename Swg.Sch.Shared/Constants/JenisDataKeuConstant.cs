﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisDataKeuConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>()
        {
            {Rkas, "Rencana Anggaran Kegiatan Sekolah" },
            {Penagihan, "Penagihan" },
            {Penerimaan, "Penerimaan" },
            {Pengajuan, "Pengajuan" },
            {Pengeluaran, "Pengeluaran" },
        };
        public const int Rkas = 1;
        public const int Penagihan = 2;
        public const int Penerimaan = 3;
        public const int Pengajuan = 4;
        public const int Pengeluaran = 5;
    }
}
