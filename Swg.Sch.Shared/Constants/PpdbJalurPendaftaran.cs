﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JalurPendaftaranPpdbConstant
    {
        public static Dictionary<int, string> DictJalurPendaftaranPpdb = new Dictionary<int, string>()
        {
            {Prestasi,"Jalur Prestasi" },
            {Reguler,"Jalur Reguler" }
        };

        public const int Prestasi = 1;
        public const int Reguler = 2;
    }
}
