﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisPendaftaranPpdbConstant
    {
        public static Dictionary<int, string> DictJenisPendaftaranPpdb = new Dictionary<int, string>()
        {
            {Internal,"Internal" },
            {Eksternal,"Eksternal" }
        };

        public const int Internal = 1;
        public const int Eksternal = 2;
    }
}
