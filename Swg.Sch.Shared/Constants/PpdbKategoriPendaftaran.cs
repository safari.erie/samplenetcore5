﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class KategoriPendaftaranPpdbConstant
    {
        public static Dictionary<int, string> DictKategoriPendaftaranPpdb = new Dictionary<int, string>()
        {
            {Baru,"Siswa Baru" },
            {Pindahan,"Siswa Pindahan" }
        };

        public const int Baru = 1;
        public const int Pindahan = 2;
    }
}
