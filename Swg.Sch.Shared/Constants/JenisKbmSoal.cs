﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisKbmSoalConstant
    {
        public static Dictionary<int, string> DictJenisKbmSoal = new Dictionary<int, string>()
        {
            {Reguler,"Reguler" },
            {Remedial,"Remedial" },
            {Susulan,"Susulan" }
        };

        public const int Reguler = 1;
        public const int Remedial = 2;
        public const int Susulan = 3;
    }
}
