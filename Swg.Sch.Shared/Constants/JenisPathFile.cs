﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
	public class JenisPathFileConstant
	{
		public static Dictionary<int, string> DictJenisPathFile = new Dictionary<int, string> {
		{
			0,
			"Tidak Keduanya"
		},
		{
			1,
			"File"
		},
		{
			2,
			"Url"
		},
		{
			3,
			"Rubrik"
		}
	};

		public const int TidakKeduanya = 0;

		public const int File = 1;

		public const int Url = 2;

		public const int Rubrik = 3;
	}
}
