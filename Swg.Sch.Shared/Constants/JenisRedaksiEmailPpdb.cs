﻿using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisRedaksiEmailPpdbConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>()
        {
            {Registrasi,"Pendaftaran PPDB"},
            {DaftarTunggu,"Daftar Tunggu PPDB"},
            {LunasDaftar,"Pelunasan Biaya Pendaftaran PPDB"},
            {JadwalObservasi,"Jadwal Pelaksanaan Observasi"},
            {ObservasiDiterima,"Lulus Observasi"},
            {ObservasiDiterimaCicil,"Lulus Observasi dengan Cicilan"},
            {ObservasiUndurDiri,"Pengunduran Diri PPDB"},
            {ObservasiDitangguhkan,"Proses PPDB Ditangguhkan"},
            {ObservasiTidakDiterima,"Gagal Observasi"},
            {LunasPendidikan,"Pelunasan Biaya Pendidikan"},
            {PengesahanDiterima,"Calon Siswa Diterima"}
        };

        public const int Registrasi = 1;
        public const int DaftarTunggu = 2;
        public const int LunasDaftar = 3; 
        public const int JadwalObservasi = 4;
        public const int ObservasiDiterima = 5;
        public const int ObservasiDiterimaCicil = 6;
        public const int ObservasiUndurDiri = 7;
        public const int ObservasiDitangguhkan = 8;
        public const int ObservasiTidakDiterima = 9;
        public const int LunasPendidikan = 10;
        public const int PengesahanDiterima = 11;
    }
}
