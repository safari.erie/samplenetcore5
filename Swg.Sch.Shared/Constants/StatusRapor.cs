using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class StatusRapor
    {
        public static Dictionary<int, string> DictStatusRapor = new Dictionary<int, string>()
        {
            {Aktif,"Aktif" },
            {NonAktif,"NonAktif" }
        };

        public const int Aktif = 1;
        public const int NonAktif = -1;
    }
}
