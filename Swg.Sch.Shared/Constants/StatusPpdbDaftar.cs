﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class StatusPpdbDaftarConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>()
        {
            {AktifVa, "Dalam Proses (VA Aktif)" },
            {DaftarTunggu, "Daftar Tunggu" }
        };
        public const int AktifVa = 1;
        public const int DaftarTunggu = -1;
    }
}
