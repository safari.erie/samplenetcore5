﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisJabatanPegawaiContant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>();

        public static int Yay;
        public static int Dir;
        public static int SekDir;
        public static int ManDik;
        public static int ManUm;
        public static int ManHum;
        public static int ManSos;
        public static int ManKeu;
        public static int KaUnit;
        public static int WakaUnit;
        public static int SekUnit;
        public static int BendUnit;
        public static int StafUnit;
        public static int AdmDir;
        public static int WaliKelas;
        public static int WaWaliKelas;
    }
}
