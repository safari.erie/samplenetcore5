﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisStatusNikahConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>()
        {
            {Kawin,"Kawin" },
            {Cerai,"Cerai" },
            {CeraiMati,"Cerai Mati" },
            {BelumKawin,"Belum Kawin" }
        };
        public const int Kawin = 1;
        public const int Cerai = 2;
        public const int CeraiMati = 3;
        public const int BelumKawin = 4;
    }
}
