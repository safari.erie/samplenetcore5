﻿using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisPegawaiGolonganConstant
    {
        public static Dictionary<int, string> DictJenisPegawaiGolongan = new Dictionary<int, string>()
        {
            {Gol0,"N/A"},
            {Gol1A,"1A"},
            {Gol1B,"1B"},
            {Gol1C,"1C"},
            {Gol1D,"1D"},
            {Gol2A,"2A"},
            {Gol2B,"2B"},
            {Gol2C,"2C"},
            {Gol2D,"2D"},
            {Gol3A,"3A"},
            {Gol3B,"3B"},
            {Gol3C,"3C"},
            {Gol3D,"3D"},
            {Gol4A,"4A"},
            {Gol4B,"4B"},
            {Gol4C,"4C"},
            {Gol4D,"4D"}
        };
        public const int Gol0 = 0;
        public const int Gol1A = 1;
        public const int Gol1B = 2;
        public const int Gol1C = 3;
        public const int Gol1D = 4;
        public const int Gol2A = 5;
        public const int Gol2B = 6;
        public const int Gol2C = 7;
        public const int Gol2D = 8;
        public const int Gol3A = 9;
        public const int Gol3B = 10;
        public const int Gol3C = 11;
        public const int Gol3D = 12;
        public const int Gol4A = 13;
        public const int Gol4B = 14;
        public const int Gol4C = 15;
        public const int Gol4D = 16;
    }
}
