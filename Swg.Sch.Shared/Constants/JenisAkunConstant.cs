﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.Constants
{
    public class JenisAkunConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>();
        public static int Golongan;
        public static int Utama;
        public static int Madya;
        public static int Pratama;
        //public static int BukuBesar;
        public static int Detil;
    }
}
