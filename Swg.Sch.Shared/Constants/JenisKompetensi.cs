using System;
using System.Collections.Generic;

namespace Swg.Sch.Shared.Constants
{
    public class JenisKompetensiConstant
    {
        public static Dictionary<int, string> DictJenisKompetensi = new Dictionary<int, string>()
        {
            {Lainnya, "Lainnya" },
            {BahasaArab, "Bahasa Arab" },
            {BahasaInggris, "Bahasa Inggris" },
            {BahasaJepang, "Bahasa Jepang" },
            {BahasaMandarin, "Bahasa Mandarin" },
            {DesainGrafis, "Desain Grafis" },
            {KomputerJaringan, "Komputer Jaringan" },
            {WebProgrammer, "Web Programmer" },
            {Webmaster, "Webmaster" },
            {TrainerMotivator, "Trainer/Motivator" },
            {Penulis, "Penulis" },
            {Jurnalistik, "Jurnalistik" },
            {PublicSpeaking, "Public Speaking" },
        };

        public const int Lainnya = 0;
        public const int BahasaArab = 1;
        public const int BahasaInggris = 2;
        public const int BahasaJepang = 3;
        public const int BahasaMandarin = 4;
        public const int DesainGrafis = 5;
        public const int KomputerJaringan = 6;
        public const int WebProgrammer = 7;
        public const int Webmaster = 8;
        public const int TrainerMotivator = 9;
        public const int Penulis = 10;
        public const int Jurnalistik = 11;
        public const int PublicSpeaking = 12;
    }
}

