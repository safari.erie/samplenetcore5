﻿using System;
using System.Collections.Generic;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Shared.Interfaces
{
    public interface IKelasService
    {
        List<KelasModel> GetKelass(int IdUnit, out string oMessage);
        KelasModel GetKelas(int IdKelas, out string oMessage);
        string KelasAdd(int IdUser, int IdUnit, string Nama, string NamaSIngkat);
        string KelasEdit(int IdUser, int IdKelas, string Nama, string NamaSIngkat);
        string KelasDelete(int IdUser, int IdKelas);

        List<KelasParalelModel> GetKelasParalels(int IdKelas, out string oMessage);
        KelasParalelModel GetKelasParalel(int IdKelasParalel, out string oMessage);
        string KelasParalelAdd(int IdUser, int IdKelas, string Nama, string NamaSIngkat);
        string KelasParalelEdit(int IdUser, int IdKelasParalel, string Nama, string NamaSIngkat);
        string KelasParalelDelete(int IdUser, int IdKelasParalel);

        List<KelasParalelPegawaiModel> GetKelasParalelPegawais(int IdKelasParalel, out string oMessage);
        //string KelasParalelPegawaiAdd(int IdUser, int IdKelasParalel, List<PegawaiJenisModel> Pegawais);
        //string KelasParalelPegawaiDelete(int IdUser, int IdKelasParalel, List<PegawaiJenisModel> Pegawais);
    }
}
