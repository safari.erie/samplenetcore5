
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Shared.Interfaces
{
    public interface IBankService
    {
        string PartnerLogin(string PartnerId, string Password);
        string PartnerLogout(string PartnerId);
        string PartnerChangePassword(string PartnerId, string OldPassword, string NewPassword, string ConfirmPassword);

        string GetInfoSppSiswa(string PartnerId, string Nis, out PartnerSppModel Data);
        string SetLunasSppSiswa(string PartnerId, string Nis, double Rp, out string RefNo);
        string SetBatalSppSiswa(string PartnerId, string Nis, double Rp, string RefNo);
        string GetInfoTagihanPpdb(string PartnerId, string NoVa, out PartnerPpdbModel Data);
        string SetLunasTagihanPpdb(string PartnerId, string NoVa, double Rp, out string RefNo);
        string SetBatalTagihanPpdb(string PartnerId, string NoVa, double Rp, string RefNo);
        string GetInfoTagihanSiswaBaru(string PartnerId, string NoVa, out PpdbBiayaDaftarModel Data);
        string SetLunasTagihanSiswaBaru(string PartnerId, string NoVa, double Rp, out string RefNo);
        string SetBatalTagihanSiswaBaru(string PartnerId, string NoVa, double Rp, string RefNo);

        string LogPartnerRequest(PartnerRequestModel Data);
        string LogPartnerPayment(
            string KodeBayar,
            string Tipe,
            double JumlahBayar,
            string KeteranganGagal
        );
    }
}