﻿using System.Collections.Generic;
using Swg.Models;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Shared.Interfaces
{
    public interface IUnitService
    {
        List<ComboModel> GetStatusData();
        List<UnitReadModel> GetUnits(out string oMessage);
        UnitReadModel GetUnit(int IdUnit, out string oMessage);
        string UnitEdit(int IdUser, UnitAddEditModel Data);

        List<UnitGaleriReadModel> GetUnitGaleris(int IdUser, out string oMessage);
        UnitGaleriReadModel GetUnitGaleri(int IdUnitGaleri, out string oMessage);
        string UnitGaleriAdd(int IdUser, UnitGaleriAddEditModel Data);
        string UnitGaleriEdit(int IdUser, UnitGaleriAddEditModel Data);
        string UnitGaleriReject(int IdUser, int IdUnitGaleri);
        string UnitGaleriDelete(int IdUser, int IdUnitGaleri);

        List<UnitKegiatanReadModel> GetUnitKegiatans(int IdUser, out string oMessage);
        UnitKegiatanReadModel GetUnitKegiatan(int IdUnitKegiatan, out string oMessage);
        string UnitKegiatanAdd(int IdUser, UnitKegiatanAddEditModel Data);
        string UnitKegiatanEdit(int IdUser, UnitKegiatanAddEditModel Data);
        string UnitKegiatanReject(int IdUser, int IdUnitKegiatan);
        string UnitKegiatanDelete(int IdUser, int IdUnitKegiatan);


        List<PegawaiModel> GetUnitPegawais(int IdUnit, out string oMessage);
        string UnitPegawaiAdd(int IdUser, int IdUnit, List<PegawaiJenisModel> Pegawais);
        string UnitPegawaiDelete(int IdUser, int IdUnit, List<PegawaiJenisModel> Pegawais);

        List<UnitEkskulReadModel> GetUnitEkskuls(int IdUser, out string oMessage);
        UnitEkskulReadModel GetUnitEkskul(int IdUser, int IdUnitEkskul, out string oMessage);
        string UnitEkskulAdd(int IdUser, UnitEkskulAddEditModel Data, out string oMessage);
        string UnitEkskulEdit(int IdUser, UnitEkskulAddEditModel Data, out string oMessage);
        string UnitEkskulReject(int IdUser, int IdUnitEkskul, out string oMessage);
        string UnitEkskulDelete(int IdUser, int IdUnitEkskul, out string oMessage);

        List<UnitBannerReadModel> GetUnitBanners(out string oMessage);
        UnitBannerReadModel GetUnitBanner(int IdUnitBanner, out string oMessage);
        string UnitBannerAdd(UnitBannerAddEditModel Data, out string oMessage);
        string UnitBannerEdit(UnitBannerAddEditModel Data, out string oMessage);
        string UnitBannerDelete(int IdUnitBanner, out string oMessage);
    }
}
