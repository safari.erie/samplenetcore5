﻿using System;
using System.Collections.Generic;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Shared.Interfaces
{
    public interface IELearningLaporanService
    {
        List<ElDashboardModel> GetElDashboard(out string oMessage);
        ElMateriTugasModel GetElMateriTugas(int IdUser, string Tanggal, out string oMessage);
        List<ElMateriTugasKelasModel> GetElMateriTugasKelas(int IdUser, int IdUnit, string Tanggal, out string oMessage);
        List<ElMateriTugasKelasParalelModel> GetElMateriTugasKelasParalel(int IdUser, int IdKelas, string Tanggal, out string oMessage);
        ElMateriTugasListModel GetElMateriTugasList(int IdUser, int IdKelasParalel, string Tanggal, out string oMessage);

        ElKbmSiswaModel GetElKbmSiswa(int IdUser, string Tanggal, out string oMessage);
        List<ElKbmSiswaKelasModel> GetElKbmSiswaKelas(int IdUser, int IdUnit, string Tanggal, out string oMessage);
        List<ElKbmSiswaKelasParalelModel> GetElKbmSiswaKelasParalel(int IdUser, int IdKelas, string Tanggal, out string oMessage);
        ElKbmSiswaListModel GetElKbmSiswaList(int IdUser, int IdKelasParalel, string Tanggal, out string oMessage);

        ElAbsenSiswaModel GetElAbsenSiswa(int IdUser, string Tanggal, out string oMessage);
        List<ElAbsenSiswaKelasModel> GetElAbsenSiswaKelas(int IdUser, int IdUnit, string Tanggal, out string oMessage);
        List<ElAbsenSiswaKelasParalelModel> GetElAbsenSiswaKelasParalel(int IdUser, int IdKelas, string Tanggal, out string oMessage);
        List<ElAbsenSiswaMapelModel> GetElAbsenSiswaMapel(int IdUser, int IdKelasParalel, string Tanggal, out string oMessage);
        ElAbsenSiswaMapelListModel GetElAbsenSiswaList(int IdUser, int IdKelasParalel, int IdMapel, string Tanggal, out string oMessage);

        ElEvalHarianModel GetElEvalHarian(int IdUser, string Tanggal, out string oMessage);
        List<ElEvalHarianKelasModel> GetElEvalHarianKelas(int IdUser, int IdUnit, string Tanggal, out string oMessage);
        List<ElEvalHarianKelasParalelModel> GetElEvalHarianKelasParalel(int IdUser, int IdKelas, string Tanggal, out string oMessage);
        List<ElEvalHarianListModel> GetElEvalHarianList(int IdUser, int IdKelasParalel, string Tanggal, out string oMessage);
        ElEvalHarianSiswaModel GetElEvalHarianSiswa(int IdUser, int IdSiswa, string Tanggal, int HariKe, out string oMessage);

        List<SheetLaporanElearningProgressPegawai> GetLaporanElearningProgressPegawai(int IdUser, string TanggalAwal, string TanggalAkhir, int IdUnit, out string oMessage);
    }
}
