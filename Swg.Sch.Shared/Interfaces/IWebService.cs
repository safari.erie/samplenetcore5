﻿using System.Collections.Generic;
using Swg.Models;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Shared.Interfaces
{
    public interface IWebService
    {
        WebHomeModel GetHome(out string oMessage);
        List<ApplTaskModel> GetMenus(int IdAppl, int IdUser, out string oMessage);
        WebAboutModel GetAbout(out string oMessage);
        WebPropenModel GetPropen(int IdUnit, out string oMessage);
        List<WebKegiatanModel> GetAllUnitKegiatans(string Search, out string oMessage);
        List<WebGaleriImageModel> GetGaleriUnit(out string oMessage);
        WebPpdbListInfoModel GetPpdbInfo(out string oMessage);
        WebPropenModel GetPropenDkm(out string oMessage);

        WebKegiatanByUrlModel GetKegiatanByUrl(string Url, out string oMessage);

        List<SiswaRaporModel> GetSiswaRapors(string Nis, out string oMessage);
        SiswaRaporModel GetSiswaRapor(int IdSiswa, int IdJenisRaport, out string oMessage);
        SiswaModel GetSiswaByNis(string Nis, out string oMessage);

        // add by eri konten sekolah service
        WebSekolahModel GetProfSekolah(out string oMessage);

        WebSekolahVisiMisi GetProfileSekolah(out string oMessage);

        List<WebPegawaiModel> GetJabatanPegawai(out string oMessage);

        List<WebGaleriVideoModel> GetGaleriSekolah(int JenisGaleri, out string oMessage);

        WebSekolahModel GetKegiatanSekolah(out string oMessage);

        ApplSettingModel GetApplSetting(string code, out string oMessage);

        WebUnitKepsekModel GetPegawaiByUsername(string Username, out string oMessage);

        List<KalenderPendidikanModel> GetKalenderPendidikan(int Semester, out string oMessage);
        List<SekolahPrestasiModel> GetSekolahPrestasi(int IdJenisGaleri, out string oMessage);
        List<SekolahFasilitasModel> GetSekolahFasilitas(out string oMessage);

        string AddTestimoni(WebTestimoniAddModel Data, out string oMessage);
        List<TestimoniModel> GetTestimonis(out string oMessage);
        WebKegiatanModel GetEkskul(int IdUnitEkskul, out string oMessage);
        List<UnitBannerReadModel> GetUnitBanner(int IdUnit, out string oMessage);



    }
}
