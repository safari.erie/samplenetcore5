﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Swg.Models;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Shared.Interfaces
{
    public interface IPpdbService
    {
        string UploadVA(int IdUser, List<PpdbVaSheetModel> Data);
        List<PpdbVaSheetModel> GetListVa(int IdUser, out string oMessage);
        string PpdbAdd(PpdbModelAddEdit Data);
        string PpdbEdit(PpdbModelAddEdit Data);
        string PpdbDelete(int IdPpdb, out string oMessage);
        List<PpdbRedaksiEmailModel> GetPpdbRedaksiEmails(out string oMessage);
        PpdbRedaksiEmailModel GetPpdbRedaksiEmail(int IdPpdbRedaksiEmail, out string oMessage);
        string PpdbRedaksiEmailAdd(PpdbRedaksiEmailModel Data);
        string PpdbRedaksiEmailEdit(PpdbRedaksiEmailModel Data);
        string PpdbRedaksiEmailDelete(int IdPpdbRedaksiEmail, out string oMessage);
        List<PpdbInfoModelRead> GetPpdbInfos(out string oMessage);
        PpdbInfoModelRead GetPpdbInfo(int IdPpdbInfo, out string oMessage);
        string PpdbInfoAdd(PpdbInfoModelAddEdit Data);
        string PpdbInfoEdit(PpdbInfoModelAddEdit Data);
        string PpdbInfoDelete(int IdPpdbInfo, out string oMessage);
        List<PpdbBiayaModel> GetPpdbBiayas(out string oMessage);
        PpdbBiayaModel GetPpdbBiaya(int IdPpdbBiaya, out string oMessage);
        string PpdbBiayaAdd(int IdUser, PpdbBiayaAddEditModel Data);
        string PpdbBiayaEdit(int IdUser, PpdbBiayaAddEditModel Data);
        string PpdbBiayaDelete(int IdUser, int IdPpdbBiaya, out string oMessage);
        List<ComboModel> GetPpdbJenisBiaya(int IdUser, int IdJenisBiayaPpdb, out string oMessage);
        string PpdbJenisBiayaAdd(int IdUser, ComboModel Data);
        string PpdbJenisBiayaEdit(int IdUser, ComboModel Data);
        string PpdbJenisBiayaDelete(int IdUser, int IdJenisBiayaPpdb, out string oMessage);
        List<PpdbSeragamListModel> GetPpdbSeragamByUnit(int IdUnit, out string oMessage);
        List<PpdbSeragamListModel> GetPpdbSeragams(int IdUser, out string oMessage);
        PpdbSeragamModel GetPpdbSeragam(int IdPpdbSeragam, out string oMessage);
        string PpdbSeragamAdd(int IdUser, PpdbSeragamModel Data);
        string PpdbSeragamEdit(int IdUser, PpdbSeragamModel Data);
        string PpdbSeragamDelete(int IdUser, int IdPpdbSeragam, out string oMessage);
        List<PpdbKelasViewModel> GetPpdbKelass(out string oMessage);
        string PpdbKelasAdd(int IdUser, PpdbKelasModel Data);
        string PpdbKelasEdit(int IdUser, PpdbKelasEditModel Data);
        string PpdbKelasDelete(int Iduse, int IdPpdbKelas);
        List<PpdbJadwalViewModel> GetPpdbJadwals(out string oMessage);
        string PpdbJadwalAdd(int IdUser, PpdbJadwalModel Data);
        string PpdbJadwalEdit(int IdUser, PpdbJadwalEditModel Data);
        string PpdbJadwalDelete(int Iduser, int IdUnit, int IdJenisGelombangPpdb, int IdJenisPendaftaran, int IdJalurPendaftaran);

        List<PpdbDaftarModel> GetPpdbDaftars(int IdUser, int Status, out string oMessage);
        PpdbBiayaDaftarModel GetPpdbDaftarBiaya(int IdPpdbDaftar, out string oMessage);
        string SetLunasBiayaDaftar(int IdUser, int IdPpdbDaftar, string TanggalLunas, string Keterangan);
        string SetTidakDaftarTunggu(int IdUser, int IdPpdbDaftar);
        PpdbDaftarModel GetPpdbDaftar(int IdPpdbDaftar, out string oMessage);
        PpdbDaftarModel GetPpdbDaftar(int IdPpdbDaftar, string KodePin, out string oMessage);
        List<PpdbSiswaListModel> GetPpdbSiswas(int IdUser, int Status, out string oMessage);
        PpdbSiswaModel GetPpdbSiswa(int IdPpdbDaftar, out string oMessage);
        string SetHadirTestObservasi(int IdUser, int IdPpdbDaftar, string Catatan);
        string SetHadirWawancara(int IdUser, int IdPpdbDaftar, string Catatan);
        string InputObservasi(int IdUser, int IdPpdbDaftar, int StatusHasilObservasi, string Catatan, List<PpdbBiayaSiswaDetilAddModel> Biayas, List<PpdbBiayaSiswaCicilAddModel> Cicils);
        string VerifikasiObservasi(int IdUser, int IdPpdbDaftar, int StatusVerfikasiObservasi, string Catatan);
        string SetLunasBiayaSiswa(int IdUser, int IdPpdbDaftar, string TanggalLunas, string Catatan, int CilKe);
        string BackToHadirObservasi(int IdUser, int IdPpdbDaftar);
        string Pengesahan(int IdUser, int IdPpdbDaftar);

        List<PpdbKuisionerModel> GetPertanyaanKuisioner(int IdUnit, int IdJenisPendaftaran, int idJalurPendaftaran, int IdJenisPertanyaan, out string oMessage);
        List<PpdbBiayaModel> GetPpdbBiayaByUnit(int IdPpdbKelas, out string oMessage);

        string ResendEmail(int IdPpdbDaftar, int IdProses);
        string EditPpdbDaftar(int IdUser, int IdPpdbDaftar, PpdbDaftarAddModel Data);
        string EditPpdbSiswa(
            int IdUser,
            PpdbSiswaModel Siswa,
            List<PpdbSiswaOrtuAddModel> Ortus,
            List<PpdbSiswaRaporAddModel> Rapors,
            List<PpdbSiswaPrestasiAddModel> Prestasis,
            IFormFile FileFoto,
            IFormFile FileKtpIbu,
            IFormFile FileKtpAyah,
            IFormFile FileKk,
            IFormFile FileAkte,
            IFormFile FileSuratKesehatan,
            IFormFile FileBebasNarkoba,
            List<IFormFile> FileRapors,
            List<IFormFile> FilePrestasis
        );
        List<PpdbSiswaListModel> GetPpdbSiswasByAdmin(int IdUser, out string oMessage);

        List<SheetPpdbDaftarSiswaModel> GetPpdbSiswas(int IdUser, out string oMessage);

        List<PpdbSiswaObservasiWawancara> GetPpdbSiswaObservasiWawancaras(int IdUser, out string oMessage);

        List<PpdbSiswaObservasiWawancara> GetPpdbSiswaObservasiWawancaras(int IdUser, int Tipe, out string oMessage);

        string UploadFileAkteKk(int IdPpdbDaftar, IFormFile FileAkte, IFormFile FileKK);
        string UploadAbsensiObservasi(int IdUser, List<PpdbAbsensiSheetModel> Data);
        string UploadAbsensiWawancara(int IdUser, List<PpdbAbsensiSheetModel> Data);

        List<PpdbSiswaModel> GetPpdbSiswaDatas(int IdUser, int Status, out string oMessage);

        string UploadInputObservasiMassal(int IdUser, List<PpdbInputObservasiSheetModel> Data);

        SheetLaporanGelombangPpdbModel GetLaporanGelombangPpdb(int IdUser, string TanggalAwal, string TanggalAkhir, SheetLaporanStatikGelombangPpdbModel Data, out string oMessage);
        PpdbLaporanModel GetLaporanGelombangPpdbTest(int IdUser, string TanggalAwal, string TanggalAkhir, out string oMessage);

        string DaftarPeminatPpdb(AddPpdbDaftarPeminat Data, out string oMessage);
        List<PpdbDaftarPeminat> GetDaftarPeminatPpdbs(out string oMessage);
        PpdbDaftarPeminat GetDaftarPeminatPpdb(int IdPpdbDaftarPeminat, out string oMessage);
        List<PpdbKontenListModel> GetPpdbKontens(out string oMessage);
        PpdbKontenByModel GetPpdbKonten(int IdPpdbKonten, out string oMessage);
        string AddPpdbKonten(int IdJenisPpdbKonten, string Deskripsi, out string oMessage);
        string EditPpdbKonten(int IdPpdbKonten, string Deskripsi, out string oMessage);
        string UploadLembarKomitmen(int IdPpdbDaftar, string Pin, IFormFile FileLembarKomitmen, out string oMessage);

        string GenerateBaruNameTag(int IdPpdbDaftar, out string oMessage);
        string DownloadBerkas(out string oMessage);
    }
}
