﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Swg.Models;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Shared.Interfaces
{
    public interface IKeuProgKegService
    {
        List<ComboModel> GetCoas(out string oMessage);
        List<ComboModel> GetUnitPrograms(int IdUnitProkeg, out string oMessage);
        List<KeuUnitListModel> GetUnits(int IdUser, int IdTahunAjaran, out string oMessage);
        List<KeuUnitProListModel> GetPrograms(int IdUnitProkeg, out string oMessage);
        List<KeuUnitProKegListModel> GetKegiatans(int IdUser, int IdUnitProkeg, int IdProgram, out string oMessage);
        List<KeuUnitProKegWajibCoaListModel> GetKegiatanWajibCoas(int IdUser, int IdUnitProkegWajib, out string oMessage);
        List<KeuUnitProKegTambahanCoaListModel> GetKegiatanTambahanCoas(int IdUser, int IdUnitProkegTambahan, out string oMessage);
        KeuUnitProKegWajibCoaListModel GetKegiatanWajibCoa(int IdUnitProkegWajibCoa, out string oMessage);
        KeuUnitProKegTambahanCoaListModel GetKegiatanTambahanCoa(int IdUnitProkegTambahanCoa, out string oMessage);

        string KegiatanWajibEdit(int IdUser, int IdUnitProkegWajib, string Tanggal);

        string KegiatanTambahanAdd(int IdUser, int IdUnitProkeg, int IdProgram, string NamaKegiatan, string Tanggal, out int IdUnitProkegTambahan);
        string KegiatanTambahanEdit(int IdUser, int IdUnitProkegTambahan, string NamaKegiatan, string Tanggal);
        string KegiatanTambahanDelete(int IdUser, int IdUnitProkegTambahan);

        string KegiatanWajibCoaAdd(int IdUser, KeuUnitProKegCoaWajibAddModel Data);
        string KegiatanWajibCoaEdit(int IdUser, KeuUnitProKegCoaWajibEditModel Data);
        string KegiatanWajibCoaDelete(int IdUser, int IdUnitProkegWajibCoa);

        string KegiatanTambahanCoaAdd(int IdUser, KeuUnitProKegCoaTambahanAddModel Data);
        string KegiatanTambahanCoaEdit(int IdUser, KeuUnitProKegCoaTambahanEditModel Data);
        string KegiatanTambahanCoaDelete(int IdUser, int IdUnitProkegTambahanCoa);

        string ApproveByKaUnit(int IdUser, int IdUnitProkeg, int StatusProses, string Catatan);
        string ApproveByManUnit(int IdUser, int IdUnitProkeg, int StatusProses, string Catatan);
        string ApproveByManKeu(int IdUser, int IdUnitProkeg, int StatusProses, string Catatan);
        string ApproveByDirektur(int IdUser, int IdUnitProkeg, int StatusProses, string Catatan);
        string SetSah(int IdUser, int IdUnitProkeg);


        KeuUnitProKegDownloadModel GetUnitDownload(int IdUser, int IdUnitProkeg, out string oMessage);
    }
}
