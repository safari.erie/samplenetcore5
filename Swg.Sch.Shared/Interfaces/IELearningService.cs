﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Swg.Models;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Shared.Interfaces
{
    public interface IELearningService
    {
        List<ComboModel> GetJenisBantuan();
        List<KbmSiswaListModel> GetKbmBySiswa(int IdSiswa, string Tanggal, out string oMessage);
        KbmSiswaModel GetKbmBySiswa(int IdSiswa, int IdKbmMateri, out string oMessage);

        List<KbmGuruListModel> GetKbmByPegawai(int IdPegawai, string Tanggal, out string oMessage);
        KbmGuruModel GetKbmByPegawai(int IdKbmMateri, out string oMessage);

        List<KbmSiswaListModel> GetKbmTugasSiswas(int IdKbmMateri, out string oMessage);

        string UpdateMateri(int IdPegawai, int IdKbmMateri, KbmUpdateMateriModel Data, IFormFile FileMateri, List<KbmUpdateSoalModel> FileSoals, List<KbmUpdateMateriMediaModel> FileMedias);
        string UpdateMateri(int IdPegawai, List<int> IdKbmMateris, KbmUpdateMateriModel Data, IFormFile FileMateri, List<KbmUpdateSoalModel> FileSoals, List<KbmUpdateMateriMediaModel> FileMedias);
        string UploadJawaban(int IdSiswa, int IdKbmMateri, int IdJenisSoal, int IdJenisBantuanOrtu, string NamaUrl, IFormFile FileUpload);
        string UploadJawaban(int IdSiswa, int IdKbmMateri, int IdJenisSoal, int IdJenisBantuanOrtu, string NamaUrl, List<IFormFile> FileUploads);
        string UpdateNilai(int IdPegawai, int IdKbmMateri, int IdSiswa, double NilaiAngka, string NilaiHuruf, int IsRemedial, string Catatan);
        string UpdateAdbsensi(int IdPegawai, int IdKbmMateri, string NamaUrl, List<int> IdSiswas);
        string UploadNilaiSiswa(int IdPegawai, List<SheetNilaiSiswa> Data);
        string UploadJawabanSusulan(int IdPegawai, int IdSiswa, int IdKbmMateri, string Catatan, IFormFile FileJawaban);

        /// EVALUASI HARIAN SISWA
        string DeleteJenisEvaluasiHarian(int IdJenisEvaluasiHarian);
        string JenisEvaluasiHarianAddEdit(EvalHarianModel Data);
        string AddEvalHarianUnit(int IdUnit, List<int> IdJenisEvaluasiHarians);
        EvalHarianUnitModel GetJenisEvaluasiHariansByUnit(int IdUnit, out string oMessage);
        List<EvalHarianModel> GetJenisEvaluasiHarians(out string oMessage);
        List<ComboModel> GetJenisEvaluasiHarian(int IdSiswa, out string oMessage);
        List<KbmEvalHarianListModel> GetKbmEvalHarians(int IdSiswa, string Tanggal, out string oMessage);
        KbmEvalHarianModel GetKbmEvalHarian(int IdSiswa, string Tanggal, int IdJenisEvaluasiHarian, out string oMessage);
        string EvaluasiHarianAdd(int IdSiswa, List<KbmEvalHarianAddModel> Data);

        /// EVALUASI HARIAN PEGAWAI
        string DeleteJenisEvaluasiHarianPegawai(int IdJenisEvaluasiHarianPegawai);
        string JenisEvaluasiHarianAddEditPegawai(EvalHarianPegawaiModel Data);
        string AddEvalHarianUnitPegawai(int IdUnit, List<int> IdJenisEvaluasiHarianPegawais);
        EvalHarianUnitPegawaiModel GetJenisEvaluasiHariansByUnitPegawai(int IdUnit, out string oMessage);
        List<EvalHarianPegawaiModel> GetJenisEvaluasiHarianPegawais(out string oMessage);
        List<ComboModel> GetJenisEvaluasiHarianPegawai(int IdPegawai, out string oMessage);
        List<KbmEvalHarianListPegawaiModel> GetKbmEvalHarianPegawais(int IdPegawai, string Tanggal, out string oMessage);
        KbmEvalHarianPegawaiModel GetKbmEvalHarianPegawai(int IdPegawai, string Tanggal, int IdJenisEvaluasiHarianPegawai, out string oMessage);
        string EvaluasiHarianAddPegawai(int IdPegawai, List<KbmEvalHarianAddPegawaiModel> Data);

        string UpdateKelompokQuran(int IdUser, KelasQuranModel Data);
        string UpdateKelompokQuranSiswa(int IdUser, int IdKelasQuran, List<int> IdSiswas);
        List<KelasQuranModel> GetKelompokQuran(int IdUser, out string oMessage);
        List<KelompokQuranSiswaModel> GetKelompokQuranSiswa(int IdKelasQuran, out string oMessage);
        string AddKelompokQuranExcel(int IdUser, KelasQuranAllExcelModel Data);
        string AddKelompokQuranJadwalExcel(int IdUser, List<KelasQuranJadwalExcelModel> Data);
        string AddKelompokQuranSiswaExcel(int IdUser, List<KelasQuranSiswaExcelModel> Data);


        string DeleteFileMateri(int IdUser, int IdKbmMateri);
        string DeleteFileSoal(int IdUser, int IdKbmMateri, int IdJenisSoal);
        string DeleteFileMedia(int IdUser, int IdKbmMedia);
        string ResetMateriSoal(int IdUser, int IdKbmMateri);

        string SetJadwalKelasParalelNextWeek(int IdUser, List<SheetJadwalKbmModel> Data);
        string SetJadwalKelasParalelNextWeek(int IdUser, int Kelompok, List<int> IdKelasParalels);
        string SetJadwalKelasParalel(int IdUser, int Kelompok, List<int> IdKelasParalels);
        List<SheetJadwalKbmModel> GetKbms(int IdUser, int IdKelompok, out string oMessage);
        List<SheetJadwalKbmModel> GetKbmsByKelas(int IdUser, int IdKelompok, int IdKelas, out string oMessage);
        List<SheetJadwalKbmModel> GetKbmsByKelasParalelKelompok(int IdKelasParalel, int IdKelompok, out string oMessage);
        string UploadJadwalKbm(int IdUser, List<SheetJadwalKbmModel> Data);
        KbmJadwalUnitModel GetJadwalKbmsUnit(int IdUser, int IdUnit, string Tanggal, out string oMessage);

        List<MapelModel> GetMapels(out string oMessage);
        MapelModel GetMapel(int IdMapel, out string oMessage);
        string MapelAddEdit(int IdUser, MapelModel Data);
        string MapelDelete(int IdUser, int IdMapel);

        string PenilaianDiriSiswaAdd(PenilaianDiriSiswaAddModel Data);
        List<PenilaianDiriSiswaListModel> GetPenilaianDiriSiswa(int IdSiswa, int IdTahunAjaran, int IdSemester, out string oMessage);
    }
}
