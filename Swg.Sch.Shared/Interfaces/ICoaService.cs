﻿using Swg.Models;
using Swg.Sch.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Swg.Sch.Shared.Interfaces
{
    public interface ICoaService
    {
        List<ComboModel> GetJenisAkun();
        List<ComboModel> GetJenisTransaksi();

        string AddJenisSatuan(string Nama, string NamaSingkat);
        string EditJenisSatuan(int IdJenisSatuan, string Nama, string NamaSingkat);
        string DeleteJenisSatuan(int IdJenisSatuan);
        List<ComboModel> GetJenisSatuans(out string oMessage);

        string AddJenisVolume(string Nama, string NamaSingkat);
        string EditJenisVolume(int IdJenisVolume, string Nama, string NamaSingkat);
        string DeleteJjenisVolume(int IdJenisVolume);
        List<ComboModel> GetJenisVolumes(out string oMessage);

        string AddCoa(CoaAddModel Data);
        string EditCoa(CoaAddModel Data);
        string DeleteCoa(int IdCoa);
        List<CoaModel> GetCoas(out string oMessage);
        List<CoaModel> GetCoas(int IdJenisAkun, out string oMessage);
        List<CoaModel> GetCoasByParent(int IdCoa, out string oMessage);
        CoaModel GetCoa(string Kode, out string oMessage);
        CoaModel GetCoa(int IdCoa, out string oMessage);
    }
}
