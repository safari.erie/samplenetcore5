﻿using Swg.Sch.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Sch.Shared.Interfaces
{
    public interface IKeuPtgjwbService
    {
        List<KeuPengajuanListModel> GetPengajuans(int IdUser, int IdTahunAjaran, out string oMessage);
        List<KeuPengajuanCoaListModel> GetPengajuanCoas(int IdUser, int IdPengajuan, out string oMessage);
        string SetPilihPengajuan(int IdUser, int IdPengajuan);

        List<KeuPtgjwbListModel> GetPertanggungjawabans(int IdUser, int IdTahunAjaran, out string oMessage);
        List<KeuPtgjwbCoaListModel> GetPertanggungjawabanCoas(int IdUser, int IdPengajuan, out string oMessage);

        string PertanggungjawabanDelete(int IdUser, int IdPengajuan);

        string PertanggungjawabanCoaAdd(int IdUser, KeuPtgjwbCoaAddModel Data);
        string PertanggungjawabanCoaEdit(int IdUser, KeuPtgjwbCoaEditModel Data);
        string PertanggungjawabanCoaDelete(int IdUser, int IdPtgjwbCoa);

        string SetDilaporkan(int IdUser, int IdPengajuan);
        string SetDibukukan(int IdUser, int IdPengajuan, int StatusProses, string Catatan);
        KeuPtgjwbDownloadModel GetPertanggungjawabanDownload(int IdUser, int IdPengajuan, out string oMessage);

    }
}
