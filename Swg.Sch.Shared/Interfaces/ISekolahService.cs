﻿using System.Collections.Generic;
using Swg.Models;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Shared.Interfaces
{
    public interface ISekolahService
    {
        List<ComboModel> GetJenisVisiMisi();
        List<ComboModel> GetJenisGaleri();
        List<ComboModel> GetJenisPegawai();
        SekolahReadModel GetSekolah(out string oMessage);
        string SekolahEdit(int IdUser, SekolahAddEditModel Data);

        List<SekolahVisiMisiModel> GetSekolahVisiMisis(int IdUser, out string oMessage);
        SekolahVisiMisiModel GetSekolahVisiMisi(int IdSekolahVisiMisi, out string oMessage);
        string SekolahVisiMisiAdd(int IdUser, SekolahVisiMisiAddModel Data);
        string SekolahVisiMisiEdit(int IdUser, SekolahVisiMisiEditModel Data);
        string SekolahVisiMisiDelete(int IdUser, int IdSekolahVisiMisi);

        List<SekolahGaleriReadModel> GetSekolahGaleris(out string oMessage);
        SekolahGaleriReadModel GetSekolahGaleri(int IdSekolahGaleri, out string oMessage);
        string SekolahGaleriAdd(int IdUser, SekolahGaleriAddEditModel Data);
        string SekolahGaleriEdit(int IdUser, SekolahGaleriAddEditModel Data);
        string SekolahGaleriDelete(int IdUser, int IdSekolahGaleri);

        List<SekolahKegiatanReadModel> GetSekolahKegiatans(out string oMessage);
        SekolahKegiatanReadModel GetSekolahKegiatan(int IdSekolahKegiatan, out string oMessage);
        string SekolahKegiatanAdd(int IdUser, SekolahKegiatanAddEditModel Data);
        string SekolahKegiatanEdit(int IdUser, SekolahKegiatanAddEditModel Data);
        string SekolahKegiatanDelete(int IdUser, int IdSekolahKegiatan);

        List<PegawaiModel> GetPenguruss(out string oMessage);
        //string SekolahPengurusAdd(int IdUser, List<PegawaiJenisModel> Pegawais);
        //string SekolahPengurusDelete(int IdUser, List<PegawaiJenisModel> Pegawais);

        List<TahunAjaranModel> GetTahunAjarans(out string oMessage);
        TahunAjaranModel GetTahunAjaran(int IdTahunAjaran, out string oMessage);
        string TahunAjaranAdd(int IdUser, TahunAjaranAddModel Data);
        string TahunAjaranEdit(int IdUser, TahunAjaranEditModel Data);
        string SetTahunAjaranActive(int IdUser, int IdTahunAjaran);

        WebPpdbListInfoModel GetPpdbListInfo(out string oMessage);
        #region dashboard
        DashboardPppdbModel GetDashboardPppdb(out string oMessage);
        #endregion


        List<KalenderPendidikanModel> GetKalenderPendidikans(out string oMessage);
        KalenderPendidikanModel GetKalenderPendidikan(int IdKalenderPendidikan, out string oMessage);
        string KalenderPendidikanAdd(KalenderPendidikanAddEditModel Data, out string oMessage);
        string KalenderPendidikanEdit(KalenderPendidikanAddEditModel Data, out string oMessage);
        string KalenderPendidikanDelete(int IdKalenderPendidikan, out string oMessage);


        List<SekolahFasilitasModel> GetSekolahFasilitass(out string oMessage);
        SekolahFasilitasModel GetSekolahFasilitas(int IdSekolahFasilitas, out string oMessage);
        string SekolahFasilitasAdd(SekolahFasilitasAddEditModel Data, out string oMessage);
        string SekolahFasilitasEdit(SekolahFasilitasAddEditModel Data, out string oMessage);
        string SekolahFasilitasDelete(int IdSekolahFasilitas, out string oMessage);

        List<SekolahPrestasiModel> GetSekolahPrestasis(out string oMessage);
        SekolahPrestasiModel GetSekolahPrestasi(int IdSekolahPrestasi, out string oMessage);
        string SekolahPrestasiAdd(int IdUser, SekolahPrestasiAddEditModel Data, out string oMessage);
        string SekolahPrestasiEdit(int IdUser, SekolahPrestasiAddEditModel Data, out string oMessage);
        string SekolahPrestasiDelete(int IdSekolahPrestasi, out string oMessage);
    }
}
