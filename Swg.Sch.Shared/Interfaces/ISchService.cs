﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.AspNetCore.Http;
using Swg.Models;
using Swg.Sch.Shared.ViewModels;
namespace Swg.Sch.Shared.Interfaces
{
    public interface ISchService
    {
        List<ComboModel> GetAgamas();
        List<ComboModel> GetJalurPendaftarans();
        List<ComboModel> GetJenisPekerjaans(out string oMessage);
        List<ComboModel> GetJenisPendaftarans();
        List<ComboModel> GetJenisPrestasis();
        List<ComboModel> GetKategoriDaftars();
        List<ComboModel> GetJenisPegawais();
        List<ComboModel> GetJenisPegawaiGolongans();
        List<ComboModel> GetJenisRedaksiEmails();
        List<ComboModel> GetJenisBiayaPpdbs(out string oMessage);
        List<ComboModel> GetGelombangPpdbs();
        List<ComboModel> GetUnits();
        List<ComboModel> GetTingkatPrestasiSiswas();
        List<ComboModel> GetStatusPpdbs();
        List<ComboModel> GetJenisWargaNegara();
        List<ComboModel> GetTinggalSiswa();
        List<ComboModel> GetJenisTransportasi();
        List<ComboModel> GetJenisBahasa();
        List<ComboModel> GetStatusHasilObservasi();
        List<ComboModel> GetStatusVerifikasiObservasi();
        List<ComboModel> GetKelasPpdb(int IdUnit, out string oMessage);
        List<ComboModel> GetKelasPpdbs(out string oMessage);
        List<ComboModel> GetJenisHobis(out string oMessage);
        List<ComboModel> GetJenisCita2s(out string oMessage);
        List<ComboModel> GetProvinsi(out string oMessage);
        List<ComboModel> GetKabkot(int IdProvinsi, out string oMessage);
        List<ComboModel> GetKecamatan(int IdKabkot, out string oMessage);
        List<ComboModel> GetDesa(int IdKecamatan, out string oMessage);

        string GeneratePasswordAllPegawai();
        string GeneratePasswordAllSiswa();

        List<PegawaiModel> GetPegawais(int Status, out string oMessage);
        PegawaiModel GetPegawai(int IdPegawai, out string oMessage);
        List<PegawaiModel> GetPegawais(IDbConnection conn, int Status, out string oMessage);
        PegawaiModel GetPegawai(IDbConnection conn, int IdPegawai, out string oMessage);

        List<SiswaListModel> GetSiswas(int Status, out string oMessage);
        List<SiswaListModel> GetSiswas(IDbConnection conn, int Status, out string oMessage);
        List<SiswaListModel> GetSiswasByUnit(int IdUnit, out string oMessage);
        List<SiswaListModel> GetSiswasByKelas(int IdKelas, out string oMessage);
        List<SiswaListModel> GetSiswasByKelasParalel(int IdKelasParalel, out string oMessage);

        SiswaModel GetSiswa(int IdSiswa, out string oMessage);
        SiswaModel GetSiswa(IDbConnection conn, int IdSiswa, out string oMessage);
        KelasQuranSiswaModel GetKelasQuranSiswa(IDbConnection conn, int IdSiswa, string Tanggal, out string oMessage);
        SiswaTagihanModel GetSiswaTagihan(IDbConnection conn, string Nis, out string oMessage);

        List<TahunAjaranModel> GetTahunAjarans(out string oMessage);
        TahunAjaranModel GetTahunAjaran(int IdTahunAjaran, out string oMessage);
        TahunAjaranModel GetTahunAjaran(out string oMessage);
        TahunAjaranModel GetTahunAjaranPpdb(out string oMessage);
        SemesterModel GetSemester(out string oMessage);

        string TahunAjaranAdd(int IdTahunAjaran, string Nama, string NamaSingkat, string TanggalMulai, string TanggalSelesai);
        int InsertKbmJadwal(IDbConnection conn, string Hari, string JamMulai, string JamSelesai, out string oMessage);
        string SetInitialData(int IdUser, ExcelSekolahModel Data);
        string UploadJadwalKbmTemp(int IdUser, List<SheetJadwalKbmModel> Data);

        List<SheetKelasModel> GetKelass(out string oMessage);
        List<SheetMapelModel> GetMapels(out string oMessage);
        List<SheetKelQuranModel> GetKelQurans(out string oMessage);
        List<SheetJadwalQuranModel> GetJadwalQurans(out string oMessage);
        List<SheetKelQuranSiswaModel> GetKelQuranSiswas(out string oMessage);
        List<SheetJenisEvaluasiHarianModel> GetJenisEvaluasiHarians(out string oMessage);
        List<SheetJenisJilidQuranModel> GetJenisJilidQurans(out string oMessage);

        PegawaiUnitModel GetUnitPegawai(int IdUser, out string oMessage);

        bool CanAksesByJabatan(IDbConnection conn, int IdJabatan, int IdHakAkses);
        bool CanAksesByUser(IDbConnection conn, int IdJabatan, int IdHakAkses);
        string GetLevelJabatanUnitPegawai(IDbConnection conn, int IdUser, out int LevelJabatan, out int IdUnit);

        List<HariLiburModel> GetHariLiburs(out string oMessage);
        List<HariLiburModel> GetHariLiburs(IDbConnection conn, out string oMessage);
        HariLiburModel GetHariLibur(string Tanggal, out string oMessage);
        HariLiburModel GetHariLibur(IDbConnection conn, string Tanggal, out string oMessage);
        string HariLiburAddEdit(HariLiburModel Data);
        string HariLiburDelete(HariLiburModel Data);

        string AddHariLiburUnit(int IdUnit, List<int> IdHariLiburs);
        HariLiburUnitModel GetHariLibursByUnit(int IdUnit, out string oMessage);
        HariLiburModel GetHariLiburByUnit(int IdUnit, string Tanggal, out string oMessage);


        #region PPDB
        PpdbModelRead GetPpdb(out string oMessage);

        string GetRedaksiEmailPpdbBody(int IdJenisRedaksiEmailPpdb, out string MessageBody1, out string MessageBody2);
        string SetHtmlEmailPpdbMessage(string MessageBody1, string MessageBody2, int IdPpdbDaftar, string NamaUnit, string JalurPendaftaran, string JenisPendaftaran, string Nama, string TanggalLahir, string TahunAjaran, string NamaSekolah, string KetuaPpdb);
        string Registrasi(PpdbDaftarAddModel Data, out string oMessage);
        PpdbBiayaDaftarModel GetPpdbDaftarBiaya(IDbConnection conn, int IdPpdbDaftar, out string oMessage);
        string SetLunasBiayaDaftar(IDbConnection conn, int IdUser, string RefNo, int IdPpdbDaftar, DateTime TanggalLunas, double Rp, string Catatan);

        PpdbDaftarModel GetPpdbDaftar(int IdPpdbDaftar, string Pin, out string oMessage);
        PpdbDaftarModel GetPpdbDaftar(IDbConnection conn, int IdPpdbDaftar, out string oMessage);
        string InputFormulir(
            int IdUser,
            PpdbSiswaAddModel Siswa,
            List<PpdbSiswaOrtuAddModel> Ortus,
            List<PpdbSiswaRaporAddModel> Rapors,
            List<PpdbSiswaPrestasiAddModel> Prestasis,
            List<PpdbKuisionerJawabanAddModel> Kuisioners,
            List<PpdbBiayaSiswaDetilAddModel> Biayas,
            IFormFile FileFoto,
            IFormFile FileKtpIbu,
            IFormFile FileKtpAyah,
            IFormFile FileKk,
            IFormFile FileAkte,
            IFormFile FileSuratKesehatan,
            IFormFile FileBebasNarkoba,
            List<IFormFile> FileRapors,
            List<IFormFile> FilePrestasis,
            PpdbSiswaKkModel SiswaKk);
        PpdbSiswaModel GetPpdbSiswa(IDbConnection conn, int IdPpdbDaftar, out string oMessage);
        string SetLunasBiayaSiswa(IDbConnection conn, int IdUser, string RefNo, int IdPpdbDaftar, DateTime TanggalLunas, double Rp, string Catatan);
        string SetLunasBiayaSiswaPrt(IDbConnection conn, int IdUser, string RefNo, int IdPpdbDaftar, DateTime TanggalLunas, double Rp, string Catatan, int? CilKe);

        #endregion
        string DeleteTagihanSiswa(int IdSiswa, int IdJenisTagihanSiswa, string Periode);
        string UpdateTagihanSiswa(int IdSiswa, int IdJenisTagihanSiswa, string Periode, double Rp);

        List<BannerModel> GetBanners(string Tipe, out string oMessage);
        string AddBanner(int IdUser, BannerAddEditModel Data, out string oMessage);
        string UpdateBanner(int IdUser, BannerAddEditModel Data, out string oMessage);
        string DeleteBanner(int IdBanner, out string oMessage);

        List<TestimoniModel> GetTestimonis(out string oMessage);
        TestimoniModel GetTestimoni(int IdTestimoni, out string oMessage);
        string UpdateTestimoni(int IdUser, int IdTestimoni, string Nama, string Sebagai, string Pesan, string Url, out string oMessage);
        string DeleteTestimoni(int IdTestimoni, out string oMessage);
        string ToSlug(string phrase);
    }
}
