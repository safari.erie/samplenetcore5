﻿using System.Collections.Generic;
using Swg.Models;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Shared.Interfaces
{
    public interface IPegawaiService
    {
        string PegawaiAdd(int IdUser, PegawaiAddModel Data, out string oMessage);
        string PegawaiEdit(int IdUser, PegawaiEditModel Data, out string oMessage);
        string SetPegawaiActive(int IdUser, int IdPegawai, out string oMessage);
        string SetPegawaiInActive(int IdUser, int IdPegawai, out string oMessage);
        List<PegawaiModel> GetListPegawai(out string Message);
        List<ComboModel> GetListJenjangPendidikan(out string Message);
        List<ComboModel> GetListJabatan(out string Message);

        string UploadPegawai(int IdUser, List<SheetPegawaiModel> Data);
        string UploadGajiPrevious(int IdUser, List<SheetGajiPegawaiPreviousModel> Data);
        string UploadGaji(int IdUser, List<SheetGajiPegawaiModel> Data);
        List<PegawaiGajiListModel> GetPegawaiGajis(int IdPegawai, int Month, int Year, out string oMessage);
        SlipGajiModel GetSlipGaji(int IdPegawai, string Periode, out string oMessage);

        List<GajiPegawaiBulanModel> GetGajiBulans(int IdUser, int Month, int Year, out string oMessage);
        List<GajiPegawaiUnitModel> GetGajiBulanUnits(int IdUser, string Periode, out string oMessage);
        List<PegawaiGajiListModel> GetGajiBulanUnitPegawais(int IdUser, string Periode, int IdUnit, out string oMessage);
        SlipGajiModel GetSlipGaji(int IdUser, int IdPegawai, string Periode, out string oMessage);

        List<ComboModel> GetKelasParalelPegawai(out string oMessage);
        List<ComboModel> GetKompetensiPegawai();
        List<ComboModel> GetJenisStatusNikah();

        string UpdateTokenFcmPegawai(int IdUser, string Token, out string oMessage);
        List<PegawaiModel> GetListPegawaiBy(int IdRole, out string Message);
        string KirimNotifikasi(string to, string title, string body, string click_action, out string oMessage);
    }
}
