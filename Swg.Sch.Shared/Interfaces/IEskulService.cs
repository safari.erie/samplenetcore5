﻿using System;
using System.Collections.Generic;
using Swg.Models;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Shared.Interfaces
{
    public interface IEskulService
    {
        List<ComboModel> GetJilidQuran();
        KelasQuranSiswaModel GetKelasQuranSiswa(int IdSiswa, string Tanggal, out string oMessage);
        List<KelasQuranListModel> GetKelasQurans(int IdPegawai, string Tanggal, out string oMessage);
        List<KelasQuranSiswaListModel> GetKelasQuranSiswas(int IdKelasQuran, string Tanggal, out string oMessage);
        KelasQuranSiswaProgresModel GetKelasQuranSiswaProgres(int IdSiswa, int IdKelasQuran, string Tanggal, out string oMessage);
        KelasQuranSiswaUjianModel GetKelasQuranSiswaUjian(int IdSiswa, int IdKelasQuran, string Tanggal, out string oMessage);
        string KelasQuranSiswaProgresAdd(int IdPegawai, int IdSiswa, int IdKelasQuran, string Tanggal, int IdJenisJilidQuran, int Halaman, string Catatan);
        string KelasQuranSiswaUjianAdd(int IdPegawai, int IdSiswa, int IdKelasQuran, string Tanggal, int IdJenisJilidQuran, string Hasil, string Nilai, string Catatan);
    }
}
