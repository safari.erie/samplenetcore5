﻿using System;
using System.Collections.Generic;
using Swg.Models;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Shared.Interfaces
{
    public interface IKeuPengajuanService
    {
        List<ComboModel> GetCoas(out string oMessage);
        List<KeuPengajuanListModel> GetPengajuans(int IdUser, int IdTahunAjaran, out string oMessage);
        List<ComboModel> GetPrograms(int IdUser, out string oMessage);
        List<ComboModel> GetKegiatanWajibs(int IdUser, int IdTahunAjaran, int IdProgram, out string oMessage);
        List<ComboModel> GetKegiatanTambahans(int IdUser, int IdTahunAjaran, int IdProgram, out string oMessage);
        string SetCoaPengajuanWajib(int IdUser, int IdUnitProkegWajib, string Nomor, string Tanggal);
        string SetCoaPengajuanTambahan(int IdUser, int IdUnitProkegTambahan, string Nomor, string Tanggal);
        List<KeuPengajuanCoaListModel> GetPengajuanCoas(int IdUser, int IdPengajuan, out string oMessage);

        string PengajuanDelete(int IdUser, int IdPengajuan);
        string PengajuanEdit(int IdUser, int IdPengajuan, string Nomor, string Tanggal);

        string PengajuanCoaAdd(int IdUser, KeuPengajuanCoaAddModel Data);
        string PengajuanCoaEdit(int IdUser, KeuPengajuanCoaEditModel Data);
        string PengajuanCoaDelete(int IdUser, int IdPengajuanCoa);

        string ApproveByKaUnit(int IdUser, int IdPengajuan, int StatusProses, double RpRealisasi, string Catatan);
        string ApproveByManUnit(int IdUser, int IdPengajuan, int StatusProses, double RpRealisasi, string Catatan);
        string ApproveByManKeu(int IdUser, int IdPengajuan, int StatusProses, double RpRealisasi, string Catatan);
        string ApproveByDirektur(int IdUser, int IdPengajuan, int StatusProses, double RpRealisasi, string Catatan);
        string SetRealisasi(int IdUser, int IdPengajuan);
        KeuPengajuanDownloadModel GetPengajuanDownload(int IdUser, int IdPengajuan, out string oMessage);
    }
}
