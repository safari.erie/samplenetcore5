﻿using System.Collections.Generic;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Shared.Interfaces
{
    public interface IPdfService
    {
        string GenerateKartuPeserta(PpdbDaftarKartuModel Data, string PathFile);
        string GenerateKartuPesertaP(PpdbDaftarKartuModel Data, string PathFile);
        string GenerateSuratLolosSeleksi(string No, string Nama, int IdPendaftaran, int IdUnit, string TglMasehi, string TglHijriyah, string PathFile);
        string GenerateKartuPesertaTk(PpdbDaftarKartuModel Data, string PathFile);
        string GenerateKartuPesertaSd(PpdbDaftarKartuModel Data, string PathFile);
        string GenerateKartuPesertaSmp(PpdbDaftarKartuModel Data, string PathFile);
        string GenerateKartuPesertaSma(PpdbDaftarKartuModel Data, string PathFile);
        string GenerateKartuPesertaA4(List<string> FileNames);
        string GenerateNameTagSma(PpdbDaftarKartuModel Data, string PathFile);
    }
}
