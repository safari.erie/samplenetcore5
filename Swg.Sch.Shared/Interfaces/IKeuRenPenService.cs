﻿using Swg.Models;
using Swg.Sch.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Sch.Shared.Interfaces
{
    public interface IKeuRenPenService
    {
        string InitRenPen(int IdUser, int IdTahunAjaran);
        List<ComboModel> GetCoas(out string oMessage);
        List<KeuUnitRenPenListModel> GetRenPens(int IdUser, int IdTahunAjaran, out string oMessage);
        List<KeuUnitRenPenCoaListModel> GetRenPenCoas(int IdUser, int IdUnitRenPen, out string oMessage);
        KeuUnitRenPenCoaModel GetRenPenCoa(int IdUnitRenPen, int IdCoa, out string oMessage);
        string AddData(int IdUser, int IdUnitRenPen, int IdCoa, double Rp);
        string EditData(int IdUser, int IdUnitRenPen, int IdCoa, double Rp);
        string DeleteData(int IdUser, int IdUnitRenPen, int IdCoa);
        string ApproveDataByManKeu(int IdUser, int IdUnitRenPen, int StatusProses, string Catatan);
        string ApproveDataByDirektur(int IdUser, int IdUnitRenPen, int StatusProses, string Catatan);
        string SetSah(int IdUser, int IdUnitRenPen);

        KeuUnitRenPenDownloadModel GetRenPenDownload(int IdUser, int IdUnitRenPen, out string oMessage);

        List<KeuUnitCoaPaguModel> GetUnitCoaPagus(int IdUser, int IdUnit, int IdTahunAjaran, out string oMessage);
        List<string> GetUnitCoaPaguProKegs(int IdUser, int IdUnitCoaPagu, out string oMessage);
        string EditCoaPagu(int IdUser, int IdUnitCoaPagu, double RpPagu);
    }
}
