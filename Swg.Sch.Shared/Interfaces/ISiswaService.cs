using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Swg.Models;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Shared.Interfaces
{
    public interface ISiswaService
    {
        List<ComboModel> GetJenisPrestasiSiswa();
        List<ComboModel> GetJenisKelamin();

        List<SiswaListModel> GetSiswasByUnit(int IdUnit, out string oMessage);
        List<SiswaListModel> GetSiswasByKelas(int IdKelas, out string oMessage);
        List<SiswaListModel> GetSiswasByKelasParalel(int IdKelasParalel, out string oMessage);

        List<SiswaPrestasiModel> GetSiswaPrestasis(int IdSiswa, out string oMessage);
        SiswaPrestasiModel GetSiswaPrestasi(int IdSiswaPrestasi, out string oMessage);

        List<SiswaRaporModel> GetSiswaRapors(int IdSiswa, out string oMessage);
        SiswaRaporModel GetSiswaRapor(int IdSiswa, int IdJenisRaport, out string oMessage);
        List<SiswaRaporModel> GetSiswaRapors(int IdSiswa, int IdTahunAjaran, out string oMessage);


        string SiswaAdd(int IdUser, SiswaAddModel Data);
        string SiswaUpload(int IdUser, List<SheetSiswaModel> Data);
        string SiswaListAdd(int IdUser, List<SiswaAddModel> Data);
        string SiswaEdit(int IdUser, SiswaEditModel Data);
        string SetSiswaInActive(int IdUser, int IdSiswa);
        string SiswaEditUpload(int IdUser, List<SheetSiswaModel> Data);


        string SetSiswaArchive(int IdUser, int IdTahunAjaran);
        string SetSiswaReArchive(int IdUser, int IdTahunAjaran);

        string SiswaTagihanUpload(int IdUser, List<SiswaTagihanAddModel> Data);
        string SiswaTagihanUploadSusulan(int IdUser, List<SiswaTagihanAddModel> Data);
        SiswaTagihanModel GetSiswaTagihan(string Nis, out string oMessage);
        ResumeListSiswaTagihanModel GetResumeSiswaTagihan(string TanggalAwal, string TanggalAkhir, out string oMessage);

        SiswaModel SiswaLogin(string Nis, string Password, out string oMessage);
        string SiswaChangePassword(string Nis, string OldPassword, string NewPassword1, string NewPassword2);
        string SiswaResetPassword(string Nis, out string oMessage);

        string UploadRaporSiswa(int IdUser, int IdSiswa, int IdJenisRapor, IFormFile FileRapor);
        string ReUploadRaporSiswa(int IdUser, int IdSiswa, int IdJenisRapor, IFormFile FileRapor);

        string ValidasiPin(int IdSiswa, string Pin);
        string GantiPin(int IdSiswa, string PinLama, string PinBaru);
        string ResetPin(int IdSiswa);
        string EditProfilSiswa(int IdSiswa, string Email, string EmailOrtu, string NoHandphone, IFormFile FotoProfile);
        string EditSiswaProfil(int IdSiswa, SiswaEditProfileModel Data);
        SiswaViewProfileModel GetSiswaProfile(int IdSiswa, out string oMessage);
        SheetSiswaViewProfileModel GetSheetSiswaViewProfile(int IdUser, int IdUnit, out string oMessage);
        SiswaModel GetSiswa(int IdSiswa, out string oMessage);
        SiswaModel GetSiswaByNis(string Nis, out string oMessage);
        string UploadRapor(int IdUser, IFormFile FileRapor, string PathUpload, int TahunAjaran, out string LogMessage);
        string PublishRapors(int IdUser, int IdJenisRapor, List<int> IdSiswa, out string oMessage);
        List<SiswaRaporModel> GetApprovSiswaRapors(int IdUser, int IdKelas, int IdJenisRapor, out string oMessage);
        List<SiswaRaporModel> GetApprovSiswaRapors(int IdUser, out string oMessage);
        List<JenisRaporModel> GetJenisRapors(out string oMessage);

        string UpdateTokenFcmSiswa(int IdUser, int TipePlatform, string Token, string UserAgent, out string oMessage);

        List<SiswaInboxModel> GetInboxSiswas(int IdUser, out string oMessage);
        SiswaInboxModel GetInboxSiswa(int IdSiswaInbox, out string oMessage);
        string ReadInboxSiswa(int IdSiswaInbox);
        string PushInboxNotif(int IdSiswa, string Judul, string Deskripsi, int TipeInbox, int? CreatedBy);
        List<LaporanRiwayatTransaksiBankModel> GetLaporanRiwayatTransaksiBank(string TanggalAwal, string TanggalAkhir, out string oMessage);
        bool IsEditSiswaProfil(int IdSiswa, out string oMessage);
    }
}
