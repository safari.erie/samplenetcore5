using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Dapper;
using Ionic.Zip;
using Microsoft.AspNetCore.Http;
using Swg.Entities.Etc;
using Swg.Entities.His;
using Swg.Entities.kbm;
using Swg.Entities.Kbm;
using Swg.Entities.Lmg;
using Swg.Entities.Ppdb;
using Swg.Entities.Public;
using Swg.Entities.Ref;
using Swg.Models;
using Swg.Models.Constants;
using Swg.Sch.Shared.Constants;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;

namespace Swg.Sch.Services
{
    public class SiswaService : ISiswaService
    {
        private string _languageCode = "id";
        private readonly string ServiceName = "Swg.Sch.Services.SiswaService.";
        private readonly ICommonService commonService;
        private readonly IUserAppService userAppService;
        private readonly IDateTimeService dateTimeService;
        private readonly ISchService schService;
        private readonly IEmailService emailService;
        private TahunAjaranModel tahunAjaran = new TahunAjaranModel();

        public SiswaService(
            ICommonService CommonService,
            IDateTimeService DateTimeService,
            IUserAppService UserAppService,
            ISchService SchService,
            IEmailService EmailService)
        {
            commonService = CommonService;
            userAppService = UserAppService;
            dateTimeService = DateTimeService;
            schService = SchService;
            emailService = EmailService;
            tahunAjaran = schService.GetTahunAjaran(out string oMessage);
            if (!string.IsNullOrEmpty(oMessage)) tahunAjaran = null;
        }


        public List<ComboModel> GetJenisPrestasiSiswa()
        {
            return (from a in JenisPrestasiConstant.DictJenisPrestasi
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetJenisKelamin()
        {
            return (from a in JenisKelaminConstant.Dict
                    select new ComboModel { Kode = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        private string SiswaAdd(IDbConnection conn, int IdUser, SiswaAddModel Data)
        {
            try
            {
                var data = (from a in conn.GetList<TbKelasRombel>()
                            join b in conn.GetList<TbKelasParalel>() on a.IdKelasParalel equals b.IdKelasParalel
                            join c in conn.GetList<TbSiswa>() on a.IdSiswa equals c.IdSiswa
                            where c.Nis == Data.Nis
                            select new { c.IdSiswa, KelasParalel = b.NamaSingkat, c.Nama }).FirstOrDefault();
                if (data != null)
                {
                    return string.Format("nis {0} sudah terdaftar atas nama {1} di kelas {1}", Data.Nis, data.Nama, data.KelasParalel);
                }
                TbSiswa tbSiswa = new TbSiswa();


                Random generator = new Random();
                string newPin = generator.Next(0, 999999).ToString("D6");
                string _newPassword = commonService.CreateRandomPassword(8);
                string _newMessage = string.Empty;

                var maxIdSiswa = (from a in conn.GetList<TbSiswa>()
                                  select new { a.IdSiswa }).OrderByDescending(g => g.IdSiswa).FirstOrDefault();
                var tbSiswas = conn.GetList<TbSiswa>();
                if (tbSiswas == null || tbSiswas.Count() == 0)
                {
                    tbSiswa.IdSiswa = 1; // ini jika IdSiswa nya tidak ada (kalau tida ada ini bakal error)
                }
                else
                {
                    tbSiswa.IdSiswa = maxIdSiswa.IdSiswa + 1; // ini jika IdSiswa nya sudah ada + 1;
                }
                _newMessage = tbSiswa.EmailOrtu;
                tbSiswa.Pin = newPin;
                tbSiswa.Password = commonService.EncryptString(_newPassword);
                tbSiswa.Nis = Data.Nis;
                tbSiswa.NisLama = Data.Nis;
                tbSiswa.Nisn = Data.Nisn;
                tbSiswa.Nik = Data.Nik;
                tbSiswa.Nama = Data.Nama;
                tbSiswa.NamaPanggilan = Data.NamaPanggilan;
                tbSiswa.TempatLahir = Data.TempatLahir;
                tbSiswa.TanggalLahir = dateTimeService.StrToDateTime(Data.TanggalLahir);
                tbSiswa.IdAgama = Data.IdAgama;
                tbSiswa.KdJenisKelamin = Data.KdJenisKelamin;
                tbSiswa.NoHandphone = Data.NoHandphone;
                tbSiswa.NoDarurat = Data.NoDarurat;
                tbSiswa.Email = Data.Email;
                tbSiswa.AlamatTinggal = Data.AlamatTinggal;
                tbSiswa.AlamatOrtu = Data.AlamatOrtu;
                tbSiswa.NikIbu = Data.NikIbu;
                tbSiswa.NamaIbu = Data.NamaIbu;
                tbSiswa.IdJenisPekerjaanIbu = Data.IdJenisPekerjaanIbu;
                tbSiswa.NamaInstansiIbu = Data.NamaInstansiIbu;
                tbSiswa.NikAyah = Data.NikAyah;
                tbSiswa.NamaAyah = Data.NamaAyah;
                tbSiswa.IdJenisPekerjaanAyah = Data.IdJenisPekerjaanAyah;
                tbSiswa.NamaInstansiAyah = Data.NamaInstansiAyah;
                tbSiswa.EmailOrtu = Data.EmailOrtu;
                tbSiswa.Status = StatusDataConstant.Aktif;
                tbSiswa.CreatedBy = IdUser;
                tbSiswa.CreatedDate = dateTimeService.GetCurrdate();
                conn.Insert(tbSiswa);

                TbKelasRombel tbKelasRombel = new TbKelasRombel
                {
                    IdSiswa = tbSiswa.IdSiswa,
                    IdKelasParalel = Data.IdKelasParalel
                };
                conn.Insert(tbKelasRombel);

                StringBuilder stb = new StringBuilder();
                stb.Append("<html>");
                stb.Append("<body>");
                stb.Append("Berikut kami informasikan password anda adalah <strong>" + _newPassword + "</strong><br>");
                stb.Append("dan kami informasikan juga PIN anda adalah <strong>" + newPin + "</strong><br><br>");
                stb.Append("</body>");
                stb.Append("</html>");
                List<string> emailTos = new List<string>();
                List<string> emailCcs = new List<string>();
                if (string.IsNullOrEmpty(tbSiswa.EmailOrtu))
                {
                    return "Email Orang Tua tidak boleh kosong";
                }
                else
                {
                    emailTos.Add(tbSiswa.EmailOrtu);
                    emailCcs.Add(AppSetting.OrgEmail);
                }

                SendEmailModel email = new SendEmailModel
                {
                    To = emailTos,
                    Title = tbSiswa.Nama,
                    Cc = emailCcs,
                    Subject = "Informasi Akun Password & Pin Orang Tua Siswa " + tbSiswa.Nama,
                    Message = stb.ToString(),
                    Attachments = null
                };
                _ = emailService.SendHtmlEmailAsync(email);
                return _newMessage;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SiswaAdd", ex);
            }
        }
        public List<SiswaListModel> GetSiswasByUnit(int IdUnit, out string oMessage)
        {
            var ret = schService.GetSiswasByUnit(IdUnit, out oMessage);
            if (!string.IsNullOrEmpty(oMessage)) return null;
            return ret;
        }
        public List<SiswaListModel> GetSiswasByKelas(int IdKelas, out string oMessage)
        {
            var ret = schService.GetSiswasByKelas(IdKelas, out oMessage);
            if (!string.IsNullOrEmpty(oMessage)) return null;
            return ret;
        }
        public List<SiswaListModel> GetSiswasByKelasParalel(int IdKelasParalel, out string oMessage)
        {
            var ret = schService.GetSiswasByKelasParalel(IdKelasParalel, out oMessage);
            if (!string.IsNullOrEmpty(oMessage)) return null;
            return ret;
        }
        private SiswaPrestasiModel SetSiswaPrestasiModel(TbSiswaPrestasi tbSiswaPrestasi, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                SiswaPrestasiModel ret = new SiswaPrestasiModel();
                ret.FileSertifikat = tbSiswaPrestasi.FileSertifikat;
                ret.JenisPrestasi = JenisPrestasiConstant.DictJenisPrestasi[tbSiswaPrestasi.IdJenisPrestasi];
                ret.JenisTingkatPrestasi = TingkatPrestasiSiswaConstant.DictTingkatPrestasiSiswa[tbSiswaPrestasi.IdJenisTingkatPrestasi];
                ret.IdSiswa = tbSiswaPrestasi.IdSiswa;
                ret.IdJenisPrestasi = tbSiswaPrestasi.IdJenisPrestasi;
                ret.Tahun = tbSiswaPrestasi.Tahun;
                ret.IdJenisTingkatPrestasi = tbSiswaPrestasi.IdJenisTingkatPrestasi;
                ret.NamaPenyelenggara = tbSiswaPrestasi.NamaPenyelenggara;
                ret.Keterangan = tbSiswaPrestasi.Keterangan;
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "SetSiswaPrestasiModel", ex);
                return null;
            }
        }
        public List<SiswaPrestasiModel> GetSiswaPrestasis(int IdSiswa, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbSiswaPrestasi>()
                               where a.IdSiswa == IdSiswa
                               select a;
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    List<SiswaPrestasiModel> ret = new List<SiswaPrestasiModel>();
                    foreach (var item in data)
                    {
                        SiswaPrestasiModel m = SetSiswaPrestasiModel(item, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswaPrestasis", ex);
                return null;

            }
        }
        public SiswaPrestasiModel GetSiswaPrestasi(int IdSiswaPrestasi, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.Get<TbSiswaPrestasi>(IdSiswaPrestasi);
                    if (data == null) { oMessage = "data tidak ada"; return null; }

                    SiswaPrestasiModel ret = SetSiswaPrestasiModel(data, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswaPrestasi", ex);
                return null;

            }
        }
        public List<SiswaRaporModel> GetSiswaRapors(int IdSiswa, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbSiswaRapor>()
                               join b in conn.GetList<TbJenisRapor>() on a.IdJenisRapor equals b.IdJenisRapor
                               where a.IdSiswa == IdSiswa && a.Status == StatusRapor.Aktif
                               select new { a, b };
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    List<SiswaRaporModel> ret = new List<SiswaRaporModel>();
                    foreach (var item in data)
                    {
                        SiswaRaporModel m = new SiswaRaporModel
                        {
                            IdSiswa = item.a.IdSiswa,
                            IdJenisRapor = item.a.IdJenisRapor,
                            FileRapor = item.a.FileRapor,
                            JenisRapor = item.b.Nama
                        };
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswaRapors", ex);
                return null;

            }
        }
        public SiswaRaporModel GetSiswaRapor(int IdSiswa, int IdJenisRapor, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = (from a in conn.GetList<TbSiswaRapor>()
                                join b in conn.GetList<TbJenisRapor>() on a.IdJenisRapor equals b.IdJenisRapor
                                where a.IdSiswa == IdSiswa & a.IdJenisRapor == IdJenisRapor
                                select new { a, b }).FirstOrDefault();
                    if (data == null) { oMessage = "data tidak ada"; return null; }
                    SiswaRaporModel ret = new SiswaRaporModel
                    {
                        IdSiswa = data.a.IdSiswa,
                        IdJenisRapor = data.a.IdJenisRapor,
                        FileRapor = data.a.FileRapor,
                        JenisRapor = data.b.Nama
                    };
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswaRaports", ex);
                return null;

            }
        }
        public List<SiswaRaporModel> GetSiswaRapors(int IdSiswa, int IdTahunAjaran, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<SiswaRaporModel> ret = new List<SiswaRaporModel>();
                using (var conn = commonService.DbConnection())
                {

                    var siswa = (from a in conn.GetList<TbSiswa>()
                                 where a.IdSiswa == IdSiswa
                                 select a
                                 ).FirstOrDefault();

                    if (siswa == null) { oMessage = "data siswa tidak ada"; return null; }
                    if (tahunAjaran.IdTahunAjaran == IdTahunAjaran)
                    {
                        ret = GetSiswaRapors(siswa.IdSiswa, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                    }
                    else
                    {
                        var tbTahunAjaran = conn.Get<TbTahunAjaran>(IdTahunAjaran);
                        if (tbTahunAjaran == null) { oMessage = "tahun ajaran tidak terdaftar"; return null; }
                        var data = from a in conn.GetList<TaSiswaRapor>()
                                   where a.Nis == siswa.Nis
                                   & a.IdTahunAjaran == IdTahunAjaran
                                   select a;
                        if (data == null || data.Count() == 0)
                        {
                            oMessage = string.Format("rapor ananda {0} tahun ajaran {1} belum diupload", siswa.Nama, tbTahunAjaran.Nama);
                            return null;
                        }
                        foreach (var item in data)
                        {
                            SiswaRaporModel m = new SiswaRaporModel
                            {
                                IdSiswa = siswa.IdSiswa,
                                IdJenisRapor = 0,
                                FileRapor = item.FileRapor,
                                JenisRapor = item.JenisRapor
                            };
                            ret.Add(m);
                        }
                    }

                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswaRapors", ex);
                return null;

            }
        }
        public string SiswaAdd(int IdUser, SiswaAddModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string err = SiswaAdd(conn, IdUser, Data);
                        if (!string.IsNullOrEmpty(err)) return err;
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SiswaAdd", ex);
            }
        }
        public string SiswaUpload(int IdUser, List<SheetSiswaModel> Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    string err = "";
                    using (var tx = conn.BeginTransaction())
                    {

                        foreach (var item in Data)
                        {
                            var data = (from a in conn.GetList<TbKelasRombel>()
                                        join b in conn.GetList<TbKelasParalel>() on a.IdKelasParalel equals b.IdKelasParalel
                                        join c in conn.GetList<TbSiswa>() on a.IdSiswa equals c.IdSiswa
                                        where c.Nis == item.Nis
                                        select new { c.IdSiswa, KelasParalel = b.NamaSingkat, c.Nama }).FirstOrDefault();
                            if (data != null)
                            {
                                return string.Format("nis {0} sudah terdaftar atas nama {1} di kelas {1}", item.Nis, data.Nama, data.KelasParalel);
                            }
                            TbSiswa tbSiswa = new TbSiswa();
                            var tbSiswas = conn.GetList<TbSiswa>();
                            if (tbSiswas == null || tbSiswas.Count() == 0)
                            {
                                tbSiswa.IdSiswa = 1;
                            }
                            else
                            {
                                var maxIdSiswa = (from a in conn.GetList<TbSiswa>()
                                                  select new { a.IdSiswa }).OrderByDescending(g => g.IdSiswa).FirstOrDefault();
                                tbSiswa.IdSiswa = maxIdSiswa.IdSiswa + 1;
                            }

                            Random generator = new Random();
                            string newPin = generator.Next(0, 999999).ToString("D6");
                            string _newPassword = commonService.CreateRandomPassword(8);

                            tbSiswa.Pin = newPin;
                            tbSiswa.Password = commonService.EncryptString(_newPassword);
                            tbSiswa.Nis = item.Nis;
                            tbSiswa.NisLama = item.Nis;
                            tbSiswa.Nisn = item.Nisn;
                            tbSiswa.Nik = item.Nik;
                            tbSiswa.Nama = item.Nama;
                            tbSiswa.NamaPanggilan = item.NamaPanggilan;
                            tbSiswa.TempatLahir = item.TempatLahir;
                            tbSiswa.TanggalLahir = dateTimeService.StrToDateTime(item.TanggalLahir);

                            var idAgama = (from a in AgamaConstant.Dict
                                           where a.Value == item.Agama
                                           select a.Key).FirstOrDefault();
                            if (idAgama != 0)
                            {
                                tbSiswa.IdAgama = idAgama;
                            }
                            else
                            {
                                err += "pilihan agama tidak sesuai";
                            }

                            var kdJenisKelamin = (from a in JenisKelaminConstant.Dict
                                                  where a.Key == item.JenisKelamin
                                                  select a.Key).FirstOrDefault();
                            tbSiswa.KdJenisKelamin = kdJenisKelamin;

                            tbSiswa.NoHandphone = item.NoHandphone;
                            tbSiswa.NoDarurat = item.NoDarurat;
                            tbSiswa.Email = item.Email;
                            tbSiswa.AlamatTinggal = item.AlamatTinggal;
                            tbSiswa.AlamatOrtu = item.AlamatOrtu;
                            tbSiswa.NikIbu = item.NikIbu;
                            tbSiswa.NamaIbu = item.NamaIbu;

                            var jenisPekerjaanIbu = (from a in conn.GetList<TbJenisPekerjaan>()
                                                     where a.Nama == item.JenisPekerjaanIbu || a.NamaSingkat == item.JenisPekerjaanIbu
                                                     select a).FirstOrDefault();
                            if (jenisPekerjaanIbu != null)
                            {
                                tbSiswa.IdJenisPekerjaanIbu = jenisPekerjaanIbu.IdJenisPekerjaan;
                            }
                            else
                            {
                                err += "jenis pekerjaan ibu tidak sesuai!";
                            }
                            tbSiswa.NamaInstansiIbu = item.NamaInstansiIbu;
                            tbSiswa.NikAyah = item.NikAyah;
                            tbSiswa.NamaAyah = item.NamaAyah;
                            var jenisPekerjaanAyah = (from a in conn.GetList<TbJenisPekerjaan>()
                                                      where a.Nama == item.JenisPekerjaanAyah || a.NamaSingkat == item.JenisPekerjaanAyah
                                                      select a).FirstOrDefault();
                            if (jenisPekerjaanAyah != null)
                            {
                                tbSiswa.IdJenisPekerjaanAyah = jenisPekerjaanAyah.IdJenisPekerjaan;
                            }
                            else
                            {
                                err += "jenis pekerjaan ayah tidak sesuai!";
                            }
                            tbSiswa.NamaInstansiAyah = item.NamaInstansiAyah;
                            tbSiswa.EmailOrtu = item.EmailOrtu;
                            tbSiswa.Status = StatusDataConstant.Aktif;
                            tbSiswa.CreatedBy = IdUser;
                            tbSiswa.CreatedDate = dateTimeService.GetCurrdate();
                            conn.Insert(tbSiswa);
                            var kelasParalel = (from a in conn.GetList<TbKelasParalel>()
                                                where a.Nama == item.KelasParalel || a.NamaSingkat == item.KelasParalel
                                                select a).FirstOrDefault();
                            if (kelasParalel != null)
                            {
                                TbKelasRombel tbKelasRombel = new TbKelasRombel
                                {
                                    IdSiswa = tbSiswa.IdSiswa,
                                    IdKelasParalel = kelasParalel.IdKelasParalel
                                };

                                conn.Insert(tbKelasRombel);

                            }
                            else
                            {
                                err += "Kelas paralel tidak ditemukan!";
                            }

                            StringBuilder stb = new StringBuilder();
                            stb.Append("<html>");
                            stb.Append("<body>");
                            stb.Append("Berikut kami informasikan password anda adalah <strong>" + _newPassword + "</strong><br>");
                            stb.Append("dan kami informasikan juga PIN anda adalah <strong>" + newPin + "</strong><br><br>");
                            stb.Append("</body>");
                            stb.Append("</html>");
                            List<string> emailTos = new List<string>();
                            List<string> emailCcs = new List<string>();
                            if (string.IsNullOrEmpty(tbSiswa.EmailOrtu))
                            {
                                return "Email Orang Tua tidak boleh kosong";
                            }
                            else
                            {
                                emailTos.Add(tbSiswa.EmailOrtu);
                                emailCcs.Add(AppSetting.OrgEmail);
                            }

                            SendEmailModel email = new SendEmailModel
                            {
                                To = emailTos,
                                Title = tbSiswa.Nama,
                                Cc = emailCcs,
                                Subject = "Informasi Akun Password & Pin Orang Tua Siswa " + tbSiswa.Nama,
                                Message = stb.ToString(),
                                Attachments = null
                            };
                            _ = emailService.SendHtmlEmailAsync(email);
                        }
                        tx.Commit();


                        return err;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SiswaUpload", ex);
            }
        }
        public string SiswaListAdd(int IdUser, List<SiswaAddModel> Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string err = "";
                        foreach (var item in Data)
                        {
                            string tempErr = SiswaAdd(conn, IdUser, item);
                            if (!string.IsNullOrEmpty(tempErr))
                                err += tempErr + "; " + Environment.NewLine;
                        }
                        if (!string.IsNullOrEmpty(err)) return err;
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SiswaAdd", ex);
            }
        }
        public string SiswaEdit(int IdUser, SiswaEditModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var data = (from a in conn.GetList<TbKelasRombel>()
                                    join b in conn.GetList<TbKelasParalel>() on a.IdKelasParalel equals b.IdKelasParalel
                                    join c in conn.GetList<TbSiswa>() on a.IdSiswa equals c.IdSiswa
                                    where c.Nis == Data.Nis
                                    select new { c.IdSiswa, KelasParalel = b.NamaSingkat, c.Nama }).FirstOrDefault();
                        if (data != null)
                        {
                            if (data.IdSiswa != data.IdSiswa)
                                return string.Format("nis {0} sudah terdaftar atas nama {1} di kelas {1}", Data.Nis, data.Nama, data.KelasParalel);
                        }
                        TbSiswa tbSiswa = conn.Get<TbSiswa>(Data.IdSiswa);
                        if (conn == null) return "data tidak ada";
                        //                        tbSiswa.Nisn = Data.Nisn;
                        tbSiswa.Nis = Data.Nis;
                        tbSiswa.Nik = Data.Nik;
                        tbSiswa.Nama = Data.Nama;
                        tbSiswa.NamaPanggilan = Data.NamaPanggilan;
                        tbSiswa.TempatLahir = Data.TempatLahir;
                        tbSiswa.TanggalLahir = dateTimeService.StrToDateTime(Data.TanggalLahir);
                        tbSiswa.IdAgama = Data.IdAgama;
                        tbSiswa.KdJenisKelamin = Data.KdJenisKelamin;
                        tbSiswa.NoHandphone = Data.NoHandphone;
                        tbSiswa.NoDarurat = Data.NoDarurat;
                        tbSiswa.Email = Data.Email;
                        tbSiswa.AlamatTinggal = Data.AlamatTinggal;
                        tbSiswa.AlamatOrtu = Data.AlamatOrtu;
                        tbSiswa.NikIbu = Data.NikIbu;
                        tbSiswa.NamaIbu = Data.NamaIbu;
                        tbSiswa.IdJenisPekerjaanIbu = Data.IdJenisPekerjaanIbu;
                        tbSiswa.NamaInstansiIbu = Data.NamaInstansiIbu;
                        tbSiswa.NikAyah = Data.NikAyah;
                        tbSiswa.NamaAyah = Data.NamaAyah;
                        tbSiswa.IdJenisPekerjaanAyah = Data.IdJenisPekerjaanAyah;
                        tbSiswa.NamaInstansiAyah = Data.NamaInstansiAyah;
                        tbSiswa.EmailOrtu = Data.EmailOrtu;
                        tbSiswa.UpdatedBy = IdUser;
                        tbSiswa.UpdatedDate = dateTimeService.GetCurrdate();
                        conn.Update(tbSiswa);

                        TbKelasRombel tbKelasRombel = (from a in conn.GetList<TbKelasRombel>()
                                                       where a.IdSiswa == Data.IdSiswa
                                                       select a).FirstOrDefault();
                        if (tbKelasRombel != null)
                            conn.Delete(tbKelasRombel);
                        tbKelasRombel = new TbKelasRombel
                        {
                            IdSiswa = tbSiswa.IdSiswa,
                            IdKelasParalel = Data.IdKelasParalel
                        };
                        conn.Insert(tbKelasRombel);

                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SiswaEdit", ex);
            }
        }
        public string SetSiswaInActive(int IdUser, int IdSiswa)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbSiswa tbSiswa = conn.Get<TbSiswa>(IdSiswa);
                        if (conn == null) return "data tidak ada";
                        if (tbSiswa.Status == StatusDataConstant.TidakAktif) return "sudah tidak aktif";
                        tbSiswa.Status = StatusDataConstant.TidakAktif;
                        tbSiswa.UpdatedBy = IdUser;
                        tbSiswa.UpdatedDate = dateTimeService.GetCurrdate();
                        conn.Update(tbSiswa);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetSiswaInActive", ex);
            }
        }
        private string DeleteSiswa(IDbConnection conn, int IdUser, List<SiswaModel> siswas)
        {
            try
            {
                var taSiswa = (from a in conn.GetList<TaSiswa>()
                               where a.IdTahunAjaran == tahunAjaran.IdTahunAjaran
                               select a).FirstOrDefault();
                if (taSiswa == null) return string.Format("data siswa tahun ajaran {0} belum diarsipkan", tahunAjaran.Nama);

                var data = from a in conn.GetList<TbSiswa>()
                           join d in conn.GetList<TbKelasRombel>() on a.IdSiswa equals d.IdSiswa
                           join e in conn.GetList<TbKelasParalel>() on d.IdKelasParalel equals e.IdKelasParalel
                           join f in conn.GetList<TbKelas>() on e.IdKelas equals f.IdKelas
                           select new { tbsiswas = a, f.IdUnit, f.IdKelas, e.IdKelasParalel };

                var tbSiswaRapors = (from a in siswas
                                     join b in conn.GetList<TbSiswaRapor>() on a.IdSiswa equals b.IdSiswa
                                     select b).ToList();
                conn.DeleteList<TbSiswaRapor>(tbSiswaRapors);

                var tbSiswaPrestasis = (from a in siswas
                                        join b in conn.GetList<TbSiswaPrestasi>() on a.IdSiswa equals b.IdSiswa
                                        select b).ToList();
                conn.DeleteList<TbSiswaPrestasi>(tbSiswaPrestasis);

                var tbSiswaTagihans = (from a in siswas
                                       join b in conn.GetList<TbSiswaTagihan>() on a.IdSiswa equals b.IdSiswa
                                       select b).ToList();
                conn.DeleteList<TbSiswaTagihan>(tbSiswaTagihans);

                var tbPpdbDaftars = (from a in siswas
                                     join b in conn.GetList<TbPpdbSiswa>() on a.IdSiswa equals b.IdSiswa
                                     select b).ToList();
                conn.DeleteList<TbPpdbSiswa>(tbPpdbDaftars);

                var tbSiswas = (from a in siswas
                                join b in conn.GetList<TbSiswa>() on a.IdSiswa equals b.IdSiswa
                                select b).ToList();
                conn.DeleteList<TbSiswa>(tbSiswas);
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteSiswa", ex);
            }
        }
        private string SetSiswaArchive(IDbConnection conn, int IdUser, int IdTahunAjaran)
        {
            try
            {
                var tbTahunAjaran = (from a in conn.GetList<TaSiswa>()
                                     join b in conn.GetList<TbTahunAjaran>() on a.IdTahunAjaran equals b.IdTahunAjaran
                                     where a.IdTahunAjaran == IdTahunAjaran
                                     select b).FirstOrDefault();
                if (tbTahunAjaran != null) return string.Format("data siswa tahun {0} sudah diarsipkan", tbTahunAjaran.NamaSingkat);

                var dataSiswa = from a in conn.GetList<TbSiswa>()
                                join b in conn.GetList<TbKelasRombel>() on a.IdSiswa equals b.IdSiswa
                                join c in conn.GetList<TbKelasParalel>() on b.IdKelasParalel equals c.IdKelasParalel
                                join d in conn.GetList<TbKelas>() on c.IdKelas equals d.IdKelas
                                join e in conn.GetList<TbUnit>() on d.IdUnit equals e.IdUnit
                                join f in conn.GetList<TbJenisPekerjaan>() on a.IdJenisPekerjaanIbu equals f.IdJenisPekerjaan
                                join g in conn.GetList<TbJenisPekerjaan>() on a.IdJenisPekerjaanAyah equals g.IdJenisPekerjaan
                                select new TaSiswa
                                {
                                    IdTahunAjaran = IdTahunAjaran,
                                    Nis = a.Nis,
                                    Unit = e.Nama,
                                    Kelas = d.Nama,
                                    KelasParalel = c.Nama,
                                    Nik = a.Nik,
                                    Nama = a.Nama,
                                    NamaPanggilan = a.NamaPanggilan,
                                    TempatLahir = a.TempatLahir,
                                    TanggalLahir = a.TanggalLahir,
                                    Agama = AgamaConstant.Dict[a.IdAgama],
                                    KdJenisKelamin = JenisKelaminConstant.Dict[a.KdJenisKelamin],
                                    NoHandphone = a.NoHandphone,
                                    NoDarurat = a.NoDarurat,
                                    Email = a.Email,
                                    AlamatTinggal = a.AlamatTinggal,
                                    AlamatOrtu = a.AlamatOrtu,
                                    NikIbu = a.NikIbu,
                                    NamaIbu = a.NamaIbu,
                                    JenisPekerjaanIbu = f.Nama,
                                    NamaInstansiIbu = a.NamaInstansiIbu,
                                    NikAyah = a.NikAyah,
                                    NamaAyah = a.NamaAyah,
                                    JenisPekerjaanAyah = g.Nama,
                                    NamaInstansiAyah = a.NamaInstansiAyah,
                                    EmailOrtu = a.EmailOrtu,
                                    Status = a.Status,
                                    CreatedBy = a.CreatedBy,
                                    CreatedDate = a.CreatedDate,
                                    UpdatedBy = a.UpdatedBy,
                                    UpdatedDate = a.UpdatedDate,
                                    ArchivedBy = IdUser,
                                    ArchivedDate = dateTimeService.GetCurrdate()
                                };
                if (dataSiswa != null && dataSiswa.Count() > 0)
                {
                    foreach (var item in dataSiswa)
                    {
                        conn.Insert(dataSiswa);
                    }
                }

                var dataSiswaPrestasi = from a in conn.GetList<TbSiswaPrestasi>()
                                        join b in conn.GetList<TbSiswa>() on a.IdSiswa equals b.IdSiswa
                                        select new TaSiswaPrestasi
                                        {
                                            IdTahunAjaran = IdTahunAjaran,
                                            Nis = b.Nis,
                                            JenisPrestasi = JenisPrestasiConstant.DictJenisPrestasi[a.IdJenisPrestasi],
                                            Tahun = a.Tahun,
                                            JenisTingkatPrestasi = JenisTingkatPrestasiSiswaConstant.DictJenisTingkatPrestasiSiswa[a.IdJenisTingkatPrestasi],
                                            NamaPenyelenggara = a.NamaPenyelenggara,
                                            Keterangan = a.Keterangan,
                                            FileSertifikat = a.FileSertifikat
                                        };
                if (dataSiswaPrestasi != null && dataSiswaPrestasi.Count() > 0)
                    foreach (var item in dataSiswaPrestasi)
                    {
                        conn.Insert(dataSiswaPrestasi);
                    }

                var dataSiswaRapor = from a in conn.GetList<TbSiswaRapor>()
                                     join b in conn.GetList<TbJenisRapor>() on a.IdJenisRapor equals b.IdJenisRapor
                                     join c in conn.GetList<TbSiswa>() on a.IdSiswa equals c.IdSiswa
                                     select new TaSiswaRapor
                                     {
                                         IdTahunAjaran = IdTahunAjaran,
                                         Nis = c.Nis,
                                         JenisRapor = b.Nama,
                                         FileRapor = a.FileRapor
                                     };
                if (dataSiswaRapor != null && dataSiswaRapor.Count() > 0)
                    foreach (var item in dataSiswaRapor)
                    {
                        conn.Insert(dataSiswaRapor);
                    }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetSiswaArchive", ex);
            }
        }
        public string SetSiswaArchive(int IdUser, int IdTahunAjaran)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string err = SetSiswaArchive(conn, IdUser, IdTahunAjaran);
                        if (!string.IsNullOrEmpty(err)) return err;
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetSiswaArchive", ex);
            }
        }
        public string SetSiswaReArchive(int IdUser, int IdTahunAjaran)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var taSiswaRapors = from a in conn.GetList<TaSiswaRapor>()
                                            where a.IdTahunAjaran == IdTahunAjaran
                                            select a;
                        if (taSiswaRapors != null & taSiswaRapors.Count() > 0)
                            conn.DeleteList<TaSiswaRapor>(taSiswaRapors.ToList());

                        var taSiswaPrestasis = from a in conn.GetList<TaSiswaPrestasi>()
                                               where a.IdTahunAjaran == IdTahunAjaran
                                               select a;
                        if (taSiswaPrestasis != null & taSiswaPrestasis.Count() > 0)
                            conn.DeleteList<TaSiswaPrestasi>(taSiswaPrestasis.ToList());

                        var taSiswas = from a in conn.GetList<TaSiswa>()
                                       where a.IdTahunAjaran == IdTahunAjaran
                                       select a;
                        if (taSiswas != null & taSiswas.Count() > 0)
                            conn.DeleteList<TaSiswa>(taSiswas.ToList());

                        string err = SetSiswaArchive(IdUser, IdTahunAjaran);
                        if (!string.IsNullOrEmpty(err)) return err;
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetSiswaArchive", ex);
            }
        }
        // public string SiswaEditUpload(int IdUser, List<SheetSiswaModel> Data)
        // {
        //     try
        //     {
        //         using (var conn = commonService.DbConnection())
        //         {
        //             conn.Open();
        //             using (var tx = conn.BeginTransaction())
        //             {
        //                 string err = "";
        //                 foreach (var item in Data)
        //                 {
        //                     string tempErr = SiswaEditExcel(IdUser, item);
        //                     if (!string.IsNullOrEmpty(tempErr))
        //                         err += tempErr + "; " + Environment.NewLine;
        //                 }
        //                 if (!string.IsNullOrEmpty(err)) return err;
        //                 tx.Commit();
        //                 return string.Empty;
        //             }
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         return commonService.GetErrorMessage(ServiceName + "SiswaEditUpload", ex);
        //     }
        // }
        public string SiswaEditUpload(int IdUser, List<SheetSiswaModel> Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    string err = "";
                    using (var tx = conn.BeginTransaction())
                    {
                        foreach (var item in Data)
                        {
                            string nis_err = string.Format("NIS : {0} - ", item.Nis);
                            var tbSiswa = (from a in conn.GetList<TbKelasRombel>()
                                           join b in conn.GetList<TbKelasParalel>() on a.IdKelasParalel equals b.IdKelasParalel
                                           join c in conn.GetList<TbSiswa>() on a.IdSiswa equals c.IdSiswa
                                           where c.Nis == item.Nis
                                           select new { siswa = c, c.IdSiswa, KelasParalel = b.NamaSingkat, c.Nama }).FirstOrDefault();
                            if (tbSiswa == null)
                            {
                                return nis_err + "data tidak ditemukan! proses dibatalkan!";
                            }

                            tbSiswa.siswa.Nis = item.Nis;
                            tbSiswa.siswa.Nik = item.Nik;
                            tbSiswa.siswa.Nama = item.Nama;
                            tbSiswa.siswa.NamaPanggilan = item.NamaPanggilan;
                            tbSiswa.siswa.TempatLahir = item.TempatLahir;
                            tbSiswa.siswa.TanggalLahir = dateTimeService.StrToDateTime(item.TanggalLahir);

                            var idAgama = (from a in AgamaConstant.Dict
                                           where a.Value == item.Agama
                                           select a.Key).FirstOrDefault();
                            if (idAgama != 0)
                            {
                                tbSiswa.siswa.IdAgama = idAgama;
                            }
                            else
                            {
                                err += nis_err + "pilihan agama tidak sesuai";
                            }

                            if (item.JenisKelamin == "Perempuan")
                                item.JenisKelamin = "P";
                            if (item.JenisKelamin == "Laki-laki")
                                item.JenisKelamin = "L";

                            var kdJenisKelamin = (from a in JenisKelaminConstant.Dict
                                                  where a.Key == item.JenisKelamin
                                                  select a.Key).FirstOrDefault();
                            tbSiswa.siswa.KdJenisKelamin = kdJenisKelamin;

                            tbSiswa.siswa.NoHandphone = item.NoHandphone;
                            tbSiswa.siswa.NoDarurat = item.NoDarurat;
                            tbSiswa.siswa.Email = item.Email;
                            tbSiswa.siswa.AlamatTinggal = item.AlamatTinggal;
                            tbSiswa.siswa.AlamatOrtu = item.AlamatOrtu;
                            tbSiswa.siswa.NikIbu = item.NikIbu;
                            tbSiswa.siswa.NamaIbu = item.NamaIbu;

                            var jenisPekerjaanIbu = (from a in conn.GetList<TbJenisPekerjaan>()
                                                     where a.Nama == item.JenisPekerjaanIbu || a.NamaSingkat == item.JenisPekerjaanIbu
                                                     select a).FirstOrDefault();
                            if (jenisPekerjaanIbu != null)
                            {
                                tbSiswa.siswa.IdJenisPekerjaanIbu = jenisPekerjaanIbu.IdJenisPekerjaan;
                            }
                            else
                            {
                                err += nis_err + "jenis pekerjaan ibu tidak sesuai!";
                            }
                            tbSiswa.siswa.NamaInstansiIbu = item.NamaInstansiIbu;
                            tbSiswa.siswa.NikAyah = item.NikAyah;
                            tbSiswa.siswa.NamaAyah = item.NamaAyah;
                            var jenisPekerjaanAyah = (from a in conn.GetList<TbJenisPekerjaan>()
                                                      where a.Nama == item.JenisPekerjaanAyah || a.NamaSingkat == item.JenisPekerjaanAyah
                                                      select a).FirstOrDefault();
                            if (jenisPekerjaanAyah != null)
                            {
                                tbSiswa.siswa.IdJenisPekerjaanAyah = jenisPekerjaanAyah.IdJenisPekerjaan;
                            }
                            else
                            {
                                err += nis_err + "jenis pekerjaan ayah tidak sesuai!";
                            }
                            tbSiswa.siswa.NamaInstansiAyah = item.NamaInstansiAyah;
                            tbSiswa.siswa.EmailOrtu = item.EmailOrtu;
                            tbSiswa.siswa.Status = StatusDataConstant.Aktif;
                            tbSiswa.siswa.UpdatedBy = IdUser;
                            tbSiswa.siswa.UpdatedDate = dateTimeService.GetCurrdate();

                            var tbKelasParalel = (from a in conn.GetList<TbKelasParalel>()
                                                  where a.Nama == item.KelasParalel || a.NamaSingkat == item.KelasParalel
                                                  select a).FirstOrDefault();
                            if (tbKelasParalel != null)
                            {
                                var tbKelasRombel = (from a in conn.GetList<TbKelasRombel>()
                                                     where a.IdSiswa == tbSiswa.IdSiswa
                                                     select a).FirstOrDefault();
                                if (tbKelasRombel == null)
                                {
                                    tbKelasRombel = new TbKelasRombel
                                    {
                                        IdSiswa = tbSiswa.IdSiswa,
                                        IdKelasParalel = tbKelasParalel.IdKelasParalel
                                    };
                                    conn.Insert(tbKelasRombel);
                                }
                                else
                                {
                                    tbKelasRombel.IdSiswa = tbSiswa.IdSiswa;
                                    tbKelasRombel.IdKelasParalel = tbKelasParalel.IdKelasParalel;
                                    var sql = "UPDATE kbm.tb_kelas_rombel SET id_kelas_paralel = @IdKelasParalel WHERE id_siswa = @IdSiswa";
                                    conn.Execute(sql, tbKelasRombel);

                                }

                                conn.Update(tbSiswa.siswa);

                            }
                            else
                            {
                                err += nis_err + "kelas paralel tidak ditemukan!";
                            }

                        }
                        tx.Commit();
                        return err;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SiswaEditExcel", ex);
            }
        }

        public string SiswaTagihanUpload(int IdUser, List<SiswaTagihanAddModel> Data)
        {
            string tempErr = string.Empty;
            try
            {
                string err = string.Empty;
                Console.WriteLine(DateTime.Now);
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {


                        List<TbSiswaTagihan> tbSiswaTagihans = new List<TbSiswaTagihan>();
                        List<TbSiswaTagihanDetil> tbSiswaTagihanDetils = new List<TbSiswaTagihanDetil>();
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime periode = DateTime.ParseExact(Data[0].Bulan, "MM-yyyy", provider);
                        DateTime periodeBefore = periode.AddMonths(-1);
                        var currPeriode = (from a in conn.GetList<TbSiswaTagihan>()
                                           select a).FirstOrDefault();
                        if (currPeriode != null)
                        {
                            if (currPeriode.Periode == periode)
                                return string.Format("data tagihan periode {0} sudah diupload", currPeriode.Periode.ToString("MM-yyyy"));

                        }
                        var cek = (from a in conn.GetList<TaSiswaTagihan>()
                                   where a.Periode == periode
                                   select a).FirstOrDefault();
                        if (cek != null)
                            return string.Format("data tagihan periode {0} sudah diupload", cek.Periode.ToString("MM-yyyy"));

                        if (currPeriode == null)
                        {
                            currPeriode = new TbSiswaTagihan
                            {
                                Periode = periode
                            };
                        }
                        string sql = string.Empty;

                        sql += " INSERT INTO kbm.ta_siswa_tagihan(";
                        sql += " 	id_siswa, periode, status, ref_no, min, max, created_by, created_date, updated_by, updated_date, catatan, archived_by, archived_date)";
                        sql += " SELECT	id_siswa, periode, status, ref_no, min, max, created_by, created_date, updated_by, updated_date, catatan, 1 archived_by, now() archived_date";
                        sql += " FROM kbm.tb_siswa_tagihan;";
                        sql += " INSERT INTO kbm.ta_siswa_tagihan_detil(";
                        sql += " 	id_siswa, periode, id_jenis_tagihan_siswa, tipe, rp)";
                        sql += " SELECT id_siswa, periode, id_jenis_tagihan_siswa, 1, rp";
                        sql += " FROM kbm.tb_siswa_tagihan_detil";
                        sql += " where to_char(periode,'mm-yyyy') = '" + currPeriode.Periode.ToString("MM-yyyy") + "';";
                        sql += " INSERT INTO kbm.ta_siswa_tagihan_detil(";
                        sql += " 	id_siswa, periode, id_jenis_tagihan_siswa, tipe, rp)";
                        sql += " SELECT id_siswa, to_date('" + currPeriode.Periode.ToString("MM-yyyy") + "','mm-yyyy'), id_jenis_tagihan_siswa, -1, rp";
                        sql += " FROM kbm.tb_siswa_tagihan_detil";
                        sql += " where to_char(periode,'mm-yyyy') = '" + currPeriode.Periode.AddMonths(-1).ToString("MM-yyyy") + "';";

                        sql += " DELETE FROM kbm.tb_siswa_tagihan_detil;";
                        sql += " DELETE FROM kbm.tb_siswa_tagihan;";

                        //sql += " delete from kbm.tb_siswa_tagihan_detil";
                        //sql += " where id_siswa in (select id_siswa from kbm.tb_siswa_tagihan);";
                        //sql += " delete from kbm.tb_siswa_tagihan;";
                        var exec = conn.Execute(sql);

                        foreach (var item in Data)
                        {
                            var tbSiswa = (from a in conn.GetList<TbSiswa>()
                                           where a.Nis == item.Nis.Trim()
                                           select a).FirstOrDefault();
                            if (tbSiswa == null)
                            {
                                err += Environment.NewLine + string.Format("nis {0} siswa tidak terdaftar", item.Nis);
                                continue;
                            }
                            if (tbSiswa.NisLama != item.KodeBayar)
                            {
                                err += Environment.NewLine + string.Format("kode bayar {0} tidak sesuai nis {1}", item.KodeBayar, item.Nis);
                                continue;
                            }
                            tbSiswaTagihans.Add(new TbSiswaTagihan
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periode,
                                Status = 0,
                                Min = dateTimeService.StrToDateTime(item.Min),
                                Max = dateTimeService.StrToDateTime(item.Max),
                                CreatedBy = IdUser,
                                CreatedDate = dateTimeService.GetCurrdate(),
                                Catatan = item.Catatan
                            });
                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periode,
                                IdJenisTagihanSiswa = 1,
                                Rp = item.Spp2
                            });
                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periode,
                                IdJenisTagihanSiswa = 2,
                                Rp = item.Boks2
                            });

                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periode,
                                IdJenisTagihanSiswa = 3,
                                Rp = item.Ppdb2
                            });

                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periode,
                                IdJenisTagihanSiswa = 4,
                                Rp = item.Jemputan2
                            });

                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periode,
                                IdJenisTagihanSiswa = 5,
                                Rp = item.Katering2
                            });
                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periode,
                                IdJenisTagihanSiswa = 6,
                                Rp = item.Komite2
                            });

                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periodeBefore,
                                IdJenisTagihanSiswa = 1,
                                Rp = item.Spp1
                            });
                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periodeBefore,
                                IdJenisTagihanSiswa = 2,
                                Rp = item.Boks1
                            });

                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periodeBefore,
                                IdJenisTagihanSiswa = 3,
                                Rp = item.Ppdb1
                            });

                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periodeBefore,
                                IdJenisTagihanSiswa = 4,
                                Rp = item.Jemputan1
                            });

                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periodeBefore,
                                IdJenisTagihanSiswa = 5,
                                Rp = item.Katering1
                            });

                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periodeBefore,
                                IdJenisTagihanSiswa = 6,
                                Rp = item.Komite1
                            });
                        }
                        sql = "INSERT INTO kbm.tb_siswa_tagihan(";
                        sql += "	id_siswa, periode, status, ref_no, created_by, created_date, updated_by, updated_date, catatan, min, max)";
                        sql += "	VALUES (@IdSiswa, @Periode, @Status, @RefNo, @CreatedBy, @CreatedDate, @UpdatedBy, @UpdatedDate, @Catatan, @Min, @Max);";
                        var insTbSiswaTagihan = conn.Execute(sql, tbSiswaTagihans);
                        sql = "INSERT INTO kbm.tb_siswa_tagihan_detil(";
                        sql += "	id_siswa, periode, id_jenis_tagihan_siswa, rp)";
                        sql += "	VALUES (@IdSiswa, @Periode, @IdJenisTagihanSiswa, @Rp);";
                        var insTbSiswaTagihanDetil = conn.Execute(sql, tbSiswaTagihanDetils);
                        tx.Commit();
                        Console.WriteLine(DateTime.Now);
                        return err;
                    }
                }
            }
            catch (Exception ex)
            {
                return tempErr + Environment.NewLine + commonService.GetErrorMessage(ServiceName + "SiswaTagihanUpload", ex);
            }
        }
        public string SiswaTagihanUploadSusulan(int IdUser, List<SiswaTagihanAddModel> Data)
        {
            string tempErr = string.Empty;
            try
            {
                string err = string.Empty;
                Console.WriteLine(DateTime.Now);
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string sql = string.Empty;

                        List<TbSiswaTagihan> tbSiswaTagihans = new List<TbSiswaTagihan>();
                        List<TbSiswaTagihanDetil> tbSiswaTagihanDetils = new List<TbSiswaTagihanDetil>();
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime periode = DateTime.ParseExact(Data[0].Bulan, "MM-yyyy", provider);
                        DateTime periodeBefore = periode.AddMonths(-1);
                        var currPeriode = (from a in conn.GetList<TbSiswaTagihan>()
                                           where a.Periode == periode
                                           select a).FirstOrDefault();
                        if (currPeriode == null)
                        {
                            return string.Format("data tagihan periode {0} belum diupload", periode.ToString("MM-yyyy"));

                        }
                        foreach (var item in Data)
                        {
                            var tbSiswa = (from a in conn.GetList<TbSiswa>()
                                           where a.Nis == item.Nis.Trim()
                                           select a).FirstOrDefault();
                            if (tbSiswa == null)
                            {
                                err += Environment.NewLine + string.Format("nis {0} siswa tidak terdaftar", item.Nis);
                                continue;
                            }
                            if (tbSiswa.NisLama != item.KodeBayar)
                            {
                                err += Environment.NewLine + string.Format("kode bayar {0} tidak sesuai nis {1}", item.KodeBayar, item.Nis);
                                continue;
                            }

                            //hapus
                            var tbSiswaTagihan = conn.GetList<TbSiswaTagihan>().Where(x => x.IdSiswa == tbSiswa.IdSiswa).FirstOrDefault();
                            if (tbSiswaTagihan != null)
                                conn.Delete(tbSiswaTagihan);

                            var tbSiswaTagihanDetil = conn.GetList<TbSiswaTagihanDetil>().Where(x => x.IdSiswa == tbSiswa.IdSiswa).ToList();
                            if (tbSiswaTagihanDetil.Count != 0)
                            {
                                foreach (var itemx in tbSiswaTagihanDetil)
                                {
                                    conn.Delete(itemx);
                                }
                            }


                            //tambah
                            tbSiswaTagihans.Add(new TbSiswaTagihan
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periode,
                                Status = 0,
                                Min = dateTimeService.StrToDateTime(item.Min),
                                Max = dateTimeService.StrToDateTime(item.Max),
                                CreatedBy = IdUser,
                                CreatedDate = dateTimeService.GetCurrdate(),
                                Catatan = item.Catatan
                            });
                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periode,
                                IdJenisTagihanSiswa = 1,
                                Rp = item.Spp2
                            });
                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periode,
                                IdJenisTagihanSiswa = 2,
                                Rp = item.Boks2
                            });

                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periode,
                                IdJenisTagihanSiswa = 3,
                                Rp = item.Ppdb2
                            });

                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periode,
                                IdJenisTagihanSiswa = 4,
                                Rp = item.Jemputan2
                            });

                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periode,
                                IdJenisTagihanSiswa = 5,
                                Rp = item.Katering2
                            });
                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periode,
                                IdJenisTagihanSiswa = 6,
                                Rp = item.Komite2
                            });

                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periodeBefore,
                                IdJenisTagihanSiswa = 1,
                                Rp = item.Spp1
                            });
                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periodeBefore,
                                IdJenisTagihanSiswa = 2,
                                Rp = item.Boks1
                            });

                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periodeBefore,
                                IdJenisTagihanSiswa = 3,
                                Rp = item.Ppdb1
                            });

                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periodeBefore,
                                IdJenisTagihanSiswa = 4,
                                Rp = item.Jemputan1
                            });

                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periodeBefore,
                                IdJenisTagihanSiswa = 5,
                                Rp = item.Katering1
                            });

                            tbSiswaTagihanDetils.Add(new TbSiswaTagihanDetil
                            {
                                IdSiswa = tbSiswa.IdSiswa,
                                Periode = periodeBefore,
                                IdJenisTagihanSiswa = 6,
                                Rp = item.Komite1
                            });
                        }
                        sql = "INSERT INTO kbm.tb_siswa_tagihan(";
                        sql += "	id_siswa, periode, status, ref_no, created_by, created_date, updated_by, updated_date, catatan, min, max)";
                        sql += "	VALUES (@IdSiswa, @Periode, @Status, @RefNo, @CreatedBy, @CreatedDate, @UpdatedBy, @UpdatedDate, @Catatan, @Min, @Max);";
                        var insTbSiswaTagihan = conn.Execute(sql, tbSiswaTagihans);
                        sql = "INSERT INTO kbm.tb_siswa_tagihan_detil(";
                        sql += "	id_siswa, periode, id_jenis_tagihan_siswa, rp)";
                        sql += "	VALUES (@IdSiswa, @Periode, @IdJenisTagihanSiswa, @Rp);";
                        var insTbSiswaTagihanDetil = conn.Execute(sql, tbSiswaTagihanDetils);
                        tx.Commit();
                        Console.WriteLine(DateTime.Now);
                        return err;
                    }
                }
            }
            catch (Exception ex)
            {
                return tempErr + Environment.NewLine + commonService.GetErrorMessage(ServiceName + "SiswaTagihanUploadSusulan", ex);
            }
        }
        public SiswaTagihanModel GetSiswaTagihan(string Nis, out string oMessage)
        {
            oMessage = string.Empty;
            SiswaTagihanModel ret = new SiswaTagihanModel();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    ret = schService.GetSiswaTagihan(conn, Nis, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = Environment.NewLine + commonService.GetErrorMessage(ServiceName + "GetSiswaTagihan", ex);
                return null;
            }
        }


        public SiswaModel SiswaLogin(string Nis, string Password, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                //string Pass = common.DecryptString(Password);
                if (string.IsNullOrEmpty(Nis))
                {
                    oMessage = "nisn tidak boleh kosong";
                    return null;
                }
                if (string.IsNullOrEmpty(Password))
                {
                    oMessage = "password tidak boleh kosong";
                    return null;
                }
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();


                    var tbSiswa = (from a in conn.GetList<TbSiswa>()
                                   where a.Nis == Nis
                                   select a).FirstOrDefault();
                    if (tbSiswa == null)
                    {
                        oMessage = "nisn tidak terdaftar";
                        return null;
                    }
                    if (tbSiswa.Status != StatusDataConstant.Aktif)
                    {
                        oMessage = "siswa sudah tidak aktif";
                        return null;
                    }
                    if (string.IsNullOrEmpty(tbSiswa.Password)) { oMessage = "password belum disetting"; return null; }

                    string password = commonService.DecryptString(tbSiswa.Password);
                    if (password != Password)
                    {
                        oMessage = "Password Salah";
                        return null;
                    }

                    var ret = schService.GetSiswa(conn, tbSiswa.IdSiswa, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;

                    ret.KelasQuran = schService.GetKelasQuranSiswa(conn, tbSiswa.IdSiswa, string.Empty, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;

                    PushInboxNotif(
                        tbSiswa.IdSiswa,
                        "Login Terdeteksi",
                        "(Jam : " + dateTimeService.GetCurrdate().ToString("HH:mm") + ") Baru-baru ini ada yang login menggunakan akun " + tbSiswa.Nis + ", apabila bukan anda silahkan ubah password sekarang!",
                        TipeInboxConstant.Login,
                        null
                    );



                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "SiswaLogin", ex);
                return null;
            }
        }
        public string SiswaChangePassword(string Nis, string OldPassword, string NewPassword1, string NewPassword2)
        {
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        try
                        {
                            var tbSiswa = (from a in conn.GetList<TbSiswa>()
                                           where a.Nis == Nis
                                           select a).FirstOrDefault();
                            if (tbSiswa == null)
                            {
                                return "nisn tidak terdaftar";
                            }
                            string password = commonService.DecryptString(tbSiswa.Password);
                            if (password != OldPassword)
                            {
                                return "Password lama salah";
                            }

                            if (string.IsNullOrEmpty(NewPassword1))
                            {
                                return "Password baru kosong";
                            }
                            if (string.IsNullOrEmpty(NewPassword2))
                            {
                                return "Konfirmasi password kosong";
                            }
                            if (NewPassword1 != NewPassword2)
                            {
                                return "Konfirmasi password tidak sama dengan password baru";
                            }
                            tbSiswa.Password = commonService.EncryptString(NewPassword1);
                            conn.Update(tbSiswa);
                            tx.Commit();
                            return string.Empty;
                        }
                        catch (Exception ex)
                        {
                            tx.Rollback();
                            return commonService.GetErrorMessage(ServiceName + "SiswaChangePassword", ex);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SiswaChangePassword", ex);
            }

        }
        public string SiswaResetPassword(string Nis, out string oMessage)
        {
            oMessage = string.Empty;

            if (string.IsNullOrEmpty(Nis))
            {
                oMessage = "nisn tidak boleh kosong";
                return null;
            }
            string _newPassword = commonService.CreateRandomPassword(8);
            string _newMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbSiswa = (from a in conn.GetList<TbSiswa>()
                                       where a.Nis == Nis
                                       select a).FirstOrDefault();
                        if (tbSiswa == null)
                        {
                            oMessage = "nisn tidak terdaftar";
                            return null;
                        }

                        tbSiswa.Password = commonService.EncryptString(_newPassword);
                        _newMessage = tbSiswa.Email;
                        conn.Update(tbSiswa);
                        tx.Commit();

                        StringBuilder stb = new StringBuilder();
                        stb.Append("<html>");
                        stb.Append("<body>");
                        stb.Append("Berikut kami informasikan password baru anda adalah <strong>" + _newPassword + "</strong><br><br>");
                        stb.Append("</body>");
                        stb.Append("</html>");
                        List<string> emailTos = new List<string>();
                        List<string> emailCcs = new List<string>();
                        if (string.IsNullOrEmpty(tbSiswa.Email))
                        {
                            if (string.IsNullOrEmpty(tbSiswa.EmailOrtu))
                            {
                                emailTos.Add(AppSetting.OrgEmail);
                            }
                            else
                            {
                                emailTos.Add(AppSetting.OrgEmail);
                                emailCcs.Add(tbSiswa.EmailOrtu);
                            }

                        }
                        else
                        {
                            emailTos.Add(tbSiswa.Email);
                            emailCcs.Add(AppSetting.OrgEmail);
                            if (!string.IsNullOrEmpty(tbSiswa.EmailOrtu))
                            {
                                emailCcs.Add(tbSiswa.EmailOrtu);
                            }
                        }

                        SendEmailModel email = new SendEmailModel
                        {
                            To = emailTos,
                            Title = tbSiswa.Nama,
                            Cc = emailCcs,
                            Subject = "Permintaan reset password ananda " + tbSiswa.Nama,
                            Message = stb.ToString(),
                            Attachments = null
                        };
                        _ = emailService.SendHtmlEmailAsync(email);
                    }
                }
                return _newMessage;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "SiswaResetPassword", ex);
                return string.Empty;
            }
        }
        public string UploadRaporSiswa(int IdUser, int IdSiswa, int IdJenisRapor, IFormFile FileRapor)
        {
            try
            {
                if (FileRapor == null)
                    return "file pdf rapor harus diupload";
                if (FileRapor.Length == 0)
                    return "ukuran file rapor salah (0)";
                if (Path.GetExtension(FileRapor.FileName).ToLower() != ".pdf")
                {
                    return "rapor harus dalam format pdf";
                }
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbSiswa = conn.Get<TbSiswa>(IdSiswa);
                        if (tbSiswa == null) return "data siswa tidak ada";

                        var dataRapor = (from a in conn.GetList<TbSiswaRapor>()
                                         join b in conn.GetList<TbJenisRapor>() on a.IdJenisRapor equals b.IdJenisRapor
                                         where a.IdSiswa == IdSiswa
                                         & a.IdJenisRapor == IdJenisRapor
                                         select b).FirstOrDefault();
                        if (dataRapor != null)
                            return string.Format("rapor {0} sudah diupload", dataRapor.Nama);
                        TbSiswaRapor tbSiswaRapor = new TbSiswaRapor
                        {
                            IdSiswa = IdSiswa,
                            IdJenisRapor = IdJenisRapor
                        };
                        tbSiswaRapor.FileRapor = string.Format("rapor_{0}_{1}_{2}{3}", IdSiswa.ToString(), tahunAjaran.IdTahunAjaran.ToString(), IdJenisRapor.ToString(), Path.GetExtension(FileRapor.FileName));
                        using (var fileStream = new FileStream(Path.Combine(AppSetting.PathSiswaRapor, tbSiswaRapor.FileRapor), FileMode.Create))
                        {
                            FileRapor.CopyTo(fileStream);
                        }
                        conn.Insert(tbSiswaRapor);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UploadRaporSiswa", ex);
            }
        }
        public string ReUploadRaporSiswa(int IdUser, int IdSiswa, int IdJenisRapor, IFormFile FileRapor)
        {
            try
            {
                if (FileRapor == null)
                    return "file pdf rapor harus diupload";
                if (FileRapor.Length == 0)
                    return "ukuran file rapor salah (0)";
                if (Path.GetExtension(FileRapor.FileName).ToLower() != ".pdf")
                {
                    return "rapor harus dalam format pdf";
                }
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbSiswa = conn.Get<TbSiswa>(IdSiswa);
                        if (tbSiswa == null) return "data siswa tidak ada";

                        var tbJenisRapor = conn.Get<TbJenisRapor>(IdJenisRapor);
                        if (tbJenisRapor == null) return "jenis rapor salah";

                        var tbSiswaRapor = (from a in conn.GetList<TbSiswaRapor>()
                                            where a.IdSiswa == IdSiswa
                                            & a.IdJenisRapor == IdJenisRapor
                                            select a
                                            ).FirstOrDefault();

                        if (tbSiswaRapor == null)
                            return string.Format("rapor {0} {1} belum diupload", tbJenisRapor.Nama, tbSiswa.Nama);

                        if (File.Exists(Path.Combine(AppSetting.PathSiswaRapor, tbSiswaRapor.FileRapor)))
                            File.Delete(Path.Combine(AppSetting.PathSiswaRapor, tbSiswaRapor.FileRapor));

                        tbSiswaRapor.FileRapor = string.Format("rapor_{0}_{1}_{2}{3}", IdSiswa.ToString(), tahunAjaran.IdTahunAjaran.ToString(), IdJenisRapor.ToString(), Path.GetExtension(FileRapor.FileName));
                        using (var fileStream = new FileStream(Path.Combine(AppSetting.PathSiswaRapor, tbSiswaRapor.FileRapor), FileMode.Create))
                        {
                            FileRapor.CopyTo(fileStream);
                        }
                        conn.Update(tbSiswaRapor);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "ReUploadRaporSiswa", ex);
            }
        }

        public string ValidasiPin(int IdSiswa, string Pin)
        {
            try
            {
                if (string.IsNullOrEmpty(Pin))
                    return "PIN salah";
                if (Pin.Length > 6)
                    return "PIN salah";
                try
                {
                    int cekNum = int.Parse(Pin);
                }
                catch (Exception)
                {
                    return "PIN salah";
                }
                using (var conn = commonService.DbConnection())
                {
                    var tbsiswa = conn.Get<TbSiswa>(IdSiswa);
                    if (tbsiswa == null) return "data siswa tidak ada";
                    if (tbsiswa.Pin != Pin) return "PIN salah";
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "ValidasiPin", ex);
            }
        }

        public string GantiPin(int IdSiswa, string PinLama, string PinBaru)
        {
            try
            {
                if (string.IsNullOrEmpty(PinLama))
                    return "PIN Lama tidak boleh kosong";
                if (string.IsNullOrEmpty(PinBaru))
                    return "PIN Baru tidak boleh kosong";
                if (PinBaru.Length > 6)
                    return "PIN Baru tidak boleh lebih dari 6 digit angka";
                try
                {
                    int cekNum = int.Parse(PinBaru);
                }
                catch (Exception)
                {
                    return "PIN Baru salah";
                }
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbsiswa = conn.Get<TbSiswa>(IdSiswa);
                        if (tbsiswa == null) return "data siswa tidak ada";
                        if (tbsiswa.Pin != PinLama) return "PIN Lama salah";
                        tbsiswa.Pin = PinBaru;
                        conn.Update(tbsiswa);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "GantiPin", ex);
            }
        }

        public string ResetPin(int IdSiswa)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbsiswa = conn.Get<TbSiswa>(IdSiswa);
                        if (tbsiswa == null) return "data siswa tidak ada";

                        if (tbsiswa.Email == null)
                        {
                            return "Email Siswa masih kosong";
                        }
                        else if (tbsiswa.EmailOrtu == null)
                        {
                            return "Email Orang Tua masih kosong";
                        }
                        Random generator = new Random();
                        string newPin = generator.Next(0, 999999).ToString("D6");
                        tbsiswa.Pin = newPin;
                        conn.Update(tbsiswa);
                        tx.Commit();

                        StringBuilder stb = new StringBuilder();
                        stb.Append("<html>");
                        stb.Append("<body>");
                        stb.Append("Berikut kami informasikan PIN baru anda adalah <strong>" + newPin + "</strong><br><br>");
                        stb.Append("</body>");
                        stb.Append("</html>");

                        List<string> emailTos = new List<string>();
                        List<string> emailCcs = new List<string>();
                        if (string.IsNullOrEmpty(tbsiswa.EmailOrtu))
                        {
                            if (string.IsNullOrEmpty(tbsiswa.EmailOrtu))
                            {
                                emailTos.Add(AppSetting.OrgEmail);
                            }
                            else
                            {
                                emailTos.Add(AppSetting.OrgEmail);
                                emailCcs.Add(tbsiswa.EmailOrtu);
                            }

                        }
                        else
                        {
                            emailCcs.Add(AppSetting.OrgEmail);
                            if (!string.IsNullOrEmpty(tbsiswa.EmailOrtu))
                            {
                                emailTos.Add(tbsiswa.EmailOrtu);
                            }
                        }
                        SendEmailModel email = new SendEmailModel
                        {
                            To = emailTos,
                            Title = tbsiswa.Nama,
                            Cc = emailCcs,
                            Subject = "Permintaan reset PIN ananda " + tbsiswa.Nama,
                            Message = stb.ToString(),
                            Attachments = null
                        };
                        _ = emailService.SendHtmlEmailAsync(email);
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "ResetPin", ex);
            }
        }

        public string EditProfilSiswa(int IdSiswa, string Email, string EmailOrtu, string NoHandphone, IFormFile FotoProfile)
        {
            try
            {
                if (string.IsNullOrEmpty(Email))
                    return "Email Siswa tidak boleh kosong";
                if (string.IsNullOrEmpty(EmailOrtu))
                    return "Email Orang Tua tidak boleh kosong";
                if (string.IsNullOrEmpty(NoHandphone))
                    return "No Handphone tidak boleh kosong";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbsiswa = conn.Get<TbSiswa>(IdSiswa);
                        if (tbsiswa == null) return "data siswa tidak ada";
                        tbsiswa.Email = Email;
                        tbsiswa.EmailOrtu = EmailOrtu;
                        tbsiswa.NoHandphone = NoHandphone;
                        if (FotoProfile != null)
                        {
                            string pathUpload = AppSetting.PathSiswaFoto;
                            if (!Directory.Exists(pathUpload))
                                Directory.CreateDirectory(pathUpload);

                            if (tbsiswa.FotoProfile != null)
                            {
                                if (File.Exists(Path.Combine(pathUpload, tbsiswa.FotoProfile)))
                                {
                                    File.Delete(Path.Combine(pathUpload, tbsiswa.FotoProfile));
                                }
                            }

                            Random rnd = new Random();
                            var rr = rnd.Next(1, 13);
                            tbsiswa.FotoProfile = DateTime.Now.ToString("yyyyMMddHHmmss" + rr) + Path.GetExtension(FotoProfile.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, tbsiswa.FotoProfile), FileMode.Create, FileAccess.Write))
                            {
                                FotoProfile.CopyTo(fileStream);
                            }

                        }
                        conn.Update(tbsiswa);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditProfilSiswa", ex);
            }
        }
        private string EditSiswaProfilSaveFile(string pathUpload, string newFileName, IFormFile formFile, string oldFileName)
        {
            try
            {
                if (formFile != null)
                {
                    if (!Directory.Exists(pathUpload))
                        Directory.CreateDirectory(pathUpload);

                    if (!string.IsNullOrEmpty(oldFileName))
                    {
                        if (File.Exists(Path.Combine(pathUpload, oldFileName)))
                            File.Delete(Path.Combine(pathUpload, oldFileName));
                    }

                    using (var fileStream = new FileStream(Path.Combine(pathUpload, newFileName), FileMode.Create, FileAccess.Write))
                    {
                        formFile.CopyTo(fileStream);
                    }

                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditSiswaProfilSaveFile", ex);
            }
        }
        public string EditSiswaProfil(int IdSiswa, SiswaEditProfileModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbsiswa = conn.Get<TbSiswa>(IdSiswa);
                        if (tbsiswa == null) return "data siswa tidak ada";

                        //tbsiswa.Nama = ""; tidak bisa diedit, silahkan hubungi administrator
                        //tbsiswa.Nis = ""; tidak bisa diedit, silahkan hubungi administrator
                        //tbsiswa.TanggalLahir = ""; tidak bisa diedit, silahkan hubungi administrator

                        if (!string.IsNullOrEmpty(Data.Nisn))
                            tbsiswa.Nisn = Data.Nisn;
                        if (!string.IsNullOrEmpty(Data.Nik))
                            tbsiswa.Nik = Data.Nik;
                        if (!string.IsNullOrEmpty(Data.NamaPanggilan))
                            tbsiswa.NamaPanggilan = Data.NamaPanggilan;
                        if (!string.IsNullOrEmpty(Data.TempatLahir))
                            tbsiswa.TempatLahir = Data.TempatLahir;
                        if (Data.IdAgama > 0)
                            tbsiswa.IdAgama = Data.IdAgama;
                        if (!string.IsNullOrEmpty(Data.KdJenisKelamin))
                            tbsiswa.KdJenisKelamin = Data.KdJenisKelamin;
                        if (!string.IsNullOrEmpty(Data.NoHandphone))
                            tbsiswa.NoHandphone = Data.NoHandphone;
                        if (!string.IsNullOrEmpty(Data.NoDarurat))
                            tbsiswa.NoDarurat = Data.NoDarurat;
                        if (!string.IsNullOrEmpty(Data.Email))
                            tbsiswa.Email = Data.Email;
                        if (!string.IsNullOrEmpty(Data.AlamatTinggal))
                            tbsiswa.AlamatTinggal = Data.AlamatTinggal;
                        if (!string.IsNullOrEmpty(Data.AlamatOrtu))
                            tbsiswa.AlamatOrtu = Data.AlamatOrtu;
                        if (!string.IsNullOrEmpty(Data.NikIbu))
                            tbsiswa.NikIbu = Data.NikIbu;
                        if (!string.IsNullOrEmpty(Data.NamaIbu))
                            tbsiswa.NamaIbu = Data.NamaIbu;
                        if (Data.IdJenisPekerjaanIbu > 0)
                            tbsiswa.IdJenisPekerjaanIbu = Data.IdJenisPekerjaanIbu;
                        if (!string.IsNullOrEmpty(Data.NamaInstansiIbu))
                            tbsiswa.NamaInstansiIbu = Data.NamaInstansiIbu;
                        if (!string.IsNullOrEmpty(Data.NikAyah))
                            tbsiswa.NikAyah = Data.NikAyah;
                        if (!string.IsNullOrEmpty(Data.NamaAyah))
                            tbsiswa.NamaAyah = Data.NamaAyah;
                        if (Data.IdJenisPekerjaanAyah > 0)
                            tbsiswa.IdJenisPekerjaanAyah = Data.IdJenisPekerjaanAyah;
                        if (!string.IsNullOrEmpty(Data.NamaInstansiAyah))
                            tbsiswa.NamaInstansiAyah = Data.NamaInstansiAyah;
                        if (!string.IsNullOrEmpty(Data.EmailOrtu))
                            tbsiswa.EmailOrtu = Data.EmailOrtu;

                        if (Data.FotoProfile != null)
                        {
                            string pathUpload = AppSetting.PathSiswaFoto;
                            string oldFileName = tbsiswa.FotoProfile;
                            tbsiswa.FotoProfile = $"{IdSiswa}_foto_frofile{Path.GetExtension(Data.FotoProfile.FileName)}";
                            string err = EditSiswaProfilSaveFile(pathUpload, tbsiswa.FotoProfile, Data.FotoProfile, oldFileName);
                        }
                        conn.Update(tbsiswa);

                        bool isUpdate = true;
                        TbSiswaProfil tbSiswaProfil = conn.Get<TbSiswaProfil>(IdSiswa);
                        if (tbSiswaProfil == null)
                        {
                            isUpdate = false;
                            tbSiswaProfil = new TbSiswaProfil
                            {
                                IdSiswa = IdSiswa,
                                FileKk = string.Empty,
                                FileAktaLahir = string.Empty,
                                FileIjazah = string.Empty,
                                FileKip = string.Empty,
                                FileShusbn = string.Empty
                            };
                        }
                        if (!string.IsNullOrEmpty(Data.NoAktaLahir))
                            tbSiswaProfil.NoAktaLahir = Data.NoAktaLahir;
                        if (!string.IsNullOrEmpty(Data.NoKip))
                            tbSiswaProfil.NoKip = Data.NoKip;
                        if (!string.IsNullOrEmpty(Data.KdGolonganDarah))
                            tbSiswaProfil.KdGolonganDarah = Data.KdGolonganDarah;
                        if (Data.IdKewarganegaraan > 0)
                            tbSiswaProfil.IdKewarganegaraan = Data.IdKewarganegaraan;
                        if (Data.IdBahasa > 0)
                            tbSiswaProfil.IdBahasa = Data.IdBahasa;
                        if (Data.TinggiBadan > 0)
                            tbSiswaProfil.TinggiBadan = Data.TinggiBadan;
                        if (Data.BeratBadan > 0)
                            tbSiswaProfil.BeratBadan = Data.BeratBadan;
                        if (Data.LingkarKepala > 0)
                            tbSiswaProfil.LingkarKepala = Data.LingkarKepala;
                        if (Data.AnakKe > 0)
                            tbSiswaProfil.AnakKe = Data.AnakKe;
                        if (Data.JumlahSodaraKandung > 0)
                            tbSiswaProfil.JumlahSodaraKandung = Data.JumlahSodaraKandung;
                        if (Data.JumlahSodaraTiri > 0)
                            tbSiswaProfil.JumlahSodaraTiri = Data.JumlahSodaraTiri;
                        if (Data.JumlahSodaraAngkat > 0)
                            tbSiswaProfil.JumlahSodaraAngkat = Data.JumlahSodaraAngkat;
                        if (Data.IdTransportasi > 0)
                            tbSiswaProfil.IdTransportasi = Data.IdTransportasi;
                        if (Data.JarakKeSekolah > 0)
                            tbSiswaProfil.JarakKeSekolah = Data.JarakKeSekolah;
                        if (Data.WaktuTempuh > 0)
                            tbSiswaProfil.WaktuTempuh = Data.WaktuTempuh;
                        if (!string.IsNullOrEmpty(Data.PenyakitDiderita))
                            tbSiswaProfil.PenyakitDiderita = Data.PenyakitDiderita;
                        if (!string.IsNullOrEmpty(Data.KelainanJasmani))
                            tbSiswaProfil.KelainanJasmani = Data.KelainanJasmani;
                        if (!string.IsNullOrEmpty(Data.SekolahAsal))
                            tbSiswaProfil.SekolahAsal = Data.SekolahAsal;
                        if (!string.IsNullOrEmpty(Data.AlamatSekolahAsal))
                            tbSiswaProfil.AlamatSekolahAsal = Data.AlamatSekolahAsal;
                        if (!string.IsNullOrEmpty(Data.NspnSekolahAsal))
                            tbSiswaProfil.NspnSekolahAsal = Data.NspnSekolahAsal;
                        tbSiswaProfil.PernahTkFormal = Data.PernahTkFormal;
                        tbSiswaProfil.PernahTkInformal = Data.PernahTlInformal;
                        if (Data.IdHobi > 0)
                            tbSiswaProfil.IdHobi = Data.IdHobi;
                        else
                            return "Hobi harus dipilih";
                        if (Data.IdCita2 > 0)
                            tbSiswaProfil.IdCita2 = Data.IdCita2;
                        else
                            return "Cita-cita harus diiplih";
                        if (!string.IsNullOrEmpty(Data.NoUjian))
                            tbSiswaProfil.NoUjian = Data.NoUjian;
                        if (!string.IsNullOrEmpty(Data.NoIjazah))
                            tbSiswaProfil.NoIjazah = Data.NoIjazah;
                        if (!string.IsNullOrEmpty(Data.NoShusbn))
                            tbSiswaProfil.NoShusbn = Data.NoShusbn;

                        tbSiswaProfil.KelasDapodik = Data.KelasDapodik;
                        tbSiswaProfil.BerkebutuhanKhusus = Data.BerkebutuhanKhusus;
                        tbSiswaProfil.AlamatKk = Data.AlamatKk;
                        tbSiswaProfil.PunyaWali = Data.PunyaWali;

                        tbSiswaProfil.TanggalLahirAyah = dateTimeService.StrToDateTime(Data.TanggalLahirAyah);
                        tbSiswaProfil.BerkebutuhanKhususAyah = Data.BerkebutuhanKhususAyah;
                        tbSiswaProfil.PendidikanTerakhirAyah = Data.PendidikanTerakhirAyah;
                        tbSiswaProfil.PenghasilanAyah = Data.PenghasilanAyah;
                        tbSiswaProfil.NomorHpAyah = Data.NomorHpAyah;

                        tbSiswaProfil.TanggalLahirIbu = dateTimeService.StrToDateTime(Data.TanggalLahirIbu);
                        tbSiswaProfil.BerkebutuhanKhususIbu = Data.BerkebutuhanKhususIbu;
                        tbSiswaProfil.PendidikanTerakhirIbu = Data.PendidikanTerakhirIbu;
                        tbSiswaProfil.PenghasilanIbu = Data.PenghasilanIbu;
                        tbSiswaProfil.NomorHpIbu = Data.NomorHpIbu;

                        if (Data.PunyaWali == "Ya")
                        {
                            if (string.IsNullOrEmpty(Data.NikWali)) return "Nik wali harus diisi!";
                            if (string.IsNullOrEmpty(Data.NamaWali)) return "Nik wali harus diisi!";
                            if (string.IsNullOrEmpty(Data.AlamatWali)) return "Nik wali harus diisi!";
                            if (Data.IdJenisPekerjaanWali == 0) return "Jenis pekerjaan wali harus diisi!";
                            if (string.IsNullOrEmpty(Data.NamaInstansiWali)) return "Nama instansi wali harus diisi!";
                            if (string.IsNullOrEmpty(Data.EmailWali)) return "Email wali harus diisi!";
                            if (string.IsNullOrEmpty(Data.NoHandphoneWali)) return "No handphone wali harus diisi!";

                            tbSiswaProfil.NikWali = Data.NikWali;
                            tbSiswaProfil.NamaWali = Data.NamaWali;
                            tbSiswaProfil.AlamatWali = Data.AlamatWali;
                            tbSiswaProfil.IdJenisPekerjaanWali = Data.IdJenisPekerjaanWali;
                            tbSiswaProfil.NamaInstansiWali = Data.NamaInstansiWali;
                            tbSiswaProfil.EmailWali = Data.EmailWali;
                            tbSiswaProfil.NoHandphoneWali = Data.NoHandphoneWali;
                        }

                        if (Data.FileKk != null)
                        {
                            string pathUpload = AppSetting.PathSiswaFoto;
                            string oldFileName = tbSiswaProfil.FileKk;
                            tbSiswaProfil.FileKk = $"{IdSiswa}_FileKk{Path.GetExtension(Data.FileKk.FileName)}";
                            string err = EditSiswaProfilSaveFile(pathUpload, tbSiswaProfil.FileKk, Data.FileKk, oldFileName);
                        }
                        if (Data.FileAktaLahir != null)
                        {
                            string pathUpload = AppSetting.PathSiswaFoto;
                            string oldFileName = tbSiswaProfil.FileAktaLahir;
                            tbSiswaProfil.FileAktaLahir = $"{IdSiswa}_FileAktaLahir{Path.GetExtension(Data.FileAktaLahir.FileName)}";
                            string err = EditSiswaProfilSaveFile(pathUpload, tbSiswaProfil.FileAktaLahir, Data.FileAktaLahir, oldFileName);
                        }
                        if (Data.FileShusbn != null)
                        {
                            string pathUpload = AppSetting.PathSiswaFoto;
                            string oldFileName = tbSiswaProfil.FileShusbn;
                            tbSiswaProfil.FileShusbn = $"{IdSiswa}_FileShusbn{Path.GetExtension(Data.FileShusbn.FileName)}";
                            string err = EditSiswaProfilSaveFile(pathUpload, tbSiswaProfil.FileShusbn, Data.FileShusbn, oldFileName);
                        }
                        if (Data.FileKip != null)
                        {
                            string pathUpload = AppSetting.PathSiswaFoto;
                            string oldFileName = tbSiswaProfil.FileKip;
                            tbSiswaProfil.FileKip = $"{IdSiswa}_FileKip{Path.GetExtension(Data.FileKip.FileName)}";
                            string err = EditSiswaProfilSaveFile(pathUpload, tbSiswaProfil.FileKip, Data.FileKip, oldFileName);
                        }
                        if (Data.FileIjazah != null)
                        {
                            string pathUpload = AppSetting.PathSiswaFoto;
                            string oldFileName = tbSiswaProfil.FileIjazah;
                            tbSiswaProfil.FileIjazah = $"{IdSiswa}_FileIjazah{Path.GetExtension(Data.FileIjazah.FileName)}";
                            string err = EditSiswaProfilSaveFile(pathUpload, tbSiswaProfil.FileIjazah, Data.FileIjazah, oldFileName);
                        }
                        tbSiswaProfil.TerimaKip = Data.TerimaKip;
                        tbSiswaProfil.AlasanKip = Data.AlasanKip;

                        if (isUpdate)
                            conn.Update(tbSiswaProfil);
                        else
                            conn.Insert(tbSiswaProfil);

                        TbSiswaKk tbSiswaKk = conn.Get<TbSiswaKk>(tbsiswa.IdSiswa);
                        isUpdate = true;
                        if (tbSiswaKk == null)
                        {
                            isUpdate = false;
                            tbSiswaKk = new TbSiswaKk { IdSiswa = tbsiswa.IdSiswa };
                        }
                        tbSiswaKk.NoKk = string.IsNullOrEmpty(Data.NoKk) ? "-" : Data.NoKk;
                        tbSiswaKk.Alamat = string.IsNullOrEmpty(Data.Alamat) ? "-" : Data.Alamat;
                        tbSiswaKk.Rt = string.IsNullOrEmpty(Data.Rt) ? "-" : Data.Rt;
                        tbSiswaKk.Rw = string.IsNullOrEmpty(Data.Rw) ? "-" : Data.Rw;
                        tbSiswaKk.Dusun = string.IsNullOrEmpty(Data.Dusun) ? "-" : Data.Dusun;
                        tbSiswaKk.IdWilDesa = Data.IdWilDesa == 0 ? 3271060005 : Data.IdWilDesa;
                        tbSiswaKk.KdPos = string.IsNullOrEmpty(Data.KdPos) ? "-" : Data.KdPos;
                        tbSiswaKk.NoTelp = string.IsNullOrEmpty(Data.NoTelp) ? "-" : Data.NoTelp;
                        tbSiswaKk.Longitude = Data.Longitude;
                        tbSiswaKk.Latitude = Data.Latitude;
                        if (isUpdate)
                            conn.Update(tbSiswaKk);
                        else
                            conn.Insert(tbSiswaKk);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditSiswaProfil", ex);
            }
        }
        public SiswaViewProfileModel GetSiswaProfile(int IdSiswa, out string oMessage)
        {
            oMessage = string.Empty;
            SiswaViewProfileModel ret = new SiswaViewProfileModel();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var item = (from siswa in conn.GetList<TbSiswa>()
                                join rombel in conn.GetList<TbKelasRombel>() on siswa.IdSiswa equals rombel.IdSiswa
                                join pekerjaanIbu in conn.GetList<TbJenisPekerjaan>() on siswa.IdJenisPekerjaanIbu equals pekerjaanIbu.IdJenisPekerjaan
                                join pekerjaanAyah in conn.GetList<TbJenisPekerjaan>() on siswa.IdJenisPekerjaanAyah equals pekerjaanAyah.IdJenisPekerjaan
                                join kelasParalel in conn.GetList<TbKelasParalel>() on rombel.IdKelasParalel equals kelasParalel.IdKelasParalel
                                join siswaProfil in (from profil in conn.GetList<TbSiswaProfil>()
                                                     join hobi in conn.GetList<TbJenisHobi>() on profil.IdHobi equals hobi.IdJenisHobi
                                                     join cita2 in conn.GetList<TbJenisCita2>() on profil.IdCita2 equals cita2.IdJenisCita2
                                                     select new { profil, hobi, cita2 }
                                                      ) on siswa.IdSiswa equals siswaProfil.profil.IdSiswa into siswaProfil1
                                from siswaProfil in siswaProfil1.DefaultIfEmpty()
                                join siswaKk in (from kk in conn.GetList<TbSiswaKk>()
                                                 join ds in conn.GetList<TbWilDesa>() on kk.IdWilDesa equals ds.IdWilDesa
                                                 join kec in conn.GetList<TbWilKecamatan>() on ds.IdWilKecamatan equals kec.IdWilKecamatan
                                                 join kab in conn.GetList<TbWilKabkot>() on kec.IdWilKabkot equals kab.IdWilKabkot
                                                 join prov in conn.GetList<TbWilProvinsi>() on kab.IdWilProvinsi equals prov.IdWilProvinsi
                                                 select new { kk, ds, kec, kab, prov }) on siswa.IdSiswa equals siswaKk.kk.IdSiswa into siswaKk1
                                from siswaKk in siswaKk1.DefaultIfEmpty()
                                where siswa.IdSiswa == IdSiswa
                                select new { siswa, kelasParalel, pekerjaanIbu, pekerjaanAyah, siswaProfil, siswaKk }).FirstOrDefault();
                    if (item == null) { oMessage = "data tidak ada"; return null; }

                    ret.IdSiswa = item.siswa.IdSiswa;
                    ret.KelasParalel = item.kelasParalel.Nama;
                    ret.Nis = item.siswa.Nis;
                    ret.Nisn = item.siswa.Nisn;
                    ret.Nik = item.siswa.Nik;
                    ret.Nama = item.siswa.Nama;
                    ret.NamaPanggilan = item.siswa.NamaPanggilan;
                    ret.TempatLahir = item.siswa.TempatLahir;
                    ret.TanggalLahir = item.siswa.TanggalLahir.ToString("dd-MM-yyyy");
                    ret.Agama = new ComboModel { Id = item.siswa.IdAgama, Nama = AgamaConstant.Dict[item.siswa.IdAgama] };
                    ret.JenisKelamin = new ComboModel { Kode = item.siswa.KdJenisKelamin, Nama = JenisKelaminConstant.Dict[item.siswa.KdJenisKelamin] };
                    ret.NoHandphone = item.siswa.NoHandphone;
                    ret.NoDarurat = item.siswa.NoDarurat;
                    ret.Email = item.siswa.Email;
                    ret.AlamatTinggal = item.siswa.AlamatTinggal;
                    ret.AlamatOrtu = item.siswa.AlamatOrtu;
                    ret.NikIbu = item.siswa.NikIbu;
                    ret.NamaIbu = item.siswa.NamaIbu;
                    ret.JenisPekerjaanIbu = new ComboModel { Id = item.siswa.IdJenisPekerjaanIbu, Nama = item.pekerjaanIbu.Nama };
                    ret.NamaInstansiIbu = item.siswa.NamaInstansiIbu;
                    ret.NikAyah = item.siswa.NikAyah;
                    ret.NamaAyah = item.siswa.NamaAyah;
                    ret.JenisPekerjaanAyah = new ComboModel { Id = item.siswa.IdJenisPekerjaanAyah, Nama = item.pekerjaanAyah.Nama };
                    ret.NamaInstansiAyah = item.siswa.NamaInstansiAyah;
                    ret.EmailOrtu = item.siswa.EmailOrtu;
                    ret.FotoProfile = item.siswa.FotoProfile;
                    if (item.siswaProfil != null)
                    {
                        ret.Cita2 = new ComboModel { Id = item.siswaProfil.cita2.IdJenisCita2, Nama = item.siswaProfil.cita2.Nama };

                        ret.NoAktaLahir = item.siswaProfil.profil.NoAktaLahir;
                        ret.FileKk = item.siswaProfil.profil.FileKk;
                        ret.FileAktaLahir = item.siswaProfil.profil.FileAktaLahir;
                        ret.NoKip = item.siswaProfil.profil.NoKip;
                        ret.FileKip = item.siswaProfil.profil.FileKip;
                        ret.GolonganDarah = new ComboModel { Kode = item.siswaProfil.profil.KdGolonganDarah, Nama = (item.siswaProfil.profil.KdGolonganDarah == null ? null : JenisGolonganDarahConstant.Dict[item.siswaProfil.profil.KdGolonganDarah]) };
                        ret.Kewarganegaraan = new ComboModel { Id = item.siswaProfil.profil.IdKewarganegaraan, Nama = (item.siswaProfil.profil.IdKewarganegaraan == 0 ? null : JenisWargaNegaraConstant.Dict[item.siswaProfil.profil.IdKewarganegaraan]) };
                        ret.Bahasa = new ComboModel { Id = item.siswaProfil.profil.IdBahasa, Nama = (item.siswaProfil.profil.IdBahasa == 0 ? null : JenisBahasaConstant.DictJenisBahasa[item.siswaProfil.profil.IdBahasa]) };
                        ret.TinggiBadan = item.siswaProfil.profil.TinggiBadan;
                        ret.BeratBadan = item.siswaProfil.profil.BeratBadan;
                        ret.LingkarKepala = item.siswaProfil.profil.LingkarKepala;
                        ret.AnakKe = item.siswaProfil.profil.AnakKe;
                        ret.JumlahSodaraKandung = item.siswaProfil.profil.JumlahSodaraKandung;
                        ret.JumlahSodaraTiri = item.siswaProfil.profil.JumlahSodaraTiri;
                        ret.JumlahSodaraAngkat = item.siswaProfil.profil.JumlahSodaraAngkat;
                        ret.Transportasi = new ComboModel { Id = item.siswaProfil.profil.IdTransportasi, Nama = (item.siswaProfil.profil.IdTransportasi == 0 ? null : JenisTransportasiConstant.DictJenisTransportasi[item.siswaProfil.profil.IdTransportasi]) };
                        ret.JarakKeSekolah = item.siswaProfil.profil.JarakKeSekolah;
                        ret.WaktuTempuh = item.siswaProfil.profil.WaktuTempuh;
                        ret.PenyakitDiderita = item.siswaProfil.profil.PenyakitDiderita;
                        ret.KelainanJasmani = item.siswaProfil.profil.KelainanJasmani;
                        ret.SekolahAsal = item.siswaProfil.profil.SekolahAsal;
                        ret.AlamatSekolahAsal = item.siswaProfil.profil.AlamatSekolahAsal;
                        ret.NspnSekolahAsal = item.siswaProfil.profil.NspnSekolahAsal;
                        ret.PernahTkFormal = item.siswaProfil.profil.PernahTkFormal;
                        ret.PernahTlInformal = item.siswaProfil.profil.PernahTkInformal;
                        ret.Hobi = new ComboModel { Id = item.siswaProfil.profil.IdHobi, Nama = (item.siswaProfil.hobi.Nama == null ? null : item.siswaProfil.hobi.Nama) };
                        ret.Cita2 = new ComboModel { Id = item.siswaProfil.profil.IdCita2, Nama = (item.siswaProfil.cita2.Nama == null ? null : item.siswaProfil.cita2.Nama) };
                        ret.NoUjian = item.siswaProfil.profil.NoUjian;
                        ret.NoIjazah = item.siswaProfil.profil.NoIjazah;
                        ret.FileIjazah = item.siswaProfil.profil.FileIjazah;
                        ret.NoShusbn = item.siswaProfil.profil.NoShusbn;
                        ret.FileShusbn = item.siswaProfil.profil.FileShusbn;

                        ret.KelasDapodik = item.siswaProfil.profil.KelasDapodik;
                        ret.BerkebutuhanKhusus = item.siswaProfil.profil.BerkebutuhanKhusus;
                        ret.AlamatKk = item.siswaProfil.profil.AlamatKk;
                        ret.PunyaWali = item.siswaProfil.profil.PunyaWali;
                        ret.TanggalLahirAyah = item.siswaProfil.profil.TanggalLahirAyah.ToString("dd-MM-yyyy");
                        ret.BerkebutuhanKhususAyah = item.siswaProfil.profil.BerkebutuhanKhususAyah;
                        ret.PendidikanTerakhirAyah = item.siswaProfil.profil.PendidikanTerakhirAyah;
                        ret.PenghasilanAyah = item.siswaProfil.profil.PenghasilanAyah;
                        ret.NomorHpAyah = item.siswaProfil.profil.NomorHpAyah;

                        ret.TanggalLahirIbu = item.siswaProfil.profil.TanggalLahirIbu.ToString("dd-MM-yyyy");
                        ret.BerkebutuhanKhususIbu = item.siswaProfil.profil.BerkebutuhanKhususIbu;
                        ret.PendidikanTerakhirIbu = item.siswaProfil.profil.PendidikanTerakhirIbu;
                        ret.PenghasilanIbu = item.siswaProfil.profil.PenghasilanIbu;
                        ret.NomorHpIbu = item.siswaProfil.profil.NomorHpIbu;

                        ret.NikWali = item.siswaProfil.profil.NikWali;
                        ret.NamaWali = item.siswaProfil.profil.NamaWali;
                        ret.AlamatWali = item.siswaProfil.profil.AlamatWali;
                        ret.IdJenisPekerjaanWali = item.siswaProfil.profil.IdJenisPekerjaanWali;
                        ret.NamaInstansiWali = item.siswaProfil.profil.NamaInstansiWali;
                        ret.EmailWali = item.siswaProfil.profil.EmailWali;
                        ret.NoHandphoneWali = item.siswaProfil.profil.NoHandphoneWali;
                        ret.TerimaKip = item.siswaProfil.profil.TerimaKip;
                        ret.AlasanKip = item.siswaProfil.profil.AlasanKip;
                    }
                    if (item.siswaKk != null)
                    {
                        ret.NoKk = item.siswaKk.kk.NoKk;
                        ret.Alamat = item.siswaKk.kk.Alamat;
                        ret.Rt = item.siswaKk.kk.Rt;
                        ret.Rw = item.siswaKk.kk.Rw;
                        ret.Dusun = item.siswaKk.kk.Dusun;
                        ret.WilProv = new ComboModel { Id = item.siswaKk.prov.IdWilProvinsi, Nama = item.siswaKk.prov.Nama, NamaSingkat = item.siswaKk.prov.Nama };
                        ret.WilKabkot = new ComboModel { Id = item.siswaKk.kab.IdWilKabkot, Nama = item.siswaKk.kab.Nama, NamaSingkat = item.siswaKk.kab.Nama };
                        ret.WilKec = new ComboModel { Id = item.siswaKk.kec.IdWilKecamatan, Nama = item.siswaKk.kec.Nama, NamaSingkat = item.siswaKk.kec.Nama };
                        ret.WilDesa = new ComboModel { Id = item.siswaKk.ds.IdWilDesa, Nama = item.siswaKk.ds.Nama, NamaSingkat = item.siswaKk.ds.Nama };
                        ret.KdPos = item.siswaKk.kk.KdPos;
                        ret.NoTelp = item.siswaKk.kk.NoTelp;
                        ret.Longitude = item.siswaKk.kk.Longitude;
                        ret.Latitude = item.siswaKk.kk.Latitude;
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswaProfile", ex);
                return null;
            }
        }
        public SheetSiswaViewProfileModel GetSheetSiswaViewProfile(int IdUser, int IdUnit, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                SheetSiswaViewProfileModel ret = new SheetSiswaViewProfileModel
                {
                    Judul = "DATA DAPODIK",
                    DownloadOleh = "",
                    Unit = "",
                    ListData = new List<SiswaViewProfileModel>()
                };

                using (var conn = commonService.DbConnection())
                {
                    var tbUnit = conn.Get<TbUnit>(IdUnit);
                    if (tbUnit == null) { oMessage = "data unit tidak ada"; return ret; }
                    ret.Judul = $"DATA DAPODIK UNIT {tbUnit.Nama}";
                    ret.Unit = tbUnit.Nama;
                    var tbUser = conn.Get<TbUser>(IdUser);
                    if (tbUser == null) { oMessage = "data user tidak ada"; return ret; }
                    ret.DownloadOleh = $"Didownload oleh {tbUser.FirstName} pada {DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")}";
                    var data = from siswa in conn.GetList<TbSiswa>()
                               join rombel in conn.GetList<TbKelasRombel>() on siswa.IdSiswa equals rombel.IdSiswa
                               join pekerjaanIbu in conn.GetList<TbJenisPekerjaan>() on siswa.IdJenisPekerjaanIbu equals pekerjaanIbu.IdJenisPekerjaan
                               join pekerjaanAyah in conn.GetList<TbJenisPekerjaan>() on siswa.IdJenisPekerjaanAyah equals pekerjaanAyah.IdJenisPekerjaan
                               join kelasParalel in conn.GetList<TbKelasParalel>() on rombel.IdKelasParalel equals kelasParalel.IdKelasParalel
                               join kelas in conn.GetList<TbKelas>() on kelasParalel.IdKelas equals kelas.IdKelas
                               join unit in conn.GetList<TbUnit>() on kelas.IdUnit equals unit.IdUnit
                               join siswaProfil in (from profil in conn.GetList<TbSiswaProfil>()
                                                    join hobi in conn.GetList<TbJenisHobi>() on profil.IdHobi equals hobi.IdJenisHobi
                                                    join cita2 in conn.GetList<TbJenisCita2>() on profil.IdCita2 equals cita2.IdJenisCita2
                                                    join kelasDapodik in conn.GetList<TbKelasParalel>() on Int32.Parse(profil.KelasDapodik == null ? "0" : profil.KelasDapodik) equals kelasDapodik.IdKelasParalel into kelasDapodik1
                                                    from kelasDapodik in kelasDapodik1.DefaultIfEmpty()
                                                    select new { profil, hobi, cita2, kelasDapodik }
                                                     ) on siswa.IdSiswa equals siswaProfil.profil.IdSiswa into siswaProfil1
                               from siswaProfil in siswaProfil1.DefaultIfEmpty()
                               where unit.IdUnit == IdUnit
                               select new { siswa, kelasParalel, pekerjaanIbu, pekerjaanAyah, siswaProfil };

                    foreach (var item in data.OrderBy(x => x.kelasParalel.IdKelasParalel))
                    {
                        SiswaViewProfileModel m = new SiswaViewProfileModel();
                        m.NoAktaLahir = "-";
                        m.FileKk = "-";
                        m.FileAktaLahir = "-";
                        m.NoKip = "-";
                        m.FileKip = "-";
                        m.GolonganDarah = new ComboModel { Kode = "-", Nama = "-" };
                        m.Kewarganegaraan = new ComboModel { Kode = "-", Nama = "-" };
                        m.Bahasa = new ComboModel { Kode = "-", Nama = "-" };
                        m.TinggiBadan = 0;
                        m.BeratBadan = 0;
                        m.LingkarKepala = 0;
                        m.AnakKe = 0;
                        m.JumlahSodaraKandung = 0;
                        m.JumlahSodaraTiri = 0;
                        m.JumlahSodaraAngkat = 0;
                        m.Transportasi = new ComboModel { Kode = "-", Nama = "-" };
                        m.JarakKeSekolah = 0;
                        m.WaktuTempuh = 0;
                        m.PenyakitDiderita = "-";
                        m.KelainanJasmani = "-";
                        m.SekolahAsal = "-";
                        m.AlamatSekolahAsal = "-";
                        m.NspnSekolahAsal = "-";
                        m.PernahTkFormal = false;
                        m.PernahTlInformal = false;
                        m.Hobi = new ComboModel { Kode = "-", Nama = "-" };
                        m.Cita2 = new ComboModel { Kode = "-", Nama = "-" };
                        m.NoUjian = "-";
                        m.NoIjazah = "-";
                        m.FileIjazah = "-";
                        m.NoShusbn = "-";
                        m.FileShusbn = "-";

                        m.IdSiswa = item.siswa.IdSiswa;
                        m.KelasParalel = item.kelasParalel.Nama;
                        m.Nis = item.siswa.Nis;
                        m.Nisn = commonService.SetStringNull(item.siswa.Nisn);
                        m.Nik = commonService.SetStringNull(item.siswa.Nik);
                        m.Nama = item.siswa.Nama;
                        m.NamaPanggilan = commonService.SetStringNull(item.siswa.NamaPanggilan);
                        m.TempatLahir = commonService.SetStringNull(item.siswa.TempatLahir);
                        m.TanggalLahir = item.siswa.TanggalLahir.ToString("dd-MM-yyyy");
                        if (item.siswa.IdAgama == 0)
                            m.Agama = new ComboModel { Id = 0, Nama = "-" };
                        else
                            m.Agama = new ComboModel { Id = item.siswa.IdAgama, Nama = AgamaConstant.Dict[item.siswa.IdAgama] };
                        if (item.siswa.KdJenisKelamin == null)
                            m.JenisKelamin = new ComboModel { Id = 0, Nama = "-" };
                        else
                            m.JenisKelamin = new ComboModel { Kode = item.siswa.KdJenisKelamin, Nama = JenisKelaminConstant.Dict[item.siswa.KdJenisKelamin] };
                        m.NoHandphone = commonService.SetStringNull(item.siswa.NoHandphone);
                        m.NoDarurat = commonService.SetStringNull(item.siswa.NoDarurat);
                        m.Email = commonService.SetStringNull(item.siswa.Email);
                        m.AlamatTinggal = commonService.SetStringNull(item.siswa.AlamatTinggal);
                        m.AlamatOrtu = commonService.SetStringNull(item.siswa.AlamatOrtu);
                        m.NikIbu = commonService.SetStringNull(item.siswa.NikIbu);
                        m.NamaIbu = commonService.SetStringNull(item.siswa.NamaIbu);
                        if (item.siswa.IdJenisPekerjaanIbu == 0)
                            m.JenisPekerjaanIbu = new ComboModel { Id = 0, Nama = "-" };
                        else
                            m.JenisPekerjaanIbu = new ComboModel { Id = item.siswa.IdJenisPekerjaanIbu, Nama = item.pekerjaanIbu.Nama };
                        m.NamaInstansiIbu = commonService.SetStringNull(item.siswa.NamaInstansiIbu);
                        m.NikAyah = commonService.SetStringNull(item.siswa.NikAyah);
                        m.NamaAyah = commonService.SetStringNull(item.siswa.NamaAyah);
                        if (item.siswa.IdJenisPekerjaanAyah == 0)
                            m.JenisPekerjaanAyah = new ComboModel { Id = 0, Nama = "-" };
                        else
                            m.JenisPekerjaanAyah = new ComboModel { Id = item.siswa.IdJenisPekerjaanAyah, Nama = item.pekerjaanAyah.Nama };
                        m.NamaInstansiAyah = item.siswa.NamaInstansiAyah;
                        m.EmailOrtu = commonService.SetStringNull(item.siswa.EmailOrtu);
                        m.FotoProfile = commonService.SetStringNull(item.siswa.FotoProfile);
                        if (item.siswaProfil != null)
                        {

                            m.NoAktaLahir = commonService.SetStringNull(item.siswaProfil.profil.NoAktaLahir);
                            m.FileKk = commonService.SetStringNull(item.siswaProfil.profil.FileKk);
                            m.FileAktaLahir = commonService.SetStringNull(item.siswaProfil.profil.FileAktaLahir);
                            m.NoKip = commonService.SetStringNull(item.siswaProfil.profil.NoKip);
                            m.FileKip = commonService.SetStringNull(item.siswaProfil.profil.FileKip);
                            if (item.siswaProfil.profil.KdGolonganDarah == null)
                                m.GolonganDarah = new ComboModel { Id = 0, Nama = "-" };
                            else
                                m.GolonganDarah = new ComboModel { Kode = item.siswaProfil.profil.KdGolonganDarah, Nama = JenisGolonganDarahConstant.Dict[item.siswaProfil.profil.KdGolonganDarah] };
                            if (item.siswaProfil.profil.IdKewarganegaraan == 0)
                                m.Kewarganegaraan = new ComboModel { Id = 0, Nama = "-" };
                            else
                                m.Kewarganegaraan = new ComboModel { Id = item.siswaProfil.profil.IdKewarganegaraan, Nama = JenisWargaNegaraConstant.Dict[item.siswaProfil.profil.IdKewarganegaraan] };
                            if (item.siswaProfil.profil.IdBahasa == 0)
                                m.Bahasa = new ComboModel { Id = 0, Nama = "-" };
                            else
                                m.Bahasa = new ComboModel { Id = item.siswaProfil.profil.IdBahasa, Nama = JenisBahasaConstant.DictJenisBahasa[item.siswaProfil.profil.IdBahasa] };
                            m.TinggiBadan = commonService.SetIntegerNUll(item.siswaProfil.profil.TinggiBadan);
                            m.BeratBadan = commonService.SetIntegerNUll(item.siswaProfil.profil.BeratBadan);
                            m.LingkarKepala = commonService.SetNumberNull(item.siswaProfil.profil.LingkarKepala);
                            m.AnakKe = commonService.SetIntegerNUll(item.siswaProfil.profil.AnakKe);
                            m.JumlahSodaraKandung = commonService.SetIntegerNUll(item.siswaProfil.profil.JumlahSodaraKandung);
                            m.JumlahSodaraTiri = commonService.SetIntegerNUll(item.siswaProfil.profil.JumlahSodaraTiri);
                            m.JumlahSodaraAngkat = commonService.SetIntegerNUll(item.siswaProfil.profil.JumlahSodaraAngkat);
                            if (item.siswaProfil.profil.IdTransportasi == 0)
                                m.Transportasi = new ComboModel { Id = 0, Nama = "-" };
                            else
                                m.Transportasi = new ComboModel { Id = item.siswaProfil.profil.IdTransportasi, Nama = JenisTransportasiConstant.DictJenisTransportasi[item.siswaProfil.profil.IdTransportasi] };
                            m.JarakKeSekolah = commonService.SetIntegerNUll(item.siswaProfil.profil.JarakKeSekolah);
                            m.WaktuTempuh = commonService.SetNumberNull(item.siswaProfil.profil.WaktuTempuh);
                            m.PenyakitDiderita = commonService.SetStringNull(item.siswaProfil.profil.PenyakitDiderita);
                            m.KelainanJasmani = commonService.SetStringNull(item.siswaProfil.profil.KelainanJasmani);
                            m.SekolahAsal = commonService.SetStringNull(item.siswaProfil.profil.SekolahAsal);
                            m.AlamatSekolahAsal = commonService.SetStringNull(item.siswaProfil.profil.AlamatSekolahAsal);
                            m.NspnSekolahAsal = commonService.SetStringNull(item.siswaProfil.profil.NspnSekolahAsal);
                            m.PernahTkFormal = item.siswaProfil.profil.PernahTkFormal;
                            m.PernahTlInformal = item.siswaProfil.profil.PernahTkInformal;
                            if (item.siswaProfil.hobi == null)
                                m.Hobi = new ComboModel { Id = 0, Nama = "-" };
                            else
                                m.Hobi = new ComboModel { Id = item.siswaProfil.profil.IdHobi, Nama = item.siswaProfil.hobi.Nama };
                            if (item.siswaProfil.cita2 == null)
                                m.Cita2 = new ComboModel { Id = 0, Nama = "-" };
                            else
                                m.Cita2 = new ComboModel { Id = item.siswaProfil.cita2.IdJenisCita2, Nama = item.siswaProfil.cita2.Nama };
                            m.NoUjian = commonService.SetStringNull(item.siswaProfil.profil.NoUjian);
                            m.NoIjazah = commonService.SetStringNull(item.siswaProfil.profil.NoIjazah);
                            m.FileIjazah = commonService.SetStringNull(item.siswaProfil.profil.FileIjazah);
                            m.NoShusbn = commonService.SetStringNull(item.siswaProfil.profil.NoShusbn);
                            m.FileShusbn = commonService.SetStringNull(item.siswaProfil.profil.FileShusbn);

                            m.KelasDapodik = item.siswaProfil.kelasDapodik?.Nama;
                            m.BerkebutuhanKhusus = item.siswaProfil.profil.BerkebutuhanKhusus;
                            m.AlamatKk = item.siswaProfil.profil.AlamatKk;
                            m.PunyaWali = item.siswaProfil.profil.PunyaWali;
                            m.TanggalLahirAyah = item.siswaProfil.profil.TanggalLahirAyah.ToString("dd-MM-yyyy");
                            m.BerkebutuhanKhususAyah = item.siswaProfil.profil.BerkebutuhanKhususAyah;
                            m.PendidikanTerakhirAyah = item.siswaProfil.profil.PendidikanTerakhirAyah;
                            m.PenghasilanAyah = item.siswaProfil.profil.PenghasilanAyah;
                            m.NomorHpAyah = item.siswaProfil.profil.NomorHpAyah;

                            m.TanggalLahirIbu = item.siswaProfil.profil.TanggalLahirIbu.ToString("dd-MM-yyyy");
                            m.BerkebutuhanKhususIbu = item.siswaProfil.profil.BerkebutuhanKhususIbu;
                            m.PendidikanTerakhirIbu = item.siswaProfil.profil.PendidikanTerakhirIbu;
                            m.PenghasilanIbu = item.siswaProfil.profil.PenghasilanIbu;
                            m.NomorHpIbu = item.siswaProfil.profil.NomorHpIbu;

                            m.NikWali = item.siswaProfil.profil.NikWali;
                            m.NamaWali = item.siswaProfil.profil.NamaWali;
                            m.AlamatWali = item.siswaProfil.profil.AlamatWali;
                            m.IdJenisPekerjaanWali = item.siswaProfil.profil.IdJenisPekerjaanWali;
                            m.NamaInstansiWali = item.siswaProfil.profil.NamaInstansiWali;
                            m.EmailWali = item.siswaProfil.profil.EmailWali;
                            m.NoHandphoneWali = item.siswaProfil.profil.NoHandphoneWali;
                        }
                        ret.ListData.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswaProfile", ex);
                return null;

            }
        }
        public SiswaModel GetSiswa(int IdSiswa, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    var tbSiswa = (from a in conn.GetList<TbSiswa>()
                                   where a.IdSiswa == IdSiswa
                                   select a).FirstOrDefault();
                    if (tbSiswa == null)
                    {
                        oMessage = "Siswa tidak terdaftar";
                        return null;
                    }
                    if (tbSiswa.Status != StatusDataConstant.Aktif)
                    {
                        oMessage = "siswa sudah tidak aktif";
                        return null;
                    }

                    var ret = schService.GetSiswa(conn, tbSiswa.IdSiswa, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;

                    ret.KelasQuran = schService.GetKelasQuranSiswa(conn, tbSiswa.IdSiswa, string.Empty, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;

                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswa", ex);
                return null;
            }
        }

        public SiswaModel GetSiswaByNis(string Nis, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    var tbSiswa = (from a in conn.GetList<TbSiswa>()
                                   where a.Nis == Nis
                                   select a).FirstOrDefault();
                    if (tbSiswa == null)
                    {
                        oMessage = "Siswa tidak terdaftar";
                        return null;
                    }
                    if (tbSiswa.Status != StatusDataConstant.Aktif)
                    {
                        oMessage = "siswa sudah tidak aktif";
                        return null;
                    }

                    var ret = schService.GetSiswa(conn, tbSiswa.IdSiswa, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;

                    ret.KelasQuran = schService.GetKelasQuranSiswa(conn, tbSiswa.IdSiswa, string.Empty, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;

                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswaByNis", ex);
                return null;
            }
        }

        private string PublishRapor(int IdUser, int IdSiswa, int IdJenisRapor, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                if (IdSiswa < 1)
                {
                    oMessage = "Id Siswa tidak boleh kosong";
                    return null;
                }
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbSiswaRapor = (from a in conn.GetList<TbSiswaRapor>()
                                            where a.IdSiswa == IdSiswa
                                            && a.IdJenisRapor == IdJenisRapor
                                            select a).FirstOrDefault();
                        if (tbSiswaRapor == null) return "data rapor siswa tidak ada";
                        tbSiswaRapor.IdJenisRapor = IdJenisRapor;
                        tbSiswaRapor.Status = StatusRapor.Aktif;
                        conn.Update(tbSiswaRapor);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "PublishRapor", ex);
                return null;
            }
        }

        public string UploadRapor(int IdUser, IFormFile FileRapor, string PathUpload, int TahunAjaran, out string LogMessage)
        {
            LogMessage = string.Empty;
            try
            {
                string fileUpload = FileRapor.FileName.ToString();
                var filePath = Path.GetTempFileName();



                string resFileRapor = Path.Combine(AppSetting.PathSiswaRapor, FileRapor.FileName);
                if (File.Exists(resFileRapor))
                    File.Delete(resFileRapor);


                using (var fileStream = new FileStream(resFileRapor, FileMode.Create))
                {
                    FileRapor.CopyTo(fileStream);
                }

                using (ZipFile zip = ZipFile.Read(resFileRapor))
                {
                    foreach (ZipEntry zipEntry in zip)
                    {
                        if (zipEntry.IsDirectory) continue;

                        string zipDirectoryPath = Path.GetDirectoryName(zipEntry.FileName);
                        string targetExtractFile = Path.Combine(AppSetting.PathSiswaRapor, Path.GetFileName(zipEntry.FileName));
                        string _getFileName = Path.GetFileNameWithoutExtension(zipEntry.FileName);
                        string _getFileNameWithExt = Path.GetFileName(zipEntry.FileName);
                        string[] arrFilename = _getFileName.Split("_");
                        int FileTahunUpload = Int32.Parse(arrFilename[0]);
                        int JeniRaporUpload = Int32.Parse(arrFilename[1]);
                        string NisnBaru = arrFilename[2];

                        using (var conn = commonService.DbConnection())
                        {
                            var JenisRapor = conn.Get<TbJenisRapor>(JeniRaporUpload);
                            if (JenisRapor == null)
                            {
                                LogMessage += string.Format("jenis rapor {0} tidak terdaftar", zipEntry.FileName);
                                LogMessage += Environment.NewLine;
                                continue;
                            }

                            if (TahunAjaran == FileTahunUpload)  // check for exist active year
                            {
                                var CheckSiswa = (from a in conn.GetList<TbSiswa>()
                                                  where a.Nis == NisnBaru
                                                  select a).FirstOrDefault();
                                if (CheckSiswa == null)
                                {
                                    LogMessage += string.Format("nis siswa {0} tidak terdaftar", zipEntry.FileName);
                                    LogMessage += Environment.NewLine;
                                    continue;
                                }

                                var CheckRaporSiswa = (from a in conn.GetList<TbSiswaRapor>()
                                                       where a.IdSiswa == CheckSiswa.IdSiswa
                                                       && a.IdJenisRapor == JeniRaporUpload
                                                       select a).FirstOrDefault();
                                if (CheckRaporSiswa == null)
                                {
                                    TbSiswaRapor rapor = new TbSiswaRapor();
                                    rapor.IdSiswa = CheckSiswa.IdSiswa;
                                    rapor.IdJenisRapor = JeniRaporUpload;
                                    rapor.FileRapor = _getFileNameWithExt;
                                    rapor.CreatedBy = IdUser;
                                    rapor.Status = StatusRapor.NonAktif;
                                    conn.Insert(rapor);
                                    // create the directory path if folder not exist
                                    string targetExtractDir = Path.GetDirectoryName(targetExtractFile);
                                    if (!Directory.Exists(targetExtractDir)) Directory.CreateDirectory(targetExtractDir);
                                    //zipEntry.Extract();
                                    // extract the zip file to our target file name
                                    using (FileStream stream = new FileStream(targetExtractFile, FileMode.Create))
                                        zipEntry.Extract(stream);
                                }
                                else
                                {
                                    conn.Delete(CheckRaporSiswa);

                                    TbSiswaRapor rapor = new TbSiswaRapor();
                                    rapor.IdSiswa = CheckSiswa.IdSiswa;
                                    rapor.IdJenisRapor = JeniRaporUpload;
                                    rapor.FileRapor = _getFileNameWithExt;
                                    rapor.CreatedBy = IdUser;
                                    rapor.Status = StatusRapor.NonAktif;
                                    conn.Insert(rapor);
                                    // create the directory path if folder not exist
                                    string targetExtractDir = Path.GetDirectoryName(targetExtractFile);
                                    if (!Directory.Exists(targetExtractDir)) Directory.CreateDirectory(targetExtractDir);
                                    //zipEntry.Extract();
                                    // extract the zip file to our target file name
                                    using (FileStream stream = new FileStream(targetExtractFile, FileMode.Create))
                                        zipEntry.Extract(stream);
                                }

                            }
                            else
                            {
                                var TaRaporSiswa = (from a in conn.GetList<TaSiswaRapor>()
                                                    where a.IdTahunAjaran == FileTahunUpload
                                                    & a.Nis == NisnBaru
                                                    & a.JenisRapor == JenisRapor.Nama
                                                    select a).FirstOrDefault();
                                string targetExtractDir = Path.GetDirectoryName(targetExtractFile);
                                if (TaRaporSiswa == null)
                                {
                                    // initialization objek ta rapor siswa
                                    TaSiswaRaporModel TaRapor = new TaSiswaRaporModel();
                                    string sql = "INSERT INTO his.ta_siswa_rapor (id_tahun_ajaran,nis,jenis_rapor,file_rapor) Values (@IdTahunAjaran,@Nis,@JenisRapor,@FileRapor)";
                                    TaRapor.IdTahunAjaran = FileTahunUpload;
                                    TaRapor.Nis = NisnBaru;
                                    TaRapor.JenisRapor = JenisRapor.Nama;
                                    TaRapor.FileRapor = _getFileNameWithExt;
                                    var insertTaSiswaRapor = conn.Execute(sql, TaRapor);

                                    // create the directory path if folder not exist
                                    //zipEntry.Extract();
                                    // extract the zip file to our target file name
                                    if (File.Exists(targetExtractDir))
                                        File.Delete(targetExtractDir);
                                    using (FileStream stream = new FileStream(targetExtractFile, FileMode.Create))
                                        zipEntry.Extract(stream);
                                }
                                else
                                {
                                    conn.Delete(TaRaporSiswa);
                                    TaSiswaRapor TaRapor = new TaSiswaRapor();
                                    TaRapor.IdTahunAjaran = FileTahunUpload;
                                    TaRapor.Nis = NisnBaru;
                                    TaRapor.JenisRapor = JenisRapor.Nama;
                                    TaRapor.FileRapor = _getFileNameWithExt;
                                    conn.Insert(TaRapor);

                                    // create the directory path if folder not exist

                                    if (!Directory.Exists(targetExtractDir)) Directory.CreateDirectory(targetExtractDir);
                                    if (File.Exists(targetExtractDir))
                                        File.Delete(targetExtractDir);
                                    //zipEntry.Extract();
                                    // extract the zip file to our target file name
                                    using (FileStream stream = new FileStream(targetExtractFile, FileMode.Create))
                                        zipEntry.Extract(stream);
                                }
                            }


                        }




                    }
                }
                if (File.Exists(resFileRapor))
                    File.Delete(resFileRapor);
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UploadRapor", ex);
            }
        }

        public string PublishRapors(int IdUser, int IdJenisRapor, List<int> IdSiswa, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                if (IdSiswa.Count() < 1)
                {
                    oMessage = "Id Siswa tidak boleh kosong";
                    return null;
                }

                foreach (var item in IdSiswa)
                {
                    this.PublishRapor(IdUser, item, IdJenisRapor, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage))
                        return "";
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PublishRapor", ex);
            }
        }
        public List<SiswaRaporModel> GetApprovSiswaRapors(int IdUser, int IdKelas, int IdJenisRapor, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    var tbSiswaRapors = (from a in conn.GetList<TbSiswaRapor>()
                                         join b in conn.GetList<TbSiswa>() on a.IdSiswa equals b.IdSiswa
                                         join c in conn.GetList<TbKelasRombel>() on a.IdSiswa equals c.IdSiswa
                                         join d in conn.GetList<TbKelasParalel>() on c.IdKelasParalel equals d.IdKelasParalel
                                         join f in conn.GetList<TbKelas>() on d.IdKelas equals f.IdKelas
                                         join g in conn.GetList<TbJenisRapor>() on a.IdJenisRapor equals g.IdJenisRapor
                                         join h in conn.GetList<TbUser>() on a.CreatedBy equals h.IdUser
                                         where f.IdKelas == IdKelas && a.Status == StatusRapor.NonAktif
                                         && a.IdJenisRapor == IdJenisRapor
                                         select new { a, b, g, h }).ToList();
                    if (tbSiswaRapors.Count() < 1)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    List<SiswaRaporModel> ret = new List<SiswaRaporModel>();
                    foreach (var item in tbSiswaRapors.OrderBy(x => x.b.Nama))
                    {
                        ret.Add(new SiswaRaporModel
                        {
                            IdSiswa = item.a.IdSiswa,
                            IdJenisRapor = item.a.IdJenisRapor,
                            JenisRapor = item.g.Nama,
                            NamaSiswa = item.b.Nama,
                            FileRapor = item.a.FileRapor,
                            CreatedBy = userAppService.SetFullName(item.h.FirstName, item.h.MiddleName, item.h.LastName),
                            Status = StatusRapor.DictStatusRapor[item.a.Status]
                        });
                    }

                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswaRapors", ex);
                return null;
            }
        }

        public List<SiswaRaporModel> GetApprovSiswaRapors(int IdUser, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    var user = (from a in conn.GetList<TbUnitPegawai>()
                                join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                where a.IdPegawai == IdUser
                                select new { a, b, c }).FirstOrDefault();
                    if (user == null) { oMessage = "pengguna tidak terdaftar sebagai pegawai"; return null; }

                    bool isAdmin = false;
                    if (user.a.IdUnit == UnitConstant.PER) isAdmin = true;

                    conn.Open();
                    var tbSiswaRapors = (from a in conn.GetList<TbSiswaRapor>()
                                         join b in conn.GetList<TbSiswa>() on a.IdSiswa equals b.IdSiswa
                                         join c in conn.GetList<TbKelasRombel>() on a.IdSiswa equals c.IdSiswa
                                         join d in conn.GetList<TbKelasParalel>() on c.IdKelasParalel equals d.IdKelasParalel
                                         join f in conn.GetList<TbKelas>() on d.IdKelas equals f.IdKelas
                                         join g in conn.GetList<TbJenisRapor>() on a.IdJenisRapor equals g.IdJenisRapor
                                         join h in conn.GetList<TbUser>() on a.CreatedBy equals h.IdUser
                                         where f.IdUnit == (isAdmin ? f.IdUnit : user.a.IdUnit)
                                         select new { a, b, g, h }).ToList();
                    if (tbSiswaRapors.Count() < 1)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    List<SiswaRaporModel> ret = new List<SiswaRaporModel>();
                    foreach (var item in tbSiswaRapors.OrderBy(x => x.b.Nama))
                    {
                        ret.Add(new SiswaRaporModel
                        {
                            IdSiswa = item.a.IdSiswa,
                            IdJenisRapor = item.a.IdJenisRapor,
                            JenisRapor = item.g.Nama,
                            NamaSiswa = item.b.Nama,
                            FileRapor = item.a.FileRapor,
                            CreatedBy = userAppService.SetFullName(item.h.FirstName, item.h.MiddleName, item.h.LastName),
                            Status = StatusRapor.DictStatusRapor[item.a.Status]
                        });
                    }

                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswaRapors", ex);
                return null;
            }
        }

        public List<JenisRaporModel> GetJenisRapors(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    var data = (from a in conn.GetList<TbJenisRapor>()
                                select a).ToList();
                    if (data.Count() < 1)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    List<JenisRaporModel> ret = new List<JenisRaporModel>();
                    foreach (var item in data)
                    {
                        ret.Add(new JenisRaporModel
                        {
                            IdJenisRapor = item.IdJenisRapor,
                            Nama = item.Nama,
                            NamaSingkat = item.NamaSingkat,
                        });
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisRapors", ex);
                return null;
            }
        }

        public string UpdateTokenFcmSiswa(int IdUser, int TipePlatform, string Token, string UserAgent, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbSiswa = (from a in conn.GetList<TbSiswa>()
                                       where a.IdSiswa == IdUser
                                       select a).FirstOrDefault();

                        if (tbSiswa == null)
                        {
                            oMessage = "data tidak ada";
                            return null;
                        }
                        var tbSiswaNotification = (from a in conn.GetList<TbSiswaNotification>()
                                                   where a.IdSiswa == IdUser & a.TokenFcm == Token
                                                   select a).FirstOrDefault();
                        if (tbSiswaNotification == null)
                        {
                            tbSiswaNotification = new TbSiswaNotification
                            {
                                IdSiswa = IdUser,
                                TokenFcm = Token,
                                Tipe = TipePlatform,
                                UserAgent = UserAgent
                            };
                            conn.Insert(tbSiswaNotification);
                            tx.Commit();
                        }
                        return null;
                    }
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "UpdateTokenFcmSiswa", ex);
                return null;
            }
        }

        public List<SiswaInboxModel> GetInboxSiswas(int IdUser, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    var inboxs = (from a in conn.GetList<TbSiswaInbox>()
                                  where a.IdSiswa == IdUser
                                  select a).ToList();

                    if (inboxs.Count == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }

                    List<SiswaInboxModel> ret = new List<SiswaInboxModel>();
                    foreach (var inbox in inboxs.OrderByDescending(x => x.IdSiswaInbox))
                    {
                        ret.Add(new SiswaInboxModel
                        {
                            IdSiswaInbox = inbox.IdSiswaInbox,
                            IdSiswa = inbox.IdSiswa,
                            Tipe = inbox.Tipe,
                            StrTipe = TipeInboxConstant.DictTipeInbox[inbox.Tipe],
                            Judul = inbox.Judul,
                            Deskripsi = inbox.Deskripsi,
                            IsRead = inbox.IsRead,
                            CreatedBy = inbox.CreatedBy,
                            CreatedDate = inbox.CreatedDate.ToString("dd MMMM"),
                            UpdatedDate = inbox.UpdatedDate.ToString("dd-MM-yyyy")
                        });
                    }
                    return ret.Take(30).ToList();
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetInboxSiswas", ex);
                return null;
            }
        }

        public SiswaInboxModel GetInboxSiswa(int IdSiswaInbox, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    var inbox = (from a in conn.GetList<TbSiswaInbox>()
                                 where a.IdSiswaInbox == IdSiswaInbox
                                 select a).FirstOrDefault();

                    if (inbox == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }

                    SiswaInboxModel ret = new SiswaInboxModel
                    {
                        IdSiswaInbox = inbox.IdSiswaInbox,
                        IdSiswa = inbox.IdSiswa,
                        Tipe = inbox.Tipe,
                        StrTipe = TipeInboxConstant.DictTipeInbox[inbox.Tipe],
                        Judul = inbox.Judul,
                        Deskripsi = inbox.Deskripsi,
                        IsRead = inbox.IsRead,
                        CreatedBy = inbox.CreatedBy,
                        CreatedDate = inbox.CreatedDate.ToString("dd-MM-yyyy"),
                        UpdatedDate = inbox.UpdatedDate.ToString("dd-MM-yyyy")
                    };

                    return ret;

                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetInboxSiswa", ex);
                return null;
            }
        }

        public string ReadInboxSiswa(int IdSiswaInbox)
        {
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    var tbSiswaInbox = (from a in conn.GetList<TbSiswaInbox>()
                                        where a.IdSiswaInbox == IdSiswaInbox
                                        select a).FirstOrDefault();

                    if (tbSiswaInbox == null)
                    {
                        return "data tidak ada";
                    }
                    tbSiswaInbox.IsRead = true;
                    tbSiswaInbox.UpdatedDate = dateTimeService.GetCurrdate();
                    conn.Update(tbSiswaInbox);


                    return null;
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "ReadInboxSiswa", ex);
            }
        }
        public string PushInboxNotif(int IdSiswa, string Judul, string Deskripsi, int TipeInbox, int? CreatedBy)
        {
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    TbSiswaInbox tbSiswaInbox = new TbSiswaInbox
                    {
                        IdSiswa = IdSiswa,
                        Tipe = TipeInbox,
                        Judul = Judul,
                        Deskripsi = Deskripsi,
                        IsRead = false,
                        CreatedBy = CreatedBy == null ? 1 : (int)CreatedBy,
                        CreatedDate = dateTimeService.GetCurrdate(),
                    };

                    conn.Insert(tbSiswaInbox);

                    var tbSiswaNotif = (from a in conn.GetList<TbSiswaNotification>()
                                        where a.IdSiswa == IdSiswa
                                        select a).ToList();
                    if (tbSiswaNotif.Count != 0)
                    {
                        foreach (var item in tbSiswaNotif)
                        {
                            if (item.Tipe == TipePlatformConstant.Mobile)
                                commonService.PushNotifMobile(
                                    item.TokenFcm,
                                    Judul,
                                    Deskripsi
                                );
                        }
                    }

                    return null;
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PushInboxNotif", ex);
            }
        }


        public List<LaporanRiwayatTransaksiBankModel> GetLaporanRiwayatTransaksiBank(string TanggalAwal, string TanggalAkhir, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    var historys = (from a in conn.GetList<TbPartnerHistoryPayment>()
                                    where a.CreatedDate.Date >= dateTimeService.StrToDateTime(TanggalAwal).Date
                                   & a.CreatedDate.Date <= dateTimeService.StrToDateTime(TanggalAkhir).Date
                                    select a).ToList();
                    if (historys.Count == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    List<LaporanRiwayatTransaksiBankModel> ret = new List<LaporanRiwayatTransaksiBankModel>();
                    foreach (var item in historys.OrderByDescending(x => x.IdPartnerHistoryPayment))
                    {
                        ret.Add(new LaporanRiwayatTransaksiBankModel
                        {
                            KodeBayar = item.KodeBayar,
                            Tipe = item.Tipe,
                            Nama = item.Nama,
                            Kelas = item.Kelas,
                            JumlahBayar = item.JumlahBayar,
                            Terbilang = commonService.Terbilang((decimal)item.JumlahBayar),
                            Catatan = item.Catatan,
                            KeteranganGagal = item.KeteranganGagal,
                            Status = StatusLunasConstant.Dict[item.Status],
                            Tanggal = item.CreatedDate.ToString("dd-MM-yyyy HH:mm:ss")
                        });
                    }
                    return ret;

                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetLaporanRiwayatTransaksiBank", ex);
                return null;
            }
        }
        public ResumeListSiswaTagihanModel GetResumeSiswaTagihan(string TanggalAwal, string TanggalAkhir, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    var tagihanArsipDetils = (from _tagihan in conn.GetList<TaSiswaTagihan>()
                                              join _tagihanDetil in conn.GetList<TaTagihanSiswaDetil>() on _tagihan.IdSiswa equals _tagihanDetil.IdSiswa
                                              where _tagihan.Periode.Date >= dateTimeService.StrToDateTime(TanggalAwal).Date
                                             & _tagihan.Periode.Date <= dateTimeService.StrToDateTime(TanggalAkhir).Date
                                        & _tagihan.Status == StatusLunasConstant.Lunas
                                              select _tagihanDetil).ToList();

                    var tagihanDetils = (from _tagihan in conn.GetList<TbSiswaTagihan>()
                                         join _tagihanDetil in conn.GetList<TbSiswaTagihanDetil>() on _tagihan.IdSiswa equals _tagihanDetil.IdSiswa
                                         where _tagihan.Periode.Date >= dateTimeService.StrToDateTime(TanggalAwal).Date
                                        & _tagihan.Periode.Date <= dateTimeService.StrToDateTime(TanggalAkhir).Date
                                        & _tagihan.Status == StatusLunasConstant.Lunas
                                         select _tagihanDetil).ToList();

                    // if (tagihanArsipDetils.Count == 0)
                    // {
                    //     oMessage = "data tidak ada";
                    //     return null;
                    // }

                    ResumeListSiswaTagihanModel ret = new ResumeListSiswaTagihanModel
                    {
                        TotSpp2 = 0,
                        TotBoks2 = 0,
                        TotJemputan2 = 0,
                        TotKatering2 = 0,
                        TotKomite2 = 0,
                        TotPpdb2 = 0,
                        TotAll2 = 0,
                        TotSpp1 = 0,
                        TotBoks1 = 0,
                        TotJemputan1 = 0,
                        TotKatering1 = 0,
                        TotKomite1 = 0,
                        TotPpdb1 = 0,
                        TotAll1 = 0,
                        TotalAll12 = 0,
                        // SiswaTagihans = new List<ResumeSiswaTagihanModel>()
                    };

                    foreach (var item in tagihanArsipDetils)
                    {
                        ret.TotalAll12 += item.Rp;
                        ResumeSiswaTagihanModel mod = new ResumeSiswaTagihanModel();
                        switch (item.Tipe)
                        {
                            case -1:
                                switch (item.IdJenisTagihanSiswa)
                                {
                                    case 1:
                                        ret.TotSpp2 += item.Rp;
                                        // mod.Spp2 = item.Rp;
                                        break;
                                    case 2:
                                        ret.TotBoks2 += item.Rp;
                                        // mod.Boks2 = item.Rp;
                                        break;
                                    case 3:
                                        ret.TotPpdb2 += item.Rp;
                                        // mod.Ppdb2 = item.Rp;
                                        break;
                                    case 4:
                                        ret.TotJemputan2 += item.Rp;
                                        // mod.Jemputan2 = item.Rp;
                                        break;
                                    case 5:
                                        ret.TotKatering2 += item.Rp;
                                        // mod.Katering2 = item.Rp;
                                        break;
                                    case 6:
                                        ret.TotKomite2 += item.Rp;
                                        // mod.Komite2 = item.Rp;
                                        break;
                                    default:
                                        break;
                                }
                                ret.TotAll2 += item.Rp;

                                break;
                            case 1:
                                switch (item.IdJenisTagihanSiswa)
                                {
                                    case 1:
                                        ret.TotSpp1 += item.Rp;
                                        // mod.Spp1 = item.Rp;
                                        break;
                                    case 2:
                                        ret.TotBoks1 += item.Rp;
                                        // mod.Boks1 = item.Rp;
                                        break;
                                    case 3:
                                        ret.TotPpdb1 += item.Rp;
                                        // mod.Ppdb1 = item.Rp;
                                        break;
                                    case 4:
                                        ret.TotJemputan1 += item.Rp;
                                        // mod.Jemputan1 = item.Rp;
                                        break;
                                    case 5:
                                        ret.TotKatering1 += item.Rp;
                                        // mod.Katering1 = item.Rp;
                                        break;
                                    case 6:
                                        ret.TotKomite1 += item.Rp;
                                        // mod.Komite1 = item.Rp;
                                        break;
                                    default:
                                        break;
                                }
                                ret.TotAll1 += item.Rp;
                                break;
                            default:
                                break;
                        }
                        // ret.SiswaTagihans.Add(mod);
                    }

                    DateTime periodeBefore = dateTimeService.StrToDateTime(TanggalAkhir).AddMonths(-1);
                    foreach (var itemx in tagihanDetils)
                    {
                        ret.TotalAll12 += itemx.Rp;

                        if (itemx.Periode.Date == periodeBefore.Date)
                        {
                            switch (itemx.IdJenisTagihanSiswa)
                            {
                                case 1:
                                    ret.TotSpp2 += itemx.Rp;
                                    // mod.Spp2 = itemx.Rp;
                                    break;
                                case 2:
                                    ret.TotBoks2 += itemx.Rp;
                                    // mod.Boks2 = itemx.Rp;
                                    break;
                                case 3:
                                    ret.TotPpdb2 += itemx.Rp;
                                    // mod.Ppdb2 = itemx.Rp;
                                    break;
                                case 4:
                                    ret.TotJemputan2 += itemx.Rp;
                                    // mod.Jemputan2 = itemx.Rp;
                                    break;
                                case 5:
                                    ret.TotKatering2 += itemx.Rp;
                                    // mod.Katering2 = itemx.Rp;
                                    break;
                                case 6:
                                    ret.TotKomite2 += itemx.Rp;
                                    // mod.Komite2 = itemx.Rp;
                                    break;
                                default:
                                    break;
                            }
                            ret.TotAll2 += itemx.Rp;
                        }
                        else
                        {
                            switch (itemx.IdJenisTagihanSiswa)
                            {
                                case 1:
                                    ret.TotSpp1 += itemx.Rp;
                                    // mod.Spp1 = itemx.Rp;
                                    break;
                                case 2:
                                    ret.TotBoks1 += itemx.Rp;
                                    // mod.Boks1 = itemx.Rp;
                                    break;
                                case 3:
                                    ret.TotPpdb1 += itemx.Rp;
                                    // mod.Ppdb1 = itemx.Rp;
                                    break;
                                case 4:
                                    ret.TotJemputan1 += itemx.Rp;
                                    // mod.Jemputan1 = itemx.Rp;
                                    break;
                                case 5:
                                    ret.TotKatering1 += itemx.Rp;
                                    // mod.Katering1 = itemx.Rp;
                                    break;
                                case 6:
                                    ret.TotKomite1 += itemx.Rp;
                                    // mod.Komite1 = itemx.Rp;
                                    break;
                                default:
                                    break;
                            }
                            ret.TotAll1 += itemx.Rp;
                        }

                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetResumeSiswaTagihan", ex);
                return null;
            }
        }

        public bool IsEditSiswaProfil(int IdSiswa, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    var tbSiswaProfil = conn.GetList<TbSiswaProfil>().Where(x => x.IdSiswa == IdSiswa).FirstOrDefault();
                    if (tbSiswaProfil == null)
                    {
                        oMessage = "data tidak ada";
                        return false;
                    }
                    return true;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "IsEditSiswaProfil", ex);
                return false;
            }
        }
    }
}
