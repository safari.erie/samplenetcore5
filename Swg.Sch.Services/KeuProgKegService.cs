﻿using Dapper;
using Swg.Entities.Kbm;
using Swg.Entities.Keu;
using Swg.Entities.Lmg;
using Swg.Entities.Public;
using Swg.Entities.Ref;
using Swg.Models;
using Swg.Models.Constants;
using Swg.Sch.Shared.Constants;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Swg.Sch.Services
{
    public class KeuProgKegService : IKeuProgKegService
    {
        private readonly string ServiceName = "Swg.Sch.Services.KeuProgKegService.";
        private readonly string _languageCode = "id";
        KeuHakAksesModel hakAkses = new KeuHakAksesModel
        {
            CanAdd = false,
            CanApprove = false,
            CanDelete = false,
            CanDownload = false,
            CanEdit = false,
            CanRead = false,
            CanRelease = false
        };
        private int[] HakAksesProgKeg = new int[] { 21, 22, 23, 24, 25, 26 };
        private readonly ICommonService commonService;
        private readonly IDateTimeService dateTimeService;

        public KeuProgKegService(
            ICommonService CommonService,
            IDateTimeService DateTimeService)
        {
            commonService = CommonService;
            dateTimeService = DateTimeService;
        }
        private void CekAkses(IDbConnection conn, int IdUser)
        {
            var tbAksesRkas = from a in conn.GetList<TbJenisHakAksesKeu>()
                              join b in conn.GetList<TbJenisJabatanHakAksesKeu>() on a.IdJenisHakAksesKeu equals b.IdJenisHakAksesKeu
                              join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                              join d in conn.GetList<TbPegawai>() on c.IdJenisJabatan equals d.IdJenisJabatan
                              where d.IdPegawai == IdUser
                              & HakAksesProgKeg.Contains(a.IdJenisHakAksesKeu)
                              select new { a, b, c };
            foreach (var item in tbAksesRkas)
            {
                if (item.a.NamaSingkat == "CanRead")
                {
                    hakAkses.CanRead = true;
                    hakAkses.CanDownload = hakAkses.CanRead;
                }
                if (item.a.NamaSingkat == "CanAdd")
                {
                    hakAkses.CanAdd = true;
                }
                if (item.a.NamaSingkat == "CanEdit")
                {
                    hakAkses.CanEdit = true;
                }
                if (item.a.NamaSingkat == "CanApprove")
                {
                    hakAkses.CanApprove = true;
                }
                if (item.a.NamaSingkat == "CanDelete")
                {
                    hakAkses.CanDelete = true;
                }
                if (item.a.NamaSingkat == "CanRelease")
                {
                    hakAkses.CanRelease = true;
                }
            }
        }
        private int GetNextStatus(int Status)
        {
            int nextStatus = Status;
            //public static int Initial;
            //public static int Baru;
            //public static int DisetujuiKaUnit;
            //public static int DisetujuiManUnit;
            //public static int DisetujuiManKeu;
            //public static int DisetujuiDirektur;
            //public static int Disahkan;
            if (Status == JenisStatusProgKegConstant.Initial)
            {
                nextStatus = JenisStatusProgKegConstant.Baru;
            }
            else if (Status == JenisStatusProgKegConstant.Baru)
            {
                nextStatus = JenisStatusProgKegConstant.DisetujuiKaUnit;
            }
            else if (Status == JenisStatusProgKegConstant.DisetujuiKaUnit)
            {
                nextStatus = JenisStatusProgKegConstant.DisetujuiManUnit;
            }
            else if (Status == JenisStatusProgKegConstant.DisetujuiManUnit)
            {
                nextStatus = JenisStatusProgKegConstant.DisetujuiManKeu;
            }
            else if (Status == JenisStatusProgKegConstant.DisetujuiManKeu)
            {
                nextStatus = JenisStatusProgKegConstant.DisetujuiDirektur;
            }
            else if (Status == JenisStatusProgKegConstant.DisetujuiDirektur)
            {
                nextStatus = JenisStatusProgKegConstant.Disahkan;
            }
            return nextStatus;
        }
        private int GetBeforeStatus(int Status)
        {
            int beforeStatus = Status;
            if (Status == JenisStatusProgKegConstant.Disahkan)
            {
                beforeStatus = JenisStatusProgKegConstant.DisetujuiDirektur;
            }
            else if (Status == JenisStatusProgKegConstant.DisetujuiDirektur)
            {
                beforeStatus = JenisStatusProgKegConstant.DisetujuiManKeu;
            }
            else if (Status == JenisStatusProgKegConstant.DisetujuiManKeu)
            {
                beforeStatus = JenisStatusProgKegConstant.DisetujuiManUnit;
            }
            else if (Status == JenisStatusProgKegConstant.DisetujuiManUnit)
            {
                beforeStatus = JenisStatusProgKegConstant.DisetujuiKaUnit;
            }
            else if (Status == JenisStatusProgKegConstant.DisetujuiKaUnit)
            {
                beforeStatus = JenisStatusProgKegConstant.Baru;
            }
            else if (Status == JenisStatusProgKegConstant.Baru)
            {
                beforeStatus = JenisStatusProgKegConstant.Initial;
            }

            return beforeStatus;
        }
        private string ApproveData(int IdUser, int IdUnitProkeg, int StatusProses, string Catatan, int Status)
        {
            try
            {

                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        CekAkses(conn, IdUser);
                        var tbPegawaiUnit = (from a in conn.GetList<TbUnitPegawai>()
                                             join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                             join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                             where a.IdPegawai == IdUser
                                             select new { a, b, c }).FirstOrDefault();
                        if (tbPegawaiUnit == null)
                            return "Pengguna bukan pegawai";
                        if (hakAkses.CanApprove)
                            if (tbPegawaiUnit.b.IdJenisJabatan != JenisJabatanPegawaiContant.AdmDir)
                            {
                                if (Status == JenisStatusProgKegConstant.DisetujuiKaUnit)
                                    if (tbPegawaiUnit.b.IdJenisJabatan != JenisJabatanPegawaiContant.KaUnit)
                                        return "tidak berhak approve data";
                                if (Status == JenisStatusProgKegConstant.DisetujuiManUnit)
                                {
                                    int[] manUnits = new int[] {
                                        JenisJabatanPegawaiContant.ManDik,
                                        JenisJabatanPegawaiContant.ManHum,
                                        JenisJabatanPegawaiContant.ManKeu,
                                        JenisJabatanPegawaiContant.ManSos,
                                        JenisJabatanPegawaiContant.ManSos };
                                    if(!manUnits.Contains(tbPegawaiUnit.b.IdJenisJabatan))
                                        return "tidak berhak approve data";
                                }
                                if (Status == JenisStatusProgKegConstant.DisetujuiManKeu)
                                    if (tbPegawaiUnit.b.IdJenisJabatan != JenisJabatanPegawaiContant.ManKeu)
                                        return "tidak berhak approve data";
                                if (Status == JenisStatusProgKegConstant.DisetujuiDirektur)
                                    if (tbPegawaiUnit.b.IdJenisJabatan != JenisJabatanPegawaiContant.Dir)
                                        return "tidak berhak approve data";
                            }
                        var tbUnitProkeg = conn.Get<TbUnitProkeg>(IdUnitProkeg);
                        if (tbUnitProkeg == null) return "data tidak ada";
                        if (StatusProses == StatusProsesKeuanganConstant.Rejected)
                        {
                            if (string.IsNullOrEmpty(Catatan)) return "catatan harus diisi";
                            int beforeStatus = GetBeforeStatus(Status);
                            var tbUnitProKegBeforeStatus = (from a in conn.GetList<TbUnitProkegStatus>()
                                                            where a.IdUnitProkeg == IdUnitProkeg
                                                            & a.Status == beforeStatus
                                                            select a).FirstOrDefault();
                            if (tbUnitProKegBeforeStatus == null)
                                return string.Format("RKAS {0} tidak ada", JenisStatusProgKegConstant.Dict[beforeStatus]);
                            tbUnitProKegBeforeStatus.UpdatedBy = IdUser;
                            tbUnitProKegBeforeStatus.UpdatedDate = dateTimeService.GetCurrdate();
                            tbUnitProKegBeforeStatus.StatusProses = StatusProses;
                            tbUnitProKegBeforeStatus.Catatan = Catatan;
                            conn.Update(tbUnitProKegBeforeStatus);
                        }
                        else
                        {
                            tbUnitProkeg.Status = Status;
                            conn.Update(tbUnitProkeg);
                            int nextStatus = GetNextStatus(Status);

                            var tbUnitProKegNextStatus = (from a in conn.GetList<TbUnitProkegStatus>()
                                                          join b in conn.GetList<TbUser>() on a.CreatedBy equals b.IdUser
                                                          where a.IdUnitProkeg == IdUnitProkeg
                                                          & a.Status == nextStatus
                                                          select new { a, b }).FirstOrDefault();
                            if (tbUnitProKegNextStatus != null)
                                return string.Format("RKAS sudah {0} oleh {1} pada tanggal {2}", JenisStatusProgKegConstant.Dict[nextStatus], tbUnitProKegNextStatus.b.FirstName, dateTimeService.DateToLongString(_languageCode, tbUnitProKegNextStatus.a.CreatedDate));
                            var tbProKegStatus = (from a in conn.GetList<TbUnitProkegStatus>()
                                                  where a.IdUnitProkeg == IdUnitProkeg
                                                  & a.Status == Status
                                                  select a).FirstOrDefault();
                            if (tbProKegStatus == null)
                            {
                                tbProKegStatus = new TbUnitProkegStatus
                                {
                                    IdUnitProkeg = IdUnitProkeg,
                                    Status = Status,
                                    Catatan = Catatan,
                                    CreatedBy = IdUser,
                                    CreatedDate = dateTimeService.GetCurrdate(),
                                    StatusProses = StatusProses

                                };
                                conn.Insert(tbProKegStatus);
                            }
                            else
                            {
                                tbProKegStatus.StatusProses = StatusProses;
                                tbProKegStatus.UpdatedBy = IdUser;
                                tbProKegStatus.UpdatedDate = dateTimeService.GetCurrdate();
                                conn.Update(tbProKegStatus);
                            }
                        }

                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "ApproveData", ex);
            }

        }

        public List<ComboModel> GetCoas(out string oMessage)
        {
            oMessage = string.Empty;
            List<ComboModel> ret = new List<ComboModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var dt = from a in conn.GetList<TbCoa>()
                             where a.IdJenisAkun == JenisAkunConstant.Pratama
                             & a.Kode.Substring(0, 1) == "5"
                             select a;
                    if (dt == null || dt.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    foreach (var item in dt)
                    {
                        ret.Add(new ComboModel
                        {
                            Id = item.IdCoa,
                            Kode = item.Kode,
                            Nama = item.Nama
                        });
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetCoas", ex);
                return null;

                throw;
            }

        }
        public List<ComboModel> GetUnitPrograms(int IdUnitProkeg, out string oMessage)
        {
            oMessage = string.Empty;
            List<ComboModel> ret = new List<ComboModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var unitProkeg = conn.Get<TbUnitProkeg>(IdUnitProkeg);
                    var dt = from a in conn.GetList<TbProgram>()
                             join b in conn.GetList<TbProgramUnit>() on a.IdProgram equals b.IdProgram
                             where b.IdUnit == unitProkeg.IdUnit
                             select a;
                    if (dt == null || dt.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    foreach (var item in dt)
                    {
                        ret.Add(new ComboModel
                        {
                            Id = item.IdProgram,
                            Kode = item.Kode,
                            Nama = item.Nama
                        });
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetUnitPrograms", ex);
                return null;

                throw;
            }

        }
        public List<KeuUnitListModel> GetUnits(int IdUser, int IdTahunAjaran, out string oMessage)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    List<KeuUnitListModel> ret = new List<KeuUnitListModel>();
                    oMessage = string.Empty;
                    var tbTahunAjaran = (from a in conn.GetList<TbTahunAjaran>()
                                         where a.IdTahunAjaran == IdTahunAjaran
                                         select a).FirstOrDefault();
                    if (tbTahunAjaran == null) { oMessage = "Tahun Ajaran Tidak Terdaftar"; return null; };
                    var tbPegawaiUnit = (from a in conn.GetList<TbUnitPegawai>()
                                         join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                         join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                         where a.IdPegawai == IdUser
                                         select new { a, b, c }).FirstOrDefault();
                    if (tbPegawaiUnit == null) { oMessage = "Pengguna bukan pegawai"; return null; };

                    var data = from dt in conn.GetList<TbUnitProkeg>()
                               join dt_init in conn.GetList<TbUnitProkegStatus>(new { Status = JenisStatusProgKegConstant.Initial }) on dt.IdUnitProkeg equals dt_init.IdUnitProkeg
                               join unit in conn.GetList<TbUnit>() on dt.IdUnit equals unit.IdUnit
                               join dt_baru in conn.GetList<TbUnitProkegStatus>(string.Format("where status = {0}", JenisStatusProgKegConstant.Baru)) on dt.IdUnitProkeg equals dt_baru.IdUnitProkeg into dt_barua
                               from dt_baru in dt_barua.DefaultIfEmpty()
                               join dt_kaunit in conn.GetList<TbUnitProkegStatus>(string.Format("where status = {0}", JenisStatusProgKegConstant.DisetujuiKaUnit)) on dt.IdUnitProkeg equals dt_kaunit.IdUnitProkeg into dt_kaunita
                               from dt_kaunit in dt_kaunita.DefaultIfEmpty()
                               join dt_mgunit in conn.GetList<TbUnitProkegStatus>(string.Format("where status = {0}", JenisStatusProgKegConstant.DisetujuiManUnit)) on dt.IdUnitProkeg equals dt_mgunit.IdUnitProkeg into dt_mgunita
                               from dt_mgunit in dt_mgunita.DefaultIfEmpty()
                               join dt_mgkeu in conn.GetList<TbUnitProkegStatus>(string.Format("where status = {0}", JenisStatusProgKegConstant.DisetujuiManKeu)) on dt.IdUnitProkeg equals dt_mgkeu.IdUnitProkeg into dt_mgkeua
                               from dt_mgkeu in dt_mgkeua.DefaultIfEmpty()
                               join dt_dir in conn.GetList<TbUnitProkegStatus>(string.Format("where status = {0}", JenisStatusProgKegConstant.DisetujuiDirektur)) on dt.IdUnitProkeg equals dt_dir.IdUnitProkeg into dt_dira
                               from dt_dir in dt_mgkeua.DefaultIfEmpty()
                               join dt_sah in conn.GetList<TbUnitProkegStatus>(string.Format("where status = {0}", JenisStatusProgKegConstant.Disahkan)) on dt.IdUnitProkeg equals dt_sah.IdUnitProkeg into dt_saha
                               from dt_sah in dt_saha.DefaultIfEmpty()
                               where dt.IdTahunAjaran == IdTahunAjaran
                               select new { dt, dt_baru, unit, dt_kaunit, dt_mgunit, dt_mgkeu, dt_dir, dt_sah };
                    if (data == null || data.Count() == 0) { oMessage = string.Format("data RKAS tahun {0} tidak ada", tbTahunAjaran.Nama); return null; }

                    CekAkses(conn, IdUser);
                    foreach (var item in data.OrderBy(x => x.dt.IdUnit))
                    {
                        var tbPagus = from a in conn.GetList<TbUnitCoaPagu>()
                                      where a.IdUnit == item.dt.IdUnit
                                      & a.IdTahunAjaran == IdTahunAjaran
                                      select a;

                        if (tbPagus == null || tbPagus.Count()==0)
                            continue;
                        var tbPagu0 = from a in tbPagus
                                       where a.RpPagu == 0
                                       select a;
                        if (tbPagu0.Count() > 0)
                        {
                            continue;
                        }

                        if (tbPegawaiUnit.a.IdUnit != item.dt.IdUnit)
                        {
                            hakAkses = new KeuHakAksesModel
                            {
                                CanAdd = false,
                                CanApprove = false,
                                CanDelete = false,
                                CanDownload = false,
                                CanEdit = false,
                                CanRead = false,
                                CanRelease = false
                            };

                            if (
                                tbPegawaiUnit.a.IdUnit == UnitConstant.AKT ||
                                tbPegawaiUnit.a.IdUnit == UnitConstant.FNC ||
                                tbPegawaiUnit.a.IdUnit == UnitConstant.DIR
                                )
                            {
                                CekAkses(conn, IdUser);
                            }
                        }
                        KeuUnitListModel m = new KeuUnitListModel
                        {
                            IdUnitProKeg = item.dt.IdUnitProkeg,
                            Unit = item.unit.Nama,
                            TahunAjaran = tbTahunAjaran.Nama,
                            Aksis = new List<string>(),
                            Status = JenisStatusProgKegConstant.Dict[item.dt.Status],
                            Rp = item.dt.Rp
                        };
                        if (item.dt.Status == JenisStatusProgKegConstant.Baru)
                        {
                            if (item.dt_baru != null)
                            {
                                m.StatusProses = StatusProsesKeuanganConstant.Dict[item.dt_baru.StatusProses];
                                m.Catatan = item.dt_baru.Catatan;
                            }
                        }
                        else if (item.dt.Status == JenisStatusProgKegConstant.DisetujuiKaUnit)
                        {
                            if (item.dt_kaunit != null)
                            {
                                m.StatusProses = StatusProsesKeuanganConstant.Dict[item.dt_kaunit.StatusProses];
                                if (item.dt_kaunit.StatusProses == StatusProsesKeuanganConstant.Rejected)
                                    m.StatusProses += " Manager Unit";
                                m.Catatan = item.dt_kaunit.Catatan;
                            }
                        }
                        else if (item.dt.Status == JenisStatusProgKegConstant.DisetujuiManUnit)
                        {
                            if (item.dt_mgunit != null)
                            {
                                m.StatusProses = StatusProsesKeuanganConstant.Dict[item.dt_mgunit.StatusProses];
                                if (item.dt_mgunit.StatusProses == StatusProsesKeuanganConstant.Rejected)
                                    m.StatusProses += " Manager Keuangan";
                                m.Catatan = item.dt_mgunit.Catatan;
                            }
                        }
                        else if (item.dt.Status == JenisStatusProgKegConstant.DisetujuiManKeu)
                        {
                            if (item.dt_mgkeu != null)
                            {
                                m.StatusProses = StatusProsesKeuanganConstant.Dict[item.dt_mgkeu.StatusProses];
                                if (item.dt_mgkeu.StatusProses == StatusProsesKeuanganConstant.Rejected)
                                    m.StatusProses += " Direktur";
                                m.Catatan = item.dt_mgkeu.Catatan;
                            }
                        }
                        else if (item.dt.Status == JenisStatusProgKegConstant.DisetujuiDirektur)
                        {
                            if (item.dt_dir != null)
                            {
                                m.StatusProses = StatusProsesKeuanganConstant.Dict[item.dt_dir.StatusProses];
                                m.Catatan = item.dt_dir.Catatan;
                            }
                        }


                        if (item.dt.Status == JenisStatusProgKegConstant.Initial
                            & item.dt_baru == null)
                        {
                            if (hakAkses.CanAdd) m.Aksis.Add("View");
                        }
                        else
                        {
                            if (hakAkses.CanRead) m.Aksis.Add("View");
                            if (hakAkses.CanDownload) m.Aksis.Add("Download");
                            if (item.dt.Status == JenisStatusProgKegConstant.Baru)
                            {
                                //if (hakAkses.CanEdit) m.Aksis.Add("Edit");
                                //if (hakAkses.CanDelete) m.Aksis.Add("Hapus");
                                if (tbPegawaiUnit.c.Level == LevelJabatanPegawaiConstant.KepalaUnit
                                    || tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                                    if (hakAkses.CanApprove) m.Aksis.Add("Approve Ka Unit");
                            }
                            else if (item.dt.Status == JenisStatusProgKegConstant.DisetujuiKaUnit)
                            {
                                //if (hakAkses.CanEdit) m.Aksis.Add("Edit");
                                //if (hakAkses.CanDelete) m.Aksis.Add("Hapus");
                                if (tbPegawaiUnit.c.Level == LevelJabatanPegawaiConstant.Manager
                                    || tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                                    if (hakAkses.CanApprove) m.Aksis.Add("Approve Mgr Unit");
                            }
                            else if (item.dt.Status == JenisStatusProgKegConstant.DisetujuiManUnit)
                            {
                                if (tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.ManKeu
                                    || tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                                    if (hakAkses.CanApprove) m.Aksis.Add("Approve Mgr Keu");
                            }
                            else if (item.dt.Status == JenisStatusProgKegConstant.DisetujuiManKeu)
                            {
                                if (tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.Dir
                                    || tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                                    if (hakAkses.CanApprove) m.Aksis.Add("Approve Direktur");
                            }
                            else if (item.dt.Status == JenisStatusProgKegConstant.DisetujuiDirektur)
                            {
                                if (tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                                    if (hakAkses.CanRelease) m.Aksis.Add("Set Sah");
                            }
                        }


                        if (hakAkses.CanRead)
                            ret.Add(m);
                    }
                    if (ret.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKeuUnitProKegs", ex);
                return null;
            }
        }
        public List<KeuUnitProListModel> GetPrograms(int IdUnitProkeg, out string oMessage)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    List<KeuUnitProListModel> ret = new List<KeuUnitProListModel>();
                    oMessage = string.Empty;

                    var dt = from a in conn.GetList<TbProgramUnit>()
                             join b in conn.GetList<TbUnitProkeg>() on a.IdUnit equals b.IdUnit
                             join c in conn.GetList<TbProgram>() on a.IdProgram equals c.IdProgram
                             where b.IdUnitProkeg == IdUnitProkeg
                             select new { a, b, c };
                    if (dt==null || dt.Count() == 0) { oMessage = "data tidak ada"; return null; }

                    foreach (var item in dt)
                    {
                        KeuUnitProListModel m = new KeuUnitProListModel
                        {
                            IdUnitProKeg = item.b.IdUnitProkeg,
                            Aksis = new List<string>(),
                            IdProgram = item.a.IdProgram,
                            Program = item.c.Nama,
                            JumlahKegiatanWajib = 0,
                            RpKegiatanWajib = 0,
                            JumlahKegiatanTambahan = 0,
                            RpKegiatanTambahan = 0,
                            RpTotal = item.b.Rp
                        };
                        var dtWajib = from a in conn.GetList<TbUnitProkegWajib>()
                                      join b in conn.GetList<TbProgramKegiatan>() on a.IdKegiatan equals b.IdKegiatan
                                      join c in conn.GetList<TbProgram>() on b.IdProgram equals c.IdProgram
                                      where a.IdUnitProkeg == item.b.IdUnitProkeg
                                      & c.IdProgram == item.c.IdProgram
                                      select a;
                        if (dtWajib != null)
                        {
                            m.JumlahKegiatanWajib += dtWajib.Count();
                            m.RpKegiatanWajib = dtWajib.Sum(x => x.Rp);
                        }
                        var dtTambahan = from a in conn.GetList<TbUnitProkegTambahan>()
                                       where a.IdUnitProkeg == item.b.IdUnitProkeg
                                       & a.IdProgram == item.c.IdProgram
                                       select a;
                        if (dtTambahan != null)
                        {
                            m.JumlahKegiatanTambahan += dtTambahan.Count();
                            m.RpKegiatanTambahan = dtTambahan.Sum(x => x.Rp);
                        }
                        m.Aksis.Add("View");
                        ret.Add(m);
                    }

                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPrograms", ex);
                return null;
            }
        }
        public List<KeuUnitProKegListModel> GetKegiatans(int IdUser, int IdUnitProkeg, int IdProgram, out string oMessage)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    List<KeuUnitProKegListModel> ret = new List<KeuUnitProKegListModel>();
                    oMessage = string.Empty;
                    CekAkses(conn, IdUser);
                    var dataWajib = from b in conn.GetList<TbUnitProkegWajib>()
                                    join c in conn.GetList<TbProgramKegiatan>() on b.IdKegiatan equals c.IdKegiatan
                                    join d in conn.GetList<TbProgram>() on c.IdProgram equals d.IdProgram
                                    where b.IdUnitProkeg == IdUnitProkeg & d.IdProgram == IdProgram
                                    select new { b, c, d };
                    if (dataWajib != null & dataWajib.Count() > 0)
                    {
                        foreach (var item in dataWajib)
                        {
                            KeuUnitProKegListModel m = new KeuUnitProKegListModel
                            {
                                IdUnitProkegWajib = item.b.IdUnitProkegWajib,
                                IdUnitProkegTambahan = null,
                                Kegiatan = item.c.Nama,
                                Tanggal = item.b.Tanggal.ToString("dd-MM-yyyy"),
                                Rp = item.b.Rp,
                                Jenis = "WAJIB",
                                Aksis = new List<string>()

                            };
                            if (hakAkses.CanRead) m.Aksis.Add("View");
                            if (hakAkses.CanEdit) m.Aksis.Add("Edit");
                            ret.Add(m);
                        }
                    }
                    var dataTambahan = from b in conn.GetList<TbUnitProkegTambahan>()
                                    join d in conn.GetList<TbProgram>() on b.IdProgram equals d.IdProgram
                                    where b.IdUnitProkeg == IdUnitProkeg & d.IdProgram == IdProgram
                                    select new { b, d };
                    if (dataTambahan != null & dataTambahan.Count() > 0)
                    {
                        foreach (var item in dataTambahan)
                        {
                            KeuUnitProKegListModel m = new KeuUnitProKegListModel
                            {
                                IdUnitProkegWajib = null,
                                IdUnitProkegTambahan = item.b.IdUnitProkegTambahan,
                                Kegiatan = item.b.NamaKegiatan,
                                Tanggal = item.b.Tanggal.ToString("dd-MM-yyyy"),
                                Rp = item.b.Rp,
                                Jenis = "TAMBAHAN",
                                Aksis = new List<string>()
                            };

                            if (hakAkses.CanRead) m.Aksis.Add("View");
                            if (hakAkses.CanEdit) m.Aksis.Add("Edit");
                            if (hakAkses.CanDelete) m.Aksis.Add("Hapus");
                            ret.Add(m);
                        }
                    }
                    if (ret.Count() > 0) return ret;
                    oMessage = "data tidak ada";
                    return null;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKegiatans", ex);
                return null;
            }

        }
        public List<KeuUnitProKegWajibCoaListModel> GetKegiatanWajibCoas(int IdUser, int IdUnitProkegWajib, out string oMessage)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    List<KeuUnitProKegWajibCoaListModel> ret = new List<KeuUnitProKegWajibCoaListModel>();
                    oMessage = string.Empty;
                    CekAkses(conn, IdUser);

                    var data = from a in conn.GetList<TbUnitProkegWajibCoa>()
                               join b in conn.GetList<TbCoa>() on a.IdCoa equals b.IdCoa
                               join c in conn.GetList<TbJenisSatuan>() on a.IdJenisSatuan equals c.IdJenisSatuan
                               join d in conn.GetList<TbJenisVolume>() on a.IdJenisVolume equals d.IdJenisVolume
                               where a.IdUnitProkegWajib == IdUnitProkegWajib
                               select new { a, b, c, d };
                    if(data==null || data.Count()==0) { oMessage = "data tidak ada"; return null; }
                    foreach (var item in data)
                    {
                        KeuUnitProKegWajibCoaListModel m = new KeuUnitProKegWajibCoaListModel
                        {
                            IdUnitProkegWajibCoa = item.a.IdUnitProkegWajibCoa,
                            Catatan = item.a.Catatan,
                            HargaSatuan = item.a.HargaSatuan,
                            JumlahSatuan = item.a.JumlahSatuan,
                            JumlahVolume = item.a.JumlahVolume,
                            Satuan = item.c.Nama,
                            Volume = item.d.Nama,
                            Rp = item.a.JumlahSatuan * item.a.JumlahVolume * item.a.HargaSatuan,
                            Aksis = new List<string>(),
                            KodeCoa = item.b.Kode,
                            NamaCoa = item.b.Nama,
                            IdCoa = item.b.IdCoa,
                            IdJenisSatuan = item.c.IdJenisSatuan,
                            IdJenisVolume = item.d.IdJenisVolume
                        };
                        if (hakAkses.CanRead) m.Aksis.Add("View");
                        if (hakAkses.CanEdit) m.Aksis.Add("Edit");
                        if (hakAkses.CanDelete) m.Aksis.Add("Hapus");
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKegiatanWajibCoas", ex);
                return null;
            }

        }
        public List<KeuUnitProKegTambahanCoaListModel> GetKegiatanTambahanCoas(int IdUser, int IdUnitProkegTambahan, out string oMessage)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    List<KeuUnitProKegTambahanCoaListModel> ret = new List<KeuUnitProKegTambahanCoaListModel>();
                    oMessage = string.Empty;
                    CekAkses(conn, IdUser);

                    var data = from a in conn.GetList<TbUnitProkegTambahanCoa>()
                               join b in conn.GetList<TbCoa>() on a.IdCoa equals b.IdCoa
                               join c in conn.GetList<TbJenisSatuan>() on a.IdJenisSatuan equals c.IdJenisSatuan
                               join d in conn.GetList<TbJenisVolume>() on a.IdJenisVolume equals d.IdJenisVolume
                               where a.IdUnitProkegTambahan == IdUnitProkegTambahan
                               select new { a, b, c, d };
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    foreach (var item in data)
                    {
                        KeuUnitProKegTambahanCoaListModel m = new KeuUnitProKegTambahanCoaListModel
                        {
                            IdUnitProkegTambahanCoa = item.a.IdUnitProkegTambahanCoa,
                            Catatan = item.a.Catatan,
                            HargaSatuan = item.a.HargaSatuan,
                            JumlahSatuan = item.a.JumlahSatuan,
                            JumlahVolume = item.a.JumlahVolume,
                            Satuan = item.c.Nama,
                            Volume = item.d.Nama,
                            Rp = item.a.JumlahSatuan * item.a.JumlahVolume * item.a.HargaSatuan,
                            Aksis = new List<string>(),
                            KodeCoa = item.b.Kode,
                            NamaCoa = item.b.Nama,
                            IdCoa = item.b.IdCoa,
                            IdJenisSatuan = item.c.IdJenisSatuan,
                            IdJenisVolume = item.d.IdJenisVolume
                        };
                        if (hakAkses.CanRead) m.Aksis.Add("View");
                        if (hakAkses.CanEdit) m.Aksis.Add("Edit");
                        if (hakAkses.CanDelete) m.Aksis.Add("Hapus");
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKegiatanTambahanCoas", ex);
                return null;
            }
        }
        public KeuUnitProKegWajibCoaListModel GetKegiatanWajibCoa(int IdUnitProkegWajibCoa, out string oMessage)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    KeuUnitProKegWajibCoaListModel ret = new KeuUnitProKegWajibCoaListModel();
                    oMessage = string.Empty;

                    var item = (from a in conn.GetList<TbUnitProkegWajibCoa>()
                                join b in conn.GetList<TbCoa>() on a.IdCoa equals b.IdCoa
                                join c in conn.GetList<TbJenisSatuan>() on a.IdJenisSatuan equals c.IdJenisSatuan
                                join d in conn.GetList<TbJenisVolume>() on a.IdJenisVolume equals d.IdJenisVolume
                                where a.IdUnitProkegWajibCoa == IdUnitProkegWajibCoa
                                select new { a, b, c, d }).FirstOrDefault();
                    if (item == null) { oMessage = "data tidak ada"; return null; }
                    ret = new KeuUnitProKegWajibCoaListModel
                    {
                        IdUnitProkegWajibCoa = item.a.IdUnitProkegWajibCoa,
                        Catatan = item.a.Catatan,
                        HargaSatuan = item.a.HargaSatuan,
                        JumlahSatuan = item.a.JumlahSatuan,
                        JumlahVolume = item.a.JumlahVolume,
                        Satuan = item.c.Nama,
                        Volume = item.d.Nama,
                        Rp = item.a.JumlahSatuan * item.a.JumlahVolume * item.a.HargaSatuan,
                        Aksis = new List<string>(),
                        KodeCoa = item.b.Kode,
                        NamaCoa = item.b.Nama,
                        IdCoa = item.b.IdCoa,
                        IdJenisSatuan = item.c.IdJenisSatuan,
                        IdJenisVolume = item.d.IdJenisVolume
                    };
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKegiatanWajibCoa", ex);
                return null;
            }

        }
        public KeuUnitProKegTambahanCoaListModel GetKegiatanTambahanCoa(int IdUnitProkegTambahanCoa, out string oMessage)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    KeuUnitProKegTambahanCoaListModel ret = new KeuUnitProKegTambahanCoaListModel();
                    oMessage = string.Empty;

                    var item = (from a in conn.GetList<TbUnitProkegTambahanCoa>()
                               join b in conn.GetList<TbCoa>() on a.IdCoa equals b.IdCoa
                               join c in conn.GetList<TbJenisSatuan>() on a.IdJenisSatuan equals c.IdJenisSatuan
                               join d in conn.GetList<TbJenisVolume>() on a.IdJenisVolume equals d.IdJenisVolume
                               where a.IdUnitProkegTambahanCoa == IdUnitProkegTambahanCoa
                               select new { a, b, c, d }).FirstOrDefault();
                    if (item == null) { oMessage = "data tidak ada"; return null; }
                    ret = new KeuUnitProKegTambahanCoaListModel
                    {
                        IdUnitProkegTambahanCoa = item.a.IdUnitProkegTambahanCoa,
                        Catatan = item.a.Catatan,
                        HargaSatuan = item.a.HargaSatuan,
                        JumlahSatuan = item.a.JumlahSatuan,
                        JumlahVolume = item.a.JumlahVolume,
                        Satuan = item.c.Nama,
                        Volume = item.d.Nama,
                        Rp = item.a.JumlahSatuan * item.a.JumlahVolume * item.a.HargaSatuan,
                        Aksis = new List<string>(),
                        KodeCoa = item.b.Kode,
                        NamaCoa = item.b.Nama,
                        IdCoa = item.b.IdCoa,
                        IdJenisSatuan = item.c.IdJenisSatuan,
                        IdJenisVolume = item.d.IdJenisVolume
                    };
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKegiatanTambahanCoa", ex);
                return null;
            }

        }
        public string KegiatanWajibEdit(int IdUser, int IdUnitProkegWajib, string Tanggal)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    using (var tx = conn.BeginTransaction())
                    {
                        if (!hakAkses.CanEdit) return "tidak berhak merubah kegiatan";
                        TbUnitProkegWajib tbUnitProkegWajib = conn.Get<TbUnitProkegWajib>(IdUnitProkegWajib);
                        if (tbUnitProkegWajib == null) return "data tidak ada";

                        var tbUnitProKeg = conn.Get<TbUnitProkeg>(tbUnitProkegWajib.IdUnitProkeg);
                        if (tbUnitProKeg == null) return "data program kegiatan tidak ada";
                        int[] statuss = new int[] { JenisStatusProgKegConstant.Initial, JenisStatusProgKegConstant.Baru };
                        if (!statuss.Contains(tbUnitProKeg.Status))
                            return string.Format("status data sudah {0}", JenisStatusProgKegConstant.Dict[tbUnitProKeg.Status]);

                        tbUnitProkegWajib.Tanggal = dateTimeService.StrToDateTime(Tanggal);
                        conn.Update(tbUnitProkegWajib);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "KegiatanWajibEdit", ex);
            }

        }

        public string KegiatanTambahanAdd(int IdUser, int IdUnitProkeg, int IdProgram, string NamaKegiatan, string Tanggal, out int IdUnitProkegTambahan)
        {
            IdUnitProkegTambahan = 0;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    using (var tx = conn.BeginTransaction())
                    {
                        if (!hakAkses.CanAdd) return "tidak berhak menambah kegiatan";
                        var tbUnitProKeg = conn.Get<TbUnitProkeg>(IdUnitProkeg);
                        if (tbUnitProKeg == null) return "data program kegiatan tidak ada";
                        int[] statuss = new int[] { JenisStatusProgKegConstant.Initial, JenisStatusProgKegConstant.Baru };
                        if (!statuss.Contains(tbUnitProKeg.Status))
                            return string.Format("status data sudah {0}", JenisStatusProgKegConstant.Dict[tbUnitProKeg.Status]);

                        TbUnitProkegTambahan tbUnitProkegTambahan = new TbUnitProkegTambahan
                        {
                            IdProgram = IdProgram,
                            IdUnitProkeg = IdUnitProkeg,
                            NamaKegiatan = NamaKegiatan,
                            Tanggal = dateTimeService.StrToDateTime(Tanggal),
                            Rp = 0
                        };
                        var idUnitProkegTambahan = conn.Insert(tbUnitProkegTambahan);
                        IdUnitProkegTambahan = (int)idUnitProkegTambahan;

                        TbUnitProkegStatus tbUnitProkegStatus = (from a in conn.GetList<TbUnitProkegStatus>()
                                                                 where a.IdUnitProkeg == tbUnitProKeg.IdUnitProkeg
                                                                 & a.Status == JenisStatusProgKegConstant.Baru
                                                                 select a).FirstOrDefault();
                        if (tbUnitProkegStatus == null)
                        {
                            tbUnitProkegStatus = new TbUnitProkegStatus
                            {
                                IdUnitProkeg = tbUnitProKeg.IdUnitProkeg,
                                Status = JenisStatusProgKegConstant.Baru,
                                CreatedBy = IdUser,
                                CreatedDate = dateTimeService.GetCurrdate(),
                                StatusProses = StatusProsesKeuanganConstant.New
                            };
                            conn.Insert(tbUnitProkegStatus);
                        }
                        else
                        {
                            tbUnitProkegStatus.UpdatedBy = IdUser;
                            tbUnitProkegStatus.UpdatedDate = dateTimeService.GetCurrdate();
                            conn.Update(tbUnitProkegStatus);
                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "KegiatanTambahanAdd", ex);
            }
        }
        public string KegiatanTambahanEdit(int IdUser, int IdUnitProkegTambahan, string NamaKegiatan, string Tanggal)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    using (var tx = conn.BeginTransaction())
                    {
                        if (!hakAkses.CanEdit) return "tidak berhak merubah kegiatan";
                        TbUnitProkegTambahan tbUnitProkegTambahan = conn.Get<TbUnitProkegTambahan>(IdUnitProkegTambahan);
                        if (tbUnitProkegTambahan == null) return "data tidak ada";

                        var tbUnitProKeg = conn.Get<TbUnitProkeg>(tbUnitProkegTambahan.IdUnitProkeg);
                        if (tbUnitProKeg == null) return "data program kegiatan tidak ada";
                        int[] statuss = new int[] { JenisStatusProgKegConstant.Initial, JenisStatusProgKegConstant.Baru };
                        if (!statuss.Contains(tbUnitProKeg.Status))
                            return string.Format("status data sudah {0}", JenisStatusProgKegConstant.Dict[tbUnitProKeg.Status]);

                        tbUnitProkegTambahan.NamaKegiatan = NamaKegiatan;
                        tbUnitProkegTambahan.Tanggal = dateTimeService.StrToDateTime(Tanggal);
                        conn.Update(tbUnitProkegTambahan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "KegiatanTambahanEdit", ex);
            }
        }
        public string KegiatanTambahanDelete(int IdUser, int IdUnitProkegTambahan)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    using (var tx = conn.BeginTransaction())
                    {
                        if (!hakAkses.CanDelete) return "tidak berhak merubah kegiatan";
                        TbUnitProkegTambahan tbUnitProkegTambahan = conn.Get<TbUnitProkegTambahan>(IdUnitProkegTambahan);
                        if (tbUnitProkegTambahan == null) return "data tidak ada";

                        TbUnitProkeg tbUnitProkeg = (from a in conn.GetList<TbUnitProkeg>()
                                                    where a.IdUnitProkeg == tbUnitProkegTambahan.IdUnitProkeg
                                                    select a).FirstOrDefault();
                        if (tbUnitProkeg == null) return "data program kegiatan tidak ada";
                        int[] statuss = new int[] { JenisStatusProgKegConstant.Initial, JenisStatusProgKegConstant.Baru };
                        if (!statuss.Contains(tbUnitProkeg.Status))
                            return string.Format("status data sudah {0}", JenisStatusProgKegConstant.Dict[tbUnitProkeg.Status]);
                        
                        tbUnitProkeg.Rp -= tbUnitProkegTambahan.Rp;
                        conn.Update(tbUnitProkeg);
                        conn.DeleteList<TbUnitProkegTambahanCoa>(string.Format("where id_unit_prokeg_tambahan = {0}", IdUnitProkegTambahan));
                        conn.Delete(tbUnitProkegTambahan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "KegiatanTambahanDelete", ex);
            }
        }
        private string CekUnitCoaPagu(IDbConnection conn, int IdUnit, int IdTahunAjaran, int IdCoaProKeg, double rpTotal, double rpTotalOld, string KodeCoa)
        {
            var tbUnitCoaPagu = (from a in conn.GetList<TbUnitCoaPagu>()
                                 join b in conn.GetList<TbUnitCoaUsed>() on a.IdUnitCoaPagu equals b.IdUnitCoaPagu
                                 where a.IdUnit == IdUnit
                                 & a.IdTahunAjaran == IdTahunAjaran
                                 & b.IdCoaProkeg== IdCoaProKeg
                                 select a).FirstOrDefault();
            if (tbUnitCoaPagu == null) return string.Format("kode akun {0} belum diset pagu", KodeCoa);

            double rpSisa = tbUnitCoaPagu.RpPagu - tbUnitCoaPagu.RpPaguUsed;
            rpSisa -= rpTotalOld;
            if (rpTotal > rpSisa)
                return string.Format("kode akun {0} sudah melebih pagu", KodeCoa);

            tbUnitCoaPagu.RpPaguUsed += (rpTotal - rpTotalOld);
            conn.Update(tbUnitCoaPagu);
            return string.Empty;
        }
        public string KegiatanWajibCoaAdd(int IdUser, KeuUnitProKegCoaWajibAddModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    using (var tx = conn.BeginTransaction())
                    {
                        if (!hakAkses.CanAdd) return "tidak berhak merubah kegiatan";
                        TbUnitProkegWajib tbUnitProkegWajib = conn.Get<TbUnitProkegWajib>(Data.IdUnitProkegWajib);
                        TbCoa tbCoa = conn.Get<TbCoa>(Data.IdCoa);
                        if (tbUnitProkegWajib == null) return "data tidak ada";
                        TbUnitProkeg tbUnitProkeg = conn.Get<TbUnitProkeg>(tbUnitProkegWajib.IdUnitProkeg);
                        if (tbUnitProkeg == null) return "data program kegiatan tidak ada";
                        int[] statuss = new int[] { JenisStatusProgKegConstant.Initial, JenisStatusProgKegConstant.Baru };
                        if (!statuss.Contains(tbUnitProkeg.Status))
                            return string.Format("status data sudah {0}", JenisStatusProgKegConstant.Dict[tbUnitProkeg.Status]);



                        var cekData = (from a in conn.GetList<TbUnitProkegWajibCoa>()
                                       join b in conn.GetList<TbUnitProkegWajib>() on a.IdUnitProkegWajib equals b.IdUnitProkegWajib
                                       join c in conn.GetList<TbProgramKegiatan>() on b.IdKegiatan equals c.IdKegiatan
                                       join d in conn.GetList<TbCoa>() on a.IdCoa equals d.IdCoa
                                       where a.IdCoa == Data.IdCoa
                                       & a.IdUnitProkegWajib == Data.IdUnitProkegWajib
                                       select new { a, b, c, d }).FirstOrDefault();
                        if (cekData != null)
                            return string.Format("kode akun {0} sudah ada untuk kegiatan {1}", cekData.d.Kode, cekData.c.Nama);

                        double rpTotal = Data.JumlahSatuan * Data.JumlahVolume * Data.HargaSatuan;
                        string err = CekUnitCoaPagu(conn, tbUnitProkeg.IdUnit, tbUnitProkeg.IdTahunAjaran, tbCoa.IdCoa, rpTotal, 0, tbCoa.Kode);
                        if (!string.IsNullOrEmpty(err)) return err;

                        tbUnitProkegWajib.Rp += rpTotal;
                        tbUnitProkeg.Rp += rpTotal;
                        tbUnitProkeg.Status = JenisStatusProgKegConstant.Baru;

                        TbUnitProkegWajibCoa tbUnitProkegWajibCoa = new TbUnitProkegWajibCoa
                        {
                            IdCoa = Data.IdCoa,
                            Catatan = Data.Catatan,
                            HargaSatuan = Data.HargaSatuan,
                            IdJenisSatuan = Data.IdJenisSatuan,
                            IdJenisVolume = Data.IdJenisVolume,
                            IdUnitProkegWajib = Data.IdUnitProkegWajib,
                            JumlahSatuan = Data.JumlahSatuan,
                            JumlahVolume = Data.JumlahVolume
                        };
                        conn.Insert(tbUnitProkegWajibCoa);
                        conn.Update(tbUnitProkegWajib);
                        conn.Update(tbUnitProkeg);

                        TbUnitProkegStatus tbUnitProkegStatus = (from a in conn.GetList<TbUnitProkegStatus>()
                                                                 where a.IdUnitProkeg == tbUnitProkeg.IdUnitProkeg
                                                                 & a.Status == JenisStatusProgKegConstant.Baru
                                                                 select a).FirstOrDefault();
                        if (tbUnitProkegStatus == null)
                        {
                            tbUnitProkegStatus = new TbUnitProkegStatus
                            {
                                IdUnitProkeg = tbUnitProkeg.IdUnitProkeg,
                                Status = JenisStatusProgKegConstant.Baru,
                                CreatedBy = IdUser,
                                CreatedDate = dateTimeService.GetCurrdate(),
                                StatusProses = StatusProsesKeuanganConstant.New
                            };
                            conn.Insert(tbUnitProkegStatus);
                        }
                        else
                        {
                            tbUnitProkegStatus.UpdatedBy = IdUser;
                            tbUnitProkegStatus.UpdatedDate = dateTimeService.GetCurrdate();
                            conn.Update(tbUnitProkegStatus);
                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "KegiatanWajibCoaAdd", ex);
            }
        }
        public string KegiatanWajibCoaEdit(int IdUser, KeuUnitProKegCoaWajibEditModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    using (var tx = conn.BeginTransaction())
                    {
                        if (!hakAkses.CanEdit) return "tidak berhak merubah data";
                        TbUnitProkegWajibCoa tbUnitProkegWajibCoa = conn.Get<TbUnitProkegWajibCoa>(Data.IdUnitProkegWajibCoa);
                        if (tbUnitProkegWajibCoa == null) return "data tidak ada";
                        double rpTotalOld = tbUnitProkegWajibCoa.JumlahSatuan * tbUnitProkegWajibCoa.JumlahVolume * tbUnitProkegWajibCoa.HargaSatuan;
                        TbCoa tbCoa = conn.Get<TbCoa>(tbUnitProkegWajibCoa.IdCoa);

                        TbUnitProkegWajib tbUnitProkegWajib = conn.Get<TbUnitProkegWajib>(Data.IdUnitProkegWajib);
                        if (tbUnitProkegWajib == null) return "data tidak ada";

                        TbUnitProkeg tbUnitProkeg = conn.Get<TbUnitProkeg>(tbUnitProkegWajib.IdUnitProkeg);
                        if (tbUnitProkeg == null) return "data program kegiatan tidak ada";
                        int[] statuss = new int[] { JenisStatusProgKegConstant.Initial, JenisStatusProgKegConstant.Baru };
                        if (!statuss.Contains(tbUnitProkeg.Status))
                            return string.Format("status data sudah {0}", JenisStatusProgKegConstant.Dict[tbUnitProkeg.Status]);



                        if (tbUnitProkegWajibCoa.IdCoa != Data.IdCoa)
                            return "kode coa tidak sesuai";

                        double rpTotal = Data.JumlahSatuan * Data.JumlahVolume * Data.HargaSatuan;
                        string err = CekUnitCoaPagu(conn, tbUnitProkeg.IdUnit, tbUnitProkeg.IdTahunAjaran, tbCoa.IdCoa, rpTotal, rpTotalOld, tbCoa.Kode);
                        if (!string.IsNullOrEmpty(err)) return err;

                        tbUnitProkegWajib.Rp -= rpTotalOld;
                        tbUnitProkeg.Rp -= rpTotalOld;

                        tbUnitProkegWajib.Rp += rpTotal;
                        tbUnitProkeg.Rp += rpTotal;

                        tbUnitProkegWajibCoa.IdCoa = Data.IdCoa;
                        tbUnitProkegWajibCoa.Catatan = Data.Catatan;
                        tbUnitProkegWajibCoa.HargaSatuan = Data.HargaSatuan;
                        tbUnitProkegWajibCoa.IdJenisSatuan = Data.IdJenisSatuan;
                        tbUnitProkegWajibCoa.IdJenisVolume = Data.IdJenisVolume;
                        tbUnitProkegWajibCoa.JumlahSatuan = Data.JumlahSatuan;
                        tbUnitProkegWajibCoa.JumlahVolume = Data.JumlahVolume;

                        conn.Update(tbUnitProkegWajibCoa);
                        conn.Update(tbUnitProkegWajib);
                        conn.Update(tbUnitProkeg);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "KegiatanWajibCoaEdit", ex);
            }
        }
        public string KegiatanWajibCoaDelete(int IdUser, int IdUnitProkegWajibCoa)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    using (var tx = conn.BeginTransaction())
                    {
                        if (!hakAkses.CanDelete) return "tidak berhak menghapus data";
                        TbUnitProkegWajibCoa tbUnitProkegWajibCoa = conn.Get<TbUnitProkegWajibCoa>(IdUnitProkegWajibCoa);
                        if (tbUnitProkegWajibCoa == null) return "data tidak ada";
                        double rpTotalOld = tbUnitProkegWajibCoa.JumlahSatuan * tbUnitProkegWajibCoa.JumlahVolume * tbUnitProkegWajibCoa.HargaSatuan;

                        TbUnitProkegWajib tbUnitProkegWajib = conn.Get<TbUnitProkegWajib>(tbUnitProkegWajibCoa.IdUnitProkegWajib);
                        if (tbUnitProkegWajib == null) return "data tidak ada";

                        TbUnitProkeg tbUnitProkeg = conn.Get<TbUnitProkeg>(tbUnitProkegWajib.IdUnitProkeg);
                        if (tbUnitProkeg == null) return "data program kegiatan tidak ada";
                        int[] statuss = new int[] { JenisStatusProgKegConstant.Initial, JenisStatusProgKegConstant.Baru };
                        if (!statuss.Contains(tbUnitProkeg.Status))
                            return string.Format("status data sudah {0}", JenisStatusProgKegConstant.Dict[tbUnitProkeg.Status]);

                        tbUnitProkegWajib.Rp -= rpTotalOld;
                        tbUnitProkeg.Rp -= rpTotalOld;
                        tbUnitProkeg.Status = JenisStatusProgKegConstant.Baru;

                        TbUnitCoaPagu tbUnitCoaPagu = (from a in conn.GetList<TbUnitCoaPagu>()
                                                       join b in conn.GetList<TbUnitCoaUsed>() on a.IdUnitCoaPagu equals b.IdUnitCoaPagu
                                                       where a.IdUnit == tbUnitProkeg.IdUnit
                                                       & a.IdTahunAjaran == tbUnitProkeg.IdTahunAjaran
                                                       & b.IdCoaProkeg == tbUnitProkegWajibCoa.IdCoa
                                                       select a).FirstOrDefault();
                        tbUnitCoaPagu.RpPaguUsed -= rpTotalOld;
                        conn.Update(tbUnitCoaPagu);

                        TbUnitProkegStatus tbUnitProkegStatus = (from a in conn.GetList<TbUnitProkegStatus>()
                                                                 where a.IdUnitProkeg == tbUnitProkeg.IdUnitProkeg
                                                                 & a.Status == JenisStatusProgKegConstant.Baru
                                                                 select a).FirstOrDefault();
                        if (tbUnitProkegStatus == null)
                        {
                            tbUnitProkegStatus = new TbUnitProkegStatus
                            {
                                IdUnitProkeg = tbUnitProkeg.IdUnitProkeg,
                                Status = JenisStatusProgKegConstant.Baru,
                                CreatedBy = IdUser,
                                CreatedDate = dateTimeService.GetCurrdate(),
                                StatusProses = StatusProsesKeuanganConstant.New
                            };
                            conn.Insert(tbUnitProkegStatus);
                        }
                        else
                        {
                            tbUnitProkegStatus.UpdatedBy = IdUser;
                            tbUnitProkegStatus.UpdatedDate = dateTimeService.GetCurrdate();
                            conn.Update(tbUnitProkegStatus);
                        }

                        conn.Delete(tbUnitProkegWajibCoa);
                        conn.Update(tbUnitProkegWajib);
                        conn.Update(tbUnitProkeg);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "KegiatanWajibCoaDelete", ex);
            }
        }

        public string KegiatanTambahanCoaAdd(int IdUser, KeuUnitProKegCoaTambahanAddModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    using (var tx = conn.BeginTransaction())
                    {
                        if (!hakAkses.CanAdd) return "tidak berhak merubah kegiatan";
                        TbUnitProkegTambahan tbUnitProkegTambahan = conn.Get<TbUnitProkegTambahan>(Data.IdUnitProkegTambahan);
                        TbCoa tbCoa = conn.Get<TbCoa>(Data.IdCoa);
                        if (tbUnitProkegTambahan == null) return "data tidak ada";

                        TbUnitProkeg tbUnitProkeg = conn.Get<TbUnitProkeg>(tbUnitProkegTambahan.IdUnitProkeg);
                        if (tbUnitProkeg == null) return "data program kegiatan tidak ada";
                        int[] statuss = new int[] { JenisStatusProgKegConstant.Initial, JenisStatusProgKegConstant.Baru };
                        if (!statuss.Contains(tbUnitProkeg.Status))
                            return string.Format("status data sudah {0}", JenisStatusProgKegConstant.Dict[tbUnitProkeg.Status]);

                        var cekData = (from a in conn.GetList<TbUnitProkegTambahanCoa>()
                                       join b in conn.GetList<TbUnitProkegTambahan>() on a.IdUnitProkegTambahan equals b.IdUnitProkegTambahan
                                       join d in conn.GetList<TbCoa>() on a.IdCoa equals d.IdCoa
                                       where a.IdCoa == Data.IdCoa
                                       & a.IdUnitProkegTambahan == Data.IdUnitProkegTambahan
                                       select new { a, b, d }).FirstOrDefault();
                        if (cekData != null)
                            return string.Format("kode akun {0} sudah ada untuk kegiatan {1}", cekData.d.Kode, cekData.b.NamaKegiatan);

                        double rpTotal = Data.JumlahSatuan * Data.JumlahVolume * Data.HargaSatuan;
                        string err = CekUnitCoaPagu(conn, tbUnitProkeg.IdUnit, tbUnitProkeg.IdTahunAjaran, tbCoa.IdCoa, rpTotal, 0, tbCoa.Kode);
                        if (!string.IsNullOrEmpty(err)) return err;


                        tbUnitProkegTambahan.Rp += rpTotal;
                        tbUnitProkeg.Rp += rpTotal;
                        TbUnitProkegTambahanCoa tbUnitProkegTambahanCoa = new TbUnitProkegTambahanCoa
                        {
                            IdCoa = Data.IdCoa,
                            Catatan = Data.Catatan,
                            HargaSatuan = Data.HargaSatuan,
                            IdJenisSatuan = Data.IdJenisSatuan,
                            IdJenisVolume = Data.IdJenisVolume,
                            IdUnitProkegTambahan = Data.IdUnitProkegTambahan,
                            JumlahSatuan = Data.JumlahSatuan,
                            JumlahVolume = Data.JumlahVolume
                        };
                        conn.Insert(tbUnitProkegTambahanCoa);
                        conn.Update(tbUnitProkegTambahan);
                        conn.Update(tbUnitProkeg);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "KegiatanTambahanCoaAdd", ex);
            }
        }
        public string KegiatanTambahanCoaEdit(int IdUser, KeuUnitProKegCoaTambahanEditModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    using (var tx = conn.BeginTransaction())
                    {
                        if (!hakAkses.CanEdit) return "tidak berhak merubah kegiatan";
                        TbUnitProkegTambahanCoa tbUnitProkegTambahanCoa = conn.Get<TbUnitProkegTambahanCoa>(Data.IdUnitProkegTambahanCoa);
                        if (tbUnitProkegTambahanCoa == null) return "data tidak ada";
                        TbCoa tbCoa = conn.Get<TbCoa>(tbUnitProkegTambahanCoa.IdCoa);
                        double rpTotalOld = tbUnitProkegTambahanCoa.JumlahSatuan * tbUnitProkegTambahanCoa.JumlahVolume * tbUnitProkegTambahanCoa.HargaSatuan;

                        TbUnitProkegTambahan tbUnitProkegTambahan = conn.Get<TbUnitProkegTambahan>(Data.IdUnitProkegTambahan);
                        if (tbUnitProkegTambahan == null) return "data tidak ada";

                        TbUnitProkeg tbUnitProkeg = conn.Get<TbUnitProkeg>(tbUnitProkegTambahan.IdUnitProkeg);
                        if (tbUnitProkeg == null) return "data program kegiatan tidak ada";
                        int[] statuss = new int[] { JenisStatusProgKegConstant.Initial, JenisStatusProgKegConstant.Baru };
                        if (!statuss.Contains(tbUnitProkeg.Status))
                            return string.Format("status data sudah {0}", JenisStatusProgKegConstant.Dict[tbUnitProkeg.Status]);

                        if (tbUnitProkegTambahanCoa.IdCoa != Data.IdCoa)
                            return "kode coa tidak sesuai";

                        double rpTotal = Data.JumlahSatuan * Data.JumlahVolume * Data.HargaSatuan;
                        string err = CekUnitCoaPagu(conn, tbUnitProkeg.IdUnit, tbUnitProkeg.IdTahunAjaran, tbCoa.IdCoa, rpTotal, rpTotalOld, tbCoa.Kode);
                        if (!string.IsNullOrEmpty(err)) return err;

                        tbUnitProkegTambahan.Rp -= rpTotalOld;
                        tbUnitProkeg.Rp -= rpTotalOld;

                        tbUnitProkegTambahan.Rp += rpTotal;
                        tbUnitProkeg.Rp += rpTotal;

                        tbUnitProkegTambahanCoa.IdCoa = Data.IdCoa;
                        tbUnitProkegTambahanCoa.Catatan = Data.Catatan;
                        tbUnitProkegTambahanCoa.HargaSatuan = Data.HargaSatuan;
                        tbUnitProkegTambahanCoa.IdJenisSatuan = Data.IdJenisSatuan;
                        tbUnitProkegTambahanCoa.IdJenisVolume = Data.IdJenisVolume;
                        tbUnitProkegTambahanCoa.JumlahSatuan = Data.JumlahSatuan;
                        tbUnitProkegTambahanCoa.JumlahVolume = Data.JumlahVolume;

                        conn.Update(tbUnitProkegTambahanCoa);
                        conn.Update(tbUnitProkegTambahan);
                        conn.Update(tbUnitProkeg);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "KegiatanTambahanCoaEdit", ex);
            }
        }
        public string KegiatanTambahanCoaDelete(int IdUser, int IdUnitProkegTambahanCoa)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    using (var tx = conn.BeginTransaction())
                    {
                        if (!hakAkses.CanDelete) return "tidak berhak menghapus data";
                        TbUnitProkegTambahanCoa tbUnitProkegTambahanCoa = conn.Get<TbUnitProkegTambahanCoa>(IdUnitProkegTambahanCoa);
                        if (tbUnitProkegTambahanCoa == null) return "data tidak ada";
                        double rpTotalOld = tbUnitProkegTambahanCoa.JumlahSatuan * tbUnitProkegTambahanCoa.JumlahVolume * tbUnitProkegTambahanCoa.HargaSatuan;

                        TbUnitProkegWajib tbUnitProkegTambahan = conn.Get<TbUnitProkegWajib>(tbUnitProkegTambahanCoa.IdUnitProkegTambahan);
                        if (tbUnitProkegTambahan == null) return "data tidak ada";

                        TbUnitProkeg tbUnitProkeg = conn.Get<TbUnitProkeg>(tbUnitProkegTambahan.IdUnitProkeg);
                        if (tbUnitProkeg == null) return "data program kegiatan tidak ada";
                        int[] statuss = new int[] { JenisStatusProgKegConstant.Initial, JenisStatusProgKegConstant.Baru };
                        if (!statuss.Contains(tbUnitProkeg.Status))
                            return string.Format("status data sudah {0}", JenisStatusProgKegConstant.Dict[tbUnitProkeg.Status]);


                        TbUnitCoaPagu tbUnitCoaPagu = (from a in conn.GetList<TbUnitCoaPagu>()
                                                       join b in conn.GetList<TbUnitCoaUsed>() on a.IdUnitCoaPagu equals b.IdUnitCoaPagu
                                                       where a.IdUnit == tbUnitProkeg.IdUnit
                                                       & a.IdTahunAjaran == tbUnitProkeg.IdTahunAjaran
                                                       & b.IdCoaProkeg == tbUnitProkegTambahanCoa.IdCoa
                                                       select a).FirstOrDefault();
                        tbUnitCoaPagu.RpPaguUsed -= rpTotalOld;
                        conn.Update(tbUnitCoaPagu);

                        tbUnitProkegTambahan.Rp -= rpTotalOld;
                        tbUnitProkeg.Rp -= rpTotalOld;

                        conn.Delete(tbUnitProkegTambahanCoa);
                        conn.Update(tbUnitProkegTambahan);
                        conn.Update(tbUnitProkeg);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "KegiatanTambahanCoaDelete", ex);
            }
        }
        public string ApproveByKaUnit(int IdUser, int IdUnitProkeg, int StatusProses, string Catatan)
        {
            return ApproveData(IdUser, IdUnitProkeg, StatusProses, Catatan, JenisStatusProgKegConstant.DisetujuiKaUnit);
        }
        public string ApproveByManUnit(int IdUser, int IdUnitProkeg, int StatusProses, string Catatan)
        {
            return ApproveData(IdUser, IdUnitProkeg, StatusProses, Catatan, JenisStatusProgKegConstant.DisetujuiManUnit);
        }
        public string ApproveByManKeu(int IdUser, int IdUnitProkeg, int StatusProses, string Catatan)
        {
            return ApproveData(IdUser, IdUnitProkeg, StatusProses, Catatan, JenisStatusProgKegConstant.DisetujuiManKeu);
        }
        public string ApproveByDirektur(int IdUser, int IdUnitProkeg, int StatusProses, string Catatan)
        {
            return ApproveData(IdUser, IdUnitProkeg, StatusProses, Catatan, JenisStatusProgKegConstant.DisetujuiDirektur);
        }
        public string SetSah(int IdUser, int IdUnitProkeg)
        {
            return ApproveData(IdUser, IdUnitProkeg, StatusProsesKeuanganConstant.New, string.Empty, JenisStatusProgKegConstant.Disahkan);
        }
        public KeuUnitProKegDownloadModel GetUnitDownload(int IdUser, int IdUnitProkeg, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbUnitProkeg = (from a in conn.GetList<TbUnitProkeg>()
                                  join b in conn.GetList<TbUnit>() on a.IdUnit equals b.IdUnit
                                  join c in conn.GetList<TbTahunAjaran>() on a.IdTahunAjaran equals c.IdTahunAjaran
                                  where a.IdUnitProkeg == IdUnitProkeg
                                  select new { a, b, c }).FirstOrDefault();
                    if (tbUnitProkeg == null) { oMessage = "data tidak ada"; return null; }

                    KeuUnitProKegDownloadModel ret = new KeuUnitProKegDownloadModel
                    {
                        NamaUnit = tbUnitProkeg.b.Nama,
                        TotalRp = tbUnitProkeg.a.Rp,
                        TahunAjaran = tbUnitProkeg.c.Nama,
                        KabKot = "Kota Bogor",
                        Kecamatan = "Tanah Sarel",
                        Provinsi = "Jawa Barat",
                        Status = JenisStatusProgKegConstant.Dict[tbUnitProkeg.a.Status],
                        Det = new List<KeuUnitProKegDetDownloadModel>()
                    };

                    //var dtProgramKelompoks = from a in conn.GetList<TbProgramKelompok>()
                    //                         join b in conn.GetList<TbProgram>() on a.IdProgramKelompok equals b.IdProgramKelompok
                    //                         join c in conn.GetList<TbProgramUnit>() on b.IdProgram equals c.IdProgram
                    //                         where c.IdUnit == tbUnitProkeg.b.IdUnit
                    //                         group a by a into ga
                    //                         select new {
                    //                             IdProgramKelompok = ga.Key.IdProgramKelompok,
                    //                             Kode = ga.Key.Kode,
                    //                             Nama = ga.Key.Nama
                    //                         };

                    var dtProgramKelompoks = conn.GetList<TbProgramKelompok>();
                    foreach (var itemProgramKelompok in dtProgramKelompoks)
                    {
                        KeuUnitProKegDetDownloadModel L1 = new KeuUnitProKegDetDownloadModel
                        {
                            Kode = itemProgramKelompok.Kode,
                            Nama = itemProgramKelompok.Nama,
                            TotalRp = 0,
                            Det = new List<KeuUnitProKegDetDownloadModel>()
                        };
                        var dtPrograms = from a in conn.GetList<TbUnitProkeg>()
                                         join b in conn.GetList<TbProgramUnit>() on a.IdUnit equals b.IdUnit
                                         join c in conn.GetList<TbProgram>() on b.IdProgram equals c.IdProgram
                                         where c.IdProgramKelompok == itemProgramKelompok.IdProgramKelompok
                                         & b.IdUnit == tbUnitProkeg.b.IdUnit
                                         select new { a, b, c };
                        if (dtPrograms != null & dtPrograms.Count() > 0)
                        {
                            foreach (var itemProgram in dtPrograms)
                            {
                                ret.TotalRp = itemProgram.a.Rp;
                                KeuUnitProKegDetDownloadModel L2 = new KeuUnitProKegDetDownloadModel
                                {
                                    Kode = itemProgram.c.Kode,
                                    Nama = itemProgram.c.Nama,
                                    TotalRp = 0,
                                    Det = new List<KeuUnitProKegDetDownloadModel>()
                                };

                                var dtKegiatanWajibs = from a in conn.GetList<TbUnitProkegWajib>()
                                                       join b in conn.GetList<TbProgramKegiatan>() on a.IdKegiatan equals b.IdKegiatan
                                                       where a.IdUnitProkeg == itemProgram.a.IdUnitProkeg
                                                       & b.IdProgram == itemProgram.c.IdProgram
                                                       select new { a, b};
                                if(dtKegiatanWajibs!=null && dtKegiatanWajibs.Count() > 0)
                                {
                                    foreach (var itemKegiatanWajib in dtKegiatanWajibs)
                                    {
                                        KeuUnitProKegDetDownloadModel L3 = new KeuUnitProKegDetDownloadModel
                                        {
                                            Kode = itemKegiatanWajib.b.Kode,
                                            Nama = itemKegiatanWajib.b.Nama,
                                            TotalRp = itemKegiatanWajib.a.Rp,
                                            Tanggal = itemKegiatanWajib.a.Tanggal.ToString("dd-MM-yyyy"),
                                            Det = new List<KeuUnitProKegDetDownloadModel>()
                                        };
                                        L2.TotalRp += L3.TotalRp;
                                        var dtKegiatanWajibCoas = from a in conn.GetList<TbUnitProkegWajibCoa>()
                                                                  join b in conn.GetList<TbCoa>() on a.IdCoa equals b.IdCoa
                                                                  join c in conn.GetList<TbJenisSatuan>() on a.IdJenisSatuan equals c.IdJenisSatuan
                                                                  join d in conn.GetList<TbJenisVolume>() on a.IdJenisVolume equals d.IdJenisVolume
                                                                  where a.IdUnitProkegWajib == itemKegiatanWajib.a.IdUnitProkegWajib
                                                                  select new { a, b, c, d };
                                        if(dtKegiatanWajibCoas!=null && dtKegiatanWajibCoas.Count()>0)
                                        {
                                            foreach (var itemKegiatanWajibCoa in dtKegiatanWajibCoas)
                                            {
                                                KeuUnitProKegDetDownloadModel L4 = new KeuUnitProKegDetDownloadModel
                                                {
                                                    Kode = itemKegiatanWajibCoa.b.Kode,
                                                    Nama = itemKegiatanWajibCoa.b.Nama,
                                                    Catatan = itemKegiatanWajibCoa.a.Catatan,
                                                    HargaSatuan = itemKegiatanWajibCoa.a.HargaSatuan,
                                                    IdCoa = itemKegiatanWajibCoa.b.IdCoa,
                                                    IdParentCoa = itemKegiatanWajibCoa.b.IdParentCoa,
                                                    JumlahSatuan = itemKegiatanWajibCoa.a.JumlahSatuan,
                                                    JumlahVolume = itemKegiatanWajibCoa.a.JumlahVolume,
                                                    Satuan = itemKegiatanWajibCoa.c.Nama,
                                                    TotalRp = itemKegiatanWajibCoa.a.JumlahSatuan * itemKegiatanWajibCoa.a.JumlahVolume * itemKegiatanWajibCoa.a.HargaSatuan,
                                                    Volume = itemKegiatanWajibCoa.d.Nama,
                                                    Det = new List<KeuUnitProKegDetDownloadModel>()
                                                };

                                                L3.Det.Add(L4);
                                            }
                                        }
                                        L2.Det.Add(L3);
                                    }
                                }

                                var dtKegiatanTambahans = from a in conn.GetList<TbUnitProkegTambahan>()
                                                       where a.IdUnitProkeg == itemProgram.a.IdUnitProkeg
                                                       & a.IdProgram == itemProgram.c.IdProgram
                                                       select new { a};
                                if (dtKegiatanTambahans != null && dtKegiatanTambahans.Count() > 0)
                                {
                                    foreach (var itemKegiatanTambahan in dtKegiatanTambahans)
                                    {
                                        KeuUnitProKegDetDownloadModel L3 = new KeuUnitProKegDetDownloadModel
                                        {
                                            Kode = "-",
                                            Nama = itemKegiatanTambahan.a.NamaKegiatan,
                                            Tanggal = itemKegiatanTambahan.a.Tanggal.ToString("dd-MM-yyyy"),
                                            TotalRp = itemKegiatanTambahan.a.Rp,
                                            Det = new List<KeuUnitProKegDetDownloadModel>()
                                        };
                                        L2.TotalRp += L3.TotalRp;
                                        var dtKegiatanTambahanCoas = from a in conn.GetList<TbUnitProkegTambahanCoa>()
                                                                  join b in conn.GetList<TbCoa>() on a.IdCoa equals b.IdCoa
                                                                  join c in conn.GetList<TbJenisSatuan>() on a.IdJenisSatuan equals c.IdJenisSatuan
                                                                  join d in conn.GetList<TbJenisVolume>() on a.IdJenisVolume equals d.IdJenisVolume
                                                                  where a.IdUnitProkegTambahan == itemKegiatanTambahan.a.IdUnitProkegTambahan
                                                                  select new { a, b, c, d };
                                        if (dtKegiatanTambahanCoas != null && dtKegiatanTambahanCoas.Count() > 0)
                                        {
                                            foreach (var itemKegiatanTambahanCoa in dtKegiatanTambahanCoas)
                                            {
                                                KeuUnitProKegDetDownloadModel L4 = new KeuUnitProKegDetDownloadModel
                                                {
                                                    Kode = itemKegiatanTambahanCoa.b.Kode,
                                                    Nama = itemKegiatanTambahanCoa.b.Nama,
                                                    Catatan = itemKegiatanTambahanCoa.a.Catatan,
                                                    HargaSatuan = itemKegiatanTambahanCoa.a.HargaSatuan,
                                                    IdCoa = itemKegiatanTambahanCoa.b.IdCoa,
                                                    IdParentCoa = itemKegiatanTambahanCoa.b.IdParentCoa,
                                                    JumlahSatuan = itemKegiatanTambahanCoa.a.JumlahSatuan,
                                                    JumlahVolume = itemKegiatanTambahanCoa.a.JumlahVolume,
                                                    Satuan = itemKegiatanTambahanCoa.c.Nama,
                                                    TotalRp = itemKegiatanTambahanCoa.a.JumlahSatuan * itemKegiatanTambahanCoa.a.JumlahVolume * itemKegiatanTambahanCoa.a.HargaSatuan,
                                                    Volume = itemKegiatanTambahanCoa.d.Nama,
                                                    Det = new List<KeuUnitProKegDetDownloadModel>()
                                                };
                                                L3.Det.Add(L4);
                                            }
                                        }
                                        L2.Det.Add(L3);
                                    }
                                }
                                L1.TotalRp += L2.TotalRp;
                                L1.Det.Add(L2);
                            }
                        }
                        ret.Det.Add(L1);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetUnitDownload", ex);
                return null;
            }
        }
    }
}
