﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Dapper;
using Microsoft.AspNetCore.Http;
using Swg.Entities.Kbm;
using Swg.Entities.Lmg;
using Swg.Entities.Ppdb;
using Swg.Entities.Public;
using Swg.Entities.Ref;
using Swg.Models;
using Swg.Models.Constants;
using Swg.Sch.Shared.Constants;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;

namespace Swg.Sch.Services
{
    public class SchService : ISchService
    {

        private string _languageCode = "id";
        private readonly string ServiceName = "Swg.Sch.Services.SchService.";
        private readonly ICommonService commonService;
        private readonly IUserAppService userAppService;
        private readonly IDateTimeService dateTimeService;
        private readonly IPdfService pdfService;
        private readonly IEmailService emailService;
        private PpdbModelRead currPpdb = new PpdbModelRead();

        public SchService(
            ICommonService CommonService,
            IDateTimeService DateTimeService,
            IUserAppService UserAppService,
            IPdfService PdfService,
            IEmailService EmailService)
        {
            commonService = CommonService;
            userAppService = UserAppService;
            dateTimeService = DateTimeService;
            pdfService = PdfService;
            emailService = EmailService;
            this.SetCurrentPPdb();

        }
        private void SetCurrentPPdb()
        {
            var ppdb = GetPpdb(out string err);
            if (string.IsNullOrEmpty(err))
                currPpdb = ppdb;
        }
        private string ValidateDataSiswa(PpdbSiswaAddModel Data)
        {
            string err = string.Empty;

            //Data.Nisn
            if (string.IsNullOrEmpty(Data.Nama))
                err += "nama harus diisi;" + Environment.NewLine;
            if (string.IsNullOrEmpty(Data.NamaPanggilan))
                err += "nama panggilan harus diisi;" + Environment.NewLine;
            if (string.IsNullOrEmpty(Data.TempatLahir))
                err += "tempat lahir harus diisi;" + Environment.NewLine;

            if (!commonService.DictCheck(AgamaConstant.Dict, Data.IdAgama))
                err += "agama harus dipilih;" + Environment.NewLine;
            if (!commonService.DictCheck(JenisWargaNegaraConstant.Dict, Data.IdKewarganegaraan))
                err += "kewarganegaraan harus dipilh;" + Environment.NewLine;
            if (!commonService.DictCheck(JenisKelaminConstant.Dict, Data.KdJenisKelamin))
                err += "jenis kelamin harus dipilih;" + Environment.NewLine;
            if (!commonService.DictCheck(JenisGolonganDarahConstant.Dict, Data.KdGolonganDarah))
                err += "golongan darah tidak sesuai;" + Environment.NewLine;
            if (string.IsNullOrEmpty(Data.AlamatTinggal))
                err += "alamat tinggal harus diisi;" + Environment.NewLine;
            if (!commonService.DictCheck(JenisTinggalSiswaConstant.DictJenisTinggalSiswa, Data.IdTinggal))
                err += "jenis tinggal harus dipilih;" + Environment.NewLine;
            if (string.IsNullOrEmpty(Data.Email))
                err += "email harus diisi;" + Environment.NewLine;
            if (string.IsNullOrEmpty(Data.NoHandphone))
                err += "no handphone harus diis;" + Environment.NewLine;
            if (string.IsNullOrEmpty(Data.NoDarurat))
                err += "no darurat harus diisi;" + Environment.NewLine;
            if (!commonService.DictCheck(JenisBahasaConstant.DictJenisBahasa, Data.IdBahasa))
                err += "bahasa harus dipilih";
            if (Data.TinggiBadan <= 0)
                err += "tinggi badan salah;" + Environment.NewLine;
            if (Data.BeratBadan <= 0)
                err += "berat badan salah;" + Environment.NewLine;
            if (Data.AnakKe <= 0 || Data.AnakKe > 20)
                err += "isian anak ke salah;" + Environment.NewLine;
            if (Data.JumlahSodaraKandung < 0)
                err += "isian sodara kandung salah;" + Environment.NewLine;
            if (Data.JumlahSodaraTiri < 0)
                err += "isian sodara tiri salah;" + Environment.NewLine;
            if (Data.JumlahSodaraAngkat < 0)
                err += "isian sodara angkat salah;" + Environment.NewLine;
            if (!commonService.DictCheck(JenisTransportasiConstant.DictJenisTransportasi, Data.IdTransportasi))
                err += "jenis transportasi harus dipilih";
            if (Data.JarakKeSekolah <= 0)
                err += "isian jarak ke sekolah salah;" + Environment.NewLine;
            if (Data.WaktuTempuh <= 0)
                err += "isian waktu tempuh salah;" + Environment.NewLine;
            //Data.PenyakitDiderita
            //Data.KelainanJasmani
            //Data.SekolahAsal
            //Data.AlamatSekolahAsal
            //Data.NspnSekolahAsal
            if (Data.IdPpdbSeragam == 0)
                err += "segaram harus dipilih;" + Environment.NewLine;
            if (Data.NoHandphone.Length > 13)
                err += "nomor hp maksimal 13 angka;" + Environment.NewLine;
            if (Data.NoDarurat.Length > 20)
                err += "nomor darurat maksimal 20 angka;" + Environment.NewLine;

            return err;
        }
        private string ValidateDataOrtus(List<PpdbSiswaOrtuAddModel> Data)
        {
            string err = string.Empty;
            if (Data == null || Data.Count() == 0)
                err += "data orang tua harus diisi;" + Environment.NewLine;
            bool isAyah = false;
            bool isIbu = false;
            foreach (var item in Data)
            {
                string tipe = "ibu";
                switch (item.IdTipe)
                {
                    case TipeOrtuSiswaConstant.Ibu:
                        isIbu = true;
                        break;
                    case TipeOrtuSiswaConstant.Ayah:
                        isAyah = true;
                        break;
                        tipe = "ayah";
                    case TipeOrtuSiswaConstant.Wali:
                        tipe = "wali";
                        break;
                    default:
                        break;
                }
                if (string.IsNullOrEmpty(item.Nama))
                    err += string.Format("nama {0} harus diisi;", tipe) + Environment.NewLine;
                if (string.IsNullOrEmpty(item.Alamat))
                    err += string.Format("Alamat {0} harus diisi;", tipe) + Environment.NewLine;
                if (string.IsNullOrEmpty(item.Nik))
                    err += string.Format("Nik {0} harus diisi;", tipe) + Environment.NewLine;
                if (item.Nik.Length != 16)
                    err += string.Format("panjang Nik {0} harus 16 karakter;", tipe) + Environment.NewLine;
                if (item.IdStatusHidup == 1) //meninggal
                {
                    // if (item.IdJenisPekerjaan == 0)
                    //     err += string.Format("Pekerjaan {0} harus diisi;", tipe) + Environment.NewLine;
                    if (string.IsNullOrEmpty(item.NamaInstansi))
                        err += string.Format("Nama instansi {0} harus diisi;", tipe) + Environment.NewLine;
                    if (string.IsNullOrEmpty(item.Jabatan))
                        err += string.Format("Jabatan {0} harus diisi;", tipe) + Environment.NewLine;
                    if (string.IsNullOrEmpty(item.Email))
                        err += string.Format("Email {0} harus diisi;", tipe) + Environment.NewLine;
                    if (string.IsNullOrEmpty(item.NoHandphone))
                        err += string.Format("NoHandphone {0} harus diisi;", tipe) + Environment.NewLine;
                    if (string.IsNullOrEmpty(item.NoTelpRumah))
                        err += string.Format("NoTelpRumah {0} harus diisi;", tipe) + Environment.NewLine;
                    if (!commonService.DictCheck(AgamaConstant.Dict, item.IdAgama))
                        err += string.Format("agama {0} harus dipilih;", tipe) + Environment.NewLine;
                    if (!commonService.DictCheck(JenisWargaNegaraConstant.Dict, item.IdKewarganegaraan))
                        err += string.Format("kewarganegaraan {0} harus dipilih;", tipe) + Environment.NewLine;
                    // if (item.IdJenjangPendidikan == 0)
                    //     err += string.Format("jenjang pendidikan {0} harus dipilih;", tipe) + Environment.NewLine;
                    // if (item.IdJenisPekerjaan == 0)
                    //     err += string.Format("pekerjaan {0} harus dipilih;", tipe) + Environment.NewLine;
                    if (!commonService.DictCheck(JenisStatusNikahConstant.Dict, item.IdStatusPenikahan))
                        err += string.Format("status pernikahan {0} harus dipilih;", tipe) + Environment.NewLine;
                    if (string.IsNullOrEmpty(item.TempatLahir))
                        err += string.Format("Tempat Lahit {0} harus diisi;", tipe) + Environment.NewLine;
                    var tgl = dateTimeService.StrToDateTime(item.TanggalLahir, out string oMessage);
                    if (tgl == null)
                        err += string.Format("format tanggal {0} lahir harus dd-mm-yyyy;", tipe) + Environment.NewLine;
                }
                else if (item.IdStatusHidup == 0)
                {
                    var tgl = dateTimeService.StrToDateTime(item.TanggalMeninggal, out string oMessage);
                    if (tgl == null)
                        err += string.Format("format tanggal {0} meninggal harus dd-mm-yyyy;", tipe) + Environment.NewLine;
                }
                else
                {
                    err += string.Format("status hidup {0} harus dipilih;", tipe) + Environment.NewLine;
                }
            }
            if (!isAyah)
                err += "data ayah harus diisi;" + Environment.NewLine;
            if (!isIbu)
                err += "data ibu harus diisi;" + Environment.NewLine;
            return err;
        }
        public string SetInitialData(int IdUser, ExcelSekolahModel Data)
        {
            string errdata = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        //sekolah
                        Console.WriteLine("proses sekolah");
                        errdata = string.Format("tbSekolah {0} {1}", Data.Nama, Data.Nama);
                        TbSekolah tbSekolah = new TbSekolah
                        {
                            IdSekolah = 1,
                            Alamat = Data.Alamat,
                            Desa = Data.Desa,
                            Email = Data.Email,
                            Kabupaten = Data.Kabupaten,
                            Kecamatan = Data.Kecamatan,
                            KodePos = Data.KodePos,
                            Moto = Data.Moto,
                            Nama = Data.Nama,
                            NamaSingkat = Data.NamaSingkat,
                            NoFaximili = Data.NoFaximili,
                            NoTelepon = Data.NoTelepon,
                            Provinsi = Data.Provinsi,
                            Web = Data.Web,
                            FotoWeb = "fotoweb.png",
                            Icon = "icon.png",
                            Logo = "logo.png",
                            VideoWeb = "video.mp4"
                        };
                        conn.Insert(tbSekolah);

                        //unit
                        Console.WriteLine("proses unit");
                        foreach (var unit in Data.XlsUnits)
                        {
                            errdata = string.Format("tbUnit {0} {1}", unit.Nama, unit.Nama);
                            int idUnit = 0;
                            switch (unit.NamaSingkat)
                            {
                                case "TK":
                                    idUnit = 1;
                                    break;
                                case "SD":
                                    idUnit = 2;
                                    break;
                                case "SMP":
                                    idUnit = 3;
                                    break;
                                case "SMA":
                                    idUnit = 4;
                                    break;
                                default:
                                    break;
                            }
                            TbUnit tbUnit = new TbUnit
                            {
                                IdUnit = idUnit,
                                IdSekolah = tbSekolah.IdSekolah,
                                Alamat = unit.Alamat,
                                BandwidthInternet = unit.BandwidthInternet,
                                DayaListrik = unit.DayaListrik,
                                Desa = unit.Desa,
                                Email = unit.Email,
                                JenisAkreditasi = unit.JenisAkreditasi,
                                Kabupaten = unit.Kabupaten,
                                Kecamatan = unit.Kecamatan,
                                KodePos = unit.KodePos,
                                Kurikulum = unit.Kurikulum,
                                LuasBangunan = unit.LuasBangunan,
                                LuasTanah = unit.LuasTanah,
                                Moto = unit.Moto,
                                Nama = unit.Nama,
                                NamaSingkat = unit.NamaSingkat,
                                NamaYayasan = unit.NamaYayasan,
                                Naungan = unit.Naungan,
                                NoFaximili = unit.NoFaximili,
                                NoSertifikatIso = unit.NoSertifikatIso,
                                NoSkAkreditasi = unit.NoSkAkreditasi,
                                NoSkOperasional = unit.NoSkOperasional,
                                NoSkPendirian = unit.NoSkPendirian,
                                NoTelepon = unit.NoTelepon,
                                Nspn = unit.Nspn,
                                ProviderInternet = unit.ProviderInternet,
                                Provinsi = unit.Provinsi,
                                StatusUnit = unit.StatusUnit,
                                SumberListrik = unit.SumberListrik,
                                WaktuPenyelenggaraan = unit.WaktuPenyelenggaraan,
                                Web = unit.Web,
                                Logo = "logo.png"
                            };
                            string strTanggal = "01-01-1900";
                            if (!string.IsNullOrEmpty(unit.TanggalSkAkreditasi))
                                strTanggal = unit.TanggalSkAkreditasi;
                            tbUnit.TanggalSkAkreditasi = dateTimeService.StrToDateTime(strTanggal);
                            if (!string.IsNullOrEmpty(unit.TanggalSkOperasional))
                                strTanggal = unit.TanggalSkOperasional;
                            tbUnit.TanggalSkOperasional = dateTimeService.StrToDateTime(strTanggal);
                            if (!string.IsNullOrEmpty(unit.TanggalSkPendirian))
                                strTanggal = unit.TanggalSkPendirian;
                            tbUnit.TanggalSkPendirian = dateTimeService.StrToDateTime(strTanggal);

                            conn.Insert(tbUnit);
                        }

                        //pegawai
                        int i = 2;
                        Console.WriteLine("proses pegawai");
                        foreach (var item in Data.XlsPegawais)
                        {
                            errdata = string.Format("tbPegawai {0} {1}", item.Nama, item.Nip);

                            TbUser tbUser = new TbUser
                            {
                                Status = StatusDataConstant.Aktif,
                                CreatedDate = DateTime.Now,
                                Email = item.Email,
                                Password = "",
                                Username = ""
                            };
                            string[] namas = item.Nama.Split(" ");
                            string _firstName = namas[0];
                            string _midleName = string.Empty;
                            string _lastName = string.Empty;
                            if (namas.Length > 1)
                                _midleName = namas[1];
                            if (namas.Length > 2)
                                for (int x = 2; x < namas.Length; x++)
                                {
                                    _lastName += namas[x] + " ";
                                }
                            _lastName = _lastName.Trim();

                            tbUser.Username = _firstName.Replace("'", "");
                            tbUser.Username += i.ToString().PadLeft(3, '0');
                            tbUser.Password = commonService.EncryptString(tbUser.Username);
                            if (string.IsNullOrEmpty(item.Email))
                                item.Email = string.Format("{0}@email.com", tbUser.Username);
                            tbUser.Email = item.Email;
                            errdata = string.Format("tbUser {0} {1}", tbUser.IdUser, tbUser.Username);
                            tbUser.Address = item.Alamat;
                            tbUser.ProfileFile = string.Format("{0}.png", tbUser.Username);
                            tbUser.FirstName = _firstName;
                            tbUser.LastName = _midleName;
                            tbUser.MiddleName = _lastName;
                            tbUser.MobileNumber = item.NoHandphone;
                            tbUser.PhoneNumber = item.NoTelpon;
                            tbUser.MobileNumber = commonService.SetNoHandphone(item.NoHandphone);
                            tbUser.PhoneNumber = commonService.SetNoHandphone(item.NoTelpon);

                            var idUser = conn.Insert(tbUser);

                            TbUserRole tbUserRole = new TbUserRole
                            {
                                IdUser = (int)idUser,
                                IdRole = 1
                            };
                            conn.Insert(tbUserRole);

                            TbPegawai tbPegawai = new TbPegawai
                            {
                                IdPegawai = (int)idUser,
                                CreatedBy = 1,
                                CreatedDate = dateTimeService.GetCurrdate(),
                                IdAgama = AgamaConstant.Islam,
                                IdJenisJabatan = 0,
                                IdJenisPegawai = JenisPegawaiConstant.Tetap,
                                IdJenisJenjangPendidikan = 1,
                                KdJenisKelamin = item.JenisKelamin,
                                NamaInstitusiPendidikan = item.NamaInstitusiPendidikan,
                                NamaPasangan = item.NamaPasangan,
                                Nik = item.Nik,
                                Nip = item.Nip,
                                NoDarurat = item.NoDarurat,
                                Status = StatusDataConstant.Aktif,
                                TanggalLahir = dateTimeService.StrToDateTime(item.TanggalLahir),
                                TempatLahir = item.TempatLahir,
                                IdGolonganPegawai = JenisPegawaiGolonganConstant.Gol0,
                                TanggalMasuk = new DateTime(1900, 1, 1)
                            };
                            tbPegawai.NoDarurat = commonService.SetNoHandphone(item.NoDarurat);

                            if (string.IsNullOrEmpty(tbPegawai.Nik)) tbPegawai.Nik = tbPegawai.Nip;
                            if (tbPegawai.Nik.Length != 16) tbPegawai.Nik = tbPegawai.Nip;

                            tbPegawai.KdJenisKelamin = (from a in JenisKelaminConstant.Dict
                                                        where a.Key == item.JenisKelamin
                                                        select a).FirstOrDefault().Key;

                            tbPegawai.IdJenisJabatan = (from a in conn.GetList<TbJenisJabatan>()
                                                        where a.NamaSingkat == item.Jabatan.Trim()
                                                        select a).FirstOrDefault().IdJenisJabatan;

                            tbPegawai.IdJenisJenjangPendidikan = (from a in conn.GetList<TbJenisJenjangPendidikan>()
                                                                  where a.NamaSingkat == item.Pendidikan.Trim()
                                                                  select a).FirstOrDefault().IdJenisJenjangPendidikan;
                            tbPegawai.IdGolonganPegawai = (from a in JenisPegawaiGolonganConstant.DictJenisPegawaiGolongan
                                                           where a.Value == item.Golongan
                                                           select a).FirstOrDefault().Key;
                            tbPegawai.TanggalMasuk = dateTimeService.StrToDateTime(item.TanggalMasuk.Replace("/", "-"));
                            tbPegawai.IdJenisPegawai = (from a in JenisPegawaiConstant.DictJenisPegawai
                                                        where a.Value == item.Status
                                                        select a).FirstOrDefault().Key;
                            conn.Insert(tbPegawai);

                            var idUnit = (from a in conn.GetList<TbUnit>()
                                          where a.NamaSingkat == item.Unit.Trim()
                                          select a).FirstOrDefault().IdUnit;
                            TbUnitPegawai tbUnitPegawai = new TbUnitPegawai
                            {
                                IdUnit = idUnit,
                                IdPegawai = tbPegawai.IdPegawai
                            };
                            conn.Insert(tbUnitPegawai);

                            i++;
                        }

                        //kelas
                        Console.WriteLine("proses kelas");
                        var kelass = Data.XlsKelas
                            .GroupBy(p => new { p.Unit, p.Kelas })
                            .Select(g => g.First()
                            ).ToList();
                        i = 1;
                        foreach (var kelas in kelass)
                        {
                            errdata = string.Format("tbKelas {0} {1}", kelas.Unit, kelas.Kelas);
                            var idUnit = (from a in conn.GetList<TbUnit>()
                                          where a.NamaSingkat == kelas.Unit
                                          select a.IdUnit).FirstOrDefault();

                            TbKelas tbKelas = new TbKelas
                            {
                                IdKelas = idUnit * 100 + i,
                                IdUnit = idUnit,
                                Nama = kelas.Kelas,
                                NamaSingkat = kelas.Kelas,
                            };
                            i++;
                            conn.Insert(tbKelas);
                        }
                        //kelas paralel
                        Console.WriteLine("proses kelas paralel");
                        i = 1;
                        foreach (var kelasParalel in Data.XlsKelas)
                        {
                            errdata = string.Format("tbKelasParalel {0} {1}", kelasParalel.Kelas, kelasParalel.KelasParalel);
                            var idKelas = (from a in conn.GetList<TbKelas>()
                                           where a.NamaSingkat == kelasParalel.Kelas
                                           select a.IdKelas).FirstOrDefault();
                            TbKelasParalel tbKelasParalel = new TbKelasParalel
                            {
                                IdKelas = idKelas,
                                IdKelasParalel = idKelas * 100 + i,
                                Nama = kelasParalel.KelasParalel,
                                NamaSingkat = kelasParalel.KelasParalel
                            };
                            i++;
                            conn.Insert(tbKelasParalel);
                        }
                        //siswa & rombel
                        Console.WriteLine("proses siswa dan rombel");
                        i = 1;
                        foreach (var item in Data.XlsSiswas)
                        {
                            errdata = string.Format("tbSiswa {0} {1}", item.Nis, item.Nama);
                            TbSiswa tbSiswa = new TbSiswa();
                            tbSiswa.IdSiswa = i;
                            i++;
                            tbSiswa.Nis = item.Nis;
                            tbSiswa.NisLama = item.Nis;
                            if (string.IsNullOrEmpty(tbSiswa.Nis.Trim())) tbSiswa.Nis = tbSiswa.IdSiswa.ToString().PadLeft(5, '0'); tbSiswa.NisLama = tbSiswa.IdSiswa.ToString().PadLeft(5, '0');

                            tbSiswa.Password = commonService.EncryptString(tbSiswa.Nis);
                            if (string.IsNullOrEmpty(item.Nisn)) item.Nisn = item.Nis;
                            if (item.Nisn.Length < 5) item.Nisn = item.Nis;
                            tbSiswa.Nisn = item.Nisn;
                            tbSiswa.Nik = item.Nik;
                            if (string.IsNullOrEmpty(tbSiswa.Nik)) tbSiswa.Nik = tbSiswa.Nis;
                            if (tbSiswa.Nik.Length != 16) tbSiswa.Nik = tbSiswa.Nis;

                            tbSiswa.Nama = item.Nama;
                            tbSiswa.NamaPanggilan = item.NamaPanggilan;
                            tbSiswa.TempatLahir = item.TempatLahir;
                            if (string.IsNullOrEmpty(tbSiswa.TempatLahir.Trim())) tbSiswa.TempatLahir = "-";
                            try
                            {
                                tbSiswa.TanggalLahir = dateTimeService.StrToDateTime(item.TanggalLahir);
                            }
                            catch (Exception)
                            {
                                tbSiswa.TanggalLahir = new DateTime(1900, 1, 1);
                            }

                            var idAgama = (from a in AgamaConstant.Dict
                                           where a.Value == item.Agama
                                           select a.Key).FirstOrDefault();
                            if (idAgama != 0)
                            {
                                tbSiswa.IdAgama = idAgama;
                            }
                            else
                            {
                                return string.Format("pilihan agama {0} siswa {1} tidak sesuai", item.Agama, item.Nama);
                            }

                            var kdJenisKelamin = (from a in JenisKelaminConstant.Dict
                                                  where a.Key.ToLower() == item.JenisKelamin.Trim().ToLower()
                                                  select a.Key).FirstOrDefault();
                            tbSiswa.KdJenisKelamin = kdJenisKelamin;

                            tbSiswa.NoHandphone = commonService.SetNoHandphone(item.NoHandphone);

                            tbSiswa.NoDarurat = commonService.SetNoHandphone(item.NoDarurat);
                            tbSiswa.Email = item.Email;
                            tbSiswa.AlamatTinggal = item.AlamatTinggal;
                            tbSiswa.AlamatOrtu = item.AlamatOrtu;
                            tbSiswa.NikIbu = item.NikIbu;
                            tbSiswa.NamaIbu = item.NamaIbu;

                            var jenisPekerjaanIbu = (from a in conn.GetList<TbJenisPekerjaan>()
                                                     where a.Nama == item.JenisPekerjaanIbu || a.NamaSingkat == item.JenisPekerjaanIbu
                                                     select a).FirstOrDefault();
                            if (jenisPekerjaanIbu != null)
                            {
                                tbSiswa.IdJenisPekerjaanIbu = jenisPekerjaanIbu.IdJenisPekerjaan;
                            }
                            else
                            {
                                tbSiswa.IdJenisPekerjaanIbu = 0;
                            }
                            tbSiswa.NamaInstansiIbu = item.NamaInstansiIbu;
                            tbSiswa.NikAyah = item.NikAyah;
                            tbSiswa.NamaAyah = item.NamaAyah;
                            var jenisPekerjaanAyah = (from a in conn.GetList<TbJenisPekerjaan>()
                                                      where a.Nama == item.JenisPekerjaanAyah || a.NamaSingkat == item.JenisPekerjaanAyah
                                                      select a).FirstOrDefault();
                            if (jenisPekerjaanAyah != null)
                            {
                                tbSiswa.IdJenisPekerjaanAyah = jenisPekerjaanAyah.IdJenisPekerjaan;
                            }
                            else
                            {
                                tbSiswa.IdJenisPekerjaanAyah = 0;
                            }
                            tbSiswa.NamaInstansiAyah = item.NamaInstansiAyah;
                            tbSiswa.EmailOrtu = item.EmailOrtu;
                            tbSiswa.Status = StatusDataConstant.Aktif;
                            tbSiswa.CreatedBy = IdUser;
                            tbSiswa.CreatedDate = dateTimeService.GetCurrdate();

                            var kelasParalel = (from a in conn.GetList<TbKelasParalel>()
                                                where a.Nama.ToLower().Trim().Replace(" ", "").Replace("-", "") == item.KelasParalel.ToLower().Trim().Replace(" ", "").Replace("-", "")
                                                select a).FirstOrDefault();
                            if (kelasParalel != null)
                            {
                                TbKelasRombel tbKelasRombel = new TbKelasRombel
                                {
                                    IdSiswa = tbSiswa.IdSiswa,
                                    IdKelasParalel = kelasParalel.IdKelasParalel
                                };
                                conn.Insert(tbSiswa);
                                conn.Insert(tbKelasRombel);

                            }
                            else
                            {
                                return string.Format("kelas paralel {0} siswa {1} tidak sesuai", item.KelasParalel, item.Nama);
                            }

                        }
                        //mapel & jadwal
                        Console.WriteLine("proses mapel dan jadwal");

                        int idMapelTemp = 0;
                        int idMapel = 1;
                        int idPegawai = (from a in conn.GetList<TbUser>()
                                         where a.FirstName.ToLower().Trim() == "Irma Dewiyana".ToLower().Trim()
                                         select a.IdUser).FirstOrDefault();
                        foreach (var item in Data.XlsKelasMapels)
                        {
                            //split kelas paralel
                            if (item.KelasParalel != null)
                            {
                                string[] arrKelasParalels = item.KelasParalel.Split(";");
                                string[] arrNamaGurus = item.NamaGuru.Split(";");

                                int iguru = 0;
                                foreach (var kelasParalel in arrKelasParalels)
                                {
                                    var namaGuru = string.Empty;
                                    if (arrNamaGurus.Length == 1)
                                    {
                                        namaGuru = arrNamaGurus[0];
                                    }
                                    else
                                    {
                                        namaGuru = arrNamaGurus[iguru];
                                    }


                                    errdata = string.Format("tbMapel {0} {1}", idMapelTemp, item.NamaMapel);
                                    var tbMapel = (from a in conn.GetList<TbMapel>()
                                                   where a.Nama.Trim() == item.NamaMapel.Trim()
                                                   select a).FirstOrDefault();
                                    if (tbMapel == null)
                                    {
                                        idMapelTemp++;
                                        tbMapel = new TbMapel
                                        {
                                            IdMapel = idMapelTemp,
                                            Kode = idMapelTemp.ToString().PadLeft(3, '0'),
                                            Nama = item.NamaMapel.Trim()
                                        };
                                        idMapel = idMapelTemp;
                                        conn.Insert(tbMapel);
                                    }
                                    else
                                    {
                                        idMapel = tbMapel.IdMapel;
                                    }

                                    //errdata = string.Format("SetKbmJadwal {0} {1}, {2}, {3}", item.Hari, item.JamMulai, item.JamSelesai);
                                    int idKbmJadwal = InsertKbmJadwal(conn, item.Hari, item.JamMulai, item.JamSelesai, out string err);
                                    if (!string.IsNullOrEmpty(err))
                                        return string.Format("SetKbmJadwal {0} {1}, {2}: {3}", item.Hari, item.JamMulai, item.JamSelesai, err);


                                    //kbm
                                    var pegawais = from a in conn.GetList<TbUser>()
                                                   where a.FirstName.ToLower().Trim() == namaGuru.ToLower().Trim()
                                                   select a;
                                    if (pegawais != null)
                                    {
                                        if (pegawais.Count() == 1)
                                            idPegawai = pegawais.SingleOrDefault().IdUser;
                                    }

                                    var kelasParalels = from a in conn.GetList<TbKelasParalel>()
                                                        where a.Nama.ToLower().Trim().Replace(" ", "").Replace("-", "") == kelasParalel.ToLower().Trim().Replace(" ", "").Replace("-", "")
                                                        select a;
                                    if (kelasParalels == null)
                                        return string.Format("jadwal kelas paralel {0} tidak ada", kelasParalel);
                                    if (kelasParalels.Count() > 1)
                                        return string.Format("kelas paralel {0} lebih dari 1", kelasParalel);
                                    if (kelasParalels.Count() == 0)
                                        return string.Format("kelas paralel {0} tidak ada", kelasParalel);

                                    errdata = string.Format("kelasParalels {0} {1}", kelasParalel, namaGuru);
                                    int idKelasParalel = kelasParalels.SingleOrDefault().IdKelasParalel;

                                    iguru++;
                                }
                            }



                        }

                        if (Data.XlsKelasQurans != null)
                        {
                            //qiroaty dan quran
                            Console.WriteLine("proses jadwal qiroaty/quran");

                            i = 1;
                            foreach (var item in Data.XlsKelasQurans)
                            {
                                errdata = string.Format("SetKbmJadwal {0} {1}, {2}", item.Hari, item.JamMulai, item.JamSelesai);
                                int idKbmJadwal = InsertKbmJadwal(conn, item.Hari, item.JamMulai, item.JamSelesai, out string oMessage);
                                if (!string.IsNullOrEmpty(oMessage))
                                    return string.Format("SetKbmJadwal {0} {1}, {2}: {3}", item.Hari, item.JamMulai, item.JamSelesai, oMessage);


                                var pegawais = from a in conn.GetList<TbUser>()
                                               where a.FirstName.ToLower().Trim() == item.NamaGuru.ToLower().Trim()
                                               select a;
                                if (pegawais != null)
                                {
                                    if (pegawais.Count() == 1)
                                        idPegawai = pegawais.SingleOrDefault().IdUser;
                                }
                                TbKelasQuran tbKelasQuran = (from a in conn.GetList<TbKelasQuran>()
                                                             where a.IdPegawai == idPegawai
                                                             & a.Nama.ToLower().Trim().Replace(" ", "") == item.NamaKelasQuran.ToLower().Trim().Replace(" ", "")
                                                             select a).FirstOrDefault();
                                int idKelasQuran = 0;
                                if (tbKelasQuran == null)
                                {
                                    idKelasQuran = i;
                                    tbKelasQuran = new TbKelasQuran
                                    {
                                        IdKelasQuran = idKelasQuran,
                                        IdPegawai = idPegawai,
                                        Nama = item.NamaKelasQuran,
                                        Kode = item.NamaKelasQuran
                                    };
                                    conn.Insert(tbKelasQuran);
                                    i++;
                                }
                                else
                                {
                                    idKelasQuran = tbKelasQuran.IdKelasQuran;
                                }
                                TbKelasQuranJadwal tbKelasQuranJadwal = new TbKelasQuranJadwal
                                {
                                    IdKbmJadwal = idKbmJadwal,
                                    IdKelasQuran = idKelasQuran
                                };
                                var cek = (from a in conn.GetList<TbKelasQuranJadwal>()
                                           where a.IdKbmJadwal == idKbmJadwal
                                           & a.IdKelasQuran == idKelasQuran
                                           select a).FirstOrDefault();
                                if (cek == null)
                                    conn.Insert(tbKelasQuranJadwal);
                            }
                        }

                        if (Data.XlsKelasQuranSiswas != null)
                        {
                            //kelas quran siswa
                            Console.WriteLine("proses siswa qiroaty/quran");
                            foreach (var item in Data.XlsKelasQuranSiswas)
                            {
                                var tbKelasQurans = from a in conn.GetList<TbKelasQuran>()
                                                    where a.Kode.ToLower().Trim().Replace(" ", "") == item.NamaKelasQuran.ToLower().Trim().Replace(" ", "")
                                                    select a;
                                if (tbKelasQurans == null)
                                    return string.Format("kelas quran {0} siswa {1} tidak ada", item.NamaKelasQuran, item.NamaSiswa);
                                if (tbKelasQurans.Count() > 1)
                                    return string.Format("kelas quran {0} siswa {1} lebih dari satu", item.NamaKelasQuran, item.NamaSiswa);
                                if (tbKelasQurans.Count() == 0)
                                    return string.Format("kelas quran {0} siswa {1} tidak ada", item.NamaKelasQuran, item.NamaSiswa);

                                errdata = string.Format("tbKelasQurans {0} siswa {1} tidak ada", item.NamaKelasQuran, item.NamaSiswa);
                                int idKelasQuran = tbKelasQurans.SingleOrDefault().IdKelasQuran;

                                TbSiswa tbSiswa = (from a in conn.GetList<TbSiswa>()
                                                   where a.Nis == item.Nis.Trim()
                                                   select a).FirstOrDefault();
                                if (tbSiswa == null) return string.Format("siswa {0} siswa {1} tidak ada", item.Nis, item.NamaSiswa);

                                TbKelasQuranSiswa tbKelasQuranSiswa = new TbKelasQuranSiswa
                                {
                                    IdKelasQuran = idKelasQuran,
                                    IdSiswa = tbSiswa.IdSiswa
                                };
                                conn.Insert(tbKelasQuranSiswa);
                            }
                        }


                        //evaluasi harian
                        Console.WriteLine("proses evaluasi harian");
                        i = 1;
                        foreach (var evaluasiHarian in Data.XlsJenisEvaluasiHarian)
                        {
                            errdata = string.Format("evaluasiHarian {0} {1}", evaluasiHarian.Kode, evaluasiHarian.Nama);
                            TbJenisEvaluasiHarian tbJenisEvaluasiHarian = new TbJenisEvaluasiHarian
                            {
                                IdJenisEvaluasiHarian = i,
                                Nama = evaluasiHarian.Nama,
                                NamaSingkat = evaluasiHarian.Kode
                            };
                            i++;
                            conn.Insert(tbJenisEvaluasiHarian);
                        }

                        Console.WriteLine("save data");
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return errdata + Environment.NewLine + commonService.GetErrorMessage(ServiceName + "SetInitialData", ex);
            }
        }
        public bool CanAksesByJabatan(IDbConnection conn, int IdJabatan, int IdHakAkses)
        {
            try
            {
                var item = (from a in conn.GetList<TbJenisJabatanHakAkses>()
                            where a.IdJenisJabatan == IdJabatan
                            & a.IdJenisHakAkses == IdHakAkses
                            select a).FirstOrDefault();
                if (item == null) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool CanAksesByUser(IDbConnection conn, int IdUser, int IdHakAkses)
        {
            try
            {
                var jabatan = (from a in conn.GetList<TbPegawai>()
                               where a.IdPegawai == IdUser
                               select a).FirstOrDefault();
                if (jabatan == null) return false;
                var item = (from a in conn.GetList<TbJenisJabatanHakAkses>()
                            where a.IdJenisJabatan == jabatan.IdJenisJabatan
                            & a.IdJenisHakAkses == IdHakAkses
                            select a).FirstOrDefault();
                if (item == null) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #region get referensi (combos)
        public List<ComboModel> GetAgamas()
        {
            return (from a in AgamaConstant.Dict
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetJalurPendaftarans()
        {
            return (from a in JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetJenisPekerjaans(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbJenisPekerjaan>();
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    return (from a in data
                            select new ComboModel { Id = a.IdJenisPekerjaan, Nama = a.Nama, NamaSingkat = a.NamaSingkat }
                            ).OrderBy(x => x.Id).ToList();
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisPekerjaans", ex);
                return null;
            }
        }
        public List<ComboModel> GetJenisPendaftarans()
        {
            return (from a in JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetJenisPrestasis()
        {
            return (from a in JenisPrestasiConstant.DictJenisPrestasi
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetKategoriDaftars()
        {
            return (from a in KategoriPendaftaranPpdbConstant.DictKategoriPendaftaranPpdb
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetJenisPegawais()
        {
            return (from a in JenisPegawaiConstant.DictJenisPegawai
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetJenisPegawaiGolongans()
        {
            return (from a in JenisPegawaiGolonganConstant.DictJenisPegawaiGolongan
                    where a.Key != 0
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();

        }
        public List<ComboModel> GetJenisRedaksiEmails()
        {
            return (from a in JenisRedaksiEmailPpdbConstant.Dict
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetJenisBiayaPpdbs(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbJenisBiayaPpdb>();
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    return (from a in data
                            select new ComboModel { Id = a.IdJenisBiayaPpdb, Nama = a.Nama, NamaSingkat = a.NamaSingkat }
                            ).OrderBy(x => x.Id).ToList();
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisBiayaPpdbs", ex);
                return null;
            }
        }
        public List<ComboModel> GetGelombangPpdbs()
        {
            return (from a in GelombangPpdbConstant.DictGelombangPpdb
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetUnits()
        {
            return (from a in UnitConstant.DictUnit
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetTingkatPrestasiSiswas()
        {
            return (from a in TingkatPrestasiSiswaConstant.DictTingkatPrestasiSiswa
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetStatusPpdbs()
        {
            return (from a in StatusPpdbConstant.Dict
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetJenisWargaNegara()
        {
            var data = (from a in JenisWargaNegaraConstant.Dict
                        select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
            return data;
        }
        public List<ComboModel> GetTinggalSiswa()
        {
            return (from a in JenisTinggalSiswaConstant.DictJenisTinggalSiswa
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetJenisTransportasi()
        {
            return (from a in JenisTransportasiConstant.DictJenisTransportasi
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetJenisBahasa()
        {
            return (from a in JenisBahasaConstant.DictJenisBahasa
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetStatusHasilObservasi()
        {
            return (from a in StatusPpdbHasilObservasiConstant.Dict
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetStatusVerifikasiObservasi()
        {
            return (from a in StatusPpdbVerifikasiObservasiConstant.Dict
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetKelasPpdbs(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<ComboModel> ret = new List<ComboModel>();
                //                PpdbModelRead ppdb = this.GetPpdb(out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return null;

                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbPpdbKelas>()
                               join b in conn.GetList<TbKelas>() on a.IdKelas equals b.IdKelas
                               where a.IdPpdb == currPpdb.IdPpdb
                               select new { a, b };
                    foreach (var item in data)
                    {
                        ComboModel m = new ComboModel
                        {
                            Id = item.a.IdPpdbKelas,
                            Kode = item.a.IdPpdbKelas.ToString(),
                            Nama = item.b.Nama,
                            NamaSingkat = item.b.Nama
                        };
                        if (item.a.IdJenisPeminatan != 0)
                            m.Nama += " " + JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb[(int)item.a.IdJenisPeminatan];
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelasPpdbs", ex);
                return null;

            }

        }
        public List<ComboModel> GetKelasPpdb(int IdUnit, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<ComboModel> ret = new List<ComboModel>();
                //                PpdbModelRead ppdb = this.GetPpdb(out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return null;

                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbPpdbKelas>()
                               join b in conn.GetList<TbKelas>() on a.IdKelas equals b.IdKelas
                               where a.IdPpdb == currPpdb.IdPpdb && b.IdUnit == IdUnit
                               select new { a, b };
                    foreach (var item in data.OrderBy(x => x.a.IdPpdbKelas))
                    {
                        ComboModel m = new ComboModel
                        {
                            Id = item.a.IdPpdbKelas,
                            Kode = item.a.IdPpdbKelas.ToString(),
                            Nama = item.b.Nama,
                            NamaSingkat = item.b.Nama
                        };
                        if (item.a.IdJenisPeminatan != 0)
                            m.Nama += " " + JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb[(int)item.a.IdJenisPeminatan];
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelasPpdb", ex);
                return null;

            }

        }
        public List<ComboModel> GetJenisHobis(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbJenisHobi>();
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    return (from a in data
                            select new ComboModel { Id = a.IdJenisHobi, Nama = a.Nama, NamaSingkat = a.NamaSingkat }
                            ).OrderBy(x => x.Id).ToList();
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisHobis", ex);
                return null;
            }
        }
        public List<ComboModel> GetJenisCita2s(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbJenisCita2>();
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    return (from a in data
                            select new ComboModel { Id = a.IdJenisCita2, Nama = a.Nama, NamaSingkat = a.NamaSingkat }
                            ).OrderBy(x => x.Id).ToList();
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisHobis", ex);
                return null;
            }
        }

        public List<ComboModel> GetProvinsi(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbWilProvinsi>();
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    return (from a in data
                            select new ComboModel { Id = a.IdWilProvinsi, Nama = a.Nama, NamaSingkat = a.Nama }
                            ).OrderBy(x => x.Id).ToList();
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisHobis", ex);
                return null;
            }
        }
        public List<ComboModel> GetKabkot(int IdProvinsi, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbWilKabkot>()
                               where a.IdWilProvinsi == IdProvinsi
                               select a;
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    return (from a in data
                            select new ComboModel { Id = a.IdWilKabkot, Nama = a.Nama, NamaSingkat = a.Nama }
                            ).OrderBy(x => x.Id).ToList();
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisHobis", ex);
                return null;
            }
        }
        public List<ComboModel> GetKecamatan(int IdKabkot, out string oMessage)
        {
            {
                try
                {
                    oMessage = string.Empty;
                    using (var conn = commonService.DbConnection())
                    {
                        var data = from a in conn.GetList<TbWilKecamatan>()
                                   where a.IdWilKabkot == IdKabkot
                                   select a;
                        if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                        return (from a in data
                                select new ComboModel { Id = a.IdWilKecamatan, Nama = a.Nama, NamaSingkat = a.Nama }
                                ).OrderBy(x => x.Id).ToList();
                    }
                }
                catch (Exception ex)
                {
                    oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisHobis", ex);
                    return null;
                }
            }
        }
        public List<ComboModel> GetDesa(int IdKecamatan, out string oMessage)
        {
            {
                {
                    try
                    {
                        oMessage = string.Empty;
                        using (var conn = commonService.DbConnection())
                        {
                            var data = from a in conn.GetList<TbWilDesa>()
                                       where a.IdWilKecamatan == IdKecamatan
                                       select a;
                            if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                            return (from a in data
                                    select new ComboModel { Id = a.IdWilDesa, Nama = a.Nama, NamaSingkat = a.Nama }
                                    ).OrderBy(x => x.Id).ToList();
                        }
                    }
                    catch (Exception ex)
                    {
                        oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisHobis", ex);
                        return null;
                    }
                }
            }
        }
        #endregion
        #region pegawai
        public string GeneratePasswordAllPegawai()
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var data = conn.GetList<TbUser>();
                        foreach (var item in data)
                        {
                            item.Password = commonService.EncryptString(item.Username);
                            conn.Update(item);
                        }
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "GeneratePasswordAllPegawai", ex);
            }
        }
        PegawaiModel SetPegawaiModel(TbPegawai tbPegawai, TbUser tbUser, TbUser TbUser, TbJenisJabatan TbJenisJabatan, TbJenisJenjangPendidikan TbJenisJenjangPendidikan, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                PegawaiModel ret = new PegawaiModel();
                ret.Email = tbUser.Email;
                ret.Nama = userAppService.SetFullName(TbUser.FirstName, TbUser.MiddleName, TbUser.LastName);
                ret.JenisKelamin = JenisKelaminConstant.Dict[tbPegawai.KdJenisKelamin];
                ret.Alamat = TbUser.Address;
                ret.NoTelpon = TbUser.PhoneNumber;
                ret.NoHandphone = TbUser.MobileNumber;
                ret.Jabatan = TbJenisJabatan.Nama;
                ret.Agama = AgamaConstant.Dict[tbPegawai.IdAgama];
                ret.JenjangPendidikan = TbJenisJenjangPendidikan.Nama;
                ret.IdPegawai = tbPegawai.IdPegawai;
                ret.IdJabatan = tbPegawai.IdJenisJabatan;
                ret.Nip = tbPegawai.Nip;
                ret.Nik = tbPegawai.Nik; ;
                ret.TempatLahir = tbPegawai.TempatLahir;
                ret.TanggalLahir = tbPegawai.TanggalLahir.ToString("dd-MM-yyyy");
                ret.KdJenisKelamin = tbPegawai.KdJenisKelamin;
                ret.IdAgama = tbPegawai.IdAgama;
                ret.IdJenjangPendidikan = tbPegawai.IdJenisJenjangPendidikan;
                ret.NamaInstitusiPendidikan = tbPegawai.NamaInstitusiPendidikan;
                ret.NamaPasangan = tbPegawai.NamaPasangan;
                ret.NoDarurat = tbPegawai.NoDarurat;
                ret.IdJenisPegawai = tbPegawai.IdJenisPegawai;
                ret.JenisPegawai = JenisPegawaiConstant.DictJenisPegawai[tbPegawai.IdJenisPegawai];
                ret.Gelar = tbPegawai.Gelar;
                if (tbPegawai.Status == 1)
                {
                    ret.Status = StatusDataConstant.Dict[1];
                }
                else if (tbPegawai.Status == -1)
                {
                    ret.Status = StatusDataConstant.Dict[-1];
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "SetPegawaiModel", ex);
                return null;
            }
        }
        public List<PegawaiModel> GetPegawais(int Status, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var ret = GetPegawais(conn, Status, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPegawais", ex);
                return null;
            }
        }
        public PegawaiModel GetPegawai(int IdPegawai, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var ret = GetPegawai(conn, IdPegawai, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPegawai", ex);
                return null;
            }
        }
        public List<PegawaiModel> GetPegawais(IDbConnection conn, int Status, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                var data = from a in conn.GetList<TbPegawai>()
                           join b in conn.GetList<TbUser>() on a.IdPegawai equals b.IdUser
                           join c in conn.GetList<TbUser>() on b.IdUser equals c.IdUser
                           join d in conn.GetList<TbJenisJabatan>() on a.IdJenisJabatan equals d.IdJenisJabatan
                           join e in conn.GetList<TbJenisJenjangPendidikan>() on a.IdJenisJenjangPendidikan equals e.IdJenisJenjangPendidikan
                           join f in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals f.IdPegawai
                           join g in conn.GetList<TbUnit>() on f.IdUnit equals g.IdUnit
                           select new { a, b, c, d, e, f, g };
                if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                List<PegawaiModel> ret = new List<PegawaiModel>();
                foreach (var item in data)
                {
                    PegawaiModel m = SetPegawaiModel(item.a, item.b, item.c, item.d, item.e, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.IdUnit = item.f.IdUnit;
                    m.Unit = item.g.Nama;
                    if (item.a.Status == Status)
                        ret.Add(m);
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPegawais", ex);
                return null;
            }
        }
        public PegawaiModel GetPegawai(IDbConnection conn, int IdPegawai, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                var data = (from a in conn.GetList<TbPegawai>()
                            join b in conn.GetList<TbUser>() on a.IdPegawai equals b.IdUser
                            join c in conn.GetList<TbUser>() on b.IdUser equals c.IdUser
                            join d in conn.GetList<TbJenisJabatan>() on a.IdJenisJabatan equals d.IdJenisJabatan
                            join e in conn.GetList<TbJenisJenjangPendidikan>() on a.IdJenisJenjangPendidikan equals e.IdJenisJenjangPendidikan
                            join f in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals f.IdPegawai
                            join g in conn.GetList<TbUnit>() on f.IdUnit equals g.IdUnit
                            where a.IdPegawai == IdPegawai
                            select new { a, b, c, d, e, f, g }).FirstOrDefault();
                if (data == null) { oMessage = "data tidak ada"; return null; }
                var ret = SetPegawaiModel(data.a, data.b, data.c, data.d, data.e, out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return null;
                ret.IdUnit = data.f.IdUnit;
                ret.Unit = data.g.Nama;
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPegawai", ex);
                return null;
            }
        }
        public PegawaiUnitModel GetUnitPegawai(int IdUser, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {

                    var unitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                       where a.IdPegawai == IdUser
                                       select new PegawaiJenisUnitModel { IdUnit = a.IdUnit, NamaUnit = UnitConstant.DictUnit[a.IdUnit] }).ToList();
                    if (unitPegawai == null || unitPegawai.Count() == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    PegawaiUnitModel ret = new PegawaiUnitModel();
                    ret.IdPegawai = IdUser;
                    ret.Unit = unitPegawai;

                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisJilidQurans", ex);
                return null;
            }

        }
        public string GetLevelJabatanUnitPegawai(IDbConnection conn, int IdUser, out int LevelJabatan, out int IdUnit)
        {
            IdUnit = -1;
            LevelJabatan = -1;
            var tbUserRoles = from a in conn.GetList<TbUserRole>()
                              where a.IdUser == IdUser
                              select a;
            foreach (var item in tbUserRoles)
            {
                if (item.IdRole == 1)
                    LevelJabatan = 99;
            }
            if (LevelJabatan != 99)
            {
                var tbUnitPegawai = (from a in conn.GetList<TbPegawai>()
                                     join b in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals b.IdPegawai
                                     join c in conn.GetList<TbJenisJabatan>() on a.IdJenisJabatan equals c.IdJenisJabatan
                                     where a.IdPegawai == IdUser
                                     select new { a, b, c }).FirstOrDefault();
                if (tbUnitPegawai == null)
                    return "pegawai belum disetting unit";
                IdUnit = tbUnitPegawai.b.IdUnit;
                LevelJabatan = tbUnitPegawai.c.Level;
            }
            return string.Empty;
        }
        #endregion
        #region siswa
        public string GeneratePasswordAllSiswa()
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var data = conn.GetList<TbSiswa>();
                        foreach (var item in data)
                        {
                            item.Password = commonService.EncryptString(item.Nis);
                            conn.Update(item);
                        }
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "GeneratePasswordAllSiswa", ex);
            }
        }
        private SiswaModel SetSiswaModel(IDbConnection conn, TbSiswa tbSiswa, string PekerjaanIbu, string PekerjaanAyah, TbKelasParalel tbKelasParalel, TbKelas tbKelas, TbUnit tbUnit, out string oMessage)
        {

            try
            {
                oMessage = string.Empty;
                SiswaModel ret = new SiswaModel();
                ret.FotoProfile = tbSiswa.FotoProfile;
                ret.NisLama = tbSiswa.NisLama;
                ret.Status = tbSiswa.Status;
                ret.Agama = AgamaConstant.Dict[tbSiswa.IdAgama];
                ret.JenisKelamin = JenisKelaminConstant.Dict[tbSiswa.KdJenisKelamin];
                ret.JenisPekerjaanIbu = PekerjaanIbu;
                ret.JenisPekerjaanAyah = PekerjaanAyah;
                ret.StrStatus = StatusDataConstant.Dict[tbSiswa.Status];
                ret.Nis = tbSiswa.Nis;
                ret.Nisn = tbSiswa.Nisn;
                ret.Nik = tbSiswa.Nik;
                ret.Nama = tbSiswa.Nama;
                ret.NamaPanggilan = tbSiswa.NamaPanggilan;
                ret.TempatLahir = tbSiswa.TempatLahir;
                ret.TanggalLahir = tbSiswa.TanggalLahir.ToString("dd-MM-yyyy");
                ret.IdAgama = tbSiswa.IdAgama;
                ret.KdJenisKelamin = tbSiswa.KdJenisKelamin;
                ret.NoHandphone = tbSiswa.NoHandphone;
                ret.NoDarurat = tbSiswa.NoDarurat;
                ret.Email = tbSiswa.Email;
                ret.AlamatTinggal = tbSiswa.AlamatTinggal;
                ret.AlamatOrtu = tbSiswa.AlamatOrtu;
                ret.NikIbu = tbSiswa.NikIbu;
                ret.NamaIbu = tbSiswa.NamaIbu;
                ret.IdJenisPekerjaanIbu = tbSiswa.IdJenisPekerjaanIbu;
                ret.NamaInstansiIbu = tbSiswa.NamaInstansiIbu;
                ret.NikAyah = tbSiswa.NikAyah;
                ret.NamaAyah = tbSiswa.NamaAyah;
                ret.IdJenisPekerjaanAyah = tbSiswa.IdJenisPekerjaanAyah;
                ret.NamaInstansiAyah = tbSiswa.NamaInstansiAyah;
                ret.EmailOrtu = tbSiswa.EmailOrtu;
                ret.IdSiswa = tbSiswa.IdSiswa;
                ret.IdKelasParalel = tbKelasParalel.IdKelasParalel;
                ret.KelasParalel = tbKelasParalel.NamaSingkat;
                ret.IdKelas = tbKelas.IdKelas;
                ret.Kelas = tbKelas.NamaSingkat;
                ret.IdUnit = tbUnit.IdUnit;
                ret.Unit = tbUnit.Nama;
                ret.Tagihans1 = new List<SiswaTagihanDetilModel>();
                ret.Tagihans2 = new List<SiswaTagihanDetilModel>();
                ret.TotalTagihan = 0;
                ret.TotalTagihan1 = 0;
                ret.TotalTagihan2 = 0;
                ret.KelasQuran = new KelasQuranSiswaModel
                {
                    Nama = string.Empty,
                    Progress = new List<KelasQuranSiswaProgresListModel>(),
                    Ujians = new List<KelasQuranSiswaUjianListModel>()
                };
                return ret;
            }

            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "SetSiswaModel", ex);
                return null;
            }
        }
        public List<SiswaListModel> GetSiswas(IDbConnection conn, int Status, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                var data = from a in conn.GetList<TbSiswa>()
                           join b in conn.GetList<TbJenisPekerjaan>() on a.IdJenisPekerjaanIbu equals b.IdJenisPekerjaan
                           join c in conn.GetList<TbJenisPekerjaan>() on a.IdJenisPekerjaanAyah equals c.IdJenisPekerjaan
                           join d in conn.GetList<TbKelasRombel>() on a.IdSiswa equals d.IdSiswa
                           join e in conn.GetList<TbKelasParalel>() on d.IdKelasParalel equals e.IdKelasParalel
                           join f in conn.GetList<TbKelas>() on e.IdKelas equals f.IdKelas
                           join g in conn.GetList<TbUnit>() on f.IdUnit equals g.IdUnit
                           where a.Status == (Status == 0 ? a.Status : Status)
                           select new SiswaListModel
                           {
                               Nis = a.Nis,
                               Nisn = a.Nisn,
                               Nik = a.Nik,
                               Nama = a.Nama,
                               NamaPanggilan = a.NamaPanggilan,
                               TempatLahir = a.TempatLahir,
                               TanggalLahir = a.TanggalLahir.ToString("dd-MM-yyyy"),
                               Agama = a == null ? "" : AgamaConstant.Dict[a.IdAgama],
                               JenisKelamin = a == null ? "" : JenisKelaminConstant.Dict[a.KdJenisKelamin],
                               NoHandphone = a.NoHandphone,
                               NoDarurat = a.NoDarurat,
                               Email = a.Email,
                               AlamatTinggal = a.AlamatTinggal,
                               AlamatOrtu = a.AlamatOrtu,
                               NikIbu = a.NikIbu,
                               NamaIbu = a.NamaIbu,
                               PekerjaanIbu = b.Nama,
                               NamaInstansiIbu = a.NamaInstansiIbu,
                               NikAyah = a.NikAyah,
                               NamaAyah = a.NamaAyah,
                               PekerjaanAyah = c.Nama,
                               NamaInstansiAyah = a.NamaInstansiAyah,
                               EmailOrtu = a.EmailOrtu,
                               Unit = g.Nama,
                               Kelas = f.Nama,
                               KelasParalel = e.Nama,
                               Status = a.Status,
                               StrStatus = a == null ? "" : StatusDataConstant.Dict[a.Status],
                               IdSiswa = a.IdSiswa,
                               IdUnit = g.IdUnit,
                               IdKelas = f.IdKelas,
                               IdKelasParalel = e.IdKelasParalel,
                               NisLama = a.NisLama,
                               Password = commonService.DecryptString(a.Password),
                               Pin = a.Pin
                           };
                if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                return data.ToList();
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswas", ex);
                return null;
            }
        }
        public List<SiswaListModel> GetSiswas(int Status, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var ret = GetSiswas(conn, Status, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswas", ex);
                return null;
            }
        }
        public List<SiswaListModel> GetSiswasByUnit(int IdUnit, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                var siswas = GetSiswas(0, out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return null;
                return siswas.Where(x => x.IdUnit == IdUnit).ToList();
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswasByUnit", ex);
                return null;
            }
        }
        public List<SiswaListModel> GetSiswasByKelas(int IdKelas, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                var siswas = GetSiswas(0, out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return null;
                return siswas.Where(x => x.IdKelas == IdKelas).ToList();
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswasByKelasParalel", ex);
                return null;
            }
        }
        public List<SiswaListModel> GetSiswasByKelasParalel(int IdKelasParalel, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                var siswas = GetSiswas(0, out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return null;
                return siswas.Where(x => x.IdKelasParalel == IdKelasParalel).ToList();
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswasByKelasParalel", ex);
                return null;
            }
        }
        public SiswaModel GetSiswa(int IdSiswa, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var ret = GetSiswa(conn, IdSiswa, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPegawai", ex);
                return null;
            }
        }
        public SiswaModel GetSiswa(IDbConnection conn, int IdSiswa, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                var data = (from a in conn.GetList<TbSiswa>()
                            join b in conn.GetList<TbJenisPekerjaan>() on a.IdJenisPekerjaanIbu equals b.IdJenisPekerjaan
                            join c in conn.GetList<TbJenisPekerjaan>() on a.IdJenisPekerjaanAyah equals c.IdJenisPekerjaan
                            join d in conn.GetList<TbKelasRombel>() on a.IdSiswa equals d.IdSiswa
                            join e in conn.GetList<TbKelasParalel>() on d.IdKelasParalel equals e.IdKelasParalel
                            join f in conn.GetList<TbKelas>() on e.IdKelas equals f.IdKelas
                            join g in conn.GetList<TbUnit>() on f.IdUnit equals g.IdUnit
                            where a.IdSiswa == IdSiswa
                            select new { a, PekerjaanIbu = b.Nama, PekerjaanAyah = c.Nama, e, f, g }).FirstOrDefault();
                if (data == null) { oMessage = "data tidak ada"; return null; }

                SiswaModel ret = SetSiswaModel(conn, data.a, data.PekerjaanIbu, data.PekerjaanAyah, data.e, data.f, data.g, out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return null;

                var siswaTagihan = GetSiswaTagihan(conn, ret.Nis, out oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                {
                    if (oMessage == "data tagihan siswa tidak ada")
                    {
                        oMessage = string.Empty;
                        ret.Tagihans1 = new List<SiswaTagihanDetilModel>();
                        ret.Tagihans2 = new List<SiswaTagihanDetilModel>();
                        ret.TotalTagihan = 0;
                        ret.TotalTagihan1 = 0;
                        ret.TotalTagihan2 = 0;

                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    if (siswaTagihan.Status != StatusLunasConstant.Dict[StatusLunasConstant.Lunas])
                    {
                        ret.Tagihans1 = siswaTagihan.TagihanSebelumnya;
                        ret.Tagihans2 = siswaTagihan.TagihanBerjalan;
                        ret.TotalTagihan = siswaTagihan.RpTotal;
                        ret.TotalTagihan1 = siswaTagihan.RpSebelumnya;
                        ret.TotalTagihan2 = siswaTagihan.RpBerjalan;
                    }
                }


                ret.Rapors = new List<SiswaRaporModel>();
                var tbSiswaRapors = from a in conn.GetList<TbSiswaRapor>()
                                    join b in conn.GetList<TbJenisRapor>() on a.IdJenisRapor equals b.IdJenisRapor
                                    where a.IdSiswa == IdSiswa
                                    select new { a, b };
                foreach (var tbSiswaRapor in tbSiswaRapors)
                {
                    ret.Rapors.Add(new SiswaRaporModel
                    {
                        IdSiswa = IdSiswa,
                        NamaSiswa = data.a.Nama,
                        FileRapor = tbSiswaRapor.a.FileRapor,
                        IdJenisRapor = tbSiswaRapor.a.IdJenisRapor,
                        JenisRapor = tbSiswaRapor.b.Nama
                    });
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswa", ex);
                return null;
            }
        }
        public KelasQuranSiswaModel GetKelasQuranSiswa(IDbConnection conn, int IdSiswa, string Tanggal, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                DateTime currDate = DateTime.Now;
                DateTime NextFirstDate = dateTimeService.GetFirstDate(currDate.AddDays(7));
                if (!string.IsNullOrEmpty(Tanggal))
                {
                    currDate = dateTimeService.StrToDateTime(Tanggal);
                }
                if (currDate >= NextFirstDate)
                {
                    oMessage = string.Format("kbm belum aktif untuk tanggal {0}", dateTimeService.DateToLongString(_languageCode, currDate));
                    return null;
                }

                DateTime minDate = dateTimeService.GetFirstDate(currDate);
                DateTime maxDate = dateTimeService.GetLastDate(currDate);

                KelasQuranSiswaModel ret = new KelasQuranSiswaModel
                {
                    Nama = string.Empty,
                    Progress = new List<KelasQuranSiswaProgresListModel>(),
                    Ujians = new List<KelasQuranSiswaUjianListModel>()
                };
                var tbKelasQuran = (from a in conn.GetList<TbKelasQuranSiswa>()
                                    join b in conn.GetList<TbKelasQuran>() on a.IdKelasQuran equals b.IdKelasQuran
                                    where a.IdSiswa == IdSiswa
                                    select b).FirstOrDefault();
                if (tbKelasQuran != null)
                    ret.Nama = tbKelasQuran.Nama;
                for (int i = 0; i < 7; i++)
                {
                    DateTime tanggal = minDate.AddDays(i);

                    KelasQuranSiswaProgresListModel m = new KelasQuranSiswaProgresListModel
                    {
                        Hari = dateTimeService.GetLongDayName(_languageCode, (int)tanggal.DayOfWeek),
                        Tanggal = tanggal.ToString("dd-MM-yyyy")
                    };
                    var itemProgres = (from a in conn.GetList<TbKelasQuranProgres>()
                                       join b in conn.GetList<TbKelasQuran>() on a.IdKelasQuran equals b.IdKelasQuran
                                       join b1 in conn.GetList<TbUser>() on b.IdPegawai equals b1.IdUser
                                       join c in conn.GetList<TbJenisJilidQuran>() on a.IdJenisJilidQuran equals c.IdJenisJilidQuran
                                       where a.IdSiswa == IdSiswa
                                       & a.Tanggal.Date == tanggal.Date
                                       select new { a, b, b1, c }).FirstOrDefault();
                    if (itemProgres != null)
                    {
                        m.Halaman = itemProgres.a.Halaman;
                        m.Jilid = itemProgres.c.Nama;
                        m.Kelompok = itemProgres.b.Nama;
                        m.NamaGuru = userAppService.SetFullName(itemProgres.b1.FirstName, itemProgres.b1.MiddleName, itemProgres.b1.LastName);
                    }
                    ret.Progress.Add(m);
                }
                var itemUjians = from a in conn.GetList<TbKelasQuranUjian>()
                                 join c in conn.GetList<TbJenisJilidQuran>() on a.IdJenisJilidQuran equals c.IdJenisJilidQuran
                                 where a.IdSiswa == IdSiswa
                                 select new { a, c };
                if (itemUjians != null & itemUjians.Count() > 0)
                {
                    foreach (var item in itemUjians)
                    {
                        KelasQuranSiswaUjianListModel m = new KelasQuranSiswaUjianListModel
                        {
                            Hari = dateTimeService.GetLongDayName(_languageCode, (int)item.a.Tanggal.DayOfWeek),
                            Tanggal = item.a.Tanggal.ToString("dd-MM-yyyy"),
                            Catatan = item.a.Catatan,
                            Hasil = item.a.Hasil,
                            Jilid = item.c.Nama,
                            Nilai = item.a.NilaiHuruf
                        };
                        ret.Ujians.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelasQuranSiswa", ex);
                return null;
            }
        }
        public SiswaTagihanModel GetSiswaTagihan(IDbConnection conn, string Nis, out string oMessage)
        {
            oMessage = string.Empty;
            SiswaTagihanModel ret = new SiswaTagihanModel
            {
                TagihanBerjalan = new List<SiswaTagihanDetilModel>(),
                TagihanSebelumnya = new List<SiswaTagihanDetilModel>()
            };
            try
            {
                var data = (from a in conn.GetList<TbSiswa>()
                            join b in conn.GetList<TbSiswaTagihan>() on a.IdSiswa equals b.IdSiswa
                            where a.Nis == Nis || a.NisLama == Nis
                            select new { a, b }).FirstOrDefault();
                if (data == null)
                {
                    oMessage = "data tagihan siswa tidak ada";
                    return null;
                }
                ret.IdSiswa = data.a.IdSiswa;
                ret.Periode = data.b.Periode.ToString("MM-yyyy");
                ret.RefNo = data.b.RefNo;
                ret.Status = StatusLunasConstant.Dict[StatusLunasConstant.Lunas];
                if (data.b.Status != StatusLunasConstant.Lunas)
                    ret.Status = StatusLunasConstant.Dict[StatusLunasConstant.BelumLunas];
                if (data.b.UpdatedDate != null)
                {
                    ret.TanggalLunas = ((DateTime)data.b.UpdatedDate).ToString("dd-MM-yyyy HH:mm:ss");
                    ret.RefNo = data.b.RefNo;
                }
                ret.RpTotal = 0;
                ret.RpBerjalan = 0;
                ret.RpSebelumnya = 0;
                ret.Min = data.b.Min;
                ret.Max = data.b.Max;
                var tagBerjalan = from a in conn.GetList<TbSiswaTagihanDetil>()
                                  join b in conn.GetList<TbJenisTagihanSiswa>() on a.IdJenisTagihanSiswa equals b.IdJenisTagihanSiswa
                                  where a.IdSiswa == ret.IdSiswa & a.Periode == data.b.Periode
                                  select new { a.Rp, JenisTagihan = b.Nama, b.IdJenisTagihanSiswa, a.Periode };
                foreach (var item in tagBerjalan)
                {
                    ret.TagihanBerjalan.Add(new SiswaTagihanDetilModel
                    {
                        IdJenisTagihanSiswa = item.IdJenisTagihanSiswa,
                        JenisTagihan = item.JenisTagihan,
                        Rp = item.Rp,
                        Periode = item.Periode.ToString("dd-MM-yyyy"),
                    });
                    ret.RpBerjalan += item.Rp;
                }
                var tagSebelumnya = from a in conn.GetList<TbSiswaTagihanDetil>()
                                    join b in conn.GetList<TbJenisTagihanSiswa>() on a.IdJenisTagihanSiswa equals b.IdJenisTagihanSiswa
                                    where a.IdSiswa == ret.IdSiswa & a.Periode == data.b.Periode.AddMonths(-1)
                                    select new { a.Rp, JenisTagihan = b.Nama, b.IdJenisTagihanSiswa, a.Periode };
                foreach (var item in tagSebelumnya)
                {
                    ret.TagihanSebelumnya.Add(new SiswaTagihanDetilModel
                    {
                        IdJenisTagihanSiswa = item.IdJenisTagihanSiswa,
                        JenisTagihan = item.JenisTagihan,
                        Rp = item.Rp,
                        Periode = item.Periode.ToString("dd-MM-yyyy"),
                    });
                    ret.RpSebelumnya += item.Rp;
                }
                ret.RpTotal = ret.RpBerjalan + ret.RpSebelumnya;
                return ret;

            }
            catch (System.Exception ex)
            {
                oMessage = Environment.NewLine + commonService.GetErrorMessage(ServiceName + "GetSiswaTagihan", ex);
                return null;
            }
        }
        #endregion
        #region tahun ajaran
        private TahunAjaranModel SetTahunAjaranModel(TbTahunAjaran tbTahunAjaran, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                TahunAjaranModel ret = new TahunAjaranModel();
                ret.IdTahunAjaran = tbTahunAjaran.IdTahunAjaran;
                ret.Nama = tbTahunAjaran.Nama;
                ret.NamaSingkat = tbTahunAjaran.NamaSingkat;
                ret.Status = tbTahunAjaran.Status;
                ret.StrStatus = StatusDataConstant.Dict[tbTahunAjaran.Status];
                ret.TanggalMulai = tbTahunAjaran.TanggalMulai.ToString("dd-MM-yyyy");
                ret.TanggalSelesai = tbTahunAjaran.TanggalSelesai.ToString("dd-MM-yyyy");
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "SetTahunAjaranModel", ex);
                return null;
            }
        }
        public List<TahunAjaranModel> GetTahunAjarans(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbTahunAjaran>();
                    List<TahunAjaranModel> ret = new List<TahunAjaranModel>();
                    foreach (var item in data)
                    {
                        TahunAjaranModel m = SetTahunAjaranModel(item, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetTahunAjarans", ex);
                return null;
            }
        }
        public TahunAjaranModel GetTahunAjaran(int IdTahunAjaran, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var item = conn.Get<TbTahunAjaran>(IdTahunAjaran);
                    var ret = SetTahunAjaranModel(item, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetTahunAjaran", ex);
                return null;
            }
        }
        public TahunAjaranModel GetTahunAjaran(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var item = (from a in conn.GetList<TbTahunAjaran>()
                                where a.Status == StatusDataConstant.Aktif
                                select a).FirstOrDefault();
                    if (item == null) { oMessage = "tahun ajaran tidak ada yang aktif"; return null; }
                    var ret = SetTahunAjaranModel(item, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetTahunAjaran", ex);
                return null;
            }
        }
        public TahunAjaranModel GetTahunAjaranPpdb(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var item = (from a in conn.GetList<TbTahunAjaran>()
                                where a.StatusPpdb == StatusDataConstant.Aktif
                                select a).FirstOrDefault();
                    if (item == null) { oMessage = "tahun ajaran ppdb tidak ada yang aktif"; return null; }
                    var ret = SetTahunAjaranModel(item, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetTahunAjaran", ex);
                return null;
            }
        }
        public SemesterModel GetSemester(out string oMessage)
        {
            oMessage = string.Empty;
            SemesterModel ret = new SemesterModel
            {
                IdSemester = 1,
                Semester = "Semester 1"
            };
            if (DateTime.Now.Month < 7)
                ret = new SemesterModel
                {
                    IdSemester = 2,
                    Semester = "Semester 2"
                };
            return ret;
        }
        public string TahunAjaranAdd(int IdTahunAjaran, string Nama, string NamaSingkat, string TanggalMulai, string TanggalSelesai)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var data = from a in conn.GetList<TbTahunAjaran>()
                                   where a.Status == StatusDataConstant.Aktif
                                   select a;
                        foreach (var item in data)
                        {
                            item.Status = StatusDataConstant.TidakAktif;
                            conn.Update(item);
                        }
                        TbTahunAjaran tbTahunAjaran = new TbTahunAjaran
                        {
                            IdTahunAjaran = IdTahunAjaran,
                            Nama = Nama,
                            NamaSingkat = NamaSingkat,
                            Status = StatusDataConstant.Aktif,
                            TanggalMulai = dateTimeService.StrToDateTime(TanggalMulai),
                            TanggalSelesai = dateTimeService.StrToDateTime(TanggalSelesai)
                        };
                        conn.Insert(tbTahunAjaran);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "TahunAjaranAdd", ex);
            }
        }
        #endregion
        #region KBM
        public int InsertKbmJadwal(IDbConnection conn, string Hari, string JamMulai, string JamSelesai, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                JamMulai = JamMulai.Replace(".", ":");
                if (JamMulai.Length < 6) JamMulai += ":00";
                JamSelesai = JamSelesai.Replace(".", ":");
                if (JamSelesai.Length < 6) JamSelesai += ":00";

                TimeSpan jamMulai = TimeSpan.Parse(JamMulai);
                TimeSpan jamSelesai = TimeSpan.Parse(JamSelesai);
                string hari = Hari.Replace("'", "").Replace(" ", "").Trim().ToLower();
                List<string> haris = new List<string>();
                if (hari.IndexOf(",") > 0)
                {
                    string[] harisx = hari.Split(",");
                    for (int j = 0; j < harisx.Length; j++)
                    {
                        haris.Add(harisx[j]);
                    }
                }
                else
                {
                    haris.Add(hari);
                }
                for (int j = 0; j < haris.Count(); j++)
                {
                    int hariKe = 0;
                    switch (haris[j].Replace("'", "").Replace("`", "").Replace("’", "").ToLower().Trim())
                    {
                        case "minggu":
                            hariKe = 0;
                            break;
                        case "senin":
                            hariKe = 1;
                            break;
                        case "selasa":
                            hariKe = 2;
                            break;
                        case "rabu":
                            hariKe = 3;
                            break;
                        case "kamis":
                            hariKe = 4;
                            break;
                        case "jumat":
                            hariKe = 5;
                            break;
                        case "sabtu":
                            hariKe = 6;
                            break;
                        default:
                            oMessage = string.Format("nama hari {0} salah", haris[j]);
                            return -1;
                    }
                    var tbKbmJadwal = (from a in conn.GetList<TbKbmJadwal>()
                                       where a.HariKe == hariKe
                                       & a.JamMulai == jamMulai
                                       & a.JamSelesai == jamSelesai
                                       select a
                                       ).FirstOrDefault();
                    int idKbmJadwal = 0;
                    if (tbKbmJadwal == null)
                    {
                        /*
                        var tbKbmJadwals = conn.GetList<TbKbmJadwal>();
                        if (tbKbmJadwals.Count() > 0)
                        {
                            idKbmJadwal = tbKbmJadwals.Max(x => x.IdKbmJadwal);
                        }
                        else
                        {
                            idKbmJadwal = 1;
                        }

                        idKbmJadwal++;
                        */
                        tbKbmJadwal = new TbKbmJadwal
                        {
                            HariKe = hariKe,
                            JamMulai = jamMulai,
                            JamSelesai = jamSelesai
                        };
                        var hasil = conn.Insert(tbKbmJadwal);
                        idKbmJadwal = (int)hasil;
                    }
                    else
                    {
                        idKbmJadwal = tbKbmJadwal.IdKbmJadwal;
                    }
                    return idKbmJadwal;
                }
                return 0;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "InsertKbmJadwal", ex);
                return -1;
            }
        }
        public string UploadJadwalKbmTemp(int IdUser, List<SheetJadwalKbmModel> Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string sql = "INSERT INTO kbm.tb_kbm(id_kelas_paralel, id_mapel, id_pegawai, id_kbm_jadwal, id_kelompok) VALUES (@IdKelasParalel, @IdMapel, @IdPegawai, @IdKbmJadwal, @IdKelompok)";
                        List<TbKbm> TbKbms = new List<TbKbm>();
                        foreach (var item in Data)
                        {
                            int idKbmJadwal = InsertKbmJadwal(conn, item.Hari, item.JamMulai, item.JamSelesai, out string oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return "gagal insert tb_kbm_jadwal";
                            var kelasParalel = (from a in conn.GetList<TbKelasParalel>()
                                                where a.Nama == item.KelasParalel
                                                select a).FirstOrDefault();
                            int idKelasParalel = 0;
                            if (kelasParalel == null) return string.Format("kelas paralel {0} tidak terdafatar", item.KelasParalel);
                            idKelasParalel = kelasParalel.IdKelasParalel;

                            int idPegawai = 0;
                            var pegawai = (from a in conn.GetList<TbPegawai>()
                                           where a.Nip == item.NipGuru
                                           select a).FirstOrDefault();
                            if (pegawai == null) return string.Format("nip guru {0} tidak terdafatar", item.NipGuru);
                            idPegawai = pegawai.IdPegawai;

                            int idMapel = 0;
                            var mapel = (from a in conn.GetList<TbMapel>()
                                         where a.Nama == item.NamaMapel
                                         select a).FirstOrDefault();
                            if (mapel == null) return string.Format("mata pelajaran {0} tidak terdafatar", item.NamaMapel);
                            idMapel = mapel.IdMapel;


                            TbKbms.Add(new TbKbm
                            {
                                IdKelasParalel = idKelasParalel,
                                IdMapel = idMapel,
                                IdPegawai = idPegawai,
                                IdKbmJadwal = idKbmJadwal,
                                IdKelompok = item.Kelompok
                            });
                        }
                        var affectedRows = conn.Execute(sql, TbKbms);
                        tx.Commit();
                        Console.WriteLine(DateTime.Now);
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UploadJadwalKbmTemp", ex);
            }
        }
        public List<SheetKelasModel> GetKelass(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                List<SheetKelasModel> ret = new List<SheetKelasModel>();
                using (var conn = commonService.DbConnection())
                {
                    var data = from unit in conn.GetList<TbUnit>()
                               join kelas in conn.GetList<TbKelas>() on unit.IdUnit equals kelas.IdUnit
                               join kelasParalel in conn.GetList<TbKelasParalel>() on kelas.IdKelas equals kelasParalel.IdKelas
                               select new { unit, kelas, kelasParalel };
                    foreach (var item in data)
                    {
                        SheetKelasModel m = new SheetKelasModel
                        {
                            Unit = item.unit.Nama,
                            Kelas = item.kelas.Nama,
                            KelasParalel = item.kelasParalel.Nama
                        };

                        var data1 = from pegawai in conn.GetList<TbPegawai>()
                                    join jabatan in conn.GetList<TbJenisJabatan>() on pegawai.IdJenisJabatan equals jabatan.IdJenisJabatan
                                    join userProfile in conn.GetList<TbUser>() on pegawai.IdPegawai equals userProfile.IdUser
                                    join unitPegawai in conn.GetList<TbUnitPegawai>() on pegawai.IdPegawai equals unitPegawai.IdPegawai
                                    join kelas in conn.GetList<TbKelas>() on unitPegawai.IdUnit equals kelas.IdUnit
                                    join kelasParalel in conn.GetList<TbKelasParalel>() on kelas.IdKelas equals kelasParalel.IdKelas
                                    where kelasParalel.IdKelasParalel == item.kelasParalel.IdKelasParalel
                                    select new { userProfile.FirstName, userProfile.MiddleName, userProfile.LastName, pegawai.IdJenisJabatan, NamaJabatan = jabatan.Nama };
                        foreach (var item1 in data1)
                        {
                            string nama = userAppService.SetFullName(item1.FirstName, item1.MiddleName, item1.LastName);
                            if (item1.IdJenisJabatan == JenisJabatanPegawaiContant.WaliKelas)
                            {
                                m.WaliKelas = nama;
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(m.WakilWaliKelas1))
                                    m.WakilWaliKelas1 = nama;
                                if (string.IsNullOrEmpty(m.WakilWaliKelas2))
                                    m.WakilWaliKelas2 = nama;
                                if (string.IsNullOrEmpty(m.WakilWaliKelas3))
                                    m.WakilWaliKelas3 = nama;
                            }
                        }
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelass", ex);
                return null;
            }
        }
        public List<SheetMapelModel> GetMapels(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                List<SheetMapelModel> ret = new List<SheetMapelModel>();
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbMapel>();
                    foreach (var item in data)
                    {
                        ret.Add(new SheetMapelModel
                        {
                            Kode = item.Kode,
                            NamaMapel = item.Nama
                        });
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetMapels", ex);
                return null;
            }
        }
        public List<SheetKelQuranModel> GetKelQurans(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                List<SheetKelQuranModel> ret = new List<SheetKelQuranModel>();
                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbKelasQuran>()
                               join b in conn.GetList<TbUser>() on a.IdPegawai equals b.IdUser
                               select new { a, b };
                    foreach (var item in data)
                    {
                        ret.Add(new SheetKelQuranModel
                        {
                            NamaKelasQuran = item.a.Nama,
                            NamaGuru = userAppService.SetFullName(item.b.FirstName, item.b.MiddleName, item.b.LastName)
                        });
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelQurans", ex);
                return null;
            }
        }
        public List<SheetJadwalQuranModel> GetJadwalQurans(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                List<SheetJadwalQuranModel> ret = new List<SheetJadwalQuranModel>();
                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbKelasQuran>()
                               join b in conn.GetList<TbKelasQuranJadwal>() on a.IdKelasQuran equals b.IdKelasQuran
                               join c in conn.GetList<TbKbmJadwal>() on b.IdKbmJadwal equals c.IdKbmJadwal
                               join d in conn.GetList<TbUser>() on a.IdPegawai equals d.IdUser
                               select new { a, b, c, d };
                    foreach (var item in data)
                    {
                        ret.Add(new SheetJadwalQuranModel
                        {
                            Hari = dateTimeService.GetLongDayName(_languageCode, item.c.HariKe),
                            JamSelesai = item.c.JamSelesai.ToString(),
                            JamMulai = item.c.JamMulai.ToString(),
                            NamaKelasQuran = item.a.Nama,
                            NamaGuru = userAppService.SetFullName(item.d.FirstName, item.d.MiddleName, item.d.LastName)
                        });
                    }
                }

                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJadwalQurans", ex);
                return null;
            }
        }
        public List<SheetKelQuranSiswaModel> GetKelQuranSiswas(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                List<SheetKelQuranSiswaModel> ret = new List<SheetKelQuranSiswaModel>();
                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbKelasQuran>()
                               join b in conn.GetList<TbKelasQuranSiswa>() on a.IdKelasQuran equals b.IdKelasQuran
                               join c in conn.GetList<TbKelasRombel>() on b.IdSiswa equals c.IdSiswa
                               join d in conn.GetList<TbKelasParalel>() on c.IdKelasParalel equals d.IdKelasParalel
                               join e in conn.GetList<TbSiswa>() on b.IdSiswa equals e.IdSiswa
                               select new { e.Nis, e.Nama, KelasParalel = d.Nama, KelasQuran = a.Nama };
                    foreach (var item in data)
                    {
                        ret.Add(new SheetKelQuranSiswaModel
                        {
                            Nis = item.Nis,
                            NamaSiswa = item.Nama,
                            KelasParalel = item.KelasParalel,
                            NamaKelasQuran = item.KelasQuran
                        });
                    }
                }

                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelQuranSiswas", ex);
                return null;
            }
        }
        public List<SheetJenisEvaluasiHarianModel> GetJenisEvaluasiHarians(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                List<SheetJenisEvaluasiHarianModel> ret = new List<SheetJenisEvaluasiHarianModel>();
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbJenisEvaluasiHarian>();
                    foreach (var item in data)
                    {
                        ret.Add(new SheetJenisEvaluasiHarianModel
                        {
                            Kode = item.IdJenisEvaluasiHarian.ToString().PadLeft(3, '0'),
                            Nama = item.Nama
                        });
                    }
                }

                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisEvaluasiHarians", ex);
                return null;
            }
        }
        public List<SheetJenisJilidQuranModel> GetJenisJilidQurans(out string oMessage)
        {

            oMessage = string.Empty;
            try
            {
                List<SheetJenisJilidQuranModel> ret = new List<SheetJenisJilidQuranModel>();
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbJenisJilidQuran>();
                    foreach (var item in data)
                    {
                        ret.Add(new SheetJenisJilidQuranModel
                        {
                            Kode = item.IdJenisJilidQuran.ToString().PadLeft(3, '0'),
                            Nama = item.Nama
                        });
                    }
                }

                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisJilidQurans", ex);
                return null;
            }
        }
        #endregion
        #region Hari Libur
        public List<HariLiburModel> GetHariLiburs(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    return GetHariLiburs(conn, out oMessage);
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetHariLiburs", ex);
                return null;
            }
        }
        public List<HariLiburModel> GetHariLiburs(IDbConnection conn, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                var data = conn.GetList<TbJenisHariLibur>();
                List<HariLiburModel> ret = new List<HariLiburModel>();
                foreach (var item in data.OrderBy(x => x.Tanggal))
                {
                    ret.Add(new HariLiburModel
                    {
                        IdHariLibur = item.IdJenisHariLibur,
                        Tanggal = item.Tanggal.ToString("dd-MM-yyyy"),
                        Nama = item.Nama
                    });
                }
                if (ret == null || ret.Count() == 0) { oMessage = "data tidak ada"; return null; }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetHariLiburs", ex);
                return null;
            }
        }
        public HariLiburModel GetHariLibur(string Tanggal, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    return GetHariLibur(conn, Tanggal, out oMessage);
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetHariLibur", ex);
                return null;
            }
        }
        public HariLiburModel GetHariLibur(IDbConnection conn, string Tanggal, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                var item = (from a in conn.GetList<TbJenisHariLibur>()
                            where a.Tanggal == dateTimeService.StrToDateTime(Tanggal)
                            select a).FirstOrDefault();
                if (item == null) { oMessage = string.Empty; return null; }
                return new HariLiburModel
                {
                    IdHariLibur = item.IdJenisHariLibur,
                    Tanggal = item.Tanggal.ToString("dd-MM-yyyy"),
                    Nama = item.Nama
                };
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetHariLibur", ex);
                return null;
            }
        }
        public string HariLiburAddEdit(HariLiburModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbHariLibur = conn.GetList<TbJenisHariLibur>("where id_jenis_hari_libur = " + Data.IdHariLibur).FirstOrDefault();
                        if (tbHariLibur == null)
                        {
                            string sql = "INSERT INTO ref.tb_jenis_hari_libur(tanggal, nama, nama_singkat) VALUES (@Tanggal, @Nama, @NamaSingkat)";
                            tbHariLibur = new TbJenisHariLibur();

                            for (var date = dateTimeService.StrToDateTime(Data.Tanggal); date <= dateTimeService.StrToDateTime(Data.TanggalEnd); date = date.AddDays(1))
                            {
                                tbHariLibur.Tanggal = date;
                                tbHariLibur.Nama = Data.Nama;
                                tbHariLibur.NamaSingkat = Data.Nama;
                                var affectedRows = conn.Execute(sql, tbHariLibur);
                            }
                        }
                        else
                        {
                            tbHariLibur.Nama = Data.Nama;
                            conn.Update(tbHariLibur);
                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "HariLiburAddEdit", ex);
            }
        }
        public string HariLiburDelete(HariLiburModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbHariLibur = conn.GetList<TbJenisHariLibur>("where id_jenis_hari_libur = " + Data.IdHariLibur).FirstOrDefault();
                        if (tbHariLibur == null)
                        {
                            return "data tidak ditemukan!";
                        }
                        conn.Delete(tbHariLibur);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "HariLiburDelete", ex);
            }
        }
        public string AddHariLiburUnit(int IdUnit, List<int> IdHariLiburs)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var oldHariLiburUnit = from a in conn.GetList<TbUnitHariLibur>()
                                               where a.IdUnit == IdUnit
                                               select a;
                        foreach (var item in oldHariLiburUnit)
                        {
                            conn.Delete(item);
                        }
                        if (IdHariLiburs != null & IdHariLiburs.Count() > 0)
                        {
                            foreach (var item in IdHariLiburs)
                            {
                                TbUnitHariLibur tbHariLiburUnit = new TbUnitHariLibur { IdUnit = IdUnit, IdJenisHariLibur = item };
                                conn.Insert(tbHariLiburUnit);
                            }
                        }
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddHariLiburUnit", ex);
            }
        }
        public HariLiburUnitModel GetHariLibursByUnit(int IdUnit, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                oMessage = string.Empty;
                HariLiburUnitModel ret = new HariLiburUnitModel();
                ret.AssignLiburs = new List<HariLiburModel>();
                ret.NoAssignLiburs = new List<HariLiburModel>();
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    var unit = conn.Get<TbUnit>(IdUnit);
                    if (unit == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    ret.IdUnit = IdUnit;
                    ret.NamaUnit = unit.Nama;

                    var assignLiburs = from a in conn.GetList<TbJenisHariLibur>()
                                       join b in conn.GetList<TbUnitHariLibur>() on a.IdJenisHariLibur equals b.IdJenisHariLibur
                                       join c in conn.GetList<TbUnit>() on b.IdUnit equals c.IdUnit
                                       where b.IdUnit == IdUnit
                                       select new { a, b, c };
                    foreach (var item in assignLiburs)
                    {
                        HariLiburModel m = new HariLiburModel
                        {
                            IdHariLibur = item.a.IdJenisHariLibur,
                            Nama = item.a.Nama
                        };
                        ret.AssignLiburs.Add(m);
                    }

                    var noassignLiburs = from a in conn.GetList<TbJenisHariLibur>()
                                         where !(from d in assignLiburs
                                                 select d.a.IdJenisHariLibur
                                               ).Contains(a.IdJenisHariLibur)
                                         select a;

                    foreach (var item in noassignLiburs.OrderBy(x => x.IdJenisHariLibur))
                    {
                        HariLiburModel m = new HariLiburModel
                        {
                            IdHariLibur = item.IdJenisHariLibur,
                            Nama = item.Nama
                        };
                        ret.NoAssignLiburs.Add(m);
                    }
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetHariLibursByUnit", ex);
                return null;
            }
        }
        public HariLiburModel GetHariLiburByUnit(int IdUnit, string Tanggal, out string oMessage)
        {
            oMessage = string.Empty;

            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var item = (from a in conn.GetList<TbJenisHariLibur>()
                                    join b in conn.GetList<TbUnitHariLibur>() on a.IdJenisHariLibur equals b.IdJenisHariLibur
                                    where a.Tanggal == dateTimeService.StrToDateTime(Tanggal) && b.IdUnit == IdUnit
                                    select a).FirstOrDefault();
                        if (item == null) { oMessage = string.Empty; return null; }
                        return new HariLiburModel
                        {
                            Tanggal = item.Tanggal.ToString("dd-MM-yyyy"),
                            Nama = item.Nama
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetHariLibur", ex);
                return null;
            }
        }
        #endregion
        #region ppdb
        public PpdbModelRead GetPpdb(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var tbPpdb = (from a in conn.GetList<TbPpdb>()
                                  join b in conn.GetList<TbUser>() on a.IdKetua equals b.IdUser
                                  where a.Status == 1
                                  & a.TanggalAwal.Date <= dateTimeService.GetCurrdate().Date
                                  & a.TanggalAkhir.Date >= dateTimeService.GetCurrdate().Date
                                  select new { a, b }).ToList();
                    if (tbPpdb == null || tbPpdb.Count == 0) { oMessage = "Data tidak ada"; return null; }
                    if (tbPpdb.Count() > 1) { oMessage = "data ppdb salah"; return null; }
                    var ppdb = tbPpdb[0].a;
                    var user = tbPpdb[0].b;
                    PpdbModelRead ret = new PpdbModelRead
                    {
                        IdPpdb = ppdb.IdPpdb,
                        IdKetua = ppdb.IdKetua,
                        NamaKetua = userAppService.SetFullName(user.FirstName, user.MiddleName, user.LastName),
                        TahunAjaran = ppdb.TahunAjaran,
                        TanggalAwal = ppdb.TanggalAwal.Date.ToString("dd-MM-yyyy"),
                        TanggalAkhir = ppdb.TanggalAkhir.Date.ToString("dd-MM-yyyy"),
                        MinNoSk = ppdb.MinNoSk,
                        FilePetunjuk = ppdb.FilePetunjuk
                    };
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdb", ex);
                return null;
            }
        }

        public string GetRedaksiEmailPpdbBody(int IdJenisRedaksiEmailPpdb, out string MessageBody1, out string MessageBody2)
        {
            this.SetCurrentPPdb();
            MessageBody1 = string.Empty;
            MessageBody2 = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tb = (from a in conn.GetList<TbPpdbRedaksiEmail>()
                              where a.IdPpdb == currPpdb.IdPpdb
                              && a.IdJenisRedaksiEmailPpdb == IdJenisRedaksiEmailPpdb
                              select new
                              {
                                  a.MessageBody1,
                                  a.MessageBody2
                              }
                              ).FirstOrDefault();
                    if (tb == null)
                        return "data redaksi email tidak ada";

                    MessageBody1 = tb.MessageBody1;
                    MessageBody2 = tb.MessageBody2;
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
        public string SetHtmlEmailPpdbMessage(string MessageBody1, string MessageBody2, int IdPpdbDaftar, string NamaUnit, string JalurPendaftaran, string JenisPendaftaran, string Nama, string TanggalLahir, string TahunAjaran, string NamaSekolah, string KetuaPpdb)
        {
            string strHtml = "<html>";
            strHtml += "    <body>";
            strHtml += MessageBody1;
            strHtml += "        <table style='border:1px solid black;' style='border:1px solid black;' cellspacing='0'>";
            strHtml += "            <tr>";
            strHtml += "                <td style='border:0.5px solid black;padding:10px;'><strong>No. Pendaftaran</strong></td>";
            strHtml += "                <td style='border:0.5px solid black;padding:10px;'>:</td>";
            strHtml += "                <td style='border:0.5px solid black;padding:10px;'>" + IdPpdbDaftar + "</td>";
            strHtml += "            </tr>";
            strHtml += "            <tr>";
            strHtml += "                <td style='border:0.5px solid black;padding:10px;'><strong>Jenjang Pendidikan</strong></td>";
            strHtml += "                <td style='border:0.5px solid black;padding:10px;'>:</td>";
            strHtml += "                <td style='border:0.5px solid black;padding:10px;'>" + NamaUnit + "</td>";
            strHtml += "            </tr>";
            strHtml += "            <tr>";
            strHtml += "                <td style='border:0.5px solid black;padding:10px;'><strong>Jalur Pendaftaran</strong></td>";
            strHtml += "                <td style='border:0.5px solid black;padding:10px;'>:</td>";
            strHtml += "                <td style='border:0.5px solid black;padding:10px;'>" + JalurPendaftaran + "</td>";
            strHtml += "            </tr>";
            strHtml += "            <tr>";
            strHtml += "                <td style='border:0.5px solid black;padding:10px;'><strong>Jenis Pendaftaran</strong></td>";
            strHtml += "                <td style='border:0.5px solid black;padding:10px;'>:</td>";
            strHtml += "                <td style='border:0.5px solid black;padding:10px;'>" + JenisPendaftaran + "</td>";
            strHtml += "            </tr>";
            strHtml += "            <tr>";
            strHtml += "                <td style='border:0.5px solid black;padding:10px;'><strong>Nama</strong></td>";
            strHtml += "                <td style='border:0.5px solid black;padding:10px;'>:</td>";
            strHtml += "                <td style='border:0.5px solid black;padding:10px;'>" + Nama + "</td>";
            strHtml += "            </tr>";
            strHtml += "            <tr>";
            strHtml += "                <td style='border:0.5px solid black;padding:10px;'><strong>Tanggal Lahir</strong></td>";
            strHtml += "                <td style='border:0.5px solid black;padding:10px;'>:</td>";
            strHtml += "                <td style='border:0.5px solid black;padding:10px;'>" + TanggalLahir + "</td>";
            strHtml += "            </tr>";
            strHtml += "        </table><br>";
            strHtml += MessageBody2;
            strHtml += "        Bogor, " + DateTime.Now.ToString("dd-MM-yyyy") + "<br>";
            strHtml += "        Ketua Panita PPDB TP. " + TahunAjaran + "<br>";
            strHtml += "        " + NamaSekolah + "<br>";
            strHtml += "        " + KetuaPpdb + "";
            strHtml += "    </body>";
            strHtml += "</html>";
            return strHtml;
        }
        public string Registrasi(PpdbDaftarAddModel Data, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                string ret = string.Empty;
                if (Data == null) { oMessage = "data input harus diisi"; return string.Empty; }
                if (Data.IdPpdbKelas == 0) { oMessage = "kelas harus dipilih"; return string.Empty; }
                if (!commonService.DictCheck(KategoriPendaftaranPpdbConstant.DictKategoriPendaftaranPpdb, Data.IdJenisKategoriPendaftaran)) { oMessage = "kategori pendaftaran dipilih"; return string.Empty; }
                if (!commonService.DictCheck(JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb, Data.IdJenisPendaftaran)) { oMessage = "jenis pendaftaran dipilih"; return string.Empty; }
                if (Data.IdJenisPendaftaran == JenisPendaftaranPpdbConstant.Internal)
                {
                    if (string.IsNullOrEmpty(Data.NisSiswa) & string.IsNullOrEmpty(Data.NisSodara))
                    {
                        oMessage = "NIS siswa atau NIS Sodara harus diisi"; return string.Empty;
                    }
                    // else if (!string.IsNullOrEmpty(Data.NisSiswa) & !string.IsNullOrEmpty(Data.NisSodara))
                    // {
                    //     oMessage = "NIS siswa atau NIS Sodara harus salah satu diisi"; return string.Empty;
                    // }
                }
                if (string.IsNullOrEmpty(Data.NikOrtu)) { oMessage = "nik orang tua harus diisi"; return string.Empty; }
                if (Data.NikOrtu.Length != 16) { oMessage = "nik orang tua salah"; return string.Empty; }
                try
                {
                    var ceknik = double.Parse(Data.NikOrtu);
                }
                catch (Exception)
                {
                    oMessage = "nik orang tua salah"; return string.Empty;
                }
                if (!commonService.DictCheck(JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb, Data.IdJalurPendaftaran)) { oMessage = "jalur pendaftaran dipilih"; return string.Empty; }
                if (string.IsNullOrEmpty(Data.Nama)) { oMessage = "nama harus diisi"; return string.Empty; }
                if (string.IsNullOrEmpty(Data.TempatLahir)) { oMessage = "tempat lahir harus diisi"; return string.Empty; }
                if (string.IsNullOrEmpty(Data.TanggalLahir)) { oMessage = "tanggal lahir harus diisi"; return string.Empty; }
                var tglLahir = dateTimeService.StrToDateTime(Data.TanggalLahir, out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) { oMessage = "format tanggal lahir salah"; return string.Empty; }
                if (string.IsNullOrEmpty(Data.Email)) { oMessage = "email harus diisi"; return string.Empty; }
                if (string.IsNullOrEmpty(Data.NoHandphone)) { oMessage = "nomor handphone harus diisi"; return string.Empty; }

                if (currPpdb == null) { oMessage = "ppdb belum disetting"; return string.Empty; }

                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    if (!string.IsNullOrEmpty(oMessage))
                        return string.Empty;


                    if (!string.IsNullOrEmpty(Data.NisSiswa))
                    {
                        var siswa = (from a in conn.GetList<TbSiswa>()
                                     where a.Nis == Data.NisSiswa
                                     select a).FirstOrDefault();
                        if (siswa == null)
                        {
                            oMessage = string.Format("nis {0} siswa tidak terdaftar", Data.NisSiswa); return string.Empty;
                        }

                        var tagSiswa = this.GetSiswaTagihan(conn, Data.NisSiswa, out string errTag);
                        if (string.IsNullOrEmpty(errTag))
                        {
                            if (tagSiswa.RpSebelumnya > 0)
                            {
                                oMessage = string.Format("pendaftaran belum bisa dilanjutkan, mohon hubungi bagian keuangan"); return string.Empty;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(Data.NisSodara))
                    {
                        var siswa = (from a in conn.GetList<TbSiswa>()
                                     where a.Nis == Data.NisSodara
                                     select a).FirstOrDefault();
                        if (siswa == null)
                        {
                            oMessage = string.Format("nis {0} sodara siswa tidak terdaftar", Data.NisSodara); return string.Empty;
                        }

                        var tagSiswa = this.GetSiswaTagihan(conn, Data.NisSodara, out string errTag);
                        if (string.IsNullOrEmpty(errTag))
                        {
                            if (tagSiswa.RpSebelumnya > 0)
                            {
                                oMessage = string.Format("pendaftaran belum bisa dilanjutkan, silahkan hubungi bagian keuangan"); return string.Empty;
                            }
                        }
                    }

                    var tbAnak = (from a in conn.GetList<TbSiswa>()
                                  where a.EmailOrtu == Data.Email
                                  || a.NikAyah == Data.NikOrtu
                                  || a.NikIbu == Data.NikOrtu
                                  select a).FirstOrDefault();
                    if (tbAnak != null)
                    {
                        var tagSiswa = this.GetSiswaTagihan(conn, tbAnak.Nis, out string errTag);
                        if (string.IsNullOrEmpty(errTag))
                        {
                            if (tagSiswa.RpSebelumnya > 0)
                            {
                                oMessage = string.Format("pendaftaran belum bisa dilanjutkan, bapak/ibu mohon hubungi bagian keuangan"); return string.Empty;
                            }
                        }
                    }

                    var ppdbKelas = (from a in conn.GetList<TbPpdbKelas>()
                                     join b in conn.GetList<TbKelas>() on a.IdKelas equals b.IdKelas
                                     join c in conn.GetList<TbUnit>() on b.IdUnit equals c.IdUnit
                                     join d in conn.GetList<TbSekolah>() on c.IdSekolah equals d.IdSekolah
                                     where a.IdPpdbKelas == Data.IdPpdbKelas
                                     select new { a, b, c, d }).FirstOrDefault();
                    if (ppdbKelas == null) { oMessage = "PPDB belum di publish"; return string.Empty; }

                    var ppdbJadwals = from a in conn.GetList<TbPpdbJadwal>()
                                      where a.IdPpdb == currPpdb.IdPpdb
                                      & a.IdUnit == ppdbKelas.c.IdUnit
                                      & dateTimeService.GetCurrdate().Date >= a.TanggalAwalDaftar.Date
                                      & dateTimeService.GetCurrdate().Date <= a.TanggalAkhirDaftar.Date
                                      & a.IdJalurPendaftaran == Data.IdJalurPendaftaran
                                      & a.IdJenisPendaftaran == Data.IdJenisPendaftaran
                                      select a;
                    if (ppdbJadwals == null || ppdbJadwals.Count() == 0) { oMessage = "jadwal belum dipublish"; return null; }
                    if (ppdbJadwals.Count() > 1)
                    {
                        oMessage = "ada kesalahan jadwal ppdb";
                        return string.Empty;
                    }
                    TbPpdbJadwal tbPpdbJadwal = ppdbJadwals.First();
                    if (tglLahir > ppdbKelas.a.MaxTanggalLahir.Date)
                    {
                        oMessage = "usia calon siswa tidak memenuhi syarat";
                        return String.Empty;
                    }

                    if (!string.IsNullOrEmpty(Data.NisSiswa))
                    {
                        var dataSiswa = (from a in conn.GetList<TbSiswa>()
                                         join b in conn.GetList<TbKelasRombel>() on a.IdSiswa equals b.IdSiswa
                                         join c in conn.GetList<TbKelasParalel>() on b.IdKelasParalel equals c.IdKelasParalel
                                         join d in conn.GetList<TbKelas>() on c.IdKelas equals d.IdKelas
                                         where a.Nis == Data.NisSiswa
                                         select new { a, d }).FirstOrDefault();
                        if (dataSiswa == null)
                        {
                            oMessage = "nis siswa tidak tedaftar"; return string.Empty;
                        }
                        var dataSiswaDaftar = (from a in conn.GetList<TbPpdbSiswa>()
                                               where a.IdSiswa == dataSiswa.a.IdSiswa
                                               select a).FirstOrDefault();
                        if (dataSiswaDaftar != null)
                        {
                            oMessage = string.Format("calon siswa sudah terdaftar dengan nomor pendaftaran {0}", dataSiswaDaftar.IdPpdbDaftar);
                            return null;

                        }
                        if (ppdbKelas.c.IdUnit == UnitConstant.Sd)
                        {
                            if (dataSiswa.d.IdUnit != UnitConstant.Tk)
                            {
                                oMessage = string.Format("siswa tidak terdaftar di unit {0}", UnitConstant.DictUnit[UnitConstant.Tk]);
                                return string.Empty;
                            }
                        }
                        else if (ppdbKelas.c.IdUnit == UnitConstant.Smp)
                        {
                            if (dataSiswa.d.IdUnit != UnitConstant.Sd)
                            {
                                oMessage = string.Format("siswa tidak terdaftar di unit {0}", UnitConstant.DictUnit[UnitConstant.Sd]);
                                return string.Empty;
                            }
                        }
                        else if (ppdbKelas.c.IdUnit == UnitConstant.Sma)
                        {
                            if (dataSiswa.d.IdUnit != UnitConstant.Smp)
                            {
                                oMessage = string.Format("siswa tidak terdaftar di unit {0}", UnitConstant.DictUnit[UnitConstant.Smp]);
                                return string.Empty;
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(Data.NisSodara))
                    {
                        var dataSiswa = (from a in conn.GetList<TbSiswa>()
                                         join b in conn.GetList<TbKelasRombel>() on a.IdSiswa equals b.IdSiswa
                                         join c in conn.GetList<TbKelasParalel>() on b.IdKelasParalel equals c.IdKelasParalel
                                         join d in conn.GetList<TbKelas>() on c.IdKelas equals d.IdKelas
                                         where a.Nis == Data.NisSodara
                                         select d).FirstOrDefault();
                        if (dataSiswa == null)
                        {
                            oMessage = "nis sodara siswa tidak tedaftar"; return string.Empty;
                        }
                    }

                    //cek kuota prestasi
                    if (ppdbKelas.c.IdUnit == UnitConstant.Smp || ppdbKelas.c.IdUnit == UnitConstant.Sma)
                    {
                        if (Data.IdJalurPendaftaran == JalurPendaftaranPpdbConstant.Prestasi)
                        {
                            var jmlDaftarPrestasi = (from a in conn.GetList<TbPpdbDaftar>()
                                                     where a.Status == StatusDataConstant.Aktif
                                                     & a.IdPpdbKelas == Data.IdPpdbKelas
                                                     & a.IdJalurPendaftaran == JalurPendaftaranPpdbConstant.Prestasi
                                                     select a).Count();
                            if (jmlDaftarPrestasi >= ppdbKelas.a.KuotaDaftarPrestasi)
                            {
                                oMessage = "kuota jalur prestasi sudah habis";
                                return string.Empty;
                            }
                        }

                    }
                    //cek kuota daftar
                    bool IsKuota = true;
                    var jmlDaftar = (from a in conn.GetList<TbPpdbDaftar>()
                                     where a.Status == StatusDataConstant.Aktif
                                     & a.IdPpdbKelas == Data.IdPpdbKelas
                                     & a.KdJenisKelamin == Data.KdJenisKelamin
                                     select a).Count();
                    if (Data.KdJenisKelamin == JenisKelaminConstant.Lakilaki)
                        if (jmlDaftar >= ppdbKelas.a.KuotaDaftarL) IsKuota = false;
                    if (Data.KdJenisKelamin == JenisKelaminConstant.Perempuan)
                        if (jmlDaftar >= ppdbKelas.a.KuotaDaftarP) IsKuota = false;
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbPpdbVas = (from a in conn.GetList<TbPpdbVa>()
                                         where a.IdUnit == ppdbKelas.c.IdUnit
                                         & a.Status == StatusPpdbVaConstant.Aktif
                                         select a).ToList();
                        TbPpdbDaftar tbPpdbDaftar = new TbPpdbDaftar();
                        if (tbPpdbVas == null || tbPpdbVas.Count() == 0)
                        {
                            oMessage = "kuota pendaftaran sudah habis";
                            return string.Empty;
                        }
                        else
                        {
                            TbPpdbVa tbPpdbVa = tbPpdbVas.OrderBy(x => x.IdPpdbVa).First();
                            tbPpdbVa.Status = StatusPpdbVaConstant.SudahDigunakan;
                            conn.Update(tbPpdbVa);
                            tbPpdbDaftar.IdPpdbDaftar = tbPpdbVa.IdPpdbVa;
                        }
                        tbPpdbDaftar.IdPpdbKelas = Data.IdPpdbKelas;
                        tbPpdbDaftar.IdJenisKategoriPendaftaran = Data.IdJenisKategoriPendaftaran;
                        tbPpdbDaftar.IdJenisPendaftaran = Data.IdJenisPendaftaran;
                        tbPpdbDaftar.IdJalurPendaftaran = Data.IdJalurPendaftaran;
                        tbPpdbDaftar.Nama = Data.Nama.ToUpper();
                        tbPpdbDaftar.NisSiswa = Data.NisSiswa;
                        tbPpdbDaftar.NisSodara = Data.NisSodara;
                        tbPpdbDaftar.NikOrtu = Data.NikOrtu;
                        tbPpdbDaftar.KdJenisKelamin = Data.KdJenisKelamin;
                        tbPpdbDaftar.TempatLahir = Data.TempatLahir.ToUpper();
                        tbPpdbDaftar.TanggalLahir = dateTimeService.StrToDateTime(Data.TanggalLahir);
                        tbPpdbDaftar.Email = Data.Email.ToLower();
                        tbPpdbDaftar.NoHandphone = Data.NoHandphone;
                        tbPpdbDaftar.Pin = null;
                        tbPpdbDaftar.Status = IsKuota ? StatusPpdbDaftarConstant.AktifVa : StatusPpdbDaftarConstant.DaftarTunggu;
                        tbPpdbDaftar.CreatedDate = dateTimeService.GetCurrdate();
                        conn.Insert(tbPpdbDaftar);

                        ret = string.Format("kuota pendaftaran sudah habis, ananda masuk ke dalam daftar tunggu, informasi selanjutnya dikirim ke alamat email: {0}", Data.Email);
                        string biayaPendaftaran = string.Empty;
                        if (IsKuota)
                        {
                            ret = string.Format("pendaftaran berhasil, informasi selanjutnya dikirim ke alamat email: {0}", Data.Email);
                            TbPpdbBiayaDaftar tbPpdbBiayaDaftar = new TbPpdbBiayaDaftar
                            {
                                IdPpdbDaftar = tbPpdbDaftar.IdPpdbDaftar,
                                IsCicil = 0,
                                Status = StatusLunasConstant.BelumLunas,
                                RefNo = null,
                                TanggalLunas = null,
                                UpdatedBy = null,
                                UpdatedDate = null
                            };
                            conn.Insert(tbPpdbBiayaDaftar);
                            TbPpdbBiaya tbPpdbBiaya = (from a in conn.GetList<TbPpdbBiaya>()
                                                       where a.IdPpdb == currPpdb.IdPpdb
                                                       & a.IdJenisBiayaPpdb == 90 //hati2 soalnya dihardoce
                                                       select a).FirstOrDefault();
                            if (tbPpdbBiaya == null)
                            { oMessage = "biaya ppdb belum diset"; return string.Empty; }
                            var strIdPpdbDaftar = tbPpdbDaftar.IdPpdbDaftar.ToString();
                            var rpTigaDigit = strIdPpdbDaftar.Substring(strIdPpdbDaftar.Length - 3);
                            var rpBiayaRandom = tbPpdbBiaya.Rp + Convert.ToDouble(rpTigaDigit);
                            TbPpdbBiayaDaftarDetil tbPpdbBiayaDaftarDetil = new TbPpdbBiayaDaftarDetil
                            {
                                IdPpdbDaftar = tbPpdbDaftar.IdPpdbDaftar,
                                IdJenisBiayaPpdb = tbPpdbBiaya.IdJenisBiayaPpdb,
                                Rp = rpBiayaRandom,
                                Catatan = null
                            };
                            biayaPendaftaran = commonService.SetStrRupiah(rpBiayaRandom);
                            conn.Insert(tbPpdbBiayaDaftarDetil);
                        }
                        //hardcode heula
                        string norekening = "0094964946100";
                        string namaBank = "";
                        string namarekening = "TKIT AT TAUFIQ";
                        if (ppdbKelas.c.IdUnit == UnitConstant.Tk)
                        {
                            norekening = "0094964946100";
                            namaBank = "Bank Jabar Banten (BJB)";
                            namarekening = "TKIT AT TAUFIQ";
                        }
                        else if (ppdbKelas.c.IdUnit == UnitConstant.Sma)
                        {
                            norekening = "713 681 5916";
                            namaBank = "Bank Syariah Indonesia (BSI)";
                            namarekening = "SMAIT AT TAUFIQ";
                        }
                        //send mail
                        string _messageBody1 = string.Empty;
                        string _messageBody2 = string.Empty;
                        string _subjects = "PPDB";

                        List<string> emailTos = new List<string>();
                        List<string> emailCcs = new List<string>();
                        List<string> _attachments = new List<string>();

                        int jenisRedaksiEmail = JenisRedaksiEmailPpdbConstant.Registrasi;
                        if (!IsKuota)
                        {
                            jenisRedaksiEmail = JenisRedaksiEmailPpdbConstant.DaftarTunggu;
                        }
                        _subjects = JenisRedaksiEmailPpdbConstant.Dict[jenisRedaksiEmail];
                        oMessage = GetRedaksiEmailPpdbBody(jenisRedaksiEmail, out _messageBody1, out _messageBody2);
                        if (!string.IsNullOrEmpty(oMessage))
                            return string.Empty;
                        _messageBody1 = _messageBody1.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                        _messageBody2 = _messageBody2.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                        if (IsKuota)
                        {
                            _messageBody2 = _messageBody2.Replace("[RPBIAYADAFTAR]", biayaPendaftaran);
                            _messageBody2 = _messageBody2.Replace("[NOMORREKENING]", norekening);
                            _messageBody2 = _messageBody2.Replace("[NAMABANK]", namaBank);
                            _messageBody2 = _messageBody2.Replace("[NAMAREKENING]", namarekening);
                        }

                        emailTos.Add(tbPpdbDaftar.Email);
                        if (IsKuota)
                            emailCcs.Add("ppdb.sekolahattaufiqbogor@gmail.com");
                        string _emailMessage = SetHtmlEmailPpdbMessage(
                            _messageBody1,
                            _messageBody2,
                            tbPpdbDaftar.IdPpdbDaftar,
                            ppdbKelas.c.Nama,
                            JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[Data.IdJalurPendaftaran],
                            JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[Data.IdJenisPendaftaran],
                            tbPpdbDaftar.Nama,
                            Data.TanggalLahir,
                            currPpdb.TahunAjaran,
                            ppdbKelas.d.Nama,
                            currPpdb.NamaKetua);
                        SendEmailModel email = new SendEmailModel
                        {
                            To = emailTos,
                            Cc = emailCcs,
                            Title = "Bapak/Ibu ",
                            Subject = _subjects,
                            Message = _emailMessage,
                            Attachments = _attachments
                        };
                        _ = emailService.SendHtmlEmailAsync(email);

                        // string toEmail = string.Empty;
                        // string ccEmail = string.Empty;
                        // string log = string.Format("============= {0} - REGISTRASI =============" + Environment.NewLine, dateTimeService.GetCurrdate());
                        // foreach (var item in email.To)
                        // {
                        //     toEmail += item + ",";
                        // }
                        // foreach (var item in email.Cc)
                        // {
                        //     ccEmail += item + ",";
                        // }
                        // log += string.Format("To : {0}" + Environment.NewLine, toEmail);
                        // log += string.Format("Cc : {0}" + Environment.NewLine, ccEmail);
                        // log += string.Format("Subject : {0}" + Environment.NewLine, email.Subject);
                        // log += string.Format("Title : {0}" + Environment.NewLine, email.Title);
                        // log += string.Format("============================================================" + Environment.NewLine + Environment.NewLine);
                        // File.AppendAllText(ConstPathFilesConstant.PathFilePpdbBerkas + "/!log_mail.txt", log);

                        tx.Commit();
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "Registrasi", ex);
                return string.Empty;
            }
        }
        public PpdbBiayaDaftarModel GetPpdbDaftarBiaya(IDbConnection conn, int IdPpdbDaftar, out string oMessage)
        {
            oMessage = string.Empty;
            PpdbBiayaDaftarModel ret = new PpdbBiayaDaftarModel { TagPpdb = new List<PpdbBiayaDaftarDetilModel>() };
            try
            {
                var item = (from a in conn.GetList<TbPpdbDaftar>()
                            join b1 in conn.GetList<TbPpdbBiayaDaftar>() on a.IdPpdbDaftar equals b1.IdPpdbDaftar
                            join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                            join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                            join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                            join e in conn.GetList<TbSekolah>() on d.IdSekolah equals e.IdSekolah
                            where a.Status == StatusDataConstant.Aktif
                            & a.IdPpdbDaftar == IdPpdbDaftar
                            select new { a, b, b1, c, d, e }).FirstOrDefault();
                if (item == null) { oMessage = "data tidak ada"; return null; }

                string peminatan = string.Empty;
                if (item.b.IdJenisPeminatan != null)
                    if (item.d.IdUnit == UnitConstant.Sma)
                    {
                        peminatan = JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb[(int)item.b.IdJenisPeminatan];
                    }
                ret = new PpdbBiayaDaftarModel
                {
                    Email = item.a.Email,
                    IdJalurPendaftaran = item.a.IdJalurPendaftaran,
                    IdJenisKategoriPendaftaran = item.a.IdJenisKategoriPendaftaran,
                    IdJenisPendaftaran = item.a.IdJenisPendaftaran,
                    IdPpdbDaftar = item.a.IdPpdbDaftar,
                    IdPpdbKelas = item.a.IdPpdbKelas,
                    KdJenisKelamin = item.a.KdJenisKelamin,
                    Nama = item.a.Nama,
                    NoHandphone = item.a.NoHandphone,
                    Pin = item.a.Pin,
                    NikOrtu = item.a.NikOrtu,
                    NisSiswa = item.a.NisSiswa,
                    NisSodara = item.a.NisSodara,
                    Status = StatusPpdbDaftarConstant.Dict[item.a.Status],
                    JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[item.a.IdJalurPendaftaran],
                    JenisKategoriPendaftaran = KategoriPendaftaranPpdbConstant.DictKategoriPendaftaranPpdb[item.a.IdJenisKategoriPendaftaran],
                    JenisPendaftaran = JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[item.a.IdJenisPendaftaran],
                    TempatLahir = item.a.TempatLahir,
                    TanggalLahir = item.a.TanggalLahir.ToString("dd-MM-yyyy"),
                    Peminatan = peminatan,
                    Sekolah = item.e.Nama,
                    Unit = item.e.Nama,
                    Kelas = item.c.Nama,
                    RefNo = item.b1.RefNo,
                    StatusLunas = StatusLunasConstant.Dict[item.b1.Status],
                    TanggalLunas = item.b1.TanggalLunas == null ? "" : ((DateTime)item.b1.TanggalLunas).ToString("dd-MM-yyyy HH:mm:ss"),
                    Catatan = item.b1.Catatan,
                    RpTotal = 0,
                    TagPpdb = new List<PpdbBiayaDaftarDetilModel>()
                };

                var dataDetil = from a in conn.GetList<TbPpdbBiayaDaftarDetil>()
                                join b in conn.GetList<TbJenisBiayaPpdb>() on a.IdJenisBiayaPpdb equals b.IdJenisBiayaPpdb
                                where a.IdPpdbDaftar == ret.IdPpdbDaftar
                                select new { a, b };
                foreach (var itemDetil in dataDetil)
                {
                    ret.TagPpdb.Add(new PpdbBiayaDaftarDetilModel
                    {
                        JenisTagihan = itemDetil.b.Nama,
                        Rp = itemDetil.a.Rp
                    });
                    ret.RpTotal += itemDetil.a.Rp;
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbDaftarBiaya", ex);
                return null;
            }
        }
        public string SetLunasBiayaDaftar(IDbConnection conn, int IdUser, string RefNo, int IdPpdbDaftar, DateTime TanggalLunas, double Rp, string Catatan)
        {
            try
            {
                this.SetCurrentPPdb();
                var item = (from a in conn.GetList<TbPpdbBiayaDaftar>()
                            where a.IdPpdbDaftar == IdPpdbDaftar
                            select a).FirstOrDefault();
                if (item == null)
                    return "data tidak ada";
                item.TanggalLunas = TanggalLunas;
                item.Status = StatusLunasConstant.Lunas;
                item.UpdatedBy = IdUser;
                item.UpdatedDate = dateTimeService.GetCurrdate();
                item.RefNo = RefNo;
                item.Catatan = Catatan;
                conn.Update(item);

                //send mail
                var itemDaftar = (from a in conn.GetList<TbPpdbDaftar>()
                                  join a1 in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals a1.IdPpdbKelas
                                  join b in conn.GetList<TbKelas>() on a1.IdKelas equals b.IdKelas
                                  join c in conn.GetList<TbUnit>() on b.IdUnit equals c.IdUnit
                                  join d in conn.GetList<TbSekolah>() on c.IdSekolah equals d.IdSekolah
                                  where a.IdPpdbDaftar == IdPpdbDaftar
                                  select new { a, a1, b, c, d }).FirstOrDefault();
                TbPpdbDaftar tbPpdbDaftar = itemDaftar.a;
                Random generator = new Random();
                string pin = generator.Next(0, 999999).ToString("D6");
                tbPpdbDaftar.Pin = pin;
                conn.Update(tbPpdbDaftar);

                string _messageBody1 = string.Empty;
                string _messageBody2 = string.Empty;
                string _subjects = "PPDB";

                List<string> emailTos = new List<string>();
                List<string> emailCcs = new List<string>();
                List<string> _attachments = new List<string>();

                int jenisRedaksiEmail = JenisRedaksiEmailPpdbConstant.LunasDaftar;
                _subjects = JenisRedaksiEmailPpdbConstant.Dict[jenisRedaksiEmail];
                string oMessage = GetRedaksiEmailPpdbBody(jenisRedaksiEmail, out _messageBody1, out _messageBody2);
                if (!string.IsNullOrEmpty(oMessage))
                    return string.Empty;
                _messageBody1 = _messageBody1.Replace("[NOVA]", IdPpdbDaftar.ToString());
                _messageBody1 = _messageBody1.Replace("[PIN]", pin);
                _messageBody2 = _messageBody2.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                _messageBody2 += Environment.NewLine + string.Format("==================<br />");
                _messageBody2 += Environment.NewLine + string.Format("No. Virtual Akun: {0}<br/>Biaya Pendaftaran: {1} <br/>PIN: <b>{2}</b><br />", IdPpdbDaftar, commonService.SetStrRupiah(Rp), pin);
                _messageBody2 += Environment.NewLine + string.Format("==================<br />");
                _messageBody2 += Environment.NewLine;
                emailTos.Add(itemDaftar.a.Email);
                emailCcs.Add("ppdb.sekolahattaufiqbogor@gmail.com"); //cc ke bagian keuangan
                string _emailMessage = SetHtmlEmailPpdbMessage(
                    _messageBody1,
                    _messageBody2,
                    itemDaftar.a.IdPpdbDaftar,
                    itemDaftar.c.Nama,
                    JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[itemDaftar.a.IdJalurPendaftaran],
                    JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[itemDaftar.a.IdJenisPendaftaran],
                    itemDaftar.a.Nama,
                    itemDaftar.a.TanggalLahir.ToString("dd-MM-yyyy"),
                    currPpdb.TahunAjaran,
                    itemDaftar.d.Nama,
                    currPpdb.NamaKetua);

                SendEmailModel email = new SendEmailModel
                {
                    To = emailTos,
                    Cc = emailCcs,
                    Title = "Bapak/Ibu ",
                    Subject = _subjects,
                    Message = _emailMessage,
                    Attachments = _attachments
                };
                _ = emailService.SendHtmlEmailAsync(email);
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetLunasBiayaDaftar", ex);
            }
        }
        public PpdbDaftarModel GetPpdbDaftar(int IdPpdbDaftar, string Pin, out string oMessage)
        {
            try
            {
                if (string.IsNullOrEmpty(Pin)) { oMessage = "PIN harus diisi"; return null; }
                using (var conn = commonService.DbConnection())
                {
                    var item = conn.Get<TbPpdbDaftar>(IdPpdbDaftar);
                    if (item == null) { oMessage = "data tidak ada"; return null; }
                    if (item.Pin != Pin) { oMessage = "PIN salah"; return null; }

                    return GetPpdbDaftar(conn, IdPpdbDaftar, out oMessage);
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbDaftar", ex);
                return null;
            }
        }
        public PpdbDaftarModel GetPpdbDaftar(IDbConnection conn, int IdPpdbDaftar, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                var item = (from a in conn.GetList<TbPpdbDaftar>()
                            join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                            join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                            join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                            join e in conn.GetList<TbSekolah>() on d.IdSekolah equals e.IdSekolah
                            where a.IdPpdbDaftar == IdPpdbDaftar
                            select new { a, b, c, d, e }).FirstOrDefault();
                if (item == null) { oMessage = "data tidak ada"; return null; }
                string peminatan = string.Empty;
                if (item.b.IdJenisPeminatan != null)
                    if (item.d.IdUnit == UnitConstant.Sma)
                    {
                        peminatan = JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb[(int)item.b.IdJenisPeminatan];
                    }
                PpdbDaftarModel ret = new PpdbDaftarModel
                {
                    Email = item.a.Email,
                    IdJalurPendaftaran = item.a.IdJalurPendaftaran,
                    IdJenisKategoriPendaftaran = item.a.IdJenisKategoriPendaftaran,
                    IdJenisPendaftaran = item.a.IdJenisPendaftaran,
                    IdPpdbDaftar = item.a.IdPpdbDaftar,
                    IdPpdbKelas = item.a.IdPpdbKelas,
                    KdJenisKelamin = item.a.KdJenisKelamin,
                    Nama = item.a.Nama,
                    NoHandphone = item.a.NoHandphone,
                    Pin = item.a.Pin,
                    NikOrtu = item.a.NikOrtu,
                    NisSiswa = item.a.NisSiswa,
                    NisSodara = item.a.NisSodara,
                    Status = StatusPpdbDaftarConstant.Dict[item.a.Status],
                    JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[item.a.IdJalurPendaftaran],
                    JenisKategoriPendaftaran = KategoriPendaftaranPpdbConstant.DictKategoriPendaftaranPpdb[item.a.IdJenisKategoriPendaftaran],
                    JenisPendaftaran = JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[item.a.IdJenisPendaftaran],
                    TempatLahir = item.a.TempatLahir,
                    TanggalLahir = item.a.TanggalLahir.ToString("dd-MM-yyyy"),
                    Peminatan = peminatan,
                    Sekolah = item.e.Nama,
                    IdUnit = item.d.IdUnit,
                    Unit = item.d.Nama,
                    Kelas = item.c.Nama
                };
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbDaftar", ex);
                return null;
            }
        }
        public string InputFormulir(
            int IdUser,
            PpdbSiswaAddModel Siswa,
            List<PpdbSiswaOrtuAddModel> Ortus,
            List<PpdbSiswaRaporAddModel> Rapors,
            List<PpdbSiswaPrestasiAddModel> Prestasis,
            List<PpdbKuisionerJawabanAddModel> Kuisioners,
            List<PpdbBiayaSiswaDetilAddModel> Biayas,
            IFormFile FileFoto,
            IFormFile FileKtpIbu,
            IFormFile FileKtpAyah,
            IFormFile FileKk,
            IFormFile FileAkte,
            IFormFile FileSuratKesehatan,
            IFormFile FileBebasNarkoba,
            List<IFormFile> FileRapors,
            List<IFormFile> FilePrestasis,
            PpdbSiswaKkModel SiswaKk)
        {
            try
            {
                string err = this.ValidateDataSiswa(Siswa);
                if (!string.IsNullOrEmpty(err)) return err;
                err = this.ValidateDataOrtus(Ortus);
                if (!string.IsNullOrEmpty(err)) return err;
                if (Kuisioners == null || Kuisioners.Count() == 0) return "kuisioner harus diisi";
                if (Biayas == null || Biayas.Count() == 0) return "biaya harus diisi";

                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var ppdbDaftar = (from a in conn.GetList<TbPpdbDaftar>()
                                          join b in conn.GetList<TbPpdbBiayaDaftar>() on a.IdPpdbDaftar equals b.IdPpdbDaftar
                                          join c in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals c.IdPpdbKelas
                                          join d in conn.GetList<TbKelas>() on c.IdKelas equals d.IdKelas
                                          join e in conn.GetList<TbUnit>() on d.IdUnit equals e.IdUnit
                                          join f in conn.GetList<TbSekolah>() on e.IdSekolah equals f.IdSekolah
                                          where a.IdPpdbDaftar == Siswa.IdPpdbDaftar
                                          select new { a, b, c, d, e, f }).FirstOrDefault();
                        if (ppdbDaftar == null) return "data tidak ada";

                        List<string> emailTos = new List<string>();
                        emailTos.Add(ppdbDaftar.a.Email);
                        TbPpdbSiswa tbPpdbSiswa = new TbPpdbSiswa
                        {
                            IdPpdbDaftar = ppdbDaftar.a.IdPpdbDaftar,
                            IdPpdbKelas = ppdbDaftar.a.IdPpdbKelas,
                            IdJenisKategoriPendaftaran = ppdbDaftar.a.IdJenisKategoriPendaftaran,
                            IdJenisPendaftaran = ppdbDaftar.a.IdJenisPendaftaran,
                            IdJalurPendaftaran = ppdbDaftar.a.IdJalurPendaftaran,
                            TanggalLahir = ppdbDaftar.a.TanggalLahir,
                            TerimaKip = Siswa.TerimaKip,
                            AlasanKip = Siswa.AlasanKip,
                            IdCita2 = Siswa.IdCita2,
                            IdHobi = Siswa.IdHobi
                        };

                        //cek rapor smp dan sma
                        if (ppdbDaftar.d.IdUnit == UnitConstant.Smp || ppdbDaftar.d.IdUnit == UnitConstant.Sma)
                        {
                            if (Rapors == null)
                            {
                                string unitAsal = UnitConstant.DictUnit[UnitConstant.Sd];
                                if (ppdbDaftar.d.IdUnit == UnitConstant.Sma)
                                    unitAsal = UnitConstant.DictUnit[UnitConstant.Smp];
                                return string.Format("nilai rapor {0} harus diisi", unitAsal);
                            }
                            // if (Rapors.Count() != FileRapors.Count())
                            //     return "semua rapor harus diupload";
                            if (FileRapors.Count() != 4)
                                return "semua rapor harus diupload";
                        }
                        else
                        {
                            Rapors = null;
                        }
                        //cek nis siswa & sodara siswa
                        if (!string.IsNullOrEmpty(ppdbDaftar.a.NisSiswa))
                        {
                            var siswa = (from a in conn.GetList<TbSiswa>()
                                         where a.Nis == ppdbDaftar.a.NisSiswa
                                         select a).FirstOrDefault();
                            if (siswa == null)
                                return string.Format("nis {0} siswa tidak terdaftar", ppdbDaftar.a.NisSiswa);
                            tbPpdbSiswa.IdSiswa = siswa.IdSiswa;
                            var tagSiswa = this.GetSiswaTagihan(conn, ppdbDaftar.a.NisSiswa, out string errTag);
                            if (string.IsNullOrEmpty(errTag))
                            {
                                if (tagSiswa.RpSebelumnya > 0)
                                    return string.Format("input formulir belum bisa dilanjutkan, mohon hubungi bagian keuangan");
                            }
                        }

                        if (!string.IsNullOrEmpty(ppdbDaftar.a.NisSodara))
                        {
                            var siswa = (from a in conn.GetList<TbSiswa>()
                                         where a.Nis == ppdbDaftar.a.NisSodara
                                         select a).FirstOrDefault();
                            if (siswa == null)
                                return string.Format("nis {0} sodara siswa tidak terdaftar", ppdbDaftar.a.NisSiswa);
                            tbPpdbSiswa.IdSiswaSodara = siswa.IdSiswa;


                            var tagSiswa = this.GetSiswaTagihan(conn, ppdbDaftar.a.NisSodara, out string errTag);
                            if (string.IsNullOrEmpty(errTag))
                            {
                                if (tagSiswa.RpSebelumnya > 0)
                                    return string.Format("input formulir belum bisa dilanjutkan, silahkan hubungi bagian keuangan");
                            }
                        }

                        tbPpdbSiswa.IdPpdbSeragam = Siswa.IdPpdbSeragam;
                        tbPpdbSiswa.Nik = Siswa.Nik;
                        tbPpdbSiswa.Nisn = Siswa.Nisn;
                        tbPpdbSiswa.Nama = Siswa.Nama;
                        tbPpdbSiswa.NamaPanggilan = Siswa.NamaPanggilan;
                        tbPpdbSiswa.TempatLahir = Siswa.TempatLahir;
                        tbPpdbSiswa.IdAgama = Siswa.IdAgama;
                        tbPpdbSiswa.IdKewarganegaraan = Siswa.IdKewarganegaraan;
                        tbPpdbSiswa.KdJenisKelamin = Siswa.KdJenisKelamin;
                        tbPpdbSiswa.KdGolonganDarah = Siswa.KdGolonganDarah;
                        tbPpdbSiswa.AlamatTinggal = Siswa.AlamatTinggal;
                        tbPpdbSiswa.IdTinggal = Siswa.IdTinggal;
                        tbPpdbSiswa.Email = Siswa.Email;
                        tbPpdbSiswa.NoHandphone = Siswa.NoHandphone;
                        tbPpdbSiswa.NoDarurat = Siswa.NoDarurat;
                        tbPpdbSiswa.IdBahasa = Siswa.IdBahasa;
                        tbPpdbSiswa.TinggiBadan = Siswa.TinggiBadan;
                        tbPpdbSiswa.BeratBadan = Siswa.BeratBadan;
                        tbPpdbSiswa.AnakKe = Siswa.AnakKe;
                        tbPpdbSiswa.JumlahSodaraKandung = Siswa.JumlahSodaraKandung;
                        tbPpdbSiswa.JumlahSodaraTiri = Siswa.JumlahSodaraTiri;
                        tbPpdbSiswa.JumlahSodaraAngkat = Siswa.JumlahSodaraAngkat;
                        tbPpdbSiswa.IdTransportasi = Siswa.IdTransportasi;
                        tbPpdbSiswa.JarakKeSekolah = Siswa.JarakKeSekolah;
                        tbPpdbSiswa.WaktuTempuh = Siswa.WaktuTempuh;
                        tbPpdbSiswa.PenyakitDiderita = Siswa.PenyakitDiderita;
                        tbPpdbSiswa.KelainanJasmani = Siswa.KelainanJasmani;
                        tbPpdbSiswa.SekolahAsal = Siswa.SekolahAsal;
                        tbPpdbSiswa.AlamatSekolahAsal = Siswa.AlamatSekolahAsal;
                        tbPpdbSiswa.NspnSekolahAsal = Siswa.NspnSekolahAsal;

                        List<TbPpdbSiswaOrtu> tbPpdbSiswaOrtus = new List<TbPpdbSiswaOrtu>();
                        foreach (var ortu in Ortus)
                        {
                            if (!string.IsNullOrEmpty(ortu.Email))
                            {
                                if (!emailTos.Contains(ortu.Email.ToLower()))
                                    emailTos.Add(ortu.Email.ToLower());
                            }
                            var tbAnak = (from a in conn.GetList<TbSiswa>()
                                          where a.EmailOrtu == ortu.Email
                                         || a.NikAyah == ortu.Nik
                                         || a.NikIbu == ortu.Nik
                                          select a).FirstOrDefault();
                            if (tbAnak != null)
                            {
                                var tagSiswa = this.GetSiswaTagihan(conn, tbAnak.Nis, out string errTag);
                                if (string.IsNullOrEmpty(errTag))
                                {
                                    if (tagSiswa.RpSebelumnya > 0)
                                        return string.Format("input formulir belum bisa dilanjutkan, bapak/ibu mohon hubungi bagian keuangan");
                                }

                            }


                            TbPpdbSiswaOrtu tbPpdbDaftarOrtu = new TbPpdbSiswaOrtu
                            {
                                IdPpdbDaftar = tbPpdbSiswa.IdPpdbDaftar,
                                IdTipe = ortu.IdTipe,
                                Nama = ortu.Nama,
                                Alamat = ortu.Alamat,
                                Nik = ortu.Nik,
                                IdJenisPekerjaan = ortu.IdJenisPekerjaan,
                                NamaInstansi = ortu.NamaInstansi,
                                Jabatan = ortu.Jabatan,
                                Email = ortu.Email,
                                NoHandphone = ortu.NoHandphone,
                                NoTelpRumah = ortu.NoTelpRumah,
                                IdAgama = ortu.IdAgama,
                                IdKewarganegaraan = ortu.IdKewarganegaraan,
                                IdJenisJenjangPendidikan = ortu.IdJenjangPendidikan,
                                IdStatusPenikahan = ortu.IdStatusPenikahan,
                                TempatLahir = ortu.TempatLahir,
                                TanggalLahir = dateTimeService.StrToDateTime(ortu.TanggalLahir),
                                IdStatusHidup = ortu.IdStatusHidup
                            };
                            if (ortu.IdStatusHidup != 1)
                                tbPpdbDaftarOrtu.TanggalMeninggal = dateTimeService.StrToDateTime(ortu.TanggalMeninggal);

                            tbPpdbSiswaOrtus.Add(tbPpdbDaftarOrtu);
                            var dtPegawai = (from a in conn.GetList<TbPegawai>()
                                             where a.Nik == ortu.Nik
                                             select a).FirstOrDefault();
                            if (dtPegawai != null)
                                tbPpdbSiswa.IdPegawai = dtPegawai.IdPegawai;
                            //harusnya disini
                        }
                        conn.Insert(tbPpdbSiswa);

                        if (SiswaKk != null)
                        {
                            TbPpdbSiswaKk tbPpdbSiswaKk = new TbPpdbSiswaKk
                            {
                                IdPpdbDaftar = tbPpdbSiswa.IdPpdbDaftar,
                                NoKk = SiswaKk.NoKk,
                                Alamat = SiswaKk.Alamat,
                                Rt = SiswaKk.Rt,
                                Rw = SiswaKk.Rw,
                                Dusun = SiswaKk.Dusun,
                                IdWilDesa = SiswaKk.IdWilDesa,
                                KdPos = SiswaKk.KdPos,
                                NoTelp = SiswaKk.NoTelp,
                                Longitude = SiswaKk.Longitude,
                                Latitude = SiswaKk.Latitude
                            };
                            conn.Insert(tbPpdbSiswaKk);
                        }

                        TbPpdbSiswaStatus tbPpdbSiswaStatus = new TbPpdbSiswaStatus
                        {
                            IdPpdbDaftar = tbPpdbSiswa.IdPpdbDaftar,
                            Status = StatusPpdbConstant.Registrasi,
                            StatusProses = 0,
                            CatatanProses = null,
                            CreatedBy = IdUser,
                            CreatedDate = dateTimeService.GetCurrdate()
                        };
                        conn.Insert(tbPpdbSiswaStatus);

                        foreach (var tbPpdbSiswaOrtu in tbPpdbSiswaOrtus)
                        {
                            conn.Insert(tbPpdbSiswaOrtu);
                        }
                        string _fileName = string.Empty;
                        //rapor
                        if (Rapors != null)
                        {
                            foreach (var item in Rapors)
                            {
                                TbPpdbSiswaRapor tbPpdbSiswaRapor = new TbPpdbSiswaRapor
                                {
                                    IdPpdbDaftar = tbPpdbSiswa.IdPpdbDaftar,
                                    Mapel = item.Mapel,
                                    Semester = item.Semester,
                                    Nilai = item.Nilai,
                                    FileRapor = string.Format("{0}_S{1}_{2}", tbPpdbSiswa.IdPpdbDaftar, item.Semester, NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.Raport])
                                };
                                conn.Insert(tbPpdbSiswaRapor);
                            }

                            int i = 0;
                            foreach (var item in FileRapors)
                            {
                                Console.WriteLine(item);
                                i++;
                                _fileName = string.Empty;
                                _fileName = string.Format("{0}_S{1}_{2}",
                                    tbPpdbSiswa.IdPpdbDaftar,
                                    i,
                                    NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.Raport]);

                                string _pathFileUpload = AppSetting.PathPpdbBerkas;
                                _fileName += Path.GetExtension(item.FileName);
                                _fileName = Path.Combine(_pathFileUpload, _fileName);
                                using (var fileStream = new FileStream(_fileName, FileMode.Create))
                                {
                                    item.CopyTo(fileStream);
                                }
                            }
                        }
                        //prestasi
                        if (Prestasis != null)
                        {
                            if (Prestasis.Count() != FilePrestasis.Count())
                                return "semua sertifikat prestasi harus diupload";
                            int i = 0;
                            foreach (var item in Prestasis)
                            {
                                _fileName = string.Empty;
                                if (FilePrestasis[i] == null || FilePrestasis[i].Length == 0)
                                    return "semua sertifikat pretasi harus diupload";
                                _fileName = string.Format("{0}_{1}_{2}",
                                    tbPpdbSiswa.IdPpdbDaftar,
                                    i,
                                    NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.BuktiPrestasi]);

                                TbPpdbSiswaPrestasi tbPpdbSiswaPrestasi = new TbPpdbSiswaPrestasi
                                {
                                    IdPpdbDaftar = tbPpdbSiswa.IdPpdbDaftar,
                                    IdJenisPrestasi = item.IdJenisPrestasi,
                                    IdJenisTingkatPrestasi = item.IdJenisTingkatPrestasi,
                                    NamaPenyelenggara = item.NamaPenyelenggara,
                                    Tahun = item.Tahun,
                                    Keterangan = item.Keterangan,
                                    FileSertifikat = _fileName
                                };
                                conn.Insert(tbPpdbSiswaPrestasi);

                                string _pathFileUpload = AppSetting.PathPpdbBerkas;
                                _fileName += Path.GetExtension(FilePrestasis[i].FileName);
                                _fileName = Path.Combine(_pathFileUpload, _fileName);
                                using (var fileStream = new FileStream(_fileName, FileMode.Create))
                                {
                                    FilePrestasis[i].CopyTo(fileStream);
                                }
                                ++i;
                            }
                        }
                        //kuisioner
                        if (Kuisioners != null)
                        {
                            var pertanyaans = from a in conn.GetList<TbPpdbKuisioner>()
                                              join b in conn.GetList<TbPpdbKuisionerPertanyaan>() on a.IdPpdbKuisionerPertanyaan equals b.IdPpdbKuisionerPertanyaan
                                              where a.IdUnit == ppdbDaftar.d.IdUnit
                                              & a.IdJenisPendaftaran == ppdbDaftar.a.IdJenisPendaftaran
                                              & a.IdJalurPendaftaran == ppdbDaftar.a.IdJalurPendaftaran
                                              select b;
                            foreach (var pertanyaan in pertanyaans)
                            {
                                var jawaban = Kuisioners.Where(x => x.IdPpdbKuisionerPertanyaan == pertanyaan.IdPpdbKuisionerPertanyaan).FirstOrDefault();
                                if (jawaban == null)
                                    return pertanyaan.Pertanyaan + " HARUS DIJAWAB!";
                                switch (pertanyaan.IdJenisPertanyaan)
                                {
                                    case 1:
                                    case 2:
                                    case 3:
                                    case 5:
                                        if (string.IsNullOrEmpty(jawaban.OpsiJawaban))
                                            return "Pertanyaan " + pertanyaan.Pertanyaan + " HARUS DIJAWAB!";
                                        break;
                                    default:
                                        if (string.IsNullOrEmpty(jawaban.Jawaban))
                                            return "Pertanyaan " + pertanyaan.Pertanyaan + " HARUS DIJAWAB!";
                                        break;
                                }
                                TbPpdbKuisionerJawaban tbPpdbKuisionerJawaban = new TbPpdbKuisionerJawaban
                                {
                                    IdPpdbDaftar = tbPpdbSiswa.IdPpdbDaftar,
                                    IdPpdbKuisionerPertanyaan = pertanyaan.IdPpdbKuisionerPertanyaan,
                                    Jawaban = jawaban.Jawaban,
                                    OpsiJawaban = jawaban.OpsiJawaban
                                };
                                conn.Insert(tbPpdbKuisionerJawaban);
                            }
                        }
                        //biaya
                        TbPpdbBiayaSiswa tbPpdbBiayaSiswa = new TbPpdbBiayaSiswa
                        {
                            IdPpdbDaftar = tbPpdbSiswa.IdPpdbDaftar,
                            IsCicil = 0,
                            Catatan = null,
                            RefNo = null,
                            Status = StatusLunasConstant.BelumLunas,
                            TanggalLunas = null,
                            UpdatedBy = null,
                            UpdatedDate = null
                        };
                        conn.Insert(tbPpdbBiayaSiswa);
                        foreach (var item in Biayas)
                        {
                            TbPpdbBiayaSiswaDetil tbPpdbBiayaSiswaDetil = new TbPpdbBiayaSiswaDetil
                            {
                                IdPpdbDaftar = tbPpdbBiayaSiswa.IdPpdbDaftar,
                                IdJenisBiayaPpdb = item.IdJenisBiayaPpdb,
                                Catatan = item.Catatan,
                                Rp = item.Rp
                            };
                            conn.Insert(tbPpdbBiayaSiswaDetil);
                        }
                        //file-file
                        string nametagFoto = null;
                        _fileName = string.Empty;
                        if (FileFoto != null && FileFoto.Length > 0)
                        {
                            _fileName = string.Format("{0}{1}",
                                    tbPpdbSiswa.IdPpdbDaftar,
                                    NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.PasFoto]);
                            string _pathFileUpload = AppSetting.PathPpdbBerkas;
                            _fileName += Path.GetExtension(FileFoto.FileName);
                            _fileName = Path.Combine(_pathFileUpload, _fileName);
                            nametagFoto = _fileName;
                            using (var fileStream = new FileStream(_fileName, FileMode.Create))
                            {
                                FileFoto.CopyTo(fileStream);
                            }
                        }

                        _fileName = string.Empty;
                        if (FileKtpIbu != null && FileKtpIbu.Length > 0)
                        {
                            _fileName = string.Format("{0}{1}",
                                    tbPpdbSiswa.IdPpdbDaftar,
                                    NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KtpIbu]);
                            string _pathFileUpload = AppSetting.PathPpdbBerkas;
                            _fileName += Path.GetExtension(FileKtpIbu.FileName);
                            _fileName = Path.Combine(_pathFileUpload, _fileName);
                            using (var fileStream = new FileStream(_fileName, FileMode.Create))
                            {
                                FileKtpIbu.CopyTo(fileStream);
                            }
                        }


                        _fileName = string.Empty;
                        if (FileKtpAyah != null && FileKtpAyah.Length > 0)
                        {
                            _fileName = string.Format("{0}{1}",
                                    tbPpdbSiswa.IdPpdbDaftar,
                                    NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KtpAyah]);
                            string _pathFileUpload = AppSetting.PathPpdbBerkas;
                            _fileName += Path.GetExtension(FileKtpAyah.FileName);
                            _fileName = Path.Combine(_pathFileUpload, _fileName);
                            using (var fileStream = new FileStream(_fileName, FileMode.Create))
                            {
                                FileKtpAyah.CopyTo(fileStream);
                            }
                        }

                        _fileName = string.Empty;
                        if (FileKk != null && FileKk.Length > 0)
                        {
                            _fileName = string.Format("{0}{1}",
                                    tbPpdbSiswa.IdPpdbDaftar,
                                    NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuKeluarga]);
                            string _pathFileUpload = AppSetting.PathPpdbBerkas;
                            _fileName += Path.GetExtension(FileKk.FileName);
                            _fileName = Path.Combine(_pathFileUpload, _fileName);
                            using (var fileStream = new FileStream(_fileName, FileMode.Create))
                            {
                                FileKk.CopyTo(fileStream);
                            }
                        }

                        _fileName = string.Empty;
                        if (FileAkte != null && FileAkte.Length > 0)
                        {
                            _fileName = string.Format("{0}{1}",
                                    tbPpdbSiswa.IdPpdbDaftar,
                                    NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.Akte]);
                            string _pathFileUpload = AppSetting.PathPpdbBerkas;
                            _fileName += Path.GetExtension(FileAkte.FileName);
                            _fileName = Path.Combine(_pathFileUpload, _fileName);
                            using (var fileStream = new FileStream(_fileName, FileMode.Create))
                            {
                                FileAkte.CopyTo(fileStream);
                            }
                        }

                        _fileName = string.Empty;
                        if (FileSuratKesehatan != null && FileSuratKesehatan.Length > 0)
                        {
                            _fileName = string.Format("{0}{1}",
                                    tbPpdbSiswa.IdPpdbDaftar,
                                    NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.SuratKesehatan]);
                            string _pathFileUpload = AppSetting.PathPpdbBerkas;
                            _fileName += Path.GetExtension(FileSuratKesehatan.FileName);
                            _fileName = Path.Combine(_pathFileUpload, _fileName);
                            using (var fileStream = new FileStream(_fileName, FileMode.Create))
                            {
                                FileSuratKesehatan.CopyTo(fileStream);
                            }
                        }

                        _fileName = string.Empty;
                        if (FileBebasNarkoba != null && FileBebasNarkoba.Length > 0)
                        {
                            _fileName = string.Format("{0}{1}",
                                    tbPpdbSiswa.IdPpdbDaftar,
                                    NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KetBebasNarkoba]);
                            string _pathFileUpload = AppSetting.PathPpdbBerkas;
                            _fileName += Path.GetExtension(FileBebasNarkoba.FileName);
                            _fileName = Path.Combine(_pathFileUpload, _fileName);
                            using (var fileStream = new FileStream(_fileName, FileMode.Create))
                            {
                                FileBebasNarkoba.CopyTo(fileStream);
                            }
                        }

                        //set jadwal observasi
                        var jadwalObservasis = from a in conn.GetList<TbPpdbJadwalObservasi>()
                                               join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                               join c in conn.GetList<TbUser>() on a.IdPegawai equals c.IdUser
                                               join d in conn.GetList<TbUser>() on a.IdPegawai equals d.IdUser
                                               where a.IdUnit == ppdbDaftar.e.IdUnit
                                                & a.JumlahKuota > a.JumlahSiswa
                                               select new { a, b, c, d };
                        int idPpdbJadwalObservasi = 0;
                        string tanggalObservasi = "";
                        string jamAwalObservasi = "";
                        string jamAkhirObservasi = "";
                        string petugasObservasi = "";
                        string emailPetugasObservasi = "";
                        string noHpPetugasObservasi = "";

                        foreach (var item in jadwalObservasis.ToList().OrderBy(x => x.a.Tanggal).ThenBy(x => x.a.JamAwal))
                        {
                            idPpdbJadwalObservasi = item.a.IdPpdbJadwalObservasi;
                            tanggalObservasi = item.a.Tanggal.ToString("dd-MM-yyyy");
                            jamAwalObservasi = item.a.JamAwal.ToString("hh':'mm");
                            jamAkhirObservasi = item.a.JamAwal.ToString("hh':'mm");
                            petugasObservasi = userAppService.SetFullName(item.d.FirstName, item.d.MiddleName, item.d.LastName);
                            emailPetugasObservasi = item.c.Email;
                            noHpPetugasObservasi = item.d.MobileNumber;

                            TbPpdbJadwalObservasi tbPpdbJadwalObservasi = item.a;
                            tbPpdbJadwalObservasi.JumlahSiswa += tbPpdbJadwalObservasi.JumlahSiswa;
                            conn.Update(tbPpdbJadwalObservasi);
                            break;
                        }
                        TbPpdbSiswaObservasi tbPpdbSiswaObservasi = new TbPpdbSiswaObservasi
                        {
                            IdPpdbDaftar = ppdbDaftar.a.IdPpdbDaftar,
                            IdPpdbJadwalObservasi = idPpdbJadwalObservasi
                        };
                        conn.Insert(tbPpdbSiswaObservasi);
                        emailTos.Add(emailPetugasObservasi);

                        var jadwalWawancaras = from a in conn.GetList<TbPpdbJadwalWawancara>()
                                               join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                               join c in conn.GetList<TbUser>() on a.IdPegawai equals c.IdUser
                                               join d in conn.GetList<TbUser>() on a.IdPegawai equals d.IdUser
                                               where a.IdUnit == ppdbDaftar.e.IdUnit
                                                & a.JumlahKuota > a.JumlahSiswa
                                               select new { a, b, c, d };
                        int idPpdbJadwalWawancara = 0;
                        string tanggalWawancara = "";
                        string jamAwalWawancara = "";
                        string jamAkhirWawancara = "";
                        string petugasWawancara = "";
                        string emailPetugasWawancara = "";
                        string noHpPetugasWawancara = "";
                        foreach (var item in jadwalWawancaras.ToList().OrderBy(x => x.a.Tanggal).ThenBy(x => x.a.JamAwal))
                        {
                            idPpdbJadwalWawancara = item.a.IdPpdbJadwalWawancara;
                            tanggalWawancara = item.a.Tanggal.ToString("dd-MM-yyyy");
                            jamAwalWawancara = item.a.JamAwal.ToString("hh':'mm");
                            jamAkhirWawancara = item.a.JamAwal.ToString("hh':'mm");
                            petugasWawancara = userAppService.SetFullName(item.d.FirstName, item.d.MiddleName, item.d.LastName);
                            emailPetugasWawancara = item.c.Email;
                            noHpPetugasWawancara = item.d.MobileNumber;

                            TbPpdbJadwalWawancara tbPpdbJadwalWawancara = item.a;
                            tbPpdbJadwalWawancara.JumlahSiswa += tbPpdbJadwalWawancara.JumlahSiswa;
                            conn.Update(tbPpdbJadwalWawancara);
                            break;
                        }
                        TbPpdbSiswaWawancara tbPpdbSiswaWawancara = new TbPpdbSiswaWawancara
                        {
                            IdPpdbDaftar = ppdbDaftar.a.IdPpdbDaftar,
                            IdPpdbJadwalWawancara = idPpdbJadwalWawancara
                        };
                        conn.Insert(tbPpdbSiswaWawancara);
                        emailTos.Add(emailPetugasWawancara);

                        tx.Commit();

                        //generate name tag
                        PpdbDaftarKartuModel dataNametag = new();
                        dataNametag.Nama = Siswa.Nama;
                        dataNametag.NamaPanggilan = Siswa.NamaPanggilan;
                        dataNametag.JenisKategoriPendaftaran = KategoriPendaftaranPpdbConstant.DictKategoriPendaftaranPpdb[ppdbDaftar.a.IdJenisKategoriPendaftaran];
                        dataNametag.JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[ppdbDaftar.a.IdJalurPendaftaran];
                        dataNametag.NamaPanggilan = Siswa.NamaPanggilan;

                        if (ppdbDaftar.e.IdUnit == UnitConstant.Sma)
                            pdfService.GenerateNameTagSma(dataNametag, nametagFoto);

                        //send mail
                        string _messageBody1 = string.Empty;
                        string _messageBody2 = string.Empty;
                        string _subjects = "PPDB " + ppdbDaftar.a.Nama;

                        List<string> emailCcs = new List<string>();
                        List<string> _attachments = new List<string>();

                        int jenisRedaksiEmail = JenisRedaksiEmailPpdbConstant.JadwalObservasi;
                        _subjects = JenisRedaksiEmailPpdbConstant.Dict[jenisRedaksiEmail];

                        string oMessage = GetRedaksiEmailPpdbBody(jenisRedaksiEmail, out _messageBody1, out _messageBody2);
                        if (!string.IsNullOrEmpty(oMessage))
                            return string.Empty;
                        //hardcode heula
                        string lembarKomitmen = "lembar_komitmen_tkit.pdf";
                        if (ppdbDaftar.e.IdUnit == UnitConstant.Tk)
                        {
                            lembarKomitmen = "lembar_komitmen_tkit.pdf";
                        }
                        else if (ppdbDaftar.e.IdUnit == UnitConstant.Sma)
                        {
                            lembarKomitmen = "lembar_komitmen_smait.pdf";
                        }
                        _messageBody1 = _messageBody1.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                        _messageBody2 = _messageBody2.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);

                        // _messageBody2 = _messageBody2.Replace("[WCRTANGGAL]", tanggalWawancara);
                        // _messageBody2 = _messageBody2.Replace("[WCRWAKTU]", String.Format("{0}", jamAwalWawancara));
                        _messageBody2 = _messageBody2.Replace("[WCRTANGGAL]", " "); //hapus jangan lupa
                        _messageBody2 = _messageBody2.Replace("[WCRWAKTU]", " "); //hapus jangan lupa

                        _messageBody2 = _messageBody2.Replace("[WCRPIC]", petugasWawancara);
                        _messageBody2 = _messageBody2.Replace("[WCREMAIL]", emailPetugasWawancara);
                        _messageBody2 = _messageBody2.Replace("[WCRNOHP]", noHpPetugasWawancara);

                        // _messageBody2 = _messageBody2.Replace("[OBSTANGGAL]", tanggalObservasi);
                        // _messageBody2 = _messageBody2.Replace("[OBSWAKTU]", String.Format("{0}", jamAwalObservasi));
                        _messageBody2 = _messageBody2.Replace("[OBSTANGGAL]", " "); //hapus jangan lupa
                        _messageBody2 = _messageBody2.Replace("[OBSWAKTU]", " "); //hapus jangan lupa

                        _messageBody2 = _messageBody2.Replace("[OBSPIC]", petugasObservasi);
                        _messageBody2 = _messageBody2.Replace("[OBSEMAIL]", emailPetugasObservasi);
                        _messageBody2 = _messageBody2.Replace("[OBSNOHP]", noHpPetugasObservasi);

                        _messageBody2 = _messageBody2.Replace("[FILEKOMITMEN]", lembarKomitmen);
                        _messageBody2 = _messageBody2.Replace("[IDPPDBDAFTAR]", ppdbDaftar.a.IdPpdbDaftar.ToString());

                        //_messageBody2 += Environment.NewLine + string.Format("Tanggal observasi: {0} <br />Jam: {1}<br />", tanggalObservasi, jamAwalWawancara);
                        //_messageBody2 += Environment.NewLine + string.Format("Petugas: {0} <br />Email: {1} <br />No. HP: {2}", petugasObservasi, emailPetugasObservasi, noHpPetugasObservasi);

                        string _emailMessage = SetHtmlEmailPpdbMessage(
                            _messageBody1,
                            _messageBody2,
                            ppdbDaftar.a.IdPpdbDaftar,
                            ppdbDaftar.e.Nama,
                            JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[ppdbDaftar.a.IdJalurPendaftaran],
                            JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[ppdbDaftar.a.IdJenisPendaftaran],
                            ppdbDaftar.a.Nama,
                            ppdbDaftar.a.TanggalLahir.ToString("dd-MM-yyyy"),
                            currPpdb.TahunAjaran,
                            ppdbDaftar.f.Nama,
                            currPpdb.NamaKetua);
                        SendEmailModel email = new SendEmailModel
                        {
                            To = emailTos,
                            Cc = emailCcs,
                            Title = "Bapak/Ibu ",
                            Subject = _subjects,
                            Message = _emailMessage,
                            Attachments = _attachments
                        };
                        _ = emailService.SendHtmlEmailAsync(email);

                        string toEmail = string.Empty;
                        string ccEmail = string.Empty;
                        string log = string.Format("=============  {0} - FORMULIR  =============" + Environment.NewLine, dateTimeService.GetCurrdate());
                        foreach (var item in email.To)
                        {
                            toEmail += item + ",";
                        }
                        foreach (var item in email.Cc)
                        {
                            ccEmail += item + ",";
                        }
                        log += string.Format("No. Pendaftaran : {0}" + Environment.NewLine, tbPpdbSiswa.IdPpdbDaftar);
                        log += string.Format("To : {0}" + Environment.NewLine, toEmail);
                        log += string.Format("Cc : {0}" + Environment.NewLine, ccEmail);
                        log += string.Format("Subject : {0}" + Environment.NewLine, email.Subject);
                        log += string.Format("Title : {0}" + Environment.NewLine, email.Title);
                        log += string.Format("============================================================" + Environment.NewLine + Environment.NewLine);
                        File.AppendAllText(AppSetting.PathApplFile + "/!log_mail.txt", log);
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "InputFormulir", ex);
            }
        }
        public PpdbSiswaModel GetPpdbSiswa(IDbConnection conn, int IdPpdbDaftar, out string oMessage)
        {
            oMessage = string.Empty;
            PpdbSiswaModel ret = new PpdbSiswaModel
            {
                TagPpdb = new List<PpdbBiayaSiswaDetilModel>(),
                TagCicil = new List<PpdbBiayaSiswaCicilModel>(),
                DataOrtu = new List<PpdbSiswaOrtuAddModel>(),
                Prestasi = new List<PpdbSiswaPrestasiModel>(),
                Rapor = new List<PpdbSiswaRaporModel>(),
            };
            try
            {
                var item = (from a in conn.GetList<TbPpdbSiswa>()
                            join b1 in conn.GetList<TbPpdbBiayaSiswa>() on a.IdPpdbDaftar equals b1.IdPpdbDaftar
                            join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                            join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                            join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                            join e in conn.GetList<TbSekolah>() on d.IdSekolah equals e.IdSekolah
                            where a.IdPpdbDaftar == IdPpdbDaftar
                            select new { a, b, b1, c, d, e }).FirstOrDefault();
                if (item == null) { oMessage = "data tidak ada"; return null; }

                //prestasi
                var tbPpdbSiswaPrestasi = (from a in conn.GetList<TbPpdbSiswaPrestasi>()
                                           where a.IdPpdbDaftar == IdPpdbDaftar
                                           select a).ToList();
                foreach (var value in tbPpdbSiswaPrestasi)
                {
                    var FileSertifikat = value.FileSertifikat;
                    string[] getFileSertifikat = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileSertifikat));
                    FileSertifikat = getFileSertifikat.Count() != 0 ? new DirectoryInfo(getFileSertifikat[0]).Name : null;

                    ret.Prestasi.Add(new PpdbSiswaPrestasiModel
                    {
                        IdPpdbSiswaPrestasi = value.IdPpdbSiswaPrestasi,
                        IdPpdbDaftar = value.IdPpdbDaftar,
                        IdJenisPrestasi = value.IdJenisPrestasi,
                        IdJenisTingkatPrestasi = value.IdJenisTingkatPrestasi,
                        JenisPrestasi = JenisPrestasiConstant.DictJenisPrestasi[value.IdJenisPrestasi],
                        JenisTingkatPrestasi = TingkatPrestasiSiswaConstant.DictTingkatPrestasiSiswa[value.IdJenisTingkatPrestasi],
                        Tahun = value.Tahun,
                        NamaPenyelenggara = value.NamaPenyelenggara,
                        Keterangan = value.Keterangan,
                        FileSertifikat = FileSertifikat,
                    });

                }
                //rapor
                var tbPpdbSiswaRapor = (from a in conn.GetList<TbPpdbSiswaRapor>()
                                        where a.IdPpdbDaftar == IdPpdbDaftar
                                        select a).ToList();
                foreach (var value in tbPpdbSiswaRapor)
                {
                    var FileRapor = value.FileRapor;
                    string[] getFileRapor = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileRapor));
                    FileRapor = getFileRapor.Count() != 0 ? new DirectoryInfo(getFileRapor[0]).Name : null;

                    ret.Rapor.Add(new PpdbSiswaRaporModel
                    {
                        Semester = value.Semester,
                        Mapel = value.Mapel,
                        Nilai = value.Nilai,
                        FileRapor = FileRapor
                    });
                }

                ret.IdPpdbDaftar = item.a.IdPpdbDaftar;
                ret.IdPpdbSeragam = item.a.IdPpdbSeragam;
                ret.Nik = item.a.Nik;
                ret.Nisn = item.a.Nisn;
                ret.Nama = item.a.Nama;
                ret.NamaPanggilan = item.a.NamaPanggilan;
                ret.TempatLahir = item.a.TempatLahir;
                ret.IdAgama = item.a.IdAgama;
                ret.IdKewarganegaraan = item.a.IdKewarganegaraan;
                ret.KdJenisKelamin = item.a.KdJenisKelamin;
                ret.KdGolonganDarah = item.a.KdGolonganDarah;
                ret.AlamatTinggal = item.a.AlamatTinggal;
                ret.IdTinggal = item.a.IdTinggal;
                ret.Email = item.a.Email;
                ret.NoHandphone = item.a.NoHandphone;
                ret.NoDarurat = item.a.NoDarurat;
                ret.IdBahasa = item.a.IdBahasa == null ? 0 : (int)item.a.IdBahasa;
                ret.TinggiBadan = item.a.TinggiBadan == null ? 0 : (int)item.a.TinggiBadan;
                ret.BeratBadan = item.a.BeratBadan == null ? 0 : (int)item.a.BeratBadan;
                ret.AnakKe = item.a.AnakKe == null ? 0 : (int)item.a.AnakKe;
                ret.JumlahSodaraKandung = item.a.JumlahSodaraKandung == null ? 0 : (int)item.a.JumlahSodaraKandung;
                ret.JumlahSodaraTiri = item.a.JumlahSodaraTiri == null ? 0 : (int)item.a.JumlahSodaraTiri;
                ret.JumlahSodaraAngkat = item.a.JumlahSodaraAngkat == null ? 0 : (int)item.a.JumlahSodaraAngkat;
                ret.IdTransportasi = item.a.IdTransportasi;
                ret.JarakKeSekolah = item.a.JarakKeSekolah == null ? 0 : (int)item.a.JarakKeSekolah;
                ret.WaktuTempuh = item.a.WaktuTempuh == null ? 0 : (int)item.a.WaktuTempuh;
                ret.PenyakitDiderita = item.a.PenyakitDiderita;
                ret.KelainanJasmani = item.a.KelainanJasmani;
                ret.SekolahAsal = item.a.SekolahAsal;
                ret.AlamatSekolahAsal = item.a.AlamatSekolahAsal;
                ret.NspnSekolahAsal = item.a.NspnSekolahAsal;
                ret.IdPpdbKelas = item.a.IdPpdbKelas;
                ret.IdJenisKategoriPendaftaran = item.a.IdJenisKategoriPendaftaran;
                ret.IdJenisPendaftaran = item.a.IdJenisPendaftaran;
                ret.IdJalurPendaftaran = item.a.IdJalurPendaftaran;
                ret.TanggalLahir = item.a.TanggalLahir.ToString("dd-MM-yyyy");
                ret.IdSiswa = item.a.IdSiswa;
                ret.IdPegawai = item.a.IdPegawai;
                ret.IdSiswaSodara = item.a.IdSiswaSodara;
                ret.Unit = item.d.Nama;
                ret.Kelas = item.c.Nama;
                string peminatan = string.Empty;
                if (item.b.IdJenisPeminatan != null)
                    if (item.d.IdUnit == UnitConstant.Sma)
                    {
                        peminatan = JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb[(int)item.b.IdJenisPeminatan];
                    }
                ret.Peminatan = peminatan;
                ret.JenisKategoriPendaftaran = KategoriPendaftaranPpdbConstant.DictKategoriPendaftaranPpdb[item.a.IdJenisKategoriPendaftaran];
                ret.JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[item.a.IdJalurPendaftaran];
                ret.JenisPendaftaran = JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[item.a.IdJenisPendaftaran];
                if (item.a.IdSiswa != null)
                    ret.SekolahAsal = conn.GetList<TbSekolah>().FirstOrDefault().Nama;
                if (item.a.IdSiswaSodara != null)
                    ret.NamaSodara = conn.Get<TbSiswa>(item.a.IdSiswaSodara).Nama;
                if (item.a.IdPegawai != null)
                {
                    ret.NamaPegawai = (from a in conn.GetList<TbUser>()
                                       where a.IdUser == item.a.IdPegawai
                                       select userAppService.SetFullName(a.FirstName, a.MiddleName, a.LastName)).FirstOrDefault();
                }
                // var tbPpdbSiswaStatuss = from a in conn.GetList<TbPpdbSiswaStatus>()
                //                          where a.IdPpdbDaftar == IdPpdbDaftar
                //                          select a;
                // foreach (var tbPpdbSiswaStatus in tbPpdbSiswaStatuss.OrderByDescending(x => x.Status))
                // {
                //     ret.Status = StatusPpdbConstant.Dict[tbPpdbSiswaStatus.Status];
                //     ret.CatatanProses = tbPpdbSiswaStatus.CatatanProses;
                //     if (tbPpdbSiswaStatus.Status == StatusPpdbConstant.TestObservasi)
                //     {
                //         ret.StatusProses = StatusPpdbTestObservasiConstant.Dict[tbPpdbSiswaStatus.StatusProses];
                //     }
                //     else if (tbPpdbSiswaStatus.Status == StatusPpdbConstant.Wawancara)
                //     {
                //         ret.StatusProses = StatusPpdbWawancaraConstant.Dict[tbPpdbSiswaStatus.StatusProses];
                //     }
                //     else if (tbPpdbSiswaStatus.Status == StatusPpdbConstant.TestObservasiWawancara)
                //     {
                //         ret.StatusProses = StatusPpdbTestObservasiWawancaraConstant.Dict[tbPpdbSiswaStatus.StatusProses];
                //     }
                //     else if (tbPpdbSiswaStatus.Status == StatusPpdbConstant.InputObservasi)
                //     {
                //         ret.StatusProses = StatusPpdbHasilObservasiConstant.Dict[tbPpdbSiswaStatus.StatusProses];
                //     }
                //     else if (tbPpdbSiswaStatus.Status == StatusPpdbConstant.VerifikasiObservasi)
                //     {
                //         ret.StatusProses = StatusPpdbVerifikasiObservasiConstant.Dict[tbPpdbSiswaStatus.StatusProses];
                //     }
                // }


                var tbPpdbSiswaStatus = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                         where a.IdPpdbDaftar == IdPpdbDaftar
                                         select a).LastOrDefault();

                ret.Status = StatusPpdbConstant.Dict[tbPpdbSiswaStatus.Status];
                ret.CatatanProses = tbPpdbSiswaStatus.CatatanProses;
                if (tbPpdbSiswaStatus.Status == StatusPpdbConstant.TestObservasi)
                {
                    ret.StatusProses = StatusPpdbTestObservasiConstant.Dict[tbPpdbSiswaStatus.StatusProses];
                }
                else if (tbPpdbSiswaStatus.Status == StatusPpdbConstant.Wawancara)
                {
                    ret.StatusProses = StatusPpdbWawancaraConstant.Dict[tbPpdbSiswaStatus.StatusProses];
                }
                else if (tbPpdbSiswaStatus.Status == StatusPpdbConstant.TestObservasiWawancara)
                {
                    ret.StatusProses = StatusPpdbTestObservasiWawancaraConstant.Dict[tbPpdbSiswaStatus.StatusProses];
                }
                else if (tbPpdbSiswaStatus.Status == StatusPpdbConstant.InputObservasi)
                {
                    ret.StatusProses = StatusPpdbHasilObservasiConstant.Dict[tbPpdbSiswaStatus.StatusProses];
                }
                else if (tbPpdbSiswaStatus.Status == StatusPpdbConstant.VerifikasiObservasi)
                {
                    ret.StatusProses = StatusPpdbVerifikasiObservasiConstant.Dict[tbPpdbSiswaStatus.StatusProses];
                }

                ret.IsCicil = 0;
                ret.IsCicil = item.b1.IsCicil;
                ret.RpBiayaTotal = 0;
                ret.StatusBiaya = StatusLunasConstant.Dict[item.b1.Status];
                ret.CatatanBiaya = item.b1.Catatan;
                ret.RefNo = item.b1.RefNo;
                if (item.b1.TanggalLunas != null)
                    ret.TanggalLunas = ((DateTime)item.b1.TanggalLunas).ToString("dd-MM-yyyy HH:mm:ss");
                var biayas = from a in conn.GetList<TbPpdbBiayaSiswaDetil>()
                             join b in conn.GetList<TbJenisBiayaPpdb>() on a.IdJenisBiayaPpdb equals b.IdJenisBiayaPpdb
                             where a.IdPpdbDaftar == IdPpdbDaftar
                             select new { a, b };
                foreach (var biaya in biayas)
                {
                    ret.TagPpdb.Add(new PpdbBiayaSiswaDetilModel
                    {
                        IdJenisBiayaPpdb = biaya.a.IdJenisBiayaPpdb,
                        JenisBiayaPpdb = biaya.b.Nama,
                        Catatan = biaya.a.Catatan,
                        Rp = biaya.a.Rp
                    });
                    ret.RpBiayaTotal += biaya.a.Rp;
                }
                var biayaCicils = from a in conn.GetList<TbPpdbBiayaSiswaCicil>()
                                  where a.IdPpdbDaftar == IdPpdbDaftar
                                  select a;
                foreach (var biayaCicil in biayaCicils.OrderBy(x => x.CilKe))
                {
                    ret.TagCicil.Add(new PpdbBiayaSiswaCicilModel
                    {
                        CilKe = biayaCicil.CilKe,
                        RpCilKe = biayaCicil.RpCilKe,
                        Status = StatusLunasConstant.Dict[biayaCicil.Status],
                        TanggalCilKe = biayaCicil.TanggalCilKe.ToString("dd-MM-yyyy"),
                        TanggalLunas = biayaCicil.TanggalLunas == null ? "" : ((DateTime)biayaCicil.TanggalLunas).ToString("dd-MM-yyyy")
                    });
                }

                //ortu
                var dataOrtus = from a in conn.GetList<TbPpdbSiswaOrtu>()
                                where a.IdPpdbDaftar == IdPpdbDaftar
                                select a;
                foreach (var dataOrtu in dataOrtus)
                {
                    ret.DataOrtu.Add(new PpdbSiswaOrtuAddModel
                    {
                        IdTipe = dataOrtu.IdTipe,
                        Nama = dataOrtu.Nama,
                        Alamat = dataOrtu.Alamat,
                        Nik = dataOrtu.Nik,
                        IdJenisPekerjaan = dataOrtu.IdJenisPekerjaan,
                        NamaInstansi = dataOrtu.NamaInstansi,
                        Jabatan = dataOrtu.Jabatan,
                        Email = dataOrtu.Email,
                        NoHandphone = dataOrtu.NoHandphone,
                        NoTelpRumah = dataOrtu.NoTelpRumah,
                        IdAgama = dataOrtu.IdAgama,
                        IdKewarganegaraan = dataOrtu.IdKewarganegaraan,
                        IdJenjangPendidikan = dataOrtu.IdJenisJenjangPendidikan,
                        IdStatusPenikahan = dataOrtu.IdStatusPenikahan,
                        TempatLahir = dataOrtu.TempatLahir,
                        TanggalLahir = dataOrtu.TanggalLahir.ToString("dd-MM-yyyy"),
                        IdStatusHidup = dataOrtu.IdStatusHidup,
                        TanggalMeninggal = dataOrtu.TanggalMeninggal.ToString("dd-MM-yyyy"),
                    });
                }

                //file
                var Foto = IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.PasFoto];
                string[] getsFoto = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", Foto));
                Foto = getsFoto.Count() != 0 ? new DirectoryInfo(getsFoto[0]).Name : null;
                ret.Foto = Foto;

                var FileKtpIbu = IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KtpIbu];
                string[] getsFileKtpIbu = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileKtpIbu));
                FileKtpIbu = getsFileKtpIbu.Count() != 0 ? new DirectoryInfo(getsFileKtpIbu[0]).Name : null;
                ret.FileKtpIbu = FileKtpIbu;

                var FileKtpAyah = IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KtpAyah];
                string[] getsFileKtpAyah = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileKtpAyah));
                FileKtpAyah = getsFileKtpAyah.Count() != 0 ? new DirectoryInfo(getsFileKtpAyah[0]).Name : null;
                ret.FileKtpAyah = FileKtpAyah;

                var FileKartuKeluarga = IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuKeluarga];
                string[] getsFileKartuKeluarga = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileKartuKeluarga));
                FileKartuKeluarga = getsFileKartuKeluarga.Count() != 0 ? new DirectoryInfo(getsFileKartuKeluarga[0]).Name : null;
                ret.FileKartuKeluarga = FileKartuKeluarga;

                var FileAkte = IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.Akte];
                string[] getsFileAkte = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileAkte));
                FileAkte = getsFileAkte.Count() != 0 ? new DirectoryInfo(getsFileAkte[0]).Name : null;
                ret.FileAkte = FileAkte;

                var FileSuratKesehatan = IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.SuratKesehatan];
                string[] getsFileSuratKesehatan = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileSuratKesehatan));
                FileSuratKesehatan = getsFileSuratKesehatan.Count() != 0 ? new DirectoryInfo(getsFileSuratKesehatan[0]).Name : null;
                ret.FileSuratKesehatan = FileSuratKesehatan;

                var FileBebasNarkoba = IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KetBebasNarkoba];
                string[] getsFileBebasNarkoba = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileBebasNarkoba));
                FileBebasNarkoba = getsFileBebasNarkoba.Count() != 0 ? new DirectoryInfo(getsFileBebasNarkoba[0]).Name : null;
                ret.FileBebasNarkoba = FileBebasNarkoba;

                var FileKartuPeserta = IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuPeserta];
                string[] getsFileKartuPeserta = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileKartuPeserta));
                FileKartuPeserta = getsFileKartuPeserta.Count() != 0 ? new DirectoryInfo(getsFileKartuPeserta[0]).Name : null;
                ret.FileKartuPeserta = FileKartuPeserta;

                var SuratPerjanjianCicilan = IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.SuratPerjanjianCicilan];
                string[] getsSuratPerjanjianCicilan = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", SuratPerjanjianCicilan));
                SuratPerjanjianCicilan = getsSuratPerjanjianCicilan.Count() != 0 ? new DirectoryInfo(getsSuratPerjanjianCicilan[0]).Name : null;
                ret.FilePerjanjianCicil = SuratPerjanjianCicilan;

                var KartuPeserta = IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuPeserta];
                string[] getsKartuPeserta = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", KartuPeserta));
                KartuPeserta = getsKartuPeserta.Count() != 0 ? new DirectoryInfo(getsKartuPeserta[0]).Name : null;
                ret.FileKartuPeserta = KartuPeserta;

                ret.FileLembarKomitmen = item.a.FileLembarKomitmen;

                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbSiswa", ex);
                return null;
            }
        }
        public string SetLunasBiayaSiswa(IDbConnection conn, int IdUser, string RefNo, int IdPpdbDaftar, DateTime TanggalLunas, double Rp, string Catatan)
        {
            try
            {
                this.SetCurrentPPdb();
                var item = (from a in conn.GetList<TbPpdbBiayaSiswa>()
                            where a.IdPpdbDaftar == IdPpdbDaftar
                            select a).FirstOrDefault();
                if (item == null)
                    return "data tidak ada";
                item.TanggalLunas = TanggalLunas;
                item.Status = StatusLunasConstant.Lunas;
                item.UpdatedBy = IdUser;
                item.UpdatedDate = dateTimeService.GetCurrdate();
                item.RefNo = RefNo;
                item.Catatan = Catatan;
                conn.Update(item);
                var itemCicils = from a in conn.GetList<TbPpdbBiayaSiswaCicil>()
                                 where a.IdPpdbDaftar == IdPpdbDaftar
                                 & a.Status == StatusLunasConstant.BelumLunas
                                 select a;
                foreach (var itemCicil in itemCicils.OrderBy(x => x.CilKe))
                {
                    if (itemCicil.RpCilKe == Rp)
                    {
                        item.Status = StatusLunasConstant.Lunas;
                        item.TanggalLunas = TanggalLunas;
                        item.UpdatedBy = IdUser;
                        item.UpdatedDate = dateTimeService.GetCurrdate();
                        item.RefNo = RefNo;
                        item.Catatan = Catatan;
                        conn.Update(itemCicil);
                        break;
                    }
                }
                TbPpdbSiswaStatus tbPpdbSiswaStatus = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                                       where a.IdPpdbDaftar == IdPpdbDaftar
                                                       & a.Status == StatusPpdbConstant.PelunasanBiayaPendidikan
                                                       select a).FirstOrDefault();
                bool isInsert = false;
                if (tbPpdbSiswaStatus == null)
                {
                    tbPpdbSiswaStatus = new TbPpdbSiswaStatus();
                    isInsert = true;
                }

                tbPpdbSiswaStatus.Status = StatusPpdbConstant.PelunasanBiayaPendidikan;
                tbPpdbSiswaStatus.StatusProses = 0;
                tbPpdbSiswaStatus.CreatedBy = IdUser;
                tbPpdbSiswaStatus.CreatedDate = dateTimeService.GetCurrdate();
                tbPpdbSiswaStatus.IdPpdbDaftar = IdPpdbDaftar;
                tbPpdbSiswaStatus.CatatanProses = Catatan;
                if (isInsert)
                {
                    conn.Insert(tbPpdbSiswaStatus);
                }
                else
                {
                    conn.Update(tbPpdbSiswaStatus);
                }

                //send mail
                var itemDaftar = (from a in conn.GetList<TbPpdbSiswa>()
                                  join a1 in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals a1.IdPpdbKelas
                                  join b in conn.GetList<TbKelas>() on a1.IdKelas equals b.IdKelas
                                  join c in conn.GetList<TbUnit>() on b.IdUnit equals c.IdUnit
                                  join d in conn.GetList<TbSekolah>() on c.IdSekolah equals d.IdSekolah
                                  where a.IdPpdbDaftar == IdPpdbDaftar
                                  select new { a, a1, b, c, d }).FirstOrDefault();


                string _messageBody1 = string.Empty;
                string _messageBody2 = string.Empty;
                string _subjects = "PPDB";

                List<string> emailTos = new List<string>();
                List<string> emailCcs = new List<string>();
                List<string> _attachments = new List<string>();
                var dataOrtus = from a in conn.GetList<TbPpdbSiswaOrtu>()
                                where a.IdPpdbDaftar == IdPpdbDaftar
                                select a;
                foreach (var ortu in dataOrtus)
                {
                    if (!string.IsNullOrEmpty(ortu.Email))
                    {
                        if (!emailTos.Contains(ortu.Email.ToLower()))
                            emailTos.Add(ortu.Email.ToLower());
                    }
                }
                int jenisRedaksiEmail = JenisRedaksiEmailPpdbConstant.LunasPendidikan;
                _subjects = JenisRedaksiEmailPpdbConstant.Dict[jenisRedaksiEmail];

                string oMessage = GetRedaksiEmailPpdbBody(jenisRedaksiEmail, out _messageBody1, out _messageBody2);
                if (!string.IsNullOrEmpty(oMessage))
                    return string.Empty;
                _messageBody1 = _messageBody1.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                _messageBody2 = _messageBody2.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);

                if (!emailTos.Contains(itemDaftar.a.Email))
                    emailTos.Add(itemDaftar.a.Email);
                emailCcs.Add("ppdb.sekolahattaufiqbogor@gmail.com"); //cc ke bagian keuangan
                string _emailMessage = SetHtmlEmailPpdbMessage(
                    _messageBody1,
                    _messageBody2,
                    itemDaftar.a.IdPpdbDaftar,
                    itemDaftar.c.Nama,
                    JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[itemDaftar.a.IdJalurPendaftaran],
                    JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[itemDaftar.a.IdJenisPendaftaran],
                    itemDaftar.a.Nama,
                    itemDaftar.a.TanggalLahir.ToString("dd-MM-yyyy"),
                    currPpdb.TahunAjaran,
                    itemDaftar.d.Nama,
                    currPpdb.NamaKetua);

                SendEmailModel email = new SendEmailModel
                {
                    To = emailTos,
                    Cc = emailCcs,
                    Title = "Bapak/Ibu ",
                    Subject = _subjects,
                    Message = _emailMessage,
                    Attachments = _attachments
                };
                _ = emailService.SendHtmlEmailAsync(email);
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetLunasBiayaSiswa", ex);
            }
        }

        public string SetLunasBiayaSiswaPrt(IDbConnection conn, int IdUser, string RefNo, int IdPpdbDaftar, DateTime TanggalLunas, double Rp, string Catatan, int? CilKe)
        {
            try
            {
                this.SetCurrentPPdb();

                bool isLunas = false;
                if (CilKe != null)
                {
                    var itemCicil = (from a in conn.GetList<TbPpdbBiayaSiswaCicil>()
                                     where a.IdPpdbDaftar == IdPpdbDaftar
                                     & a.Status == StatusLunasConstant.BelumLunas
                                     & a.CilKe == CilKe
                                     select a).FirstOrDefault();
                    if (itemCicil != null)
                    {
                        itemCicil.Status = StatusLunasConstant.Lunas;
                        itemCicil.TanggalLunas = TanggalLunas;
                        conn.Update(itemCicil);
                    }


                    var itemCicils = (from a in conn.GetList<TbPpdbBiayaSiswaCicil>()
                                      where a.IdPpdbDaftar == IdPpdbDaftar
                                      select a).ToList();
                    foreach (var icil in itemCicils)
                    {
                        isLunas = true;
                        if (icil.Status == StatusLunasConstant.BelumLunas)
                        {
                            isLunas = false;
                            break;
                        }
                    }

                }
                else
                {
                    isLunas = true;
                }

                if (isLunas)
                {
                    var item = (from a in conn.GetList<TbPpdbBiayaSiswa>()
                                where a.IdPpdbDaftar == IdPpdbDaftar
                                select a).FirstOrDefault();
                    if (item == null)
                        return "data tidak ada";

                    item.TanggalLunas = TanggalLunas;
                    item.Status = StatusLunasConstant.Lunas;
                    item.UpdatedBy = IdUser;
                    item.UpdatedDate = dateTimeService.GetCurrdate();
                    item.RefNo = RefNo;
                    item.Catatan = Catatan;
                    conn.Update(item);

                    TbPpdbSiswaStatus tbPpdbSiswaStatus = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                                           where a.IdPpdbDaftar == IdPpdbDaftar
                                                           & a.Status == StatusPpdbConstant.PelunasanBiayaPendidikan
                                                           select a).FirstOrDefault();
                    bool isInsert = false;
                    if (tbPpdbSiswaStatus == null)
                    {
                        tbPpdbSiswaStatus = new TbPpdbSiswaStatus();
                        isInsert = true;
                    }

                    tbPpdbSiswaStatus.Status = StatusPpdbConstant.PelunasanBiayaPendidikan;
                    tbPpdbSiswaStatus.StatusProses = 0;
                    tbPpdbSiswaStatus.CreatedBy = IdUser;
                    tbPpdbSiswaStatus.CreatedDate = dateTimeService.GetCurrdate();
                    tbPpdbSiswaStatus.IdPpdbDaftar = IdPpdbDaftar;
                    tbPpdbSiswaStatus.CatatanProses = Catatan;
                    if (isInsert)
                    {
                        conn.Insert(tbPpdbSiswaStatus);
                    }
                    else
                    {
                        conn.Update(tbPpdbSiswaStatus);
                    }

                    //send mail
                    var itemDaftar = (from a in conn.GetList<TbPpdbSiswa>()
                                      join a1 in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals a1.IdPpdbKelas
                                      join b in conn.GetList<TbKelas>() on a1.IdKelas equals b.IdKelas
                                      join c in conn.GetList<TbUnit>() on b.IdUnit equals c.IdUnit
                                      join d in conn.GetList<TbSekolah>() on c.IdSekolah equals d.IdSekolah
                                      where a.IdPpdbDaftar == IdPpdbDaftar
                                      select new { a, a1, b, c, d }).FirstOrDefault();


                    string _messageBody1 = string.Empty;
                    string _messageBody2 = string.Empty;
                    string _subjects = "PPDB";

                    List<string> emailTos = new List<string>();
                    List<string> emailCcs = new List<string>();
                    List<string> _attachments = new List<string>();
                    var dataOrtus = from a in conn.GetList<TbPpdbSiswaOrtu>()
                                    where a.IdPpdbDaftar == IdPpdbDaftar
                                    select a;
                    foreach (var ortu in dataOrtus)
                    {
                        if (!string.IsNullOrEmpty(ortu.Email))
                        {
                            if (!emailTos.Contains(ortu.Email.ToLower()))
                                emailTos.Add(ortu.Email.ToLower());
                        }
                    }
                    int jenisRedaksiEmail = JenisRedaksiEmailPpdbConstant.LunasPendidikan;
                    _subjects = JenisRedaksiEmailPpdbConstant.Dict[jenisRedaksiEmail];

                    string oMessage = GetRedaksiEmailPpdbBody(jenisRedaksiEmail, out _messageBody1, out _messageBody2);
                    if (!string.IsNullOrEmpty(oMessage))
                        return string.Empty;
                    _messageBody1 = _messageBody1.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                    _messageBody2 = _messageBody2.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);

                    if (!emailTos.Contains(itemDaftar.a.Email))
                        emailTos.Add(itemDaftar.a.Email);
                    emailCcs.Add("ppdb.sekolahattaufiqbogor@gmail.com"); //cc ke bagian keuangan
                    string _emailMessage = SetHtmlEmailPpdbMessage(
                        _messageBody1,
                        _messageBody2,
                        itemDaftar.a.IdPpdbDaftar,
                        itemDaftar.c.Nama,
                        JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[itemDaftar.a.IdJalurPendaftaran],
                        JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[itemDaftar.a.IdJenisPendaftaran],
                        itemDaftar.a.Nama,
                        itemDaftar.a.TanggalLahir.ToString("dd-MM-yyyy"),
                        currPpdb.TahunAjaran,
                        itemDaftar.d.Nama,
                        currPpdb.NamaKetua);

                    SendEmailModel email = new SendEmailModel
                    {
                        To = emailTos,
                        Cc = emailCcs,
                        Title = "Bapak/Ibu ",
                        Subject = _subjects,
                        Message = _emailMessage,
                        Attachments = _attachments
                    };
                    _ = emailService.SendHtmlEmailAsync(email);
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetLunasBiayaSiswaPrt", ex);
            }
        }
        #endregion

        public string DeleteTagihanSiswa(int IdSiswa, int IdJenisTagihanSiswa, string Periode)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    TbSiswaTagihanDetil siswaTagihan = new TbSiswaTagihanDetil();
                    siswaTagihan = (from a in conn.GetList<TbSiswaTagihanDetil>()
                                    where a.IdSiswa == IdSiswa && a.IdJenisTagihanSiswa == IdJenisTagihanSiswa && a.Periode == dateTimeService.StrToDateTime(Periode)
                                    select a).FirstOrDefault();
                    if (siswaTagihan == null) return "data tidak ada";
                    siswaTagihan.IdJenisTagihanSiswa = IdJenisTagihanSiswa;
                    siswaTagihan.Rp = 0;
                    conn.Update(siswaTagihan);
                    return null;
                }
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteTagihanSiswa", ex);
            }
        }

        public string UpdateTagihanSiswa(int IdSiswa, int IdJenisTagihanSiswa, string Periode, double Rp)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    TbSiswaTagihanDetil siswaTagihan = new TbSiswaTagihanDetil();
                    siswaTagihan = (from a in conn.GetList<TbSiswaTagihanDetil>()
                                    where a.IdSiswa == IdSiswa && a.IdJenisTagihanSiswa == IdJenisTagihanSiswa
                                    && a.Periode == dateTimeService.StrToDateTime(Periode)
                                    select a).FirstOrDefault();
                    if (siswaTagihan == null) return "data tidak ada";
                    siswaTagihan.IdJenisTagihanSiswa = IdJenisTagihanSiswa;
                    siswaTagihan.Rp = Rp;
                    conn.Update(siswaTagihan);

                    if (Rp != 0)
                    {
                        var tbSiswaTagihan = (from a in conn.GetList<TbSiswaTagihan>()
                                              where a.IdSiswa == IdSiswa
                                              select a).FirstOrDefault();
                        if (tbSiswaTagihan == null) return "data tidak ada";
                        tbSiswaTagihan.RefNo = string.Empty;
                        tbSiswaTagihan.UpdatedDate = null;
                        tbSiswaTagihan.UpdatedBy = null;
                        tbSiswaTagihan.Status = StatusLunasConstant.BelumLunas;
                        conn.Update(tbSiswaTagihan);
                    }
                    return null;
                }
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UpdateTagihanSiswa", ex);
            }
        }

        public List<BannerModel> GetBanners(string Tipe, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbBanner>().Where(x => x.Tipe == (Tipe == null ? x.Tipe : Tipe)).ToList();
                    if (data.Count == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    List<BannerModel> ret = new List<BannerModel>();
                    foreach (var item in data.OrderByDescending(x => x.IdBanner))
                    {
                        ret.Add(new BannerModel
                        {
                            IdBanner = item.IdBanner,
                            Nama = item.Nama,
                            FileImage = item.FileImage,
                            Tipe = item.Tipe,
                            CreatedBy = item.CreatedBy,
                            CreatedDate = item.CreatedDate.ToString("dd-MM-yyyy")
                        });
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetBanners", ex);
                return null;
            }
        }
        public string AddBanner(int IdUser, BannerAddEditModel Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    TbBanner tbBanner = new TbBanner
                    {
                        Nama = Data.Nama,
                        Tipe = Data.Tipe,
                        CreatedBy = IdUser,
                        CreatedDate = dateTimeService.GetCurrdate()
                    };

                    if (string.IsNullOrEmpty(Data.Tipe))
                    {
                        oMessage = "tipe wajib dipilih!";
                        return null;
                    }
                    if (Data.FileImage == null)
                    {
                        oMessage = "gambar wajib di upload!";
                        return null;
                    }
                    var _fileName = string.Empty;
                    _fileName = string.Format("{0}", Guid.NewGuid());
                    string _pathFileUpload = AppSetting.PathApplFile;
                    _fileName += Path.GetExtension(Data.FileImage.FileName);
                    tbBanner.FileImage = _fileName;

                    _fileName = Path.Combine(_pathFileUpload, _fileName);
                    using (var fileStream = new FileStream(_fileName, FileMode.Create))
                    {
                        Data.FileImage.CopyTo(fileStream);
                    }

                    conn.Insert(tbBanner);
                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "AddBanner", ex);
                return null;
            }
        }

        public string UpdateBanner(int IdUser, BannerAddEditModel Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbBanner = conn.GetList<TbBanner>().Where(x => x.IdBanner == Data.IdBanner).FirstOrDefault();
                    if (tbBanner == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    tbBanner.Nama = Data.Nama;
                    tbBanner.Tipe = Data.Tipe;

                    if (Data.FileImage != null)
                    {
                        var _fileName = string.Empty;
                        _fileName = string.Format("{0}", Guid.NewGuid());
                        string _pathFileUpload = AppSetting.PathApplFile;
                        _fileName += Path.GetExtension(Data.FileImage.FileName);
                        tbBanner.FileImage = _fileName;

                        _fileName = Path.Combine(_pathFileUpload, _fileName);
                        using (var fileStream = new FileStream(_fileName, FileMode.Create))
                        {
                            Data.FileImage.CopyTo(fileStream);
                        }
                    }

                    conn.Update(tbBanner);
                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "UpdateBanner", ex);
                return null;
            }
        }

        public string DeleteBanner(int IdBanner, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbBanner>().Where(x => x.IdBanner == IdBanner).FirstOrDefault();
                    if (data == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    var pathUpload = AppSetting.PathApplImage;
                    if (File.Exists(Path.Combine(pathUpload, data.FileImage)))
                        File.Delete(Path.Combine(pathUpload, data.FileImage));
                    conn.Delete(data);

                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "DeleteBanner", ex);
                return null;
            }
        }

        #region TESTIMONI
        public List<TestimoniModel> GetTestimonis(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbTestimonis = conn.GetList<TbTestimoni>().ToList();
                    if (tbTestimonis.Count == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }

                    List<TestimoniModel> ret = new List<TestimoniModel>();
                    foreach (var item in tbTestimonis.OrderByDescending(x => x.IdTestimoni))
                    {
                        ret.Add(new TestimoniModel
                        {
                            IdTestimoni = item.IdTestimoni,
                            Nama = item.Nama,
                            Sebagai = item.Sebagai,
                            Pesan = item.Pesan,
                            FotoUser = item.FotoUser,
                            Status = item.Status,
                            StrStatus = StatusDataConstant.Dict[item.Status],
                            Url = item.Url,
                            CreatedDate = item.CreatedDate.ToString("dd-MM-yyyy")
                        });
                    }

                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetTestimonis", ex);
                return null;
            }
        }
        public TestimoniModel GetTestimoni(int IdTestimoni, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbTestimoni = conn.GetList<TbTestimoni>().Where(x => x.IdTestimoni == IdTestimoni).FirstOrDefault();
                    if (tbTestimoni == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }

                    TestimoniModel ret = new TestimoniModel
                    {
                        IdTestimoni = tbTestimoni.IdTestimoni,
                        Nama = tbTestimoni.Nama,
                        Sebagai = tbTestimoni.Sebagai,
                        Pesan = tbTestimoni.Pesan,
                        FotoUser = tbTestimoni.FotoUser,
                        Status = tbTestimoni.Status,
                        StrStatus = StatusDataConstant.Dict[tbTestimoni.Status],
                        Url = tbTestimoni.Url,
                        CreatedDate = tbTestimoni.CreatedDate.ToString("dd-MM-yyyy")
                    };

                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetTestimoni", ex);
                return null;
            }
        }
        public string UpdateTestimoni(int IdUser, int IdTestimoni, string Nama, string Sebagai, string Pesan, string Url, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbTestimoni = conn.GetList<TbTestimoni>().Where(x => x.IdTestimoni == IdTestimoni).FirstOrDefault();
                    if (tbTestimoni == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }

                    tbTestimoni.Status = StatusDataConstant.Aktif;
                    tbTestimoni.Nama = Nama;
                    tbTestimoni.Sebagai = Sebagai;
                    tbTestimoni.Pesan = Pesan;
                    tbTestimoni.Url = Url;
                    tbTestimoni.UpdatedBy = IdUser;
                    tbTestimoni.UpdatedDate = dateTimeService.GetCurrdate();
                    conn.Update(tbTestimoni);

                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "UpdateTestimoni", ex);
                return null;
            }
        }
        public string DeleteTestimoni(int IdTestimoni, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbTestimoni = conn.GetList<TbTestimoni>().Where(x => x.IdTestimoni == IdTestimoni).FirstOrDefault();
                    if (tbTestimoni == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    if (tbTestimoni.FotoUser != null)
                    {
                        if (File.Exists(Path.Combine(AppSetting.PathTestimoni, tbTestimoni.FotoUser)))
                            File.Delete(Path.Combine(AppSetting.PathTestimoni, tbTestimoni.FotoUser));
                    }

                    conn.Delete(tbTestimoni);

                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "DeleteTestimoni", ex);
                return null;
            }
        }
        public string ToSlug(string phrase)
        {
            return commonService.ToSlug(phrase);
        }
        #endregion



    }
}
