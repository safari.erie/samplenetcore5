﻿using System;
using System.IO;
using PdfSharpCore.Fonts;

namespace Swg.Sch.Services
{
    public class FontResolver : IFontResolver
    {
        private string _resourcesPath = string.Empty;
        public FontResolver(string resourcesPath)
        {
            _resourcesPath = resourcesPath;
        }


        public string DefaultFontName => throw new NotImplementedException();

        public byte[] GetFont(string faceName)
        {
            using (var ms = new MemoryStream())
            {
                using (var fs = File.Open(faceName, FileMode.Open))
                {
                    fs.CopyTo(ms);
                    ms.Position = 0;
                    return ms.ToArray();
                }
            }
        }
        public FontResolverInfo ResolveTypeface(string familyName, bool isBold, bool isItalic)
        {
            if (familyName.Equals("OpenSans", StringComparison.CurrentCultureIgnoreCase))
            {
                if (isBold && isItalic)
                {
                    return new FontResolverInfo($"{_resourcesPath}Tinos-BoldItalic.ttf");
                }
                else if (isBold)
                {
                    return new FontResolverInfo($"{_resourcesPath}Tinos-Bold.ttf");
                }
                else if (isItalic)
                {
                    return new FontResolverInfo($"{_resourcesPath}Tinos-Italic.ttf");
                }
                else
                {
                    return new FontResolverInfo($"{_resourcesPath}Tinos-Regular.ttf");
                }
            }
            else if (familyName.Equals("Courier", StringComparison.CurrentCultureIgnoreCase))
            {
                return new FontResolverInfo($"{_resourcesPath}cour.ttf");
            }
            else if (familyName.Equals("Calibri", StringComparison.CurrentCultureIgnoreCase))
            {
                if (isBold && isItalic)
                {
                    return new FontResolverInfo($"{_resourcesPath}CalibriBoldItalic.ttf");
                }
                else if (isBold)
                {
                    return new FontResolverInfo($"{_resourcesPath}CalibriBold.ttf");
                }
                else if (isItalic)
                {
                    return new FontResolverInfo($"{_resourcesPath}CalibriItalic.ttf");
                }
                else
                {
                    return new FontResolverInfo($"{_resourcesPath}CalibriRegular.ttf");
                }
            }

            return null;
        }
    }
}