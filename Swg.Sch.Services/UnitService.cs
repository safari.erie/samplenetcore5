﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Dapper;
using Swg.Entities.Lmg;
using Swg.Models;
using Swg.Models.Constants;
using Swg.Sch.Shared.Constants;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;

namespace Swg.Sch.Services
{
    public class UnitService : IUnitService
    {
        private string _languageCode = "id";
        private readonly string ServiceName = "Swg.Sch.Services.UnitService.";
        private readonly ICommonService commonService;
        private readonly IUserAppService userAppService;
        private readonly IDateTimeService dateTimeService;
        private readonly ISchService schService;

        public UnitService(
            ICommonService CommonService,
            IDateTimeService DateTimeService,
            IUserAppService UserAppService,
            ISchService SchService)
        {
            commonService = CommonService;
            userAppService = UserAppService;
            dateTimeService = DateTimeService;
            schService = SchService;
        }
        public List<ComboModel> GetStatusData()
        {
            return (from a in StatusDataConstant.Dict
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        private string SetUrlKonten(string Judul)
        {
            return Guid.NewGuid().ToString();
        }

        private UnitReadModel SetUnitModel(TbUnit tbUnit, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                UnitReadModel ret = new UnitReadModel();
                ret.IdUnit = tbUnit.IdUnit;
                ret.IdSekolah = tbUnit.IdSekolah;
                ret.Nspn = tbUnit.Nspn;
                ret.Nama = tbUnit.Nama;
                ret.NamaSingkat = tbUnit.NamaSingkat;
                ret.Alamat = tbUnit.Alamat;
                ret.KodePos = tbUnit.KodePos;
                ret.Desa = tbUnit.Desa;
                ret.Kecamatan = tbUnit.Kecamatan;
                ret.Kabupaten = tbUnit.Kabupaten;
                ret.Provinsi = tbUnit.Provinsi;
                ret.NoTelepon = tbUnit.NoTelepon;
                ret.NoFaximili = tbUnit.NoFaximili;
                ret.Email = tbUnit.Email;
                ret.Web = tbUnit.Web;
                ret.StatusUnit = tbUnit.StatusUnit;
                ret.WaktuPenyelenggaraan = tbUnit.WaktuPenyelenggaraan;
                ret.Kurikulum = tbUnit.Kurikulum;
                ret.Naungan = tbUnit.Naungan;
                ret.NoSkPendirian = tbUnit.NoSkPendirian;
                ret.TanggalSkPendirian = tbUnit.TanggalSkAkreditasi.ToString("dd-MM-yyyy");
                ret.NoSkOperasional = tbUnit.NoSkOperasional;
                ret.TanggalSkOperasional = tbUnit.TanggalSkOperasional.ToString("dd-MM-yyyy");
                ret.JenisAkreditasi = tbUnit.JenisAkreditasi;
                ret.NoSkAkreditasi = tbUnit.NoSkAkreditasi;
                ret.TanggalSkAkreditasi = tbUnit.TanggalSkPendirian.ToString("dd-MM-yyyy");
                ret.NoSertifikatIso = tbUnit.NoSertifikatIso;
                ret.NamaYayasan = tbUnit.NamaYayasan;
                ret.LuasTanah = tbUnit.LuasTanah;
                ret.LuasBangunan = tbUnit.LuasBangunan;
                ret.ProviderInternet = tbUnit.ProviderInternet;
                ret.BandwidthInternet = tbUnit.BandwidthInternet;
                ret.SumberListrik = tbUnit.SumberListrik;
                ret.DayaListrik = tbUnit.DayaListrik;
                ret.Moto = tbUnit.Moto;
                ret.Logo = tbUnit.Logo;
                ret.SliderBanner = tbUnit.SliderBanner;
                ret.SliderImage = tbUnit.SliderImage;
                ret.SliderJudul = tbUnit.SliderJudul;
                ret.SliderKeterangan = tbUnit.SliderKeterangan;
                ret.Thumbnail = tbUnit.Thumbnail;
                ret.ThumbnailBesar = tbUnit.ThumbnailBesar;
                ret.Profile = tbUnit.Profile;
                ret.VisiMisi = tbUnit.VisiMisi;
                ret.StrukturOrganisasi = tbUnit.StrukturOrganisasi;

                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "SetUnitModel", ex);
                return null;
            }
        }
        public List<UnitReadModel> GetUnits(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbUnit>();
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    List<UnitReadModel> ret = new List<UnitReadModel>();
                    foreach (var item in data.OrderBy(x => x.IdUnit))
                    {
                        UnitReadModel m = SetUnitModel(item, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetUnits", ex);
                return null;
            }
        }
        public UnitReadModel GetUnit(int IdUnit, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.Get<TbUnit>(IdUnit);
                    if (data == null) { oMessage = "data tidak ada"; return null; }

                    UnitReadModel ret = SetUnitModel(data, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetUnit", ex);
                return null;
            }
        }
        public string UnitEdit(int IdUser, UnitAddEditModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbUnit = conn.Get<TbUnit>(Data.IdUnit);
                        if (tbUnit == null) { return "data tidak ada"; }

                        tbUnit.IdUnit = Data.IdUnit;
                        tbUnit.IdSekolah = Data.IdSekolah;
                        tbUnit.Nspn = Data.Nspn;
                        tbUnit.Nama = Data.Nama;
                        tbUnit.NamaSingkat = Data.NamaSingkat;
                        tbUnit.Alamat = Data.Alamat;
                        tbUnit.KodePos = Data.KodePos;
                        tbUnit.Desa = Data.Desa;
                        tbUnit.Kecamatan = Data.Kecamatan;
                        tbUnit.Kabupaten = Data.Kabupaten;
                        tbUnit.Provinsi = Data.Provinsi;
                        tbUnit.NoTelepon = Data.NoTelepon;
                        tbUnit.NoFaximili = Data.NoFaximili;
                        tbUnit.Email = Data.Email;
                        tbUnit.Web = Data.Web;
                        tbUnit.StatusUnit = Data.StatusUnit;
                        tbUnit.WaktuPenyelenggaraan = Data.WaktuPenyelenggaraan;
                        tbUnit.Kurikulum = Data.Kurikulum;
                        tbUnit.Naungan = Data.Naungan;
                        tbUnit.NoSkPendirian = Data.NoSkPendirian;
                        tbUnit.TanggalSkPendirian = dateTimeService.StrToDateTime(Data.TanggalSkAkreditasi);
                        tbUnit.NoSkOperasional = Data.NoSkOperasional;
                        tbUnit.TanggalSkOperasional = dateTimeService.StrToDateTime(Data.TanggalSkOperasional);
                        tbUnit.JenisAkreditasi = Data.JenisAkreditasi;
                        tbUnit.NoSkAkreditasi = Data.NoSkAkreditasi;
                        tbUnit.TanggalSkAkreditasi = dateTimeService.StrToDateTime(Data.TanggalSkPendirian);
                        tbUnit.NoSertifikatIso = Data.NoSertifikatIso;
                        tbUnit.NamaYayasan = Data.NamaYayasan;
                        tbUnit.LuasTanah = Data.LuasTanah;
                        tbUnit.LuasBangunan = Data.LuasBangunan;
                        tbUnit.ProviderInternet = Data.ProviderInternet;
                        tbUnit.BandwidthInternet = Data.BandwidthInternet;
                        tbUnit.SumberListrik = Data.SumberListrik;
                        tbUnit.DayaListrik = Data.DayaListrik;
                        tbUnit.Moto = Data.Moto;
                        tbUnit.SliderJudul = Data.SliderJudul;
                        tbUnit.SliderKeterangan = Data.SliderKeterangan;
                        tbUnit.Profile = Data.Profile;
                        tbUnit.VisiMisi = Data.VisiMisi;



                        /// upload logo if exists
                        if (Data.Logo != null)
                        {
                            var pathUpload = AppSetting.PathApplImage;
                            if (File.Exists(Path.Combine(pathUpload, tbUnit.Logo)))
                                File.Delete(Path.Combine(pathUpload, tbUnit.Logo));

                            tbUnit.Logo = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.LogoUnit] + Path.GetExtension(Data.Logo.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, tbUnit.Logo), FileMode.Create, FileAccess.Write))
                            {
                                Data.Logo.CopyTo(fileStream);
                            }
                        }

                        if (Data.SliderBanner != null)
                        {
                            var pathUpload = AppSetting.PathApplImage;
                            if (File.Exists(Path.Combine(pathUpload, tbUnit.SliderBanner)))
                                File.Delete(Path.Combine(pathUpload, tbUnit.SliderBanner));

                            tbUnit.SliderBanner = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.UnitSliderBanner] + Path.GetExtension(Data.SliderBanner.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, tbUnit.SliderBanner), FileMode.Create, FileAccess.Write))
                            {
                                Data.SliderBanner.CopyTo(fileStream);
                            }
                        }

                        if (Data.SliderImage != null)
                        {
                            var pathUpload = AppSetting.PathApplImage;
                            if (File.Exists(Path.Combine(pathUpload, tbUnit.SliderImage)))
                                File.Delete(Path.Combine(pathUpload, tbUnit.SliderImage));

                            tbUnit.SliderImage = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.UnitSliderImage] + Path.GetExtension(Data.SliderImage.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, tbUnit.SliderImage), FileMode.Create, FileAccess.Write))
                            {
                                Data.SliderImage.CopyTo(fileStream);
                            }
                        }

                        if (Data.Thumbnail != null)
                        {
                            var pathUpload = AppSetting.PathApplImage;
                            if (File.Exists(Path.Combine(pathUpload, tbUnit.Thumbnail)))
                                File.Delete(Path.Combine(pathUpload, tbUnit.Thumbnail));

                            tbUnit.Thumbnail = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.UnitThumbnail] + Path.GetExtension(Data.Thumbnail.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, tbUnit.Thumbnail), FileMode.Create, FileAccess.Write))
                            {
                                Data.Thumbnail.CopyTo(fileStream);
                            }
                        }

                        if (Data.ThumbnailBesar != null)
                        {
                            var pathUpload = AppSetting.PathApplImage;
                            if (File.Exists(Path.Combine(pathUpload, tbUnit.ThumbnailBesar)))
                                File.Delete(Path.Combine(pathUpload, tbUnit.ThumbnailBesar));

                            tbUnit.ThumbnailBesar = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.UnitThumbnailBesar] + Path.GetExtension(Data.ThumbnailBesar.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, tbUnit.ThumbnailBesar), FileMode.Create, FileAccess.Write))
                            {
                                Data.ThumbnailBesar.CopyTo(fileStream);
                            }
                        }

                        if (Data.StrukturOrganisasi != null)
                        {
                            var pathUpload = AppSetting.PathApplImage;
                            if (File.Exists(Path.Combine(pathUpload, tbUnit.StrukturOrganisasi)))
                                File.Delete(Path.Combine(pathUpload, tbUnit.StrukturOrganisasi));

                            tbUnit.StrukturOrganisasi = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.UnitStrukturOrganisasi] + Path.GetExtension(Data.StrukturOrganisasi.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, tbUnit.StrukturOrganisasi), FileMode.Create, FileAccess.Write))
                            {
                                Data.StrukturOrganisasi.CopyTo(fileStream);
                            }
                        }

                        conn.Update(tbUnit);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UnitEdit", ex);
            }
        }

        public List<UnitGaleriReadModel> GetUnitGaleris(int IdUser, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<UnitGaleriReadModel> ret = new List<UnitGaleriReadModel>();
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        bool canReadAll = false;
                        var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                             where a.IdPegawai == IdUser
                                             select a).FirstOrDefault();
                        if (tbUnitPegawai == null) { oMessage = string.Format("id_user {0} tidak terdaftar di unit", IdUser); return null; }

                        if (tbUnitPegawai.IdUnit == UnitConstant.DIR || tbUnitPegawai.IdUnit == UnitConstant.PER)
                        {
                            canReadAll = true;
                        }

                        var data = (from a in conn.GetList<TbUnitGaleri>()
                                    where a.IdUnit == (canReadAll == true ? a.IdUnit : tbUnitPegawai.IdUnit)
                                    select a).ToList();
                        if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                        foreach (var item in data)
                        {
                            ret.Add(new UnitGaleriReadModel
                            {
                                IdUnitGaleri = item.IdUnitGaleri,
                                IdUnit = item.IdUnit,
                                Unit = this.GetUnit(item.IdUnit, out string err).Nama,
                                IdJenisGaleri = item.IdJenisGaleri,
                                Judul = item.Judul,
                                Keterangan = item.Keterangan,
                                FileUrl = item.FileUrl,
                                JenisGaleri = JenisGaleriConstant.DictJenisGaleri[item.IdJenisGaleri],
                                UrlVideo = item.FileUrl,
                                Status = item.Status,
                                Jenis = item.Jenis,
                                NamaStatus = StatusProsesKontenConstant.Dict[item.Status],
                                NamaJenis = JenisDataKontenUnitConstant.DictJenisDataKontenUnit[item.Jenis]
                            });
                        }
                        return ret;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetUnitGaleris", ex);
                return null;
            }
        }
        public UnitGaleriReadModel GetUnitGaleri(int IdUnitGaleri, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                UnitGaleriReadModel ret = new UnitGaleriReadModel();
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var dt = (from a in conn.GetList<TbUnitGaleri>()
                                  where a.IdUnitGaleri == IdUnitGaleri
                                  select a).ToList();
                        if (dt == null || dt.Count() == 0)
                        {
                            oMessage = "data tidak ada";
                            return null;
                        }

                        var data = conn.Get<TbUnitGaleri>(IdUnitGaleri);
                        ret.IdUnitGaleri = data.IdUnitGaleri;
                        ret.IdUnit = data.IdUnit;
                        ret.IdJenisGaleri = data.IdJenisGaleri;
                        ret.Judul = data.Judul;
                        ret.Keterangan = data.Keterangan;
                        ret.FileUrl = data.FileUrl;
                        ret.UrlVideo = data.FileUrl;
                        ret.JenisGaleri = JenisGaleriConstant.DictJenisGaleri[data.IdJenisGaleri];
                        ret.Status = data.Status;
                        ret.Jenis = data.Jenis;
                        ret.NamaStatus = StatusProsesKontenConstant.Dict[data.Status];
                        ret.NamaJenis = JenisDataKontenUnitConstant.DictJenisDataKontenUnit[data.Jenis];
                        return ret;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetUnitGaleri", ex);
                return null;
            }
        }
        public string UnitGaleriAdd(int IdUser, UnitGaleriAddEditModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        bool canReadAll = false;
                        var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                             where a.IdPegawai == IdUser
                                             select a).FirstOrDefault();
                        if (tbUnitPegawai == null) { return string.Format("id_user {0} tidak terdaftar di unit", IdUser); }

                        if (tbUnitPegawai.IdUnit == UnitConstant.Tk || tbUnitPegawai.IdUnit == UnitConstant.Sd || tbUnitPegawai.IdUnit == UnitConstant.Smp || tbUnitPegawai.IdUnit == UnitConstant.Sma)
                        {
                            canReadAll = true;
                        }
                        if (!canReadAll)
                        {
                            if (Data.IdUnit != tbUnitPegawai.IdUnit)
                                return "anda tidak berhak input data galeri unit";
                        }

                        TbUnit tbUnit = conn.Get<TbUnit>(tbUnitPegawai.IdUnit);

                        TbUnitGaleri tbUnitGaleri = new TbUnitGaleri();
                        var tbUnitGaleris = conn.GetList<TbUnitGaleri>();
                        if (tbUnitGaleris == null || tbUnitGaleris.Count() == 0)
                        {
                            tbUnitGaleri.IdUnitGaleri = 1;
                        }
                        else
                        {
                            var maxUnitGaleri = (from a in conn.GetList<TbUnitGaleri>()
                                                 select new { a.IdUnitGaleri }).OrderByDescending(g => g.IdUnitGaleri).FirstOrDefault();
                            tbUnitGaleri.IdUnitGaleri = maxUnitGaleri.IdUnitGaleri + 1;
                        }

                        tbUnitGaleri.IdUnit = tbUnit.IdUnit;
                        tbUnitGaleri.IdJenisGaleri = Data.IdJenisGaleri;
                        tbUnitGaleri.Judul = Data.Judul;
                        tbUnitGaleri.Keterangan = Data.Keterangan;
                        tbUnitGaleri.CreatedBy = IdUser;
                        tbUnitGaleri.CreatedDate = dateTimeService.GetCurrdate();
                        if (Data.IdJenisGaleri == JenisGaleriConstant.Image)
                        {
                            string fileName = string.Empty;
                            if (Data.FileUrl != null && Data.FileUrl.Length > 0)
                            {
                                var pathUpload = AppSetting.PathGaleriUnit;

                                fileName = Path.GetRandomFileName() + Path.GetExtension(Data.FileUrl.FileName);
                                using (var fileStream = new FileStream(Path.Combine(pathUpload, fileName), FileMode.Create, FileAccess.Write))
                                {
                                    Data.FileUrl.CopyTo(fileStream);
                                }
                            }
                            tbUnitGaleri.FileUrl = fileName;
                        }
                        else if (Data.IdJenisGaleri == JenisGaleriConstant.Video)
                        {
                            tbUnitGaleri.FileUrl = Data.UrlVideo;
                        }
                        conn.Insert(tbUnitGaleri);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UnitGaleriAdd", ex);
            }
        }
        public string UnitGaleriEdit(int IdUser, UnitGaleriAddEditModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbUnitGaleri tbUnitGaleri = conn.Get<TbUnitGaleri>(Data.IdUnitGaleri);
                        if (tbUnitGaleri == null) return "data tidak ada";

                        var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                             join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                             where a.IdPegawai == IdUser
                                             select new { a, b }).FirstOrDefault();

                        SetAccessKontenUnit(conn, tbUnitPegawai.a.IdUnit, tbUnitPegawai.b.IdJenisJabatan, out bool CanReadKonten, out bool CanCreateKonten, out bool CanApproveKonten, out bool CanPublishKonten, out bool CanRejectKonten, out bool CanDeleteKonten, out bool CanEditKonten);

                        int statusData = 0;
                        if (tbUnitGaleri.Status == StatusProsesKontenConstant.New)
                        {
                            if (CanCreateKonten)
                            {
                                statusData = StatusProsesKontenConstant.New;
                            }
                            else if (CanApproveKonten)
                            {
                                statusData = StatusProsesKontenConstant.Approved;
                            }
                            else
                            {
                                return "Data " + tbUnitGaleri.Judul + " harus " + StatusProsesKontenConstant.Dict[tbUnitGaleri.Status + 1] + " terlebih dahulu";
                            }
                        }
                        else if (tbUnitGaleri.Status == StatusProsesKontenConstant.Approved)
                        {
                            if (CanPublishKonten)
                            {
                                if (tbUnitGaleri.Jenis == JenisDataKontenUnitConstant.Profil)
                                {
                                    // update STATUS semua data yang JENIS nya profil menjadi non-aktif
                                    var tbUnitGaleris = conn.GetList<TbUnitGaleri>().ToList();
                                    foreach (var item in tbUnitGaleris)
                                    {
                                        if (item.Jenis == JenisDataKontenUnitConstant.Profil)
                                        {
                                            item.Status = StatusProsesKontenConstant.InActive;
                                            conn.Update(item);
                                        }
                                    }
                                }
                                statusData = StatusProsesKontenConstant.Published;
                            }
                            else
                            {
                                return "Data " + tbUnitGaleri.Judul + " harus " + StatusProsesKontenConstant.Dict[tbUnitGaleri.Status + 1] + " terlebih dahulu";
                            }
                        }
                        else
                        {
                            return "Anda tidak berhak merubah data";
                        }
                        tbUnitGaleri.Status = statusData;
                        tbUnitGaleri.Jenis = Data.Jenis;

                        if (Data.IdJenisGaleri == JenisGaleriConstant.Image)
                        {
                            string fileName = string.Empty;
                            if (Data.FileUrl != null && Data.FileUrl.Length > 0)
                            {
                                var pathUpload = AppSetting.PathGaleriUnit;
                                //if (!string.IsNullOrEmpty(err)) return err;
                                if (File.Exists(Path.Combine(pathUpload, tbUnitGaleri.FileUrl)))
                                    File.Delete(Path.Combine(pathUpload, tbUnitGaleri.FileUrl));

                                fileName = Path.GetRandomFileName() + Path.GetExtension(Data.FileUrl.FileName);
                                using (var fileStream = new FileStream(Path.Combine(pathUpload, fileName), FileMode.Create, FileAccess.Write))
                                {
                                    Data.FileUrl.CopyTo(fileStream);
                                }
                                tbUnitGaleri.FileUrl = fileName;
                            }

                        }
                        else if (Data.IdJenisGaleri == JenisGaleriConstant.Video)
                        {
                            tbUnitGaleri.FileUrl = Data.UrlVideo;
                        }

                        tbUnitGaleri.IdJenisGaleri = Data.IdJenisGaleri;
                        tbUnitGaleri.Judul = Data.Judul;
                        tbUnitGaleri.Keterangan = Data.Keterangan;
                        conn.Update(tbUnitGaleri);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UnitGaleriEdit", ex);
            }
        }
        public string UnitGaleriReject(int IdUser, int IdUnitGaleri)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbUnitGaleri tbUnitGaleri = conn.Get<TbUnitGaleri>(IdUnitGaleri);

                        var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                             join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                             where a.IdPegawai == IdUser
                                             select new { a, b }).FirstOrDefault();

                        SetAccessKontenUnit(conn, tbUnitPegawai.a.IdUnit, tbUnitPegawai.b.IdJenisJabatan, out bool CanReadKonten, out bool CanCreateKonten, out bool CanApproveKonten, out bool CanPublishKonten, out bool CanRejectKonten, out bool CanDeleteKonten, out bool CanEditKonten);

                        if (CanRejectKonten)
                        {
                            // reject ke status yang dibawah nya
                            tbUnitGaleri.Status = tbUnitGaleri.Status - 1;
                            conn.Update(tbUnitGaleri);
                        }
                        else
                        {
                            return "Anda tidak berhak me-reject data";
                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UnitGaleriReject", ex);
            }
        }
        public string UnitGaleriDelete(int IdUser, int IdUnitGaleri)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbUnitGaleri tbUnitGaleri = conn.Get<TbUnitGaleri>(IdUnitGaleri);
                        var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                             join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                             where a.IdPegawai == IdUser
                                             select new { a, b }).FirstOrDefault();

                        SetAccessKontenUnit(conn, tbUnitPegawai.a.IdUnit, tbUnitPegawai.b.IdJenisJabatan, out bool CanReadKonten, out bool CanCreateKonten, out bool CanApproveKonten, out bool CanPublishKonten, out bool CanRejectKonten, out bool CanDeleteKonten, out bool CanEditKonten);
                        bool canDelete = false;
                        if (tbUnitGaleri.Status == StatusProsesKontenConstant.Published)
                        {
                            if (CanDeleteKonten)
                            {
                                canDelete = true;
                            }
                            else
                            {
                                // jika status data sudah di publish, hanya Admin yang boleh menghapus
                                return "Anda tidak berhak menghapus data";
                            }
                        }
                        if (canDelete)
                        {
                            var pathUpload = AppSetting.PathGaleriUnit;
                            //if (!string.IsNullOrEmpty(err)) return err;
                            if (File.Exists(Path.Combine(pathUpload, tbUnitGaleri.FileUrl)))
                                File.Delete(Path.Combine(pathUpload, tbUnitGaleri.FileUrl));
                            if (tbUnitGaleri == null)
                            {
                                return "data tidak ditemukan!";
                            }
                            conn.Delete(tbUnitGaleri);
                            tx.Commit();
                        }
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UnitGaleriDelete", ex);
            }
        }

        public List<UnitKegiatanReadModel> GetUnitKegiatans(int IdUser, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<UnitKegiatanReadModel> ret = new List<UnitKegiatanReadModel>();
                using (var conn = commonService.DbConnection())
                {
                    bool canReadAll = false;
                    var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                         join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                         where a.IdPegawai == IdUser
                                         select new { a, b }).FirstOrDefault();
                    SetAccessKontenUnit(conn, tbUnitPegawai.a.IdUnit, tbUnitPegawai.b.IdJenisJabatan, out bool CanReadKonten, out bool CanCreateKonten, out bool CanApproveKonten, out bool CanPublishKonten, out bool CanRejectKonten, out bool CanDeleteKonten, out bool CanEditKonten);

                    if (tbUnitPegawai == null) { oMessage = string.Format("id_user {0} tidak terdaftar di unit", IdUser); return null; }

                    if (tbUnitPegawai.a.IdUnit == UnitConstant.DIR || tbUnitPegawai.a.IdUnit == UnitConstant.PER)
                    {
                        canReadAll = true;
                    }

                    var data = (from a in conn.GetList<TbUnitKegiatan>()
                                join b in conn.GetList<TbUnit>() on a.IdUnit equals b.IdUnit
                                where a.IdUnit == (canReadAll == true ? a.IdUnit : tbUnitPegawai.a.IdUnit)
                                select new { a, b }).ToList();
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }

                    foreach (var item in data)
                    {
                        UnitKegiatanReadModel retUnit = new UnitKegiatanReadModel();


                        retUnit.ActionButton = new List<ActionButtonModel>();

                        ActionButtonModel mAction = new ActionButtonModel();
                        mAction.ButtonDelete = CanDeleteKonten;
                        mAction.ButtonEdit = CanEditKonten;
                        if (item.a.Status > 0)
                        {
                            mAction.ButtonReject = CanRejectKonten;
                        }
                        retUnit.ActionButton.Add(mAction);

                        retUnit.IdUnitKegiatan = item.a.IdUnitKegiatan;
                        retUnit.IdUnit = item.a.IdUnit;
                        retUnit.Unit = item.b.Nama;
                        retUnit.Judul = item.a.Judul;
                        retUnit.Keterangan = item.a.Keterangan;
                        retUnit.FileImage = item.a.FileImage;
                        retUnit.Url = item.a.Url;
                        retUnit.NamaJenis = JenisDataKontenUnitConstant.DictJenisDataKontenUnit[item.a.Jenis];
                        retUnit.Jenis = item.a.Jenis;
                        retUnit.NamaStatus = StatusProsesKontenConstant.Dict[item.a.Status];
                        retUnit.Status = item.a.Status;
                        ret.Add(retUnit);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetUnitKegiatans", ex);
                return null;
            }
        }
        public UnitKegiatanReadModel GetUnitKegiatan(int IdUnitKegiatan, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                UnitKegiatanReadModel ret = new UnitKegiatanReadModel();
                using (var conn = commonService.DbConnection())
                {

                    var item = (from a in conn.GetList<TbUnitKegiatan>()
                                join b in conn.GetList<TbUnit>() on a.IdUnit equals b.IdUnit
                                where a.IdUnitKegiatan == IdUnitKegiatan
                                select new { a, b }).FirstOrDefault();

                    if (item == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }

                    ret = new UnitKegiatanReadModel
                    {
                        IdUnitKegiatan = item.a.IdUnitKegiatan,
                        IdUnit = item.a.IdUnit,
                        Unit = item.b.Nama,
                        Judul = item.a.Judul,
                        Keterangan = item.a.Keterangan,
                        FileImage = item.a.FileImage,
                        Url = item.a.Url,
                        NamaJenis = JenisDataKontenUnitConstant.DictJenisDataKontenUnit[item.a.Jenis],
                        Jenis = item.a.Jenis,
                        NamaStatus = StatusProsesKontenConstant.Dict[item.a.Status],
                        Status = item.a.Status
                    };
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetUnitKegiatan", ex);
                return null;
            }
        }
        public string UnitKegiatanAdd(int IdUser, UnitKegiatanAddEditModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    bool canReadAll = false;
                    var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                         where a.IdPegawai == IdUser
                                         select a).FirstOrDefault();
                    if (tbUnitPegawai == null) { return string.Format("id_user {0} tidak terdaftar di unit", IdUser); }

                    if (tbUnitPegawai.IdUnit == UnitConstant.Tk || tbUnitPegawai.IdUnit == UnitConstant.Sd || tbUnitPegawai.IdUnit == UnitConstant.Smp || tbUnitPegawai.IdUnit == UnitConstant.Sma)
                    {
                        canReadAll = true;
                    }
                    if (!canReadAll)
                    {
                        if (Data.IdUnit != tbUnitPegawai.IdUnit)
                            return "anda tidak berhak input data kegiatan unit";
                    }
                    using (var tx = conn.BeginTransaction())
                    {
                        string fileName = string.Empty;
                        if (Data.FileImage != null && Data.FileImage.Length > 0)
                        {
                            var pathUpload = AppSetting.PathKegiatanUnit;

                            fileName = Path.GetRandomFileName() + Path.GetExtension(Data.FileImage.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, fileName), FileMode.Create, FileAccess.Write))
                            {
                                Data.FileImage.CopyTo(fileStream);
                            }
                        }


                        var tbUnitKegiatans = conn.GetList<TbUnitKegiatan>();
                        int idUnitKegiatan = 0;
                        if (tbUnitKegiatans == null || tbUnitKegiatans.Count() == 0)
                        {
                            idUnitKegiatan = 1;
                        }
                        else
                        {
                            var maxUnitKegiatan = (from a in conn.GetList<TbUnitKegiatan>()
                                                   select new { a.IdUnitKegiatan }).OrderByDescending(g => g.IdUnitKegiatan).FirstOrDefault();
                            idUnitKegiatan = maxUnitKegiatan.IdUnitKegiatan + 1;
                        }
                        TbUnitKegiatan tbUnitKegiatan = new TbUnitKegiatan
                        {
                            IdUnitKegiatan = idUnitKegiatan,
                            CreatedBy = IdUser,
                            CreatedDate = dateTimeService.GetCurrdate(),
                            FileImage = fileName,
                            IdUnit = tbUnitPegawai.IdUnit,
                            Jenis = Data.Jenis,
                            Judul = Data.Judul,
                            Keterangan = Data.Keterangan,
                            Status = StatusProsesKontenConstant.New,
                            Url = Data.Url
                        };

                        conn.Insert(tbUnitKegiatan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UnitKegiatanAdd", ex);
            }
        }
        private void SetAccessKontenUnit(IDbConnection conn, int IdUnit, int IdJabatan,
            out bool CanReadKonten,
            out bool CanCreateKonten,
            out bool CanApproveKonten,
            out bool CanPublishKonten,
            out bool CanRejectKonten,
            out bool CanDeleteKonten,
            out bool CanEditKonten)
        {
            CanReadKonten = schService.CanAksesByJabatan(conn, IdJabatan, HakAksesConstant.CanReadKonten);
            CanCreateKonten = schService.CanAksesByJabatan(conn, IdJabatan, HakAksesConstant.CanCreateKonten);
            CanApproveKonten = schService.CanAksesByJabatan(conn, IdJabatan, HakAksesConstant.CanApproveKonten);
            CanPublishKonten = schService.CanAksesByJabatan(conn, IdJabatan, HakAksesConstant.CanPublishKonten);
            CanRejectKonten = schService.CanAksesByJabatan(conn, IdJabatan, HakAksesConstant.CanRejectKonten);
            CanDeleteKonten = schService.CanAksesByJabatan(conn, IdJabatan, HakAksesConstant.CanDeleteKonten);
            CanEditKonten = schService.CanAksesByJabatan(conn, IdJabatan, HakAksesConstant.CanEditKonten);
            if (IdUnit == UnitConstant.DIR)
            {
                CanReadKonten = true;
                CanCreateKonten = true;
                CanPublishKonten = true;
                CanRejectKonten = true;
                CanDeleteKonten = true;
                CanEditKonten = true;
            }
            if (IdUnit == 401)
            {
                CanReadKonten = true;
                CanCreateKonten = true;
                CanPublishKonten = true;
                CanRejectKonten = true;
                CanDeleteKonten = true;
                CanEditKonten = true;
            }
        }
        public string UnitKegiatanEdit(int IdUser, UnitKegiatanAddEditModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbUnitKegiatan tbUnitkegiatan = conn.Get<TbUnitKegiatan>(Data.IdUnitKegiatan);
                        if (tbUnitkegiatan == null) return "data tidak ada";

                        var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                             join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                             where a.IdPegawai == IdUser
                                             select new { a, b }).FirstOrDefault();

                        SetAccessKontenUnit(conn, tbUnitPegawai.a.IdUnit, tbUnitPegawai.b.IdJenisJabatan, out bool CanReadKonten, out bool CanCreateKonten, out bool CanApproveKonten, out bool CanPublishKonten, out bool CanRejectKonten, out bool CanDeleteKonten, out bool CanEditKonten);

                        int statusData = 0;
                        if (tbUnitkegiatan.Status == StatusProsesKontenConstant.New)
                        {
                            if (CanApproveKonten)
                            {
                                statusData = StatusProsesKontenConstant.Approved;
                            }
                            else
                            {
                                if (tbUnitkegiatan.IdUnit != tbUnitPegawai.a.IdUnit)
                                {
                                    return "Data " + tbUnitkegiatan.Judul + " harus " + StatusProsesKontenConstant.Dict[tbUnitkegiatan.Status + 1] + " terlebih dahulu";
                                }
                                // return "Data " + tbUnitkegiatan.Judul + " harus " + StatusProsesKontenConstant.Dict[tbUnitkegiatan.Status + 1] + " terlebih dahulu";
                            }
                        }
                        else if (tbUnitkegiatan.Status == StatusProsesKontenConstant.Approved)
                        {
                            if (CanPublishKonten)
                            {
                                if (tbUnitkegiatan.Jenis == JenisDataKontenUnitConstant.Profil)
                                {
                                    // update STATUS semua data yang JENIS nya profil menjadi non-aktif
                                    var tbUnitKegiatans = conn.GetList<TbUnitKegiatan>().ToList();
                                    foreach (var item in tbUnitKegiatans)
                                    {
                                        if (item.Jenis == JenisDataKontenUnitConstant.Profil)
                                        {
                                            item.Status = StatusProsesKontenConstant.InActive;
                                            conn.Update(item);
                                        }
                                    }
                                }
                                statusData = StatusProsesKontenConstant.Published;
                            }
                            else
                            {
                                return "Data " + tbUnitkegiatan.Judul + " harus " + StatusProsesKontenConstant.Dict[tbUnitkegiatan.Status + 1] + " terlebih dahulu";
                            }
                        }
                        else
                        {
                            return "Anda tidak berhak merubah data";
                        }
                        tbUnitkegiatan.Status = statusData;
                        tbUnitkegiatan.Jenis = Data.Jenis;

                        string fileName = string.Empty;
                        if (Data.FileImage != null && Data.FileImage.Length > 0)
                        {
                            var pathUpload = AppSetting.PathKegiatanUnit;
                            //if (!string.IsNullOrEmpty(err)) return err;
                            if (File.Exists(Path.Combine(pathUpload, tbUnitkegiatan.FileImage)))
                                File.Delete(Path.Combine(pathUpload, tbUnitkegiatan.FileImage));

                            fileName = Path.GetRandomFileName() + Path.GetExtension(Data.FileImage.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, fileName), FileMode.Create, FileAccess.Write))
                            {
                                Data.FileImage.CopyTo(fileStream);
                            }
                            tbUnitkegiatan.FileImage = fileName;
                        }
                        tbUnitkegiatan.Judul = Data.Judul;
                        tbUnitkegiatan.Keterangan = Data.Keterangan;
                        tbUnitkegiatan.Url = Data.Url;
                        conn.Update(tbUnitkegiatan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UnitKegiatanEdit", ex);
            }
        }
        public string UnitKegiatanReject(int IdUser, int IdUnitKegiatan)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbUnitKegiatan tbUnitKegiatan = conn.Get<TbUnitKegiatan>(IdUnitKegiatan);

                        var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                             join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                             where a.IdPegawai == IdUser
                                             select new { a, b }).FirstOrDefault();

                        SetAccessKontenUnit(conn, tbUnitPegawai.a.IdUnit, tbUnitPegawai.b.IdJenisJabatan, out bool CanReadKonten, out bool CanCreateKonten, out bool CanApproveKonten, out bool CanPublishKonten, out bool CanRejectKonten, out bool CanDeleteKonten, out bool CanEditKonten);

                        if (CanRejectKonten)
                        {
                            // reject ke status yang dibawah nya
                            tbUnitKegiatan.Status = tbUnitKegiatan.Status - 1;
                            conn.Update(tbUnitKegiatan);
                        }
                        else
                        {
                            return "Anda tidak berhak me-reject data";
                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UnitKegiatanReject", ex);
            }
        }
        public string UnitKegiatanDelete(int IdUser, int IdUnitKegiatan)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbUnitKegiatan tbUnitKegiatan = conn.Get<TbUnitKegiatan>(IdUnitKegiatan);
                        var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                             join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                             where a.IdPegawai == IdUser
                                             select new { a, b }).FirstOrDefault();

                        SetAccessKontenUnit(conn, tbUnitPegawai.a.IdUnit, tbUnitPegawai.b.IdJenisJabatan, out bool CanReadKonten, out bool CanCreateKonten, out bool CanApproveKonten, out bool CanPublishKonten, out bool CanRejectKonten, out bool CanDeleteKonten, out bool CanEditKonten);
                        bool canDelete = false;
                        if (tbUnitKegiatan.Status == StatusProsesKontenConstant.Published)
                        {
                            if (CanDeleteKonten)
                            {
                                canDelete = true;
                            }
                            else
                            {
                                // jika status data sudah di publish, hanya Admin yang boleh menghapus
                                return "Anda tidak berhak menghapus data";
                            }
                        }

                        if (CanDeleteKonten)
                        {
                            conn.Delete(tbUnitKegiatan);
                            var pathUpload = AppSetting.PathKegiatanUnit;
                            //if (!string.IsNullOrEmpty(err)) return err;
                            if (File.Exists(Path.Combine(pathUpload, tbUnitKegiatan.FileImage)))
                                File.Delete(Path.Combine(pathUpload, tbUnitKegiatan.FileImage));
                            tx.Commit();
                        }
                        else
                        {
                            // jika status data sudah di publish, hanya Admin yang boleh menghapus
                            return "Anda tidak berhak menghapus data";
                        }
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UnitKegiatanDelete", ex);
            }
        }

        public List<PegawaiModel> GetUnitPegawais(int IdUnit, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var pegawais = schService.GetPegawais(conn, StatusDataConstant.Aktif, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;

                    var data = from a in pegawais
                               join b in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals b.IdPegawai
                               where b.IdUnit == IdUnit
                               select a;
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetUnitPegawais", ex);
                return null;

            }
        }
        public string UnitPegawaiAdd(int IdUser, int IdUnit, List<PegawaiJenisModel> Pegawais)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        foreach (var pegawai in Pegawais)
                        {
                            var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                                 where a.IdUnit == IdUnit & a.IdPegawai == pegawai.IdPegawai
                                                 select a).FirstOrDefault();
                            if (tbUnitPegawai == null)
                            {
                                tbUnitPegawai = new TbUnitPegawai { IdUnit = IdUnit, IdPegawai = pegawai.IdPegawai };
                                conn.Insert(tbUnitPegawai);
                            }
                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UnitPegawaiAdd", ex);
            }
        }
        public string UnitPegawaiDelete(int IdUser, int IdUnit, List<PegawaiJenisModel> Pegawais)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        foreach (var pegawai in Pegawais)
                        {
                            var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                                 where a.IdUnit == IdUnit & a.IdPegawai == pegawai.IdPegawai
                                                 select a).FirstOrDefault();
                            if (tbUnitPegawai == null) return "data tidak ada";
                            conn.Delete(tbUnitPegawai);
                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UnitPegawaiDelete", ex);
            }
        }

        public List<UnitEkskulReadModel> GetUnitEkskuls(int IdUser, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    bool canReadAll = false;
                    var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                         join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                         where a.IdPegawai == IdUser
                                         select new { a, b }).FirstOrDefault();
                    SetAccessKontenUnit(conn, tbUnitPegawai.a.IdUnit, tbUnitPegawai.b.IdJenisJabatan, out bool CanReadKonten, out bool CanCreateKonten, out bool CanApproveKonten, out bool CanPublishKonten, out bool CanRejectKonten, out bool CanDeleteKonten, out bool CanEditKonten);

                    if (tbUnitPegawai == null) { oMessage = string.Format("id_user {0} tidak terdaftar di unit", IdUser); return null; }

                    if (tbUnitPegawai.a.IdUnit == UnitConstant.DIR || tbUnitPegawai.a.IdUnit == UnitConstant.PER)
                    {
                        canReadAll = true;
                    }

                    if (tbUnitPegawai.a.IdPegawai == 102)
                    {
                        canReadAll = true;
                    }

                    var datas = (from a in conn.GetList<TbUnitEkskul>()
                                 where a.IdUnit == (canReadAll == true ? a.IdUnit : tbUnitPegawai.a.IdUnit)
                                 select a).ToList();

                    if (datas.Count == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    List<UnitEkskulReadModel> ret = new List<UnitEkskulReadModel>();
                    foreach (var data in datas)
                    {
                        UnitEkskulReadModel retAction = new UnitEkskulReadModel();
                        retAction.ActionButton = new List<ActionButtonModel>();

                        ActionButtonModel mAction = new ActionButtonModel();
                        mAction.ButtonDelete = CanDeleteKonten;
                        mAction.ButtonEdit = CanEditKonten;
                        if (data.Status > 0)
                        {
                            mAction.ButtonReject = CanRejectKonten;
                        }
                        retAction.ActionButton.Add(mAction);

                        retAction.IdUnitEkskul = data.IdUnitEkskul;
                        retAction.IdUnit = data.IdUnit;
                        retAction.Judul = data.Judul;
                        retAction.Deskripsi = data.Deskripsi;
                        retAction.FileImage = data.FileImage;
                        retAction.Unit = UnitConstant.DictUnit[data.IdUnit];
                        retAction.Status = StatusProsesKontenConstant.Dict[data.Status];
                        ret.Add(retAction);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetUnitEkskuls", ex);
                return null;
            }
        }

        public UnitEkskulReadModel GetUnitEkskul(int IdUser, int IdUnitEkskul, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbUnitEkskul>().Where(x => x.IdUnitEkskul == IdUnitEkskul).FirstOrDefault();
                    if (data == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    UnitEkskulReadModel ret = new UnitEkskulReadModel
                    {
                        IdUnitEkskul = data.IdUnitEkskul,
                        IdUnit = data.IdUnit,
                        Judul = data.Judul,
                        Deskripsi = data.Deskripsi,
                        FileImage = data.FileImage
                    };
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "UnitPegawaiDelete", ex);
                return null;
            }
        }
        public string UnitEkskulAdd(int IdUser, UnitEkskulAddEditModel Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    bool canReadAll = false;
                    var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                         where a.IdPegawai == IdUser
                                         select a).FirstOrDefault();
                    if (tbUnitPegawai == null) { oMessage = string.Format("id user {0} tidak terdaftar di unit", IdUser); return null; }

                    if (tbUnitPegawai.IdUnit == UnitConstant.Tk || tbUnitPegawai.IdUnit == UnitConstant.Sd || tbUnitPegawai.IdUnit == UnitConstant.Smp || tbUnitPegawai.IdUnit == UnitConstant.Sma)
                    {
                        canReadAll = true;
                    }
                    if (!canReadAll)
                    {
                        if (Data.IdUnit != tbUnitPegawai.IdUnit)
                        {
                            oMessage = "anda tidak berhak input data ekskul unit";
                            return null;
                        }
                    }
                    TbUnitEkskul tbUnitEkskul = new TbUnitEkskul
                    {
                        Judul = Data.Judul,
                        IdUnit = tbUnitPegawai.IdUnit,
                        Deskripsi = Data.Deskripsi,
                        Status = StatusProsesKontenConstant.New,
                        CreatedBy = IdUser,
                        CreatedDate = dateTimeService.GetCurrdate(),
                    };
                    var pathUpload = AppSetting.PathEkskulUnit;
                    if (Data.FileImage != null)
                    {
                        // if (File.Exists(Path.Combine(pathUpload, tbUnitEkskul.FileImage)))
                        //     File.Delete(Path.Combine(pathUpload, tbUnitEkskul.FileImage));

                        tbUnitEkskul.FileImage = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.UnitEkskul] + Path.GetExtension(Data.FileImage.FileName);
                    }
                    else
                    {
                        oMessage = "foto wajib diupload!";
                        return null;
                    }
                    conn.Insert(tbUnitEkskul);
                    using (var fileStream = new FileStream(Path.Combine(pathUpload, tbUnitEkskul.FileImage), FileMode.Create, FileAccess.Write))
                    {
                        Data.FileImage.CopyTo(fileStream);
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "UnitEkskulAdd", ex);
                return null;
            }
        }

        public string UnitEkskulEdit(int IdUser, UnitEkskulAddEditModel Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbUnitEkskul = conn.GetList<TbUnitEkskul>().Where(x => x.IdUnitEkskul == Data.IdUnitEkskul).FirstOrDefault();
                    if (tbUnitEkskul == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }

                    var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                         join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                         where a.IdPegawai == IdUser
                                         select new { a, b }).FirstOrDefault();

                    SetAccessKontenUnit(conn, tbUnitPegawai.a.IdUnit, tbUnitPegawai.b.IdJenisJabatan, out bool CanReadKonten, out bool CanCreateKonten, out bool CanApproveKonten, out bool CanPublishKonten, out bool CanRejectKonten, out bool CanDeleteKonten, out bool CanEditKonten);

                    int statusData = 0;
                    if (tbUnitEkskul.Status == StatusProsesKontenConstant.New)
                    {
                        if (CanApproveKonten)
                        {
                            statusData = StatusProsesKontenConstant.Approved;
                        }
                        else
                        {
                            if (tbUnitEkskul.IdUnit != tbUnitPegawai.a.IdUnit)
                            {
                                oMessage = "Data " + tbUnitEkskul.Judul + " harus " + StatusProsesKontenConstant.Dict[tbUnitEkskul.Status + 1] + " terlebih dahulu";
                                return null;
                            }
                        }
                    }
                    else if (tbUnitEkskul.Status == StatusProsesKontenConstant.Approved)
                    {
                        if (CanPublishKonten)
                        {
                            statusData = StatusProsesKontenConstant.Published;
                        }
                        else
                        {
                            oMessage = "Data " + tbUnitEkskul.Judul + " harus " + StatusProsesKontenConstant.Dict[tbUnitEkskul.Status + 1] + " terlebih dahulu";
                            return null;
                        }
                    }
                    else
                    {
                        oMessage = "Anda tidak berhak merubah data";
                        return null;
                    }

                    tbUnitEkskul.Judul = Data.Judul;
                    tbUnitEkskul.Deskripsi = Data.Deskripsi;
                    tbUnitEkskul.Status = statusData;
                    tbUnitEkskul.UpdatedBy = IdUser;
                    tbUnitEkskul.UpdatedDate = dateTimeService.GetCurrdate();

                    var pathUpload = AppSetting.PathEkskulUnit;
                    if (Data.FileImage != null)
                    {
                        if (File.Exists(Path.Combine(pathUpload, tbUnitEkskul.FileImage)))
                            File.Delete(Path.Combine(pathUpload, tbUnitEkskul.FileImage));

                        tbUnitEkskul.FileImage = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.UnitEkskul] + Path.GetExtension(Data.FileImage.FileName);
                        using (var fileStream = new FileStream(Path.Combine(pathUpload, tbUnitEkskul.FileImage), FileMode.Create, FileAccess.Write))
                        {
                            Data.FileImage.CopyTo(fileStream);
                        }
                    }
                    conn.Update(tbUnitEkskul);

                    return null;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "UnitEkskulEdit", ex);
                return null;
            }
        }
        public string UnitEkskulReject(int IdUser, int IdUnitEkskul, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbUnitEkskul tbUnitEkskul = conn.Get<TbUnitEkskul>(IdUnitEkskul);

                        var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                             join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                             where a.IdPegawai == IdUser
                                             select new { a, b }).FirstOrDefault();

                        SetAccessKontenUnit(conn, tbUnitPegawai.a.IdUnit, tbUnitPegawai.b.IdJenisJabatan, out bool CanReadKonten, out bool CanCreateKonten, out bool CanApproveKonten, out bool CanPublishKonten, out bool CanRejectKonten, out bool CanDeleteKonten, out bool CanEditKonten);

                        if (CanRejectKonten)
                        {
                            // reject ke status yang dibawah nya
                            tbUnitEkskul.Status = tbUnitEkskul.Status - 1;
                            conn.Update(tbUnitEkskul);
                        }
                        else
                        {
                            oMessage = "Anda tidak berhak me-reject data";
                            return null;
                        }
                        tx.Commit();
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "UnitEkskulReject", ex);
                return null;
            }
        }

        public string UnitEkskulDelete(int IdUser, int IdUnitEkskul, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    TbUnitEkskul tbUnitEkskul = conn.Get<TbUnitEkskul>(IdUnitEkskul);
                    var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                         join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                         where a.IdPegawai == IdUser
                                         select new { a, b }).FirstOrDefault();

                    SetAccessKontenUnit(conn, tbUnitPegawai.a.IdUnit, tbUnitPegawai.b.IdJenisJabatan, out bool CanReadKonten, out bool CanCreateKonten, out bool CanApproveKonten, out bool CanPublishKonten, out bool CanRejectKonten, out bool CanDeleteKonten, out bool CanEditKonten);
                    bool canDelete = false;
                    if (tbUnitEkskul.Status == StatusProsesKontenConstant.Published)
                    {
                        if (CanDeleteKonten)
                        {
                            canDelete = true;
                        }
                        else
                        {
                            // jika status data sudah di publish, hanya Admin yang boleh menghapus
                            return "Anda tidak berhak menghapus data";
                        }
                    }

                    if (CanDeleteKonten)
                    {
                        conn.Delete(tbUnitEkskul);
                        var pathUpload = AppSetting.PathEkskulUnit;
                        //if (!string.IsNullOrEmpty(err)) return err;
                        if (File.Exists(Path.Combine(pathUpload, tbUnitEkskul.FileImage)))
                            File.Delete(Path.Combine(pathUpload, tbUnitEkskul.FileImage));
                    }
                    else
                    {
                        // jika status data sudah di publish, hanya Admin yang boleh menghapus
                        return "Anda tidak berhak menghapus data";
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "UnitEkskulDelete", ex);
                return null;
            }
        }

        public List<UnitBannerReadModel> GetUnitBanners(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var datas = conn.GetList<TbUnitBanner>().ToList();
                    if (datas.Count == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    List<UnitBannerReadModel> ret = new();
                    foreach (var data in datas.OrderByDescending(x => x.IdUnitBanner))
                    {
                        UnitBannerReadModel mod = new();
                        mod.IdUnitBanner = data.IdUnitBanner;
                        mod.IdUnit = data.IdUnit;
                        mod.Unit = UnitConstant.DictUnit[data.IdUnit];
                        mod.FileImage = data.FileImage;
                        ret.Add(mod);
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetUnitBanners", ex);
                return null;
            }
        }
        public UnitBannerReadModel GetUnitBanner(int IdUnitBanner, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.Get<TbUnitBanner>(IdUnitBanner);
                    if (data == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    UnitBannerReadModel ret = new();
                    ret.IdUnitBanner = data.IdUnitBanner;
                    ret.IdUnit = data.IdUnit;
                    ret.FileImage = data.FileImage;

                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetUnitBanner", ex);
                return null;
            }
        }
        public string UnitBannerAdd(UnitBannerAddEditModel Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    if (Data.FileImage == null)
                    {
                        oMessage = "gambar wajib di upload!";
                        return null;
                    }
                    TbUnitBanner tbUnitBanner = new();
                    tbUnitBanner.IdUnit = Data.IdUnit;
                    var pathUpload = AppSetting.PathGaleriUnit;
                    tbUnitBanner.FileImage = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.UnitSliderBanner] + Path.GetExtension(Data.FileImage.FileName);
                    using (var fileStream = new FileStream(Path.Combine(pathUpload, tbUnitBanner.FileImage), FileMode.Create, FileAccess.Write))
                    {
                        Data.FileImage.CopyTo(fileStream);
                    }
                    conn.Insert(tbUnitBanner);
                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "UnitBannerAdd", ex);
                return null;
            }
        }
        public string UnitBannerEdit(UnitBannerAddEditModel Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.Get<TbUnitBanner>(Data.IdUnitBanner);
                    if (data == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    var pathUpload = AppSetting.PathGaleriUnit;
                    if (Data.FileImage != null)
                    {
                        if (File.Exists(Path.Combine(pathUpload, data.FileImage)))
                            File.Delete(Path.Combine(pathUpload, data.FileImage));

                        data.FileImage = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.UnitSliderBanner] + Path.GetExtension(Data.FileImage.FileName);
                        using (var fileStream = new FileStream(Path.Combine(pathUpload, data.FileImage), FileMode.Create, FileAccess.Write))
                        {
                            Data.FileImage.CopyTo(fileStream);
                        }
                    }
                    conn.Update(data);

                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "UnitBannerEdit", ex);
                return null;
            }
        }
        public string UnitBannerDelete(int IdUnitBanner, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.Get<TbUnitBanner>(IdUnitBanner);
                    if (data == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    var pathUpload = AppSetting.PathGaleriUnit;
                    if (File.Exists(Path.Combine(pathUpload, data.FileImage)))
                        File.Delete(Path.Combine(pathUpload, data.FileImage));

                    conn.Delete(data);

                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "UnitBannerDelete", ex);
                return null;
            }
        }
    }
}
