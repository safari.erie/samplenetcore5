﻿using Dapper;
using Swg.Entities.Kbm;
using Swg.Entities.Keu;
using Swg.Entities.Lmg;
using Swg.Entities.Public;
using Swg.Entities.Ref;
using Swg.Models;
using Swg.Models.Constants;
using Swg.Sch.Shared.Constants;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Sch.Services
{
    public class KeuPtgjwbService : IKeuPtgjwbService
    {
        private readonly string ServiceName = "Swg.Sch.Services.KeuPtgjwbService.";
        private readonly string _languageCode = "id";
        private readonly string pathUpload = AppSetting.PathApplFile;
        private readonly string urlPathUpload = AppSetting.PathUrlApplFile;

        KeuHakAksesModel hakAkses = new KeuHakAksesModel
        {
            CanAdd = false,
            CanApprove = false,
            CanDelete = false,
            CanDownload = false,
            CanEdit = false,
            CanRead = false,
            CanRelease = false
        };
        private int[] HakAksesPengajuans = new int[] { 41, 42, 43, 44, 45, 46 };
        private readonly ICommonService commonService;
        private readonly IDateTimeService dateTimeService;
        private readonly IKeuPengajuanService pengajuanService;
        public KeuPtgjwbService(
            ICommonService CommonService,
            IDateTimeService DateTimeService,
            IKeuPengajuanService PengajuanService)
        {
            commonService = CommonService;
            dateTimeService = DateTimeService;
            pengajuanService = PengajuanService;
        }
        private void CekAkses(IDbConnection conn, int IdUser)
        {
            var tbAksesRkas = from a in conn.GetList<TbJenisHakAksesKeu>()
                              join b in conn.GetList<TbJenisJabatanHakAksesKeu>() on a.IdJenisHakAksesKeu equals b.IdJenisHakAksesKeu
                              join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                              join d in conn.GetList<TbPegawai>() on c.IdJenisJabatan equals d.IdJenisJabatan
                              where d.IdPegawai == IdUser
                              & HakAksesPengajuans.Contains(a.IdJenisHakAksesKeu)
                              select new { a, b, c };
            foreach (var item in tbAksesRkas)
            {
                if (item.a.NamaSingkat == "CanRead")
                {
                    hakAkses.CanRead = true;
                    hakAkses.CanDownload = hakAkses.CanRead;
                }
                if (item.a.NamaSingkat == "CanAdd")
                {
                    hakAkses.CanAdd = true;
                }
                if (item.a.NamaSingkat == "CanEdit")
                {
                    hakAkses.CanEdit = true;
                }
                if (item.a.NamaSingkat == "CanApprove")
                {
                    hakAkses.CanApprove = true;
                }
                if (item.a.NamaSingkat == "CanDelete")
                {
                    hakAkses.CanDelete = true;
                }
                if (item.a.NamaSingkat == "CanRelease")
                {
                    hakAkses.CanRelease = true;
                }
            }
        }
        private int GetNextStatus(int Status)
        {
            int nextStatus = Status;
            if (Status == JenisStatusPengajuanConstant.Direalisasikan)
            {
                nextStatus = JenisStatusPengajuanConstant.InitDilaporkan;
            }
            else if (Status == JenisStatusPengajuanConstant.InitDilaporkan)
            {
                nextStatus = JenisStatusPengajuanConstant.Dilaporkan;
            }
            else if (Status == JenisStatusPengajuanConstant.Dilaporkan)
            {
                nextStatus = JenisStatusPengajuanConstant.Dibukukan;
            }
            return nextStatus;
        }
        private int GetBeforeStatus(int Status)
        {
            int beforeStatus = Status;
            if (Status == JenisStatusPengajuanConstant.InitDilaporkan)
            {
                beforeStatus = JenisStatusPengajuanConstant.Direalisasikan;
            }
            else if (Status == JenisStatusPengajuanConstant.Dilaporkan)
            {
                beforeStatus = JenisStatusPengajuanConstant.InitDilaporkan;
            }
            else if (Status == JenisStatusPengajuanConstant.Dibukukan)
            {
                beforeStatus = JenisStatusPengajuanConstant.Dilaporkan;
            }
            return beforeStatus;
        }

        private string ApproveData(int IdUser, int IdPengajuan, int StatusProses, string Catatan, int Status)
        {
            try
            {
                int[] statuss = new int[] { JenisStatusPengajuanConstant.Dilaporkan, JenisStatusPengajuanConstant.Dibukukan };
                if (!statuss.Contains(Status))
                    return "status salas";

                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        CekAkses(conn, IdUser);
                        var tbPegawaiUnit = (from a in conn.GetList<TbUnitPegawai>()
                                             join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                             join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                             where a.IdPegawai == IdUser
                                             select new { a, b, c }).FirstOrDefault();
                        if (tbPegawaiUnit == null)
                            return "Pengguna bukan pegawai";

                        if (tbPegawaiUnit.b.IdJenisJabatan != JenisJabatanPegawaiContant.AdmDir)
                        {
                            if (Status == JenisStatusPengajuanConstant.Dilaporkan)
                            {
                                if (!hakAkses.CanApprove)
                                    return "tidak berhak melaporkan data";
                                if (tbPegawaiUnit.b.IdJenisJabatan != JenisJabatanPegawaiContant.KaUnit)
                                    return "tidak berhak melaporkan data";
                            }
                            else if (Status == JenisStatusPengajuanConstant.Dibukukan)
                            {
                                if (!hakAkses.CanRelease)
                                    return "tidak berhak membukukan data";
                                if (tbPegawaiUnit.b.IdJenisJabatan != JenisJabatanPegawaiContant.ManKeu)
                                    return "tidak berhak melaporkan data";
                            }
                        }

                        var tbPengajuan = conn.Get<TbPengajuan>(IdPengajuan);
                        if (tbPengajuan == null) return "data tidak ada";
                        if (StatusProses == StatusProsesKeuanganConstant.Rejected)
                        {
                            if (string.IsNullOrEmpty(Catatan)) return "catatan harus diisi";
                            int beforeStatus = GetBeforeStatus(Status);
                            var tbPengajuanBeforeStatus = (from a in conn.GetList<TbPengajuanStatus>()
                                                           where a.IdPengajuan == IdPengajuan
                                                           & a.Status == beforeStatus
                                                           select a).FirstOrDefault();
                            if (tbPengajuanBeforeStatus == null)
                                return string.Format("Pengajuan {0} tidak ada", JenisStatusPengajuanConstant.Dict[beforeStatus]);
                            tbPengajuanBeforeStatus.UpdatedBy = IdUser;
                            tbPengajuanBeforeStatus.UpdatedDate = dateTimeService.GetCurrdate();
                            tbPengajuanBeforeStatus.StatusProses = StatusProses;
                            tbPengajuanBeforeStatus.Catatan = Catatan;
                            conn.Update(tbPengajuanBeforeStatus);
                        }
                        else
                        {
                            tbPengajuan.Status = Status;
                            conn.Update(tbPengajuan);
                            int nextStatus = GetNextStatus(Status);

                            var tbPengajuanNextStatus = (from a in conn.GetList<TbPengajuanStatus>()
                                                         join b in conn.GetList<TbUser>() on a.CreatedBy equals b.IdUser
                                                         where a.IdPengajuan == IdPengajuan
                                                         & a.Status == nextStatus
                                                         select new { a, b }).FirstOrDefault();
                            if (tbPengajuanNextStatus != null)
                                return string.Format("Pengajuan sudah {0} oleh {1} pada tanggal {2}", JenisStatusPengajuanConstant.Dict[nextStatus], tbPengajuanNextStatus.b.FirstName, dateTimeService.DateToLongString(_languageCode, tbPengajuanNextStatus.a.CreatedDate));
                            var tbPengajuanStatus = (from a in conn.GetList<TbPengajuanStatus>()
                                                     where a.IdPengajuan == IdPengajuan
                                                     & a.Status == Status
                                                     select a).FirstOrDefault();
                            if (tbPengajuanStatus == null)
                            {
                                tbPengajuanStatus = new TbPengajuanStatus
                                {
                                    IdPengajuan = IdPengajuan,
                                    Status = Status,
                                    Catatan = Catatan,
                                    CreatedBy = IdUser,
                                    CreatedDate = dateTimeService.GetCurrdate(),
                                    StatusProses = StatusProses

                                };
                                conn.Insert(tbPengajuanStatus);
                            }
                            else
                            {
                                tbPengajuanStatus.StatusProses = StatusProses;
                                tbPengajuanStatus.UpdatedBy = IdUser;
                                tbPengajuanStatus.UpdatedDate = dateTimeService.GetCurrdate();
                                conn.Update(tbPengajuanStatus);
                            }
                        }

                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "ApproveData", ex);
            }

        }
        public List<KeuPengajuanListModel> GetPengajuans(int IdUser, int IdTahunAjaran, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<KeuPengajuanListModel> ret = new List<KeuPengajuanListModel>();
                var dtPengajuans = pengajuanService.GetPengajuans(IdUser, IdTahunAjaran, out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return null;

                if (dtPengajuans == null || dtPengajuans.Count() == 0) { oMessage = "data tidak ada"; return null; }

                var dt = from a in dtPengajuans
                         where a.Status == JenisStatusPengajuanConstant.Dict[JenisStatusPengajuanConstant.Direalisasikan]
                         select a;
                if (dt == null || dt.Count() == 0) { oMessage = "data tidak ada"; return null; }

                List<string> aksis = new List<string>();
                aksis.Add("View");
                aksis.Add("Download");
                aksis.Add("Pilih");
                foreach (var item in dt)
                {
                    KeuPengajuanListModel m = item;
                    m.Aksis = new List<string>();
                    m.Aksis = aksis;
                    ret.Add(m);
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPengajuans", ex);
                return null;
            }
        }
        public List<KeuPengajuanCoaListModel> GetPengajuanCoas(int IdUser, int IdPengajuan, out string oMessage)
        {
            oMessage = string.Empty;
            return pengajuanService.GetPengajuanCoas(IdUser, IdPengajuan, out oMessage);
        }
        public string SetPilihPengajuan(int IdUser, int IdPengajuan)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbPengajuan = conn.Get<TbPengajuan>(IdPengajuan);
                        if (tbPengajuan.Status != JenisStatusPengajuanConstant.Direalisasikan)
                            return string.Format("data pengajuan sudah {0}", JenisStatusPengajuanConstant.Dict[tbPengajuan.Status]);

                        var tbPengajuanCoas = from a in conn.GetList<TbPengajuanCoa>()
                                              where a.IdPengajuan == IdPengajuan
                                              select a;
                        foreach (var item in tbPengajuanCoas)
                        {
                            TbPtgjwbCoa tbPtgjwbCoa = new TbPtgjwbCoa
                            {
                                IdPengajuan = IdPengajuan,
                                IdCoa = item.IdCoa,
                                Catatan = string.Empty,
                                FileBukti = string.Empty,
                                HargaSatuan = item.HargaSatuan,
                                IdJenisSatuan = item.IdJenisSatuan,
                                IdJenisVolume = item.IdJenisVolume,
                                JumlahSatuan = item.JumlahSatuan,
                                JumlahVolume = item.JumlahVolume
                            };
                            conn.Insert(tbPtgjwbCoa);
                            tbPengajuan.TotalRpDilaporkan += (item.JumlahSatuan * item.JumlahVolume * item.HargaSatuan);
                        }
                        tbPengajuan.Status = JenisStatusPengajuanConstant.InitDilaporkan;
                        conn.Update(tbPengajuan);
                        TbPengajuanStatus tbPengajuanStatus = new TbPengajuanStatus
                        {
                            IdPengajuan = IdPengajuan,
                            Status = JenisStatusPengajuanConstant.InitDilaporkan,
                            CreatedBy = IdUser,
                            CreatedDate = dateTimeService.GetCurrdate(),
                            StatusProses = StatusProsesKeuanganConstant.New
                        };
                        conn.Insert(tbPengajuanStatus);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetPilihPengajuan", ex);
            }

        }
        public List<KeuPtgjwbListModel> GetPertanggungjawabans(int IdUser, int IdTahunAjaran, out string oMessage)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    oMessage = string.Empty;
                    List<KeuPtgjwbListModel> ret = new List<KeuPtgjwbListModel>();
                    List<KeuPtgjwbListModel> tempret = new List<KeuPtgjwbListModel>();

                    var tbPegawaiUnit = (from a in conn.GetList<TbUnitPegawai>()
                                         join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                         join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                         where a.IdPegawai == IdUser
                                         select new { a, b, c }).FirstOrDefault();
                    if (tbPegawaiUnit == null) { oMessage = "Pengguna bukan pegawai"; return null; };
                    bool isAdmin = false;
                    if (tbPegawaiUnit.a.IdUnit == UnitConstant.DIR ||
                        tbPegawaiUnit.a.IdUnit == UnitConstant.FNC ||
                        tbPegawaiUnit.a.IdUnit == UnitConstant.AKT)
                        isAdmin = true;

                    var dtWajibs = from dt in conn.GetList<TbPengajuan>()
                                   join b in conn.GetList<TbUnitProkegWajib>() on dt.IdUnitProkegWajib equals b.IdUnitProkegWajib
                                   join c in conn.GetList<TbUnitProkeg>(string.Format("where status = {0}", JenisStatusProgKegConstant.Disahkan)) on b.IdUnitProkeg equals c.IdUnitProkeg
                                   join keg in conn.GetList<TbProgramKegiatan>() on b.IdKegiatan equals keg.IdKegiatan
                                   join prog in conn.GetList<TbProgram>() on keg.IdProgram equals prog.IdProgram
                                   join unit in conn.GetList<TbUnit>() on c.IdUnit equals unit.IdUnit
                                   join thAjaran in conn.GetList<TbTahunAjaran>() on c.IdTahunAjaran equals thAjaran.IdTahunAjaran
                                   join dt_init in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.InitDilaporkan)) on dt.IdPengajuan equals dt_init.IdPengajuan
                                   join dt_lapor in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.Dilaporkan)) on dt.IdPengajuan equals dt_lapor.IdPengajuan into dt_lapora
                                   from dt_lapor in dt_lapora.DefaultIfEmpty()
                                   join dt_buku in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.Dibukukan)) on dt.IdPengajuan equals dt_buku.IdPengajuan into dt_bukua
                                   from dt_buku in dt_bukua.DefaultIfEmpty()
                                   where c.IdUnit == (isAdmin ? c.IdUnit : tbPegawaiUnit.a.IdUnit)
                                   & c.IdTahunAjaran == IdTahunAjaran
                                   select new { dt, unit, thAjaran, prog, keg, dt_init, dt_lapor, dt_buku };

                    if (dtWajibs != null || dtWajibs.Count() > 0)
                    {
                        foreach (var item in dtWajibs)
                        {
                            KeuPtgjwbListModel m = new KeuPtgjwbListModel
                            {
                                IdPengajuan = item.dt.IdPengajuan,
                                Nomor = item.dt.Nomor,
                                NamaUnit = item.unit.Nama,
                                Status = JenisStatusPengajuanConstant.Dict[item.dt.Status],
                                StatusProses = string.Empty,
                                TahunAjaran = item.thAjaran.Nama,
                                TotalRp = item.dt.TotalRp,
                                TotalRealisasiRp = item.dt.TotalRpRealisasi,
                                Kegiatan = item.keg.Nama,
                                Program = item.prog.Nama,
                                TanggalButuh = item.dt.TanggalButuh.ToString("dd-MM-yyyy"),
                                TotalRpDilaporkan = item.dt.TotalRpDilaporkan,
                                IdUnit = item.unit.IdUnit,
                                Aksis = new List<string>()
                            };
                            if (item.dt.Status == JenisStatusPengajuanConstant.Dilaporkan)
                            {
                                if (item.dt_lapor != null)
                                {
                                    m.TanggalDilaporkan = item.dt_lapor.CreatedDate.ToString("dd-MM-yyyy");
                                    m.StatusProses = StatusProsesKeuanganConstant.Dict[item.dt_lapor.StatusProses];
                                    if (item.dt_lapor.StatusProses == StatusProsesKeuanganConstant.Rejected)
                                        m.StatusProses += " Manager Keu";
                                }
                            }
                            tempret.Add(m);
                        }

                    }

                    var dtTambahans = from dt in conn.GetList<TbPengajuan>()
                                      join b in conn.GetList<TbUnitProkegTambahan>() on dt.IdUnitProkegTambahan equals b.IdUnitProkegTambahan
                                      join c in conn.GetList<TbUnitProkeg>(string.Format("where status = {0}", JenisStatusProgKegConstant.Disahkan)) on b.IdUnitProkeg equals c.IdUnitProkeg
                                      join prog in conn.GetList<TbProgram>() on b.IdProgram equals prog.IdProgram
                                      join unit in conn.GetList<TbUnit>() on c.IdUnit equals unit.IdUnit
                                      join thAjaran in conn.GetList<TbTahunAjaran>() on c.IdTahunAjaran equals thAjaran.IdTahunAjaran
                                      join dt_init in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.InitDilaporkan)) on dt.IdPengajuan equals dt_init.IdPengajuan
                                      join dt_lapor in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.Dilaporkan)) on dt.IdPengajuan equals dt_lapor.IdPengajuan into dt_lapora
                                      from dt_lapor in dt_lapora.DefaultIfEmpty()
                                      join dt_buku in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.Dibukukan)) on dt.IdPengajuan equals dt_buku.IdPengajuan into dt_bukua
                                      from dt_buku in dt_bukua.DefaultIfEmpty()
                                      where c.IdUnit == (isAdmin ? c.IdUnit : tbPegawaiUnit.a.IdUnit)
                                      & c.IdTahunAjaran == IdTahunAjaran
                                      select new { dt, unit, thAjaran, prog, b, dt_init, dt_lapor, dt_buku };

                    if (dtTambahans != null || dtTambahans.Count() > 0)
                    {
                        foreach (var item in dtTambahans)
                        {
                            KeuPtgjwbListModel m = new KeuPtgjwbListModel
                            {
                                IdPengajuan = item.dt.IdPengajuan,
                                Nomor = item.dt.Nomor,
                                IdUnit = item.unit.IdUnit,
                                NamaUnit = item.unit.Nama,
                                Status = JenisStatusPengajuanConstant.Dict[item.dt.Status],
                                StatusProses = string.Empty,
                                TahunAjaran = item.thAjaran.Nama,
                                TotalRp = item.dt.TotalRp,
                                TotalRealisasiRp = item.dt.TotalRpRealisasi,
                                Kegiatan = item.b.NamaKegiatan,
                                Program = item.prog.Nama,
                                TanggalButuh = item.dt.TanggalButuh.ToString("dd-MM-yyyy"),
                                TotalRpDilaporkan = item.dt.TotalRpDilaporkan,
                                Aksis = new List<string>()
                            };
                            if (item.dt.Status == JenisStatusPengajuanConstant.Dilaporkan)
                            {
                                if (item.dt_lapor != null)
                                {
                                    m.TanggalDilaporkan = item.dt_lapor.CreatedDate.ToString("dd-MM-yyyy");
                                    m.StatusProses = StatusProsesKeuanganConstant.Dict[item.dt_lapor.StatusProses];
                                    if (item.dt_lapor.StatusProses == StatusProsesKeuanganConstant.Rejected)
                                        m.StatusProses += " Manager Keu";
                                }
                            }
                            tempret.Add(m);
                        }

                    }
                    foreach (var item in tempret)
                    {
                        KeuPtgjwbListModel m = new KeuPtgjwbListModel();
                        m = item;
                        m.Aksis = new List<string>();
                        CekAkses(conn, IdUser);
                        if (item.IdUnit != tbPegawaiUnit.a.IdUnit)
                        {
                            hakAkses = new KeuHakAksesModel
                            {
                                CanAdd = false,
                                CanApprove = false,
                                CanDelete = false,
                                CanDownload = false,
                                CanEdit = false,
                                CanRead = false,
                                CanRelease = false
                            };

                            if (
                                tbPegawaiUnit.a.IdUnit == UnitConstant.AKT ||
                                tbPegawaiUnit.a.IdUnit == UnitConstant.FNC ||
                                tbPegawaiUnit.a.IdUnit == UnitConstant.DIR
                                )
                            {
                                CekAkses(conn, IdUser);
                            }
                        }


                        if (hakAkses.CanRead) m.Aksis.Add("View");
                        if (hakAkses.CanDownload) m.Aksis.Add("Download");
                        if (item.Status == JenisStatusPengajuanConstant.Dict[JenisStatusPengajuanConstant.InitDilaporkan])
                        {
                            if (hakAkses.CanDelete) m.Aksis.Add("Hapus");
                            if (tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.KaUnit
                                || tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                                if (hakAkses.CanApprove) m.Aksis.Add("Dilaporkan");
                        }
                        else if (item.Status == JenisStatusPengajuanConstant.Dict[JenisStatusPengajuanConstant.Dilaporkan])
                        {
                            if (hakAkses.CanDelete) m.Aksis.Add("Hapus");
                            if (tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.ManKeu
                                || tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                                if (hakAkses.CanRelease) m.Aksis.Add("Dibukukan");
                        }

                        if (hakAkses.CanRead)
                            ret.Add(m);
                    }

                    if (ret.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPertanggungjawabans", ex);
                return null;
            }

        }
        public List<KeuPtgjwbCoaListModel> GetPertanggungjawabanCoas(int IdUser, int IdPengajuan, out string oMessage)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    List<KeuPtgjwbCoaListModel> ret = new List<KeuPtgjwbCoaListModel>();
                    oMessage = string.Empty;
                    CekAkses(conn, IdUser);

                    var data = from a in conn.GetList<TbPtgjwbCoa>()
                               join b in conn.GetList<TbCoa>() on a.IdCoa equals b.IdCoa
                               join c in conn.GetList<TbJenisSatuan>() on a.IdJenisSatuan equals c.IdJenisSatuan
                               join d in conn.GetList<TbJenisVolume>() on a.IdJenisVolume equals d.IdJenisVolume
                               where a.IdPengajuan == IdPengajuan
                               select new { a, b, c, d };
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    foreach (var item in data)
                    {
                        KeuPtgjwbCoaListModel m = new KeuPtgjwbCoaListModel
                        {
                            IdPtgjwbCoa = item.a.IdPtgjwbCoa,
                            Catatan = item.a.Catatan,
                            HargaSatuan = item.a.HargaSatuan,
                            JumlahSatuan = item.a.JumlahSatuan,
                            JumlahVolume = item.a.JumlahVolume,
                            Satuan = item.c.Nama,
                            Volume = item.d.Nama,
                            Rp = item.a.JumlahSatuan * item.a.JumlahVolume * item.a.HargaSatuan,
                            Aksis = new List<string>(),
                            KodeCoa = item.b.Kode,
                            NamaCoa = item.b.Nama,
                            IdCoa = item.b.IdCoa,
                            IdJenisSatuan = item.c.IdJenisSatuan,
                            IdJenisVolume = item.d.IdJenisVolume
                        };
                        if (!string.IsNullOrEmpty(item.a.FileBukti))
                            m.UrlFileBukti = string.Format("{0}/{1}", urlPathUpload, item.a.FileBukti);
                        if (hakAkses.CanEdit) m.Aksis.Add("Edit");
                        if (hakAkses.CanDelete) m.Aksis.Add("Hapus");
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPertanggungjawabanCoas", ex);
                return null;
            }

        }
        public string PertanggungjawabanDelete(int IdUser, int IdPengajuan)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    if (!hakAkses.CanDelete) return "tidak berhak hapus pertanggungjawaban";
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbPengajuan = conn.Get<TbPengajuan>(IdPengajuan);
                        if (tbPengajuan == null) return "data tidak ada";

                        int[] statuss = new int[] { JenisStatusPengajuanConstant.InitDilaporkan, JenisStatusPengajuanConstant.Dilaporkan };
                        if (!statuss.Contains(tbPengajuan.Status))
                            return string.Format("status data sudah {0}", JenisStatusPengajuanConstant.Dict[tbPengajuan.Status]);

                        conn.DeleteList<TbPtgjwbCoa>(string.Format("where id_pengajuan = {0}", tbPengajuan.IdPengajuan));
                        conn.DeleteList<TbPengajuanStatus>(string.Format("where id_pengajuan = {0} AND status = {1}", tbPengajuan.IdPengajuan, JenisStatusPengajuanConstant.InitDilaporkan));
                        conn.DeleteList<TbPengajuanStatus>(string.Format("where id_pengajuan = {0} AND status = {1}", tbPengajuan.IdPengajuan, JenisStatusPengajuanConstant.Dilaporkan));
                        tbPengajuan.Status = JenisStatusPengajuanConstant.Direalisasikan;
                        conn.Update(tbPengajuan);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PertanggungjawabanDelete", ex);
            }

        }

        public string PertanggungjawabanCoaAdd(int IdUser, KeuPtgjwbCoaAddModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    using (var tx = conn.BeginTransaction())
                    {
                        if (!hakAkses.CanAdd) return "tidak berhak merubah pengajuan";
                        TbCoa tbCoa = conn.Get<TbCoa>(Data.IdCoa);
                        if (tbCoa == null) return "data tidak ada";

                        TbPengajuan tbPengajuan = conn.Get<TbPengajuan>(Data.IdPengajuan);
                        if (tbPengajuan == null) return "data Pengajuan tidak ada";

                        int[] statuss = new int[] { JenisStatusPengajuanConstant.InitDilaporkan, JenisStatusPengajuanConstant.Dilaporkan };
                        if (!statuss.Contains(tbPengajuan.Status))
                            return string.Format("status data sudah {0}", JenisStatusPengajuanConstant.Dict[tbPengajuan.Status]);

                        var cekData = (from a in conn.GetList<TbPtgjwbCoa>()
                                       join d in conn.GetList<TbCoa>() on a.IdCoa equals d.IdCoa
                                       where a.IdCoa == Data.IdCoa
                                       & a.IdPengajuan == Data.IdPengajuan
                                       select new { a, d }).FirstOrDefault();
                        if (cekData != null)
                            return string.Format("kode akun {0} sudah ada untuk pengajuan ini", cekData.d.Kode);

                        double rpTotal = Data.JumlahSatuan * Data.JumlahVolume * Data.HargaSatuan;


                        tbPengajuan.TotalRpDilaporkan += rpTotal;
                        string fileName = string.Empty;
                        if (Data.FileUpload != null && Data.FileUpload.Length > 0)
                        {
                            fileName = Path.GetRandomFileName() + "." + Path.GetExtension(Data.FileUpload.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, fileName), FileMode.Create, FileAccess.Write))
                            {
                                Data.FileUpload.CopyTo(fileStream);
                            }
                        }
                        TbPtgjwbCoa tbPtgjwbCoa = new TbPtgjwbCoa
                        {
                            IdCoa = Data.IdCoa,
                            Catatan = Data.Catatan,
                            HargaSatuan = Data.HargaSatuan,
                            IdJenisSatuan = Data.IdJenisSatuan,
                            IdJenisVolume = Data.IdJenisVolume,
                            IdPengajuan = Data.IdPengajuan,
                            JumlahSatuan = Data.JumlahSatuan,
                            JumlahVolume = Data.JumlahVolume,
                            FileBukti = fileName
                        };
                        conn.Insert(tbPtgjwbCoa);
                        conn.Update(tbPengajuan);
                        
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PertanggungjawabanCoaAdd", ex);
            }

        }
        public string PertanggungjawabanCoaEdit(int IdUser, KeuPtgjwbCoaEditModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    using (var tx = conn.BeginTransaction())
                    {
                        if (!hakAkses.CanAdd) return "tidak berhak merubah pertanggungjawaban";
                        TbPtgjwbCoa tbPtgjwbCoa = conn.Get<TbPtgjwbCoa>(Data.IdPtgjwbCoa);
                        if (tbPtgjwbCoa == null) return "data tidak ada";

                        TbPengajuan tbPengajuan = conn.Get<TbPengajuan>(tbPtgjwbCoa.IdPengajuan);
                        if (tbPengajuan == null) return "data Pengajuan tidak ada";
                        int[] statuss = new int[] { JenisStatusPengajuanConstant.InitDilaporkan, JenisStatusPengajuanConstant.Dilaporkan };
                        if (!statuss.Contains(tbPengajuan.Status))
                            return string.Format("status data sudah {0}", JenisStatusPengajuanConstant.Dict[tbPengajuan.Status]);

                        double rpTotalOld = tbPtgjwbCoa.JumlahSatuan * tbPtgjwbCoa.JumlahVolume * tbPtgjwbCoa.HargaSatuan;


                        double rpTotal = Data.JumlahSatuan * Data.JumlahVolume * Data.HargaSatuan;
                        tbPengajuan.TotalRp -= rpTotalOld;
                        tbPengajuan.TotalRp += rpTotal;

                        tbPtgjwbCoa.Catatan = Data.Catatan;
                        tbPtgjwbCoa.HargaSatuan = Data.HargaSatuan;
                        tbPtgjwbCoa.IdJenisSatuan = Data.IdJenisSatuan;
                        tbPtgjwbCoa.IdJenisVolume = Data.IdJenisVolume;
                        tbPtgjwbCoa.JumlahSatuan = Data.JumlahSatuan;
                        tbPtgjwbCoa.JumlahVolume = Data.JumlahVolume;

                        string fileName = string.Empty;
                        if (Data.FileUpload != null && Data.FileUpload.Length > 0)
                        {
                            if (!string.IsNullOrEmpty(tbPtgjwbCoa.FileBukti))
                                if (File.Exists(Path.Combine(pathUpload, tbPtgjwbCoa.FileBukti)))
                                    File.Delete(Path.Combine(pathUpload, tbPtgjwbCoa.FileBukti));

                            fileName = Path.GetRandomFileName() + "." + Path.GetExtension(Data.FileUpload.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, fileName), FileMode.Create, FileAccess.Write))
                            {
                                Data.FileUpload.CopyTo(fileStream);
                            }
                            tbPtgjwbCoa.FileBukti = fileName;
                        }
                        conn.Update(tbPtgjwbCoa);
                        conn.Update(tbPengajuan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PengajuanCoaEdit", ex);
            }

        }
        public string PertanggungjawabanCoaDelete(int IdUser, int IdPtgjwbCoa)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    using (var tx = conn.BeginTransaction())
                    {
                        if (!hakAkses.CanAdd) return "tidak berhak menghapus pertanggungjawaban";
                        TbPtgjwbCoa tbPtgjwbCoa = conn.Get<TbPtgjwbCoa>(IdPtgjwbCoa);
                        if (tbPtgjwbCoa == null) return "data tidak ada";

                        TbPengajuan tbPengajuan = conn.Get<TbPengajuan>(tbPtgjwbCoa.IdPengajuan);
                        if (tbPengajuan == null) return "data Pengajuan tidak ada";
                        int[] statuss = new int[] { JenisStatusPengajuanConstant.InitDilaporkan, JenisStatusPengajuanConstant.Dilaporkan };
                        if (!statuss.Contains(tbPengajuan.Status))
                            return string.Format("status data sudah {0}", JenisStatusPengajuanConstant.Dict[tbPengajuan.Status]);


                        double rpTotal = tbPtgjwbCoa.JumlahSatuan * tbPtgjwbCoa.JumlahVolume * tbPtgjwbCoa.HargaSatuan;
                        tbPengajuan.TotalRp -= rpTotal;

                        if (!string.IsNullOrEmpty(tbPtgjwbCoa.FileBukti))
                            if (File.Exists(Path.Combine(pathUpload, tbPtgjwbCoa.FileBukti)))
                                File.Delete(Path.Combine(pathUpload, tbPtgjwbCoa.FileBukti));

                        conn.Delete(tbPtgjwbCoa);

                        conn.Update(tbPengajuan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PertanggungjawabanCoaDelete", ex);
            }

        }
        public string SetDilaporkan(int IdUser, int IdPengajuan)
        {
            return ApproveData(IdUser, IdPengajuan, 0, string.Empty, JenisStatusPengajuanConstant.Dilaporkan);
        }
        public string SetDibukukan(int IdUser, int IdPengajuan, int StatusProses, string Catatan)
        {
            return ApproveData(IdUser, IdPengajuan, StatusProses, Catatan, JenisStatusPengajuanConstant.Dibukukan);
        }
        public KeuPtgjwbDownloadModel GetPertanggungjawabanDownload(int IdUser, int IdPengajuan, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                KeuPtgjwbDownloadModel ret = new KeuPtgjwbDownloadModel();
                using (var conn = commonService.DbConnection())
                {
                    var dtPengajuan = (from a in conn.GetList<TbPengajuan>()
                                       join b in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.Dilaporkan)) on a.IdPengajuan equals b.IdPengajuan
                                       where a.IdPengajuan == IdPengajuan
                                       select new { a.IdPengajuan, a.Nomor, b.Status, b.CreatedDate, a.IdUnitProkegTambahan, a.IdUnitProkegWajib}).FirstOrDefault();

                        
                    if (dtPengajuan == null) { oMessage = "data tidak ada"; return null; }
                    ret = new KeuPtgjwbDownloadModel
                    {
                        Nomor = dtPengajuan.Nomor,
                        Status = JenisStatusPengajuanConstant.Dict[dtPengajuan.Status],
                        Tanggal = dtPengajuan.CreatedDate.ToString("dd-MM-yyyy"),
                        NamaUnit = string.Empty,
                        Kegiatan = string.Empty,
                        Program = string.Empty,
                        TotalRp = 0,
                        TahunAjaran = string.Empty,
                        Det = new List<KeuPtgjwbDetDownloadModel>()
                    };
                    if (dtPengajuan.IdUnitProkegWajib != null)
                    {
                        var dtx = (from a in conn.GetList<TbUnitProkegWajib>()
                                   join b in conn.GetList<TbUnitProkeg>() on a.IdUnitProkeg equals b.IdUnit
                                   join c in conn.GetList<TbUnit>() on b.IdUnit equals c.IdUnit
                                   join d in conn.GetList<TbTahunAjaran>() on b.IdTahunAjaran equals d.IdTahunAjaran
                                   join e in conn.GetList<TbProgramKegiatan>() on a.IdKegiatan equals e.IdKegiatan
                                   join f in conn.GetList<TbProgram>() on e.IdProgram equals f.IdProgram
                                   where a.IdUnitProkegWajib == dtPengajuan.IdUnitProkegWajib
                                   select new { a, b, c, d, e, f }).FirstOrDefault();
                        ret.NamaUnit = dtx.c.Nama;
                        ret.Program = dtx.f.Nama;
                        ret.Kegiatan = dtx.e.Nama + " [WAJIB]";
                        ret.TahunAjaran = dtx.d.Nama;
                    }
                    else
                    {
                        var dtx = (from a in conn.GetList<TbUnitProkegTambahan>()
                                   join b in conn.GetList<TbUnitProkeg>() on a.IdUnitProkeg equals b.IdUnit
                                   join c in conn.GetList<TbUnit>() on b.IdUnit equals c.IdUnit
                                   join d in conn.GetList<TbTahunAjaran>() on b.IdTahunAjaran equals d.IdTahunAjaran
                                   join f in conn.GetList<TbProgram>() on a.IdProgram equals f.IdProgram
                                   where a.IdUnitProkegTambahan == dtPengajuan.IdUnitProkegTambahan
                                   select new { a, b, c, d, f }).FirstOrDefault();
                        ret.NamaUnit = dtx.c.Nama;
                        ret.Program = dtx.f.Nama;
                        ret.Kegiatan = dtx.a.NamaKegiatan + " [TAMBAHAN]";
                        ret.TahunAjaran = dtx.d.Nama;
                    }
                    var dtPengajuanCoas = from a in conn.GetList<TbPtgjwbCoa>()
                                          join b in conn.GetList<TbCoa>() on a.IdCoa equals b.IdCoa
                                          join c in conn.GetList<TbJenisSatuan>() on a.IdJenisSatuan equals c.IdJenisSatuan
                                          join d in conn.GetList<TbJenisVolume>() on a.IdJenisVolume equals d.IdJenisVolume
                                          where a.IdPengajuan == IdPengajuan
                                          select new { a, b, c, d };
                    foreach (var item in dtPengajuanCoas)
                    {
                        KeuPtgjwbDetDownloadModel m = new KeuPtgjwbDetDownloadModel
                        {
                            Kode = item.b.Kode,
                            Nama = item.b.Nama,
                            Catatan = string.IsNullOrEmpty(item.a.Catatan) ? string.Empty : item.a.Catatan,
                            HargaSatuan = item.a.HargaSatuan,
                            IdCoa = item.b.IdCoa,
                            IdParentCoa = item.b.IdParentCoa,
                            JumlahSatuan = item.a.JumlahSatuan,
                            JumlahVolume = item.a.JumlahVolume,
                            Satuan = item.c.Nama,
                            TotalRp = item.a.JumlahSatuan * item.a.JumlahVolume * item.a.HargaSatuan,
                            Volume = item.d.Nama
                        };
                        if(!string.IsNullOrEmpty(item.a.FileBukti))
                            m.UrlFileBukti = string.Format("{0}/{1}", urlPathUpload, item.a.FileBukti);
                        ret.Det.Add(m);
                    }
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPertanggungjawabanDownload", ex);
                return null;
            }

        }
    }
}
