﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Swg.Entities.Kbm;
using Swg.Entities.Keu;
using Swg.Entities.Lmg;
using Swg.Entities.Public;
using Swg.Entities.Ref;
using Swg.Models;
using Swg.Models.Constants;
using Swg.Sch.Shared.Constants;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;

namespace Swg.Sch.Services
{
    public class KeuPengajuanService : IKeuPengajuanService
    {
        private readonly string ServiceName = "Swg.Sch.Services.KeuPengajuanService.";
        private readonly string _languageCode = "id";
        KeuHakAksesModel hakAkses = new KeuHakAksesModel
        {
            CanAdd = false,
            CanApprove = false,
            CanDelete = false,
            CanDownload = false,
            CanEdit = false,
            CanRead = false,
            CanRelease = false
        };
        private int[] HakAksesPengajuans = new int[] { 31, 32, 33, 34, 35, 36 };
        private readonly ICommonService commonService;
        private readonly IDateTimeService dateTimeService;
        public KeuPengajuanService(
            ICommonService CommonService,
            IDateTimeService DateTimeService)
        {
            commonService = CommonService;
            dateTimeService = DateTimeService;
        }
        private void CekAksesPengajuan(IDbConnection conn, int IdUser)
        {
            var tbAksesRkas = from a in conn.GetList<TbJenisHakAksesKeu>()
                              join b in conn.GetList<TbJenisJabatanHakAksesKeu>() on a.IdJenisHakAksesKeu equals b.IdJenisHakAksesKeu
                              join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                              join d in conn.GetList<TbPegawai>() on c.IdJenisJabatan equals d.IdJenisJabatan
                              where d.IdPegawai == IdUser
                              & HakAksesPengajuans.Contains(a.IdJenisHakAksesKeu)
                              select new { a, b, c };
            foreach (var item in tbAksesRkas)
            {
                if (item.a.NamaSingkat == "CanRead")
                {
                    hakAkses.CanRead = true;
                    hakAkses.CanDownload = hakAkses.CanRead;
                }
                if (item.a.NamaSingkat == "CanAdd")
                {
                    hakAkses.CanAdd = true;
                }
                if (item.a.NamaSingkat == "CanEdit")
                {
                    hakAkses.CanEdit = true;
                }
                if (item.a.NamaSingkat == "CanApprove")
                {
                    hakAkses.CanApprove = true;
                }
                if (item.a.NamaSingkat == "CanDelete")
                {
                    hakAkses.CanDelete = true;
                }
                if (item.a.NamaSingkat == "CanRelease")
                {
                    hakAkses.CanRelease = true;
                }
            }
        }
        private int GetNextStatus(int Status)
        {
            int nextStatus = Status;
            if (Status == JenisStatusPengajuanConstant.DisetujuiKaUnit)
            {
                nextStatus = JenisStatusPengajuanConstant.DisetujuiManUnit;
            }
            else if (Status == JenisStatusPengajuanConstant.DisetujuiManUnit)
            {
                nextStatus = JenisStatusPengajuanConstant.DisetujuiManKeu;
            }
            else if (Status == JenisStatusPengajuanConstant.DisetujuiDirektur)
            {
                nextStatus = JenisStatusPengajuanConstant.Direalisasikan;
            }
            else if (Status == JenisStatusPengajuanConstant.Direalisasikan)
            {
                nextStatus = JenisStatusPengajuanConstant.Dilaporkan;
            }
            else if (Status == JenisStatusPengajuanConstant.Dilaporkan)
            {
                nextStatus = JenisStatusPengajuanConstant.Dibukukan;
            }
            return nextStatus;
        }
        private int GetBeforeStatus(int Status)
        {
            int beforeStatus = Status;
            if (Status == JenisStatusPengajuanConstant.DisetujuiKaUnit)
            {
                beforeStatus = JenisStatusPengajuanConstant.Baru;
            }
            else if (Status == JenisStatusPengajuanConstant.DisetujuiManUnit)
            {
                beforeStatus = JenisStatusPengajuanConstant.DisetujuiKaUnit;
            }
            else if (Status == JenisStatusPengajuanConstant.DisetujuiDirektur)
            {
                beforeStatus = JenisStatusPengajuanConstant.DisetujuiManKeu;
            }
            else if (Status == JenisStatusPengajuanConstant.Direalisasikan)
            {
                beforeStatus = JenisStatusPengajuanConstant.DisetujuiDirektur;
            }
            else if (Status == JenisStatusPengajuanConstant.Dilaporkan)
            {
                beforeStatus = JenisStatusPengajuanConstant.Direalisasikan;
            }
            else if (Status == JenisStatusPengajuanConstant.Dibukukan)
            {
                beforeStatus = JenisStatusPengajuanConstant.Dilaporkan;
            }
            return beforeStatus;
        }

        private string ApproveData(int IdUser, int IdPengajuan, double TotalRpRealisasi, int StatusProses, string Catatan, int Status)
        {
            try
            {

                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        CekAksesPengajuan(conn, IdUser);
                        var tbPegawaiUnit = (from a in conn.GetList<TbUnitPegawai>()
                                             join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                             join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                             where a.IdPegawai == IdUser
                                             select new { a, b, c }).FirstOrDefault();
                        if (tbPegawaiUnit == null)
                            return "Pengguna bukan pegawai";
                        if (hakAkses.CanApprove)
                            if (tbPegawaiUnit.b.IdJenisJabatan != JenisJabatanPegawaiContant.AdmDir)
                            {
                                if (Status == JenisStatusPengajuanConstant.DisetujuiKaUnit)
                                    if (tbPegawaiUnit.b.IdJenisJabatan != JenisJabatanPegawaiContant.KaUnit)
                                        return "tidak berhak approve data";
                                if (Status == JenisStatusPengajuanConstant.DisetujuiManUnit)
                                {
                                    int[] manUnits = new int[] {
                                        JenisJabatanPegawaiContant.ManDik,
                                        JenisJabatanPegawaiContant.ManHum,
                                        JenisJabatanPegawaiContant.ManKeu,
                                        JenisJabatanPegawaiContant.ManSos,
                                        JenisJabatanPegawaiContant.ManSos };
                                    if (!manUnits.Contains(tbPegawaiUnit.b.IdJenisJabatan))
                                        return "tidak berhak approve data";
                                }
                                if (Status == JenisStatusPengajuanConstant.DisetujuiManKeu)
                                    if (tbPegawaiUnit.b.IdJenisJabatan != JenisJabatanPegawaiContant.ManKeu)
                                        return "tidak berhak approve data";
                                if (Status == JenisStatusPengajuanConstant.DisetujuiDirektur)
                                    if (tbPegawaiUnit.b.IdJenisJabatan != JenisJabatanPegawaiContant.Dir)
                                        return "tidak berhak approve data";
                            }
                        var tbPengajuan = conn.Get<TbPengajuan>(IdPengajuan);
                        if (tbPengajuan == null) return "data tidak ada";
                        if (StatusProses == StatusProsesKeuanganConstant.Rejected)
                        {
                            if (string.IsNullOrEmpty(Catatan)) return "catatan harus diisi";
                            int beforeStatus = GetBeforeStatus(Status);
                            var tbPengajuanBeforeStatus = (from a in conn.GetList<TbPengajuanStatus>()
                                                           where a.IdPengajuan == IdPengajuan
                                                           & a.Status == beforeStatus
                                                           select a).FirstOrDefault();
                            if (tbPengajuanBeforeStatus == null)
                                return string.Format("Pengajuan {0} tidak ada", JenisStatusPengajuanConstant.Dict[beforeStatus]);
                            tbPengajuanBeforeStatus.UpdatedBy = IdUser;
                            tbPengajuanBeforeStatus.UpdatedDate = dateTimeService.GetCurrdate();
                            tbPengajuanBeforeStatus.StatusProses = StatusProses;
                            tbPengajuanBeforeStatus.Catatan = Catatan;
                            conn.Update(tbPengajuanBeforeStatus);
                        }
                        else
                        {
                            tbPengajuan.Status = Status;
                            if (TotalRpRealisasi == 0)
                                TotalRpRealisasi = tbPengajuan.TotalRp;

                            tbPengajuan.TotalRpRealisasi = TotalRpRealisasi;
                            conn.Update(tbPengajuan);
                            int nextStatus = GetNextStatus(Status);

                            var tbPengajuanNextStatus = (from a in conn.GetList<TbPengajuanStatus>()
                                                         join b in conn.GetList<TbUser>() on a.CreatedBy equals b.IdUser
                                                         where a.IdPengajuan == IdPengajuan
                                                         & a.Status == nextStatus
                                                         select new { a, b }).FirstOrDefault();
                            if (tbPengajuanNextStatus != null)
                                return string.Format("Pengajuan sudah {0} oleh {1} pada tanggal {2}", JenisStatusPengajuanConstant.Dict[nextStatus], tbPengajuanNextStatus.b.FirstName, dateTimeService.DateToLongString(_languageCode, tbPengajuanNextStatus.a.CreatedDate));
                            var tbPengajuanStatus = (from a in conn.GetList<TbPengajuanStatus>()
                                                     where a.IdPengajuan == IdPengajuan
                                                     & a.Status == Status
                                                     select a).FirstOrDefault();
                            if (tbPengajuanStatus == null)
                            {
                                tbPengajuanStatus = new TbPengajuanStatus
                                {
                                    IdPengajuan = IdPengajuan,
                                    Status = Status,
                                    Catatan = Catatan,
                                    CreatedBy = IdUser,
                                    CreatedDate = dateTimeService.GetCurrdate(),
                                    StatusProses = StatusProses

                                };
                                conn.Insert(tbPengajuanStatus);
                            }
                            else
                            {
                                tbPengajuanStatus.StatusProses = StatusProses;
                                tbPengajuanStatus.UpdatedBy = IdUser;
                                tbPengajuanStatus.UpdatedDate = dateTimeService.GetCurrdate();
                                conn.Update(tbPengajuanStatus);
                            }
                        }

                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "ApproveData", ex);
            }

        }
        private List<KeuPengajuanListModel> GetPengajuanWajibs(IDbConnection conn, int IdUser, int IdTahunAjaran, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<KeuPengajuanListModel> ret = new List<KeuPengajuanListModel>();

                var tbPegawaiUnit = (from a in conn.GetList<TbUnitPegawai>()
                                     join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                     join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                     where a.IdPegawai == IdUser
                                     select new { a, b, c }).FirstOrDefault();
                if (tbPegawaiUnit == null) { oMessage = "Pengguna bukan pegawai"; return null; };
                bool isAdmin = false;
                if (tbPegawaiUnit.a.IdUnit == UnitConstant.DIR ||
                    tbPegawaiUnit.a.IdUnit == UnitConstant.FNC ||
                    tbPegawaiUnit.a.IdUnit == UnitConstant.AKT)
                    isAdmin = true;

                var dtWajibs = from dt in conn.GetList<TbPengajuan>()
                               join b in conn.GetList<TbUnitProkegWajib>() on dt.IdUnitProkegWajib equals b.IdUnitProkegWajib
                               join c in conn.GetList<TbUnitProkeg>(string.Format("where status = {0}", JenisStatusProgKegConstant.Disahkan)) on b.IdUnitProkeg equals c.IdUnitProkeg
                               join keg in conn.GetList<TbProgramKegiatan>() on b.IdKegiatan equals keg.IdKegiatan
                               join prog in conn.GetList<TbProgram>() on keg.IdProgram equals prog.IdProgram
                               join dt_baru in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.Baru)) on dt.IdPengajuan equals dt_baru.IdPengajuan into dt_barua
                               join unit in conn.GetList<TbUnit>() on c.IdUnit equals unit.IdUnit
                               join thAjaran in conn.GetList<TbTahunAjaran>() on c.IdTahunAjaran equals thAjaran.IdTahunAjaran
                               from dt_baru in dt_barua.DefaultIfEmpty()
                               join dt_kaunit in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.DisetujuiKaUnit)) on dt.IdPengajuan equals dt_kaunit.IdPengajuan into dt_kaunita
                               from dt_kaunit in dt_kaunita.DefaultIfEmpty()
                               join dt_mgunit in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.DisetujuiManUnit)) on dt.IdPengajuan equals dt_mgunit.IdPengajuan into dt_mgunita
                               from dt_mgunit in dt_mgunita.DefaultIfEmpty()
                               join dt_mgkeu in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.DisetujuiManKeu)) on dt.IdPengajuan equals dt_mgkeu.IdPengajuan into dt_mgkeua
                               from dt_mgkeu in dt_mgkeua.DefaultIfEmpty()
                               join dt_dir in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.DisetujuiDirektur)) on dt.IdPengajuan equals dt_dir.IdPengajuan into dt_dira
                               from dt_dir in dt_mgkeua.DefaultIfEmpty()
                               join dt_rel in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.Direalisasikan)) on dt.IdPengajuan equals dt_rel.IdPengajuan into dt_rela
                               from dt_rel in dt_rela.DefaultIfEmpty()
                               where c.IdUnit == (isAdmin ? c.IdUnit : tbPegawaiUnit.a.IdUnit)
                               & c.IdTahunAjaran == IdTahunAjaran
                               select new { dt, prog, b, keg, unit, thAjaran, dt_baru, dt_kaunit, dt_mgunit, dt_mgkeu, dt_dir, dt_rel };
                if (dtWajibs == null || dtWajibs.Count() == 0) { oMessage = "data tidak ada"; return null; }

                foreach (var item in dtWajibs)
                {
                    CekAksesPengajuan(conn, IdUser);
                    if (item.unit.IdUnit != tbPegawaiUnit.a.IdUnit)
                    {
                        hakAkses = new KeuHakAksesModel
                        {
                            CanAdd = false,
                            CanApprove = false,
                            CanDelete = false,
                            CanDownload = false,
                            CanEdit = false,
                            CanRead = false,
                            CanRelease = false
                        };

                        if (
                            tbPegawaiUnit.a.IdUnit == UnitConstant.AKT ||
                            tbPegawaiUnit.a.IdUnit == UnitConstant.FNC ||
                            tbPegawaiUnit.a.IdUnit == UnitConstant.DIR
                            )
                        {
                            CekAksesPengajuan(conn, IdUser);
                        }
                    }
                    KeuPengajuanListModel m = new KeuPengajuanListModel
                    {
                        IdPengajuan = item.dt.IdPengajuan,
                        Nomor = item.dt.Nomor,
                        NamaUnit = item.unit.Nama,
                        Status = JenisStatusPengajuanConstant.Dict[item.dt.Status],
                        StatusProses = string.Empty,
                        TahunAjaran = item.thAjaran.Nama,
                        TotalRp = item.dt.TotalRp,
                        TotalRealisasiRp = item.dt.TotalRpRealisasi,
                        Kegiatan = item.keg.Nama,
                        Program = item.prog.Nama,
                        TanggalButuh = item.dt.TanggalButuh.ToString("dd-MM-yyyy"),
                        Aksis = new List<string>()
                    };

                    if (item.dt.Status == JenisStatusPengajuanConstant.Baru)
                    {
                        if (item.dt_baru != null)
                        {
                            m.StatusProses = StatusProsesKeuanganConstant.Dict[item.dt_baru.StatusProses];
                        }
                    }
                    else if (item.dt.Status == JenisStatusPengajuanConstant.DisetujuiKaUnit)
                    {
                        if (item.dt_kaunit != null)
                        {
                            m.StatusProses = StatusProsesKeuanganConstant.Dict[item.dt_kaunit.StatusProses];
                            if (item.dt_kaunit.StatusProses == StatusProsesKeuanganConstant.Rejected)
                                m.StatusProses += " Manager Unit";
                        }
                    }
                    else if (item.dt.Status == JenisStatusPengajuanConstant.DisetujuiManUnit)
                    {
                        if (item.dt_mgunit != null)
                        {
                            m.StatusProses = StatusProsesKeuanganConstant.Dict[item.dt_mgunit.StatusProses];
                            if (item.dt_mgunit.StatusProses == StatusProsesKeuanganConstant.Rejected)
                                m.StatusProses += " Manager Keuangan";
                        }
                    }
                    else if (item.dt.Status == JenisStatusPengajuanConstant.DisetujuiManKeu)
                    {
                        if (item.dt_mgkeu != null)
                        {
                            m.StatusProses = StatusProsesKeuanganConstant.Dict[item.dt_mgkeu.StatusProses];
                            if (item.dt_mgkeu.StatusProses == StatusProsesKeuanganConstant.Rejected)
                                m.StatusProses += " Direktur";
                        }
                    }

                    if (hakAkses.CanRead) m.Aksis.Add("View");
                    if (hakAkses.CanDownload) m.Aksis.Add("Download");
                    if (item.dt.Status == JenisStatusPengajuanConstant.Baru)
                    {
                        if (hakAkses.CanEdit) m.Aksis.Add("Edit");
                        if (hakAkses.CanDelete) m.Aksis.Add("Hapus");
                        if (tbPegawaiUnit.c.Level == LevelJabatanPegawaiConstant.KepalaUnit
                            || tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                            if (hakAkses.CanApprove) m.Aksis.Add("Approve Ka Unit");
                    }
                    else if (item.dt.Status == JenisStatusPengajuanConstant.DisetujuiKaUnit)
                    {
                        if (hakAkses.CanEdit) m.Aksis.Add("Edit");
                        if (hakAkses.CanDelete) m.Aksis.Add("Hapus");
                        if (tbPegawaiUnit.c.Level == LevelJabatanPegawaiConstant.Manager
                            || tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                            if (hakAkses.CanApprove) m.Aksis.Add("Approve Mgr Unit");
                    }
                    else if (item.dt.Status == JenisStatusPengajuanConstant.DisetujuiManUnit)
                    {
                        if (tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.ManKeu
                            || tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                            if (hakAkses.CanApprove) m.Aksis.Add("Approve Mgr Keu");
                    }
                    else if (item.dt.Status == JenisStatusPengajuanConstant.DisetujuiManKeu)
                    {
                        if (tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.Dir
                            || tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                            if (hakAkses.CanApprove) m.Aksis.Add("Approve Direktur");
                    }
                    else if (item.dt.Status == JenisStatusPengajuanConstant.DisetujuiDirektur)
                    {
                        if (tbPegawaiUnit.a.IdUnit == UnitConstant.AKT ||
                            tbPegawaiUnit.a.IdUnit == UnitConstant.FNC ||
                            tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                            if (hakAkses.CanRelease) m.Aksis.Add("Realisasikan");
                    }

                    if (hakAkses.CanRead)
                        ret.Add(m);
                }
                if (ret.Count() == 0) { oMessage = "data tidak ada"; return null; }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPengajuans", ex);
                return null;
            }

        }
        private List<KeuPengajuanListModel> GetPengajuanTambahans(IDbConnection conn, int IdUser, int IdTahunAjaran, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<KeuPengajuanListModel> ret = new List<KeuPengajuanListModel>();

                var tbPegawaiUnit = (from a in conn.GetList<TbUnitPegawai>()
                                     join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                     join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                     where a.IdPegawai == IdUser
                                     select new { a, b, c }).FirstOrDefault();
                if (tbPegawaiUnit == null) { oMessage = "Pengguna bukan pegawai"; return null; };
                bool isAdmin = false;
                if (tbPegawaiUnit.a.IdUnit == UnitConstant.DIR ||
                    tbPegawaiUnit.a.IdUnit == UnitConstant.FNC ||
                    tbPegawaiUnit.a.IdUnit == UnitConstant.AKT)
                    isAdmin = true;

                var dtWajibs = from dt in conn.GetList<TbPengajuan>()
                               join keg in conn.GetList<TbUnitProkegTambahan>() on dt.IdUnitProkegTambahan equals keg.IdUnitProkegTambahan
                               join c in conn.GetList<TbUnitProkeg>(string.Format("where status = {0}", JenisStatusProgKegConstant.Disahkan)) on keg.IdUnitProkeg equals c.IdUnitProkeg
                               join prog in conn.GetList<TbProgram>() on keg.IdProgram equals prog.IdProgram
                               join dt_baru in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.Baru)) on dt.IdPengajuan equals dt_baru.IdPengajuan into dt_barua
                               join unit in conn.GetList<TbUnit>() on c.IdUnit equals unit.IdUnit
                               join thAjaran in conn.GetList<TbTahunAjaran>() on c.IdTahunAjaran equals thAjaran.IdTahunAjaran
                               from dt_baru in dt_barua.DefaultIfEmpty()
                               join dt_kaunit in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.DisetujuiKaUnit)) on dt.IdPengajuan equals dt_kaunit.IdPengajuan into dt_kaunita
                               from dt_kaunit in dt_kaunita.DefaultIfEmpty()
                               join dt_mgunit in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.DisetujuiManUnit)) on dt.IdPengajuan equals dt_mgunit.IdPengajuan into dt_mgunita
                               from dt_mgunit in dt_mgunita.DefaultIfEmpty()
                               join dt_mgkeu in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.DisetujuiManKeu)) on dt.IdPengajuan equals dt_mgkeu.IdPengajuan into dt_mgkeua
                               from dt_mgkeu in dt_mgkeua.DefaultIfEmpty()
                               join dt_dir in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.DisetujuiDirektur)) on dt.IdPengajuan equals dt_dir.IdPengajuan into dt_dira
                               from dt_dir in dt_mgkeua.DefaultIfEmpty()
                               join dt_rel in conn.GetList<TbPengajuanStatus>(string.Format("where status = {0}", JenisStatusPengajuanConstant.Direalisasikan)) on dt.IdPengajuan equals dt_rel.IdPengajuan into dt_rela
                               from dt_rel in dt_rela.DefaultIfEmpty()
                               where c.IdUnit == (isAdmin ? c.IdUnit : tbPegawaiUnit.a.IdUnit)
                               & c.IdTahunAjaran == IdTahunAjaran
                               select new { dt, prog, keg, unit, thAjaran, dt_baru, dt_kaunit, dt_mgunit, dt_mgkeu, dt_dir, dt_rel };
                if (dtWajibs == null || dtWajibs.Count() == 0) { oMessage = "data tidak ada"; return null; }

                foreach (var item in dtWajibs)
                {
                    CekAksesPengajuan(conn, IdUser);
                    if (item.unit.IdUnit != tbPegawaiUnit.a.IdUnit)
                    {
                        hakAkses = new KeuHakAksesModel
                        {
                            CanAdd = false,
                            CanApprove = false,
                            CanDelete = false,
                            CanDownload = false,
                            CanEdit = false,
                            CanRead = false,
                            CanRelease = false
                        };

                        if (
                            tbPegawaiUnit.a.IdUnit == UnitConstant.AKT ||
                            tbPegawaiUnit.a.IdUnit == UnitConstant.FNC ||
                            tbPegawaiUnit.a.IdUnit == UnitConstant.DIR
                            )
                        {
                            CekAksesPengajuan(conn, IdUser);
                        }
                    }
                    KeuPengajuanListModel m = new KeuPengajuanListModel
                    {
                        IdPengajuan = item.dt.IdPengajuan,
                        Nomor = item.dt.Nomor,
                        NamaUnit = item.unit.Nama,
                        Status = JenisStatusPengajuanConstant.Dict[item.dt.Status],
                        StatusProses = string.Empty,
                        TahunAjaran = item.thAjaran.Nama,
                        TotalRp = item.dt.TotalRp,
                        TotalRealisasiRp = item.dt.TotalRpRealisasi,
                        Kegiatan = item.keg.NamaKegiatan,
                        Program = item.prog.Nama,
                        Aksis = new List<string>()
                    };

                    if (item.dt.Status == JenisStatusPengajuanConstant.Baru)
                    {
                        if (item.dt_baru != null)
                        {
                            m.StatusProses = StatusProsesKeuanganConstant.Dict[item.dt_baru.StatusProses];
                        }
                    }
                    else if (item.dt.Status == JenisStatusPengajuanConstant.DisetujuiKaUnit)
                    {
                        if (item.dt_kaunit != null)
                        {
                            m.StatusProses = StatusProsesKeuanganConstant.Dict[item.dt_kaunit.StatusProses];
                            if (item.dt_kaunit.StatusProses == StatusProsesKeuanganConstant.Rejected)
                                m.StatusProses += " Manager Unit";
                        }
                    }
                    else if (item.dt.Status == JenisStatusPengajuanConstant.DisetujuiManUnit)
                    {
                        if (item.dt_mgunit != null)
                        {
                            m.StatusProses = StatusProsesKeuanganConstant.Dict[item.dt_mgunit.StatusProses];
                            if (item.dt_mgunit.StatusProses == StatusProsesKeuanganConstant.Rejected)
                                m.StatusProses += " Manager Keuangan";
                        }
                    }
                    else if (item.dt.Status == JenisStatusPengajuanConstant.DisetujuiManKeu)
                    {
                        if (item.dt_mgkeu != null)
                        {
                            m.StatusProses = StatusProsesKeuanganConstant.Dict[item.dt_mgkeu.StatusProses];
                            if (item.dt_mgkeu.StatusProses == StatusProsesKeuanganConstant.Rejected)
                                m.StatusProses += " Direktur";
                        }
                    }

                    if (hakAkses.CanRead) m.Aksis.Add("View");
                    if (hakAkses.CanDownload) m.Aksis.Add("Download");
                    if (item.dt.Status == JenisStatusPengajuanConstant.Baru)
                    {
                        if (hakAkses.CanEdit) m.Aksis.Add("Edit");
                        if (hakAkses.CanDelete) m.Aksis.Add("Hapus");
                        if (tbPegawaiUnit.c.Level == LevelJabatanPegawaiConstant.KepalaUnit
                            || tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                            if (hakAkses.CanApprove) m.Aksis.Add("Approve Ka Unit");
                    }
                    else if (item.dt.Status == JenisStatusPengajuanConstant.DisetujuiKaUnit)
                    {
                        if (hakAkses.CanEdit) m.Aksis.Add("Edit");
                        if (hakAkses.CanDelete) m.Aksis.Add("Hapus");
                        if (tbPegawaiUnit.c.Level == LevelJabatanPegawaiConstant.Manager
                            || tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                            if (hakAkses.CanApprove) m.Aksis.Add("Approve Mgr Unit");
                    }
                    else if (item.dt.Status == JenisStatusPengajuanConstant.DisetujuiManUnit)
                    {
                        if (tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.ManKeu
                            || tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                            if (hakAkses.CanApprove) m.Aksis.Add("Approve Mgr Keu");
                    }
                    else if (item.dt.Status == JenisStatusPengajuanConstant.DisetujuiManKeu)
                    {
                        if (tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.Dir
                            || tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                            if (hakAkses.CanApprove) m.Aksis.Add("Approve Direktur");
                    }
                    else if (item.dt.Status == JenisStatusPengajuanConstant.DisetujuiDirektur)
                    {
                        if (tbPegawaiUnit.a.IdUnit == UnitConstant.AKT ||
                            tbPegawaiUnit.a.IdUnit == UnitConstant.FNC ||
                            tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                            if (hakAkses.CanRelease) m.Aksis.Add("Realisasikan");
                    }

                    if (hakAkses.CanRead)
                        ret.Add(m);
                }
                if (ret.Count() == 0) { oMessage = "data tidak ada"; return null; }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPengajuans", ex);
                return null;
            }

        }
        public List<ComboModel> GetCoas(out string oMessage)
        {
            oMessage = string.Empty;
            List<ComboModel> ret = new List<ComboModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var dt = from a in conn.GetList<TbCoa>()
                             where a.IdJenisAkun == JenisAkunConstant.Pratama
                             & a.Kode.Substring(0, 1) == "5"
                             select a;
                    if (dt == null || dt.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    foreach (var item in dt)
                    {
                        ret.Add(new ComboModel
                        {
                            Id = item.IdCoa,
                            Kode = item.Kode,
                            Nama = item.Nama
                        });
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetCoas", ex);
                return null;

                throw;
            }

        }

        public List<KeuPengajuanListModel> GetPengajuans(int IdUser, int IdTahunAjaran, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<KeuPengajuanListModel> ret = new List<KeuPengajuanListModel>();
                using (var conn = commonService.DbConnection())
                {
                    var retWajibs = GetPengajuanWajibs(conn, IdUser, IdTahunAjaran, out oMessage);
                    if (string.IsNullOrEmpty(oMessage))
                        ret = retWajibs;
                    var retTambahans = GetPengajuanTambahans(conn, IdUser, IdTahunAjaran, out oMessage);
                    if (string.IsNullOrEmpty(oMessage))
                        ret.AddRange(retTambahans);
                    if (ret.Count() == 0)
                        return null;
                    oMessage = string.Empty;
                    return ret;

                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPengajuans", ex);
                return null;
            }
        }
        public List<ComboModel> GetPrograms(int IdUser, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<ComboModel> ret = new List<ComboModel>();
                using (var conn = commonService.DbConnection())
                {
                    var tbPegawaiUnit = (from a in conn.GetList<TbUnitPegawai>()
                                         join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                         join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                         where a.IdPegawai == IdUser
                                         select new { a, b, c }).FirstOrDefault();
                    var dataPrograms = from a in conn.GetList<TbProgramUnit>()
                                       join b in conn.GetList<TbProgram>() on a.IdProgram equals b.IdProgram
                                       where a.IdUnit == tbPegawaiUnit.a.IdUnit
                                       select b;
                    foreach (var item in dataPrograms)
                    {
                        ComboModel m = new ComboModel
                        {
                            Id = item.IdProgram,
                            Kode = item.Kode,
                            Nama = item.Nama,
                            NamaSingkat = item.Nama
                        };
                        ret.Add(m);
                    }
                    if (ret.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPrograms", ex);
                return null;
            }
        }
        public List<ComboModel> GetKegiatanWajibs(int IdUser, int IdTahunAjaran, int IdProgram, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<ComboModel> ret = new List<ComboModel>();
                using (var conn = commonService.DbConnection())
                {
                    var tbPegawaiUnit = (from a in conn.GetList<TbUnitPegawai>()
                                         join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                         join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                         where a.IdPegawai == IdUser
                                         select new { a, b, c }).FirstOrDefault();

                    var dt = from a in conn.GetList<TbProgramKegiatan>()
                             join b in conn.GetList<TbUnitProkegWajib>() on a.IdKegiatan equals b.IdKegiatan
                             join c in conn.GetList<TbUnitProkeg>(string.Format("where status = {0}", JenisStatusProgKegConstant.Disahkan)) on b.IdUnitProkeg equals c.IdUnitProkeg
                             where c.IdTahunAjaran == IdTahunAjaran
                             & a.IdProgram == IdProgram
                             & c.IdUnit == tbPegawaiUnit.a.IdUnit
                             select new { a, b, c };
                    foreach (var item in dt)
                    {
                        ComboModel m = new ComboModel
                        {
                            Id = item.b.IdUnitProkegWajib,
                            Kode = item.a.Kode,
                            Nama = item.a.Nama,
                            NamaSingkat = "WAJIB"
                        };
                        ret.Add(m);
                    }
                }
                if (ret.Count() == 0) { oMessage = "data tidak ada"; return null; }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKegiatanWajibs", ex);
                return null;
            }
        }
        public List<ComboModel> GetKegiatanTambahans(int IdUser, int IdTahunAjaran, int IdProgram, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<ComboModel> ret = new List<ComboModel>();
                using (var conn = commonService.DbConnection())
                {
                    var tbPegawaiUnit = (from a in conn.GetList<TbUnitPegawai>()
                                         join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                         join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                         where a.IdPegawai == IdUser
                                         select new { a, b, c }).FirstOrDefault();

                    var dt = from b in conn.GetList<TbUnitProkegTambahan>()
                             join c in conn.GetList<TbUnitProkeg>(string.Format("where status = {0}", JenisStatusProgKegConstant.Disahkan)) on b.IdUnitProkeg equals c.IdUnitProkeg
                             where c.IdTahunAjaran == IdTahunAjaran
                             & b.IdProgram == IdProgram
                             & c.IdUnit == tbPegawaiUnit.a.IdUnit
                             select new { b, c };

                    foreach (var item in dt)
                    {
                        ComboModel m = new ComboModel
                        {
                            Id = item.b.IdUnitProkegTambahan,
                            Kode = item.b.IdUnitProkegTambahan.ToString(),
                            Nama = item.b.NamaKegiatan,
                            NamaSingkat = "TAMBAHAN"
                        };
                        ret.Add(m);
                    }
                }
                if (ret.Count() == 0) { oMessage = "data tidak ada"; return null; }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKegiatanTambahans", ex);
                return null;
            }
        }
        public string SetCoaPengajuanWajib(int IdUser, int IdUnitProkegWajib, string Nomor, string Tanggal)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var dt = from a in conn.GetList<TbUnitProkegWajibCoa>()
                                 where a.IdUnitProkegWajib == IdUnitProkegWajib
                                 select a;
                        if (dt == null || dt.Count() == 0) return "data akun program kegiatan tidak ada";

                        TbPengajuan tbPengajuan = new TbPengajuan
                        {
                            IdUnitProkegWajib = IdUnitProkegWajib,
                            Nomor = Nomor,
                            TanggalButuh = dateTimeService.StrToDateTime(Tanggal),
                            Status = JenisStatusPengajuanConstant.Baru,
                            TotalRp = 0,
                            TotalRpRealisasi = 0,
                            TotalRpDilaporkan = 0
                        };
                        var idPengajuan = conn.Insert(tbPengajuan);
                        TbPengajuanStatus tbPengajuanStatus = new TbPengajuanStatus
                        {
                            IdPengajuan = (int)idPengajuan,
                            Status = JenisStatusPengajuanConstant.Baru,
                            CreatedBy = IdUser,
                            CreatedDate = dateTimeService.GetCurrdate(),
                            StatusProses = StatusProsesKeuanganConstant.New
                        };
                        conn.Insert(tbPengajuanStatus);
                        double totalRp = 0;
                        foreach (var item in dt)
                        {
                            TbPengajuanCoa tbPengajuanCoa = new TbPengajuanCoa
                            {
                                IdPengajuan = (int)idPengajuan,
                                IdCoa = item.IdCoa,
                                HargaSatuan = item.HargaSatuan,
                                IdJenisSatuan = item.IdJenisSatuan,
                                IdJenisVolume = item.IdJenisVolume,
                                JumlahSatuan = item.JumlahSatuan,
                                JumlahVolume = item.JumlahVolume
                            };
                            totalRp += (item.JumlahSatuan * item.JumlahVolume * item.HargaSatuan);
                            conn.Insert(tbPengajuanCoa);
                        }
                        tbPengajuan = conn.Get<TbPengajuan>((int)idPengajuan);
                        tbPengajuan.TotalRp = totalRp;
                        conn.Update(tbPengajuan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetCoaPengajuanWajib", ex);
            }
        }
        public string SetCoaPengajuanTambahan(int IdUser, int IdUnitProkegTambahan, string Nomor, string Tanggal)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var dt = from a in conn.GetList<TbUnitProkegTambahanCoa>()
                                 where a.IdUnitProkegTambahan == IdUnitProkegTambahan
                                 select a;
                        if (dt == null || dt.Count() == 0) return "data akun program kegiatan tidak ada";

                        TbPengajuan tbPengajuan = new TbPengajuan
                        {
                            IdUnitProkegTambahan = IdUnitProkegTambahan,
                            Nomor = Nomor,
                            TanggalButuh = dateTimeService.StrToDateTime(Tanggal),
                            Status = JenisStatusPengajuanConstant.Baru,
                            TotalRp = 0,
                            TotalRpRealisasi = 0,
                            TotalRpDilaporkan = 0
                        };
                        var idPengajuan = conn.Insert(tbPengajuan);
                        TbPengajuanStatus tbPengajuanStatus = new TbPengajuanStatus
                        {
                            IdPengajuan = (int)idPengajuan,
                            Status = JenisStatusPengajuanConstant.Baru,
                            CreatedBy = IdUser,
                            CreatedDate = dateTimeService.GetCurrdate(),
                            StatusProses = StatusProsesKeuanganConstant.New
                        };
                        conn.Insert(tbPengajuanStatus);
                        double totalRp = 0;
                        foreach (var item in dt)
                        {
                            TbPengajuanCoa tbPengajuanCoa = new TbPengajuanCoa
                            {
                                IdPengajuan = (int)idPengajuan,
                                IdCoa = item.IdCoa,
                                HargaSatuan = item.HargaSatuan,
                                IdJenisSatuan = item.IdJenisSatuan,
                                IdJenisVolume = item.IdJenisVolume,
                                JumlahSatuan = item.JumlahSatuan,
                                JumlahVolume = item.JumlahVolume
                            };
                            conn.Insert(tbPengajuanCoa);
                            totalRp += (item.JumlahSatuan * item.JumlahVolume * item.HargaSatuan);
                        }
                        tbPengajuan = conn.Get<TbPengajuan>((int)idPengajuan);
                        tbPengajuan.TotalRp = totalRp;
                        conn.Update(tbPengajuan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetCoaPengajuanTambahan", ex);
            }
        }
        public List<KeuPengajuanCoaListModel> GetPengajuanCoas(int IdUser, int IdPengajuan, out string oMessage)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    List<KeuPengajuanCoaListModel> ret = new List<KeuPengajuanCoaListModel>();
                    oMessage = string.Empty;
                    CekAksesPengajuan(conn, IdUser);

                    var data = from a in conn.GetList<TbPengajuanCoa>()
                               join b in conn.GetList<TbCoa>() on a.IdCoa equals b.IdCoa
                               join c in conn.GetList<TbJenisSatuan>() on a.IdJenisSatuan equals c.IdJenisSatuan
                               join d in conn.GetList<TbJenisVolume>() on a.IdJenisVolume equals d.IdJenisVolume
                               where a.IdPengajuan == IdPengajuan
                               select new { a, b, c, d };
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    foreach (var item in data)
                    {
                        KeuPengajuanCoaListModel m = new KeuPengajuanCoaListModel
                        {
                            IdPengajuanCoa = item.a.IdPengajuanCoa,
                            Catatan = item.a.Catatan,
                            HargaSatuan = item.a.HargaSatuan,
                            JumlahSatuan = item.a.JumlahSatuan,
                            JumlahVolume = item.a.JumlahVolume,
                            Satuan = item.c.Nama,
                            Volume = item.d.Nama,
                            Rp = item.a.JumlahSatuan * item.a.JumlahVolume * item.a.HargaSatuan,
                            Aksis = new List<string>(),
                            KodeCoa = item.b.Kode,
                            NamaCoa = item.b.Nama,
                            IdCoa = item.b.IdCoa,
                            IdJenisSatuan = item.c.IdJenisSatuan,
                            IdJenisVolume = item.d.IdJenisVolume
                        };
                        if (hakAkses.CanRead) m.Aksis.Add("View");
                        if (hakAkses.CanEdit) m.Aksis.Add("Edit");
                        if (hakAkses.CanDelete) m.Aksis.Add("Hapus");
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPengajuanCoas", ex);
                return null;
            }

        }
        public string PengajuanDelete(int IdUser, int IdPengajuan)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAksesPengajuan(conn, IdUser);
                    if (!hakAkses.CanDelete) return "tidak berhak hapus pengajuan";
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbPengajuan = conn.Get<TbPengajuan>(IdPengajuan);
                        if (tbPengajuan == null) return "data tidak ada";

                        int[] statuss = new int[] { JenisStatusPengajuanConstant.Baru, JenisStatusPengajuanConstant.DisetujuiKaUnit };
                        if (!statuss.Contains(tbPengajuan.Status))
                            return string.Format("status data sudah {0}", JenisStatusPengajuanConstant.Dict[tbPengajuan.Status]);

                        conn.DeleteList<TbPengajuanCoa>(string.Format("where id_pengajuan = {0}", tbPengajuan.IdPengajuan));
                        conn.DeleteList<TbPengajuanStatus>(string.Format("where id_pengajuan = {0}", tbPengajuan.IdPengajuan));
                        conn.Delete(tbPengajuan);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PengajuanDelete", ex);
            }
        }
        public string PengajuanEdit(int IdUser, int IdPengajuan, string Nomor, string Tanggal)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAksesPengajuan(conn, IdUser);
                    if (!hakAkses.CanEdit) return "tidak berhak edit pengajuan";
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbPengajuan = conn.Get<TbPengajuan>(IdPengajuan);
                        if (tbPengajuan == null) return "data tidak ada";

                        int[] statuss = new int[] { JenisStatusPengajuanConstant.Baru, JenisStatusPengajuanConstant.DisetujuiKaUnit };
                        if (!statuss.Contains(tbPengajuan.Status))
                            return string.Format("status data sudah {0}", JenisStatusPengajuanConstant.Dict[tbPengajuan.Status]);
                        tbPengajuan.Nomor = Nomor;
                        tbPengajuan.TanggalButuh = dateTimeService.StrToDateTime(Tanggal);
                        conn.Update(tbPengajuan);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PengajuanEdit", ex);
            }
        }
        public string PengajuanCoaAdd(int IdUser, KeuPengajuanCoaAddModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAksesPengajuan(conn, IdUser);
                    using (var tx = conn.BeginTransaction())
                    {
                        if (!hakAkses.CanAdd) return "tidak berhak merubah pengajuan";
                        TbCoa tbCoa = conn.Get<TbCoa>(Data.IdCoa);
                        if (tbCoa == null) return "data tidak ada";

                        TbPengajuan tbPengajuan = conn.Get<TbPengajuan>(Data.IdPengajuan);
                        if (tbPengajuan == null) return "data Pengajuan tidak ada";
                        if (tbPengajuan.Status != JenisStatusPengajuanConstant.Baru)
                            return string.Format("status data sudah {0}", JenisStatusPengajuanConstant.Dict[tbPengajuan.Status]);

                        var cekData = (from a in conn.GetList<TbPengajuanCoa>()
                                       join d in conn.GetList<TbCoa>() on a.IdCoa equals d.IdCoa
                                       where a.IdCoa == Data.IdCoa
                                       & a.IdPengajuan == Data.IdPengajuan
                                       select new { a, d }).FirstOrDefault();
                        if (cekData != null)
                            return string.Format("kode akun {0} sudah ada untuk pengajuan ini", cekData.d.Kode);

                        double rpTotal = Data.JumlahSatuan * Data.JumlahVolume * Data.HargaSatuan;


                        tbPengajuan.TotalRp += rpTotal;
                        TbPengajuanCoa tbPengajuanCoa = new TbPengajuanCoa
                        {
                            IdCoa = Data.IdCoa,
                            Catatan = Data.Catatan,
                            HargaSatuan = Data.HargaSatuan,
                            IdJenisSatuan = Data.IdJenisSatuan,
                            IdJenisVolume = Data.IdJenisVolume,
                            IdPengajuan = Data.IdPengajuan,
                            JumlahSatuan = Data.JumlahSatuan,
                            JumlahVolume = Data.JumlahVolume
                        };
                        conn.Insert(tbPengajuanCoa);
                        conn.Update(tbPengajuan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PengajuanCoaAdd", ex);
            }

        }
        public string PengajuanCoaEdit(int IdUser, KeuPengajuanCoaEditModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAksesPengajuan(conn, IdUser);
                    using (var tx = conn.BeginTransaction())
                    {
                        if (!hakAkses.CanAdd) return "tidak berhak merubah pengajuan";
                        TbPengajuanCoa tbPengajuanCoa = conn.Get<TbPengajuanCoa>(Data.IdPengajuanCoa);
                        if (tbPengajuanCoa == null) return "data tidak ada";

                        TbPengajuan tbPengajuan = conn.Get<TbPengajuan>(tbPengajuanCoa.IdPengajuan);
                        if (tbPengajuan == null) return "data Pengajuan tidak ada";
                        if (tbPengajuan.Status != JenisStatusPengajuanConstant.Baru)
                            return string.Format("status data sudah {0}", JenisStatusPengajuanConstant.Dict[tbPengajuan.Status]);
                        double rpTotalOld = tbPengajuanCoa.JumlahSatuan * tbPengajuanCoa.JumlahVolume * tbPengajuanCoa.HargaSatuan;


                        double rpTotal = Data.JumlahSatuan * Data.JumlahVolume * Data.HargaSatuan;
                        tbPengajuan.TotalRp -= rpTotalOld;
                        tbPengajuan.TotalRp += rpTotal;

                        tbPengajuanCoa.Catatan = Data.Catatan;
                        tbPengajuanCoa.HargaSatuan = Data.HargaSatuan;
                        tbPengajuanCoa.IdJenisSatuan = Data.IdJenisSatuan;
                        tbPengajuanCoa.IdJenisVolume = Data.IdJenisVolume;
                        tbPengajuanCoa.JumlahSatuan = Data.JumlahSatuan;
                        tbPengajuanCoa.JumlahVolume = Data.JumlahVolume;

                        conn.Update(tbPengajuanCoa);
                        conn.Update(tbPengajuan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PengajuanCoaEdit", ex);
            }
        }
        public string PengajuanCoaDelete(int IdUser, int IdPengajuanCoa)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAksesPengajuan(conn, IdUser);
                    using (var tx = conn.BeginTransaction())
                    {
                        if (!hakAkses.CanAdd) return "tidak berhak menghapus pengajuan";
                        TbPengajuanCoa tbPengajuanCoa = conn.Get<TbPengajuanCoa>(IdPengajuanCoa);
                        if (tbPengajuanCoa == null) return "data tidak ada";

                        TbPengajuan tbPengajuan = conn.Get<TbPengajuan>(tbPengajuanCoa.IdPengajuan);
                        if (tbPengajuan == null) return "data Pengajuan tidak ada";
                        if (tbPengajuan.Status != JenisStatusPengajuanConstant.Baru)
                            return string.Format("status data sudah {0}", JenisStatusPengajuanConstant.Dict[tbPengajuan.Status]);


                        double rpTotal = tbPengajuanCoa.JumlahSatuan * tbPengajuanCoa.JumlahVolume * tbPengajuanCoa.HargaSatuan;
                        tbPengajuan.TotalRp -= rpTotal;

                        conn.Delete(tbPengajuanCoa);
                        conn.Update(tbPengajuan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PengajuanCoaDelete", ex);
            }
        }

        public string ApproveByKaUnit(int IdUser, int IdPengajuan, int StatusProses, double RpRealisasi, string Catatan)
        {
            return ApproveData(IdUser, IdPengajuan, RpRealisasi, StatusProses, Catatan, JenisStatusPengajuanConstant.DisetujuiKaUnit);

        }
        public string ApproveByManUnit(int IdUser, int IdPengajuan, int StatusProses, double RpRealisasi, string Catatan)
        {
            return ApproveData(IdUser, IdPengajuan, RpRealisasi, StatusProses, Catatan, JenisStatusPengajuanConstant.DisetujuiManUnit);
        }
        public string ApproveByManKeu(int IdUser, int IdPengajuan, int StatusProses, double RpRealisasi, string Catatan)
        {
            return ApproveData(IdUser, IdPengajuan, RpRealisasi, StatusProses, Catatan, JenisStatusPengajuanConstant.DisetujuiManKeu);
        }
        public string ApproveByDirektur(int IdUser, int IdPengajuan, int StatusProses, double RpRealisasi, string Catatan)
        {
            return ApproveData(IdUser, IdPengajuan, RpRealisasi, StatusProses, Catatan, JenisStatusPengajuanConstant.DisetujuiDirektur);
        }
        public string SetRealisasi(int IdUser, int IdPengajuan)
        {
            return ApproveData(IdUser, IdPengajuan, 0, 0, string.Empty, JenisStatusPengajuanConstant.Direalisasikan);
        }
        public KeuPengajuanDownloadModel GetPengajuanDownload(int IdUser, int IdPengajuan, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                KeuPengajuanDownloadModel ret = new KeuPengajuanDownloadModel();
                using (var conn = commonService.DbConnection())
                {
                    var dtPengajuan = conn.Get<TbPengajuan>(IdPengajuan);
                    if (dtPengajuan == null) { oMessage = "data tidak ada"; return null; }
                    ret = new KeuPengajuanDownloadModel
                    {
                        Nomor = dtPengajuan.Nomor,
                        Status = JenisStatusPengajuanConstant.Dict[dtPengajuan.Status],
                        Tanggal = dtPengajuan.TanggalButuh.ToString("dd-MM-yyyy"),
                        NamaUnit = string.Empty,
                        Kegiatan = string.Empty,
                        Program = string.Empty,
                        TotalRp = 0,
                        TahunAjaran = string.Empty,
                        Det = new List<KeuPengajuanDetDownloadModel>()
                    };
                    if (dtPengajuan.IdUnitProkegWajib != null)
                    {
                        var dtx = (from a in conn.GetList<TbUnitProkegWajib>()
                                   join b in conn.GetList<TbUnitProkeg>() on a.IdUnitProkeg equals b.IdUnit
                                   join c in conn.GetList<TbUnit>() on b.IdUnit equals c.IdUnit
                                   join d in conn.GetList<TbTahunAjaran>() on b.IdTahunAjaran equals d.IdTahunAjaran
                                   join e in conn.GetList<TbProgramKegiatan>() on a.IdKegiatan equals e.IdKegiatan
                                   join f in conn.GetList<TbProgram>() on e.IdProgram equals f.IdProgram
                                   where a.IdUnitProkegWajib == dtPengajuan.IdUnitProkegWajib
                                   select new { a, b, c, d, e, f }).FirstOrDefault();
                        ret.NamaUnit = dtx.c.Nama;
                        ret.Program = dtx.f.Nama;
                        ret.Kegiatan = dtx.e.Nama + " [WAJIB]";
                        ret.TahunAjaran = dtx.d.Nama;
                    }
                    else
                    {
                        var dtx = (from a in conn.GetList<TbUnitProkegTambahan>()
                                   join b in conn.GetList<TbUnitProkeg>() on a.IdUnitProkeg equals b.IdUnit
                                   join c in conn.GetList<TbUnit>() on b.IdUnit equals c.IdUnit
                                   join d in conn.GetList<TbTahunAjaran>() on b.IdTahunAjaran equals d.IdTahunAjaran
                                   join f in conn.GetList<TbProgram>() on a.IdProgram equals f.IdProgram
                                   where a.IdUnitProkegTambahan == dtPengajuan.IdUnitProkegTambahan
                                   select new { a, b, c, d, f }).FirstOrDefault();
                        ret.NamaUnit = dtx.c.Nama;
                        ret.Program = dtx.f.Nama;
                        ret.Kegiatan = dtx.a.NamaKegiatan + " [TAMBAHAN]";
                        ret.TahunAjaran = dtx.d.Nama;
                    }
                    var dtPengajuanCoas = from a in conn.GetList<TbPengajuanCoa>()
                                          join b in conn.GetList<TbCoa>() on a.IdCoa equals b.IdCoa
                                          join c in conn.GetList<TbJenisSatuan>() on a.IdJenisSatuan equals c.IdJenisSatuan
                                          join d in conn.GetList<TbJenisVolume>() on a.IdJenisVolume equals d.IdJenisVolume
                                          where a.IdPengajuan == IdPengajuan
                                          select new { a, b, c, d };
                    foreach (var item in dtPengajuanCoas)
                    {
                        KeuPengajuanDetDownloadModel m = new KeuPengajuanDetDownloadModel
                        {
                            Kode = item.b.Kode,
                            Nama = item.b.Nama,
                            Catatan = string.IsNullOrEmpty(item.a.Catatan) ? string.Empty : item.a.Catatan,
                            HargaSatuan = item.a.HargaSatuan,
                            IdCoa = item.b.IdCoa,
                            IdParentCoa = item.b.IdParentCoa,
                            JumlahSatuan = item.a.JumlahSatuan,
                            JumlahVolume = item.a.JumlahVolume,
                            Satuan = item.c.Nama,
                            TotalRp = item.a.JumlahSatuan * item.a.JumlahVolume * item.a.HargaSatuan,
                            Volume = item.d.Nama
                        };
                        ret.Det.Add(m);
                    }
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPengajuanDownload", ex);
                return null;
            }
        }

    }
}
