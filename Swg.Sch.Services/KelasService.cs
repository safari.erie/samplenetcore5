﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Swg.Entities.Kbm;
using Swg.Entities.Lmg;
using Swg.Entities.Public;
using Swg.Entities.Ref;
using Swg.Sch.Shared.Constants;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;

namespace Swg.Sch.Services
{
    public class KelasService : IKelasService
    {
        private readonly string ServiceName = "Swg.Sch.Services.KelasService.";
        private readonly ICommonService commonService;
        private readonly IUserAppService userAppService;
        public KelasService(
            ICommonService CommonService,
            IUserAppService UserAppService)
        {
            commonService = CommonService;
            userAppService = UserAppService;
        }


        public List<KelasModel> GetKelass(int IdUnit, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbKelas>()
                               join b in conn.GetList<TbUnit>() on a.IdUnit equals b.IdUnit
                               where a.IdUnit == IdUnit
                               select a;
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    return (from a in data
                            select new KelasModel { IdKelas = a.IdKelas, Nama = a.Nama, NamaSingkat = a.NamaSingkat, IdUnit = a.IdUnit }).ToList();
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelass", ex);
                return null;
            }
        }
        public KelasModel GetKelas(int IdKelas, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.Get<TbKelas>(IdKelas);
                    if (data == null) { oMessage = "data tidak ada"; return null; }
                    return new KelasModel { IdKelas = data.IdKelas, Nama = data.Nama, NamaSingkat = data.NamaSingkat, IdUnit = data.IdUnit };
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelas", ex);
                return null;
            }
        }
        public string KelasAdd(int IdUser, int IdUnit, string Nama, string NamaSingkat)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    var tbKelass = from a in conn.GetList<TbKelas>()
                                   where a.IdUnit == IdUnit
                                   select a;
                    int idKelas = 1;
                    if (tbKelass != null || tbKelass.Count() > 0)
                        idKelas = tbKelass.Count() + 1;
                    idKelas += IdUnit * 100;
                    using (var tx = conn.BeginTransaction())
                    {
                        TbKelas tbKelas = new TbKelas { IdUnit = IdUnit, IdKelas = idKelas, Nama = Nama, NamaSingkat = NamaSingkat };
                        conn.Insert(tbKelas);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "KelasAdd", ex);
            }
        }
        public string KelasEdit(int IdUser, int IdKelas, string Nama, string NamaSingkat)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbKelas = conn.Get<TbKelas>(IdKelas);
                        if (tbKelas == null) return "data tidak ada";
                        tbKelas.Nama = Nama;
                        tbKelas.NamaSingkat = NamaSingkat;
                        conn.Update(tbKelas);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "KelasAdd", ex);
            }
        }
        public string KelasDelete(int IdUser, int IdKelas)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbKelas = conn.Get<TbKelas>(IdKelas);
                        if (tbKelas == null) return "data tidak ada";
                        conn.Delete(tbKelas);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "KelasAdd", ex);
            }
        }

        public List<KelasParalelModel> GetKelasParalels(int IdKelas, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbKelasParalel>()
                               join b in conn.GetList<TbKelas>() on a.IdKelas equals b.IdKelas
                               where a.IdKelas == IdKelas
                               select new { a, b };
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    return (from item in data
                            select new KelasParalelModel { IdKelasParalel = item.a.IdKelasParalel, Nama = item.a.Nama, NamaSingkat = item.a.NamaSingkat, IdKelas = item.a.IdKelas, IdUnit = item.b.IdUnit }).ToList();
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelasParalels", ex);
                return null;
            }
        }

        public KelasParalelModel GetKelasParalel(int IdKelasParalel, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = (from a in conn.GetList<TbKelasParalel>()
                                join b in conn.GetList<TbKelas>() on a.IdKelas equals b.IdKelas
                                where a.IdKelasParalel == IdKelasParalel
                                select new { a, b }).FirstOrDefault();
                    if (data == null) { oMessage = "data tidak ada"; return null; }
                    return new KelasParalelModel { IdKelasParalel = data.a.IdKelasParalel, Nama = data.a.Nama, NamaSingkat = data.a.NamaSingkat, IdKelas = data.a.IdKelas, IdUnit = data.b.IdUnit };
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelasParalel", ex);
                return null;
            }
        }
        public string KelasParalelAdd(int IdUser, int IdKelas, string Nama, string NamaSingkat)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    var tbKelass = from a in conn.GetList<TbKelasParalel>()
                                   where a.IdKelas == IdKelas
                                   select a;
                    int idKelasParalel = 1;
                    if (tbKelass != null || tbKelass.Count() > 0)
                        idKelasParalel = tbKelass.Count() + 1;
                    idKelasParalel += IdKelas * 100;
                    using (var tx = conn.BeginTransaction())
                    {
                        TbKelasParalel tbKelas = new TbKelasParalel { IdKelasParalel = idKelasParalel, IdKelas = IdKelas, Nama = Nama, NamaSingkat = NamaSingkat };
                        conn.Insert(tbKelas);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "KelasParalelAdd", ex);
            }
        }
        public string KelasParalelEdit(int IdUser, int IdKelasParalel, string Nama, string NamaSingkat)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbKelas = conn.Get<TbKelasParalel>(IdKelasParalel);
                        if (tbKelas == null) return "data tidak ada";
                        tbKelas.Nama = Nama;
                        tbKelas.NamaSingkat = NamaSingkat;
                        conn.Update(tbKelas);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "KelasParalelEdit", ex);
            }
        }
        public string KelasParalelDelete(int IdUser, int IdKelasParalel)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbKelas = conn.Get<TbKelasParalel>(IdKelasParalel);
                        if (tbKelas == null) return "data tidak ada";
                        conn.Delete(tbKelas);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "KelasParalelDelete", ex);
            }
        }
        public List<KelasParalelPegawaiModel> GetKelasParalelPegawais(int IdKelasParalel, out string oMessage)
        {
            try
            {
                List<KelasParalelPegawaiModel> ret = new List<KelasParalelPegawaiModel>();
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = from pegawai in conn.GetList<TbPegawai>()
                               join jabatan in conn.GetList<TbJenisJabatan>() on pegawai.IdJenisJabatan equals jabatan.IdJenisJabatan
                               join userProfile in conn.GetList<TbUser>() on pegawai.IdPegawai equals userProfile.IdUser
                               join unitPegawai in conn.GetList<TbUnitPegawai>() on pegawai.IdPegawai equals unitPegawai.IdPegawai
                               join kelas in conn.GetList<TbKelas>() on unitPegawai.IdUnit equals kelas.IdUnit
                               join kelasParalel in conn.GetList<TbKelasParalel>() on kelas.IdKelas equals kelasParalel.IdKelas
                               where kelasParalel.IdKelasParalel == IdKelasParalel
                               select new { pegawai, jabatan, userProfile, unitPegawai, kelas, kelasParalel };
                    if (data == null & data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    foreach (var item in data)
                    {
                        KelasParalelPegawaiModel m = new KelasParalelPegawaiModel
                        {
                            IdKelasParalel = item.kelasParalel.IdKelasParalel,
                            IdPegawai = item.pegawai.IdPegawai,
                            IdJenisPegawaiKelasParalel = item.jabatan.IdJenisJabatan,
                            JenisPegawaiKelasParalel = item.jabatan.Nama,
                            IdUnit = item.unitPegawai.IdUnit,
                            NamaPegawai = userAppService.SetFullName(item.userProfile.FirstName, item.userProfile.MiddleName, item.userProfile.LastName),
                            Nama = item.kelasParalel.Nama,
                            NamaSingkat = item.kelasParalel.NamaSingkat
                        };
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelasParalelPegawais", ex);
                return null;

            }
        }
        //public string KelasParalelPegawaiAdd(int IdUser, int IdKelasParalel, List<PegawaiJenisModel> Pegawais)
        //{
        //    try
        //    {
        //        using (var conn = commonService.DbConnection())
        //        {
        //            conn.Open();
        //            var unitKelas = (from a in conn.GetList<TbKelasParalel>()
        //                             join b in conn.GetList<TbKelas>() on a.IdKelas equals b.IdKelas
        //                             where a.IdKelasParalel == IdKelasParalel
        //                             select b.IdUnit).FirstOrDefault();
        //            string err = "";
        //            using (var tx = conn.BeginTransaction())
        //            {
        //                foreach (var pegawai in Pegawais)
        //                {
        //                    var unitPegawai = (from a in conn.GetList<TbUnitPegawai>()
        //                                       where a.IdPegawai == pegawai.IdPegawai
        //                                       select a.IdUnit).FirstOrDefault();
        //                    if (unitKelas != unitPegawai)
        //                        err += string.Format("unit {0} kelas tidak sama dengan unit pegawai {1}; ", unitKelas, unitPegawai) + Environment.NewLine;
        //                    if(string.IsNullOrEmpty(err))
        //                    {
        //                        var tbKelasParalelPegawai = (from a in conn.GetList<TbKelasParalelPegawai>()
        //                                             where a.IdKelasParalel == IdKelasParalel & a.IdPegawai == pegawai.IdPegawai
        //                                             select a).FirstOrDefault();
        //                        if (tbKelasParalelPegawai == null)
        //                        {
        //                            tbKelasParalelPegawai = new TbKelasParalelPegawai { IdKelasParalel = IdKelasParalel, IdUnit = unitKelas, IdPegawai = pegawai.IdPegawai };
        //                            conn.Insert(tbKelasParalelPegawai);
        //                        }
        //                    }
        //                }
        //                if (string.IsNullOrEmpty(err))
        //                {
        //                    tx.Commit();
        //                }
        //                return err;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return commonService.GetErrorMessage(ServiceName + "KelasParalelPegawaiAdd", ex);
        //    }
        //}
        //public string KelasParalelPegawaiDelete(int IdUser, int IdKelasParalel, List<PegawaiJenisModel> Pegawais)
        //{
        //    try
        //    {
        //        using (var conn = commonService.DbConnection())
        //        {
        //            conn.Open();
        //            using (var tx = conn.BeginTransaction())
        //            {
        //                foreach (var pegawai in Pegawais)
        //                {
        //                    var tbKelasParalelPegawai = (from a in conn.GetList<TbKelasParalelPegawai>()
        //                                         where a.IdKelasParalel == IdKelasParalel & a.IdPegawai == pegawai.IdPegawai
        //                                         select a).FirstOrDefault();
        //                    if (tbKelasParalelPegawai == null) return "data tidak ada";
        //                    conn.Delete(tbKelasParalelPegawai);
        //                }
        //                tx.Commit();
        //                return string.Empty;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return commonService.GetErrorMessage(ServiceName + "KelasParalelPegawaiDelete", ex);
        //    }
        //}
    }
}
