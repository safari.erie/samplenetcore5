﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Swg.Entities.Kbm;
using Swg.Entities.Lmg;
using Swg.Entities.Public;
using Swg.Entities.Ref;
using Swg.Sch.Shared.Constants;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;
using Swg.Models.Constants;
namespace Swg.Sch.Services
{
    public class ELearningLaporanService : IELearningLaporanService
    {
        private readonly string ServiceName = "Swg.Sch.Services.ELearningLaporanService.";
        private readonly ICommonService commonService;
        private readonly IUserAppService userAppService;
        private readonly IDateTimeService dateTimeService;
        private readonly ISchService schService;
        public ELearningLaporanService(
            ICommonService CommonService,
            IDateTimeService DateTimeService,
            IUserAppService UserAppService,
            ISchService SchService)
        {
            commonService = CommonService;
            userAppService = UserAppService;
            dateTimeService = DateTimeService;
            schService = SchService;
        }
        private DateTime minDate;
        private DateTime maxDate;
        private string _languageCode = "id";
        private string SetMinMaxDate(string Tanggal)
        {
            try
            {
                DateTime currDate = DateTime.Now.AddDays(7).Date;
                DateTime NextFirstDate = dateTimeService.GetFirstDate(currDate.AddDays(7)).Date;
                if (!string.IsNullOrEmpty(Tanggal))
                {
                    currDate = dateTimeService.StrToDateTime(Tanggal);
                }
                if (currDate >= NextFirstDate)
                {
                    return string.Format("kbm belum aktif untuk tanggal {0}", dateTimeService.DateToLongString(_languageCode, currDate));
                }
                minDate = dateTimeService.GetFirstDate(currDate);
                maxDate = dateTimeService.GetLastDate(currDate);
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetMinMaxDate", ex);
            }
        }
        private bool CanReadAllUnit(int LevelJabatan)
        {
            bool canReadAllUnit = false;
            switch (LevelJabatan)
            {
                case LevelJabatanPegawaiConstant.Administrator:
                case LevelJabatanPegawaiConstant.Direksi:
                case LevelJabatanPegawaiConstant.Manager:
                    canReadAllUnit = true;
                    break;
                default:
                    break;
            }
            return canReadAllUnit;
        }
        public List<ElDashboardModel> GetElDashboard(out string oMessage)
        {
            oMessage = SetMinMaxDate(dateTimeService.GetCurrdate().ToString("dd-MM-yyyy"));
            if (!string.IsNullOrEmpty(oMessage)) return null;
            List<ElDashboardModel> ret = new List<ElDashboardModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    string sql = "";
                    sql += " select ";
                    sql += "  e.id_unit as IdUnit, ";
                    sql += "  e.nama NamaUnit,";
                    sql += "  100*(sum(case when a.judul is not null then 1 else 0 end)/count(9)::float) PstGuruIsiMateri";
                    sql += " from kbm.tb_kbm_materi a";
                    sql += " join kbm.tb_kelas_paralel c on a.id_kelas_paralel = c.id_kelas_paralel";
                    sql += " join kbm.tb_kelas d on c.id_kelas = d.id_kelas";
                    sql += " join lmg.tb_unit e on d.id_unit = e.id_unit";
                    sql += string.Format(" where tanggal between '{0}' and '{1}'", minDate.ToString("yyyy-MM-dd"), maxDate.ToString("yyyy-MM-dd"));
                    sql += " group by e.id_unit, e.nama";
                    sql += " order by e.id_unit";
                    var data1 = conn.Query<ElDashboardModel>(sql).ToList();
                    ElDashboardModel m1 = new ElDashboardModel();
                    ElDashboardModel m2 = new ElDashboardModel();
                    ElDashboardModel m3 = new ElDashboardModel();
                    ElDashboardModel m4 = new ElDashboardModel();
                    foreach (var item1 in data1)
                    {
                        int idUnit = item1.IdUnit;

                        if (idUnit == UnitConstant.Tk)
                        {
                            m1.IdUnit = item1.IdUnit;
                            m1.NamaUnit = item1.NamaUnit;
                            m1.PstGuruIsiMateri = item1.PstGuruIsiMateri;
                        }
                        else if (idUnit == UnitConstant.Sd)
                        {
                            m2.IdUnit = item1.IdUnit;
                            m2.NamaUnit = item1.NamaUnit;
                            m2.PstGuruIsiMateri = item1.PstGuruIsiMateri;
                        }
                        else if (idUnit == UnitConstant.Smp)
                        {
                            m3.IdUnit = item1.IdUnit;
                            m3.NamaUnit = item1.NamaUnit;
                            m3.PstGuruIsiMateri = item1.PstGuruIsiMateri;
                        }
                        else if (idUnit == UnitConstant.Sma)
                        {
                            m4.IdUnit = item1.IdUnit;
                            m4.NamaUnit = item1.NamaUnit;
                            m4.PstGuruIsiMateri = item1.PstGuruIsiMateri;
                        }
                    }
                    sql = "";
                    sql += " select ";
                    sql += "  e.id_unit as IdUnit, ";
                    sql += "  e.nama NamaUnit,";
                    sql += "  100*(sum(case when (g.nilai_angka is not null or g.nilai_huruf is not null) then 1 else 0 end)/count(0)::float) PstGuruIsiNilaiSiswa,";
                    sql += "  100*(sum(case when g.id_siswa is not null then 1 else 0 end)/count(0)::float) PstSiswaTugasSelesai,";
                    sql += "  100*(sum(case when (g.hadir is not null and g.hadir = -1) then 1 else 0 end)/count(0)::float) PstSiswaTidakHadir";
                    sql += " from kbm.tb_kbm_materi a";
                    sql += " join kbm.tb_kelas_paralel c on a.id_kelas_paralel = c.id_kelas_paralel";
                    sql += " join kbm.tb_kelas d on c.id_kelas = d.id_kelas";
                    sql += " join lmg.tb_unit e on d.id_unit = e.id_unit";
                    sql += " join kbm.tb_kelas_rombel f on c.id_kelas_paralel = f.id_kelas_paralel";
                    sql += " left join kbm.tb_kbm_jawaban g on f.id_siswa = g.id_siswa and a.id_kbm_materi = g.id_kbm_materi";
                    sql += string.Format(" where tanggal between '{0}' and '{1}'", minDate.ToString("yyyy-MM-dd"), maxDate.ToString("yyyy-MM-dd"));
                    sql += " group by e.id_unit, e.nama";

                    var data2 = conn.Query<ElDashboardModel>(sql).ToList();
                    foreach (var item2 in data2)
                    {
                        switch (item2.IdUnit)
                        {
                            case 1:
                                m1.PstGuruIsiNilaiSiswa = item2.PstGuruIsiNilaiSiswa;
                                m1.PstSiswaTidakHadir = item2.PstSiswaTidakHadir;
                                m1.PstSiswaTugasSelesai = item2.PstSiswaTugasSelesai;
                                break;
                            case 2:
                                m2.PstGuruIsiNilaiSiswa = item2.PstGuruIsiNilaiSiswa;
                                m2.PstSiswaTidakHadir = item2.PstSiswaTidakHadir;
                                m2.PstSiswaTugasSelesai = item2.PstSiswaTugasSelesai;
                                break;
                            case 3:
                                m3.PstGuruIsiNilaiSiswa = item2.PstGuruIsiNilaiSiswa;
                                m3.PstSiswaTidakHadir = item2.PstSiswaTidakHadir;
                                m3.PstSiswaTugasSelesai = item2.PstSiswaTugasSelesai;
                                break;
                            case 4:
                                m4.PstGuruIsiNilaiSiswa = item2.PstGuruIsiNilaiSiswa;
                                m4.PstSiswaTidakHadir = item2.PstSiswaTidakHadir;
                                m4.PstSiswaTugasSelesai = item2.PstSiswaTugasSelesai;
                                break;
                            default:
                                break;
                        }
                    }

                    sql = "";
                    sql += " select ";
                    sql += "  e.id_unit as IdUnit, ";
                    sql += "  e.nama NamaUnit,";
                    sql += "  100*(sum(case when g.id_siswa is not null then 1 else 0 end)/count(0)::float) PstSiswaIsiMutabaah";
                    sql += " from kbm.tb_kelas_paralel c";
                    sql += " join kbm.tb_kelas d on c.id_kelas = d.id_kelas";
                    sql += " join lmg.tb_unit e on d.id_unit = e.id_unit";
                    sql += " join kbm.tb_kelas_rombel f on c.id_kelas_paralel = f.id_kelas_paralel";
                    sql += string.Format(" left join (select distinct id_siswa from kbm.tb_kbm_evaluasi_harian where tanggal between '{0}' and '{0}' and is_done = 'Y') g on f.id_siswa = g.id_siswa ", minDate.ToString("yyyy-MM-dd"), maxDate.ToString("yyyy-MM-dd"));
                    sql += " group by e.id_unit, e.nama";


                    var data3 = conn.Query<ElDashboardModel>(sql).ToList();
                    foreach (var item3 in data2)
                    {
                        switch (item3.IdUnit)
                        {
                            case 1:
                                m1.PstSiswaIsiMutabaah = item3.PstSiswaIsiMutabaah;
                                break;
                            case 2:
                                m2.PstSiswaIsiMutabaah = item3.PstSiswaIsiMutabaah;
                                break;
                            case 3:
                                m3.PstSiswaIsiMutabaah = item3.PstSiswaIsiMutabaah;
                                break;
                            case 4:
                                m4.PstSiswaIsiMutabaah = item3.PstSiswaIsiMutabaah;
                                break;
                            default:
                                break;
                        }
                    }
                    ret.Add(m1);
                    ret.Add(m2);
                    ret.Add(m3);
                    ret.Add(m4);
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElDashboard", ex);
                return null;
            }
        }

        #region materi dan tugas
        public ElMateriTugasModel GetElMateriTugas(int IdUser, string Tanggal, out string oMessage)
        {
            oMessage = SetMinMaxDate(Tanggal);
            if (!string.IsNullOrEmpty(oMessage)) return null;
            ElMateriTugasModel ret = new ElMateriTugasModel
            {
                Periode = new ElTanggalModel
                {
                    TanggalAwal = minDate.ToString("dd-MM-yyyy"),
                    TanggalAkhir = maxDate.ToString("dd-MM-yyyy")
                },
                Units = new List<ElMateriTugasUnitModel>()
            };
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    oMessage = schService.GetLevelJabatanUnitPegawai(conn, IdUser, out int LevelJabatan, out int IdUnit);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    bool canReadAllUnit = CanReadAllUnit(LevelJabatan);
                    var data = from materi in conn.GetList<TbKbmMateri>()
                               join kelasParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelasParalel.IdKelasParalel
                               join kelas in conn.GetList<TbKelas>() on kelasParalel.IdKelas equals kelas.IdKelas
                               join unit in conn.GetList<TbUnit>() on kelas.IdUnit equals unit.IdUnit
                               where materi.Tanggal.Date >= minDate.Date & materi.Tanggal <= maxDate.Date
                               & unit.IdUnit == (canReadAllUnit ? unit.IdUnit : IdUnit)
                               select new { unit.IdUnit, unit.Nama, materi.Judul };
                    string tempNama = string.Empty;
                    int jmlTotal = 0;
                    int jmlInput = 0;
                    foreach (var item in data.OrderBy(x => x.IdUnit))
                    {
                        if (tempNama != item.Nama)
                        {
                            if (!string.IsNullOrEmpty(tempNama))
                            {
                                ret.Units.Add(new ElMateriTugasUnitModel
                                {
                                    IdUnit = IdUnit,
                                    Unit = tempNama,
                                    JmlTotal = jmlTotal,
                                    JmlInput = jmlInput,
                                    JmlNoInput = jmlTotal - jmlInput
                                });
                            }
                            IdUnit = item.IdUnit;
                            tempNama = item.Nama;
                            jmlTotal = 0;
                            jmlInput = 0;
                        }
                        jmlTotal++;
                        if (!string.IsNullOrEmpty(item.Judul))
                            jmlInput++;
                    }
                    ret.Units.Add(new ElMateriTugasUnitModel
                    {
                        IdUnit = IdUnit,
                        Unit = tempNama,
                        JmlTotal = jmlTotal,
                        JmlInput = jmlInput,
                        JmlNoInput = jmlTotal - jmlInput
                    });
                }

                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElMateriTugas", ex);
                return null;
            }
        }
        public List<ElMateriTugasKelasModel> GetElMateriTugasKelas(int IdUser, int IdUnit, string Tanggal, out string oMessage)
        {
            oMessage = SetMinMaxDate(Tanggal);
            if (!string.IsNullOrEmpty(oMessage)) return null;
            List<ElMateriTugasKelasModel> ret = new List<ElMateriTugasKelasModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = from materi in conn.GetList<TbKbmMateri>()
                               join kelasParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelasParalel.IdKelasParalel
                               join kelas in conn.GetList<TbKelas>() on kelasParalel.IdKelas equals kelas.IdKelas
                               join unit in conn.GetList<TbUnit>() on kelas.IdUnit equals unit.IdUnit
                               where materi.Tanggal.Date >= minDate.Date & materi.Tanggal <= maxDate.Date
                               & unit.IdUnit == IdUnit
                               select new { unit.IdUnit, kelas.IdKelas, TempNama = unit.Nama + kelas.Nama, NamaUnit = unit.Nama, NamaKelas = kelas.Nama, materi.Judul };
                    string tempNama = string.Empty;
                    string tempNamaUnit = string.Empty;
                    int idKelas = 0;
                    string tempNamaKelas = string.Empty;
                    int jmlTotal = 0;
                    int jmlInput = 0;
                    foreach (var item in data.OrderBy(x => x.TempNama))
                    {
                        if (tempNama != item.TempNama)
                        {
                            if (!string.IsNullOrEmpty(tempNama))
                            {

                                ret.Add(new ElMateriTugasKelasModel
                                {
                                    IdUnit = IdUnit,
                                    Unit = tempNamaUnit,
                                    IdKelas = idKelas,
                                    Kelas = tempNamaKelas,
                                    JmlTotal = jmlTotal,
                                    JmlInput = jmlInput,
                                    JmlNoInput = jmlTotal - jmlInput
                                });
                            }
                            idKelas = item.IdKelas;
                            tempNama = item.TempNama;
                            tempNamaUnit = item.NamaUnit;
                            tempNamaKelas = item.NamaKelas;
                            jmlTotal = 0;
                            jmlInput = 0;
                        }
                        jmlTotal++;
                        if (!string.IsNullOrEmpty(item.Judul))
                            jmlInput++;
                    }
                    ret.Add(new ElMateriTugasKelasModel
                    {
                        IdUnit = IdUnit,
                        IdKelas = idKelas,
                        Unit = tempNamaUnit,
                        Kelas = tempNamaKelas,
                        JmlTotal = jmlTotal,
                        JmlInput = jmlInput,
                        JmlNoInput = jmlTotal - jmlInput
                    });
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElMateriTugasKelas", ex);
                return null;
            }
        }
        public List<ElMateriTugasKelasParalelModel> GetElMateriTugasKelasParalel(int IdUser, int IdKelas, string Tanggal, out string oMessage)
        {
            oMessage = SetMinMaxDate(Tanggal);
            if (!string.IsNullOrEmpty(oMessage)) return null;

            List<ElMateriTugasKelasParalelModel> ret = new List<ElMateriTugasKelasParalelModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = from materi in conn.GetList<TbKbmMateri>()
                               join kelasParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelasParalel.IdKelasParalel
                               join kelas in conn.GetList<TbKelas>() on kelasParalel.IdKelas equals kelas.IdKelas
                               join unit in conn.GetList<TbUnit>() on kelas.IdUnit equals unit.IdUnit
                               where materi.Tanggal.Date >= minDate.Date & materi.Tanggal <= maxDate.Date
                               & kelas.IdKelas == IdKelas
                               select new { unit.IdUnit, kelas.IdKelas, kelasParalel.IdKelasParalel, TempNama = unit.Nama + kelas.Nama + kelasParalel.Nama, NamaUnit = unit.Nama, NamaKelas = kelas.Nama, NamaKelasParalel = kelasParalel.Nama, materi.Judul };
                    string tempNama = string.Empty;
                    int idKelasParalel = 0;
                    int idUnit = 0;

                    string tempNamaUnit = string.Empty;
                    string tempNamaKelas = string.Empty;
                    string tempNamaKelasParalel = string.Empty;
                    int jmlTotal = 0;
                    int jmlInput = 0;
                    foreach (var item in data.OrderBy(x => x.TempNama))
                    {
                        if (tempNama != item.TempNama)
                        {
                            if (!string.IsNullOrEmpty(tempNama))
                            {

                                ret.Add(new ElMateriTugasKelasParalelModel
                                {
                                    IdUnit = idUnit,
                                    IdKelas = IdKelas,
                                    IdKelasParalel = idKelasParalel,
                                    Unit = tempNamaUnit,
                                    Kelas = tempNamaKelas,
                                    KelasParalel = tempNamaKelasParalel,
                                    JmlTotal = jmlTotal,
                                    JmlInput = jmlInput,
                                    JmlNoInput = jmlTotal - jmlInput
                                });
                            }
                            idUnit = item.IdUnit;
                            idKelasParalel = item.IdKelasParalel;
                            tempNama = item.TempNama;
                            tempNamaUnit = item.NamaUnit;
                            tempNamaKelas = item.NamaKelas;
                            tempNamaKelasParalel = item.NamaKelasParalel;
                            jmlTotal = 0;
                            jmlInput = 0;
                        }
                        jmlTotal++;
                        if (!string.IsNullOrEmpty(item.Judul))
                            jmlInput++;
                    }
                    ret.Add(new ElMateriTugasKelasParalelModel
                    {
                        IdUnit = idUnit,
                        IdKelas = IdKelas,
                        IdKelasParalel = idKelasParalel,
                        Unit = tempNamaUnit,
                        Kelas = tempNamaKelas,
                        KelasParalel = tempNamaKelasParalel,
                        JmlTotal = jmlTotal,
                        JmlInput = jmlInput,
                        JmlNoInput = jmlTotal - jmlInput
                    });
                }

                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElMateriTugasKelasParalel", ex);
                return null;
            }
        }
        public ElMateriTugasListModel GetElMateriTugasList(int IdUser, int IdKelasParalel, string Tanggal, out string oMessage)
        {
            oMessage = SetMinMaxDate(Tanggal);
            if (!string.IsNullOrEmpty(oMessage)) return null;
            ElMateriTugasListModel ret = new ElMateriTugasListModel
            {
                Input = new List<ElMateriTugasMapelModel>(),
                NotInput = new List<ElMateriTugasMapelModel>()
            };

            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = from materi in conn.GetList<TbKbmMateri>()
                               join user in conn.GetList<TbUser>() on materi.IdPegawai equals user.IdUser
                               join mapel in conn.GetList<TbMapel>() on materi.IdMapel equals mapel.IdMapel
                               join kbmJadwal in conn.GetList<TbKbmJadwal>() on materi.IdKbmJadwal equals kbmJadwal.IdKbmJadwal
                               join kelasParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelasParalel.IdKelasParalel
                               join kelas in conn.GetList<TbKelas>() on kelasParalel.IdKelas equals kelas.IdKelas
                               join unit in conn.GetList<TbUnit>() on kelas.IdUnit equals unit.IdUnit
                               where materi.Tanggal.Date >= minDate.Date & materi.Tanggal <= maxDate.Date
                               & kelasParalel.IdKelasParalel == IdKelasParalel
                               select new { materi, user, kbmJadwal, mapel, kelasParalel, kelas, unit };

                    foreach (var item in data)
                    {
                        ret.InfoJudul = string.Format("Unit {0}, Kelas {1}, Kelas Paralel {2}", item.unit.Nama, item.kelas.Nama, item.kelasParalel.Nama);
                        ElMateriTugasMapelModel m = new ElMateriTugasMapelModel
                        {
                            FileMateri = item.materi.NamaFileMateri,
                            Hari = dateTimeService.GetLongDayName(_languageCode, (int)item.materi.Tanggal.DayOfWeek),
                            JamMulai = item.kbmJadwal.JamMulai.ToString(),
                            JamSelesai = item.kbmJadwal.JamSelesai.ToString(),
                            JudulMateri = item.materi.Judul,
                            MataPelajaran = item.mapel.Nama,
                            NamaGuru = userAppService.SetFullName(item.user.FirstName, item.user.MiddleName, item.user.LastName)
                        };
                        var libur = schService.GetHariLibur(item.materi.Tanggal.ToString("dd-MM-yyyy"), out oMessage);
                        if (string.IsNullOrEmpty(oMessage))
                        {
                            if (libur != null)
                            {
                                m.MataPelajaran = string.Format("LIBUR {0}", libur.Nama);
                            }
                        }
                        if (string.IsNullOrEmpty(item.materi.Judul))
                        {
                            ret.NotInput.Add(m);
                        }
                        else
                        {
                            ret.Input.Add(m);
                        }
                    }
                }

                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElMateriTugasList", ex);
                return null;
            }
        }
        #endregion
        #region kbm siswa
        public ElKbmSiswaModel GetElKbmSiswa(int IdUser, string Tanggal, out string oMessage)
        {
            oMessage = SetMinMaxDate(Tanggal);
            if (!string.IsNullOrEmpty(oMessage)) return null;
            ElKbmSiswaModel ret = new ElKbmSiswaModel
            {
                Periode = new ElTanggalModel
                {
                    TanggalAwal = minDate.ToString("dd-MM-yyyy"),
                    TanggalAkhir = maxDate.ToString("dd-MM-yyyy")
                },
                Units = new List<ElKbmSiswaUnitModel>()
            };
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    oMessage = schService.GetLevelJabatanUnitPegawai(conn, IdUser, out int LevelJabatan, out int IdUnit);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    bool canReadAllUnit = CanReadAllUnit(LevelJabatan);

                    var data = from materi in conn.GetList<TbKbmMateri>()
                               join kelasParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelasParalel.IdKelasParalel
                               join kelas in conn.GetList<TbKelas>() on kelasParalel.IdKelas equals kelas.IdKelas
                               join unit in conn.GetList<TbUnit>() on kelas.IdUnit equals unit.IdUnit
                               join rombel in conn.GetList<TbKelasRombel>() on kelasParalel.IdKelasParalel equals rombel.IdKelasParalel
                               join jawab in conn.GetList<TbKbmJawaban>() on new { rombel.IdSiswa, materi.IdKbmMateri } equals new { jawab.IdSiswa, jawab.IdKbmMateri } into jawab1
                               from jawab in jawab1.DefaultIfEmpty()
                               where materi.Tanggal.Date >= minDate.Date & materi.Tanggal <= maxDate.Date
                               & unit.IdUnit == (canReadAllUnit ? unit.IdUnit : IdUnit)
                               select new { unit.IdUnit, unit.Nama, jawab };
                    string tempNama = string.Empty;
                    int jmlTotal = 0;
                    int jmlJawab = 0;
                    foreach (var item in data.OrderBy(x => x.IdUnit))
                    {
                        if (tempNama != item.Nama)
                        {
                            if (!string.IsNullOrEmpty(tempNama))
                            {
                                ret.Units.Add(new ElKbmSiswaUnitModel
                                {
                                    IdUnit = IdUnit,
                                    Unit = tempNama,
                                    JmlTotal = jmlTotal,
                                    JmlJawab = jmlJawab,
                                    JmlNoJawab = jmlTotal - jmlJawab
                                });
                            }
                            IdUnit = item.IdUnit;
                            tempNama = item.Nama;
                            jmlTotal = 0;
                            jmlJawab = 0;
                        }
                        jmlTotal++;
                        if (item.jawab != null)
                        {
                            if (!string.IsNullOrEmpty(item.jawab.NamaFile) || !string.IsNullOrEmpty(item.jawab.NamaUrl))
                                jmlJawab++;

                        }
                    }
                    ret.Units.Add(new ElKbmSiswaUnitModel
                    {
                        IdUnit = IdUnit,
                        Unit = tempNama,
                        JmlTotal = jmlTotal,
                        JmlJawab = jmlJawab,
                        JmlNoJawab = jmlTotal - jmlJawab
                    });
                }

                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElKbmSiswa", ex);
                return null;
            }
        }
        public List<ElKbmSiswaKelasModel> GetElKbmSiswaKelas(int IdUser, int IdUnit, string Tanggal, out string oMessage)
        {
            oMessage = SetMinMaxDate(Tanggal);
            if (!string.IsNullOrEmpty(oMessage)) return null;
            List<ElKbmSiswaKelasModel> ret = new List<ElKbmSiswaKelasModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = from materi in conn.GetList<TbKbmMateri>()
                               join kelasParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelasParalel.IdKelasParalel
                               join kelas in conn.GetList<TbKelas>() on kelasParalel.IdKelas equals kelas.IdKelas
                               join unit in conn.GetList<TbUnit>() on kelas.IdUnit equals unit.IdUnit
                               join rombel in conn.GetList<TbKelasRombel>() on kelasParalel.IdKelasParalel equals rombel.IdKelasParalel
                               join jawab in conn.GetList<TbKbmJawaban>() on new { rombel.IdSiswa, materi.IdKbmMateri } equals new { jawab.IdSiswa, jawab.IdKbmMateri } into jawab1
                               from jawab in jawab1.DefaultIfEmpty()
                               where materi.Tanggal.Date >= minDate.Date & materi.Tanggal <= maxDate.Date
                               & unit.IdUnit == IdUnit
                               select new { unit.IdUnit, kelas.IdKelas, TempNama = unit.Nama + kelas.Nama, NamaUnit = unit.Nama, NamaKelas = kelas.Nama, jawab };
                    string tempNama = string.Empty;
                    string tempNamaUnit = string.Empty;
                    int idKelas = 0;
                    string tempNamaKelas = string.Empty;
                    int jmlTotal = 0;
                    int jmlJawab = 0;
                    foreach (var item in data.OrderBy(x => x.TempNama))
                    {
                        if (tempNama != item.TempNama)
                        {
                            if (!string.IsNullOrEmpty(tempNama))
                            {

                                ret.Add(new ElKbmSiswaKelasModel
                                {
                                    IdUnit = IdUnit,
                                    Unit = tempNamaUnit,
                                    IdKelas = idKelas,
                                    Kelas = tempNamaKelas,
                                    JmlTotal = jmlTotal,
                                    JmlJawab = jmlJawab,
                                    JmlNoJawab = jmlTotal - jmlJawab
                                });
                            }
                            idKelas = item.IdKelas;
                            tempNama = item.TempNama;
                            tempNamaUnit = item.NamaUnit;
                            tempNamaKelas = item.NamaKelas;
                            jmlTotal = 0;
                            jmlJawab = 0;
                        }
                        jmlTotal++;
                        if (item.jawab != null)
                        {
                            if (!string.IsNullOrEmpty(item.jawab.NamaFile) || !string.IsNullOrEmpty(item.jawab.NamaUrl))
                                jmlJawab++;

                        }
                    }
                    ret.Add(new ElKbmSiswaKelasModel
                    {
                        IdUnit = IdUnit,
                        IdKelas = idKelas,
                        Unit = tempNamaUnit,
                        Kelas = tempNamaKelas,
                        JmlTotal = jmlTotal,
                        JmlJawab = jmlJawab,
                        JmlNoJawab = jmlTotal - jmlJawab
                    });
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElKbmSiswaKelas", ex);
                return null;
            }
        }
        public List<ElKbmSiswaKelasParalelModel> GetElKbmSiswaKelasParalel(int IdUser, int IdKelas, string Tanggal, out string oMessage)
        {
            oMessage = SetMinMaxDate(Tanggal);
            if (!string.IsNullOrEmpty(oMessage)) return null;

            List<ElKbmSiswaKelasParalelModel> ret = new List<ElKbmSiswaKelasParalelModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = from materi in conn.GetList<TbKbmMateri>()
                               join kelasParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelasParalel.IdKelasParalel
                               join kelas in conn.GetList<TbKelas>() on kelasParalel.IdKelas equals kelas.IdKelas
                               join unit in conn.GetList<TbUnit>() on kelas.IdUnit equals unit.IdUnit
                               join rombel in conn.GetList<TbKelasRombel>() on kelasParalel.IdKelasParalel equals rombel.IdKelasParalel
                               join jawab in conn.GetList<TbKbmJawaban>() on new { rombel.IdSiswa, materi.IdKbmMateri } equals new { jawab.IdSiswa, jawab.IdKbmMateri } into jawab1
                               from jawab in jawab1.DefaultIfEmpty()
                               where materi.Tanggal.Date >= minDate.Date & materi.Tanggal <= maxDate.Date
                               & kelas.IdKelas == IdKelas
                               select new { unit.IdUnit, kelas.IdKelas, kelasParalel.IdKelasParalel, TempNama = unit.Nama + kelas.Nama + kelasParalel.Nama, NamaUnit = unit.Nama, NamaKelas = kelas.Nama, NamaKelasParalel = kelasParalel.Nama, jawab };
                    string tempNama = string.Empty;
                    int idKelasParalel = 0;
                    int idUnit = 0;

                    string tempNamaUnit = string.Empty;
                    string tempNamaKelas = string.Empty;
                    string tempNamaKelasParalel = string.Empty;
                    int jmlTotal = 0;
                    int jmlJawab = 0;
                    foreach (var item in data.OrderBy(x => x.TempNama))
                    {
                        if (tempNama != item.TempNama)
                        {
                            if (!string.IsNullOrEmpty(tempNama))
                            {

                                ret.Add(new ElKbmSiswaKelasParalelModel
                                {
                                    IdUnit = idUnit,
                                    IdKelas = IdKelas,
                                    IdKelasParalel = idKelasParalel,
                                    Unit = tempNamaUnit,
                                    Kelas = tempNamaKelas,
                                    KelasParalel = tempNamaKelasParalel,
                                    JmlTotal = jmlTotal,
                                    JmlJawab = jmlJawab,
                                    JmlNoJawab = jmlTotal - jmlJawab
                                });
                            }
                            idUnit = item.IdUnit;
                            idKelasParalel = item.IdKelasParalel;
                            tempNama = item.TempNama;
                            tempNamaUnit = item.NamaUnit;
                            tempNamaKelas = item.NamaKelas;
                            tempNamaKelasParalel = item.NamaKelasParalel;
                            jmlTotal = 0;
                            jmlJawab = 0;
                        }
                        jmlTotal++;
                        if (item.jawab != null)
                        {
                            if (!string.IsNullOrEmpty(item.jawab.NamaFile) || !string.IsNullOrEmpty(item.jawab.NamaUrl))
                                jmlJawab++;

                        }
                    }
                    ret.Add(new ElKbmSiswaKelasParalelModel
                    {
                        IdUnit = idUnit,
                        IdKelas = IdKelas,
                        IdKelasParalel = idKelasParalel,
                        Unit = tempNamaUnit,
                        Kelas = tempNamaKelas,
                        KelasParalel = tempNamaKelasParalel,
                        JmlTotal = jmlTotal,
                        JmlJawab = jmlJawab,
                        JmlNoJawab = jmlTotal - jmlJawab
                    });
                }

                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElKbmSiswaKelasParalel", ex);
                return null;
            }
        }
        public ElKbmSiswaListModel GetElKbmSiswaList(int IdUser, int IdKelasParalel, string Tanggal, out string oMessage)
        {
            oMessage = SetMinMaxDate(Tanggal);
            if (!string.IsNullOrEmpty(oMessage)) return null;
            ElKbmSiswaListModel ret = new ElKbmSiswaListModel
            {
                Jawab = new List<ElKbmSiswaMapelModel>(),
                NoJawab = new List<ElKbmSiswaMapelModel>()
            };

            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = from materi in conn.GetList<TbKbmMateri>()
                               join user in conn.GetList<TbUser>() on materi.IdPegawai equals user.IdUser
                               join mapel in conn.GetList<TbMapel>() on materi.IdMapel equals mapel.IdMapel
                               join kbmJadwal in conn.GetList<TbKbmJadwal>() on materi.IdKbmJadwal equals kbmJadwal.IdKbmJadwal
                               join kelasParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelasParalel.IdKelasParalel
                               join kelas in conn.GetList<TbKelas>() on kelasParalel.IdKelas equals kelas.IdKelas
                               join unit in conn.GetList<TbUnit>() on kelas.IdUnit equals unit.IdUnit
                               join rombel in conn.GetList<TbKelasRombel>() on kelasParalel.IdKelasParalel equals rombel.IdKelasParalel
                               join siswa in conn.GetList<TbSiswa>() on rombel.IdSiswa equals siswa.IdSiswa
                               join jawab in conn.GetList<TbKbmJawaban>() on new { rombel.IdSiswa, materi.IdKbmMateri } equals new { jawab.IdSiswa, jawab.IdKbmMateri } into jawab1
                               from jawab in jawab1.DefaultIfEmpty()
                               where materi.Tanggal.Date >= minDate.Date & materi.Tanggal <= maxDate.Date
                               & kelasParalel.IdKelasParalel == IdKelasParalel
                               select new { materi, user, kbmJadwal, mapel, kelasParalel, kelas, unit, siswa, jawab };

                    foreach (var item in data)
                    {
                        ret.InfoJudul = string.Format("Unit {0}, Kelas {1}, Kelas Paralel {2}", item.unit.Nama, item.kelas.Nama, item.kelasParalel.Nama);
                        ElKbmSiswaMapelModel m = new ElKbmSiswaMapelModel
                        {
                            Hari = dateTimeService.GetLongDayName(_languageCode, (int)item.materi.Tanggal.DayOfWeek),
                            JamMulai = item.kbmJadwal.JamMulai.ToString(),
                            JamSelesai = item.kbmJadwal.JamSelesai.ToString(),
                            MataPelajaran = item.mapel.Nama,
                            NamaSiswa = item.siswa.Nama,
                            Nis = item.siswa.Nis,
                            FileJawaban = "",
                            Nilai = string.Empty
                        };
                        var libur = schService.GetHariLibur(item.materi.Tanggal.ToString("dd-MM-yyyy"), out oMessage);
                        if (string.IsNullOrEmpty(oMessage))
                        {
                            if (libur != null)
                            {
                                m.MataPelajaran = "LIBUR";
                            }
                        }
                        if (item.jawab == null)
                        {
                            ret.NoJawab.Add(m);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(item.jawab.NamaFile) || !string.IsNullOrEmpty(item.jawab.NamaUrl))
                            {
                                m.FileJawaban = item.jawab.NamaFile;
                                if (string.IsNullOrEmpty(m.FileJawaban))
                                    m.FileJawaban = item.jawab.NamaUrl;
                                m.Nilai = item.jawab.NilaiHuruf;
                                if (item.jawab.NilaiAngka != null)
                                    m.Nilai = item.jawab.NilaiAngka.ToString();
                                ret.Jawab.Add(m);
                            }
                            else
                            {
                                ret.NoJawab.Add(m);
                            }
                        }
                    }
                }

                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElKbmSiswaList", ex);
                return null;
            }
        }
        #endregion
        #region kehadiran siswa
        public ElAbsenSiswaModel GetElAbsenSiswa(int IdUser, string Tanggal, out string oMessage)
        {
            oMessage = SetMinMaxDate(Tanggal);
            if (!string.IsNullOrEmpty(oMessage)) return null;
            ElAbsenSiswaModel ret = new ElAbsenSiswaModel
            {
                Periode = new ElTanggalModel
                {
                    TanggalAwal = minDate.ToString("dd-MM-yyyy"),
                    TanggalAkhir = maxDate.ToString("dd-MM-yyyy")
                },
                Units = new List<ElAbsenSiswaUnitModel>()
            };
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    oMessage = schService.GetLevelJabatanUnitPegawai(conn, IdUser, out int LevelJabatan, out int IdUnit);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    bool canReadAllUnit = CanReadAllUnit(LevelJabatan);

                    var data = from materi in conn.GetList<TbKbmMateri>()
                               join kelasParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelasParalel.IdKelasParalel
                               join kelas in conn.GetList<TbKelas>() on kelasParalel.IdKelas equals kelas.IdKelas
                               join unit in conn.GetList<TbUnit>() on kelas.IdUnit equals unit.IdUnit
                               join rombel in conn.GetList<TbKelasRombel>() on kelasParalel.IdKelasParalel equals rombel.IdKelasParalel
                               join jawab in conn.GetList<TbKbmJawaban>() on new { rombel.IdSiswa, materi.IdKbmMateri } equals new { jawab.IdSiswa, jawab.IdKbmMateri } into jawab1
                               from jawab in jawab1.DefaultIfEmpty()
                               where materi.Tanggal.Date >= minDate.Date & materi.Tanggal <= maxDate.Date
                               & unit.IdUnit == (canReadAllUnit ? unit.IdUnit : IdUnit)
                               select new { unit.IdUnit, unit.Nama, jawab };
                    string tempNama = string.Empty;
                    int jmlTotal = 0;
                    int jmlHadir = 0;
                    foreach (var item in data.OrderBy(x => x.IdUnit))
                    {
                        if (tempNama != item.Nama)
                        {
                            if (!string.IsNullOrEmpty(tempNama))
                            {
                                ret.Units.Add(new ElAbsenSiswaUnitModel
                                {
                                    IdUnit = IdUnit,
                                    Unit = tempNama,
                                    JmlTotal = jmlTotal,
                                    JmlHadir = jmlHadir,
                                    JmlNoHadir = jmlTotal - jmlHadir
                                });
                            }
                            IdUnit = item.IdUnit;
                            tempNama = item.Nama;
                            jmlTotal = 0;
                            jmlHadir = 0;
                        }
                        jmlTotal++;
                        if (item.jawab != null)
                        {
                            if (item.jawab.Hadir != -1)
                                jmlHadir++;

                        }
                    }
                    ret.Units.Add(new ElAbsenSiswaUnitModel
                    {
                        IdUnit = IdUnit,
                        Unit = tempNama,
                        JmlTotal = jmlTotal,
                        JmlHadir = jmlHadir,
                        JmlNoHadir = jmlTotal - jmlHadir
                    });
                }

                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElAbsenSiswa", ex);
                return null;
            }
        }
        public List<ElAbsenSiswaKelasModel> GetElAbsenSiswaKelas(int IdUser, int IdUnit, string Tanggal, out string oMessage)
        {
            oMessage = SetMinMaxDate(Tanggal);
            if (!string.IsNullOrEmpty(oMessage)) return null;
            List<ElAbsenSiswaKelasModel> ret = new List<ElAbsenSiswaKelasModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {

                    var data = from materi in conn.GetList<TbKbmMateri>()
                               join kelasParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelasParalel.IdKelasParalel
                               join kelas in conn.GetList<TbKelas>() on kelasParalel.IdKelas equals kelas.IdKelas
                               join unit in conn.GetList<TbUnit>() on kelas.IdUnit equals unit.IdUnit
                               join rombel in conn.GetList<TbKelasRombel>() on kelasParalel.IdKelasParalel equals rombel.IdKelasParalel
                               join jawab in conn.GetList<TbKbmJawaban>() on new { rombel.IdSiswa, materi.IdKbmMateri } equals new { jawab.IdSiswa, jawab.IdKbmMateri } into jawab1
                               from jawab in jawab1.DefaultIfEmpty()
                               where materi.Tanggal.Date >= minDate.Date & materi.Tanggal <= maxDate.Date
                               & unit.IdUnit == IdUnit
                               select new { unit.IdUnit, kelas.IdKelas, TempNama = unit.Nama + kelas.Nama, NamaUnit = unit.Nama, NamaKelas = kelas.Nama, jawab };
                    string tempNama = string.Empty;
                    string tempNamaUnit = string.Empty;
                    int idKelas = 0;
                    string tempNamaKelas = string.Empty;
                    int jmlTotal = 0;
                    int jmlHadir = 0;
                    foreach (var item in data.OrderBy(x => x.TempNama))
                    {
                        if (tempNama != item.TempNama)
                        {
                            if (!string.IsNullOrEmpty(tempNama))
                            {

                                ret.Add(new ElAbsenSiswaKelasModel
                                {
                                    IdUnit = IdUnit,
                                    Unit = tempNamaUnit,
                                    IdKelas = idKelas,
                                    Kelas = tempNamaKelas,
                                    JmlTotal = jmlTotal,
                                    JmlHadir = jmlHadir,
                                    JmlNoHadir = jmlTotal - jmlHadir
                                });
                            }
                            idKelas = item.IdKelas;
                            tempNama = item.TempNama;
                            tempNamaUnit = item.NamaUnit;
                            tempNamaKelas = item.NamaKelas;
                            jmlTotal = 0;
                            jmlHadir = 0;
                        }
                        jmlTotal++;
                        if (item.jawab != null)
                        {
                            if (item.jawab.Hadir != -1)
                                jmlHadir++;

                        }
                    }
                    ret.Add(new ElAbsenSiswaKelasModel
                    {
                        IdUnit = IdUnit,
                        IdKelas = idKelas,
                        Unit = tempNamaUnit,
                        Kelas = tempNamaKelas,
                        JmlTotal = jmlTotal,
                        JmlHadir = jmlHadir,
                        JmlNoHadir = jmlTotal - jmlHadir
                    });
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElAbsenSiswaKelas", ex);
                return null;
            }
        }
        public List<ElAbsenSiswaKelasParalelModel> GetElAbsenSiswaKelasParalel(int IdUser, int IdKelas, string Tanggal, out string oMessage)
        {
            oMessage = SetMinMaxDate(Tanggal);
            if (!string.IsNullOrEmpty(oMessage)) return null;

            List<ElAbsenSiswaKelasParalelModel> ret = new List<ElAbsenSiswaKelasParalelModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = from materi in conn.GetList<TbKbmMateri>()
                               join kelasParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelasParalel.IdKelasParalel
                               join kelas in conn.GetList<TbKelas>() on kelasParalel.IdKelas equals kelas.IdKelas
                               join unit in conn.GetList<TbUnit>() on kelas.IdUnit equals unit.IdUnit
                               join rombel in conn.GetList<TbKelasRombel>() on kelasParalel.IdKelasParalel equals rombel.IdKelasParalel
                               join jawab in conn.GetList<TbKbmJawaban>() on new { rombel.IdSiswa, materi.IdKbmMateri } equals new { jawab.IdSiswa, jawab.IdKbmMateri } into jawab1
                               from jawab in jawab1.DefaultIfEmpty()
                               where materi.Tanggal.Date >= minDate.Date & materi.Tanggal <= maxDate.Date
                               & kelas.IdKelas == IdKelas
                               select new { unit.IdUnit, kelas.IdKelas, kelasParalel.IdKelasParalel, TempNama = unit.Nama + kelas.Nama + kelasParalel.Nama, NamaUnit = unit.Nama, NamaKelas = kelas.Nama, NamaKelasParalel = kelasParalel.Nama, jawab };
                    string tempNama = string.Empty;
                    int idKelasParalel = 0;
                    int idUnit = 0;

                    string tempNamaUnit = string.Empty;
                    string tempNamaKelas = string.Empty;
                    string tempNamaKelasParalel = string.Empty;
                    int jmlTotal = 0;
                    int jmlHadir = 0;
                    foreach (var item in data.OrderBy(x => x.TempNama))
                    {
                        if (tempNama != item.TempNama)
                        {
                            if (!string.IsNullOrEmpty(tempNama))
                            {

                                ret.Add(new ElAbsenSiswaKelasParalelModel
                                {
                                    IdUnit = idUnit,
                                    IdKelas = IdKelas,
                                    IdKelasParalel = idKelasParalel,
                                    Unit = tempNamaUnit,
                                    Kelas = tempNamaKelas,
                                    KelasParalel = tempNamaKelasParalel,
                                    JmlTotal = jmlTotal,
                                    JmlHadir = jmlHadir,
                                    JmlNoHadir = jmlTotal - jmlHadir
                                });
                            }
                            idUnit = item.IdUnit;
                            idKelasParalel = item.IdKelasParalel;
                            tempNama = item.TempNama;
                            tempNamaUnit = item.NamaUnit;
                            tempNamaKelas = item.NamaKelas;
                            tempNamaKelasParalel = item.NamaKelasParalel;
                            jmlTotal = 0;
                            jmlHadir = 0;
                        }
                        jmlTotal++;
                        if (item.jawab != null)
                        {
                            if (item.jawab.Hadir != -1)
                                jmlHadir++;

                        }
                    }
                    ret.Add(new ElAbsenSiswaKelasParalelModel
                    {
                        IdUnit = idUnit,
                        IdKelas = IdKelas,
                        IdKelasParalel = idKelasParalel,
                        Unit = tempNamaUnit,
                        Kelas = tempNamaKelas,
                        KelasParalel = tempNamaKelasParalel,
                        JmlTotal = jmlTotal,
                        JmlHadir = jmlHadir,
                        JmlNoHadir = jmlTotal - jmlHadir
                    });
                }

                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElAbsenSiswaKelasParalel", ex);
                return null;
            }
        }
        public List<ElAbsenSiswaMapelModel> GetElAbsenSiswaMapel(int IdUser, int IdKelasParalel, string Tanggal, out string oMessage)
        {
            oMessage = SetMinMaxDate(Tanggal);
            if (!string.IsNullOrEmpty(oMessage)) return null;

            List<ElAbsenSiswaMapelModel> ret = new List<ElAbsenSiswaMapelModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = from materi in conn.GetList<TbKbmMateri>()
                               join mapel in conn.GetList<TbMapel>() on materi.IdMapel equals mapel.IdMapel
                               join kelasParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelasParalel.IdKelasParalel
                               join kelas in conn.GetList<TbKelas>() on kelasParalel.IdKelas equals kelas.IdKelas
                               join unit in conn.GetList<TbUnit>() on kelas.IdUnit equals unit.IdUnit
                               join rombel in conn.GetList<TbKelasRombel>() on kelasParalel.IdKelasParalel equals rombel.IdKelasParalel
                               join jawab in conn.GetList<TbKbmJawaban>() on new { rombel.IdSiswa, materi.IdKbmMateri } equals new { jawab.IdSiswa, jawab.IdKbmMateri } into jawab1
                               from jawab in jawab1.DefaultIfEmpty()
                               where materi.Tanggal.Date >= minDate.Date & materi.Tanggal <= maxDate.Date
                               & kelasParalel.IdKelasParalel == IdKelasParalel
                               select new { unit.IdUnit, kelas.IdKelas, kelasParalel.IdKelasParalel, mapel.IdMapel, TempNama = unit.Nama + kelas.Nama + kelasParalel.Nama + mapel.Nama, mapel.Nama, NamaUnit = unit.Nama, NamaKelas = kelas.Nama, NamaKelasParalel = kelasParalel.Nama, NamaMapel = mapel.Nama, jawab };
                    string tempNama = string.Empty;
                    int idKelas = 0;
                    int idUnit = 0;
                    int idMapel = 0;
                    string tempNamaUnit = string.Empty;
                    string tempNamaKelas = string.Empty;
                    string tempNamaKelasParalel = string.Empty;
                    string tempNamaMapel = string.Empty;

                    int jmlTotal = 0;
                    int jmlHadir = 0;
                    foreach (var item in data.OrderBy(x => x.TempNama))
                    {
                        if (tempNama != item.TempNama)
                        {
                            if (!string.IsNullOrEmpty(tempNama))
                            {

                                ret.Add(new ElAbsenSiswaMapelModel
                                {
                                    IdUnit = idUnit,
                                    IdKelas = idKelas,
                                    IdKelasParalel = IdKelasParalel,
                                    IdMapel = idMapel,
                                    NamaMapel = tempNamaMapel,
                                    Unit = tempNamaUnit,
                                    Kelas = tempNamaKelas,
                                    KelasParalel = tempNamaKelasParalel,
                                    JmlTotal = jmlTotal,
                                    JmlHadir = jmlHadir,
                                    JmlNoHadir = jmlTotal - jmlHadir
                                });
                            }
                            idUnit = item.IdUnit;
                            idKelas = item.IdKelas;
                            idMapel = item.IdMapel;
                            tempNama = item.TempNama;
                            tempNamaUnit = item.NamaUnit;
                            tempNamaKelas = item.NamaKelas;
                            tempNamaKelasParalel = item.NamaKelasParalel;
                            tempNamaMapel = item.NamaMapel;
                            jmlTotal = 0;
                            jmlHadir = 0;
                        }
                        jmlTotal++;
                        if (item.jawab != null)
                        {
                            if (item.jawab.Hadir != -1)
                                jmlHadir++;

                        }
                    }
                    ret.Add(new ElAbsenSiswaMapelModel
                    {
                        IdUnit = idUnit,
                        IdKelas = idKelas,
                        IdKelasParalel = IdKelasParalel,
                        IdMapel = idMapel,
                        Unit = tempNamaUnit,
                        Kelas = tempNamaKelas,
                        KelasParalel = tempNamaKelasParalel,
                        NamaMapel = tempNamaMapel,
                        JmlTotal = jmlTotal,
                        JmlHadir = jmlHadir,
                        JmlNoHadir = jmlTotal - jmlHadir
                    });
                }

                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElAbsenSiswaMapel", ex);
                return null;
            }
        }
        public ElAbsenSiswaMapelListModel GetElAbsenSiswaList(int IdUser, int IdKelasParalel, int IdMapel, string Tanggal, out string oMessage)
        {
            oMessage = SetMinMaxDate(Tanggal);
            if (!string.IsNullOrEmpty(oMessage)) return null;
            ElAbsenSiswaMapelListModel ret = new ElAbsenSiswaMapelListModel
            {
                Hadir = new List<ElAbsenSiswaListModel>(),
                NoHadir = new List<ElAbsenSiswaListModel>()
            };

            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = from materi in conn.GetList<TbKbmMateri>()
                               join user in conn.GetList<TbUser>() on materi.IdPegawai equals user.IdUser
                               join mapel in conn.GetList<TbMapel>() on materi.IdMapel equals mapel.IdMapel
                               join kbmJadwal in conn.GetList<TbKbmJadwal>() on materi.IdKbmJadwal equals kbmJadwal.IdKbmJadwal
                               join kelasParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelasParalel.IdKelasParalel
                               join kelas in conn.GetList<TbKelas>() on kelasParalel.IdKelas equals kelas.IdKelas
                               join unit in conn.GetList<TbUnit>() on kelas.IdUnit equals unit.IdUnit
                               join rombel in conn.GetList<TbKelasRombel>() on kelasParalel.IdKelasParalel equals rombel.IdKelasParalel
                               join siswa in conn.GetList<TbSiswa>() on rombel.IdSiswa equals siswa.IdSiswa
                               join jawab in conn.GetList<TbKbmJawaban>() on new { rombel.IdSiswa, materi.IdKbmMateri } equals new { jawab.IdSiswa, jawab.IdKbmMateri } into jawab1
                               from jawab in jawab1.DefaultIfEmpty()
                               where materi.Tanggal.Date >= minDate.Date & materi.Tanggal <= maxDate.Date
                               & kelasParalel.IdKelasParalel == IdKelasParalel
                               & mapel.IdMapel == IdMapel
                               select new { materi, user, kbmJadwal, mapel, kelasParalel, kelas, unit, siswa, jawab };

                    foreach (var item in data)
                    {
                        ret.InfoJudul = string.Format("Unit {0}, Kelas {1}, Kelas Paralel {2}, Mata Pelajaran {3}", item.unit.Nama, item.kelas.Nama, item.kelasParalel.Nama, item.mapel.Nama);

                        ElAbsenSiswaListModel m = new ElAbsenSiswaListModel
                        {
                            Hari = dateTimeService.GetLongDayName(_languageCode, (int)item.materi.Tanggal.DayOfWeek),
                            JamMulai = item.kbmJadwal.JamMulai.ToString(),
                            JamSelesai = item.kbmJadwal.JamSelesai.ToString(),
                            MataPelajaran = item.mapel.Nama,
                            NamaSiswa = item.siswa.Nama,
                            Nis = item.siswa.Nis
                        };

                        var libur = schService.GetHariLibur(item.materi.Tanggal.ToString("dd-MM-yyyy"), out oMessage);
                        if (string.IsNullOrEmpty(oMessage))
                        {
                            if (libur != null)
                            {
                                m.MataPelajaran = string.Format("LIBUR {0}", libur.Nama);
                            }
                        }
                        if (item.jawab == null)
                        {
                            ret.NoHadir.Add(m);
                        }
                        else
                        {
                            if (item.jawab.Hadir != -1)
                            {

                                ret.Hadir.Add(m);
                            }
                            else
                            {
                                ret.NoHadir.Add(m);
                            }
                        }
                    }
                }

                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElAbsenSiswaList", ex);
                return null;
            }
        }
        #endregion
        #region evaluasi harian
        private string GetSqlEvalHarian()
        {
            string sql = "";
            sql += "  sum(JmlTotal) as JmlTotal, ";
            sql += "  sum(JmlIsi) as JmlIsi, ";
            sql += "  sum(JmlTotal-JmlIsi) as JmlNoIsi ";
            sql += " from (";
            sql += " select d.id_unit as IdUnit, d.nama as Unit, ";
            sql += " 	c.id_kelas as IdKelas, c.Nama as Kelas,";
            sql += " 	b.id_kelas_paralel as IdKelasParalel, b.nama as KelasParalel,";
            sql += " 	count(9) as JmlTotal,";
            sql += " 	sum(case when e.id_siswa is not null then 1 else 0 end) as JmlIsi";
            sql += " from kbm.tb_kelas_rombel a";
            sql += " join kbm.tb_kelas_paralel b on a.id_kelas_paralel = b.id_kelas_paralel";
            sql += " join kbm.tb_kelas c on b.id_kelas = c.id_kelas";
            sql += " join lmg.tb_unit d on c.id_unit = d.id_unit";
            sql += " left join ";
            sql += " 	(select distinct id_siswa from kbm.tb_kbm_evaluasi_harian ";
            sql += string.Format(" 	 where tanggal between '{0}' and '{1}') e on a.id_siswa = e.id_siswa", minDate.ToString("yyyy-MM-dd"), maxDate.ToString("yyyy-MM-dd"));
            sql += " where 1 = 1";
            return sql;

        }
        public ElEvalHarianModel GetElEvalHarian(int IdUser, string Tanggal, out string oMessage)
        {
            oMessage = SetMinMaxDate(Tanggal);
            if (!string.IsNullOrEmpty(oMessage)) return null;
            ElEvalHarianModel ret = new ElEvalHarianModel
            {
                Periode = new ElTanggalModel
                {
                    TanggalAwal = minDate.ToString("dd-MM-yyyy"),
                    TanggalAkhir = maxDate.ToString("dd-MM-yyyy")
                },
                Units = new List<ElEvalHarianUnitModel>()
            };
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    oMessage = schService.GetLevelJabatanUnitPegawai(conn, IdUser, out int LevelJabatan, out int IdUnit);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    bool canReadAllUnit = CanReadAllUnit(LevelJabatan);

                    string sql = " select IdUnit,  Unit, ";
                    sql += GetSqlEvalHarian();
                    if (!canReadAllUnit)
                        sql += string.Format(" and d.id_unit = {0}", IdUnit);
                    sql += " group by d.id_unit, d.nama, c.id_kelas, c.nama, b.id_kelas_paralel, b.nama";
                    sql += " ) as tb";
                    sql += " group by IdUnit, Unit ";
                    sql += " order by IdUnit, Unit";
                    var data = conn.Query<ElEvalHarianUnitModel>(sql).ToList();
                    foreach (var item in data.OrderBy(x => x.IdUnit))
                    {
                        ret.Units.Add(new ElEvalHarianUnitModel
                        {
                            IdUnit = item.IdUnit,
                            Unit = item.Unit,
                            JmlTotal = item.JmlTotal,
                            JmlIsi = item.JmlIsi,
                            JmlNoIsi = item.JmlNoIsi
                        });
                    }
                    if (ret.Units.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElEvalHarian", ex);
                return null;
            }
        }
        public List<ElEvalHarianKelasModel> GetElEvalHarianKelas(int IdUser, int IdUnit, string Tanggal, out string oMessage)
        {
            oMessage = SetMinMaxDate(Tanggal);
            if (!string.IsNullOrEmpty(oMessage)) return null;

            List<ElEvalHarianKelasModel> ret = new List<ElEvalHarianKelasModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    string sql = " select IdUnit, Unit, IdKelas, Kelas,";
                    sql += GetSqlEvalHarian();
                    sql += string.Format(" and d.id_unit = {0}", IdUnit);
                    sql += " group by d.id_unit, d.nama, c.id_kelas, c.nama, b.id_kelas_paralel, b.nama";
                    sql += " ) as tb";
                    sql += " group by IdUnit, Unit, IdKelas, Kelas";
                    sql += " order by IdUnit, Unit, IdKelas, Kelas";
                    var data = conn.Query<ElEvalHarianKelasModel>(sql).ToList();

                    foreach (var item in data)
                    {
                        ret.Add(new ElEvalHarianKelasModel
                        {
                            IdUnit = item.IdUnit,
                            IdKelas = item.IdKelas,
                            Unit = item.Unit,
                            Kelas = item.Kelas,
                            JmlTotal = item.JmlTotal,
                            JmlIsi = item.JmlIsi,
                            JmlNoIsi = item.JmlNoIsi
                        });
                    }

                    if (ret.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElEvalHarianKelas", ex);
                return null;
            }
        }
        public List<ElEvalHarianKelasParalelModel> GetElEvalHarianKelasParalel(int IdUser, int IdKelas, string Tanggal, out string oMessage)
        {
            oMessage = SetMinMaxDate(Tanggal);
            if (!string.IsNullOrEmpty(oMessage)) return null;

            List<ElEvalHarianKelasParalelModel> ret = new List<ElEvalHarianKelasParalelModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    string sql = " select IdUnit, Unit, IdKelas, Kelas, IdKelasParalel, KelasParalel,";
                    sql += GetSqlEvalHarian();
                    sql += string.Format(" and c.id_kelas = {0}", IdKelas);
                    sql += " group by d.id_unit, d.nama, c.id_kelas, c.nama, b.id_kelas_paralel, b.nama";
                    sql += " ) as tb";
                    sql += " group by IdUnit, Unit, IdKelas, Kelas, IdKelasParalel, KelasParalel";
                    sql += " order by IdUnit, Unit, IdKelas, Kelas, IdKelasParalel, KelasParalel";
                    var data = conn.Query<ElEvalHarianKelasParalelModel>(sql).ToList();


                    foreach (var item in data)
                    {
                        ret.Add(new ElEvalHarianKelasParalelModel
                        {
                            IdUnit = item.IdUnit,
                            IdKelas = item.IdKelas,
                            IdKelasParalel = item.IdKelasParalel,
                            Unit = item.Unit,
                            Kelas = item.Kelas,
                            KelasParalel = item.KelasParalel,
                            JmlTotal = item.JmlTotal,
                            JmlIsi = item.JmlIsi,
                            JmlNoIsi = item.JmlNoIsi
                        });
                    }
                    if (ret.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElEvalHarianKelasParalel", ex);
                return null;
            }
        }
        public List<ElEvalHarianListModel> GetElEvalHarianList(int IdUser, int IdKelasParalel, string Tanggal, out string oMessage)
        {
            oMessage = SetMinMaxDate(Tanggal);
            if (!string.IsNullOrEmpty(oMessage)) return null;

            List<ElEvalHarianListModel> ret = new List<ElEvalHarianListModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = from unit in conn.GetList<TbUnit>()
                               join kelas in conn.GetList<TbKelas>() on unit.IdUnit equals kelas.IdUnit
                               join kelasParalel in conn.GetList<TbKelasParalel>() on kelas.IdKelas equals kelasParalel.IdKelas
                               join rombel in conn.GetList<TbKelasRombel>() on kelasParalel.IdKelasParalel equals rombel.IdKelasParalel
                               join siswa in conn.GetList<TbSiswa>() on rombel.IdSiswa equals siswa.IdSiswa
                               join mut in conn.GetList<TbKbmEvaluasiHarian>()
                                    .Where(x => x.Tanggal.Date >= minDate.Date
                                    & x.Tanggal.Date <= maxDate.Date) on siswa.IdSiswa equals mut.IdSiswa into mut1
                               from mut in mut1.DefaultIfEmpty()
                               where kelasParalel.IdKelasParalel == IdKelasParalel
                               select new
                               {
                                   NamaUnit = unit.Nama,
                                   NamaKelas = kelas.Nama,
                                   NamaKelasParalel = kelasParalel.Nama,
                                   rombel.IdSiswa,
                                   siswa.Nis,
                                   siswa.Nama,
                                   mut
                               };

                    int idSiswa = 0;
                    string namaUnit = "";
                    string namaKelas = "";
                    string namaKelasParalel = "";
                    string nis = "";
                    string nama = "";
                    int jml1 = 0;
                    int jml2 = 0;
                    int jml3 = 0;
                    int jml4 = 0;
                    int jml5 = 0;
                    int jml6 = 0;
                    int jml7 = 0;
                    ElEvalHarianListModel m = new ElEvalHarianListModel();
                    foreach (var item in data.OrderBy(x => x.IdSiswa))
                    {
                        if (idSiswa != item.IdSiswa)
                        {
                            m = new ElEvalHarianListModel
                            {
                                IdSiswa = idSiswa,
                                Unit = namaUnit,
                                Kelas = namaKelas,
                                KelasParalel = namaKelasParalel,
                                Nis = nis,
                                NamaSiswa = nama,
                                Minggu = jml1.ToString(),
                                Senin = jml2.ToString(),
                                Selasa = jml3.ToString(),
                                Rabu = jml4.ToString(),
                                Kamis = jml5.ToString(),
                                Jumat = jml6.ToString(),
                                Sabtu = jml7.ToString()
                            };
                            if (idSiswa != 0)
                                ret.Add(m);
                            idSiswa = item.IdSiswa;
                            namaUnit = item.NamaUnit;
                            namaKelas = item.NamaKelas;
                            namaKelasParalel = item.NamaKelasParalel;
                            nis = item.Nis;
                            nama = item.Nama;
                            jml1 = 0;
                            jml2 = 0;
                            jml3 = 0;
                            jml4 = 0;
                            jml5 = 0;
                            jml6 = 0;
                            jml7 = 0;
                        }
                        if (item.mut != null)
                        {
                            switch ((int)item.mut.Tanggal.DayOfWeek)
                            {
                                case 0:
                                    if (item.mut.IsDone == "Y")
                                        jml1++;
                                    break;
                                case 1:
                                    if (item.mut.IsDone == "Y")
                                        jml2++;
                                    break;
                                case 2:
                                    if (item.mut.IsDone == "Y")
                                        jml3++;
                                    break;
                                case 3:
                                    if (item.mut.IsDone == "Y")
                                        jml4++;
                                    break;
                                case 4:
                                    if (item.mut.IsDone == "Y")
                                        jml5++;
                                    break;
                                case 5:
                                    if (item.mut.IsDone == "Y")
                                        jml6++;
                                    break;
                                case 6:
                                    if (item.mut.IsDone == "Y")
                                        jml7++;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    m = new ElEvalHarianListModel
                    {
                        IdSiswa = idSiswa,
                        Unit = namaUnit,
                        Kelas = namaKelas,
                        KelasParalel = namaKelasParalel,
                        Nis = nis,
                        NamaSiswa = nama,
                        Minggu = jml1.ToString(),
                        Senin = jml2.ToString(),
                        Selasa = jml3.ToString(),
                        Rabu = jml4.ToString(),
                        Kamis = jml5.ToString(),
                        Jumat = jml6.ToString(),
                        Sabtu = jml7.ToString()
                    };
                    ret.Add(m);
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElEvalHarianList", ex);
                return null;
            }
        }
        public ElEvalHarianSiswaModel GetElEvalHarianSiswa(int IdUser, int IdSiswa, string Tanggal, int HariKe, out string oMessage)
        {
            oMessage = SetMinMaxDate(Tanggal);
            if (!string.IsNullOrEmpty(oMessage)) return null;

            ElEvalHarianSiswaModel ret = new ElEvalHarianSiswaModel
            {
                Data = new List<ElEvalHarianSiswaListModel>()
            };

            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbSiswa = conn.Get<TbSiswa>(IdSiswa);
                    ret.InfoJudul = string.Format("Mutaba'ah Ananda {0} tanggal {1}", tbSiswa.Nama, minDate.AddDays(HariKe).ToString("dd-MM-yyyy"));

                    var dataSiswa = from a in conn.GetList<TbKbmEvaluasiHarian>()
                                    where a.Tanggal.Date == minDate.AddDays(HariKe).Date
                                    & a.IdSiswa == IdSiswa
                                    select a;
                    var data = from a in conn.GetList<TbJenisEvaluasiHarian>()
                               join b in dataSiswa on a.IdJenisEvaluasiHarian equals b.IdJenisEvaluasiHarian into b1
                               from b in b1.DefaultIfEmpty()
                               select new { a, b };
                    foreach (var item in data)
                    {
                        ElEvalHarianSiswaListModel m = new ElEvalHarianSiswaListModel
                        {
                            NamaMutabaah = item.a.Nama
                        };
                        m.YaTidak = item.b == null ? "" : item.b.IsDone;
                        m.Catatan = item.b == null ? "" : item.b.Catatan;
                        ret.Data.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetElEvalHarianSiswa", ex);
                return null;
            }
        }
        public List<SheetLaporanElearningProgressPegawai> GetLaporanElearningProgressPegawai(int IdUser, string TanggalAwal, string TanggalAkhir, int IdUnit, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = (from _tbMateri in conn.GetList<TbKbmMateri>()
                                join _tbKelasParalel in conn.GetList<TbKelasParalel>() on _tbMateri.IdKelasParalel equals _tbKelasParalel.IdKelasParalel
                                join _tbKelas in conn.GetList<TbKelas>() on _tbKelasParalel.IdKelas equals _tbKelas.IdKelas
                                join _tbUnit in conn.GetList<TbUnit>() on _tbKelas.IdUnit equals _tbUnit.IdUnit
                                join _tbPegawai in conn.GetList<TbPegawai>() on _tbMateri.IdPegawai equals _tbPegawai.IdPegawai
                                join _tbUserProfile in conn.GetList<TbUser>() on _tbMateri.IdPegawai equals _tbUserProfile.IdUser
                                join _nilaiHadir in (from a in conn.GetList<TbKbmJawaban>()
                                                     select new
                                                     {
                                                         a.IdKbmMateri,
                                                         JmlNilai = (a.NilaiAngka != null || !string.IsNullOrEmpty(a.NilaiHuruf)) ? 1 : 0,
                                                         JmlAbsen = a.Hadir >= 0 ? 1 : 0
                                                     }
                                                     ).ToList().GroupBy(x => x.IdKbmMateri)
                                                     .Select(g => new { IdKbmMateri = g.Key, SumAbsen = g.Sum(x => x.JmlAbsen), SumNilai = g.Sum(x => x.JmlNilai) }) on _tbMateri.IdKbmMateri equals _nilaiHadir.IdKbmMateri into x1
                                from _nilaiHadir in x1.DefaultIfEmpty()
                                where _tbMateri.Tanggal.Date >= dateTimeService.StrToDateTime(TanggalAwal).Date
                                & _tbMateri.Tanggal.Date <= dateTimeService.StrToDateTime(TanggalAkhir).Date
                                & _tbUnit.IdUnit == IdUnit
                                select new { _tbMateri, _tbUnit, _tbPegawai, _tbUserProfile, Nilai = _nilaiHadir == null ? 0 : _nilaiHadir.SumNilai > 0 ? 1 : 0, Absen = _nilaiHadir == null ? 0 : _nilaiHadir.SumAbsen > 0 ? 1 : 0 }).ToList();

                    List<SheetLaporanElearningProgressPegawai> ret = new List<SheetLaporanElearningProgressPegawai>();
                    var pegawais = data.GroupBy(x => x._tbPegawai).Select(x => x.First()).ToList();
                    foreach (var pegawai in pegawais.OrderBy(x => x._tbUserProfile.FirstName))
                    {
                        SheetLaporanElearningProgressPegawai mod = new SheetLaporanElearningProgressPegawai
                        {
                            Nip = pegawai._tbPegawai.Nip,
                            NamaPegawai = userAppService.SetFullName(pegawai._tbUserProfile.FirstName, pegawai._tbUserProfile.MiddleName, pegawai._tbUserProfile.LastName),
                            UnitKerja = pegawai._tbUnit.Nama,
                            Materi = 0,
                            Soal = 0,
                            Menilai = 0,
                            KehadiranSiswa = 0
                        };
                        int jmlMateri = data.Where(x => x._tbPegawai == pegawai._tbPegawai).Count();
                        int pctMateri = data.Where(x => x._tbPegawai == pegawai._tbPegawai & !string.IsNullOrEmpty(x._tbMateri.Judul)).Count();
                        int pctSoal = data.Where(x => x._tbPegawai == pegawai._tbPegawai & !string.IsNullOrEmpty(x._tbMateri.DeskripsiTugas)).Count();
                        int pctMenilai = data.Where(x => x._tbPegawai == pegawai._tbPegawai & x.Nilai > 0).Count();
                        int pctKehadiranSiswa = data.Where(x => x._tbPegawai == pegawai._tbPegawai & x.Absen > 0).Count();
                        if (jmlMateri > 0)
                        {
                            mod.Materi = 100 * pctMateri / jmlMateri;
                            mod.Soal = 100 * pctSoal / jmlMateri;
                            mod.Menilai = 100 * pctMenilai / jmlMateri;
                            mod.KehadiranSiswa = 100 * pctKehadiranSiswa / jmlMateri;
                        }
                        ret.Add(mod);
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetLaporanElearningProgressPegawai", ex);
                return null;
            }
        }
        #endregion
    }
}
