﻿using Dapper;
using Swg.Entities.Kbm;
using Swg.Entities.Keu;
using Swg.Entities.Lmg;
using Swg.Entities.Public;
using Swg.Entities.Ref;
using Swg.Models;
using Swg.Models.Constants;
using Swg.Sch.Shared.Constants;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Sch.Services
{
    public class KeuRenPenService : IKeuRenPenService
    {
        private readonly string ServiceName = "Swg.Sch.Services.KeuRenPenService.";
        private readonly string _languageCode = "id";
        KeuHakAksesModel hakAkses = new KeuHakAksesModel
        {
            CanAdd = false,
            CanApprove = false,
            CanDelete = false,
            CanDownload = false,
            CanEdit = false,
            CanRead = false,
            CanRelease = false
        };
        private int[] HakAksesRenPens = new int[] { 11, 12, 13, 14, 15, 16 };
        private readonly ICommonService commonService;
        private readonly IDateTimeService dateTimeService;

        public KeuRenPenService(
            ICommonService CommonService,
            IDateTimeService DateTimeService)
        {
            commonService = CommonService;
            dateTimeService = DateTimeService;

        }
        private void CekAkses(IDbConnection conn, int IdUser)
        {
            var tbAksesRkas = from a in conn.GetList<TbJenisHakAksesKeu>()
                              join b in conn.GetList<TbJenisJabatanHakAksesKeu>() on a.IdJenisHakAksesKeu equals b.IdJenisHakAksesKeu
                              join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                              join d in conn.GetList<TbPegawai>() on c.IdJenisJabatan equals d.IdJenisJabatan
                              where d.IdPegawai == IdUser
                              & HakAksesRenPens.Contains(a.IdJenisHakAksesKeu)
                              select new { a, b, c };
            foreach (var item in tbAksesRkas)
            {
                if (item.a.NamaSingkat == "CanRead")
                {
                    hakAkses.CanRead = true;
                    hakAkses.CanDownload = hakAkses.CanRead;
                }
                if (item.a.NamaSingkat == "CanAdd")
                {
                    hakAkses.CanAdd = true;
                }
                if (item.a.NamaSingkat == "CanEdit")
                {
                    hakAkses.CanEdit = true;
                }
                if (item.a.NamaSingkat == "CanApprove")
                {
                    hakAkses.CanApprove = true;
                }
                if (item.a.NamaSingkat == "CanDelete")
                {
                    hakAkses.CanDelete = true;
                }
                if (item.a.NamaSingkat == "CanRelease")
                {
                    hakAkses.CanRelease = true;
                }
            }
        }
        private int GetNextStatus(int Status)
        {
            int nextStatus = Status;
            if (Status == JenisStatusRenPenConstant.Initial)
            {
                nextStatus = JenisStatusRenPenConstant.Baru;
            }
            else if (Status == JenisStatusRenPenConstant.Baru)
            {
                nextStatus = JenisStatusRenPenConstant.DisetujuiManKeu;
            }
            else if (Status == JenisStatusRenPenConstant.DisetujuiManKeu)
            {
                nextStatus = JenisStatusRenPenConstant.DisetujuiDirektur;
            }
            else if (Status == JenisStatusRenPenConstant.DisetujuiDirektur)
            {
                nextStatus = JenisStatusRenPenConstant.Disahkan;
            }
            return nextStatus;
        }
        private int GetBeforeStatus(int Status)
        {
            int beforeStatus = Status;
            if (Status == JenisStatusRenPenConstant.Disahkan)
            {
                beforeStatus = JenisStatusRenPenConstant.DisetujuiDirektur;
            }
            else if (Status == JenisStatusRenPenConstant.DisetujuiDirektur)
            {
                beforeStatus = JenisStatusRenPenConstant.DisetujuiManKeu;
            }
            else if (Status == JenisStatusRenPenConstant.DisetujuiManKeu)
            {
                beforeStatus = JenisStatusRenPenConstant.Baru;
            }
            else if (Status == JenisStatusRenPenConstant.Baru)
            {
                beforeStatus = JenisStatusRenPenConstant.Initial;
            }

            return beforeStatus;
        }
        public string InitRenPen(int IdUser, int IdTahunAjaran)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbPegawaiUnit = (from a in conn.GetList<TbUnitPegawai>()
                                             join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                             join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                             where a.IdPegawai == IdUser
                                             select new { a, b, c }).FirstOrDefault();
                        if (tbPegawaiUnit == null)
                            return "Pengguna bukan pegawai";
                        int[] idUnits = new int[] { UnitConstant.AKT, UnitConstant.FNC, UnitConstant.DIR };
                        if (!idUnits.Contains(tbPegawaiUnit.a.IdUnit))
                            return "tidak berhak melakukan inisialisasi keuangan";

                        var units = conn.GetList<TbUnit>();
                        foreach (var item in units)
                        {
                            TbUnitRenpen tbUnitRenpen = (from a in conn.GetList<TbUnitRenpen>()
                                              where a.IdUnit == item.IdUnit
                                              & a.IdTahunAjaran == IdTahunAjaran
                                              select a).FirstOrDefault();
                            if (tbUnitRenpen == null)
                            {
                                tbUnitRenpen = new TbUnitRenpen
                                {
                                    IdTahunAjaran = IdTahunAjaran,
                                    IdUnit = item.IdUnit,
                                    Rp = 0,
                                    Status = JenisStatusRenPenConstant.Initial
                                };
                                var idUnitRenPen = conn.Insert(tbUnitRenpen);

                                TbUnitRenpenStatus tbUnitRenpenStatus = new TbUnitRenpenStatus
                                {
                                    IdUnitRenpen = (int)idUnitRenPen,
                                    Status = JenisStatusRenPenConstant.Initial,
                                    CreatedBy = IdUser,
                                    CreatedDate = dateTimeService.GetCurrdate()
                                };
                                conn.Insert(tbUnitRenpenStatus);
                            }

                            TbUnitProkeg tbUnitProkeg = (from a in conn.GetList<TbUnitProkeg>()
                                                   where a.IdUnit == item.IdUnit
                                                   & a.IdTahunAjaran == IdTahunAjaran
                                                   select a).FirstOrDefault();
                            if (tbUnitProkeg == null)
                            {
                                tbUnitProkeg = new TbUnitProkeg
                                {
                                    IdTahunAjaran = IdTahunAjaran,
                                    IdUnit = item.IdUnit,
                                    Rp = 0,
                                    Status = JenisStatusRenPenConstant.Initial
                                };
                                var idUnitProKeg = conn.Insert(tbUnitProkeg);
                                var tbUnitProkegStatus = (from a in conn.GetList<TbUnitProkegStatus>()
                                          where a.IdUnitProkeg == (int)idUnitProKeg
                                          & a.Status == JenisStatusProgKegConstant.Initial
                                          select a).FirstOrDefault();
                                if (tbUnitProkegStatus == null)
                                {
                                    tbUnitProkegStatus = new TbUnitProkegStatus
                                    {
                                        IdUnitProkeg = (int)idUnitProKeg,
                                        CreatedBy = IdUser,
                                        CreatedDate = dateTimeService.GetCurrdate(),
                                        Status = JenisStatusProgKegConstant.Initial,
                                        StatusProses = StatusProsesKeuanganConstant.New
                                    };
                                    conn.Insert(tbUnitProkegStatus);
                                }
                                var tbProgramUnits = from a in conn.GetList<TbProgramUnit>()
                                                    where a.IdUnit == item.IdUnit
                                                    select a;
                                foreach (var programWajib in tbProgramUnits)
                                {
                                    var tbProgramKegiatans = from a in conn.GetList<TbProgramKegiatan>()
                                                         where a.IdProgram == programWajib.IdProgram
                                                         select a;
                                    foreach (var kegiatanWajib in tbProgramKegiatans)
                                    {
                                        TbUnitProkegWajib tbUnitProkegWajib = new TbUnitProkegWajib
                                        {
                                            IdUnitProkeg = (int)idUnitProKeg,
                                            IdKegiatan = kegiatanWajib.IdKegiatan,
                                            Tanggal = new DateTime(1900,1,1),
                                            Rp = 0
                                        };
                                        conn.Insert(tbUnitProkegWajib);
                                    }
                                }
                            }
                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "InitRenPen", ex);
            }
        }
        public List<ComboModel> GetCoas(out string oMessage)
        {
            oMessage = string.Empty;
            List<ComboModel> ret = new List<ComboModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var dt = from a in conn.GetList<TbCoa>()
                             where a.IdJenisAkun == JenisAkunConstant.Pratama
                             & a.Kode.Substring(0, 1) == "4"
                             select a;
                    if (dt == null || dt.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    foreach (var item in dt)
                    {
                        ret.Add(new ComboModel
                        {
                            Id = item.IdCoa,
                            Kode = item.Kode,
                            Nama = item.Nama
                        });
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetCoas", ex);
                return null;

                throw;
            }

        }
        public List<KeuUnitRenPenListModel> GetRenPens(int IdUser, int IdTahunAjaran, out string oMessage)
        {
            oMessage = string.Empty;
            List<KeuUnitRenPenListModel> ret = new List<KeuUnitRenPenListModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbPegawaiUnit = (from a in conn.GetList<TbUnitPegawai>()
                                         join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                         join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                         where a.IdPegawai == IdUser
                                         select new { a, b, c }).FirstOrDefault();
                    if (tbPegawaiUnit == null) { oMessage = "Pengguna bukan pegawai"; return null; };

                    var dt = from a in conn.GetList<TbUnitRenpen>()
                             join b in conn.GetList<TbTahunAjaran>() on a.IdTahunAjaran equals b.IdTahunAjaran
                             join c in conn.GetList<TbUnit>() on a.IdUnit equals c.IdUnit
                             where a.IdTahunAjaran == IdTahunAjaran
                             select new { a, b, c };
                    if(dt==null || dt.Count()==0) { oMessage = "data tidak ada"; return null; }
                    CekAkses(conn, IdUser);

                    foreach (var item in dt.OrderBy(x=>x.c.IdUnit))
                    {
                        if (tbPegawaiUnit.a.IdUnit != item.a.IdUnit)
                        {
                            hakAkses = new KeuHakAksesModel
                            {
                                CanAdd = false,
                                CanApprove = false,
                                CanDelete = false,
                                CanDownload = false,
                                CanEdit = false,
                                CanRead = false,
                                CanRelease = false
                            };

                            if (
                                tbPegawaiUnit.a.IdUnit == UnitConstant.AKT ||
                                tbPegawaiUnit.a.IdUnit == UnitConstant.FNC ||
                                tbPegawaiUnit.a.IdUnit == UnitConstant.DIR
                                )
                            {
                                CekAkses(conn, IdUser);
                            }
                        }
                        KeuUnitRenPenListModel m = new KeuUnitRenPenListModel
                        {
                            IdUnitRenPen = item.a.IdUnitRenpen,
                            Rp = item.a.Rp,
                            Status = JenisStatusRenPenConstant.Dict[item.a.Status],
                            TahunAjaran = item.b.Nama,
                            Unit = item.c.Nama,
                            Aksis = new List<string>()
                        };
                        if (hakAkses.CanRead) m.Aksis.Add("View");
                        if (hakAkses.CanDownload) m.Aksis.Add("Download");

                        if (item.a.Status == JenisStatusRenPenConstant.Baru)
                        {
                            if (tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.ManKeu
                                || tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                                if (hakAkses.CanApprove) m.Aksis.Add("Approve Mgr Keu");
                        }
                        else if (item.a.Status == JenisStatusRenPenConstant.DisetujuiManKeu)
                        {
                            if (tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.Dir
                                || tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                                if (hakAkses.CanApprove) m.Aksis.Add("Approve Direktur");
                        }
                        else if (item.a.Status == JenisStatusRenPenConstant.DisetujuiDirektur)
                        {
                            if (tbPegawaiUnit.b.IdJenisJabatan == JenisJabatanPegawaiContant.AdmDir)
                                if (hakAkses.CanRelease) m.Aksis.Add("Set Sah");
                        }
                        if (hakAkses.CanRead)
                            ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetRenPens", ex);
                return null;
            }
        }
        public List<KeuUnitRenPenCoaListModel> GetRenPenCoas(int IdUser, int IdUnitRenPen, out string oMessage)
        {
            oMessage = string.Empty;
            List<KeuUnitRenPenCoaListModel> ret = new List<KeuUnitRenPenCoaListModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var dt = from a in conn.GetList<TbUnitRenpenCoa>()
                             join b in conn.GetList<TbCoa>() on a.IdCoa equals b.IdCoa
                             where a.IdUnitRenpen == IdUnitRenPen
                             select new { a, b };
                    if (dt == null || dt.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    CekAkses(conn, IdUser);
                    foreach (var item in dt.OrderBy(x=>x.b.Kode))
                    {
                        KeuUnitRenPenCoaListModel m = new KeuUnitRenPenCoaListModel
                        {
                            IdUnitRenPen = item.a.IdUnitRenpen,
                            Rp = item.a.Rp,
                            IdCoa = item.a.IdCoa,
                            KodeCoa = item.b.Kode,
                            NamaCoa = item.b.Nama,
                            Aksis = new List<string>()
                        };
                        if (hakAkses.CanRead) m.Aksis.Add("View");
                        if (hakAkses.CanEdit) m.Aksis.Add("Edit");
                        if (hakAkses.CanDelete) m.Aksis.Add("Hapus");
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetRenPenCoas", ex);
                return null;
            }

        }
        public KeuUnitRenPenCoaModel GetRenPenCoa(int IdUnitRenPen, int IdCoa, out string oMessage)
        {
            oMessage = string.Empty;
            List<KeuUnitRenPenCoaModel> ret = new List<KeuUnitRenPenCoaModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var item = (from a in conn.GetList<TbUnitRenpenCoa>()
                             join b in conn.GetList<TbCoa>() on a.IdCoa equals b.IdCoa
                             where a.IdUnitRenpen == IdUnitRenPen
                             & a.IdCoa == IdCoa
                             select new { a, b }).FirstOrDefault();
                    if (item == null ) { oMessage = "data tidak ada"; return null; }
                    return new KeuUnitRenPenCoaModel
                    {
                        IdUnitRenPen = item.a.IdUnitRenpen,
                        Rp = item.a.Rp,
                        IdCoa = item.a.IdCoa,
                        KodeCoa = item.b.Kode,
                        NamaCoa = item.b.Nama
                    };
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetRenPenCoas", ex);
                return null;
            }
        }
        public string AddData(int IdUser, int IdUnitRenPen, int IdCoa, double Rp)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    if (!hakAkses.CanAdd) return "tidak berhak nambah data";
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbPegawaiUnit = (from a in conn.GetList<TbUnitPegawai>()
                                             join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                             join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                             where a.IdPegawai == IdUser
                                             select new { a, b, c }).FirstOrDefault();
                        if (tbPegawaiUnit == null)
                            return "Pengguna bukan pegawai";
                        int[] idUnits = new int[] { UnitConstant.AKT, UnitConstant.FNC, UnitConstant.DIR };
                        if (!idUnits.Contains(tbPegawaiUnit.a.IdUnit))
                            return "tidak berhak nambah data";

                        var tbUnitRenPen = conn.Get<TbUnitRenpen>(IdUnitRenPen);
                        if (tbUnitRenPen == null) return "data rencana pendapatan tidak ada";
                        int[] statuss = new int[] { JenisStatusRenPenConstant.Initial, JenisStatusRenPenConstant.Baru };
                        if (!statuss.Contains(tbUnitRenPen.Status))
                            return string.Format("status data sudah {0}", JenisStatusRenPenConstant.Dict[tbUnitRenPen.Status]);

                        tbUnitRenPen.Rp += Rp;
                        tbUnitRenPen.Status = JenisStatusRenPenConstant.Baru;
                        var tbUnitRenPenCoa = (from a in conn.GetList<TbUnitRenpenCoa>()
                                               where a.IdUnitRenpen == IdUnitRenPen
                                               & a.IdCoa == IdCoa
                                               select a).FirstOrDefault();
                        if (tbUnitRenPenCoa != null) return "data sudah ada";
                        tbUnitRenPenCoa = new TbUnitRenpenCoa
                        {
                            IdCoa = IdCoa,
                            IdUnitRenpen = IdUnitRenPen,
                            Rp = Rp
                        };
                        TbUnitRenpenStatus tbUnitRenpenStatus = (from a in conn.GetList<TbUnitRenpenStatus>()
                                                                 where a.IdUnitRenpen == tbUnitRenPen.IdUnitRenpen
                                                                 & a.Status == JenisStatusRenPenConstant.Baru
                                                                 select a).FirstOrDefault();
                        if (tbUnitRenpenStatus == null)
                        {
                            tbUnitRenpenStatus = new TbUnitRenpenStatus
                            {
                                IdUnitRenpen = tbUnitRenPen.IdUnitRenpen,
                                Status = JenisStatusRenPenConstant.Baru,
                                CreatedBy = IdUser,
                                CreatedDate = dateTimeService.GetCurrdate(),
                                StatusProses = StatusProsesKeuanganConstant.New
                            };
                            conn.Insert(tbUnitRenpenStatus);
                        }
                        else
                        {
                            tbUnitRenpenStatus.UpdatedBy = IdUser;
                            tbUnitRenpenStatus.UpdatedDate = dateTimeService.GetCurrdate();
                            conn.Update(tbUnitRenpenStatus);
                        }
                        conn.Insert(tbUnitRenPenCoa);
                        conn.Update(tbUnitRenPen);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddData", ex);
            }
        }
        public string EditData(int IdUser, int IdUnitRenPen, int IdCoa, double Rp)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    if (!hakAkses.CanEdit) return "tidak berhak edit data";
                    int[] statuss = new int[] { JenisStatusRenPenConstant.Initial, JenisStatusRenPenConstant.Baru };
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbPegawaiUnit = (from a in conn.GetList<TbUnitPegawai>()
                                             join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                             join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                             where a.IdPegawai == IdUser
                                             select new { a, b, c }).FirstOrDefault();
                        if (tbPegawaiUnit == null)
                            return "Pengguna bukan pegawai";
                        int[] idUnits = new int[] { UnitConstant.AKT, UnitConstant.FNC, UnitConstant.DIR };
                        if (!idUnits.Contains(tbPegawaiUnit.a.IdUnit))
                            return "tidak berhak edit data";

                        var tbUnitRenPen = conn.Get<TbUnitRenpen>(IdUnitRenPen);
                        if (tbUnitRenPen == null) return "data rencana pendapatan tidak ada";
                        if (!statuss.Contains(tbUnitRenPen.Status))
                            return string.Format("status data sudah {0}", JenisStatusRenPenConstant.Dict[tbUnitRenPen.Status]);

                        var tbUnitRenPenCoa = (from a in conn.GetList<TbUnitRenpenCoa>()
                                               where a.IdUnitRenpen == IdUnitRenPen
                                               & a.IdCoa == IdCoa
                                               select a).FirstOrDefault();
                        if (tbUnitRenPenCoa == null) return "data tidak ada";
                        tbUnitRenPen.Rp -= tbUnitRenPenCoa.Rp;
                        tbUnitRenPen.Rp += Rp;


                        tbUnitRenPenCoa.Rp = Rp;
                        conn.Update(tbUnitRenPenCoa);
                        conn.Update(tbUnitRenPen);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddData", ex);
            }
        }
        public string DeleteData(int IdUser, int IdUnitRenPen, int IdCoa)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    if (!hakAkses.CanDelete) return "tidak berhak hapus data";
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbPegawaiUnit = (from a in conn.GetList<TbUnitPegawai>()
                                             join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                             join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                             where a.IdPegawai == IdUser
                                             select new { a, b, c }).FirstOrDefault();
                        if (tbPegawaiUnit == null)
                            return "Pengguna bukan pegawai";
                        int[] idUnits = new int[] { UnitConstant.AKT, UnitConstant.FNC, UnitConstant.DIR };
                        if (!idUnits.Contains(tbPegawaiUnit.a.IdUnit))
                            return "tidak berhak hapus data";

                        var tbUnitRenPen = conn.Get<TbUnitRenpen>(IdUnitRenPen);
                        if (tbUnitRenPen == null) return "data rencana pendapatan tidak ada";
                        int[] statuss = new int[] { JenisStatusRenPenConstant.Initial, JenisStatusRenPenConstant.Baru };
                        if (!statuss.Contains(tbUnitRenPen.Status))
                            return string.Format("status data sudah {0}", JenisStatusRenPenConstant.Dict[tbUnitRenPen.Status]);

                        var tbUnitRenPenCoa = (from a in conn.GetList<TbUnitRenpenCoa>()
                                               where a.IdUnitRenpen == IdUnitRenPen
                                               & a.IdCoa == IdCoa
                                               select a).FirstOrDefault();
                        if (tbUnitRenPenCoa == null) return "data tidak ada";
                        tbUnitRenPen.Rp -= tbUnitRenPenCoa.Rp;


                        conn.Delete(tbUnitRenPenCoa);
                        conn.Update(tbUnitRenPen);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddData", ex);
            }
        }
        private string ApproveData(int IdUser, int IdUnitRenPen, int StatusProses, string Catatan, int Status)
        {
            try
            {

                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    CekAkses(conn, IdUser);
                    var tbPegawaiUnit = (from a in conn.GetList<TbUnitPegawai>()
                                         join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                         join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                         where a.IdPegawai == IdUser
                                         select new { a, b, c }).FirstOrDefault();
                    if (tbPegawaiUnit == null)
                        return "Pengguna bukan pegawai";
                    int[] idUnits = new int[] { UnitConstant.AKT, UnitConstant.FNC, UnitConstant.DIR };
                    if (!idUnits.Contains(tbPegawaiUnit.a.IdUnit))
                        return "tidak berhak approve data";
                    if (hakAkses.CanApprove)
                        if (tbPegawaiUnit.b.IdJenisJabatan != JenisJabatanPegawaiContant.AdmDir)
                        {
                            if (Status == JenisStatusRenPenConstant.DisetujuiManKeu)
                                if (tbPegawaiUnit.b.IdJenisJabatan != JenisJabatanPegawaiContant.ManKeu)
                                    return "tidak berhak approve data";
                            if (Status == JenisStatusRenPenConstant.DisetujuiDirektur)
                                if (tbPegawaiUnit.b.IdJenisJabatan != JenisJabatanPegawaiContant.Dir)
                                    return "tidak berhak approve data";
                        }
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbRkas = conn.Get<TbUnitRenpen>(IdUnitRenPen);
                        if (tbRkas == null) return "data tidak ada";
                        if (StatusProses == StatusProsesKeuanganConstant.Rejected)
                        {
                            if (string.IsNullOrEmpty(Catatan)) return "catatan harus diisi";
                            int beforeStatus = GetBeforeStatus(Status);
                            var tbUnitRenPenBeforeStatus = (from a in conn.GetList<TbUnitRenpenStatus>()
                                                            where a.IdUnitRenpen == IdUnitRenPen
                                                            & a.Status == beforeStatus
                                                            select a).FirstOrDefault();
                            if (tbUnitRenPenBeforeStatus == null)
                                return string.Format("RKAS {0} tidak ada", JenisStatusRenPenConstant.Dict[beforeStatus]);
                            tbUnitRenPenBeforeStatus.UpdatedBy = IdUser;
                            tbUnitRenPenBeforeStatus.UpdatedDate = dateTimeService.GetCurrdate();
                            tbUnitRenPenBeforeStatus.StatusProses = StatusProses;
                            tbUnitRenPenBeforeStatus.Catatan = Catatan;
                            conn.Update(tbUnitRenPenBeforeStatus);
                        }
                        else
                        {
                            tbRkas.Status = Status;
                            conn.Update(tbRkas);
                            int nextStatus = GetNextStatus(Status);

                            var tbUnitRenPenNextStatus = (from a in conn.GetList<TbUnitRenpenStatus>()
                                                          join b in conn.GetList<TbUser>() on a.CreatedBy equals b.IdUser
                                                          where a.IdUnitRenpen == IdUnitRenPen
                                                          & a.Status == nextStatus
                                                          select new { a, b }).FirstOrDefault();
                            if (tbUnitRenPenNextStatus != null)
                                return string.Format("RKAS sudah {0} oleh {1} pada tanggal {2}", JenisStatusRenPenConstant.Dict[nextStatus], tbUnitRenPenNextStatus.b.FirstName, dateTimeService.DateToLongString(_languageCode, tbUnitRenPenNextStatus.a.CreatedDate));
                            var tbRenPenStatus = (from a in conn.GetList<TbUnitRenpenStatus>()
                                                  where a.IdUnitRenpen == IdUnitRenPen
                                                  & a.Status == Status
                                                  select a).FirstOrDefault();
                            if (tbRenPenStatus == null)
                            {
                                tbRenPenStatus = new TbUnitRenpenStatus
                                {
                                    IdUnitRenpen = IdUnitRenPen,
                                    Status = Status,
                                    Catatan = Catatan,
                                    CreatedBy = IdUser,
                                    CreatedDate = dateTimeService.GetCurrdate(),
                                    StatusProses = StatusProses

                                };
                                conn.Insert(tbRenPenStatus);
                            }
                            else
                            {
                                tbRenPenStatus.StatusProses = StatusProses;
                                tbRenPenStatus.UpdatedBy = IdUser;
                                tbRenPenStatus.UpdatedDate = dateTimeService.GetCurrdate();
                                conn.Update(tbRenPenStatus);
                            }
                        }

                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "ApproveData", ex);
            }

        }
        public string ApproveDataByManKeu(int IdUser, int IdUnitRenPen, int StatusProses, string Catatan)
        {
            return ApproveData(IdUser, IdUnitRenPen, StatusProses, Catatan, JenisStatusRenPenConstant.DisetujuiManKeu);

        }
        public string ApproveDataByDirektur(int IdUser, int IdUnitRenPen, int StatusProses, string Catatan)
        {
            return ApproveData(IdUser, IdUnitRenPen, StatusProses, Catatan, JenisStatusRenPenConstant.DisetujuiDirektur);

        }
        public string SetSah(int IdUser, int IdUnitRenPen)
        {
            return ApproveData(IdUser, IdUnitRenPen, StatusProsesKeuanganConstant.New, string.Empty, JenisStatusRenPenConstant.Disahkan);
        }
        public KeuUnitRenPenDownloadModel GetRenPenDownload(int IdUser, int IdUnitRenPen, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbUnitRenPen = (from a in conn.GetList<TbUnitRenpen>()
                                  join b in conn.GetList<TbUnit>() on a.IdUnit equals b.IdUnit
                                  join c in conn.GetList<TbTahunAjaran>() on a.IdTahunAjaran equals c.IdTahunAjaran
                                  where a.IdUnitRenpen == IdUnitRenPen
                                  select new { a, b, c }).FirstOrDefault();
                    if (tbUnitRenPen == null) { oMessage = "data tidak ada"; return null; }

                    KeuUnitRenPenDownloadModel ret = new KeuUnitRenPenDownloadModel
                    {
                        NamaUnit = tbUnitRenPen.b.Nama,
                        Nomor = "",
                        TotalRp = tbUnitRenPen.a.Rp,
                        TahunAjaran = tbUnitRenPen.c.Nama,
                        Status = JenisStatusRenPenConstant.Dict[tbUnitRenPen.a.Status],
                        Det = new List<KeuUnitRenPenDetDownloadModel>()
                    };

                    var dataRenPen = from a1 in conn.GetList<TbCoa>(string.Format("where id_jenis_akun = {0}", JenisAkunConstant.Golongan))
                                   join a2 in conn.GetList<TbCoa>(string.Format("where id_jenis_akun = {0}", JenisAkunConstant.Utama)) on a1.IdCoa equals a2.IdParentCoa
                                   join a3 in conn.GetList<TbCoa>(string.Format("where id_jenis_akun = {0}", JenisAkunConstant.Madya)) on a2.IdCoa equals a3.IdParentCoa
                                   join a4 in conn.GetList<TbCoa>(string.Format("where id_jenis_akun = {0}", JenisAkunConstant.Pratama)) on a3.IdCoa equals a4.IdParentCoa
                                   join a in conn.GetList<TbUnitRenpenCoa>() on a4.IdCoa equals a.IdCoa
                                   where a.IdUnitRenpen == tbUnitRenPen.a.IdUnitRenpen
                                   select new { a1, a2, a3, a4, a};

                    var dt1 = from dt in dataRenPen
                              group dt by dt.a1 into gdt
                              select new { IdCoa = gdt.Key.IdCoa, Kode = gdt.Key.Kode, Nama = gdt.Key.Nama, Rp = gdt.Sum(item => item.a.Rp) };

                    foreach (var item1 in dt1)
                    {
                        KeuUnitRenPenDetDownloadModel L1 = new KeuUnitRenPenDetDownloadModel
                        {
                            KodeCoa = item1.Kode,
                            NamaCoa = item1.Nama,
                            Rp = item1.Rp,
                            Det = new List<KeuUnitRenPenDetDownloadModel>()
                        };
                        var dt2 = from dt in dataRenPen
                                  where dt.a2.IdParentCoa == item1.IdCoa
                                  group dt by dt.a2 into gdt
                                  select new { IdCoa = gdt.Key.IdCoa, Kode = gdt.Key.Kode, Nama = gdt.Key.Nama, Rp = gdt.Sum(item => item.a.Rp) };

                        foreach (var item2 in dt2)
                        {
                            KeuUnitRenPenDetDownloadModel L2 = new KeuUnitRenPenDetDownloadModel
                            {
                                KodeCoa = item2.Kode,
                                NamaCoa = item2.Nama,
                                Rp = item2.Rp,
                                Det = new List<KeuUnitRenPenDetDownloadModel>()
                            };
                            var dt3 = from dt in dataRenPen
                                      where dt.a3.IdParentCoa == item2.IdCoa
                                      group dt by dt.a3 into gdt
                                      select new { IdCoa = gdt.Key.IdCoa, Kode = gdt.Key.Kode, Nama = gdt.Key.Nama, Rp = gdt.Sum(item => item.a.Rp) };
                            foreach (var item3 in dt3)
                            {
                                KeuUnitRenPenDetDownloadModel L3 = new KeuUnitRenPenDetDownloadModel
                                {
                                    KodeCoa = item3.Kode,
                                    NamaCoa = item3.Nama,
                                    Rp = item3.Rp,
                                    Det = new List<KeuUnitRenPenDetDownloadModel>()
                                };
                                var dt4 = from dt in dataRenPen
                                          where dt.a4.IdParentCoa == item3.IdCoa
                                          group dt by dt.a4 into gdt
                                          select new { IdCoa = gdt.Key.IdCoa, Kode = gdt.Key.Kode, Nama = gdt.Key.Nama, Rp = gdt.Sum(item => item.a.Rp) };
                                foreach (var item4 in dt4)
                                {
                                    KeuUnitRenPenDetDownloadModel L4 = new KeuUnitRenPenDetDownloadModel
                                    {
                                        KodeCoa = item4.Kode,
                                        NamaCoa = item4.Nama,
                                        Rp = item4.Rp,
                                        Det = new List<KeuUnitRenPenDetDownloadModel>()
                                    };
                                    L3.Det.Add(L4);
                                }
                                L2.Det.Add(L3);
                            }
                            L1.Det.Add(L2);
                        }
                        ret.Det.Add(L1);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetRkasDownload", ex);
                return null;
            }
        }
        public List<KeuUnitCoaPaguModel> GetUnitCoaPagus(int IdUser, int IdUnit, int IdTahunAjaran, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var dt = from a in conn.GetList<TbUnitCoaPagu>()
                             join b in conn.GetList<TbUnit>() on a.IdUnit equals b.IdUnit
                             join c in conn.GetList<TbTahunAjaran>() on a.IdTahunAjaran equals c.IdTahunAjaran
                             join d in conn.GetList<TbCoa>() on a.IdCoaRenpen equals d.IdCoa
                             where a.IdTahunAjaran == IdTahunAjaran
                             & a.IdUnit == IdUnit
                             select new { a, b, c, d };
                    if(dt==null||dt.Count()==0) { oMessage = " data tidak ada"; return null; }
                    List<KeuUnitCoaPaguModel> ret = new List<KeuUnitCoaPaguModel>();
                    foreach (var item in dt.OrderBy(x=>x.a.IdUnitCoaPagu))
                    {
                        KeuUnitCoaPaguModel m = new KeuUnitCoaPaguModel
                        {
                            IdUnitCoaPagu = item.a.IdUnitCoaPagu,
                            IdCoaRenPen = item.a.IdCoaRenpen,
                            IdTahunAjaran = item.a.IdTahunAjaran,
                            IdUnit = item.a.IdUnit,
                            NamaCoaRenPen = "[" + item.d.Kode + "] - " + item.d.Nama,
                            RpPagu = item.a.RpPagu,
                            RpPaguUsed = item.a.RpPaguUsed,
                            TahunAjaran = item.c.Nama,
                            Unit = item.b.Nama
                        };
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetUnitCoaPagus", ex);
                return null;
            }
        }
        public List<string> GetUnitCoaPaguProKegs(int IdUser, int IdUnitCoaPagu, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<string> ret = new List<string>();
                using (var conn = commonService.DbConnection())
                {
                    var dt1 = from a in conn.GetList<TbUnitCoaUsed>()
                              join b in conn.GetList<TbCoa>() on a.IdCoaProkeg equals b.IdCoa
                              where a.IdUnitCoaPagu == IdUnitCoaPagu
                              select new { a, b };
                    foreach (var item1 in dt1)
                    {
                        ret.Add("[" + item1.b.Kode + "] - " + item1.b.Nama);
                    }

                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetUnitCoaPaguProKegs", ex);
                return null;
            }
        }
        public string EditCoaPagu(int IdUser, int IdUnitCoaPagu, double RpPagu)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tb = conn.Get<TbUnitCoaPagu>(IdUnitCoaPagu);
                        if (tb == null) return "data tidak ada";
                        tb.RpPagu = RpPagu;
                        conn.Update(tb);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditCoaPagu", ex);
            }
        }

    }
}
