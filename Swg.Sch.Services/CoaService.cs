﻿using Dapper;
using Swg.Entities.Keu;
using Swg.Entities.Lmg;
using Swg.Models;
using Swg.Sch.Shared;
using Swg.Sch.Shared.Constants;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Swg.Sch.Services
{
    public class CoaService : ICoaService
    {
        private readonly string ServiceName = "Swg.Sch.Services.CoaService.";
        private readonly ICommonService commonService;
        private readonly IUserAppService userAppService;
        private readonly IDateTimeService dateTimeService;
        public CoaService(
            ICommonService CommonService,
            IUserAppService UserAppService,
            IDateTimeService DateTimeService)
        {
            commonService = CommonService;
            userAppService = UserAppService;
            dateTimeService = DateTimeService;
        }
        public List<ComboModel> GetJenisAkun()
        {
            return (from a in JenisAkunConstant.Dict
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetJenisTransaksi()
        {
            return (from a in JenisTransaksiConstant.Dict
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }

        public string AddJenisSatuan(string Nama, string NamaSingkat) {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var id = (from a in conn.GetList<TbJenisSatuan>()
                                  select a).Count() + 1;
                        TbJenisSatuan tbJenisSatuan = new TbJenisSatuan { IdJenisSatuan = id, Nama = Nama, NamaSingkat = NamaSingkat };
                        conn.Insert(tbJenisSatuan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddJenisSatuan", ex);
            }
        }
        public string EditJenisSatuan(int IdJenisSatuan, string Nama, string NamaSingkat)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbJenisSatuan tbJenisSatuan = conn.Get<TbJenisSatuan>(IdJenisSatuan);
                        if (tbJenisSatuan == null) return "data tidak ada";
                        tbJenisSatuan.Nama = Nama;
                        tbJenisSatuan.NamaSingkat = NamaSingkat;
                        conn.Update(tbJenisSatuan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditJenisSatuan", ex);
            }
        }
        public string DeleteJenisSatuan(int IdJenisSatuan) {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbJenisSatuan tbJenisSatuan = conn.Get<TbJenisSatuan>(IdJenisSatuan);
                        if (tbJenisSatuan == null) return "data tidak ada";
                        conn.Delete(tbJenisSatuan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteJenisSatuan", ex);
            }
        }
        public List<ComboModel> GetJenisSatuans(out string oMessage) {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    var tbs = conn.GetList<TbJenisSatuan>();
                    if (tbs == null || tbs.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    List<ComboModel> ret = new List<ComboModel>();
                    foreach (var item in tbs)
                    {
                        ret.Add(new ComboModel { Id = item.IdJenisSatuan, Nama = item.Nama, NamaSingkat = item.NamaSingkat });
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisSatuans", ex);
                return null;
            }
        }

        public string AddJenisVolume(string Nama, string NamaSingkat) {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var id = (from a in conn.GetList<TbJenisVolume>()
                                  select a).Count() + 1;
                        TbJenisVolume tbJenisVolume = new TbJenisVolume { IdJenisVolume = id, Nama = Nama, NamaSingkat = NamaSingkat };
                        conn.Insert(tbJenisVolume);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddJenisVolume", ex);
            }
        }
        public string EditJenisVolume(int IdJenisVolume, string Nama, string NamaSingkat)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbJenisVolume tbJenisVolume= conn.Get<TbJenisVolume>(IdJenisVolume);
                        if (tbJenisVolume == null) return "data tidak ada";
                        tbJenisVolume.Nama = Nama;
                        tbJenisVolume.NamaSingkat = NamaSingkat;
                        conn.Update(tbJenisVolume);

                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditJenisVolume", ex);
            }
        }
        public string DeleteJjenisVolume(int IdJenisVolume) {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbJenisVolume tbJenisVolume = conn.Get<TbJenisVolume>(IdJenisVolume);
                        if (tbJenisVolume == null) return "data tidak ada";
                        conn.Delete(tbJenisVolume);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteJjenisVolume", ex);
            }
        }
        public List<ComboModel> GetJenisVolumes(out string oMessage) {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    var tbs = conn.GetList<TbJenisVolume>();
                    if (tbs == null || tbs.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    List<ComboModel> ret = new List<ComboModel>();
                    foreach (var item in tbs)
                    {
                        ret.Add(new ComboModel { Id = item.IdJenisVolume, Nama = item.Nama, NamaSingkat = item.NamaSingkat });
                    }
                    return ret;
                }

            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisVolumes", ex);
                return null;
            }
        }

        public string AddCoa(CoaAddModel Data) {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbCoa = (from a in conn.GetList<TbCoa>()
                                     where a.Kode == Data.Kode
                                     select a).FirstOrDefault();
                        if (tbCoa != null) return string.Format("kode coa :{0} sudah ada", Data.Kode);

                        var idCoa = (from a in conn.GetList<TbCoa>()
                                  select a).Count() + 1;
                        tbCoa = new TbCoa
                        {
                            IdCoa = idCoa,
                            IdJenisAkun = Data.IdJenisAkun,
                            IdJenisTransaksi = Data.IdJenisTransaksi,
                            IdParentCoa = Data.IdParentCoa,
                            Kode = Data.Kode,
                            Nama = Data.Nama
                        };
                        conn.Insert(tbCoa);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddCoa", ex);
            }
        }
        public string EditCoa(CoaAddModel Data) {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbCoa = conn.Get<TbCoa>(Data.IdCoa);
                        if (tbCoa == null) return string.Format("data coa :{0} tidak ada", Data.Kode);
                        
                        tbCoa = (from a in conn.GetList<TbCoa>()
                                     where a.Kode == Data.Kode
                                     select a).FirstOrDefault();
                        if (tbCoa != null) return string.Format("kode coa :{0} sudah ada", Data.Kode);

                        tbCoa.IdJenisAkun = Data.IdJenisAkun;
                        tbCoa.IdJenisTransaksi = Data.IdJenisTransaksi;
                        tbCoa.Nama = Data.Nama;
                        tbCoa.IdParentCoa = Data.IdParentCoa;
                        conn.Update(tbCoa);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditCoa", ex);
            }
        }
        public string DeleteCoa(int IdCoa) {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbCoa tbCoa = conn.Get<TbCoa>(IdCoa);
                        if (tbCoa == null) return "data tidak ada";
                        conn.Delete(tbCoa);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteCoa", ex);
            }
        }
        public List<CoaModel> GetCoasByParent(int IdCoa, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<CoaModel> ret = new List<CoaModel>();
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    var coas = from a in conn.GetList<TbCoa>()
                               where a.IdParentCoa == IdCoa
                               select a;
                    foreach (var item in coas.OrderBy(x => x.Kode))
                    {
                        CoaModel m = new CoaModel
                        {
                            IdCoa = item.IdCoa,
                            IdJenisAkun = item.IdJenisAkun,
                            IdJenisTransaksi = item.IdJenisTransaksi,
                            Kode = item.Kode,
                            IdParentCoa = item.IdParentCoa,
                            Nama = item.Nama,
                            JenisAkun = JenisAkunConstant.Dict[item.IdJenisAkun],
                            JenisTransaksi = JenisTransaksiConstant.Dict[item.IdJenisTransaksi]
                        };
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetCoas", ex);
                return null;
            }
        }
        public List<CoaModel> GetCoas(int IdJenisAkun, out string oMessage) {
            try
            {
                oMessage = string.Empty;
                List<CoaModel> ret = new List<CoaModel>();
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    var coas = from a in conn.GetList<TbCoa>()
                               where a.IdJenisAkun == IdJenisAkun
                               select a;
                    foreach (var item in coas.OrderBy(x => x.Kode))
                    {
                        CoaModel m = new CoaModel
                        {
                            IdCoa = item.IdCoa,
                            IdJenisAkun = item.IdJenisAkun,
                            IdJenisTransaksi = item.IdJenisTransaksi,
                            Kode = item.Kode,
                            IdParentCoa = item.IdParentCoa,
                            Nama = item.Nama,
                            JenisAkun = JenisAkunConstant.Dict[item.IdJenisAkun],
                            JenisTransaksi = JenisTransaksiConstant.Dict[item.IdJenisTransaksi]
                        };
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetCoas", ex);
                return null;
            }
        }
        public List<CoaModel> GetCoas(out string oMessage) {
            try
            {
                oMessage = string.Empty;
                List<CoaModel> ret = new List<CoaModel>();
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    var coas = from a in conn.GetList<TbCoa>()
                               select a;
                    foreach (var item in coas.OrderBy(x => x.Kode))
                    {
                        CoaModel m = new CoaModel
                        {
                            IdCoa = item.IdCoa,
                            IdJenisAkun = item.IdJenisAkun,
                            IdJenisTransaksi = item.IdJenisTransaksi,
                            Kode = item.Kode,
                            IdParentCoa = item.IdParentCoa,
                            Nama = item.Nama,
                            JenisAkun = JenisAkunConstant.Dict[item.IdJenisAkun],
                            JenisTransaksi = JenisTransaksiConstant.Dict[item.IdJenisTransaksi]
                        };
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetCoas", ex);
                return null;
            }
        }
        public CoaModel GetCoa(string Kode, out string oMessage) {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    TbCoa item = (from a in conn.GetList<TbCoa>() where a.Kode == Kode select a).FirstOrDefault();
                    if (item == null) { oMessage = "data tidad ada"; return null; }
                    return new CoaModel
                    {
                        IdCoa = item.IdCoa,
                        IdJenisAkun = item.IdJenisAkun,
                        IdJenisTransaksi = item.IdJenisTransaksi,
                        JenisAkun = JenisAkunConstant.Dict[item.IdJenisAkun],
                        JenisTransaksi = JenisTransaksiConstant.Dict[item.IdJenisTransaksi],
                        Kode = item.Kode,
                        IdParentCoa = item.IdParentCoa,
                        Nama = item.Nama
                    };
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetCoa", ex);
                return null;
            }
        }
        public CoaModel GetCoa(int IdCoa, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    TbCoa item = conn.Get<TbCoa>(IdCoa);
                    if (item == null) { oMessage = "data tidad ada"; return null; }
                    return new CoaModel
                    {
                        IdCoa = item.IdCoa,
                        IdJenisAkun = item.IdJenisAkun,
                        IdJenisTransaksi = item.IdJenisTransaksi,
                        JenisAkun = JenisAkunConstant.Dict[item.IdJenisAkun],
                        JenisTransaksi = JenisTransaksiConstant.Dict[item.IdJenisTransaksi],
                        Kode = item.Kode,
                        IdParentCoa = item.IdParentCoa,
                        Nama = item.Nama
                    };
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetCoa", ex);
                return null;
            }
        }
    }
}
