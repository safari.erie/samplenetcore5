﻿using System;
using System.Collections.Generic;
using System.IO;
using Swg.Sch.Shared.Interfaces;
using PdfSharpCore.Drawing;
using PdfSharpCore.Drawing.Layout;
using PdfSharpCore.Fonts;
using PdfSharpCore.Pdf;
using PdfSharpCore.Pdf.IO;
using Swg.Sch.Shared.Constants;
using Swg.Sch.Shared.ViewModels;
using Swg.Models.Constants;
using Swg.Models;
using Swg.Services;

namespace Swg.Sch.Services
{
    public class PdfService : IPdfService
    {
        private string EkstensiPasFoto = ".jpeg";
        private string _languageCode = "id";
        private readonly string ServiceName = "Swg.Sch.Services.PdfService.";
        private readonly ICommonService commonService;
        public PdfService(
            ICommonService CommonService)
        {
            commonService = CommonService;
        }

        private string FontType { get; set; }
        void AddImage(XGraphics gfx, string imagePath, int xPosition, int yPosition, int width, int height)
        {
            try
            {
                if (!File.Exists(imagePath))
                {
                    throw new FileNotFoundException(String.Format("Could not find image {0}.", imagePath));
                }

                XImage xImage = XImage.FromFile(imagePath);
                gfx.DrawImage(xImage, xPosition, yPosition, width, height);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void AddImage(XGraphics gfx, PdfPage page, string imagePath, int xPosition, int yPosition)
        {
            if (!File.Exists(imagePath))
            {
                throw new FileNotFoundException(String.Format("Could not find image {0}.", imagePath));
            }

            XImage xImage = XImage.FromFile(imagePath);
            gfx.DrawImage(xImage, xPosition, yPosition, xImage.PixelWidth, xImage.PointHeight);
            //gfx.DrawImage(xImage, xPosition, yPosition, PixelWidth, PixelHeight);
        }
        void AddText(XGraphics gfx, string xText, int x, int y, int width, int height, int fontsize, XBrush xBrush, XFontStyle fontstyle, XParagraphAlignment xParagraphAlignment)
        {
            if (string.IsNullOrEmpty(FontType))
                FontType = "OpenSans";
            XFont font = new XFont(FontType, fontsize, fontstyle);
            XTextFormatter tf = new XTextFormatter(gfx);
            XRect rect = new XRect(x, y, width, height);
            gfx.DrawRectangle(XBrushes.Transparent, rect);
            tf.Alignment = xParagraphAlignment;
            tf.DrawString(xText, font, xBrush, rect, XStringFormats.TopLeft);
        }

        public string GenerateKartuPeserta(PpdbDaftarKartuModel Data, string PathFile)
        {
            if (GlobalFontSettings.FontResolver == null)
            {
                string _pathFonts = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PdfFonts/");
                GlobalFontSettings.FontResolver = new FontResolver(_pathFonts);
            }
            PdfDocument document = PdfReader.Open(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/kartu_peserta.pdf"));
            var gfx1 = XGraphics.FromPdfPage(document.Pages[0]);
            AddText(gfx1, Data.Nama, 75, 45, 200, 10, 14, XBrushes.Black, XFontStyle.Bold, XParagraphAlignment.Left);
            AddText(gfx1, Data.Unit, 150, 70, 200, 10, 10, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.JenisPendaftaran + "/" + Data.JalurPendaftaran, 150, 87, 200, 10, 10, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.IdPpdbDaftar.ToString(), 150, 105, 200, 10, 10, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.TempatLahir, 150, 125, 200, 10, 10, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.TanggalLahir, 150, 144, 200, 10, 10, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);


            //int x = int.Parse(Data.Nama.Substring(0, 3));
            //int y = int.Parse(Data.Nama.Substring(3, 3));
            //int w = int.Parse(Data.Nama.Substring(6, 3));
            //int h = int.Parse(Data.Nama.Substring(9, 3));

            //AddText(gfx1, Data.Nama, x, y, w, h, 12, XFontStyle.Regular);

            //            AddText(gfx1, Data.IdPendaftaran.ToString(), 135, 80, 200, 10, XFontStyle.Bold);
            //            AddText(gfx1, Data.TempatLahir, 135, 95, 200, 10, XFontStyle.Regular);
            //            AddText(gfx1, Data.TanggalLahir, 135, 110, 200, 10, XFontStyle.Regular);
            //            int x = int.Parse(Data.Nama.Substring(0, 3));
            //            int y = int.Parse(Data.Nama.Substring(3, 3));
            //            int w = int.Parse(Data.Nama.Substring(6, 3));
            //            int h = int.Parse(Data.Nama.Substring(9, 3));
            //            AddImage(gfx1, Path.Combine(PathFile, Data.IdPendaftaran.ToString() + "_foto.jpeg"), x, y, w, h);
            AddImage(gfx1, Path.Combine(PathFile, Data.IdPpdbDaftar.ToString() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.PasFoto] + ".jpeg"), 8, 70, 55, 84);
            // string _fileName = Path.Combine(PathFile, Data.IdPpdbDaftar
            //     + "_sendtomail_daftar.pdf");
            string _fileName = Path.Combine(PathFile, Data.IdPpdbDaftar.ToString()
                + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuPeserta] + ".pdf");
            if (File.Exists(_fileName))
                File.Delete(_fileName);
            document.Save(_fileName);
            return _fileName;
        }

        public string GenerateKartuPesertaP(PpdbDaftarKartuModel Data, string PathFile)
        {
            if (GlobalFontSettings.FontResolver == null)
            {
                string _pathFonts = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PdfFonts/");
                GlobalFontSettings.FontResolver = new FontResolver(_pathFonts);
            }
            PdfDocument document = PdfReader.Open(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/kartu_peserta1.pdf"), PdfDocumentOpenMode.Import);

            PdfDocument oDocument = new PdfDocument();
            oDocument.AddPage(document.Pages[0]);
            oDocument.AddPage(document.Pages[0]);

            var gfx1 = XGraphics.FromPdfPage(oDocument.Pages[0]);

            //int x = int.Parse(Data.Nama.Substring(0, 3));
            //int y = int.Parse(Data.Nama.Substring(3, 3));
            //int w = int.Parse(Data.Nama.Substring(6, 3));
            //int h = int.Parse(Data.Nama.Substring(9, 3));
            XBrush xBrush = XBrushes.Blue;
            if (Data.KdJenisKelamin == JenisKelaminConstant.Perempuan)
                xBrush = XBrushes.Red;
            AddText(gfx1, Data.Nama, 10, 80, 235, 20, 14, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Center);
            AddText(gfx1, Data.NamaPanggilan.ToUpper(), 10, 100, 235, 25, 16, XBrushes.Black, XFontStyle.Bold, XParagraphAlignment.Center);
            AddText(gfx1, Data.IdPpdbDaftar.ToString(), 10, 250, 235, 25, 16, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Center);
            AddText(gfx1, "PESERTA PPDB", 10, 275, 235, 25, 16, xBrush, XFontStyle.Regular, XParagraphAlignment.Center);
            AddText(gfx1, Data.Unit.ToUpper(), 10, 300, 235, 20, 10, xBrush, XFontStyle.Regular, XParagraphAlignment.Center);
            AddText(gfx1, Data.JenisPendaftaran.ToUpper() + "-" + Data.JalurPendaftaran.ToUpper(), 10, 315, 235, 25, 10, xBrush, XFontStyle.Regular, XParagraphAlignment.Center);

            AddImage(gfx1, Path.Combine(PathFile, Data.IdPpdbDaftar.ToString() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.PasFoto] + ".jpeg"), 86, 130, 84, 84 + 30);
            string _fileName = Path.Combine(PathFile, Data.IdPpdbDaftar
                + "_sendtomail_daftar.pdf");
            if (File.Exists(_fileName))
                File.Delete(_fileName);
            oDocument.Save(_fileName);
            return _fileName;
        }

        public string GenerateSuratLolosSeleksi(string No, string Nama, int IdPendaftaran, int IdUnit, string TglMasehi, string TglHijriyah, string PathFile)
        {
            if (GlobalFontSettings.FontResolver == null)
            {
                string _pathFonts = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PdfFonts/");
                GlobalFontSettings.FontResolver = new FontResolver(_pathFonts);
            }
            FontType = "Calibri";
            PdfDocument document = PdfReader.Open(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/SuratLolosSeleksi.pdf"), PdfDocumentOpenMode.Import);

            PdfDocument oDocument = new PdfDocument();
            oDocument.AddPage(document.Pages[0]);
            oDocument.AddPage(document.Pages[1]);
            var gfx1 = XGraphics.FromPdfPage(oDocument.Pages[0]);

            AddText(gfx1, No, 200, 129, 200, 12, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Center);
            AddText(gfx1, Nama, 256, 355, 200, 12, 12, XBrushes.Black, XFontStyle.Bold, XParagraphAlignment.Left);
            AddText(gfx1, IdPendaftaran.ToString(), 256, 368, 200, 12, 12, XBrushes.Black, XFontStyle.Bold, XParagraphAlignment.Left);

            string _jenjang = "";
            if (IdUnit == UnitConstant.Tk)
                _jenjang = "TKIT";
            if (IdUnit == UnitConstant.Sd)
                _jenjang = "SDIT";
            if (IdUnit == UnitConstant.Smp)
                _jenjang = "SMPIT";
            if (IdUnit == UnitConstant.Sma)
                _jenjang = "SMAIT";

            AddText(gfx1, _jenjang, 256, 383, 200, 12, 12, XBrushes.Black, XFontStyle.Bold, XParagraphAlignment.Left);
            string _str = _jenjang + " At Taufiq TP. 2020/2021";
            AddText(gfx1, _str, 333, 407, 200, 11, 11, XBrushes.Black, XFontStyle.Bold, XParagraphAlignment.Left);
            AddText(gfx1, "Bogor, " + TglMasehi, 70, 503, 200, 12, 12, XBrushes.Black, XFontStyle.Bold, XParagraphAlignment.Left);
            AddText(gfx1, "_".PadLeft(TglMasehi.Length, '_'), 106, 504, 200, 1, 12, XBrushes.Black, XFontStyle.Bold, XParagraphAlignment.Left);
            AddText(gfx1, TglHijriyah, 106, 517, 200, 12, 12, XBrushes.Black, XFontStyle.Bold, XParagraphAlignment.Left);

            var gfx2 = XGraphics.FromPdfPage(oDocument.Pages[1]);
            AddText(gfx2, "Surat " + No, 89, 127, 200, 12, 12, XBrushes.Black, XFontStyle.Italic, XParagraphAlignment.Left);

            string _fileName = Path.Combine(PathFile, IdPendaftaran
                + "_sendtomail_Lolos.pdf");
            if (File.Exists(_fileName))
                File.Delete(_fileName);
            oDocument.Save(_fileName);
            return _fileName;
        }

        public string GenerateKartuPesertaTk(PpdbDaftarKartuModel Data, string PathFile)
        {
            if (GlobalFontSettings.FontResolver == null)
            {
                string _pathFonts = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PdfFonts/");
                GlobalFontSettings.FontResolver = new FontResolver(_pathFonts);
            }
            PdfDocument document = PdfReader.Open(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/kartu_peserta_tk.pdf"), PdfDocumentOpenMode.Import);

            PdfDocument oDocument = new PdfDocument();
            oDocument.AddPage(document.Pages[0]);

            var gfx1 = XGraphics.FromPdfPage(oDocument.Pages[0]);

            AddText(gfx1, "Nama Panggilan:", 23, 120, 100, 12, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.NamaPanggilan, 30, 140, 200, 18, 18, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);
            AddText(gfx1, "Nama Lengkap:", 23, 170, 100, 12, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.Nama, 30, 185, 200, 12, 12, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);
            AddText(gfx1, "Nomor PPDB:", 108, 210, 100, 12, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.IdPpdbDaftar.ToString(), 115, 225, 200, 12, 12, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);
            AddText(gfx1, "Jenjang:", 108, 245, 100, 20, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.Unit.ToLower().Replace("at taufiq", "").ToUpper(), 115, 260, 100, 12, 12, XBrushes.Black, XFontStyle.Bold, XParagraphAlignment.Left);
            string _fileFoto = Path.Combine(Data.Foto);
            if (!File.Exists(_fileFoto))
                _fileFoto = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/no_foto.jpeg");
            AddImage(gfx1, _fileFoto, 25, 211, 69, 97);


            string _fileName = Path.Combine(PathFile, Data.IdPpdbDaftar.ToString()
                + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuPeserta] + ".pdf");
            if (File.Exists(_fileName))
                File.Delete(_fileName);
            oDocument.Save(_fileName);
            return _fileName;
        }
        public string GenerateKartuPesertaSd(PpdbDaftarKartuModel Data, string PathFile)
        {
            if (GlobalFontSettings.FontResolver == null)
            {
                string _pathFonts = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PdfFonts/");
                GlobalFontSettings.FontResolver = new FontResolver(_pathFonts);
            }
            PdfDocument document = PdfReader.Open(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/kartu_peserta_sd.pdf"), PdfDocumentOpenMode.Import);

            PdfDocument oDocument = new PdfDocument();
            oDocument.AddPage(document.Pages[0]);

            var gfx1 = XGraphics.FromPdfPage(oDocument.Pages[0]);

            AddText(gfx1, "Nama Panggilan:", 23, 120, 100, 12, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.NamaPanggilan, 30, 140, 100, 18, 18, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);
            AddText(gfx1, "Nama Lengkap:", 23, 170, 100, 12, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.Nama, 30, 185, 200, 12, 12, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);
            AddText(gfx1, "Nomor PPDB:", 108, 210, 100, 12, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.IdPpdbDaftar.ToString(), 115, 225, 100, 12, 12, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);
            AddText(gfx1, "Jenjang:", 108, 245, 100, 20, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.Unit.ToLower().Replace("at taufiq", "").ToUpper(), 115, 260, 100, 12, 12, XBrushes.Black, XFontStyle.Bold, XParagraphAlignment.Left);
            AddText(gfx1, "Kategori:", 108, 285, 100, 12, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.JenisPendaftaran.ToUpper() + "-" + Data.JalurPendaftaran.ToUpper(), 115, 300, 100, 12, 12, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);

            string _fileFoto = Path.Combine(Data.Foto);
            if (!File.Exists(_fileFoto))
                _fileFoto = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/no_foto.jpeg");

            AddImage(gfx1, _fileFoto, 25, 211, 69, 97);


            // string _fileName = Path.Combine(PathFile, Data.IdPpdbDaftar
            //     + "_sendtomail_daftar.pdf");
            string _fileName = Path.Combine(PathFile, Data.IdPpdbDaftar.ToString()
               + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuPeserta] + ".pdf");
            if (File.Exists(_fileName))
                File.Delete(_fileName);
            oDocument.Save(_fileName);
            return _fileName;
        }
        public string GenerateKartuPesertaSmp(PpdbDaftarKartuModel Data, string PathFile)
        {
            if (GlobalFontSettings.FontResolver == null)
            {
                string _pathFonts = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PdfFonts/");
                GlobalFontSettings.FontResolver = new FontResolver(_pathFonts);
            }
            PdfDocument document = PdfReader.Open(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/kartu_peserta_smp.pdf"), PdfDocumentOpenMode.Import);

            PdfDocument oDocument = new PdfDocument();
            oDocument.AddPage(document.Pages[0]);

            var gfx1 = XGraphics.FromPdfPage(oDocument.Pages[0]);

            AddText(gfx1, "Nama Panggilan:", 23, 120, 100, 12, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.NamaPanggilan, 30, 140, 100, 18, 18, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);
            AddText(gfx1, "Nama Lengkap:", 23, 170, 100, 12, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.Nama, 30, 185, 200, 12, 12, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);
            AddText(gfx1, "Nomor PPDB:", 108, 210, 100, 12, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.IdPpdbDaftar.ToString(), 115, 225, 100, 12, 12, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);
            AddText(gfx1, "Jenjang:", 108, 245, 100, 20, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.Unit.ToLower().Replace("at taufiq", "").ToUpper(), 115, 260, 100, 12, 12, XBrushes.Black, XFontStyle.Bold, XParagraphAlignment.Left);
            AddText(gfx1, "Kategori:", 108, 285, 100, 12, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.JenisPendaftaran.ToUpper() + "-" + Data.JalurPendaftaran.ToUpper(), 115, 300, 100, 12, 12, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);

            string _fileFoto = Path.Combine(Data.Foto);
            if (!File.Exists(_fileFoto))
                _fileFoto = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/no_foto.jpeg");

            AddImage(gfx1, _fileFoto, 25, 211, 69, 97);

            // string _fileName = Path.Combine(PathFile, Data.IdPpdbDaftar
            //     + "_sendtomail_daftar.pdf");
            string _fileName = Path.Combine(PathFile, Data.IdPpdbDaftar.ToString()
               + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuPeserta] + ".pdf");
            if (File.Exists(_fileName))
                File.Delete(_fileName);
            oDocument.Save(_fileName);
            return _fileName;
        }
        public string GenerateKartuPesertaSma(PpdbDaftarKartuModel Data, string PathFile)
        {
            if (GlobalFontSettings.FontResolver == null)
            {
                string _pathFonts = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PdfFonts/");
                GlobalFontSettings.FontResolver = new FontResolver(_pathFonts);
            }
            PdfDocument document = PdfReader.Open(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/kartu_peserta_sma.pdf"), PdfDocumentOpenMode.Import);

            PdfDocument oDocument = new PdfDocument();
            oDocument.AddPage(document.Pages[0]);

            var gfx1 = XGraphics.FromPdfPage(oDocument.Pages[0]);

            AddText(gfx1, "Nama Panggilan:", 23, 120, 100, 12, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.NamaPanggilan, 30, 140, 100, 18, 18, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);
            AddText(gfx1, "Nama Lengkap:", 23, 170, 100, 12, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.Nama, 30, 185, 200, 12, 12, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);
            AddText(gfx1, "Nomor PPDB:", 108, 208, 100, 12, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.IdPpdbDaftar.ToString(), 115, 220, 100, 12, 12, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);
            AddText(gfx1, "Jalur:", 108, 235, 100, 20, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.JalurPendaftaran, 115, 248, 100, 12, 12, XBrushes.Black, XFontStyle.Bold, XParagraphAlignment.Left);
            AddText(gfx1, "Kategori:", 108, 265, 100, 12, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.JenisPendaftaran.ToUpper(), 115, 276, 100, 11, 11, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);
            AddText(gfx1, "Kelas:", 108, 293, 100, 12, 12, XBrushes.Black, XFontStyle.Regular, XParagraphAlignment.Left);
            AddText(gfx1, Data.Kelas, 115, 305, 100, 11, 11, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);

            string _fileFoto = Path.Combine(Data.Foto);
            if (!File.Exists(_fileFoto))
                _fileFoto = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/no_foto.jpeg");

            AddImage(gfx1, _fileFoto, 25, 211, 69, 97);

            // string _fileName = Path.Combine(PathFile, Data.IdPpdbDaftar
            //     + "_sendtomail_daftar.pdf");
            string _fileName = Path.Combine(PathFile, Data.IdPpdbDaftar.ToString()
           + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuPeserta] + ".pdf");
            if (File.Exists(_fileName))
                File.Delete(_fileName);
            oDocument.Save(_fileName);
            return _fileName;
        }

        public string GenerateKartuPesertaA4(List<string> FileNames)
        {
            using (var conn = commonService.DbConnection())
            {
                conn.Open();
                using (var tx = conn.BeginTransaction())
                {
                    var document = new PdfDocument();
                    var _pages = Math.Ceiling(FileNames.Count / (double)4);
                    List<PdfPage> pages = new List<PdfPage>();
                    for (int i = 0; i < _pages; i++)
                    {
                        var page = document.AddPage();
                        page.Size = PdfSharpCore.PageSize.A4;
                        page.Orientation = PdfSharpCore.PageOrientation.Portrait;
                        pages.Add(page);
                    }
                    int j = 0;
                    int k = 0;
                    var gfx = XGraphics.FromPdfPage(pages[k]);
                    for (int i = 0; i < FileNames.Count; i++)
                    {
                        if (j == 4)
                        {
                            k++;
                            gfx = XGraphics.FromPdfPage(pages[k]);
                            j = 0;
                        }
                        if (File.Exists(FileNames[i]))
                        {
                            if (j == 0)
                                AddImage(gfx, pages[k], FileNames[i], 40, 40);
                            if (j == 1)
                                AddImage(gfx, pages[k], FileNames[i], 40, 410);
                            if (j == 2)
                                AddImage(gfx, pages[k], FileNames[i], 310, 40);
                            if (j == 3)
                                AddImage(gfx, pages[k], FileNames[i], 310, 410);

                        }
                        j++;
                    }
                    string _fileName = Path.Combine(AppSetting.PathPpdbBerkas, "berkas" + "_sendtomail_daftar.pdf");
                    if (File.Exists(_fileName))
                        File.Delete(_fileName);
                    document.Save(_fileName);
                    return _fileName;
                }
            }

        }

        public string GenerateNameTagSma(PpdbDaftarKartuModel Data, string PathFile)
        {
            if (GlobalFontSettings.FontResolver == null)
            {
                string _pathFonts = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PdfFonts/");
                GlobalFontSettings.FontResolver = new FontResolver(_pathFonts);
            }
            PdfDocument document = PdfReader.Open(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/nametag_sma.pdf"), PdfDocumentOpenMode.Import);

            PdfDocument oDocument = new PdfDocument();
            oDocument.AddPage(document.Pages[0]);

            var gfx1 = XGraphics.FromPdfPage(oDocument.Pages[0]);
            string namaLengkap = Data.Nama;
            if (Data.Nama.Length >= 26)
                namaLengkap = namaLengkap.Substring(0, 26);

            AddText(gfx1, namaLengkap, 78, 233, 200, 0, 10, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);
            AddText(gfx1, Data.NamaPanggilan, 33, 261, 200, 0, 10, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);
            AddText(gfx1, Data.IdPpdbDaftar.ToString(), 95, 289, 200, 0, 10, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);
            AddText(gfx1, $"{Data.JalurPendaftaran} / {Data.JenisKategoriPendaftaran}", 35, 319, 200, 0, 10, XBrushes.Black, XFontStyle.BoldItalic, XParagraphAlignment.Left);

            string _fileFoto = Path.Combine(Data.Foto);
            if (!File.Exists(_fileFoto))
                _fileFoto = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/no_foto.jpeg");

            AddImage(gfx1, _fileFoto, 73, 63, 110, 122);

            // string _fileName = Path.Combine(PathFile, Data.IdPpdbDaftar
            //     + "_sendtomail_daftar.pdf");
            string _fileName = Path.Combine(PathFile, Data.IdPpdbDaftar.ToString()
           + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuPeserta] + ".pdf");
            if (File.Exists(_fileName))
                File.Delete(_fileName);
            oDocument.Save(_fileName);
            return _fileName;
        }
    }

}
