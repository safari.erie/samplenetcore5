﻿using System;
using System.Collections.Generic;
using Dapper;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using System.Linq;
using System.Data;
using Swg.Sch.Shared.Constants;
using System.IO;
using Swg.Services;
using Swg.Models;
using Swg.Entities.Lmg;
using Swg.Entities.Ref;
using Swg.Entities.Public;
using Swg.Models.Constants;
using Swg.Entities.Kbm;
using Swg.Entities.Ppdb;

namespace Swg.Sch.Services
{
    public class SekolahService : ISekolahService
    {
        private string _languageCode = "id";
        private readonly string ServiceName = "Swg.Sch.Services.SekolahService.";
        private readonly ICommonService commonService;
        private readonly IUserAppService userAppService;
        private readonly IDateTimeService dateTimeService;
        private readonly ISchService schService;

        public SekolahService(
            ICommonService CommonService,
            IDateTimeService DateTimeService,
            IUserAppService UserAppService,
            ISchService SchService)
        {
            commonService = CommonService;
            userAppService = UserAppService;
            dateTimeService = DateTimeService;
            schService = SchService;
        }

        public List<ComboModel> GetJenisVisiMisi()
        {
            return (from a in JenisVisiMisiConstant.DictJenisVisiMisi
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetJenisGaleri()
        {
            return (from a in JenisGaleriConstant.DictJenisGaleri
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetJenisPegawai()
        {
            return (from a in JenisPegawaiConstant.DictJenisPegawai
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }

        public SekolahReadModel GetSekolah(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = (from a in conn.GetList<TbSekolah>()
                                select a).FirstOrDefault();
                    if (data == null) { oMessage = "data tidak ada"; return null; }
                    return new SekolahReadModel
                    {
                        Alamat = data.Alamat,
                        Email = data.Email,
                        IdSekolah = data.IdSekolah,
                        KodePos = data.KodePos,
                        Nama = data.Nama,
                        NamaSingkat = data.NamaSingkat,
                        NoFaximili = data.NoFaximili,
                        NoTelepon = data.NoTelepon,
                        Desa = data.Desa,
                        Kabupaten = data.Kabupaten,
                        Kecamatan = data.Kecamatan,
                        Provinsi = data.Provinsi,
                        Moto = data.Moto,
                        Web = data.Web,
                        FotoWeb = data.FotoWeb,
                        Icon = data.Icon,
                        Logo = data.Logo,
                        Facebook = data.Facebook,
                        Twitter = data.Twitter,
                        Instagram = data.Instagram,
                        Youtube = data.Youtube,
                        Profile = data.Profile,
                        Video = data.Video
                    };
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSekolah", ex);
                return null;
            }
        }
        public string SekolahEdit(int IdUser, SekolahAddEditModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbSekolah = conn.Get<TbSekolah>(Data.IdSekolah);
                        if (tbSekolah == null) return "data tidak ada";

                        tbSekolah.Alamat = Data.Alamat;
                        tbSekolah.Email = Data.Email;
                        tbSekolah.KodePos = Data.KodePos;
                        tbSekolah.Nama = Data.Nama;
                        tbSekolah.NamaSingkat = Data.NamaSingkat;
                        tbSekolah.NoFaximili = Data.NoFaximili;
                        tbSekolah.NoTelepon = Data.NoTelepon;
                        tbSekolah.Desa = Data.Desa;
                        tbSekolah.Kabupaten = Data.Kabupaten;
                        tbSekolah.Kecamatan = Data.Kecamatan;
                        tbSekolah.Provinsi = Data.Provinsi;
                        tbSekolah.Moto = Data.Moto;
                        tbSekolah.Web = Data.Web;
                        tbSekolah.Facebook = Data.Facebook;
                        tbSekolah.Twitter = Data.Twitter;
                        tbSekolah.Instagram = Data.Instagram;
                        tbSekolah.Youtube = Data.Youtube;
                        tbSekolah.Profile = Data.Profile;
                        tbSekolah.Video = Data.Video;

                        var pathUpload = AppSetting.PathApplImage;
                        /// upload foto web if exists
                        string fotoWebName = string.Empty;
                        if (Data.FotoWeb != null)
                        {
                            if (File.Exists(Path.Combine(pathUpload, tbSekolah.FotoWeb)))
                                File.Delete(Path.Combine(pathUpload, tbSekolah.FotoWeb));

                            fotoWebName = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.FotoWebSekolah] + Path.GetExtension(Data.FotoWeb.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, fotoWebName), FileMode.Create, FileAccess.Write))
                            {
                                Data.FotoWeb.CopyTo(fileStream);
                            }
                        }
                        tbSekolah.FotoWeb = fotoWebName;

                        /// upload logo if exists
                        string logoName = string.Empty;
                        if (Data.Logo != null)
                        {
                            if (File.Exists(Path.Combine(pathUpload, tbSekolah.Logo)))
                                File.Delete(Path.Combine(pathUpload, tbSekolah.Logo));

                            logoName = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.LogoSekolah] + Path.GetExtension(Data.Logo.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, logoName), FileMode.Create, FileAccess.Write))
                            {
                                Data.Logo.CopyTo(fileStream);
                            }
                        }
                        tbSekolah.Logo = logoName;

                        /// upload icon if exists
                        string iconName = string.Empty;
                        if (Data.Icon != null)
                        {
                            if (File.Exists(Path.Combine(pathUpload, tbSekolah.Icon)))
                                File.Delete(Path.Combine(pathUpload, tbSekolah.Icon));

                            iconName = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.IconSekolah] + Path.GetExtension(Data.Icon.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, iconName), FileMode.Create, FileAccess.Write))
                            {
                                Data.Logo.CopyTo(fileStream);
                            }
                        }
                        tbSekolah.Icon = iconName;

                        conn.Update(tbSekolah);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SekolahEdit", ex);
            }
        }

        public List<SekolahVisiMisiModel> GetSekolahVisiMisis(int IdUser, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<SekolahVisiMisiModel> ret = new List<SekolahVisiMisiModel>();
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {

                        TbUnitPegawai tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                                       where a.IdPegawai == IdUser
                                                       select a).FirstOrDefault();
                        TbUnit tbUnit = conn.Get<TbUnit>(tbUnitPegawai.IdUnit);
                        TbSekolah tbSekolah = conn.Get<TbSekolah>(tbUnit.IdSekolah);

                        var data = from a in conn.GetList<TbSekolahVisiMisi>()
                                   where a.IdSekolah == tbSekolah.IdSekolah
                                   select a;
                        if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                        foreach (var item in data)
                        {
                            ret.Add(new SekolahVisiMisiModel
                            {
                                IdSekolahVisiMisi = item.IdSekolahVisiMisi,
                                IdJenisVisiMisi = item.IdJenisVisiMisi,
                                IdSekolah = item.IdSekolah,
                                FileIkon = item.FileIkon,
                                Judul = item.Judul,
                                Konten = item.Konten,
                                JenisVisiMisi = JenisVisiMisiConstant.DictJenisVisiMisi[item.IdJenisVisiMisi]
                            });
                        }
                        return ret;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSekolahVisiMisis", ex);
                return null;
            }
        }
        public SekolahVisiMisiModel GetSekolahVisiMisi(int IdSekolahVisiMisi, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                SekolahVisiMisiModel ret = new SekolahVisiMisiModel();
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var dt = (from a in conn.GetList<TbSekolahVisiMisi>()
                                  where a.IdSekolahVisiMisi == IdSekolahVisiMisi
                                  select a).ToList();
                        if (dt == null || dt.Count() == 0)
                        {
                            oMessage = "data tidak ada";
                            return null;
                        }

                        var data = conn.Get<TbSekolahVisiMisi>(IdSekolahVisiMisi);
                        ret.IdSekolahVisiMisi = data.IdSekolahVisiMisi;
                        ret.IdSekolah = data.IdSekolah;
                        ret.IdJenisVisiMisi = data.IdJenisVisiMisi;
                        ret.Judul = data.Judul;
                        ret.Konten = data.Konten;
                        ret.FileIkon = data.FileIkon;
                        return ret;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSekolahVisiMisi", ex);
                return null;
            }
        }
        public string SekolahVisiMisiAdd(int IdUser, SekolahVisiMisiAddModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbUnitPegawai tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                                       where a.IdPegawai == IdUser
                                                       select a).FirstOrDefault();
                        TbUnit tbUnit = conn.Get<TbUnit>(tbUnitPegawai.IdUnit);
                        TbSekolah tbSekolah = conn.Get<TbSekolah>(tbUnit.IdSekolah);

                        TbSekolahVisiMisi tbSekolahVisiMisi = new TbSekolahVisiMisi();
                        var tbSekolahVisiMisis = conn.GetList<TbSekolahVisiMisi>();
                        if (tbSekolahVisiMisis == null || tbSekolahVisiMisis.Count() == 0)
                        {
                            tbSekolahVisiMisi.IdSekolahVisiMisi = 1;
                        }
                        else
                        {
                            var maxSekolahVisiMisi = (from a in conn.GetList<TbSekolahVisiMisi>()
                                                      select new { a.IdSekolahVisiMisi }).OrderByDescending(vm => vm.IdSekolahVisiMisi).FirstOrDefault();
                            tbSekolahVisiMisi.IdSekolahVisiMisi = maxSekolahVisiMisi.IdSekolahVisiMisi + 1;
                        }

                        tbSekolahVisiMisi.IdJenisVisiMisi = Data.IdJenisVisiMisi;
                        tbSekolahVisiMisi.IdSekolah = tbSekolah.IdSekolah;
                        tbSekolahVisiMisi.Judul = Data.Judul;
                        tbSekolahVisiMisi.Konten = Data.Konten;
                        tbSekolahVisiMisi.FileIkon = Data.FileIkon;
                        conn.Insert(tbSekolahVisiMisi);
                        tx.Commit();

                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SekolahVisiMisiAdd", ex);
            }
        }
        public string SekolahVisiMisiEdit(int IdUser, SekolahVisiMisiEditModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbUnitPegawai tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                                       where a.IdPegawai == IdUser
                                                       select a).FirstOrDefault();
                        TbUnit tbUnit = conn.Get<TbUnit>(tbUnitPegawai.IdUnit);
                        TbSekolah tbSekolah = conn.Get<TbSekolah>(tbUnit.IdSekolah);
                        TbSekolahVisiMisi tbSekolahVisiMisi = conn.Get<TbSekolahVisiMisi>(Data.IdSekolahVisiMisi);
                        if (tbSekolahVisiMisi == null)
                        {
                            return "data tidak ditemukan!";
                        }
                        tbSekolahVisiMisi.IdJenisVisiMisi = Data.IdJenisVisiMisi;
                        tbSekolahVisiMisi.IdSekolah = tbSekolah.IdSekolah;
                        tbSekolahVisiMisi.Judul = Data.Judul;
                        tbSekolahVisiMisi.Konten = Data.Konten;
                        tbSekolahVisiMisi.FileIkon = Data.FileIkon;
                        conn.Update(tbSekolahVisiMisi);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SekolahVisiMisiEdit", ex);
            }
        }
        public string SekolahVisiMisiDelete(int IdUser, int IdSekolahVisiMisi)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbSekolahVisiMisi tbSekolahVisiMisi = conn.Get<TbSekolahVisiMisi>(IdSekolahVisiMisi);
                        if (tbSekolahVisiMisi == null)
                        {
                            return "data tidak ditemukan!";
                        }
                        conn.Delete(tbSekolahVisiMisi);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SekolahVisiMisiDelete", ex);
            }
        }

        public List<SekolahGaleriReadModel> GetSekolahGaleris(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<SekolahGaleriReadModel> ret = new List<SekolahGaleriReadModel>();
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var data = from a in conn.GetList<TbSekolahGaleri>()
                                   select a;
                        if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                        foreach (var item in data)
                        {
                            ret.Add(new SekolahGaleriReadModel
                            {
                                IdSekolahGaleri = item.IdSekolahGaleri,
                                IdSekolah = item.IdSekolah,
                                IdJenisGaleri = item.IdJenisGaleri,
                                Judul = item.Judul,
                                Keterangan = item.Keterangan,
                                FileUrl = item.FileUrl,
                                JenisGaleri = JenisGaleriConstant.DictJenisGaleri[item.IdJenisGaleri],
                                UrlVideo = item.FileUrl
                            });
                        }
                        return ret;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSekolahGaleris", ex);
                return null;
            }
        }
        public SekolahGaleriReadModel GetSekolahGaleri(int IdSekolahGaleri, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                SekolahGaleriReadModel ret = new SekolahGaleriReadModel();
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var dt = (from a in conn.GetList<TbSekolahGaleri>()
                                  where a.IdSekolahGaleri == IdSekolahGaleri
                                  select a).ToList();
                        if (dt == null || dt.Count() == 0)
                        {
                            oMessage = "data tidak ada";
                            return null;
                        }

                        var data = conn.Get<TbSekolahGaleri>(IdSekolahGaleri);
                        ret.IdSekolahGaleri = data.IdSekolahGaleri;
                        ret.IdSekolah = data.IdSekolah;
                        ret.IdJenisGaleri = data.IdJenisGaleri;
                        ret.Judul = data.Judul;
                        ret.Keterangan = data.Keterangan;
                        ret.FileUrl = data.FileUrl;
                        ret.UrlVideo = data.FileUrl;
                        ret.JenisGaleri = JenisGaleriConstant.DictJenisGaleri[data.IdJenisGaleri];
                        return ret;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSekolahGaleri", ex);
                return null;
            }
        }
        public string SekolahGaleriAdd(int IdUser, SekolahGaleriAddEditModel Data)
        {
            try
            {

                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbUnitPegawai tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                                       where a.IdPegawai == IdUser
                                                       select a).FirstOrDefault();

                        TbUnit tbUnit = conn.Get<TbUnit>(tbUnitPegawai.IdUnit);
                        TbSekolah tbSekolah = conn.Get<TbSekolah>(tbUnit.IdSekolah);

                        TbSekolahGaleri tbSekolahGaleri = new TbSekolahGaleri();
                        var tbSekolahGaleris = conn.GetList<TbSekolahGaleri>();
                        if (tbSekolahGaleris == null || tbSekolahGaleris.Count() == 0)
                        {
                            tbSekolahGaleri.IdSekolahGaleri = 1;
                        }
                        else
                        {
                            var maxSekolahGaleri = (from a in conn.GetList<TbSekolahGaleri>()
                                                    select new { a.IdSekolahGaleri }).OrderByDescending(g => g.IdSekolahGaleri).FirstOrDefault();
                            tbSekolahGaleri.IdSekolahGaleri = maxSekolahGaleri.IdSekolahGaleri + 1;
                        }
                        tbSekolahGaleri.IdSekolah = tbSekolah.IdSekolah;
                        tbSekolahGaleri.IdJenisGaleri = Data.IdJenisGaleri;
                        tbSekolahGaleri.Judul = Data.Judul;
                        tbSekolahGaleri.Keterangan = Data.Keterangan;
                        tbSekolahGaleri.CreatedBy = IdUser;
                        tbSekolahGaleri.CreatedDate = dateTimeService.GetCurrdate();
                        if (Data.IdJenisGaleri == JenisGaleriConstant.Image)
                        {
                            string fileName = string.Empty;
                            if (Data.FileUrl != null && Data.FileUrl.Length > 0)
                            {
                                var pathUpload = AppSetting.PathGaleriSekolah;

                                fileName = Path.GetRandomFileName() + Path.GetExtension(Data.FileUrl.FileName);
                                using (var fileStream = new FileStream(Path.Combine(pathUpload, fileName), FileMode.Create, FileAccess.Write))
                                {
                                    Data.FileUrl.CopyTo(fileStream);
                                }
                            }
                            tbSekolahGaleri.FileUrl = fileName;
                        }
                        else if (Data.IdJenisGaleri == JenisGaleriConstant.Video)
                        {
                            tbSekolahGaleri.FileUrl = Data.UrlVideo;
                        }
                        conn.Insert(tbSekolahGaleri);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SekolahGaleriAdd", ex);
            }
        }
        public string SekolahGaleriEdit(int IdUser, SekolahGaleriAddEditModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbUnitPegawai tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                                       where a.IdPegawai == IdUser
                                                       select a).FirstOrDefault();
                        TbUnit tbUnit = conn.Get<TbUnit>(tbUnitPegawai.IdUnit);
                        TbSekolah tbSekolah = conn.Get<TbSekolah>(tbUnit.IdSekolah);

                        TbSekolahGaleri tbSekolahGaleri = conn.Get<TbSekolahGaleri>(Data.IdSekolahGaleri);
                        if (tbSekolahGaleri == null) return "data tidak ada";

                        if (Data.IdJenisGaleri == JenisGaleriConstant.Image)
                        {
                            string fileName = string.Empty;
                            if (Data.FileUrl != null && Data.FileUrl.Length > 0)
                            {
                                var pathUpload = AppSetting.PathGaleriSekolah;
                                //if (!string.IsNullOrEmpty(err)) return err;
                                if (File.Exists(Path.Combine(pathUpload, tbSekolahGaleri.FileUrl)))
                                    File.Delete(Path.Combine(pathUpload, tbSekolahGaleri.FileUrl));

                                fileName = Path.GetRandomFileName() + Path.GetExtension(Data.FileUrl.FileName);
                                using (var fileStream = new FileStream(Path.Combine(pathUpload, fileName), FileMode.Create, FileAccess.Write))
                                {
                                    Data.FileUrl.CopyTo(fileStream);
                                }
                                tbSekolahGaleri.FileUrl = fileName;
                            }
                        }
                        else if (Data.IdJenisGaleri == JenisGaleriConstant.Video)
                        {
                            tbSekolahGaleri.FileUrl = Data.UrlVideo;
                        }

                        tbSekolahGaleri.IdSekolah = tbSekolah.IdSekolah;
                        tbSekolahGaleri.IdJenisGaleri = Data.IdJenisGaleri;
                        tbSekolahGaleri.Judul = Data.Judul;
                        tbSekolahGaleri.Keterangan = Data.Keterangan;
                        conn.Update(tbSekolahGaleri);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SekolahGaleriEdit", ex);
            }
        }
        public string SekolahGaleriDelete(int IdUser, int IdSekolahGaleri)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbSekolahGaleri tbSekolahGaleri = conn.Get<TbSekolahGaleri>(IdSekolahGaleri);
                        var pathUpload = AppSetting.PathGaleriSekolah;
                        //if (!string.IsNullOrEmpty(err)) return err;
                        if (File.Exists(Path.Combine(pathUpload, tbSekolahGaleri.FileUrl)))
                            File.Delete(Path.Combine(pathUpload, tbSekolahGaleri.FileUrl));
                        if (tbSekolahGaleri == null)
                        {
                            return "data tidak ditemukan!";
                        }
                        if (tbSekolahGaleri == null)
                        {
                            return "data tidak ditemukan!";
                        }
                        conn.Delete(tbSekolahGaleri);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SekolahGaleriDelete", ex);
            }
        }

        public List<SekolahKegiatanReadModel> GetSekolahKegiatans(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<SekolahKegiatanReadModel> ret = new List<SekolahKegiatanReadModel>();
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var data = from a in conn.GetList<TbSekolahKegiatan>()
                                   select a;
                        if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                        foreach (var item in data)
                        {
                            ret.Add(new SekolahKegiatanReadModel
                            {
                                IdSekolahKegiatan = item.IdSekolahKegiatan,
                                IdSekolah = item.IdSekolah,
                                Judul = item.Judul,
                                Keterangan = item.Keterangan,
                                FileImage = item.FileImage,
                                Url = item.Url
                            });
                        }
                        return ret;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSekolahKegiatans", ex);
                return null;
            }
        }
        public SekolahKegiatanReadModel GetSekolahKegiatan(int IdSekolahKegiatan, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                SekolahKegiatanReadModel ret = new SekolahKegiatanReadModel();
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var dt = (from a in conn.GetList<TbSekolahKegiatan>()
                                  where a.IdSekolahKegiatan == IdSekolahKegiatan
                                  select a).ToList();
                        if (dt == null || dt.Count() == 0)
                        {
                            oMessage = "data tidak ada";
                            return null;
                        }

                        var data = conn.Get<TbSekolahKegiatan>(IdSekolahKegiatan);
                        ret.IdSekolahKegiatan = data.IdSekolahKegiatan;
                        ret.IdSekolah = data.IdSekolah;
                        ret.Judul = data.Judul;
                        ret.Keterangan = data.Keterangan;
                        ret.Url = data.Url;
                        ret.FileImage = data.FileImage;
                        return ret;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSekolahKegiatan", ex);
                return null;
            }
        }
        public string SekolahKegiatanAdd(int IdUser, SekolahKegiatanAddEditModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbUnitPegawai tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                                       where a.IdPegawai == IdUser
                                                       select a).FirstOrDefault();
                        TbUnit tbUnit = conn.Get<TbUnit>(tbUnitPegawai.IdUnit);
                        TbSekolah tbSekolah = conn.Get<TbSekolah>(tbUnit.IdSekolah);

                        TbSekolahKegiatan tbSekolahKegiatan = new TbSekolahKegiatan();
                        var tbSekolahKegiatans = conn.GetList<TbSekolahKegiatan>();
                        if (tbSekolahKegiatans == null || tbSekolahKegiatans.Count() == 0)
                        {
                            tbSekolahKegiatan.IdSekolahKegiatan = 1;
                        }
                        else
                        {
                            // GET MAX ID + 1 
                            var maxSekolahKegiatan = (from a in conn.GetList<TbSekolahKegiatan>()
                                                      select new { a.IdSekolahKegiatan }).OrderByDescending(uk => uk.IdSekolahKegiatan).FirstOrDefault();
                            tbSekolahKegiatan.IdSekolahKegiatan = maxSekolahKegiatan.IdSekolahKegiatan + 1;
                        }
                        tbSekolahKegiatan.IdSekolah = tbSekolah.IdSekolah;
                        tbSekolahKegiatan.Judul = Data.Judul;
                        tbSekolahKegiatan.Keterangan = Data.Keterangan;
                        tbSekolahKegiatan.Url = Data.Url;
                        tbSekolahKegiatan.CreatedBy = IdUser;
                        tbSekolahKegiatan.CreatedDate = dateTimeService.GetCurrdate();
                        string fileName = string.Empty;
                        if (Data.FileImage != null && Data.FileImage.Length > 0)
                        {
                            var pathUpload = AppSetting.PathKegiatanSekolah;
                            //if (!string.IsNullOrEmpty(oMessage)) return oMessage;

                            fileName = Path.GetRandomFileName() + Path.GetExtension(Data.FileImage.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, fileName), FileMode.Create, FileAccess.Write))
                            {
                                Data.FileImage.CopyTo(fileStream);
                            }
                            tbSekolahKegiatan.FileImage = fileName;
                        }
                        conn.Insert(tbSekolahKegiatan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SekolahKegiatanAdd", ex);
            }
        }
        public string SekolahKegiatanEdit(int IdUser, SekolahKegiatanAddEditModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbUnitPegawai tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                                       where a.IdPegawai == IdUser
                                                       select a).FirstOrDefault();
                        TbUnit tbUnit = conn.Get<TbUnit>(tbUnitPegawai.IdUnit);
                        TbSekolah tbSekolah = conn.Get<TbSekolah>(tbUnit.IdSekolah);

                        TbSekolahKegiatan tbSekolahKegiatan = conn.Get<TbSekolahKegiatan>(Data.IdSekolahKegiatan);
                        if (tbSekolahKegiatan == null) return "data tidak ada";


                        string fileName = string.Empty;
                        if (Data.FileImage != null && Data.FileImage.Length > 0)
                        {
                            var pathUpload = AppSetting.PathKegiatanSekolah;
                            //if (!string.IsNullOrEmpty(err)) return err;
                            if (File.Exists(Path.Combine(pathUpload, tbSekolahKegiatan.FileImage)))
                                File.Delete(Path.Combine(pathUpload, tbSekolahKegiatan.FileImage));

                            fileName = Path.GetRandomFileName() + Path.GetExtension(Data.FileImage.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, fileName), FileMode.Create, FileAccess.Write))
                            {
                                Data.FileImage.CopyTo(fileStream);
                            }
                            tbSekolahKegiatan.FileImage = fileName;
                        }
                        tbSekolahKegiatan.IdSekolah = tbSekolah.IdSekolah;
                        tbSekolahKegiatan.Judul = Data.Judul;
                        tbSekolahKegiatan.Keterangan = Data.Keterangan;
                        tbSekolahKegiatan.Url = Data.Url;
                        conn.Update(tbSekolahKegiatan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SekolahKegiatanEdit", ex);
            }
        }
        public string SekolahKegiatanDelete(int IdUser, int IdSekolahKegiatan)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbSekolahKegiatan tbSekolahKegiatan = conn.Get<TbSekolahKegiatan>(IdSekolahKegiatan);
                        var pathUpload = AppSetting.PathKegiatanSekolah;
                        //if (!string.IsNullOrEmpty(err)) return err;
                        if (File.Exists(Path.Combine(pathUpload, tbSekolahKegiatan.FileImage)))
                            File.Delete(Path.Combine(pathUpload, tbSekolahKegiatan.FileImage));
                        if (tbSekolahKegiatan == null)
                        {
                            return "data tidak ditemukan!";
                        }
                        conn.Delete(tbSekolahKegiatan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SekolahKegiatanDelete", ex);
            }
        }

        public List<PegawaiModel> GetPenguruss(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = from pegawai in conn.GetList<TbPegawai>()
                               join jabatan in conn.GetList<TbJenisJabatan>() on pegawai.IdJenisJabatan equals jabatan.IdJenisJabatan
                               join user in conn.GetList<TbUser>() on pegawai.IdPegawai equals user.IdUser
                               join jenjangPendidikan in conn.GetList<TbJenisJenjangPendidikan>() on pegawai.IdJenisJenjangPendidikan equals jenjangPendidikan.IdJenisJenjangPendidikan
                               where jabatan.IdJenisJabatan == JenisJabatanPegawaiContant.Yay
                               select new { pegawai, jabatan, user, jenjangPendidikan };
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    List<PegawaiModel> ret = new List<PegawaiModel>();
                    foreach (var item in data)
                    {
                        PegawaiModel m = new PegawaiModel();
                        m.Email = item.user.Email;
                        m.Nama = userAppService.SetFullName(item.user.FirstName, item.user.MiddleName, item.user.LastName);
                        m.JenisKelamin = JenisKelaminConstant.Dict[item.pegawai.KdJenisKelamin];
                        m.Alamat = item.user.Address;
                        m.NoTelpon = item.user.PhoneNumber;
                        m.NoHandphone = item.user.MobileNumber;
                        m.Jabatan = item.jabatan.Nama;
                        m.Agama = AgamaConstant.Dict[item.pegawai.IdAgama];
                        m.JenjangPendidikan = item.jenjangPendidikan.Nama;
                        m.IdPegawai = item.pegawai.IdPegawai;
                        m.IdJabatan = item.pegawai.IdJenisJabatan;
                        m.Nip = item.pegawai.Nip;
                        m.Nik = item.pegawai.Nik; ;
                        m.TempatLahir = item.pegawai.TempatLahir;
                        m.TanggalLahir = item.pegawai.TanggalLahir.ToString("dd-MM-yyyy");
                        m.KdJenisKelamin = item.pegawai.KdJenisKelamin;
                        m.IdAgama = item.pegawai.IdAgama;
                        m.IdJenjangPendidikan = item.pegawai.IdJenisJenjangPendidikan;
                        m.NamaInstitusiPendidikan = item.pegawai.NamaInstitusiPendidikan;
                        m.NamaPasangan = item.pegawai.NamaPasangan;
                        m.NoDarurat = item.pegawai.NoDarurat;
                        m.IdJenisPegawai = item.pegawai.IdJenisPegawai;
                        m.JenisPegawai = JenisPegawaiConstant.DictJenisPegawai[item.pegawai.IdJenisPegawai];
                        if (item.pegawai.Status == 1)
                        {
                            m.Status = StatusDataConstant.Dict[1];
                        }
                        else if (item.pegawai.Status == -1)
                        {
                            m.Status = StatusDataConstant.Dict[-1];
                        }
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPenguruss", ex);
                return null;

            }
        }
        //public string SekolahPengurusAdd(int IdUser, List<PegawaiJenisModel> Pegawais)
        //{
        //    try
        //    {
        //        using (var conn = commonService.DbConnection())
        //        {
        //            conn.Open();
        //            var sekolah = conn.GetList<TbSekolah>();
        //            if (sekolah == null || sekolah.Count() == 0) return "data sekolah tidak ada";
        //            if (sekolah.Count() > 1) return "data sekolah lebih dari satu";
        //            int idSekolah = (from a in sekolah select a.IdSekolah).FirstOrDefault();

        //            using (var tx = conn.BeginTransaction())
        //            {
        //                foreach (var pegawai in Pegawais)
        //                {
        //                    var tbSekolahPengurus = (from a in conn.GetList<TbSekolahPengurus>()
        //                                             where a.IdPegawai == pegawai.IdPegawai
        //                                             select a).FirstOrDefault();
        //                    if (tbSekolahPengurus == null)
        //                    {
        //                        tbSekolahPengurus = new TbSekolahPengurus { IdSekolah = idSekolah, IdPegawai = pegawai.IdPegawai, IdJenisPegawaiPengurus = pegawai.IdJenisPegawai };
        //                        conn.Insert(tbSekolahPengurus);
        //                    }
        //                }
        //                tx.Commit();
        //                return string.Empty;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return commonService.GetErrorMessage(ServiceName + "SekolahPengurusAdd", ex);
        //    }
        //}
        //public string SekolahPengurusDelete(int IdUser, List<PegawaiJenisModel> Pegawais)
        //{
        //    try
        //    {
        //        using (var conn = commonService.DbConnection())
        //        {
        //            conn.Open();
        //            var sekolah = conn.GetList<TbSekolah>();
        //            if (sekolah == null || sekolah.Count() == 0) return "data sekolah tidak ada";
        //            if (sekolah.Count() > 1) return "data sekolah lebih dari satu";
        //            int idSekolah = (from a in sekolah select a.IdSekolah).FirstOrDefault();
        //
        //            using (var tx = conn.BeginTransaction())
        //            {
        //                foreach (var pegawai in Pegawais)
        //                {
        //                    var tbSekolahPengurus = (from a in conn.GetList<TbSekolahPengurus>()
        //                                             where a.IdPegawai == pegawai.IdPegawai
        //                                             select a).FirstOrDefault();
        //                    if (tbSekolahPengurus == null) return "data tidak ada";
        //                    conn.Delete(tbSekolahPengurus);
        //                }
        //                tx.Commit();
        //                return string.Empty;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return commonService.GetErrorMessage(ServiceName + "SekolahPengurusDelete", ex);
        //    }
        //}
        public List<TahunAjaranModel> GetTahunAjarans(out string oMessage)
        {
            throw new NotImplementedException();
        }
        public TahunAjaranModel GetTahunAjaran(int IdTahunAjaran, out string oMessage)
        {
            throw new NotImplementedException();
        }

        public string TahunAjaranAdd(int IdUser, TahunAjaranAddModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbTahunAjarans = conn.GetList<TbTahunAjaran>();
                        TbTahunAjaran tbTahunAjaran = new TbTahunAjaran
                        {
                            IdTahunAjaran = tbTahunAjarans.Count() + 1,
                            Nama = Data.Nama,
                            NamaSingkat = Data.NamaSingkat,
                            Status = 0,
                            TanggalMulai = dateTimeService.StrToDateTime(Data.TanggalMulai),
                            TanggalSelesai = dateTimeService.StrToDateTime(Data.TanggalSelesai)
                        };
                        conn.Insert(tbTahunAjaran);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "TahunAjaranAdd", ex);
            }
        }
        public string TahunAjaranEdit(int IdUser, TahunAjaranEditModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbTahunAjaran = conn.Get<TbTahunAjaran>(Data.IdTahunAjaran);
                        if (tbTahunAjaran == null) return "data tidak ada";
                        tbTahunAjaran.Nama = Data.Nama;
                        tbTahunAjaran.NamaSingkat = Data.NamaSingkat;
                        tbTahunAjaran.TanggalMulai = dateTimeService.StrToDateTime(Data.TanggalMulai);
                        tbTahunAjaran.TanggalSelesai = dateTimeService.StrToDateTime(Data.TanggalSelesai);
                        conn.Update(tbTahunAjaran);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "TahunAjaranEdit", ex);
            }
        }
        public string SetTahunAjaranActive(int IdUser, int IdTahunAjaran)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbTahunAjarans = from a in conn.GetList<TbTahunAjaran>()
                                             where a.Status == StatusDataConstant.Aktif
                                             select a;
                        foreach (var item in tbTahunAjarans)
                        {
                            item.Status = StatusDataConstant.TidakAktif;
                            conn.Update(item);
                        }
                        var tbTahunAjaran = conn.Get<TbTahunAjaran>(IdTahunAjaran);
                        if (tbTahunAjaran == null) return "data tidak ada";
                        tbTahunAjaran.Status = StatusDataConstant.Aktif;
                        conn.Update(tbTahunAjaran);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetTahunAjaranActive", ex);
            }
        }

        public WebPpdbListInfoModel GetPpdbListInfo(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                WebPpdbListInfoModel ret = new WebPpdbListInfoModel();
                using (var conn = commonService.DbConnection())
                {
                    var tbSekolah = (from a in conn.GetList<TbSekolah>()
                                     select a).FirstOrDefault();
                    if (tbSekolah == null) { oMessage = "data tidak ada"; return null; }

                    #region Aturan
                    var tbAturans = from a in conn.GetList<TbPpdbInfo>()
                                    join b in conn.GetList<TbPpdb>() on a.IdPpdb equals b.IdPpdb
                                    where a.IdJenisPpdbInfo == JenisPpdbInfoConstant.Aturan
                                    select new { a.IdPpdbInfo, a.Judul, a.Konten, a.FileImage };
                    ret.Aturan = new List<WebKontenModel>();
                    foreach (var items in tbAturans.OrderByDescending(x => x.IdPpdbInfo))
                    {
                        ret.Aturan.Add(new WebKontenModel
                        {
                            IdKonten = items.IdPpdbInfo,
                            Judul = items.Judul,
                            Konten = items.Konten
                        });
                    }

                    #endregion

                    #region Alur
                    var tbAlurs = from a in conn.GetList<TbPpdbInfo>()
                                  join b in conn.GetList<TbPpdb>() on a.IdPpdb equals b.IdPpdb
                                  where a.IdJenisPpdbInfo == JenisPpdbInfoConstant.Alur
                                  select new { a.IdPpdbInfo, a.Judul, a.Konten, a.FileImage };
                    ret.Alur = new List<WebKontenModel>();
                    foreach (var items in tbAlurs.OrderByDescending(x => x.IdPpdbInfo))
                    {
                        ret.Alur.Add(new WebKontenModel
                        {
                            IdKonten = items.IdPpdbInfo,
                            Judul = items.Judul,
                            Konten = items.Konten
                        });
                    }
                    #endregion

                    #region Biaya
                    var tbBiayas = from a in conn.GetList<TbPpdbInfo>()
                                   join b in conn.GetList<TbPpdb>() on a.IdPpdb equals b.IdPpdb
                                   where a.IdJenisPpdbInfo == JenisPpdbInfoConstant.Biaya
                                   select new { a.IdPpdbInfo, a.Judul, a.Konten, a.FileImage };
                    ret.Biaya = new List<WebKontenModel>();
                    foreach (var items in tbBiayas.OrderByDescending(x => x.IdPpdbInfo))
                    {
                        ret.Biaya.Add(new WebKontenModel
                        {
                            IdKonten = items.IdPpdbInfo,
                            Judul = items.Judul,
                            Konten = items.Konten
                        });
                    }
                    #endregion

                    #region Pengumuman
                    var tbPengumumans = from a in conn.GetList<TbPpdbInfo>()
                                        join b in conn.GetList<TbPpdb>() on a.IdPpdb equals b.IdPpdb
                                        where a.IdJenisPpdbInfo == JenisPpdbInfoConstant.Pengumuman
                                        select new { a.IdPpdbInfo, a.Judul, a.Konten, a.FileImage };
                    ret.Pengumuman = new List<WebKontenModel>();
                    foreach (var items in tbPengumumans.OrderByDescending(x => x.IdPpdbInfo))
                    {
                        ret.Pengumuman.Add(new WebKontenModel
                        {
                            IdKonten = items.IdPpdbInfo,
                            Judul = items.Judul,
                            Konten = items.Konten
                        });
                    }
                    #endregion
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbInfo", ex);
                return null;
            }
        }
        public DashboardPppdbModel GetDashboardPppdb(out string oMessage)
        {
            var ppdb = schService.GetPpdb(out oMessage);
            if (!string.IsNullOrEmpty(oMessage)) return null;
            DashboardPppdbModel ret = new DashboardPppdbModel
            {
                TahunAjaran = ppdb.TahunAjaran,
                LastUpdate = dateTimeService.DateToLongString(_languageCode, dateTimeService.GetCurrdate()),
                Daftars = new List<DashboardPpdbKelasModel>(),
                Vas = new List<DashboardPpdbKelasModel>(),
                Observasis = new List<DashboardPpdbKelasModel>(),
                Bayars = new List<DashboardPpdbKelasModel>()
            };
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    List<int> idKelass = new List<int>();
                    idKelass.Add(104);
                    idKelass.Add(105);
                    idKelass.Add(209);
                    idKelass.Add(301);
                    idKelass.Add(406);
                    var ppdbKelass = (from a in conn.GetList<TbPpdbKelas>()
                                      join b in conn.GetList<TbKelas>() on a.IdKelas equals b.IdKelas
                                      join c in conn.GetList<TbUnit>() on b.IdUnit equals c.IdUnit
                                      where a.IdPpdb == ppdb.IdPpdb
                                      & idKelass.Contains(b.IdKelas)
                                      select new { a, b, c }).ToList();


                    //REKAP PENDAFTARAN PPDB TA 20-21
                    DashboardPpdbKelasModel daftar = new DashboardPpdbKelasModel
                    {
                        Judul = "Kuota",
                        Infos = new List<DashboardSiswaModel>()
                    };
                    DashboardPpdbKelasModel daftarVa = new DashboardPpdbKelasModel
                    {
                        Judul = "VA Aktif",
                        Infos = new List<DashboardSiswaModel>()
                    };

                    DashboardPpdbKelasModel daftarTunggu = new DashboardPpdbKelasModel
                    {
                        Judul = "Daftar Tunggu",
                        Infos = new List<DashboardSiswaModel>()
                    };
                    //REKAP VA DAN OBSERVASI PPDB TA 20-21
                    DashboardPpdbKelasModel vaBayar = new DashboardPpdbKelasModel
                    {
                        Judul = "Bayar VA",
                        Infos = new List<DashboardSiswaModel>()
                    };
                    DashboardPpdbKelasModel vaFormulir = new DashboardPpdbKelasModel
                    {
                        Judul = "Input Formulir",
                        Infos = new List<DashboardSiswaModel>()
                    };

                    DashboardPpdbKelasModel vaWawancara = new DashboardPpdbKelasModel
                    {
                        Judul = "Wawancara",
                        Infos = new List<DashboardSiswaModel>()
                    };
                    DashboardPpdbKelasModel vaTestObservasi = new DashboardPpdbKelasModel
                    {
                        Judul = "Test/Observasi)",
                        Infos = new List<DashboardSiswaModel>()
                    };
                    //REKAP HASIL OBSERVASI PPDB TA 20-21
                    DashboardPpdbKelasModel observasiKuota = new DashboardPpdbKelasModel
                    {
                        Judul = "Kuota Kelas",
                        Infos = new List<DashboardSiswaModel>()
                    };
                    DashboardPpdbKelasModel observasiDiterima = new DashboardPpdbKelasModel
                    {
                        Judul = "Diterima",
                        Infos = new List<DashboardSiswaModel>()
                    };
                    DashboardPpdbKelasModel observasiDitangguhkan = new DashboardPpdbKelasModel
                    {
                        Judul = "Ditangguhkan",
                        Infos = new List<DashboardSiswaModel>()
                    };
                    DashboardPpdbKelasModel observasiTidakDiterima = new DashboardPpdbKelasModel
                    {
                        Judul = "Tidak Diterima/Undur Diri",
                        Infos = new List<DashboardSiswaModel>()
                    };
                    //REKAP PEMBAYARAN BIAYA PENDIDIKAN PPDB TA 20-21
                    DashboardPpdbKelasModel bayarTotal = new DashboardPpdbKelasModel
                    {
                        Judul = "Bayar Total",
                        Infos = new List<DashboardSiswaModel>()
                    };
                    DashboardPpdbKelasModel bayarCicil = new DashboardPpdbKelasModel
                    {
                        Judul = "Bayar Cicil",
                        Infos = new List<DashboardSiswaModel>()
                    };
                    DashboardPpdbKelasModel bayarTidak = new DashboardPpdbKelasModel
                    {
                        Judul = "Tidak/Belum Bayar",
                        Infos = new List<DashboardSiswaModel>()
                    };
                    foreach (var item in ppdbKelass.OrderBy(x => x.c.IdUnit).ThenBy(x => x.b.IdKelas))
                    {
                        string kelas = item.b.Nama;
                        if (item.a.IdJenisPeminatan != null)
                        {
                            if (item.a.IdJenisPeminatan > 0)
                                kelas += " " + JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb[(int)item.a.IdJenisPeminatan];
                        }
                        //REKAP PENDAFTARAN PPDB TA 20-21
                        DashboardSiswaModel m = new DashboardSiswaModel
                        {
                            Unit = item.c.Nama,
                            Kelas = kelas,
                            JumlahL = item.a.KuotaDaftarL,
                            JumlahP = item.a.KuotaDaftarP,
                            Total = item.a.KuotaDaftarL + item.a.KuotaDaftarP
                        };
                        daftar.Infos.Add(m);

                        var dataVa = from a in conn.GetList<TbPpdbDaftar>()
                                     where a.IdPpdbKelas == item.a.IdPpdbKelas
                                     & a.Status == StatusPpdbDaftarConstant.AktifVa
                                     group a by a.KdJenisKelamin into jk
                                     select new { JenisKelamin = jk.Key, Jumlah = jk.Count() };
                        int jml_L = 0;
                        int jml_P = 0;
                        foreach (var itemData in dataVa)
                        {
                            if (itemData.JenisKelamin == "L") jml_L = itemData.Jumlah;
                            if (itemData.JenisKelamin == "P") jml_P = itemData.Jumlah;
                        }
                        m = new DashboardSiswaModel
                        {
                            Unit = item.c.Nama,
                            Kelas = kelas,
                            JumlahL = jml_L,
                            JumlahP = jml_P,
                            Total = jml_L + jml_P
                        };
                        daftarVa.Infos.Add(m);

                        var dataTunggu = from a in conn.GetList<TbPpdbDaftar>()
                                         where a.IdPpdbKelas == item.a.IdPpdbKelas
                                         & a.Status == StatusPpdbDaftarConstant.DaftarTunggu
                                         group a by a.KdJenisKelamin into jk
                                         select new { JenisKelamin = jk.Key, Jumlah = jk.Count() };
                        jml_L = 0;
                        jml_P = 0;
                        foreach (var itemData in dataTunggu)
                        {
                            if (itemData.JenisKelamin == "L") jml_L = itemData.Jumlah;
                            if (itemData.JenisKelamin == "P") jml_P = itemData.Jumlah;
                        }

                        m = new DashboardSiswaModel
                        {
                            Unit = item.c.Nama,
                            Kelas = kelas,
                            JumlahL = jml_L,
                            JumlahP = jml_P,
                            Total = jml_L + jml_P
                        };
                        daftarTunggu.Infos.Add(m);

                        //REKAP VA DAN OBSERVASI PPDB TA 20-21
                        var dataBayarVa = from a in conn.GetList<TbPpdbDaftar>()
                                          join b in conn.GetList<TbPpdbBiayaDaftar>() on a.IdPpdbDaftar equals b.IdPpdbDaftar
                                          where a.IdPpdbKelas == item.a.IdPpdbKelas
                                          & b.Status == StatusLunasConstant.Lunas
                                          group a by a.KdJenisKelamin into jk
                                          select new { JenisKelamin = jk.Key, Jumlah = jk.Count() };
                        jml_L = 0;
                        jml_P = 0;
                        foreach (var itemData in dataBayarVa)
                        {
                            if (itemData.JenisKelamin == "L") jml_L = itemData.Jumlah;
                            if (itemData.JenisKelamin == "P") jml_P = itemData.Jumlah;
                        }

                        m = new DashboardSiswaModel
                        {
                            Unit = item.c.Nama,
                            Kelas = item.b.Nama,
                            JumlahL = jml_L,
                            JumlahP = jml_P,
                            Total = jml_L + jml_P
                        };
                        vaBayar.Infos.Add(m);

                        var dataFormulir = from a in conn.GetList<TbPpdbSiswa>()
                                           join b in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals b.IdPpdbDaftar
                                           where a.IdPpdbKelas == item.a.IdPpdbKelas
                                           & b.Status == StatusPpdbConstant.Registrasi
                                           group a by a.KdJenisKelamin into jk
                                           select new { JenisKelamin = jk.Key, Jumlah = jk.Count() };
                        jml_L = 0;
                        jml_P = 0;
                        foreach (var itemData in dataFormulir)
                        {
                            if (itemData.JenisKelamin == "L") jml_L = itemData.Jumlah;
                            if (itemData.JenisKelamin == "P") jml_P = itemData.Jumlah;
                        }

                        m = new DashboardSiswaModel
                        {
                            Unit = item.c.Nama,
                            Kelas = item.b.Nama,
                            JumlahL = jml_L,
                            JumlahP = jml_P,
                            Total = jml_L + jml_P
                        };
                        vaFormulir.Infos.Add(m);

                        var dataWawancara = from a in conn.GetList<TbPpdbSiswa>()
                                            join b in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals b.IdPpdbDaftar
                                            where a.IdPpdbKelas == item.a.IdPpdbKelas
                                            & b.Status == StatusPpdbConstant.Wawancara
                                            & b.StatusProses == StatusPpdbWawancaraConstant.Hadir
                                            group a by a.KdJenisKelamin into jk
                                            select new { JenisKelamin = jk.Key, Jumlah = jk.Count() };
                        jml_L = 0;
                        jml_P = 0;
                        foreach (var itemData in dataWawancara)
                        {
                            if (itemData.JenisKelamin == "L") jml_L = itemData.Jumlah;
                            if (itemData.JenisKelamin == "P") jml_P = itemData.Jumlah;
                        }

                        m = new DashboardSiswaModel
                        {
                            Unit = item.c.Nama,
                            Kelas = item.b.Nama,
                            JumlahL = jml_L,
                            JumlahP = jml_P,
                            Total = jml_L + jml_P
                        };
                        vaWawancara.Infos.Add(m);
                        var dataTestObservasi = from a in conn.GetList<TbPpdbSiswa>()
                                                join b in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals b.IdPpdbDaftar
                                                where a.IdPpdbKelas == item.a.IdPpdbKelas
                                                & b.Status == StatusPpdbConstant.TestObservasi
                                                & b.StatusProses == StatusPpdbTestObservasiConstant.Hadir
                                                group a by a.KdJenisKelamin into jk
                                                select new { JenisKelamin = jk.Key, Jumlah = jk.Count() };
                        jml_L = 0;
                        jml_P = 0;
                        foreach (var itemData in dataTestObservasi)
                        {
                            if (itemData.JenisKelamin == "L") jml_L = itemData.Jumlah;
                            if (itemData.JenisKelamin == "P") jml_P = itemData.Jumlah;
                        }

                        m = new DashboardSiswaModel
                        {
                            Unit = item.c.Nama,
                            Kelas = item.b.Nama,
                            JumlahL = jml_L,
                            JumlahP = jml_P,
                            Total = jml_L + jml_P
                        };
                        vaTestObservasi.Infos.Add(m);

                        //REKAP HASIL OBSERVASI PPDB TA 20-21
                        m = new DashboardSiswaModel
                        {
                            Unit = item.c.Nama,
                            Kelas = item.b.Nama,
                            JumlahL = item.a.KuotaTerimaL,
                            JumlahP = item.a.KuotaTerimaP,
                            Total = item.a.KuotaTerimaL + item.a.KuotaTerimaP
                        };
                        observasiKuota.Infos.Add(m);
                        var datTerima = from a in conn.GetList<TbPpdbSiswa>()
                                        join b in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals b.IdPpdbDaftar
                                        where a.IdPpdbKelas == item.a.IdPpdbKelas
                                        & b.Status == StatusPpdbConstant.VerifikasiObservasi
                                        & b.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima
                                        group a by a.KdJenisKelamin into jk
                                        select new { JenisKelamin = jk.Key, Jumlah = jk.Count() };
                        jml_L = 0;
                        jml_P = 0;
                        foreach (var itemData in datTerima)
                        {
                            if (itemData.JenisKelamin == "L") jml_L = itemData.Jumlah;
                            if (itemData.JenisKelamin == "P") jml_P = itemData.Jumlah;
                        }

                        m = new DashboardSiswaModel
                        {
                            Unit = item.c.Nama,
                            Kelas = item.b.Nama,
                            JumlahL = jml_L,
                            JumlahP = jml_P,
                            Total = jml_L + jml_P
                        };
                        observasiDiterima.Infos.Add(m);
                        var dataCadangan = from a in conn.GetList<TbPpdbSiswa>()
                                           join b in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals b.IdPpdbDaftar
                                           where a.IdPpdbKelas == item.a.IdPpdbKelas
                                                  & b.Status == StatusPpdbConstant.VerifikasiObservasi
                                                  & b.StatusProses == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan
                                           group a by a.KdJenisKelamin into jk
                                           select new { JenisKelamin = jk.Key, Jumlah = jk.Count() };
                        jml_L = 0;
                        jml_P = 0;
                        foreach (var itemData in dataCadangan)
                        {
                            if (itemData.JenisKelamin == "L") jml_L = itemData.Jumlah;
                            if (itemData.JenisKelamin == "P") jml_P = itemData.Jumlah;
                        }

                        m = new DashboardSiswaModel
                        {
                            Unit = item.c.Nama,
                            Kelas = item.b.Nama,
                            JumlahL = jml_L,
                            JumlahP = jml_P,
                            Total = jml_L + jml_P
                        };
                        observasiDitangguhkan.Infos.Add(m);

                        List<int> notIns = new List<int>();
                        notIns.Add(StatusPpdbVerifikasiObservasiConstant.Diterima);
                        notIns.Add(StatusPpdbVerifikasiObservasiConstant.Ditangguhkan);
                        var dataTidakDiterima = from a in conn.GetList<TbPpdbSiswa>()
                                                join b in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals b.IdPpdbDaftar
                                                where a.IdPpdbKelas == item.a.IdPpdbKelas
                                                       & b.Status == StatusPpdbConstant.VerifikasiObservasi
                                                       & !notIns.Contains(b.StatusProses)
                                                group a by a.KdJenisKelamin into jk
                                                select new { JenisKelamin = jk.Key, Jumlah = jk.Count() };
                        jml_L = 0;
                        jml_P = 0;
                        foreach (var itemData in dataTidakDiterima)
                        {
                            if (itemData.JenisKelamin == "L") jml_L = itemData.Jumlah;
                            if (itemData.JenisKelamin == "P") jml_P = itemData.Jumlah;
                        }

                        m = new DashboardSiswaModel
                        {
                            Unit = item.c.Nama,
                            Kelas = item.b.Nama,
                            JumlahL = jml_L,
                            JumlahP = jml_P,
                            Total = jml_L + jml_P
                        };
                        observasiTidakDiterima.Infos.Add(m);

                        //REKAP PEMBAYARAN BIAYA PENDIDIKAN PPDB TA 20-21
                        var dataLunasTotal = from a in conn.GetList<TbPpdbSiswa>()
                                             join b in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals b.IdPpdbDaftar
                                             join c in conn.GetList<TbPpdbBiayaSiswa>() on a.IdPpdbDaftar equals c.IdPpdbDaftar
                                             where a.IdPpdbKelas == item.a.IdPpdbKelas
                                                    & b.Status == StatusPpdbConstant.PelunasanBiayaPendidikan
                                                    & c.IsCicil == 0
                                             group a by a.KdJenisKelamin into jk
                                             select new { JenisKelamin = jk.Key, Jumlah = jk.Count() };
                        jml_L = 0;
                        jml_P = 0;
                        foreach (var itemData in dataLunasTotal)
                        {
                            if (itemData.JenisKelamin == "L") jml_L = itemData.Jumlah;
                            if (itemData.JenisKelamin == "P") jml_P = itemData.Jumlah;
                        }

                        m = new DashboardSiswaModel
                        {
                            Unit = item.c.Nama,
                            Kelas = item.b.Nama,
                            JumlahL = jml_L,
                            JumlahP = jml_P,
                            Total = jml_L + jml_P
                        };
                        bayarTotal.Infos.Add(m);

                        var dataLunasCicil = from a in conn.GetList<TbPpdbSiswa>()
                                             join b in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals b.IdPpdbDaftar
                                             join c in conn.GetList<TbPpdbBiayaSiswa>() on a.IdPpdbDaftar equals c.IdPpdbDaftar
                                             where a.IdPpdbKelas == item.a.IdPpdbKelas
                                                    & b.Status == StatusPpdbConstant.PelunasanBiayaPendidikan
                                                    & c.IsCicil == 1
                                             group a by a.KdJenisKelamin into jk
                                             select new { JenisKelamin = jk.Key, Jumlah = jk.Count() };
                        jml_L = 0;
                        jml_P = 0;
                        foreach (var itemData in dataLunasCicil)
                        {
                            if (itemData.JenisKelamin == "L") jml_L = itemData.Jumlah;
                            if (itemData.JenisKelamin == "P") jml_P = itemData.Jumlah;
                        }

                        m = new DashboardSiswaModel
                        {
                            Unit = item.c.Nama,
                            Kelas = item.b.Nama,
                            JumlahL = jml_L,
                            JumlahP = jml_P,
                            Total = jml_L + jml_P
                        };
                        bayarCicil.Infos.Add(m);

                        var dataLunasTidak = from a in conn.GetList<TbPpdbSiswa>()
                                             join b in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals b.IdPpdbDaftar
                                             join c in conn.GetList<TbPpdbBiayaSiswa>() on a.IdPpdbDaftar equals c.IdPpdbDaftar
                                             where a.IdPpdbKelas == item.a.IdPpdbKelas
                                                    & b.Status == StatusPpdbConstant.PelunasanBiayaPendidikan
                                                    & c.IsCicil == 1
                                             group a by a.KdJenisKelamin into jk
                                             select new { JenisKelamin = jk.Key, Jumlah = jk.Count() };
                        jml_L = 0;
                        jml_P = 0;
                        foreach (var itemData in dataLunasCicil)
                        {
                            if (itemData.JenisKelamin == "L") jml_L = itemData.Jumlah;
                            if (itemData.JenisKelamin == "P") jml_P = itemData.Jumlah;
                        }

                        m = new DashboardSiswaModel
                        {
                            Unit = item.c.Nama,
                            Kelas = item.b.Nama,
                            JumlahL = jml_L,
                            JumlahP = jml_P,
                            Total = jml_L + jml_P
                        };
                        bayarTidak.Infos.Add(m);
                    }
                    ret.Daftars.Add(daftar);
                    ret.Daftars.Add(daftarVa);
                    ret.Daftars.Add(daftarTunggu);

                    ret.Vas.Add(vaBayar);
                    ret.Vas.Add(vaFormulir);
                    ret.Vas.Add(vaWawancara);
                    ret.Vas.Add(vaTestObservasi);

                    ret.Observasis.Add(observasiKuota);
                    ret.Observasis.Add(observasiDiterima);
                    ret.Observasis.Add(observasiDitangguhkan);
                    ret.Observasis.Add(observasiTidakDiterima);

                    ret.Bayars.Add(bayarTotal);
                    ret.Bayars.Add(bayarCicil);
                    ret.Bayars.Add(bayarTidak);

                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetDashboardPppdb", ex);
                return null;
            }
        }

        public List<KalenderPendidikanModel> GetKalenderPendidikans(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbKalenderPendidikans = conn.GetList<TbKalenderPendidikan>().ToList();
                    if (tbKalenderPendidikans.Count == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    List<KalenderPendidikanModel> ret = new();
                    foreach (var item in tbKalenderPendidikans.OrderByDescending(x => x.IdKalenderPendidikan))
                    {
                        ret.Add(new KalenderPendidikanModel
                        {
                            IdKalenderPendidikan = item.IdKalenderPendidikan,
                            Keterangan = item.Keterangan,
                            Semester = item.Semester,
                            FileImage = item.FileImage
                        });
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKalenderPendidikans", ex);
                return null;
            }
        }

        public KalenderPendidikanModel GetKalenderPendidikan(int IdKalenderPendidikan, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbKalenderPendidikan = conn.GetList<TbKalenderPendidikan>().Where(x => x.IdKalenderPendidikan == IdKalenderPendidikan).FirstOrDefault();
                    if (tbKalenderPendidikan == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    KalenderPendidikanModel ret = new KalenderPendidikanModel
                    {
                        IdKalenderPendidikan = tbKalenderPendidikan.IdKalenderPendidikan,
                        Keterangan = tbKalenderPendidikan.Keterangan,
                        Semester = tbKalenderPendidikan.Semester,
                        FileImage = tbKalenderPendidikan.FileImage
                    };
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKalenderPendidikan", ex);
                return null;
            }
        }

        public string KalenderPendidikanAdd(KalenderPendidikanAddEditModel Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    TbKalenderPendidikan tbKalenderPendidikan = new TbKalenderPendidikan
                    {
                        Semester = Data.Semester,
                        Keterangan = Data.Keterangan,
                        FileImage = null
                    };
                    if (Data.FileImage != null)
                    {
                        tbKalenderPendidikan.FileImage = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KalenderPendidikan] + Path.GetExtension(Data.FileImage.FileName);
                    }
                    else
                    {
                        oMessage = "foto kalender wajib diupload!";
                        return null;
                    }

                    conn.Insert(tbKalenderPendidikan);

                    var pathUpload = AppSetting.PathApplImage;
                    if (Data.FileImage != null)
                    {
                        using (var fileStream = new FileStream(Path.Combine(pathUpload, tbKalenderPendidikan.FileImage), FileMode.Create, FileAccess.Write))
                        {
                            Data.FileImage.CopyTo(fileStream);
                        }
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "KalenderPendidikanAdd", ex);
                return null;
            }
        }

        public string KalenderPendidikanEdit(KalenderPendidikanAddEditModel Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbKalenderPendidikan = conn.GetList<TbKalenderPendidikan>().Where(x => x.IdKalenderPendidikan == Data.IdKalenderPendidikan).FirstOrDefault();
                    if (tbKalenderPendidikan == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    tbKalenderPendidikan.Semester = Data.Semester;
                    tbKalenderPendidikan.Keterangan = Data.Keterangan;
                    if (Data.FileImage != null)
                    {
                        tbKalenderPendidikan.FileImage = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KalenderPendidikan] + Path.GetExtension(Data.FileImage.FileName);
                    }
                    conn.Update(tbKalenderPendidikan);

                    var pathUpload = AppSetting.PathApplImage;
                    if (Data.FileImage != null)
                    {
                        if (File.Exists(Path.Combine(pathUpload, tbKalenderPendidikan.FileImage)))
                            File.Delete(Path.Combine(pathUpload, tbKalenderPendidikan.FileImage));

                        using (var fileStream = new FileStream(Path.Combine(pathUpload, tbKalenderPendidikan.FileImage), FileMode.Create, FileAccess.Write))
                        {
                            Data.FileImage.CopyTo(fileStream);
                        }
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "KalenderPendidikanEdit", ex);
                return null;
            }
        }

        public string KalenderPendidikanDelete(int IdKalenderPendidikan, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbKalenderPendidikan = conn.GetList<TbKalenderPendidikan>().Where(x => x.IdKalenderPendidikan == IdKalenderPendidikan).FirstOrDefault();
                    if (tbKalenderPendidikan == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    conn.Delete(tbKalenderPendidikan);

                    var pathUpload = AppSetting.PathApplImage;
                    if (File.Exists(Path.Combine(pathUpload, tbKalenderPendidikan.FileImage)))
                        File.Delete(Path.Combine(pathUpload, tbKalenderPendidikan.FileImage));

                    return null;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "KalenderPendidikanDelete", ex);
                return null;
            }
        }

        public List<SekolahFasilitasModel> GetSekolahFasilitass(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    var tbSekolahFasilitas = conn.GetList<TbSekolahFasilitas>().ToList();
                    if (tbSekolahFasilitas.Count == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }

                    List<SekolahFasilitasModel> ret = new List<SekolahFasilitasModel>();
                    foreach (var item in tbSekolahFasilitas)
                    {
                        ret.Add(new SekolahFasilitasModel
                        {
                            IdSekolahFasilitas = item.IdSekolahFasilitas,
                            Judul = item.Judul,
                            Deskripsi = item.Deskripsi,
                            FileImage = item.FileImage
                        });
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSekolahFasilitass", ex);
                return null;
            }
        }
        public SekolahFasilitasModel GetSekolahFasilitas(int IdSekolahFasilitas, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    var tbSekolahFasilitas = conn.GetList<TbSekolahFasilitas>().Where(x => x.IdSekolahFasilitas == IdSekolahFasilitas).FirstOrDefault();
                    if (tbSekolahFasilitas == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }

                    SekolahFasilitasModel ret = new SekolahFasilitasModel
                    {
                        IdSekolahFasilitas = tbSekolahFasilitas.IdSekolahFasilitas,
                        Judul = tbSekolahFasilitas.Judul,
                        Deskripsi = tbSekolahFasilitas.Deskripsi,
                        FileImage = tbSekolahFasilitas.FileImage
                    };
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSekolahFasilitas", ex);
                return null;
            }
        }
        public string SekolahFasilitasAdd(SekolahFasilitasAddEditModel Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    TbSekolahFasilitas tbSekolahFasilitas = new TbSekolahFasilitas
                    {
                        Judul = Data.Judul,
                        Deskripsi = Data.Deskripsi
                    };
                    if (Data.FileImage != null)
                    {
                        tbSekolahFasilitas.FileImage = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.FasilitasSekolah] + Path.GetExtension(Data.FileImage.FileName);
                    }
                    else
                    {
                        oMessage = "foto fasilitas wajib diupload!";
                        return null;
                    }

                    conn.Insert(tbSekolahFasilitas);

                    var pathUpload = AppSetting.PathFasilitasSekolah;
                    if (Data.FileImage != null)
                    {
                        using (var fileStream = new FileStream(Path.Combine(pathUpload, tbSekolahFasilitas.FileImage), FileMode.Create, FileAccess.Write))
                        {
                            Data.FileImage.CopyTo(fileStream);
                        }
                    }
                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "SekolahFasilitasAdd", ex);
                return null;
            }
        }
        public string SekolahFasilitasEdit(SekolahFasilitasAddEditModel Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    var tbSekolahFasilitas = conn.GetList<TbSekolahFasilitas>().Where(x => x.IdSekolahFasilitas == Data.IdSekolahFasilitas).FirstOrDefault();
                    if (tbSekolahFasilitas == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    tbSekolahFasilitas.Judul = Data.Judul;
                    tbSekolahFasilitas.Deskripsi = Data.Deskripsi;
                    if (Data.FileImage != null)
                        tbSekolahFasilitas.FileImage = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.FasilitasSekolah] + Path.GetExtension(Data.FileImage.FileName);

                    conn.Update(tbSekolahFasilitas);

                    var pathUpload = AppSetting.PathFasilitasSekolah;
                    if (Data.FileImage != null)
                    {
                        if (File.Exists(Path.Combine(pathUpload, tbSekolahFasilitas.FileImage)))
                            File.Delete(Path.Combine(pathUpload, tbSekolahFasilitas.FileImage));

                        using (var fileStream = new FileStream(Path.Combine(pathUpload, tbSekolahFasilitas.FileImage), FileMode.Create, FileAccess.Write))
                        {
                            Data.FileImage.CopyTo(fileStream);
                        }
                    }

                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "SekolahFasilitasEdit", ex);
                return null;
            }
        }
        public string SekolahFasilitasDelete(int IdSekolahFasilitas, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    var tbSekolahFasilitas = conn.GetList<TbSekolahFasilitas>().Where(x => x.IdSekolahFasilitas == IdSekolahFasilitas).FirstOrDefault();
                    if (tbSekolahFasilitas == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    conn.Delete(tbSekolahFasilitas);
                    var pathUpload = AppSetting.PathFasilitasSekolah;
                    if (File.Exists(Path.Combine(pathUpload, tbSekolahFasilitas.FileImage)))
                        File.Delete(Path.Combine(pathUpload, tbSekolahFasilitas.FileImage));
                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "SekolahFasilitasDelete", ex);
                return null;
            }
        }


        public List<SekolahPrestasiModel> GetSekolahPrestasis(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    var tbSekolahPrestasi = conn.GetList<TbSekolahPrestasi>().ToList();
                    if (tbSekolahPrestasi.Count == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }

                    List<SekolahPrestasiModel> ret = new List<SekolahPrestasiModel>();
                    foreach (var item in tbSekolahPrestasi)
                    {
                        ret.Add(new SekolahPrestasiModel
                        {
                            IdSekolahPrestasi = item.IdSekolahPrestasi,
                            IdJenisGaleri = item.IdJenisGaleri,
                            JenisGaleri = JenisGaleriConstant.DictJenisGaleri[item.IdJenisGaleri],
                            Judul = item.Judul,
                            Keterangan = item.Keterangan,
                            FileUrl = item.FileUrl
                        });
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSekolahPrestasis", ex);
                return null;
            }
        }
        public SekolahPrestasiModel GetSekolahPrestasi(int IdSekolahPrestasi, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    var tbSekolahPrestasi = conn.GetList<TbSekolahPrestasi>().Where(x => x.IdSekolahPrestasi == IdSekolahPrestasi).FirstOrDefault();
                    if (tbSekolahPrestasi == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }

                    SekolahPrestasiModel ret = new SekolahPrestasiModel
                    {
                        IdSekolahPrestasi = tbSekolahPrestasi.IdSekolahPrestasi,
                        IdJenisGaleri = tbSekolahPrestasi.IdJenisGaleri,
                        JenisGaleri = JenisGaleriConstant.DictJenisGaleri[tbSekolahPrestasi.IdJenisGaleri],
                        Judul = tbSekolahPrestasi.Judul,
                        Keterangan = tbSekolahPrestasi.Keterangan,
                        FileUrl = tbSekolahPrestasi.FileUrl
                    };
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSekolahPrestasi", ex);
                return null;
            }
        }
        public string SekolahPrestasiAdd(int IdUser, SekolahPrestasiAddEditModel Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    TbSekolahPrestasi tbSekolahPrestasi = new TbSekolahPrestasi
                    {
                        Judul = Data.Judul,
                        IdJenisGaleri = Data.IdJenisGaleri,
                        Keterangan = Data.Keterangan,
                        CreatedBy = IdUser,
                        CreatedDate = dateTimeService.GetCurrdate()
                    };

                    if (Data.IdJenisGaleri == JenisGaleriConstant.Image)
                    {
                        if (Data.FileImage != null)
                        {
                            tbSekolahPrestasi.FileUrl = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.PrestasiSekolah] + Path.GetExtension(Data.FileImage.FileName);
                        }
                        else
                        {
                            oMessage = "foto Prestasi wajib diupload!";
                            return null;
                        }
                    }
                    if (Data.IdJenisGaleri == JenisGaleriConstant.Video)
                    {
                        if (string.IsNullOrEmpty(Data.FileUrl))
                        {
                            oMessage = "URL video wajib di isi!";
                            return null;
                        }
                        else
                        {
                            tbSekolahPrestasi.FileUrl = Data.FileUrl;
                        }
                    }

                    conn.Insert(tbSekolahPrestasi);

                    var pathUpload = AppSetting.PathPrestasiSekolah;
                    if (Data.FileImage != null)
                    {
                        using (var fileStream = new FileStream(Path.Combine(pathUpload, tbSekolahPrestasi.FileUrl), FileMode.Create, FileAccess.Write))
                        {
                            Data.FileImage.CopyTo(fileStream);
                        }
                    }
                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "SekolahPrestasiAdd", ex);
                return null;
            }
        }

        public string SekolahPrestasiEdit(int IdUser, SekolahPrestasiAddEditModel Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    var tbSekolahPrestasi = conn.GetList<TbSekolahPrestasi>().Where(x => x.IdSekolahPrestasi == Data.IdSekolahPrestasi).FirstOrDefault();
                    if (tbSekolahPrestasi == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    tbSekolahPrestasi.Judul = Data.Judul;
                    tbSekolahPrestasi.Keterangan = Data.Keterangan;
                    tbSekolahPrestasi.UpdateBy = IdUser;
                    tbSekolahPrestasi.UpdatedDate = dateTimeService.GetCurrdate();

                    var pathUpload = AppSetting.PathPrestasiSekolah;
                    if (Data.IdJenisGaleri == JenisGaleriConstant.Image)
                    {
                        if (Data.FileImage != null)
                        {
                            tbSekolahPrestasi.FileUrl = Path.GetRandomFileName() + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.PrestasiSekolah] + Path.GetExtension(Data.FileImage.FileName);
                        }
                    }
                    if (Data.IdJenisGaleri == JenisGaleriConstant.Video)
                    {
                        if (string.IsNullOrEmpty(Data.FileUrl))
                        {
                            oMessage = "URL video wajib di isi!";
                            return null;
                        }
                        else
                        {
                            if (File.Exists(Path.Combine(pathUpload, tbSekolahPrestasi.FileUrl)))
                                File.Delete(Path.Combine(pathUpload, tbSekolahPrestasi.FileUrl));

                            tbSekolahPrestasi.FileUrl = Data.FileUrl;
                        }
                    }

                    conn.Update(tbSekolahPrestasi);

                    if (Data.FileImage != null)
                    {
                        if (File.Exists(Path.Combine(pathUpload, tbSekolahPrestasi.FileUrl)))
                            File.Delete(Path.Combine(pathUpload, tbSekolahPrestasi.FileUrl));

                        using (var fileStream = new FileStream(Path.Combine(pathUpload, tbSekolahPrestasi.FileUrl), FileMode.Create, FileAccess.Write))
                        {
                            Data.FileImage.CopyTo(fileStream);
                        }
                    }

                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "SekolahPrestasiEdit", ex);
                return null;
            }
        }

        public string SekolahPrestasiDelete(int IdSekolahPrestasi, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    var tbSekolahPrestasi = conn.GetList<TbSekolahPrestasi>().Where(x => x.IdSekolahPrestasi == IdSekolahPrestasi).FirstOrDefault();
                    if (tbSekolahPrestasi == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    conn.Delete(tbSekolahPrestasi);
                    if (tbSekolahPrestasi.IdJenisGaleri == JenisGaleriConstant.Image)
                    {
                        var pathUpload = AppSetting.PathPrestasiSekolah;
                        if (File.Exists(Path.Combine(pathUpload, tbSekolahPrestasi.FileUrl)))
                            File.Delete(Path.Combine(pathUpload, tbSekolahPrestasi.FileUrl));
                    }
                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "SekolahPrestasiDelete", ex);
                return null;
            }
        }
    }
}
