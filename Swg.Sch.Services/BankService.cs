using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Sch.Shared.Constants;
using System.Globalization;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using Swg.Models;
using Swg.Entities.Etc;
using Swg.Entities.Kbm;
using Swg.Entities.Ppdb;
using Swg.Services;

namespace Swg.Sch.Services
{
    public class BankErrorCodeConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>()
        {
            {1000, "Partner/Client tidak terdaftar"},
            {1100, "Password salah"},
            {1200, "Konfirmasi password tidak sesuai"},
            {1300, "Partner/Client harus login"},
            {2000, "Nis Siswa tidak terdaftar"},
            {3000, "No PPDB (VA) tidak terdaftar"},
            {9100, "Tagihan tidak/belum ada"},
            {9200, "Nilai Tagihan tidak sesuai"},
            {9310, "Tagihan Sudah Lunas"},
            {9320, "Tagihan Belum Lunas"},
            {9400, "No Referensi Pembayaran Tidak Sesuai"},
            {9500,"Tagihan Tidak Dapat Dibatalkan"},
            {9999, "Error tidak terdeteksi"},
        };
    }

    public class BankService : IBankService
    {
        private readonly ICommonService common;
        private readonly ISchService schService;
        private readonly ISiswaService siswaService;
        private readonly IDateTimeService dateTimeService;
        private readonly string KeyString = AppSetting.Secret;
        private string CekPartner(IDbConnection conn, string KdPartner, out int IdPartner)
        {
            IdPartner = 0;
            try
            {
                var partner = (from a in conn.GetList<TbPartner>()
                               where a.KdPartner == KdPartner
                               select a).FirstOrDefault();
                if (partner == null) return BankErrorCodeConstant.Dict[1000];
                if (partner.IsLogin != 1) return BankErrorCodeConstant.Dict[1300];
                TimeSpan span = dateTimeService.GetCurrdate().Subtract((DateTime)partner.LastLogin);
                if (span.Hours > 1) return BankErrorCodeConstant.Dict[1300];
                IdPartner = partner.IdUser;
                return string.Empty;
            }
            catch (System.Exception)
            {
                return BankErrorCodeConstant.Dict[9999];
            }

        }
        public BankService(
            ICommonService CommonService,
            ISchService SchService,
            ISiswaService SiswaService,
            IDateTimeService DateTimeService
            )
        {
            schService = SchService;
            siswaService = SiswaService;
            common = CommonService;
            dateTimeService = DateTimeService;
        }

        public static string Encrypt(string clearText)
        {
            string EncryptionKey = AppSetting.Secret;
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = AppSetting.Secret;
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
        private string SetRefno(string KdPartner, string Nis)
        {
            // string refNo = string.Format("{0} {1} {2}", dateTimeService.GetCurrdate().ToString("yyyyMMddHH24mmss"), Nis, KdPartner);
            // return Encrypt(refNo);
            string refNo = System.Guid.NewGuid().ToString();
            return refNo;
        }
        private bool IsRefNo(IDbConnection conn, string RefNo, string KdPartner, string Nis, DateTime date)
        {

            // string refNo = Decrypt(RefNo);
            // string[] refNox = refNo.Split(" ");
            // if (refNox.Length != 3) return false;
            // if (refNox[0] != date.ToString("yyyyMMddHH24mmss")) return false;
            // if (refNox[1] != Nis) return false;
            // if (refNox[2] != KdPartner) return false;
            var cekRefNo = conn.GetList<TbSiswaTagihan>().Where(x => x.RefNo == RefNo).FirstOrDefault();
            if (cekRefNo == null)
                return false;
            return true;
        }
        public string PartnerLogin(string PartnerId, string Password)
        {
            if (string.IsNullOrEmpty(PartnerId)) return BankErrorCodeConstant.Dict[1000];
            if (string.IsNullOrEmpty(Password)) return BankErrorCodeConstant.Dict[1100];
            try
            {
                using (IDbConnection conn = common.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var item = (from a in conn.GetList<TbPartner>()
                                    where a.KdPartner == PartnerId
                                    select a).FirstOrDefault();
                        if (item == null)
                        {
                            return BankErrorCodeConstant.Dict[1000];
                        }
                        var x = common.DecryptString(item.Password);
                        if (common.DecryptString(item.Password) != Password) return BankErrorCodeConstant.Dict[1100];
                        item.IsLogin = 1;
                        item.LastLogin = dateTimeService.GetCurrdate();
                        conn.Update(item);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return BankErrorCodeConstant.Dict[9999];
            }
        }

        public string PartnerLogout(string PartnerId)
        {
            if (string.IsNullOrEmpty(PartnerId)) return BankErrorCodeConstant.Dict[1000];
            try
            {
                using (IDbConnection conn = common.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var item = (from a in conn.GetList<TbPartner>()
                                    where a.KdPartner == PartnerId
                                    select a).FirstOrDefault();
                        if (item == null)
                        {
                            return BankErrorCodeConstant.Dict[1000];
                        }
                        item.IsLogin = 0;
                        conn.Update(item);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception)
            {
                return BankErrorCodeConstant.Dict[9999];
            }
        }

        public string PartnerChangePassword(string PartnerId, string OldPassword, string NewPassword, string ConfirmPassword)
        {
            if (string.IsNullOrEmpty(PartnerId)) return BankErrorCodeConstant.Dict[1000];
            if (string.IsNullOrEmpty(OldPassword)) return BankErrorCodeConstant.Dict[1100];
            if (string.IsNullOrEmpty(NewPassword)) return BankErrorCodeConstant.Dict[1200];
            if (string.IsNullOrEmpty(ConfirmPassword)) return BankErrorCodeConstant.Dict[1200];
            if (NewPassword != ConfirmPassword) return BankErrorCodeConstant.Dict[1200];
            try
            {
                using (IDbConnection conn = common.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var item = (from a in conn.GetList<TbPartner>()
                                    where a.KdPartner == PartnerId
                                    select a).FirstOrDefault();
                        if (item == null)
                        {
                            return BankErrorCodeConstant.Dict[1000];
                        }
                        if (common.DecryptString(item.Password) != OldPassword) return BankErrorCodeConstant.Dict[1100];
                        item.IsLogin = 0;
                        item.Password = common.EncryptString(NewPassword);
                        conn.Update(item);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception)
            {
                return BankErrorCodeConstant.Dict[9999];
            }
        }

        public string GetInfoSppSiswa(string PartnerId, string Nis, out PartnerSppModel Data)
        {
            Data = new PartnerSppModel
            {
                TagBerjalan = new List<PartnerSppTagModel>(),
                TagSebelumnya = new List<PartnerSppTagModel>()

            };
            try
            {
                using (IDbConnection conn = common.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        //cek login
                        string err = CekPartner(conn, PartnerId, out int IdPartner);
                        if (!string.IsNullOrEmpty(err)) return err;

                        //get info
                        var siswa = (from a in conn.GetList<TbSiswa>()
                                     join b in conn.GetList<TbKelasRombel>() on a.IdSiswa equals b.IdSiswa
                                     join c in conn.GetList<TbKelasParalel>() on b.IdKelasParalel equals c.IdKelasParalel
                                     where a.Nis == Nis || a.NisLama == Nis
                                     select new { a, c }).FirstOrDefault();
                        if (siswa == null) return BankErrorCodeConstant.Dict[2000];
                        var tagSiswa = schService.GetSiswaTagihan(conn, Nis, out err);
                        if (!string.IsNullOrEmpty(err)) return err;

                        bool isOpen = false;
                        if (dateTimeService.GetCurrdate().Date >= tagSiswa.Min.Date && dateTimeService.GetCurrdate().Date <= tagSiswa.Max.Date) isOpen = true;
                        if (!isOpen) return "Waktu pembayaran sudah habis";

                        Data.Nis = siswa.a.Nis;
                        Data.Nama = siswa.a.Nama;
                        Data.TanggalLahir = siswa.a.TanggalLahir.ToString("dd-MM-yyyy");
                        Data.Kelas = siswa.c.Nama;
                        Data.Bulan = tagSiswa.Periode;
                        Data.RpTotal = tagSiswa.Status == StatusLunasConstant.Dict[StatusLunasConstant.Lunas] ? 0 : tagSiswa.RpTotal;
                        Data.RpBerjalan = tagSiswa.Status == StatusLunasConstant.Dict[StatusLunasConstant.Lunas] ? 0 : tagSiswa.RpBerjalan;
                        Data.RpSebelumnya = tagSiswa.Status == StatusLunasConstant.Dict[StatusLunasConstant.Lunas] ? 0 : tagSiswa.RpSebelumnya;
                        Data.Status = tagSiswa.Status;
                        Data.TagBerjalan = new List<PartnerSppTagModel>();
                        Data.TagSebelumnya = new List<PartnerSppTagModel>();

                        if (tagSiswa.Status != StatusLunasConstant.Dict[StatusLunasConstant.Lunas])
                        {
                            foreach (var item in tagSiswa.TagihanBerjalan.OrderBy(x => x.JenisTagihan))
                            {
                                Data.TagBerjalan.Add(new PartnerSppTagModel
                                {
                                    JenisTagihan = item.JenisTagihan,
                                    Rp = item.Rp
                                });
                            }
                            if (Data.TagBerjalan.Count == 0) Data.RpBerjalan = 0;

                            foreach (var item in tagSiswa.TagihanSebelumnya.OrderBy(x => x.JenisTagihan))
                            {
                                Data.TagSebelumnya.Add(new PartnerSppTagModel
                                {
                                    JenisTagihan = item.JenisTagihan,
                                    Rp = item.Rp
                                });
                            }
                            if (Data.TagSebelumnya.Count == 0) Data.RpSebelumnya = 0;

                            Data.RpTotal = tagSiswa.RpSebelumnya + tagSiswa.RpBerjalan;
                        }


                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return BankErrorCodeConstant.Dict[9999];
            }
        }

        public string SetLunasSppSiswa(string PartnerId, string Nis, double Rp, out string RefNo)
        {
            RefNo = string.Empty;
            try
            {
                using (IDbConnection conn = common.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string err = CekPartner(conn, PartnerId, out int IdPartner);
                        if (!string.IsNullOrEmpty(err)) return err;

                        var siswa = (from a in conn.GetList<TbSiswa>()
                                     where a.Nis == Nis || a.NisLama == Nis
                                     select a).FirstOrDefault();
                        if (siswa == null) return BankErrorCodeConstant.Dict[2000];

                        var tagSiswa = schService.GetSiswaTagihan(conn, Nis, out err);
                        if (!string.IsNullOrEmpty(err)) return err;

                        bool isOpen = false;
                        if (dateTimeService.GetCurrdate().Date >= tagSiswa.Min.Date && dateTimeService.GetCurrdate().Date <= tagSiswa.Max.Date) isOpen = true;
                        if (!isOpen) return "Waktu pembayaran sudah habis";

                        if (tagSiswa.RpTotal != Rp) return BankErrorCodeConstant.Dict[9200];
                        if (tagSiswa.Status == StatusLunasConstant.Dict[StatusLunasConstant.Lunas]) return BankErrorCodeConstant.Dict[9310];

                        // var tagihanDetil = (from a in conn.GetList<TbSiswaTagihanDetil>()
                        //                     where a.IdSiswa == siswa.IdSiswa
                        //                     select a).ToList();
                        // if (tagihanDetil.Count == 0) return BankErrorCodeConstant.Dict[2000];

                        // foreach (var item in tagihanDetil)
                        // {
                        //     item.Rp = 0;
                        //     conn.Update(item);
                        // }

                        RefNo = SetRefno(PartnerId, Nis);
                        // var decrypts = Decrypt(RefNo);
                        TbSiswaTagihan tbSiswaTagihan = conn.Get<TbSiswaTagihan>(siswa.IdSiswa);
                        tbSiswaTagihan.Status = StatusLunasConstant.Lunas;
                        tbSiswaTagihan.UpdatedBy = IdPartner;
                        tbSiswaTagihan.UpdatedDate = dateTimeService.GetCurrdate();
                        tbSiswaTagihan.RefNo = RefNo;
                        conn.Update(tbSiswaTagihan);
                        tx.Commit();
                        // var notifSiswa = siswaService.PushInboxNotif(
                        //     siswa.IdSiswa,
                        //     "Pembayaran Sukses",
                        //     "(Jam : " + dateTimeService.GetCurrdate().ToString("HH:mm") + ") Terimakasih anda telah melakukan pembayaran.",
                        //     TipeInboxConstant.Tagihan,
                        //     null
                        // );
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return BankErrorCodeConstant.Dict[9999];
            }
        }

        public string SetBatalSppSiswa(string PartnerId, string Nis, double Rp, string RefNo)
        {
            try
            {
                using (IDbConnection conn = common.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string err = CekPartner(conn, PartnerId, out int IdPartner);
                        if (!string.IsNullOrEmpty(err)) return err;

                        var siswa = (from a in conn.GetList<TbSiswa>()
                                     where a.Nis == Nis || a.NisLama == Nis
                                     select a).FirstOrDefault();
                        if (siswa == null) return BankErrorCodeConstant.Dict[2000];

                        var tagSiswa = schService.GetSiswaTagihan(conn, Nis, out err);
                        if (!string.IsNullOrEmpty(err)) return err;

                        if (tagSiswa.RpTotal != Rp) return BankErrorCodeConstant.Dict[9200];
                        if (tagSiswa.Status == StatusLunasConstant.Dict[StatusLunasConstant.BelumLunas]) return BankErrorCodeConstant.Dict[9320];

                        DateTime tanggalLunas = DateTime.ParseExact(tagSiswa.TanggalLunas, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        if (!IsRefNo(conn, RefNo, PartnerId, Nis, tanggalLunas)) return BankErrorCodeConstant.Dict[9400];

                        TbSiswaTagihan tbSiswaTagihan = conn.Get<TbSiswaTagihan>(siswa.IdSiswa);
                        tbSiswaTagihan.Status = StatusLunasConstant.BelumLunas;
                        tbSiswaTagihan.UpdatedBy = IdPartner;
                        tbSiswaTagihan.UpdatedDate = dateTimeService.GetCurrdate();
                        tbSiswaTagihan.RefNo = null;
                        conn.Update(tbSiswaTagihan);
                        tx.Commit();
                        var notifSiswa = siswaService.PushInboxNotif(
                           siswa.IdSiswa,
                           "Pembayaran Dibatalkan",
                           "(Jam : " + dateTimeService.GetCurrdate().ToString("HH:mm") + ") Terjadi kesalahan, pembayaran anda telah dibatalkan.",
                           TipeInboxConstant.Tagihan,
                           null
                       );
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return BankErrorCodeConstant.Dict[9999];
            }

        }

        public string GetInfoTagihanPpdb(string PartnerId, string NoVa, out PartnerPpdbModel Data)
        {
            Data = new PartnerPpdbModel();
            try
            {
                using (IDbConnection conn = common.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string err = CekPartner(conn, PartnerId, out int IdPartner);
                        if (!string.IsNullOrEmpty(err)) return err;
                        int idPpdbDaftar = 0;
                        try
                        {
                            idPpdbDaftar = int.Parse(NoVa);
                        }
                        catch (Exception)
                        {
                            return BankErrorCodeConstant.Dict[3000];
                        }
                        var ppdb = (from a in conn.GetList<TbPpdbDaftar>()
                                    where a.IdPpdbDaftar == idPpdbDaftar
                                    select a).FirstOrDefault();
                        if (ppdb == null) return BankErrorCodeConstant.Dict[3000];

                        var tagPpdbDaftar = schService.GetPpdbDaftarBiaya(conn, idPpdbDaftar, out err);
                        if (!string.IsNullOrEmpty(err)) return err;
                        if (tagPpdbDaftar.StatusLunas == StatusLunasConstant.Dict[StatusLunasConstant.Lunas])
                            return BankErrorCodeConstant.Dict[9310];
                        Data = new PartnerPpdbModel
                        {
                            Kelas = tagPpdbDaftar.Kelas,
                            Nama = tagPpdbDaftar.Nama,
                            NoVa = tagPpdbDaftar.IdPpdbDaftar.ToString(),
                            RpTotal = tagPpdbDaftar.RpTotal,
                            Status = tagPpdbDaftar.StatusLunas,
                            TanggalLahir = tagPpdbDaftar.TanggalLahir,
                            Unit = tagPpdbDaftar.Unit,
                            TagPpdb = new List<PartnerTagPpdbModel>()
                        };
                        foreach (var item in tagPpdbDaftar.TagPpdb)
                        {
                            Data.TagPpdb.Add(new PartnerTagPpdbModel
                            {
                                JenisTagihan = item.JenisTagihan,
                                Rp = item.Rp
                            });
                        }
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return BankErrorCodeConstant.Dict[9999];
            }

        }

        public string SetLunasTagihanPpdb(string PartnerId, string NoVa, double Rp, out string RefNo)
        {
            RefNo = string.Empty;
            try
            {
                using (IDbConnection conn = common.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string err = CekPartner(conn, PartnerId, out int IdPartner);
                        if (!string.IsNullOrEmpty(err)) return err;

                        int idPpdbDaftar = 0;
                        try
                        {
                            idPpdbDaftar = int.Parse(NoVa);
                        }
                        catch (Exception)
                        {
                            return BankErrorCodeConstant.Dict[3000];
                        }
                        var ppdb = (from a in conn.GetList<TbPpdbDaftar>()
                                    where a.IdPpdbDaftar == idPpdbDaftar
                                    select a).FirstOrDefault();
                        if (ppdb == null) return BankErrorCodeConstant.Dict[3000];

                        var tagPpdbDaftar = schService.GetPpdbDaftarBiaya(conn, idPpdbDaftar, out err);
                        if (!string.IsNullOrEmpty(err)) return BankErrorCodeConstant.Dict[9999];
                        if (tagPpdbDaftar.StatusLunas == StatusLunasConstant.Dict[StatusLunasConstant.Lunas])
                            return BankErrorCodeConstant.Dict[9310];
                        if (tagPpdbDaftar.RpTotal != Rp) return BankErrorCodeConstant.Dict[9200];
                        RefNo = SetRefno(PartnerId, NoVa);
                        err = schService.SetLunasBiayaDaftar(conn, IdPartner, RefNo, idPpdbDaftar, dateTimeService.GetCurrdate(), Rp, string.Empty);
                        if (!string.IsNullOrEmpty(err)) return BankErrorCodeConstant.Dict[9999];
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return BankErrorCodeConstant.Dict[9999];
            }

        }

        public string SetBatalTagihanPpdb(string PartnerId, string NoVa, double Rp, string RefNo)
        {
            try
            {
                using (IDbConnection conn = common.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string err = CekPartner(conn, PartnerId, out int IdPartner);
                        if (!string.IsNullOrEmpty(err)) return err;
                        int idPpdbDaftar = 0;
                        try
                        {
                            idPpdbDaftar = int.Parse(NoVa);
                        }
                        catch (Exception)
                        {
                            return BankErrorCodeConstant.Dict[3000];
                        }
                        var tagPpdbDaftar = schService.GetPpdbDaftarBiaya(conn, idPpdbDaftar, out err);
                        if (!string.IsNullOrEmpty(err)) return BankErrorCodeConstant.Dict[9999];
                        if (tagPpdbDaftar.StatusLunas == StatusLunasConstant.Dict[StatusLunasConstant.BelumLunas])
                            return BankErrorCodeConstant.Dict[9320];
                        if (tagPpdbDaftar.RpTotal != Rp) return BankErrorCodeConstant.Dict[9200];

                        var tbPpdbSiswa = conn.Get<TbPpdbSiswa>(idPpdbDaftar);
                        if (tbPpdbSiswa != null)
                            return BankErrorCodeConstant.Dict[9500];

                        DateTime tanggalLunas = DateTime.ParseExact(tagPpdbDaftar.TanggalLunas, "dd-MM-yyyy HH24:mm:ss", CultureInfo.InvariantCulture);
                        if (!IsRefNo(conn, RefNo, PartnerId, NoVa, tanggalLunas)) return BankErrorCodeConstant.Dict[9400];

                        TbPpdbBiayaDaftar tbPpdbDaftar = conn.Get<TbPpdbBiayaDaftar>(idPpdbDaftar);
                        tbPpdbDaftar.Status = StatusLunasConstant.BelumLunas;
                        tbPpdbDaftar.UpdatedBy = IdPartner;
                        tbPpdbDaftar.UpdatedDate = dateTimeService.GetCurrdate();
                        tbPpdbDaftar.RefNo = null;
                        conn.Update(tbPpdbDaftar);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return BankErrorCodeConstant.Dict[9999];
            }
        }

        public string GetInfoTagihanSiswaBaru(string PartnerId, string NoVa, out PpdbBiayaDaftarModel Data)
        {
            Data = new PpdbBiayaDaftarModel();
            try
            {
                using (IDbConnection conn = common.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string err = CekPartner(conn, PartnerId, out int IdPartner);
                        if (!string.IsNullOrEmpty(err)) return err;
                        int idPpdbDaftar = 0;
                        try
                        {
                            idPpdbDaftar = int.Parse(NoVa);
                        }
                        catch (Exception)
                        {
                            return BankErrorCodeConstant.Dict[3000];
                        }
                        var tagPpdbSiswa = schService.GetPpdbSiswa(conn, idPpdbDaftar, out err);
                        if (!string.IsNullOrEmpty(err))
                            return BankErrorCodeConstant.Dict[3000];

                        if (tagPpdbSiswa.StatusBiaya == StatusLunasConstant.Dict[StatusLunasConstant.Lunas])
                            return BankErrorCodeConstant.Dict[9310];

                        var tot = tagPpdbSiswa.RpBiayaTotal;
                        int? cilKe = null;
                        if (tagPpdbSiswa.IsCicil == 1)
                        {
                            foreach (var item in tagPpdbSiswa.TagCicil.OrderBy(x => x.CilKe))
                            {
                                if (item.Status != StatusLunasConstant.Dict[StatusLunasConstant.Lunas])
                                {
                                    tot = item.RpCilKe;
                                    cilKe = item.CilKe;
                                    break;
                                }
                            }
                        }

                        Data = new PpdbBiayaDaftarModel
                        {
                            Kelas = tagPpdbSiswa.Kelas,
                            Nama = tagPpdbSiswa.Nama,
                            NoVa = tagPpdbSiswa.IdPpdbDaftar.ToString(),
                            RpTotal = tot,
                            Status = tagPpdbSiswa.StatusBiaya,
                            StatusLunas = tagPpdbSiswa.StatusBiaya,
                            TanggalLahir = tagPpdbSiswa.TanggalLahir,
                            Unit = tagPpdbSiswa.Unit,
                            Catatan = string.Empty,
                            CilKe = cilKe,
                            TagPpdb = new List<PpdbBiayaDaftarDetilModel>()
                        };
                        foreach (var item in tagPpdbSiswa.TagPpdb)
                        {
                            Data.TagPpdb.Add(new PpdbBiayaDaftarDetilModel
                            {
                                JenisTagihan = item.JenisBiayaPpdb,
                                Rp = item.Rp
                            });
                        }
                        double rpCilKe = 0;
                        //foreach (var item in tagPpdbSiswa.TagCicil.OrderBy(x=>x.CilKe))
                        //{
                        //    if (rpCilKe == 0)
                        //    {
                        //        if (item.Status == StatusLunasConstant.Dict[StatusLunasConstant.BelumLunas])
                        //        {
                        //            rpCilKe = item.RpCilKe;
                        //            Data.Status = item.Status;
                        //            Data.RpTotal = item.RpCilKe;
                        //            Data.Catatan = "Cicilan Ke " + item.CilKe;
                        //        }
                        //    }
                        //}
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return BankErrorCodeConstant.Dict[9999];
            }

        }
        public string SetLunasTagihanSiswaBaru(string PartnerId, string NoVa, double Rp, out string RefNo)
        {
            RefNo = string.Empty;
            try
            {
                using (IDbConnection conn = common.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string err = CekPartner(conn, PartnerId, out int IdPartner);
                        if (!string.IsNullOrEmpty(err)) return err;

                        int idPpdbDaftar = 0;
                        try
                        {
                            idPpdbDaftar = int.Parse(NoVa);
                        }
                        catch (Exception)
                        {
                            return BankErrorCodeConstant.Dict[3000];
                        }

                        var tagPpdbSiswa = schService.GetPpdbSiswa(conn, idPpdbDaftar, out err);
                        if (!string.IsNullOrEmpty(err))
                            return BankErrorCodeConstant.Dict[3000];

                        if (tagPpdbSiswa.StatusBiaya == StatusLunasConstant.Dict[StatusLunasConstant.Lunas])
                            return BankErrorCodeConstant.Dict[9310];

                        int? cilKe = null;
                        if (tagPpdbSiswa.IsCicil == 1)
                        {
                            foreach (var item in tagPpdbSiswa.TagCicil.OrderBy(x => x.CilKe))
                            {
                                if (item.Status != StatusLunasConstant.Dict[StatusLunasConstant.Lunas])
                                {
                                    if (item.RpCilKe != Rp) return BankErrorCodeConstant.Dict[9200];
                                    cilKe = item.CilKe;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            if (tagPpdbSiswa.RpBiayaTotal != Rp) return BankErrorCodeConstant.Dict[9200];
                        }
                        RefNo = SetRefno(PartnerId, NoVa);
                        err = schService.SetLunasBiayaSiswaPrt(conn, IdPartner, RefNo, idPpdbDaftar, dateTimeService.GetCurrdate(), Rp, string.Empty, cilKe);
                        if (!string.IsNullOrEmpty(err)) return BankErrorCodeConstant.Dict[9999];



                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return BankErrorCodeConstant.Dict[9999];
            }
        }

        public string SetBatalTagihanSiswaBaru(string PartnerId, string NoVa, double Rp, string RefNo)
        {
            try
            {
                using (IDbConnection conn = common.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string err = CekPartner(conn, PartnerId, out int IdPartner);
                        if (!string.IsNullOrEmpty(err)) return err;
                        int idPpdbDaftar = 0;
                        try
                        {
                            idPpdbDaftar = int.Parse(NoVa);
                        }
                        catch (Exception)
                        {
                            return BankErrorCodeConstant.Dict[3000];
                        }

                        var tagPpdbSiswa = schService.GetPpdbSiswa(conn, idPpdbDaftar, out err);
                        if (!string.IsNullOrEmpty(err))
                            return BankErrorCodeConstant.Dict[3000];

                        if (tagPpdbSiswa.StatusBiaya == StatusLunasConstant.Dict[StatusLunasConstant.BelumLunas])
                            return BankErrorCodeConstant.Dict[9320];
                        if (tagPpdbSiswa.RpBiayaTotal != Rp) return BankErrorCodeConstant.Dict[9200];

                        var tbPpdbSiswaPengesahan = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                                     where a.IdPpdbDaftar == idPpdbDaftar &&
                                                     a.Status == StatusPpdbConstant.Pengesahan
                                                     select a).FirstOrDefault();
                        if (tbPpdbSiswaPengesahan != null)
                            return BankErrorCodeConstant.Dict[9500];

                        DateTime tanggalLunas = DateTime.ParseExact(tagPpdbSiswa.TanggalLunas, "dd-MM-yyyy HH24:mm:ss", CultureInfo.InvariantCulture);
                        if (!IsRefNo(conn, RefNo, PartnerId, NoVa, tanggalLunas)) return BankErrorCodeConstant.Dict[9400];

                        TbPpdbBiayaSiswa tbPpdbBiayaSiswa = conn.Get<TbPpdbBiayaSiswa>(idPpdbDaftar);
                        tbPpdbBiayaSiswa.Status = 0;
                        tbPpdbBiayaSiswa.RefNo = null;
                        tbPpdbBiayaSiswa.TanggalLunas = null;
                        conn.Update(tbPpdbBiayaSiswa);

                        var tbPpdbSiswaStatusPelunasan = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                                          where a.IdPpdbDaftar == idPpdbDaftar &
                                                          a.Status == StatusPpdbConstant.PelunasanBiayaPendidikan
                                                          select a).FirstOrDefault();
                        conn.Delete(tbPpdbSiswaStatusPelunasan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return BankErrorCodeConstant.Dict[9999];
            }
        }

        public string LogPartnerRequest(PartnerRequestModel Data)
        {
            try
            {
                using (IDbConnection conn = common.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbPartnerRequest tbPartnerRequest = new TbPartnerRequest
                        {
                            KdPartner = Data.KdPartner,
                            MethodName = Data.MethodName,
                            InDate = dateTimeService.GetCurrdate(),
                            ResponseStatus = Data.ResponseStatus,
                            ResponseMessage = Data.ResponseMessage,
                            ResponseResult = Data.ResponseResult
                        };
                        var _idPartnerRequest = conn.Insert(tbPartnerRequest);
                        TbPartnerRequestDetil tbPartnerRequestDetil = new TbPartnerRequestDetil
                        {
                            IdPartnerRequest = (int)_idPartnerRequest,
                            ParamName = Data.ParamName,
                            ParamValue = Data.ParamValue
                        };
                        conn.Insert(tbPartnerRequestDetil);

                        tx.Commit();

                        string filex = AppSetting.PathApplFile + "/!LOG_PARTNER_BANK.txt";

                        string log = string.Format("=============================== START LOG  ===============================" + Environment.NewLine);
                        log += string.Format("IdPartnerRequest : {0}" + Environment.NewLine, _idPartnerRequest.ToString());
                        log += string.Format("KdPartner : {0}" + Environment.NewLine, Data.KdPartner);
                        log += string.Format("MethodName : {0}" + Environment.NewLine, Data.MethodName);
                        log += string.Format("Param Name : {0}" + Environment.NewLine, Data.ParamName);
                        log += string.Format("Param Value : {0}" + Environment.NewLine, Data.ParamValue);
                        log += string.Format("InDate : {0}" + Environment.NewLine, dateTimeService.GetCurrdate());
                        log += string.Format("IpAddress : {0}" + Environment.NewLine, Data.IpAddress);
                        log += string.Format("ResponseStatus : {0}" + Environment.NewLine, Data.ResponseStatus);
                        log += string.Format("ResponseMessage : {0}" + Environment.NewLine, Data.ResponseMessage);
                        log += string.Format("ResponseResult : {0}" + Environment.NewLine, Data.ResponseResult);
                        log += string.Format("=============================== END LOG ===============================" + Environment.NewLine + Environment.NewLine);
                        File.AppendAllText(filex, log);

                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                return BankErrorCodeConstant.Dict[9999];
            }
        }

        public string LogPartnerPayment(
            string KodeBayar,
            string Tipe,
            double JumlahBayar,
            string KeteranganGagal
        )
        {
            try
            {
                using (IDbConnection conn = common.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var nama = string.Empty;
                        var catatan = string.Empty;
                        var kelas = string.Empty;
                        if (Tipe == "SPP")
                        {
                            var siswa = (from a in conn.GetList<TbSiswa>()
                                         join b in conn.GetList<TbSiswaTagihan>() on a.IdSiswa equals b.IdSiswa
                                         join c in conn.GetList<TbKelasRombel>() on a.IdSiswa equals c.IdSiswa
                                         join d in conn.GetList<TbKelasParalel>() on c.IdKelasParalel equals d.IdKelasParalel
                                         where a.Nis == KodeBayar || a.NisLama == KodeBayar
                                         select new { a, b, d }).FirstOrDefault();
                            nama = siswa.a.Nama;
                            catatan = siswa.b.Catatan;
                            kelas = siswa.d.Nama;
                        }
                        if (Tipe == "PPDB-DAFTAR-BARU")
                        {
                            var ppdb = (from a in conn.GetList<TbPpdbDaftar>()
                                        join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                        join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                        where a.IdPpdbDaftar == Int32.Parse(KodeBayar)
                                        select new { a, c }).FirstOrDefault();
                            nama = ppdb.a.Nama;
                            catatan = Tipe;
                            kelas = ppdb.c.Nama;
                        }
                        if (Tipe == "PPDB-DAFTAR-ULANG")
                        {
                            var ppdb = (from a in conn.GetList<TbPpdbDaftar>()
                                        join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                        join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                        where a.IdPpdbDaftar == Int32.Parse(KodeBayar)
                                        select new { a, c }).FirstOrDefault();
                            nama = ppdb.a.Nama;
                            catatan = Tipe;
                            kelas = ppdb.c.Nama;
                        }

                        TbPartnerHistoryPayment tbPartnerHistoryPayment = new TbPartnerHistoryPayment
                        {
                            KodeBayar = KodeBayar,
                            Tipe = Tipe,
                            Nama = nama,
                            Kelas = kelas,
                            JumlahBayar = JumlahBayar,
                            Catatan = catatan,
                            KeteranganGagal = KeteranganGagal,
                            Status = string.IsNullOrEmpty(KeteranganGagal) ? StatusLunasConstant.Lunas : StatusLunasConstant.BelumLunas,
                            CreatedDate = dateTimeService.GetCurrdate()
                        };
                        conn.Insert(tbPartnerHistoryPayment);
                        tx.Commit();

                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                return BankErrorCodeConstant.Dict[9999];
            }
        }
    }
}