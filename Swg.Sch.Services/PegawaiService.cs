﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Newtonsoft.Json;
using Swg.Entities.Kbm;
using Swg.Entities.Lmg;
using Swg.Entities.Public;
using Swg.Entities.Ref;
using Swg.Models;
using Swg.Models.Constants;
using Swg.Sch.Shared.Constants;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;

namespace Swg.Sch.Services
{
    public class PegawaiService : IPegawaiService
    {

        private string _languageCode = "id";
        private readonly string ServiceName = "Swg.Sch.Services.PegawaiService.";
        private readonly ICommonService commonService;
        private readonly IUserAppService userAppService;
        private readonly IDateTimeService dateTimeService;
        private readonly ISchService schService;
        public PegawaiService(
            ICommonService CommonService,
            IDateTimeService DateTimeService,
            IUserAppService UserAppService,
            ISchService SchService)
        {
            commonService = CommonService;
            userAppService = UserAppService;
            dateTimeService = DateTimeService;
            schService = SchService;
        }
        PegawaiModel SetPegawaiModelx(TbPegawai tbPegawai, TbUser tbUser, TbJenisJabatan TbJenisJabatan, TbJenisJenjangPendidikan tbJenjangPendidikan, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                PegawaiModel ret = new PegawaiModel();
                ret.Email = tbUser.Email;
                ret.Nama = userAppService.SetFullName(tbUser.FirstName, tbUser.MiddleName, tbUser.LastName);
                ret.JenisKelamin = JenisKelaminConstant.Dict[tbPegawai.KdJenisKelamin];
                ret.Alamat = tbUser.Address;
                ret.NoTelpon = tbUser.PhoneNumber;
                ret.NoHandphone = tbUser.MobileNumber;
                ret.Jabatan = TbJenisJabatan.Nama;
                ret.Agama = AgamaConstant.Dict[tbPegawai.IdAgama];
                ret.JenjangPendidikan = tbJenjangPendidikan.Nama;
                ret.IdPegawai = tbPegawai.IdPegawai;
                ret.IdJabatan = tbPegawai.IdJenisJabatan;
                ret.Nip = tbPegawai.Nip;
                ret.Nik = tbPegawai.Nik; ;
                ret.TempatLahir = tbPegawai.TempatLahir;
                ret.TanggalLahir = tbPegawai.TanggalLahir.ToString("dd-MM-yyyy");
                ret.KdJenisKelamin = tbPegawai.KdJenisKelamin;
                ret.IdAgama = tbPegawai.IdAgama;
                ret.IdJenjangPendidikan = tbPegawai.IdJenisJenjangPendidikan;
                ret.NamaInstitusiPendidikan = tbPegawai.NamaInstitusiPendidikan;
                ret.NamaPasangan = tbPegawai.NamaPasangan;
                ret.NoDarurat = tbPegawai.NoDarurat;
                ret.IdJenisPegawai = tbPegawai.IdJenisPegawai;
                ret.JenisPegawai = JenisPegawaiConstant.DictJenisPegawai[tbPegawai.IdJenisPegawai];
                ret.Facebook = tbPegawai.Facebook;
                ret.Twitter = tbPegawai.Twitter;
                ret.Instagram = tbPegawai.Instagram;
                ret.Youtube = tbPegawai.Youtube;
                ret.Sambutan = tbPegawai.Sambutan;
                ret.StatusKawin = tbPegawai.StatusKawin;
                ret.JumlahAnak = tbPegawai.JumlahAnak;
                if (tbPegawai.Status == 1)
                {
                    ret.Status = StatusDataConstant.Dict[1];
                }
                else if (tbPegawai.Status == -1)
                {
                    ret.Status = StatusDataConstant.Dict[-1];
                }
                ret.IdGolonganPegawai = tbPegawai.IdGolonganPegawai;
                ret.GolonganPegawai = JenisPegawaiGolonganConstant.DictJenisPegawaiGolongan[tbPegawai.IdGolonganPegawai];
                ret.TanggalMasuk = tbPegawai.TanggalMasuk.ToString("dd-MM-yyyy");
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "SetPegawaiModel", ex);
                return null;
            }
        }
        public string PegawaiAdd(int IdUser, PegawaiAddModel Data, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        try
                        {
                            var tbUser = conn.Get<TbUser>(Data.IdPegawai);
                            if (tbUser == null) { oMessage = "belum terdaftar di user aplikasi"; return null; }
                            TbPegawai tbPegawai = new TbPegawai();
                            tbPegawai.IdPegawai = Data.IdPegawai;
                            tbPegawai.IdJenisJabatan = Data.IdJabatan;
                            tbPegawai.IdJenisPegawai = Data.IdJenisPegawai;
                            tbPegawai.Nip = Data.Nip;
                            tbPegawai.Nik = Data.Nik;
                            tbPegawai.TempatLahir = Data.TempatLahir;
                            tbPegawai.TanggalLahir = dateTimeService.StrToDateTime(Data.TanggalLahir);
                            tbPegawai.KdJenisKelamin = Data.KdJenisKelamin;
                            tbPegawai.IdAgama = Data.IdAgama;
                            tbPegawai.IdJenisJenjangPendidikan = Data.IdJenjangPendidikan;
                            tbPegawai.NamaInstitusiPendidikan = Data.NamaInstitusiPendidikan;
                            tbPegawai.NamaPasangan = Data.NamaPasangan;
                            tbPegawai.NoDarurat = Data.NoDarurat;
                            tbPegawai.Status = StatusDataConstant.Aktif;
                            tbPegawai.TanggalMasuk = dateTimeService.StrToDateTime(Data.TanggalMasuk);
                            tbPegawai.IdGolonganPegawai = Data.IdGolonganPegawai;
                            tbPegawai.CreatedBy = IdUser;
                            tbPegawai.CreatedDate = dateTimeService.GetCurrdate();
                            tbPegawai.NoKk = Data.NoKk;
                            tbPegawai.JurusanPendidikan = Data.JurusanPendidikan;
                            tbPegawai.AktifitasDakwah = Data.AktifitasDakwah;
                            tbPegawai.OrganisasiMasyarakat = Data.OrganisasiMasyarakat;
                            tbPegawai.PengalamanKerja = Data.PengalamanKerja;
                            tbPegawai.Facebook = Data.Facebook;
                            tbPegawai.Twitter = Data.Twitter;
                            tbPegawai.Instagram = Data.Instagram;
                            tbPegawai.Youtube = Data.Youtube;
                            tbPegawai.Sambutan = Data.Sambutan;
                            tbPegawai.StatusKawin = Data.StatusKawin;
                            tbPegawai.JumlahAnak = Data.JumlahAnak;
                            tbPegawai.Gelar = Data.Gelar;

                            string idKompetensi = "";
                            tbPegawai.Kompetensi = idKompetensi;
                            if (Data.Kompetensi != null)
                            {
                                int index = 0;
                                foreach (var item in Data.Kompetensi)
                                {
                                    if (index > 0) idKompetensi += ",";
                                    idKompetensi += item.IdKompetensi.ToString();

                                    index++;
                                }
                                tbPegawai.Kompetensi = idKompetensi;
                            }

                            conn.Insert(tbPegawai);

                            string sqlInsert = "INSERT INTO lmg.tb_unit_pegawai(id_unit, id_pegawai) VALUES (@IdUnit, @IdPegawai)";
                            TbUnitPegawai tbUnitPegawai = new TbUnitPegawai
                            {
                                IdPegawai = tbPegawai.IdPegawai,
                                IdUnit = Data.IdUnit
                            };
                            var affectedRowsTam = conn.Execute(sqlInsert, tbUnitPegawai);

                            var TbKelasParalelPegawai = (from a in conn.GetList<TbKelasParalelPegawai>()
                                                         where a.IdPegawai == tbPegawai.IdPegawai
                                                         select a).ToList();
                            if (TbKelasParalelPegawai != null || TbKelasParalelPegawai.Count() != 0)
                            {
                                foreach (var val in TbKelasParalelPegawai)
                                {
                                    conn.Delete(val);
                                }
                            }
                            if (Data.KelasDiampu != null)
                            {
                                foreach (var item in Data.KelasDiampu)
                                {
                                    sqlInsert = "INSERT INTO kbm.tb_kelas_paralel_pegawai(id_pegawai, id_kelas_paralel) VALUES (@IdPegawai, @IdKelasParalel)";
                                    TbKelasParalelPegawai tbxPegawaiKelasParalel = new TbKelasParalelPegawai
                                    {
                                        IdPegawai = Data.IdPegawai,
                                        IdKelasParalel = item.IdKelasParalel
                                    };
                                    affectedRowsTam = conn.Execute(sqlInsert, tbxPegawaiKelasParalel);
                                }
                            }

                            tx.Commit();
                            return string.Empty;
                        }
                        catch (Exception ex)
                        {
                            oMessage = commonService.GetErrorMessage(ServiceName + "PegawaiAdd", ex);
                            return null;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "PegawaiAdd", ex);
                return null;
            }
        }
        public string PegawaiEdit(int IdUser, PegawaiEditModel Data, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        try
                        {
                            TbPegawai tbPegawai = conn.Get<TbPegawai>(Data.IdPegawai);
                            if (tbPegawai == null) { oMessage = "data tidak ada"; return null; }
                            tbPegawai.IdPegawai = Data.IdPegawai;
                            tbPegawai.IdJenisJabatan = Data.IdJabatan;
                            tbPegawai.IdJenisPegawai = Data.IdJenisPegawai;
                            tbPegawai.Nip = Data.Nip;
                            tbPegawai.Nik = Data.Nik; ;
                            tbPegawai.TempatLahir = Data.TempatLahir;
                            tbPegawai.TanggalLahir = dateTimeService.StrToDateTime(Data.TanggalLahir);
                            tbPegawai.KdJenisKelamin = Data.KdJenisKelamin;
                            tbPegawai.IdAgama = Data.IdAgama;
                            tbPegawai.IdJenisJenjangPendidikan = Data.IdJenjangPendidikan;
                            tbPegawai.NamaInstitusiPendidikan = Data.NamaInstitusiPendidikan;
                            tbPegawai.NamaPasangan = Data.NamaPasangan;
                            tbPegawai.NoDarurat = Data.NoDarurat;
                            if (!string.IsNullOrEmpty(Data.TanggalMasuk))
                                tbPegawai.TanggalMasuk = dateTimeService.StrToDateTime(Data.TanggalMasuk);
                            if (Data.IdGolonganPegawai > 0)
                                tbPegawai.IdGolonganPegawai = Data.IdGolonganPegawai;
                            tbPegawai.UpdatedBy = IdUser;
                            tbPegawai.UpdatedDate = dateTimeService.GetCurrdate();
                            tbPegawai.NoKk = Data.NoKk;
                            tbPegawai.JurusanPendidikan = Data.JurusanPendidikan;
                            Console.WriteLine($"<{Data.Kompetensi}>");
                            tbPegawai.AktifitasDakwah = Data.AktifitasDakwah;
                            tbPegawai.OrganisasiMasyarakat = Data.OrganisasiMasyarakat;
                            tbPegawai.PengalamanKerja = Data.PengalamanKerja;
                            tbPegawai.Npwp = Data.Npwp;
                            tbPegawai.Facebook = Data.Facebook;
                            tbPegawai.Twitter = Data.Twitter;
                            tbPegawai.Instagram = Data.Instagram;
                            tbPegawai.Youtube = Data.Youtube;
                            tbPegawai.Sambutan = Data.Sambutan;
                            tbPegawai.StatusKawin = Data.StatusKawin;
                            tbPegawai.JumlahAnak = Data.JumlahAnak;
                            tbPegawai.Gelar = Data.Gelar;

                            string idKompetensi = "";
                            tbPegawai.Kompetensi = idKompetensi;
                            if (Data.Kompetensi != null)
                            {
                                int index = 0;
                                foreach (var item in Data.Kompetensi)
                                {
                                    if (index > 0) idKompetensi += ",";
                                    idKompetensi += item.IdKompetensi.ToString();

                                    index++;
                                }
                                tbPegawai.Kompetensi = idKompetensi;
                            }

                            conn.Update(tbPegawai);
                            var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                                 where a.IdPegawai == Data.IdPegawai
                                                 select a).FirstOrDefault();
                            if (tbUnitPegawai == null)
                            {
                                string sqlInsert = "INSERT INTO lmg.tb_unit_pegawai(id_unit, id_pegawai) VALUES (@IdUnit, @IdPegawai)";
                                tbUnitPegawai = new TbUnitPegawai
                                {
                                    IdPegawai = Data.IdPegawai,
                                    IdUnit = Data.IdUnit
                                };
                                var affectedRowsTam = conn.Execute(sqlInsert, tbUnitPegawai);
                            }
                            else
                            {
                                string sqlUpdate = string.Format("UPDATE lmg.tb_unit_pegawai SET id_unit = {0} WHERE id_pegawai = {1}", Data.IdUnit, Data.IdPegawai);
                                var affectedRowsTam = conn.Execute(sqlUpdate);
                            }

                            var TbKelasParalelPegawai = (from a in conn.GetList<TbKelasParalelPegawai>()
                                                         where a.IdPegawai == Data.IdPegawai
                                                         select a).ToList();
                            if (TbKelasParalelPegawai != null || TbKelasParalelPegawai.Count() != 0)
                            {
                                foreach (var val in TbKelasParalelPegawai)
                                {
                                    string sqlDelete = "DELETE FROM kbm.tb_kelas_paralel_pegawai WHERE id_kelas_paralel = @IdKelasParalel AND id_pegawai = @IdPegawai;";
                                    TbKelasParalelPegawai tbxPegawaiKelasParalel = new TbKelasParalelPegawai
                                    {
                                        IdKelasParalel = val.IdKelasParalel,
                                        IdPegawai = Data.IdPegawai
                                    };
                                    var affectedRowsTam = conn.Execute(sqlDelete, tbxPegawaiKelasParalel);
                                }
                            }
                            if (Data.KelasDiampu != null)
                            {

                                foreach (var item in Data.KelasDiampu)
                                {
                                    string sqlInsert = "INSERT INTO kbm.tb_kelas_paralel_pegawai(id_kelas_paralel, id_pegawai) VALUES (@IdKelasParalel, @IdPegawai)";
                                    TbKelasParalelPegawai tbxPegawaiKelasParalel = new TbKelasParalelPegawai
                                    {
                                        IdKelasParalel = item.IdKelasParalel,
                                        IdPegawai = Data.IdPegawai
                                    };
                                    var affectedRowsTam = conn.Execute(sqlInsert, tbxPegawaiKelasParalel);
                                }
                            }

                            tx.Commit();
                            return string.Empty;
                        }
                        catch (Exception ex)
                        {
                            oMessage = commonService.GetErrorMessage(ServiceName + "PegawaiEdit 1", ex);
                            return null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "PegawaiEdit 2", ex);
                return null;
            }
        }
        public string SetPegawaiActive(int IdUser, int IdPegawai, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbPegawai tbPegawai = conn.Get<TbPegawai>(IdPegawai);
                        if (tbPegawai == null) { oMessage = "data tidak ada"; return null; }
                        if (tbPegawai.Status == StatusDataConstant.Aktif) { oMessage = "data sudah aktif"; return null; }
                        tbPegawai.Status = StatusDataConstant.Aktif;
                        tbPegawai.UpdatedBy = IdUser;
                        tbPegawai.UpdatedDate = dateTimeService.GetCurrdate();
                        conn.Update(tbPegawai);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "SetPegawaiActive", ex);
                return null;
            }
        }
        public string SetPegawaiInActive(int IdUser, int IdPegawai, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbPegawai tbPegawai = conn.Get<TbPegawai>(IdPegawai);
                        if (tbPegawai == null) { oMessage = "data tidak ada"; return null; }
                        if (tbPegawai.Status == StatusDataConstant.TidakAktif) { oMessage = "data sudah tidak aktif"; return null; }

                        tbPegawai.Status = StatusDataConstant.TidakAktif;
                        tbPegawai.UpdatedBy = IdUser;
                        tbPegawai.UpdatedDate = dateTimeService.GetCurrdate();
                        conn.Update(tbPegawai);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "SetPegawaiInActive", ex);
                return null;
            }
        }
        public List<ComboModel> GetKelasParalelPegawai(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbKelasParalel>();
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    List<ComboModel> ret = new List<ComboModel>();
                    foreach (var item in data)
                    {
                        ret.Add(new ComboModel
                        {
                            Id = item.IdKelasParalel,
                            Nama = item.Nama,
                            NamaSingkat = item.NamaSingkat
                        });
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelasPararelPegawai", ex);
                return null;
            }
        }
        public List<ComboModel> GetKompetensiPegawai()
        {
            return (from a in JenisKompetensiConstant.DictJenisKompetensi
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<ComboModel> GetJenisStatusNikah()
        {
            return (from a in JenisStatusNikahConstant.Dict
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();
        }
        public List<PegawaiModel> GetListPegawai(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    oMessage = string.Empty;
                    var data = from a in conn.GetList<TbPegawai>()
                               join b in conn.GetList<TbUser>() on a.IdPegawai equals b.IdUser
                               join d in conn.GetList<TbJenisJabatan>() on a.IdJenisJabatan equals d.IdJenisJabatan
                               join e in conn.GetList<TbJenisJenjangPendidikan>() on a.IdJenisJenjangPendidikan equals e.IdJenisJenjangPendidikan
                               join f in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals f.IdPegawai
                               join g in conn.GetList<TbUnit>() on f.IdUnit equals g.IdUnit
                               select new { a, b, d, e, f, g };
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    List<PegawaiModel> ret = new List<PegawaiModel>();
                    foreach (var item in data.OrderByDescending(x => x.a.IdPegawai))
                    {
                        PegawaiModel m = new PegawaiModel();
                        m.IdPegawai = item.a.IdPegawai;
                        m.IdUnit = item.g.IdUnit;
                        m.Email = item.b.Email;
                        m.Nama = userAppService.SetFullName(item.b.FirstName, item.b.MiddleName, item.b.LastName);
                        m.JenisKelamin = JenisKelaminConstant.Dict[item.a.KdJenisKelamin];
                        m.Alamat = item.b.Address;
                        m.NoTelpon = item.b.PhoneNumber;
                        m.NoHandphone = item.b.MobileNumber;
                        m.Jabatan = item.d.Nama;
                        m.Agama = AgamaConstant.Dict[item.a.IdAgama];
                        m.JenjangPendidikan = item.e.NamaSingkat;
                        m.IdPegawai = item.a.IdPegawai;
                        m.IdJabatan = item.a.IdJenisJabatan;
                        m.Nip = item.a.Nip;
                        m.Nik = item.a.Nik; ;
                        m.TempatLahir = item.a.TempatLahir;
                        m.TanggalLahir = item.a.TanggalLahir.ToString("dd-MM-yyyy");
                        m.KdJenisKelamin = item.a.KdJenisKelamin;
                        m.IdAgama = item.a.IdAgama;
                        m.IdJenjangPendidikan = item.a.IdJenisJenjangPendidikan;
                        m.NamaInstitusiPendidikan = item.a.NamaInstitusiPendidikan;
                        m.NamaPasangan = item.a.NamaPasangan;
                        m.NoDarurat = item.a.NoDarurat;
                        m.IdJenisPegawai = item.a.IdJenisPegawai;
                        m.JenisPegawai = JenisPegawaiConstant.DictJenisPegawai[item.a.IdJenisPegawai];
                        m.Facebook = item.a.Facebook;
                        m.Twitter = item.a.Twitter;
                        m.Instagram = item.a.Instagram;
                        m.Youtube = item.a.Youtube;
                        m.Sambutan = item.a.Sambutan;
                        m.StatusKawin = item.a.StatusKawin;
                        m.StrStatusKawin = item.a.StatusKawin != 0 ? JenisStatusNikahConstant.Dict[item.a.StatusKawin] : null;
                        m.JumlahAnak = item.a.JumlahAnak;
                        if (item.a.Status == 1)
                        {
                            m.Status = StatusDataConstant.Dict[1];
                        }
                        else
                        {
                            m.Status = StatusDataConstant.Dict[-1];
                        }
                        m.IdGolonganPegawai = item.a.IdGolonganPegawai;
                        m.GolonganPegawai = JenisPegawaiGolonganConstant.DictJenisPegawaiGolongan[item.a.IdGolonganPegawai];
                        m.TanggalMasuk = item.a.TanggalMasuk.ToString("dd-MM-yyyy");
                        m.Username = item.b.Username;
                        m.Password = commonService.DecryptString(item.b.Password);
                        m.Unit = item.g.Nama;
                        m.NoKk = item.a.NoKk;
                        m.JurusanPendidikan = item.a.JurusanPendidikan;
                        m.AktifitasDakwah = item.a.AktifitasDakwah;
                        m.OrganisasiMasyarakat = item.a.OrganisasiMasyarakat;
                        m.PengalamanKerja = item.a.PengalamanKerja;
                        m.KelasDiampu = new List<PegawaiKelasParalelModel>();
                        var TbKelasParalelPegawai = (from ax in conn.GetList<TbKelasParalelPegawai>()
                                                     join bx in conn.GetList<TbKelasParalel>() on ax.IdKelasParalel equals bx.IdKelasParalel
                                                     where ax.IdPegawai == m.IdPegawai
                                                     select bx).ToList();
                        if (TbKelasParalelPegawai != null || TbKelasParalelPegawai.Count() != 0)
                        {
                            foreach (var val in TbKelasParalelPegawai)
                            {
                                PegawaiKelasParalelModel modx = new PegawaiKelasParalelModel();
                                modx.IdKelasParalel = val.IdKelasParalel;
                                modx.KelasParalel = val.Nama;
                                m.KelasDiampu.Add(modx);
                            }
                        }

                        m.Kompetensi = new List<PegawaiKompetensiModel>();
                        Console.WriteLine("id pegawai : " + item.a.IdPegawai + "\n");
                        if (!string.IsNullOrEmpty(item.a.Kompetensi))
                        {
                            string[] _kompetensis = item.a.Kompetensi.Split(',');
                            foreach (var val in _kompetensis)
                            {
                                PegawaiKompetensiModel modx = new PegawaiKompetensiModel();
                                modx.IdKompetensi = val;
                                modx.Kompetensi = JenisKompetensiConstant.DictJenisKompetensi[Int32.Parse(val)];
                                m.Kompetensi.Add(modx);
                            }
                        }
                        m.Npwp = item.a.Npwp;
                        m.Gelar = item.a.Gelar;

                        ret.Add(m);
                    }
                    return ret.ToList();
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetListPegawai", ex);
                return null;
            }
        }

        public List<ComboModel> GetListJenjangPendidikan(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    oMessage = string.Empty;
                    var data = from a in conn.GetList<TbJenisJenjangPendidikan>()
                               select a;
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }

                    List<ComboModel> ret = new List<ComboModel>();
                    foreach (var item in data)
                    {
                        ComboModel m = new ComboModel
                        {
                            Id = item.IdJenisJenjangPendidikan,
                            Nama = item.Nama,
                            NamaSingkat = item.NamaSingkat
                        };
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetListJenjangPendidikan", ex);
                return null;
            }
        }
        public List<ComboModel> GetListJabatan(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    oMessage = string.Empty;
                    var data = from a in conn.GetList<TbJenisJabatan>()
                               select a;
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }

                    List<ComboModel> ret = new List<ComboModel>();
                    foreach (var item in data)
                    {
                        ComboModel m = new ComboModel
                        {
                            Id = item.IdJenisJabatan,
                            Nama = item.Nama,
                            NamaSingkat = item.NamaSingkat
                        };
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetListJenjangPendidikan", ex);
                return null;
            }
        }

        public string UploadPegawai(int IdUser, List<SheetPegawaiModel> Data)
        {
            string tempErr = string.Empty;
            try
            {
                string err = string.Empty;
                Console.WriteLine(DateTime.Now);
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        foreach (var item in Data)
                        {
                            tempErr = string.Format("nip {0}", item.Nip);
                            TbPegawai tbPegawai = conn.Get<TbPegawai>(item.IdPegawai);
                            if (tbPegawai == null)
                            {
                                err += Environment.NewLine + string.Format("nip {0} pegawai tidak terdaftar", item.Nip);
                                continue;
                            }

                            tbPegawai.IdAgama = AgamaConstant.Islam;
                            var _jabatan = (from a in conn.GetList<TbJenisJabatan>()
                                            where a.Nama == item.Jabatan.Trim()
                                            select a).FirstOrDefault();
                            if (_jabatan == null)
                            {
                                err += Environment.NewLine + string.Format("jabatan {0} pegawai tidak terdaftar", item.Jabatan);
                                continue;
                            }
                            tbPegawai.IdJenisJabatan = _jabatan.IdJenisJabatan;

                            try
                            {
                                tbPegawai.IdJenisPegawai = (from a in JenisPegawaiConstant.DictJenisPegawai
                                                            where a.Value == item.Status
                                                            select a).FirstOrDefault().Key;
                            }
                            catch (Exception)
                            {
                                err += Environment.NewLine + string.Format("status {0} pegawai tidak terdaftar", item.Status);
                                continue;
                            }

                            var _jenjangPendidikan = (from a in conn.GetList<TbJenisJenjangPendidikan>()
                                                      where a.NamaSingkat == item.Pendidikan.Trim()
                                                      select a).FirstOrDefault();
                            if (_jenjangPendidikan == null)
                            {
                                err += Environment.NewLine + string.Format("jenjang pendidikan {0} pegawai tidak terdaftar", item.Pendidikan);
                                continue;
                            }
                            tbPegawai.IdJenisJenjangPendidikan = _jenjangPendidikan.IdJenisJenjangPendidikan;

                            try
                            {
                                tbPegawai.KdJenisKelamin = (from a in JenisKelaminConstant.Dict
                                                            where a.Value == item.JenisKelamin
                                                            select a).FirstOrDefault().Key;
                            }
                            catch (Exception)
                            {
                                err += Environment.NewLine + string.Format("jenis kelamin {0} pegawai tidak terdaftar", item.JenisKelamin);
                                continue;
                            }
                            tbPegawai.NamaInstitusiPendidikan = item.NamaInstitusiPendidikan;
                            tbPegawai.NamaPasangan = item.NamaPasangan;
                            tbPegawai.Nik = item.Nik;
                            tbPegawai.Nip = item.Nip;
                            tbPegawai.NoDarurat = item.NoDarurat;
                            tbPegawai.Status = StatusDataConstant.Aktif;
                            tbPegawai.TanggalLahir = dateTimeService.StrToDateTime(item.TanggalLahir);
                            tbPegawai.TempatLahir = item.TempatLahir;
                            try
                            {
                                tbPegawai.IdGolonganPegawai = (from a in JenisPegawaiGolonganConstant.DictJenisPegawaiGolongan
                                                               where a.Value == item.Golongan
                                                               select a).FirstOrDefault().Key;
                            }
                            catch (Exception)
                            {
                                err += Environment.NewLine + string.Format("golongan {0} pegawai tidak terdaftar", item.Golongan);
                                continue;
                            }
                            tbPegawai.TanggalMasuk = dateTimeService.StrToDateTime(item.TanggalMasuk.Replace("/", "-"));

                            tbPegawai.NoDarurat = commonService.SetNoHandphone(item.NoDarurat);

                            if (string.IsNullOrEmpty(tbPegawai.Nik)) tbPegawai.Nik = tbPegawai.Nip;
                            if (tbPegawai.Nik.Length != 16) tbPegawai.Nik = tbPegawai.Nip;
                            tbPegawai.UpdatedBy = IdUser;
                            tbPegawai.UpdatedDate = dateTimeService.GetCurrdate();
                            conn.Update(tbPegawai);
                            var tbUnitPegawais = from a in conn.GetList<TbUnitPegawai>()
                                                 where a.IdPegawai == item.IdPegawai
                                                 select a;
                            foreach (var item1 in tbUnitPegawais)
                            {
                                conn.Delete(item1);
                            }

                            var idUnit = (from a in conn.GetList<TbUnit>()
                                          where a.Nama == item.Unit.Trim()
                                          select a).FirstOrDefault().IdUnit;
                            TbUnitPegawai tbUnitPegawai = new TbUnitPegawai
                            {
                                IdUnit = idUnit,
                                IdPegawai = tbPegawai.IdPegawai
                            };
                            conn.Insert(tbUnitPegawai);

                            string _firstName = string.Empty;
                            string _midleName = string.Empty;
                            string _lastName = string.Empty;

                            string[] namas = item.Nama.Split(" ");
                            _firstName = namas[0];
                            if (namas.Length > 1)
                                _midleName = namas[1];
                            if (namas.Length > 2)
                                for (int x = 2; x < namas.Length; x++)
                                {
                                    _lastName += namas[x] + " ";
                                }
                            _lastName = _lastName.Trim();

                            Console.WriteLine(string.Format("{0} -> {1}-{2}-{3}", item.Nama, _firstName, _midleName, _lastName));
                            TbUser tbUserProfile = conn.Get<TbUser>(item.IdPegawai);
                            tbUserProfile.FirstName = _firstName;
                            tbUserProfile.MiddleName = _midleName;
                            tbUserProfile.LastName = _lastName;
                            tbUserProfile.MobileNumber = commonService.SetNoHandphone(item.NoHandphone);
                            tbUserProfile.PhoneNumber = commonService.SetNoHandphone(item.NoTelpon);
                            conn.Update(tbUserProfile);
                        }
                        tx.Commit();
                        Console.WriteLine(DateTime.Now);
                        return err;
                    }
                }
            }
            catch (Exception ex)
            {
                return tempErr + Environment.NewLine + commonService.GetErrorMessage(ServiceName + "UploadPegawai", ex);
            }
        }
        public string UploadGajiPrevious(int IdUser, List<SheetGajiPegawaiPreviousModel> Data)
        {
            string tempErr = string.Empty;
            try
            {
                string err = string.Empty;
                Console.WriteLine(DateTime.Now);
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime periode = DateTime.ParseExact(Data[0].Bulan, "MM-yyyy", provider);

                        var tbPegawaiGajiTetaps = from a in conn.GetList<TbPegawaiGajiTetap>()
                                                  where a.Periode == periode
                                                  select a;
                        foreach (var item in tbPegawaiGajiTetaps)
                        {
                            conn.Delete(item);
                        }

                        var tbPegawaiGajiTidakTetaps = from a in conn.GetList<TbPegawaiGajiTidakTetap>()
                                                       where a.Periode == periode
                                                       select a;
                        foreach (var item in tbPegawaiGajiTidakTetaps)
                        {
                            conn.Delete(item);
                        }

                        var tbPegawaiGajiPotongans = from a in conn.GetList<TbPegawaiGajiPotongan>()
                                                     where a.Periode == periode
                                                     select a;
                        foreach (var item in tbPegawaiGajiPotongans)
                        {
                            conn.Delete(item);
                        }
                        var tbPegawaiGajiTambahans = from a in conn.GetList<TbPegawaiGajiTambahan>()
                                                     where a.Periode == periode
                                                     select a;
                        foreach (var item in tbPegawaiGajiTambahans)
                        {
                            conn.Delete(item);
                        }

                        var tbPegawaiGaji = from a in conn.GetList<TbPegawaiGaji>()
                                            where a.Periode == periode
                                            select a;
                        foreach (var item in tbPegawaiGaji)
                        {
                            conn.Delete(item);
                        }

                        string sqlGT = "INSERT INTO lmg.tb_pegawai_gaji_tetap(" +
                                        "id_pegawai, periode, id_jenis_gaji_tetap, rp)" +
                                        "VALUES(@IdPegawai, @Periode, @IdJenisGajiTetap, @Rp);";
                        List<TbPegawaiGajiTetap> tbGT = new List<TbPegawaiGajiTetap>();

                        string sqlGTT = "INSERT INTO lmg.tb_pegawai_gaji_tidak_tetap(" +
                                        "id_pegawai, periode, id_jenis_gaji_tidak_tetap, rp)" +
                                        "VALUES(@IdPegawai, @Periode, @IdJenisGajiTidakTetap, @Rp);";
                        List<TbPegawaiGajiTidakTetap> tbGTT = new List<TbPegawaiGajiTidakTetap>();

                        string sqlPot = "INSERT INTO lmg.tb_pegawai_gaji_potongan(" +
                                        "id_pegawai, periode, id_jenis_gaji_potongan, rp)" +
                                        "VALUES(@IdPegawai, @Periode, @IdJenisGajiPotongan, @Rp);";
                        List<TbPegawaiGajiPotongan> tbPot = new List<TbPegawaiGajiPotongan>();

                        string sqlTam = "INSERT INTO lmg.tb_pegawai_gaji_tambahan(" +
                                        "id_pegawai, periode, id_jenis_gaji_tambahan, rp)" +
                                        "VALUES(@IdPegawai, @Periode, @IdJenisGajiTambahan, @Rp);";
                        List<TbPegawaiGajiTambahan> tbTam = new List<TbPegawaiGajiTambahan>();

                        string sqlPegawaiGaji = "INSERT INTO lmg.tb_pegawai_gaji(" +
                                                "id_pegawai, periode, nama_pegawai,nip,jabatan,unit,jenis_pegawai,jenjang_pendidikan,golongan_pegawai,agama,nama_institusi_pendidikan,tanggal_masuk)" +
                                                "VALUES(@IdPegawai, @Periode, @NamaPegawai, @Nip, @Jabatan, @Unit, @JenisPegawai,@JenjangPendidikan,@GolonganPegawai,@Agama,@NamaInstitusiPendidikan,@TanggalMasuk);";
                        List<TbPegawaiGaji> tbGaji = new List<TbPegawaiGaji>();


                        foreach (var item in Data)
                        {
                            tempErr = string.Format("nip {0}", item.Nip);
                            var tbPegawais = from a in conn.GetList<TbPegawai>()
                                             where a.Nip == item.Nip.Trim()
                                             select a;
                            if (tbPegawais == null || tbPegawais.Count() == 0)
                            {
                                err += Environment.NewLine + string.Format("nip {0} pegawai tidak terdaftar", item.Nip);
                                continue;
                            }

                            if (tbPegawais.Count() > 1)
                            {
                                err += Environment.NewLine + string.Format("nip {0} pegawai duplikat", item.Nip);
                                continue;
                            }
                            var tbPegawai = tbPegawais.FirstOrDefault();

                            tbGaji.Add(new TbPegawaiGaji
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                Jabatan = item.Jabatan, //pokok
                                Unit = item.Unit,
                                JenisPegawai = item.JenisPegawai,
                                JenjangPendidikan = item.Pendidikan,
                                GolonganPegawai = item.Golongan,
                                Agama = item.Agama,
                                NamaInstitusiPendidikan = item.NamaInstitusiPendidikan,
                                NamaPegawai = item.Nama,
                                Nip = item.Nip,
                                TanggalMasuk = dateTimeService.StrToDateTime(item.TanggalMasuk)
                            });

                            //gaji tetap
                            tbGT.Add(new TbPegawaiGajiTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTetap = 1, //pokok
                                Rp = item.GT_Pokok
                            });
                            tbGT.Add(new TbPegawaiGajiTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTetap = 2, //tunjanan operasional
                                Rp = item.GT_Operasional
                            });
                            tbGT.Add(new TbPegawaiGajiTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTetap = 3, //tunjangan jabatan
                                Rp = item.GT_Jabatan
                            });
                            tbGT.Add(new TbPegawaiGajiTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTetap = 4, //tunjangan keluarga
                                Rp = item.GT_Keluarga
                            });

                            //gaji tidak tetap
                            tbGTT.Add(new TbPegawaiGajiTidakTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTidakTetap = 1,
                                Rp = item.GTT_Lembur
                            });

                            tbGTT.Add(new TbPegawaiGajiTidakTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTidakTetap = 2,
                                Rp = item.GTT_Anomali
                            });
                            tbGTT.Add(new TbPegawaiGajiTidakTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTidakTetap = 3,
                                Rp = item.GTT_Performa
                            });
                            tbGTT.Add(new TbPegawaiGajiTidakTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTidakTetap = 4,
                                Rp = item.GTT_Bpjs
                            });
                            tbGTT.Add(new TbPegawaiGajiTidakTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTidakTetap = 5,
                                Rp = item.GTT_Pensiun
                            });
                            tbGTT.Add(new TbPegawaiGajiTidakTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTidakTetap = 6,
                                Rp = item.GTT_Thr
                            });

                            //potongan
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 1,
                                Rp = item.Pot_Koreksi
                            });
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 2,
                                Rp = item.Pot_Icat
                            });
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 3,
                                Rp = item.Pot_DayCare
                            });
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 4,
                                Rp = item.Pot_Koperasi
                            });
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 5,
                                Rp = item.Pot_Absensi
                            });
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 6,
                                Rp = item.Pot_Lembur
                            });
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 7,
                                Rp = item.Pot_Bpjs
                            });
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 8,
                                Rp = item.Pot_Peserta
                            });
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 9,
                                Rp = item.Pot_Pensiun
                            });
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 10,
                                Rp = item.Pot_Lainnya
                            });

                            //tambahan
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 1,
                                Rp = item.Tam_Koreksi
                            });
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 2,
                                Rp = item.Tam_Lembur
                            });
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 3,
                                Rp = item.Tam_InvalGuru
                            });
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 4,
                                Rp = item.Tam_OverTime
                            });
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 5,
                                Rp = item.Tam_Backup
                            });
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 6,
                                Rp = item.Tam_LemburHari
                            });
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 7,
                                Rp = item.Tam_LemburJam
                            });
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 8,
                                Rp = item.Tam_Tunjangan
                            });
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 9,
                                Rp = item.Tam_Bpap
                            });
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 10,
                                Rp = item.Tam_Lainnya
                            });
                        }
                        Console.WriteLine("proses gaji tetap");
                        var affectedRowsGT = conn.Execute(sqlGT, tbGT);
                        Console.WriteLine("proses gaji tidak tetap");
                        var affectedRowsGTT = conn.Execute(sqlGTT, tbGTT);
                        Console.WriteLine("proses gaji potongan");
                        var affectedRowsPot = conn.Execute(sqlPot, tbPot);
                        Console.WriteLine("proses gaji tambahan");
                        var affectedRowsTam = conn.Execute(sqlTam, tbTam);
                        Console.WriteLine("proses gaji pegawai");
                        var affectedRowsGaji = conn.Execute(sqlPegawaiGaji, tbGaji);
                        tx.Commit();
                        Console.WriteLine(DateTime.Now);
                        return err;
                    }
                }
            }
            catch (Exception ex)
            {
                return tempErr + Environment.NewLine + commonService.GetErrorMessage(ServiceName + "UploadGajiPrevious", ex);
            }

        }
        public string UploadGaji(int IdUser, List<SheetGajiPegawaiModel> Data)
        {
            string tempErr = string.Empty;
            try
            {
                string err = string.Empty;
                Console.WriteLine(DateTime.Now);
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime periode = DateTime.ParseExact(Data[0].Bulan, "MM-yyyy", provider);

                        var tbPegawaiGajiTetaps = from a in conn.GetList<TbPegawaiGajiTetap>()
                                                  where a.Periode == periode
                                                  select a;
                        foreach (var item in tbPegawaiGajiTetaps)
                        {
                            conn.Delete(item);
                        }

                        var tbPegawaiGajiTidakTetaps = from a in conn.GetList<TbPegawaiGajiTidakTetap>()
                                                       where a.Periode == periode
                                                       select a;
                        foreach (var item in tbPegawaiGajiTidakTetaps)
                        {
                            conn.Delete(item);
                        }

                        var tbPegawaiGajiPotongans = from a in conn.GetList<TbPegawaiGajiPotongan>()
                                                     where a.Periode == periode
                                                     select a;
                        foreach (var item in tbPegawaiGajiPotongans)
                        {
                            conn.Delete(item);
                        }
                        var tbPegawaiGajiTambahans = from a in conn.GetList<TbPegawaiGajiTambahan>()
                                                     where a.Periode == periode
                                                     select a;
                        foreach (var item in tbPegawaiGajiTambahans)
                        {
                            conn.Delete(item);
                        }

                        var tbPegawaiGaji = from a in conn.GetList<TbPegawaiGaji>()
                                            where a.Periode == periode
                                            select a;
                        foreach (var item in tbPegawaiGaji)
                        {
                            conn.Delete(item);
                        }

                        string sqlGT = "INSERT INTO lmg.tb_pegawai_gaji_tetap(" +
                                        "id_pegawai, periode, id_jenis_gaji_tetap, rp)" +
                                        "VALUES(@IdPegawai, @Periode, @IdJenisGajiTetap, @Rp);";
                        List<TbPegawaiGajiTetap> tbGT = new List<TbPegawaiGajiTetap>();

                        string sqlGTT = "INSERT INTO lmg.tb_pegawai_gaji_tidak_tetap(" +
                                        "id_pegawai, periode, id_jenis_gaji_tidak_tetap, rp)" +
                                        "VALUES(@IdPegawai, @Periode, @IdJenisGajiTidakTetap, @Rp);";
                        List<TbPegawaiGajiTidakTetap> tbGTT = new List<TbPegawaiGajiTidakTetap>();

                        string sqlPot = "INSERT INTO lmg.tb_pegawai_gaji_potongan(" +
                                        "id_pegawai, periode, id_jenis_gaji_potongan, rp)" +
                                        "VALUES(@IdPegawai, @Periode, @IdJenisGajiPotongan, @Rp);";
                        List<TbPegawaiGajiPotongan> tbPot = new List<TbPegawaiGajiPotongan>();

                        string sqlTam = "INSERT INTO lmg.tb_pegawai_gaji_tambahan(" +
                                        "id_pegawai, periode, id_jenis_gaji_tambahan, rp)" +
                                        "VALUES(@IdPegawai, @Periode, @IdJenisGajiTambahan, @Rp);";
                        List<TbPegawaiGajiTambahan> tbTam = new List<TbPegawaiGajiTambahan>();

                        string sqlPegawaiGaji = "INSERT INTO lmg.tb_pegawai_gaji(" +
                                                "id_pegawai, periode, nama_pegawai,nip,jabatan,unit,jenis_pegawai,jenjang_pendidikan,golongan_pegawai,agama,nama_institusi_pendidikan,tanggal_masuk)" +
                                                "VALUES(@IdPegawai, @Periode, @NamaPegawai, @Nip, @Jabatan, @Unit, @JenisPegawai,@JenjangPendidikan,@GolonganPegawai,@Agama,@NamaInstitusiPendidikan,@TanggalMasuk);";
                        List<TbPegawaiGaji> tbGaji = new List<TbPegawaiGaji>();


                        foreach (var item in Data)
                        {
                            tempErr = string.Format("nip {0}", item.Nip);
                            var tbPegawais = from a in conn.GetList<TbPegawai>()
                                             where a.Nip == item.Nip.Trim()
                                             select a;
                            if (tbPegawais == null || tbPegawais.Count() == 0)
                            {
                                err += Environment.NewLine + string.Format("nip {0} pegawai tidak terdaftar", item.Nip);
                                continue;
                            }

                            if (tbPegawais.Count() > 1)
                            {
                                err += Environment.NewLine + string.Format("nip {0} pegawai duplikat", item.Nip);
                                continue;
                            }
                            var tbPegawai = tbPegawais.FirstOrDefault();

                            var tbUnitPegawais = from a in conn.GetList<TbUnitPegawai>()
                                                 join b in conn.GetList<TbUnit>() on a.IdUnit equals b.IdUnit
                                                 where a.IdPegawai == tbPegawai.IdPegawai
                                                 select new { a, b };
                            var tbUnitPegawai = tbUnitPegawais.FirstOrDefault();

                            var tbJenjangPendidikans = from a in conn.GetList<TbJenisJenjangPendidikan>()
                                                       where a.IdJenisJenjangPendidikan == tbPegawai.IdJenisJenjangPendidikan
                                                       select new { a };
                            var tbJenjangPendidikan = tbJenjangPendidikans.FirstOrDefault();

                            var TbJenisJabatans = from a in conn.GetList<TbJenisJabatan>()
                                                  where a.IdJenisJabatan == tbPegawai.IdJenisJabatan
                                                  select new { a };
                            var TbJenisJabatan = TbJenisJabatans.FirstOrDefault();

                            var tbUserProfiles = from a in conn.GetList<TbUser>()
                                                 where a.IdUser == tbPegawai.IdPegawai
                                                 select new { a };
                            var tbUserProfile = tbUserProfiles.FirstOrDefault();

                            tbGaji.Add(new TbPegawaiGaji
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                Jabatan = TbJenisJabatan.a.Nama, //pokok
                                Unit = tbUnitPegawai.b.Nama,
                                JenisPegawai = JenisPegawaiConstant.DictJenisPegawai[tbPegawai.IdJenisPegawai],
                                JenjangPendidikan = tbJenjangPendidikan.a.Nama,
                                GolonganPegawai = JenisPegawaiGolonganConstant.DictJenisPegawaiGolongan[tbPegawai.IdGolonganPegawai],
                                Agama = AgamaConstant.Dict[tbPegawai.IdAgama],
                                NamaInstitusiPendidikan = tbPegawai.NamaInstitusiPendidikan,
                                NamaPegawai = userAppService.SetFullName(tbUserProfile.a.FirstName, tbUserProfile.a.MiddleName, tbUserProfile.a.LastName),
                                Nip = tbPegawai.Nip,
                                TanggalMasuk = tbPegawai.TanggalMasuk
                            });

                            //gaji tetap
                            tbGT.Add(new TbPegawaiGajiTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTetap = 1, //pokok
                                Rp = item.GT_Pokok
                            });
                            tbGT.Add(new TbPegawaiGajiTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTetap = 2, //tunjanan operasional
                                Rp = item.GT_Operasional
                            });
                            tbGT.Add(new TbPegawaiGajiTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTetap = 3, //tunjangan jabatan
                                Rp = item.GT_Jabatan
                            });
                            tbGT.Add(new TbPegawaiGajiTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTetap = 4, //tunjangan keluarga
                                Rp = item.GT_Keluarga
                            });

                            //gaji tidak tetap
                            tbGTT.Add(new TbPegawaiGajiTidakTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTidakTetap = 1,
                                Rp = item.GTT_Lembur
                            });

                            tbGTT.Add(new TbPegawaiGajiTidakTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTidakTetap = 2,
                                Rp = item.GTT_Anomali
                            });
                            tbGTT.Add(new TbPegawaiGajiTidakTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTidakTetap = 3,
                                Rp = item.GTT_Performa
                            });
                            tbGTT.Add(new TbPegawaiGajiTidakTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTidakTetap = 4,
                                Rp = item.GTT_Bpjs
                            });
                            tbGTT.Add(new TbPegawaiGajiTidakTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTidakTetap = 5,
                                Rp = item.GTT_Pensiun
                            });
                            tbGTT.Add(new TbPegawaiGajiTidakTetap
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTidakTetap = 6,
                                Rp = item.GTT_Thr
                            });

                            //potongan
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 1,
                                Rp = item.Pot_Koreksi
                            });
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 2,
                                Rp = item.Pot_Icat
                            });
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 3,
                                Rp = item.Pot_DayCare
                            });
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 4,
                                Rp = item.Pot_Koperasi
                            });
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 5,
                                Rp = item.Pot_Absensi
                            });
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 6,
                                Rp = item.Pot_Lembur
                            });
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 7,
                                Rp = item.Pot_Bpjs
                            });
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 8,
                                Rp = item.Pot_Peserta
                            });
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 9,
                                Rp = item.Pot_Pensiun
                            });
                            tbPot.Add(new TbPegawaiGajiPotongan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiPotongan = 10,
                                Rp = item.Pot_Lainnya
                            });

                            //tambahan
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 1,
                                Rp = item.Tam_Koreksi
                            });
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 2,
                                Rp = item.Tam_Lembur
                            });
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 3,
                                Rp = item.Tam_InvalGuru
                            });
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 4,
                                Rp = item.Tam_OverTime
                            });
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 5,
                                Rp = item.Tam_Backup
                            });
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 6,
                                Rp = item.Tam_LemburHari
                            });
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 7,
                                Rp = item.Tam_LemburJam
                            });
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 8,
                                Rp = item.Tam_Tunjangan
                            });
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 9,
                                Rp = item.Tam_Bpap
                            });
                            tbTam.Add(new TbPegawaiGajiTambahan
                            {
                                Periode = periode,
                                IdPegawai = tbPegawai.IdPegawai,
                                IdJenisGajiTambahan = 10,
                                Rp = item.Tam_Lainnya
                            });
                        }
                        Console.WriteLine("proses gaji tetap");
                        var affectedRowsGT = conn.Execute(sqlGT, tbGT);
                        Console.WriteLine("proses gaji tidak tetap");
                        var affectedRowsGTT = conn.Execute(sqlGTT, tbGTT);
                        Console.WriteLine("proses gaji potongan");
                        var affectedRowsPot = conn.Execute(sqlPot, tbPot);
                        Console.WriteLine("proses gaji tambahan");
                        var affectedRowsTam = conn.Execute(sqlTam, tbTam);
                        Console.WriteLine("proses gaji pegawai");
                        var affectedRowsGaji = conn.Execute(sqlPegawaiGaji, tbGaji);
                        tx.Commit();
                        Console.WriteLine(DateTime.Now);
                        return err;
                    }
                }
            }
            catch (Exception ex)
            {
                return tempErr + Environment.NewLine + commonService.GetErrorMessage(ServiceName + "UploadGaji", ex);
            }

        }
        public List<PegawaiGajiListModel> GetPegawaiGajis(int IdPegawai, int Month, int Year, out string oMessage)
        {
            oMessage = string.Empty;
            List<PegawaiGajiListModel> ret = new List<PegawaiGajiListModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    int lastMonth = Month == 0 ? DateTime.Now.Month : Month;
                    int lastYear = Year == 0 ? DateTime.Now.Year : Year;
                    if (lastMonth == 0) { lastMonth = 12; lastYear -= 1; }

                    for (int i = 1; i <= lastMonth; i++)
                    {
                        var PeriodeBulan = i > 9 ? i.ToString() : "0" + i;
                        var data = (from a in conn.GetList<TbPegawaiGaji>()
                                    where a.IdPegawai == IdPegawai && a.Periode == dateTimeService.StrToDateTime("01-" + PeriodeBulan + "-" + lastYear)
                                    select new { a }).FirstOrDefault();

                        if (data != null)
                        {
                            PegawaiGajiListModel m = new PegawaiGajiListModel
                            {
                                IdPegawai = data.a.IdPegawai,
                                Nama = data.a.NamaPegawai,
                                Nip = data.a.Nip
                            };

                            m.Periode = i.ToString().PadLeft(2, '0') + "-" + lastYear.ToString();

                            var gt = from a in conn.GetList<TbPegawaiGajiTetap>()
                                     where a.IdPegawai == IdPegawai
                                     & a.Periode == new DateTime(lastYear, i, 1)
                                     select new { Rp = commonService.BulatkanRpPuluh(a.Rp), a.IdJenisGajiTetap, a.IdPegawai, a.Periode };
                            if (gt != null & gt.Count() > 0)
                            {
                                m.GT_Total = gt.Sum(x => x.Rp);
                            }
                            var gtt = from a in conn.GetList<TbPegawaiGajiTidakTetap>()
                                      where a.IdPegawai == IdPegawai
                                      & a.Periode == new DateTime(lastYear, i, 1)
                                      select new { Rp = commonService.BulatkanRpPuluh(a.Rp), a.IdJenisGajiTidakTetap, a.IdPegawai, a.Periode };
                            if (gtt != null & gtt.Count() > 0)
                            {
                                m.GTT_Total = gtt.Sum(x => x.Rp);
                            }
                            var pot = from a in conn.GetList<TbPegawaiGajiPotongan>()
                                      where a.IdPegawai == IdPegawai
                                      & a.Periode == new DateTime(lastYear, i, 1)
                                      select new { Rp = commonService.BulatkanRpPuluh(a.Rp), a.IdJenisGajiPotongan, a.IdPegawai, a.Periode };
                            if (pot != null & pot.Count() > 0)
                            {
                                m.Pot_Total = pot.Sum(x => x.Rp);
                            }
                            var tam = from a in conn.GetList<TbPegawaiGajiTambahan>()
                                      where a.IdPegawai == IdPegawai
                                      & a.Periode == new DateTime(lastYear, i, 1)
                                      select new { Rp = commonService.BulatkanRpPuluh(a.Rp), a.IdJenisGajiTambahan, a.IdPegawai, a.Periode };
                            if (tam != null & tam.Count() > 0)
                            {
                                m.Tam_Total = tam.Sum(x => x.Rp);
                            }
                            m.Total = m.GT_Total + m.GTT_Total - m.Pot_Total + m.Tam_Total;
                            if (m.Total > 0)
                                ret.Add(m);
                        }

                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPegawaiGajis", ex);
                return null;

            }
        }
        private SlipGajiModel GetSlipGaji(IDbConnection conn, int IdPegawai, string Periode, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                oMessage = string.Empty;
                var data = (from a in conn.GetList<TbPegawaiGaji>()
                            where a.IdPegawai == IdPegawai &&
                            a.Periode == dateTimeService.StrToDateTime("01-" + Periode)
                            select new { a }).FirstOrDefault();
                if (data == null) { oMessage = "data tidak ada"; return null; }

                SlipGajiModel ret = new SlipGajiModel();
                ret.Bulan = Periode;
                ret.Unit = data.a.Unit;
                ret.Nip = data.a.Nip;
                ret.Nama = data.a.NamaPegawai;
                ret.PendidikanTerakhir = data.a.JenjangPendidikan;
                ret.TanggalMasuk = data.a.TanggalMasuk.ToString("dd-MM-yyyy");
                ret.Golongan = data.a.GolonganPegawai;
                ret.JenisPegawai = data.a.JenisPegawai;

                if (data.a.TanggalMasuk.Month > 7)
                {
                    // jika bulan masuk lebih dari bulan juli, dihitung tahun depan nya
                    ret.Mkg = dateTimeService.GetCurrdate().Year - (data.a.TanggalMasuk.Year + 1);
                }
                else
                {
                    ret.Mkg = dateTimeService.GetCurrdate().Year - data.a.TanggalMasuk.Year;
                }

                ret.TakeHomePay = 0;
                var gt = from a in conn.GetList<TbPegawaiGajiTetap>()
                         join b in conn.GetList<TbJenisGajiTetap>() on a.IdJenisGajiTetap equals b.IdJenisGajiTetap
                         where a.IdPegawai == IdPegawai
                         & a.Periode == dateTimeService.StrToDateTime("01-" + Periode)
                         select new { a, b };
                if (gt != null & gt.Count() > 0)
                {
                    double total = 0;
                    ret.GajiTetaps = new List<SlipGajiDetilModel>();
                    foreach (var item in gt.OrderBy(x => x.b.IdJenisGajiTetap))
                    {
                        SlipGajiDetilModel m = new SlipGajiDetilModel
                        {
                            NoUrut = item.b.IdJenisGajiTetap,
                            Keterangan = item.b.Nama,
                            Rp = commonService.BulatkanRpPuluh(item.a.Rp)
                        };
                        total += m.Rp;
                        ret.GajiTetaps.Add(m);
                    }
                    ret.GajiTetaps.Add(new SlipGajiDetilModel
                    {
                        NoUrut = 99,
                        Keterangan = "Total Gaji Tetap",
                        Rp = total
                    });
                    ret.TakeHomePay += total;
                }

                var gtt = from a in conn.GetList<TbPegawaiGajiTidakTetap>()
                          join b in conn.GetList<TbJenisGajiTidakTetap>() on a.IdJenisGajiTidakTetap equals b.IdJenisGajiTidakTetap
                          where a.IdPegawai == IdPegawai
                          & a.Periode == dateTimeService.StrToDateTime("01-" + Periode)
                          select new { a, b };
                if (gtt != null & gtt.Count() > 0)
                {
                    double total = 0;
                    ret.GajiTidakTetaps = new List<SlipGajiDetilModel>();
                    foreach (var item in gtt.OrderBy(x => x.b.IdJenisGajiTidakTetap))
                    {
                        SlipGajiDetilModel m = new SlipGajiDetilModel
                        {
                            NoUrut = item.b.IdJenisGajiTidakTetap,
                            Keterangan = item.b.Nama,
                            Rp = commonService.BulatkanRpPuluh(item.a.Rp)
                        };
                        total += m.Rp;
                        ret.GajiTidakTetaps.Add(m);
                    }
                    ret.GajiTidakTetaps.Add(new SlipGajiDetilModel
                    {
                        NoUrut = 99,
                        Keterangan = "Total Gaji Tidak Tetap",
                        Rp = total
                    });
                    ret.TakeHomePay += total;
                }

                var pot = from a in conn.GetList<TbPegawaiGajiPotongan>()
                          join b in conn.GetList<TbJenisGajiPotongan>() on a.IdJenisGajiPotongan equals b.IdJenisGajiPotongan
                          where a.IdPegawai == IdPegawai
                          & a.Periode == dateTimeService.StrToDateTime("01-" + Periode)
                          select new { a, b };
                if (pot != null & pot.Count() > 0)
                {
                    double total = 0;
                    ret.Potongans = new List<SlipGajiDetilModel>();
                    foreach (var item in pot.OrderBy(x => x.b.IdJenisGajiPotongan))
                    {
                        SlipGajiDetilModel m = new SlipGajiDetilModel
                        {
                            NoUrut = item.b.IdJenisGajiPotongan,
                            Keterangan = item.b.Nama,
                            Rp = commonService.BulatkanRpPuluh(item.a.Rp)
                        };
                        total += m.Rp;
                        ret.Potongans.Add(m);
                    }
                    ret.Potongans.Add(new SlipGajiDetilModel
                    {
                        NoUrut = 99,
                        Keterangan = "Total Potongan",
                        Rp = total
                    });
                    ret.TakeHomePay -= total;
                }

                var tam = from a in conn.GetList<TbPegawaiGajiTambahan>()
                          join b in conn.GetList<TbJenisGajiTambahan>() on a.IdJenisGajiTambahan equals b.IdJenisGajiTambahan
                          where a.IdPegawai == IdPegawai
                          & a.Periode == dateTimeService.StrToDateTime("01-" + Periode)
                          select new { a, b };
                if (tam != null & tam.Count() > 0)
                {
                    double total = 0;
                    ret.Tambahans = new List<SlipGajiDetilModel>();
                    foreach (var item in tam.OrderBy(x => x.b.IdJenisGajiTambahan))
                    {
                        SlipGajiDetilModel m = new SlipGajiDetilModel
                        {
                            NoUrut = item.b.IdJenisGajiTambahan,
                            Keterangan = item.b.Nama,
                            Rp = commonService.BulatkanRpPuluh(item.a.Rp)
                        };
                        total += m.Rp;
                        ret.Tambahans.Add(m);
                    }
                    ret.Tambahans.Add(new SlipGajiDetilModel
                    {
                        NoUrut = 99,
                        Keterangan = "Total Tambahan",
                        Rp = total
                    });
                    ret.TakeHomePay += total;
                }
                ret.Terbilang = commonService.Terbilang((decimal)ret.TakeHomePay);
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSlipGaji", ex);
                return null;
            }

        }
        public SlipGajiModel GetSlipGaji(int IdPegawai, string Periode, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    return GetSlipGaji(conn, IdPegawai, Periode, out oMessage);
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSlipGaji", ex);
                return null;
            }

        }
        private void SetReadGaji(IDbConnection conn, int IdUnit, int IdJabatan, out bool CanReadAll, out bool CanReadGT, out bool CanReadGTT, out bool CanReadPot, out bool CanReadTam, out bool CanReadTot)
        {
            CanReadGT = schService.CanAksesByJabatan(conn, IdJabatan, HakAksesConstant.CanReadGajiGT);
            CanReadGTT = schService.CanAksesByJabatan(conn, IdJabatan, HakAksesConstant.CanReadGajiGTT);
            CanReadPot = schService.CanAksesByJabatan(conn, IdJabatan, HakAksesConstant.CanReadGajiPot);
            CanReadTam = schService.CanAksesByJabatan(conn, IdJabatan, HakAksesConstant.CanReadGajiTam);
            CanReadTot = schService.CanAksesByJabatan(conn, IdJabatan, HakAksesConstant.CanReadGajiTot);
            CanReadAll = schService.CanAksesByJabatan(conn, IdJabatan, HakAksesConstant.CanReadGajiAll);
            if (IdUnit == UnitConstant.PER)
            {
                CanReadGT = true;
                CanReadGTT = true;
                CanReadPot = true;
                CanReadTam = true;
                CanReadTot = true;
                CanReadAll = true;
            }
            if (IdUnit == UnitConstant.DIR)
            {
                CanReadGT = true;
                CanReadGTT = true;
                CanReadPot = true;
                CanReadTam = true;
                CanReadTot = true;
                CanReadAll = true;
            }
        }
        public List<GajiPegawaiBulanModel> GetGajiBulans(int IdUser, int Month, int Year, out string oMessage)
        {
            oMessage = string.Empty;
            List<GajiPegawaiBulanModel> ret = new List<GajiPegawaiBulanModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var _pegawai = (from a in conn.GetList<TbPegawai>()
                                    join b in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals b.IdPegawai
                                    where a.IdPegawai == IdUser
                                    select new { a, b }).FirstOrDefault();
                    SetReadGaji(conn, _pegawai.b.IdUnit, _pegawai.a.IdJenisJabatan, out bool canReadAll, out bool canReadGT, out bool canReadGTT, out bool canReadPot, out bool canReadTam, out bool canReadTot);

                    var _pegawaiGaji = (from a in conn.GetList<TbPegawaiGaji>()
                                        where a.IdPegawai == IdUser
                                        select new { a }).FirstOrDefault();
                    if (_pegawaiGaji == null) { oMessage = "data user tidak ada"; return null; }



                    int lastMonth = Month == 0 ? DateTime.Now.Month : Month;
                    // int lastMonth = 12;
                    int lastYear = Year == 0 ? DateTime.Now.Year : Year;
                    // int lastYear = 2020;
                    if (lastMonth == 0) { lastMonth = 12; lastYear -= 1; }

                    for (int i = 1; i <= lastMonth; i++)
                    {
                        GajiPegawaiBulanModel m = new GajiPegawaiBulanModel
                        {
                            Periode = i.ToString().PadLeft(2, '0') + "-" + lastYear.ToString(),
                            GT_Total = 0,
                            GTT_Total = 0,
                            Pot_Total = 0,
                            Tam_Total = 0,
                            Total = 0
                        };
                        var gt = from a in conn.GetList<TbPegawaiGajiTetap>()
                                 join b in conn.GetList<TbPegawaiGaji>() on a.IdPegawai equals b.IdPegawai
                                 where b.Unit == (canReadAll ? b.Unit : _pegawaiGaji.a.Unit)
                                 & a.Periode == new DateTime(lastYear, i, 1)
                                 & b.Periode == new DateTime(lastYear, i, 1)
                                 select new { Rp = commonService.BulatkanRpPuluh(a.Rp), a.IdPegawai, a.Periode };
                        if (gt != null & gt.Count() > 0)
                        {
                            m.GT_Total = gt.Sum(x => x.Rp);
                        }

                        var gtt = from a in conn.GetList<TbPegawaiGajiTidakTetap>()
                                  join b in conn.GetList<TbPegawaiGaji>() on a.IdPegawai equals b.IdPegawai
                                  where b.Unit == (canReadAll ? b.Unit : _pegawaiGaji.a.Unit)
                                   & a.Periode == new DateTime(lastYear, i, 1)
                                   & b.Periode == new DateTime(lastYear, i, 1)
                                  select new { Rp = commonService.BulatkanRpPuluh(a.Rp), a.IdPegawai, a.Periode };
                        if (gtt != null & gtt.Count() > 0)
                        {
                            m.GTT_Total = gtt.Sum(x => x.Rp);
                        }

                        var pot = from a in conn.GetList<TbPegawaiGajiPotongan>()
                                  join b in conn.GetList<TbPegawaiGaji>() on a.IdPegawai equals b.IdPegawai
                                  where b.Unit == (canReadAll ? b.Unit : _pegawaiGaji.a.Unit)
                                   & a.Periode == new DateTime(lastYear, i, 1)
                                   & b.Periode == new DateTime(lastYear, i, 1)
                                  select new { Rp = commonService.BulatkanRpPuluh(a.Rp), a.IdPegawai, a.Periode };
                        if (pot != null & pot.Count() > 0)
                        {
                            m.Pot_Total = pot.Sum(x => x.Rp);
                        }

                        var tam = from a in conn.GetList<TbPegawaiGajiTambahan>()
                                  join b in conn.GetList<TbPegawaiGaji>() on a.IdPegawai equals b.IdPegawai
                                  where b.Unit == (canReadAll ? b.Unit : _pegawaiGaji.a.Unit)
                                   & a.Periode == new DateTime(lastYear, i, 1)
                                   & b.Periode == new DateTime(lastYear, i, 1)
                                  select new { Rp = commonService.BulatkanRpPuluh(a.Rp), a.IdPegawai, a.Periode };
                        if (tam != null & tam.Count() > 0)
                        {
                            m.Tam_Total = tam.Sum(x => x.Rp);
                        }
                        m.Total = m.GT_Total + m.GTT_Total - m.Pot_Total + m.Tam_Total;
                        if (!canReadGT)
                            m.GT_Total = 0;
                        if (!canReadGTT)
                            m.GTT_Total = 0;
                        if (!canReadPot)
                            m.Pot_Total = 0;
                        if (!canReadTam)
                            m.Tam_Total = 0;
                        if (!canReadTot)
                            m.Total = 0;
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetGajiBulans", ex);
                return null;
            }
        }
        public List<GajiPegawaiUnitModel> GetGajiBulanUnits(int IdUser, string Periode, out string oMessage)
        {
            oMessage = string.Empty;
            List<GajiPegawaiUnitModel> ret = new List<GajiPegawaiUnitModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var _pegawai = (from a in conn.GetList<TbPegawai>()
                                    join b in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals b.IdPegawai
                                    where a.IdPegawai == IdUser
                                    select new { a, b }).FirstOrDefault();
                    SetReadGaji(conn, _pegawai.b.IdUnit, _pegawai.a.IdJenisJabatan, out bool canReadAll, out bool canReadGT, out bool canReadGTT, out bool canReadPot, out bool canReadTam, out bool canReadTot);

                    var _pegawaiGaji = (from a in conn.GetList<TbPegawaiGaji>()
                                        where a.IdPegawai == IdUser
                                        select new { a }).FirstOrDefault();

                    if (_pegawaiGaji == null) { oMessage = "data user tidak ada"; return null; }


                    var tbUnits = from a in conn.GetList<TbUnit>()
                                  where a.IdUnit == (canReadAll ? a.IdUnit : _pegawai.b.IdUnit)
                                  select a;
                    foreach (var item in tbUnits)
                    {
                        GajiPegawaiUnitModel m = new GajiPegawaiUnitModel
                        {
                            IdUnit = item.IdUnit,
                            Unit = item.Nama,
                            Periode = Periode,
                            GT_Total = 0,
                            GTT_Total = 0,
                            Pot_Total = 0,
                            Tam_Total = 0,
                            Total = 0
                        };

                        var gt = from a in conn.GetList<TbPegawaiGajiTetap>()
                                 join b in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals b.IdPegawai
                                 where b.IdUnit == item.IdUnit
                                 & a.Periode == dateTimeService.StrToDateTime("01-" + Periode)
                                 select new { Rp = commonService.BulatkanRpPuluh(a.Rp), a.IdPegawai, a.Periode };
                        if (gt != null & gt.Count() > 0)
                        {
                            m.GT_Total = gt.Sum(x => x.Rp);
                        }

                        var gtt = from a in conn.GetList<TbPegawaiGajiTidakTetap>()
                                  join b in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals b.IdPegawai
                                  where b.IdUnit == item.IdUnit
                                  & a.Periode == dateTimeService.StrToDateTime("01-" + Periode)
                                  select new { Rp = commonService.BulatkanRpPuluh(a.Rp), a.IdPegawai, a.Periode };
                        if (gtt != null & gtt.Count() > 0)
                        {
                            m.GTT_Total = gtt.Sum(x => x.Rp);
                        }

                        var pot = from a in conn.GetList<TbPegawaiGajiPotongan>()
                                  join b in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals b.IdPegawai
                                  where b.IdUnit == item.IdUnit
                                  & a.Periode == dateTimeService.StrToDateTime("01-" + Periode)
                                  select new { Rp = commonService.BulatkanRpPuluh(a.Rp), a.IdPegawai, a.Periode };
                        if (pot != null & pot.Count() > 0)
                        {
                            m.Pot_Total = pot.Sum(x => x.Rp);
                        }

                        var tam = from a in conn.GetList<TbPegawaiGajiTambahan>()
                                  join b in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals b.IdPegawai
                                  where b.IdUnit == item.IdUnit
                                  & a.Periode == dateTimeService.StrToDateTime("01-" + Periode)
                                  select new { Rp = commonService.BulatkanRpPuluh(a.Rp), a.IdPegawai, a.Periode };
                        if (tam != null & tam.Count() > 0)
                        {
                            m.Tam_Total = tam.Sum(x => x.Rp);
                        }
                        m.Total = m.GT_Total + m.GTT_Total - m.Pot_Total + m.Tam_Total;

                        if (!canReadGT)
                            m.GT_Total = 0;
                        if (!canReadGTT)
                            m.GTT_Total = 0;
                        if (!canReadPot)
                            m.Pot_Total = 0;
                        if (!canReadTam)
                            m.Tam_Total = 0;
                        if (!canReadTot)
                            m.Total = 0;

                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetGajiBulanUnits", ex);
                return null;
            }
        }
        public List<PegawaiGajiListModel> GetGajiBulanUnitPegawais(int IdUser, string Periode, int IdUnit, out string oMessage)
        {
            oMessage = string.Empty;
            List<PegawaiGajiListModel> ret = new List<PegawaiGajiListModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var _pegawai = (from a in conn.GetList<TbPegawai>()
                                    join b in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals b.IdPegawai
                                    where a.IdPegawai == IdUser
                                    select new { a, b }).FirstOrDefault();
                    SetReadGaji(conn, _pegawai.b.IdUnit, _pegawai.a.IdJenisJabatan, out bool canReadAll, out bool canReadGT, out bool canReadGTT, out bool canReadPot, out bool canReadTam, out bool canReadTot);

                    var tbPegawais = from a in conn.GetList<TbUnitPegawai>()
                                     join b in conn.GetList<TbPegawaiGaji>() on a.IdPegawai equals b.IdPegawai
                                     where a.IdUnit == IdUnit &&
                                     b.Periode == dateTimeService.StrToDateTime("01-" + Periode)
                                     select new { a, b };
                    foreach (var item in tbPegawais)
                    {
                        PegawaiGajiListModel m = new PegawaiGajiListModel
                        {
                            IdPegawai = item.a.IdPegawai,
                            Nama = item.b.NamaPegawai,
                            Nip = item.b.Nip
                        };

                        m.Periode = Periode;

                        var gt = from a in conn.GetList<TbPegawaiGajiTetap>()
                                 where a.IdPegawai == item.a.IdPegawai
                                 & a.Periode == dateTimeService.StrToDateTime("01-" + Periode)
                                 select new { Rp = commonService.BulatkanRpPuluh(a.Rp), a.IdJenisGajiTetap, a.IdPegawai, a.Periode };
                        if (gt != null & gt.Count() > 0)
                        {
                            m.GT_Total = gt.Sum(x => x.Rp);
                        }
                        var gtt = from a in conn.GetList<TbPegawaiGajiTidakTetap>()
                                  where a.IdPegawai == item.a.IdPegawai
                                  & a.Periode == dateTimeService.StrToDateTime("01-" + Periode)
                                  select new { Rp = commonService.BulatkanRpPuluh(a.Rp), a.IdJenisGajiTidakTetap, a.IdPegawai, a.Periode };
                        if (gtt != null & gtt.Count() > 0)
                        {
                            m.GTT_Total = gtt.Sum(x => x.Rp);
                        }
                        var pot = from a in conn.GetList<TbPegawaiGajiPotongan>()
                                  where a.IdPegawai == item.a.IdPegawai
                                  & a.Periode == dateTimeService.StrToDateTime("01-" + Periode)
                                  select new { Rp = commonService.BulatkanRpPuluh(a.Rp), a.IdJenisGajiPotongan, a.IdPegawai, a.Periode };
                        if (pot != null & pot.Count() > 0)
                        {
                            m.Pot_Total = pot.Sum(x => x.Rp);
                        }
                        var tam = from a in conn.GetList<TbPegawaiGajiTambahan>()
                                  where a.IdPegawai == item.a.IdPegawai
                                  & a.Periode == dateTimeService.StrToDateTime("01-" + Periode)
                                  select new { Rp = commonService.BulatkanRpPuluh(a.Rp), a.IdJenisGajiTambahan, a.IdPegawai, a.Periode };
                        if (tam != null & tam.Count() > 0)
                        {
                            m.Tam_Total = tam.Sum(x => x.Rp);
                        }
                        m.Total = m.GT_Total + m.GTT_Total - m.Pot_Total + m.Tam_Total;

                        if (!canReadGT)
                            m.GT_Total = 0;
                        if (!canReadGTT)
                            m.GTT_Total = 0;
                        if (!canReadPot)
                            m.Pot_Total = 0;
                        if (!canReadTam)
                            m.Tam_Total = 0;
                        if (!canReadTot)
                            m.Total = 0;

                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetGajiBulanUnitPegawais", ex);
                return null;
            }
        }
        public SlipGajiModel GetSlipGaji(int IdUser, int IdPegawai, string Periode, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    oMessage = string.Empty;
                    var _pegawai = (from a in conn.GetList<TbPegawai>()
                                    join b in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals b.IdPegawai
                                    join c in conn.GetList<TbJenisJabatan>() on a.IdJenisJabatan equals c.IdJenisJabatan
                                    where a.IdPegawai == IdUser
                                    select new { a, b, c }).FirstOrDefault();
                    if (_pegawai == null) { oMessage = "data user tidak ada"; return null; }

                    SetReadGaji(conn, _pegawai.b.IdUnit, _pegawai.a.IdJenisJabatan, out bool canReadAll, out bool canReadGT, out bool canReadGTT, out bool canReadPot, out bool canReadTam, out bool canReadTot);
                    var ret = GetSlipGaji(conn, IdPegawai, Periode, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    if (!canReadGT)
                        ret.GajiTetaps = new List<SlipGajiDetilModel>();
                    if (!canReadGTT)
                        ret.GajiTidakTetaps = new List<SlipGajiDetilModel>();
                    if (!canReadPot)
                        ret.Potongans = new List<SlipGajiDetilModel>();
                    if (!canReadTam)
                        ret.Tambahans = new List<SlipGajiDetilModel>();
                    if (!canReadTot)
                    {
                        ret.TakeHomePay = 0;
                        ret.Terbilang = string.Empty;
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSlipGaji", ex);
                return null;
            }

        }


        public List<PegawaiModel> GetListPegawaiBy(int IdRole, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbPegawai>()
                               join b in conn.GetList<TbUser>() on a.IdPegawai equals b.IdUser
                               join d in conn.GetList<TbJenisJabatan>() on a.IdJenisJabatan equals d.IdJenisJabatan
                               join e in conn.GetList<TbJenisJenjangPendidikan>() on a.IdJenisJenjangPendidikan equals e.IdJenisJenjangPendidikan
                               join f in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals f.IdPegawai
                               join g in conn.GetList<TbUnit>() on f.IdUnit equals g.IdUnit
                               join h in conn.GetList<TbUserRole>() on a.IdPegawai equals h.IdUser
                               where h.IdRole == IdRole
                               select new { a, b, d, e, f, g };
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    List<PegawaiModel> ret = new List<PegawaiModel>();
                    foreach (var item in data)
                    {
                        PegawaiModel m = new PegawaiModel();
                        m.IdPegawai = item.a.IdPegawai;
                        m.IdUnit = item.g.IdUnit;
                        m.Email = item.b.Email;
                        m.Nama = userAppService.SetFullName(item.b.FirstName, item.b.MiddleName, item.b.LastName);
                        m.JenisKelamin = JenisKelaminConstant.Dict[item.a.KdJenisKelamin];
                        m.Alamat = item.b.Address;
                        m.NoTelpon = item.b.PhoneNumber;
                        m.NoHandphone = item.b.MobileNumber;
                        m.Jabatan = item.d.Nama;
                        m.Agama = AgamaConstant.Dict[item.a.IdAgama];
                        m.JenjangPendidikan = item.e.NamaSingkat;
                        m.IdPegawai = item.a.IdPegawai;
                        m.IdJabatan = item.a.IdJenisJabatan;
                        m.Nip = item.a.Nip;
                        m.Nik = item.a.Nik; ;
                        m.TempatLahir = item.a.TempatLahir;
                        m.TanggalLahir = item.a.TanggalLahir.ToString("dd-MM-yyyy");
                        m.KdJenisKelamin = item.a.KdJenisKelamin;
                        m.IdAgama = item.a.IdAgama;
                        m.IdJenjangPendidikan = item.a.IdJenisJenjangPendidikan;
                        m.NamaInstitusiPendidikan = item.a.NamaInstitusiPendidikan;
                        m.NamaPasangan = item.a.NamaPasangan;
                        m.NoDarurat = item.a.NoDarurat;
                        m.IdJenisPegawai = item.a.IdJenisPegawai;
                        m.JenisPegawai = JenisPegawaiConstant.DictJenisPegawai[item.a.IdJenisPegawai];
                        if (item.a.Status == 1)
                        {
                            m.Status = StatusDataConstant.Dict[1];
                        }
                        else
                        {
                            m.Status = StatusDataConstant.Dict[-1];
                        }
                        m.IdGolonganPegawai = item.a.IdGolonganPegawai;
                        m.GolonganPegawai = JenisPegawaiGolonganConstant.DictJenisPegawaiGolongan[item.a.IdGolonganPegawai];
                        m.TanggalMasuk = item.a.TanggalMasuk.ToString("dd-MM-yyyy");
                        m.Username = item.b.Username;
                        m.Password = commonService.DecryptString(item.b.Password);
                        m.Unit = item.g.Nama;
                        m.NoKk = item.a.NoKk;
                        m.JurusanPendidikan = item.a.JurusanPendidikan;
                        m.AktifitasDakwah = item.a.AktifitasDakwah;
                        m.OrganisasiMasyarakat = item.a.OrganisasiMasyarakat;
                        m.PengalamanKerja = item.a.PengalamanKerja;
                        m.KelasDiampu = new List<PegawaiKelasParalelModel>();
                        var TbKelasParalelPegawai = (from ax in conn.GetList<TbKelasParalelPegawai>()
                                                     join bx in conn.GetList<TbKelasParalel>() on ax.IdKelasParalel equals bx.IdKelasParalel
                                                     where ax.IdPegawai == m.IdPegawai
                                                     select bx).ToList();
                        if (TbKelasParalelPegawai != null || TbKelasParalelPegawai.Count() != 0)
                        {
                            foreach (var val in TbKelasParalelPegawai)
                            {
                                PegawaiKelasParalelModel modx = new PegawaiKelasParalelModel();
                                modx.IdKelasParalel = val.IdKelasParalel;
                                modx.KelasParalel = val.Nama;
                                m.KelasDiampu.Add(modx);
                            }
                        }

                        m.Kompetensi = new List<PegawaiKompetensiModel>();
                        if (!string.IsNullOrEmpty(item.a.Kompetensi))
                        {
                            string[] _kompetensis = item.a.Kompetensi.Split(',');
                            foreach (var val in _kompetensis)
                            {
                                PegawaiKompetensiModel modx = new PegawaiKompetensiModel();
                                modx.IdKompetensi = val;
                                modx.Kompetensi = JenisKompetensiConstant.DictJenisKompetensi[Int32.Parse(val)];
                                m.Kompetensi.Add(modx);
                            }
                        }

                        ret.Add(m);
                    }
                    return ret.ToList();
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetListPegawai", ex);
                return null;
            }
        }

        public string UpdateTokenFcmPegawai(int IdUser, string Token, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbPegawai = (from a in conn.GetList<TbPegawai>()
                                         where a.IdPegawai == IdUser
                                         select a).FirstOrDefault();

                        if (tbPegawai == null)
                        {
                            oMessage = "data tidak ada";
                            return null;
                        }

                        var tbUserToken = (from a in conn.GetList<TbUserNotification>()
                                           where a.IdUser == IdUser & a.TokenFcm == Token
                                           select a).FirstOrDefault();
                        if (tbUserToken == null)
                        {
                            tbUserToken = new TbUserNotification
                            {
                                IdUser = IdUser,
                                TokenFcm = Token
                            };
                            conn.Insert(tbUserToken);
                        }
                        tx.Commit();
                        return null;
                    }

                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "UpdateTokenFcmPegawai", ex);
                return null;
            }
        }
        private async Task<string> KirimNotifikasi(string to, string title, string body, string click_action)
        {
            string oMessage = string.Empty;
            try
            {
                var serverKey = string.Format("key={0}", "AAAA6kRyEYM:APA91bFKEhOtQgjyj9KvoFvNPSZwUNUY2WXMZnCi8MWAHgnorouCKHFmHu37fb3WXleddSdY3x4_9fiqcgQz6hqa1iVfwBHm92RYp3cE6njvD4nVz35GMGbmOd5K_NLEciFU90f8aLOz");

                string icon = "http://admsimat.sekolahattaufiq.id/assets/img/atq.png";
                var data = new
                {
                    to,
                    notification = new { title, body, icon, click_action }
                };

                var jsonBody = JsonConvert.SerializeObject(data);
                using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "https://fcm.googleapis.com/fcm/send"))
                {
                    httpRequest.Headers.TryAddWithoutValidation("Authorization", serverKey);
                    httpRequest.Content = new StringContent(jsonBody, Encoding.UTF8, "application/json");

                    using (var httpClient = new HttpClient())
                    {
                        var result = await httpClient.SendAsync(httpRequest);
                        if (result.IsSuccessStatusCode)
                        {
                            return oMessage;
                        }
                        else
                        {
                            oMessage = "Error sending notification. Status Code:" + result.StatusCode;
                        }
                        return oMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = "Exception thrown in Notify Service " + ex;
                return oMessage;
            }

        }
        public string KirimNotifikasi(string to, string title, string body, string click_action, out string oMessage)
        {
            oMessage = string.Empty;
            var cek = KirimNotifikasi(to, title, body, click_action);
            return oMessage;
        }
    }
}
