﻿using System;
using System.Collections.Generic;
using Dapper;
using System.Linq;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using System.Data;
using Swg.Services;
using Swg.Models;
using Swg.Entities.Ref;
using Swg.Entities.Kbm;
using Swg.Entities.Public;

namespace Swg.Sch.Services
{
    public class EskulService : IEskulService
    {
        private string _languageCode = "id";
        private readonly string ServiceName = "Swg.Sch.Services.EskulService.";
        private readonly ICommonService commonService;
        private readonly IUserAppService userAppService;
        private readonly IDateTimeService dateTimeService;
        private readonly ISchService schService;
        public EskulService(
            ICommonService CommonService,
            IDateTimeService DateTimeService,
            IUserAppService UserAppService,
            ISchService SchService)
        {
            commonService = CommonService;
            userAppService = UserAppService;
            dateTimeService = DateTimeService;
            schService = SchService;
        }

       
        public List<ComboModel> GetJilidQuran()
        {
            try
            {
                using(var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbJenisJilidQuran>();
                    if (data == null) return null;
                    List<ComboModel> ret = new List<ComboModel>();
                    foreach (var item in data)
                    {
                        ret.Add(new ComboModel
                        {
                            Id = item.IdJenisJilidQuran,
                            Nama = item.Nama,
                            NamaSingkat = item.NamaSingkat

                        });
                    }
                    return ret;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public KelasQuranSiswaModel GetKelasQuranSiswa(int IdSiswa, string Tanggal, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                KelasQuranSiswaModel ret = new KelasQuranSiswaModel();
                using (var conn = commonService.DbConnection())
                {
                    ret = schService.GetKelasQuranSiswa(conn, IdSiswa, Tanggal, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return ret;
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelasQuranSiswa", ex);
                return null;
            }
        }

        public List<KelasQuranListModel> GetKelasQurans(int IdPegawai, string Tanggal, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                DateTime currDate = DateTime.Now;
                DateTime NextFirstDate = dateTimeService.GetFirstDate(currDate.AddDays(7));
                if (!string.IsNullOrEmpty(Tanggal))
                {
                    currDate = dateTimeService.StrToDateTime(Tanggal);
                }
                if (currDate >= NextFirstDate)
                {
                    oMessage = string.Format("kbm belum aktif untuk tanggal {0}", dateTimeService.DateToLongString(_languageCode, currDate));
                    return null;
                }

                DateTime minDate = dateTimeService.GetFirstDate(currDate);
                DateTime maxDate = dateTimeService.GetLastDate(currDate);

                List<KelasQuranListModel> ret = new List<KelasQuranListModel>();
                using (var conn = commonService.DbConnection())
                {
                    for (int i = 0; i < 7; i++)
                    {
                        DateTime tanggal = minDate.AddDays(i);
                        var item = (from a in conn.GetList<TbKelasQuran>()
                                    join b in conn.GetList<TbKelasQuranJadwal>() on a.IdKelasQuran equals b.IdKelasQuran
                                    join c in conn.GetList<TbKbmJadwal>() on b.IdKbmJadwal equals c.IdKbmJadwal
                                    where a.IdPegawai == IdPegawai
                                    & c.HariKe == (int)tanggal.DayOfWeek
                                    select a).FirstOrDefault();
                        if (item != null)
                        {
                            KelasQuranListModel m = new KelasQuranListModel
                            {
                                Tanggal = tanggal.ToString("dd-MM-yyyy"),
                                Hari = dateTimeService.GetLongDayName(_languageCode, (int)tanggal.DayOfWeek),
                                IdKelasQuran = item.IdKelasQuran,
                                Kelompok = item.Nama,
                                JumlahSiswa = 0,
                                JumlahInput = 0,
                                JumlahNoInput = 0
                            };
                            m.JumlahSiswa = (from a in conn.GetList<TbKelasQuranSiswa>()
                                             where a.IdKelasQuran == item.IdKelasQuran
                                             select a).Count();
                            m.JumlahInput = (from a in conn.GetList<TbKelasQuranProgres>()
                                             where a.IdKelasQuran == item.IdKelasQuran
                                             & a.Tanggal.Date == tanggal.Date
                                             select a).Count();
                            m.JumlahNoInput = m.JumlahSiswa - m.JumlahInput;
                            ret.Add(m);
                        }
                    }
                }
                if(ret.Count()==0) { oMessage = "data tidak ada"; return null; }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelasQurans", ex);
                return null;
            }
        }
        private string GetLastUjian(IDbConnection conn, int IdSiswa)
        {
            var tbKelasQurans = from a in conn.GetList<TbKelasQuranUjian>()
                     where a.IdSiswa == IdSiswa
                     select a;
            if (tbKelasQurans == null || tbKelasQurans.Count() == 0) return string.Empty;

            return tbKelasQurans.OrderByDescending(x => x.CreatedDate).First().Hasil;
        }
        public List<KelasQuranSiswaListModel> GetKelasQuranSiswas(int IdKelasQuran, string Tanggal, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<KelasQuranSiswaListModel> ret = new List<KelasQuranSiswaListModel>();
                using (var conn = commonService.DbConnection())
                {
                    var dataProgress = from a in conn.GetList<TbKelasQuranProgres>()
                                       join a1 in conn.GetList<TbJenisJilidQuran>() on a.IdJenisJilidQuran equals a1.IdJenisJilidQuran
                                       join b in conn.GetList<TbSiswa>() on a.IdSiswa equals b.IdSiswa
                                       join c in conn.GetList<TbKelasRombel>() on b.IdSiswa equals c.IdSiswa
                                       join d in conn.GetList<TbKelasParalel>() on c.IdKelasParalel equals d.IdKelasParalel
                                       where a.IdKelasQuran == IdKelasQuran
                                       & a.Tanggal == dateTimeService.StrToDateTime(Tanggal)
                                       select new { a, a1, b, c, d };
                    List<int> IdSiswas = new List<int>();
                    if (dataProgress != null & dataProgress.Count() > 0)
                        foreach (var item in dataProgress)
                        {
                            IdSiswas.Add(item.a.IdSiswa);
                            KelasQuranSiswaListModel m = new KelasQuranSiswaListModel
                            {
                                IdKelasQuran = IdKelasQuran,
                                IdSiswa = item.a.IdSiswa,
                                NamaSiswa = item.b.Nama,
                                NamaKelasParalel = item.d.Nama,
                                Catatan = item.a.Catatan,
                                Halaman = item.a.Halaman,
                                IdJenisJilidQuran = item.a.IdJenisJilidQuran,
                                JilidQuran = item.a1.Nama,
                                StatusUjian = GetLastUjian(conn, item.a.IdSiswa),
                                Tanggal = Tanggal
                            };
                            ret.Add(m);
                        }
                    var dataSiswas = (from a in conn.GetList<TbKelasQuranSiswa>()
                                      join b in conn.GetList<TbSiswa>() on a.IdSiswa equals b.IdSiswa
                                      join c in conn.GetList<TbKelasRombel>() on b.IdSiswa equals c.IdSiswa
                                      join d in conn.GetList<TbKelasParalel>() on c.IdKelasParalel equals d.IdKelasParalel
                                      where a.IdKelasQuran == IdKelasQuran
                                      select new { a, b, c, d }).Where(x => !IdSiswas.Contains(x.a.IdSiswa));
                    if(dataSiswas!=null & dataSiswas.Count()>0)
                        foreach (var item in dataSiswas)
                        {
                            KelasQuranSiswaListModel m = new KelasQuranSiswaListModel
                            {
                                IdKelasQuran = IdKelasQuran,
                                IdSiswa = item.a.IdSiswa,
                                NamaSiswa = item.b.Nama,
                                NamaKelasParalel = item.d.Nama,
                                Catatan = null,
                                Halaman = null,
                                IdJenisJilidQuran = null,
                                JilidQuran = null,
                                StatusUjian = null,
                                Tanggal = Tanggal
                            };
                            ret.Add(m);
                        }
                    if (ret.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelasQuranSiswas", ex);
                return null;
            }
        }
        public KelasQuranSiswaProgresModel GetKelasQuranSiswaProgres(int IdSiswa, int IdKelasQuran, string Tanggal, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using(var conn = commonService.DbConnection())
                {
                    var item = (from a in conn.GetList<TbKelasQuranProgres>()
                                join a1 in conn.GetList<TbJenisJilidQuran>() on a.IdJenisJilidQuran equals a1.IdJenisJilidQuran
                                join b in conn.GetList<TbKelasQuran>() on a.IdKelasQuran equals b.IdKelasQuran
                                join c in conn.GetList<TbUser>() on b.IdPegawai equals c.IdUser
                                join d in conn.GetList<TbSiswa>() on a.IdSiswa equals d.IdSiswa
                                where a.IdSiswa == IdSiswa
                                & a.IdKelasQuran == IdKelasQuran
                                & a.Tanggal.Date == dateTimeService.StrToDateTime(Tanggal).Date
                                select new { a, a1, b, c, d }).FirstOrDefault();
                    if (item == null) { oMessage = "data tidak ada"; return null; }
                    KelasQuranSiswaProgresModel ret = new KelasQuranSiswaProgresModel
                    {
                        Catatan = item.a.Catatan,
                        Halaman = item.a.Halaman,
                        Hari = dateTimeService.GetLongDayName(_languageCode, (int)item.a.Tanggal.DayOfWeek),
                        IdJenisJilidQuran = item.a.IdJenisJilidQuran,
                        IdSiswa = item.a.IdSiswa,
                        JilidQuran = item.a1.Nama,
                        NamaKelasQuran = item.b.Nama,
                        NamaGuru = userAppService.SetFullName(item.c.FirstName, item.c.MiddleName, item.c.LastName),
                        NamaSiswa = item.d.Nama,
                        Tanggal = Tanggal
                    };
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelasQuranSiswaProgres", ex);
                return null;
            }
        }
        public KelasQuranSiswaUjianModel GetKelasQuranSiswaUjian(int IdSiswa, int IdKelasQuran, string Tanggal, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var item = (from a in conn.GetList<TbKelasQuranUjian>()
                                join a1 in conn.GetList<TbJenisJilidQuran>() on a.IdJenisJilidQuran equals a1.IdJenisJilidQuran
                                join b in conn.GetList<TbKelasQuran>() on a.IdKelasQuran equals b.IdKelasQuran
                                join c in conn.GetList<TbUser>() on b.IdPegawai equals c.IdUser
                                join d in conn.GetList<TbSiswa>() on a.IdSiswa equals d.IdSiswa
                                where a.IdSiswa == IdSiswa
                                & a.IdKelasQuran == IdKelasQuran
                                & a.Tanggal.Date == dateTimeService.StrToDateTime(Tanggal).Date
                                select new { a, a1, b, c, d }).FirstOrDefault();
                    if (item == null) { oMessage = "data tidak ada"; return null; }
                    KelasQuranSiswaUjianModel ret = new KelasQuranSiswaUjianModel
                    {
                        Catatan = item.a.Catatan,
                        IdJenisJilidQuran = item.a.IdJenisJilidQuran,
                        IdSiswa = item.a.IdSiswa,
                        JilidQuran = item.a1.Nama,
                        NamaKelasQuran = item.b.Nama,
                        NamaGuru = userAppService.SetFullName(item.c.FirstName, item.c.MiddleName, item.c.LastName),
                        Tanggal = Tanggal,
                        Hasil = item.a.Hasil,
                        IdKelasQuran = item.a.IdKelasQuran,
                        Nilai = item.a.NilaiHuruf
                    };
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelasQuranSiswaUjian", ex);
                return null;
            }
        }
        public string KelasQuranSiswaProgresAdd(int IdPegawai, int IdSiswa, int IdKelasQuran, string Tanggal, int IdJenisJilidQuran, int Halaman, string Catatan)
        {
            try
            {
                if (string.IsNullOrEmpty(Tanggal)) return "tanggal harus ditentukan";
                DateTime tanggal = dateTimeService.StrToDateTime(Tanggal);
                if (tanggal.Date > DateTime.Now.Date) return string.Format("tanggal {0}, kbm belum aktif", Tanggal);
                using(var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using(var tx = conn.BeginTransaction())
                    {
                        var tbKelasQuranProgres = (from a in conn.GetList<TbKelasQuranProgres>()
                                                  where a.IdKelasQuran == IdKelasQuran
                                                        & a.Tanggal == tanggal
                                                        & a.IdSiswa == IdSiswa
                                                  select a).FirstOrDefault();
                        bool isInsert = false;
                        if(tbKelasQuranProgres == null)
                        {
                            isInsert = true;
                            tbKelasQuranProgres = new TbKelasQuranProgres
                            {
                                IdSiswa = IdSiswa,
                                IdKelasQuran = IdKelasQuran,
                                CreatedBy = IdPegawai,
                                CreatedDate = dateTimeService.GetCurrdate()
                            };
                        }
                        else
                        {
                            tbKelasQuranProgres.UpdatedBy = IdPegawai;
                            tbKelasQuranProgres.UpdatedDate = dateTimeService.GetCurrdate();
                        }
                        tbKelasQuranProgres.Tanggal = tanggal;
                        tbKelasQuranProgres.IdJenisJilidQuran = IdJenisJilidQuran;
                        tbKelasQuranProgres.Catatan = Catatan;
                        tbKelasQuranProgres.Halaman = Halaman;
                        if(isInsert)
                        {
                            conn.Insert(tbKelasQuranProgres);
                        }
                        else
                        {
                            conn.Update(tbKelasQuranProgres);
                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "KelasQuranAdd", ex);
            }
        }
        public string KelasQuranSiswaUjianAdd(int IdPegawai, int IdSiswa, int IdKelasQuran, string Tanggal, int IdJenisJilidQuran, string Hasil, string Nilai, string Catatan)
        {
            try
            {
                if (string.IsNullOrEmpty(Tanggal)) return "tanggal harus ditentukan";
                DateTime tanggal = dateTimeService.StrToDateTime(Tanggal);
                if (tanggal.Date > DateTime.Now.Date) return string.Format("tanggal {0}, kbm belum aktif", Tanggal);
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbKelasQuranUjian = (from a in conn.GetList<TbKelasQuranUjian>()
                                                   where a.IdKelasQuran == IdKelasQuran
                                                         & a.Tanggal == tanggal
                                                         & a.IdSiswa == IdSiswa
                                                   select a).FirstOrDefault();
                        bool isInsert = false;
                        if (tbKelasQuranUjian == null)
                        {
                            isInsert = true;
                            tbKelasQuranUjian = new TbKelasQuranUjian
                            {
                                IdSiswa = IdSiswa,
                                IdKelasQuran = IdKelasQuran,
                                CreatedBy = IdPegawai,
                                CreatedDate = dateTimeService.GetCurrdate()
                            };
                        }
                        else
                        {
                            tbKelasQuranUjian.UpdatedBy = IdPegawai;
                            tbKelasQuranUjian.UpdatedDate = dateTimeService.GetCurrdate();
                        }
                        tbKelasQuranUjian.Tanggal = tanggal;
                        tbKelasQuranUjian.IdJenisJilidQuran = IdJenisJilidQuran;
                        tbKelasQuranUjian.Catatan = Catatan;
                        tbKelasQuranUjian.Hasil = Hasil;
                        tbKelasQuranUjian.NilaiHuruf = Nilai;
                        if (isInsert)
                        {
                            conn.Insert(tbKelasQuranUjian);
                        }
                        else
                        {
                            conn.Update(tbKelasQuranUjian);
                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "KelasQuranSiswaUjianAdd", ex);
            }
        }
    }
}
