﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Swg.Entities.Kbm;
using Swg.Entities.Lmg;
using Swg.Entities.Ppdb;
using Swg.Entities.Public;
using Swg.Entities.Ref;
using Swg.Models;
using Swg.Sch.Shared.Constants;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;
using Swg.Models.Constants;
using System.Text.RegularExpressions;
using Swg.Entities.His;
using System.IO;

namespace Swg.Sch.Services
{
    public class WebService : IWebService
    {

        private readonly string ServiceName = "Swg.Sch.Services.WebService.";
        private string _languageCode = "id";
        private readonly ICommonService commonService;
        private readonly IUserAppService userAppService;
        private readonly IDateTimeService dateTimeService;
        private readonly ISchService schService;
        public WebService(
            ICommonService CommonService,
            IDateTimeService DateTimeService,
            IUserAppService UserAppService,
            ISchService SchService)
        {
            commonService = CommonService;
            userAppService = UserAppService;
            dateTimeService = DateTimeService;
            schService = SchService;
        }
        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }


        public SiswaModel GetSiswaByNis(string Nis, out string oMessage)
        {
            using (var conn = commonService.DbConnection())
            {
                var tb = conn.GetList<TbSiswa>("where nis = '" + Nis + "'").FirstOrDefault();
                if (tb == null)
                {
                    oMessage = "Data tidak ada";
                    return null;
                }
                return schService.GetSiswa(tb.IdSiswa, out oMessage);
            }
        }

        public SiswaModel GetSiswaById(int IdSiswa, out string oMessage)
        {
            return GetSiswa(null, IdSiswa, out oMessage);

        }

        private SiswaModel GetSiswa(string Nis, int IdSiswa, out string oMessage)
        {

            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    TbSiswa _siswa = new TbSiswa();
                    if (!string.IsNullOrEmpty(Nis))
                    {
                        _siswa = conn.GetList<TbSiswa>("where nis = '" + Nis + "'").FirstOrDefault();
                    }
                    else
                    {
                        _siswa = conn.Get<TbSiswa>(IdSiswa);
                    }

                    if (_siswa == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    if (_siswa.Status != 1)
                    {
                        oMessage = "data tidak aktif";
                        return null;
                    }

                    SiswaModel siswa = new SiswaModel
                    {
                        IdSiswa = _siswa.IdSiswa,
                        Nis = _siswa.Nis,
                        Nik = _siswa.Nik,
                        Nama = _siswa.Nama,
                        NamaPanggilan = _siswa.NamaPanggilan,
                        TempatLahir = _siswa.TempatLahir,
                        TanggalLahir = _siswa.TanggalLahir.ToString("dd-MM-yyyy"),
                        IdJenisPrestasi = 1,
                        IdAgama = _siswa.IdAgama,
                        JenisKelamin = _siswa.KdJenisKelamin,
                        NikIbu = _siswa.NikIbu,
                        NamaIbu = _siswa.NamaIbu,
                        IdJenisPekerjaanIbu = _siswa.IdJenisPekerjaanIbu,
                        NikAyah = _siswa.NikAyah,
                        NamaAyah = _siswa.NamaAyah,
                        IdJenisPekerjaanAyah = _siswa.IdJenisPekerjaanAyah,
                        NoHandphone = _siswa.NoHandphone,
                        NoDarurat = _siswa.NoDarurat,
                        Email = _siswa.Email,
                        AlamatTinggal = _siswa.AlamatTinggal,
                        AlamatOrtu = _siswa.AlamatOrtu
                    };
                    return siswa;
                }

            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswa", ex);
                return null;
            }
        }

        public WebHomeModel GetHome(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                WebHomeModel ret = new WebHomeModel();
                using (var conn = commonService.DbConnection())
                {
                    var tbSekolah = (from a in conn.GetList<TbSekolah>()
                                     select a).FirstOrDefault();
                    if (tbSekolah == null) { oMessage = "data tidak ada"; return null; }
                    ret.Icon = tbSekolah.Icon;
                    ret.Logo = tbSekolah.Logo;
                    ret.FotoSekolah = tbSekolah.FotoWeb;
                    ret.Nama = tbSekolah.Nama;
                    ret.Moto = tbSekolah.Moto;

                    var tbSekolahVisiMisis = from a in conn.GetList<TbSekolahVisiMisi>()
                                             where a.IdSekolah == tbSekolah.IdSekolah
                                             select a;
                    ret.VisiMisi = new List<WebVisiMisiModel>();
                    foreach (var item in tbSekolahVisiMisis.OrderBy(x => x.IdJenisVisiMisi))
                    {
                        ret.VisiMisi.Add(new WebVisiMisiModel
                        {
                            Ikon = item.FileIkon,
                            Judul = item.Judul,
                            Keterangan = item.Konten
                        });
                    }
                    var tbUnits = from a in conn.GetList<TbUnit>()
                                  where a.IdSekolah == tbSekolah.IdSekolah
                                  select a;
                    ret.MotoUnit = new WebMotoUnitModel();
                    foreach (var item in tbUnits)
                    {
                        if (item.IdUnit == UnitConstant.Tk)
                        {
                            ret.MotoUnit.Tk = item.Moto;
                        }
                        else if (item.IdUnit == UnitConstant.Sd)
                        {
                            ret.MotoUnit.Sd = item.Moto;
                        }
                        else if (item.IdUnit == UnitConstant.Smp)
                        {
                            ret.MotoUnit.Smp = item.Moto;
                        }
                        else if (item.IdUnit == UnitConstant.Sma)
                        {
                            ret.MotoUnit.Sma = item.Moto;
                        }
                    }
                    ret.Kegiatan = new List<WebKegiatanModel>();

                    var blogs = (from a in conn.GetList<TbSekolahKegiatan>()
                                 join b in conn.GetList<TbUser>() on a.CreatedBy equals b.IdUser
                                 where a.IdSekolah == tbSekolah.IdSekolah
                                 select new { a, b }).OrderBy(x => x.a.IdSekolahKegiatan).Take(4);
                    foreach (var blog in blogs)
                    {
                        ret.Kegiatan.Add(new WebKegiatanModel
                        {
                            Judul = blog.a.Judul,
                            Keterangan = blog.a.Keterangan,
                            Url = blog.a.Url,
                            CreatedBy = userAppService.SetFullName(blog.b.FirstName, blog.b.MiddleName, blog.b.LastName),
                            CreatedDate = dateTimeService.DateToLongString(_languageCode, blog.a.CreatedDate)
                        });
                    }
                    ret.Kontak = new WebKontakModel
                    {
                        Alamat = tbSekolah.Alamat,
                        Desa = tbSekolah.Desa,
                        Email = tbSekolah.Email,
                        Kabupaten = tbSekolah.Kabupaten,
                        Kecamatan = tbSekolah.Kecamatan,
                        KodePos = tbSekolah.KodePos,
                        NoFaximili = tbSekolah.NoFaximili,
                        NoTelepon = tbSekolah.NoTelepon,
                        Provinsi = tbSekolah.Provinsi
                    };
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetHome", ex);
                return null;
            }
        }

        public List<ApplTaskModel> GetMenus(int IdAppl, int IdUser, out string oMessage)
        {
            throw new NotImplementedException();
        }

        public WebAboutModel GetAbout(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                WebAboutModel ret = new WebAboutModel();
                using (var conn = commonService.DbConnection())
                {
                    var tbSekolah = (from a in conn.GetList<TbSekolah>()
                                     select a).FirstOrDefault();
                    if (tbSekolah == null) { oMessage = "data tidak ada"; return null; }

                    #region VisiMisi
                    var tbSekolahVisiMisis = from a in conn.GetList<TbSekolahVisiMisi>()
                                             where a.IdSekolah == tbSekolah.IdSekolah
                                             select a;
                    ret.VisiMisi = new List<WebVisiMisiModel>();
                    foreach (var item in tbSekolahVisiMisis.OrderBy(x => x.IdJenisVisiMisi))
                    {
                        ret.VisiMisi.Add(new WebVisiMisiModel
                        {
                            Ikon = item.FileIkon,
                            Judul = item.Judul,
                            Keterangan = item.Konten
                        });
                    }
                    #endregion

                    #region Direksi
                    var tbDireksis = from pegawai in conn.GetList<TbPegawai>()
                                     join jabatan in conn.GetList<TbJenisJabatan>() on pegawai.IdJenisJabatan equals jabatan.IdJenisJabatan
                                     join user in conn.GetList<TbUser>() on pegawai.IdPegawai equals user.IdUser
                                     //                               join userProfile in conn.GetList<TbUserProfile>() on pegawai.IdPegawai equals userProfile.IdUser
                                     join jenjangPendidikan in conn.GetList<TbJenisJenjangPendidikan>() on pegawai.IdJenisJenjangPendidikan equals jenjangPendidikan.IdJenisJenjangPendidikan
                                     where jabatan.IdJenisJabatan == JenisJabatanPegawaiContant.Yay
                                     select new { pegawai, jabatan, user, jenjangPendidikan };
                    ret.Direksi = new List<WebPegawaiModel>();
                    foreach (var item in tbDireksis)
                    {
                        ret.Direksi.Add(new WebPegawaiModel
                        {
                            Nama = userAppService.SetFullName(item.user.FirstName, item.user.MiddleName, item.user.LastName),
                            Jabatan = item.jabatan.Nama,
                            FileFoto = item.user.ProfileFile
                        });
                    }
                    #endregion

                    #region Kurikulum
                    var tbKurikulums = from a in conn.GetList<TbUnit>()
                                       where a.IdSekolah == tbSekolah.IdSekolah
                                       select a;
                    ret.Kurikulum = new List<WebKurikulumModel>();
                    foreach (var item in tbKurikulums)
                    {
                        ret.Kurikulum.Add(new WebKurikulumModel
                        {
                            IdUnit = item.IdUnit,
                            NamaSekolah = item.Nama,
                            Keterangan = item.Kurikulum,
                        });
                    }
                    #endregion

                    #region Kontak
                    ret.Kontak = new WebKontakModel
                    {
                        Provinsi = tbSekolah.Provinsi,
                        Kabupaten = tbSekolah.Kabupaten,
                        Kecamatan = tbSekolah.Kecamatan,
                        Desa = tbSekolah.Desa,
                        KodePos = tbSekolah.KodePos,
                        Alamat = tbSekolah.Alamat,
                        Email = tbSekolah.Email,
                        NoFaximili = tbSekolah.NoFaximili,
                        NoTelepon = tbSekolah.NoTelepon
                    };
                    #endregion


                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetAbout", ex);
                return null;
            }
        }

        public WebPropenModel GetPropen(int IdUnit, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                WebPropenModel ret = new WebPropenModel();

                ret.Tkit = new WebUnitModel();
                ret.Tkit.Detail = new UnitReadModel();
                ret.Tkit.Kepsek = new WebUnitKepsekModel();

                ret.Sdit = new WebUnitModel();
                ret.Sdit.Detail = new UnitReadModel();
                ret.Sdit.Kepsek = new WebUnitKepsekModel();

                ret.Smpit = new WebUnitModel();
                ret.Smpit.Detail = new UnitReadModel();
                ret.Smpit.Kepsek = new WebUnitKepsekModel();

                ret.Smait = new WebUnitModel();
                ret.Smait.Detail = new UnitReadModel();
                ret.Smait.Kepsek = new WebUnitKepsekModel();

                using (var conn = commonService.DbConnection())
                {
                    var tbSekolah = (from a in conn.GetList<TbSekolah>()
                                     select a).FirstOrDefault();
                    if (tbSekolah == null) { oMessage = "data tidak ada"; return null; }

                    #region Profile
                    var tbProfile = from a in conn.GetList<TbUnit>()
                                    join b in conn.GetList<TbUnitKegiatan>() on a.IdUnit equals b.IdUnit
                                    join c in conn.GetList<TbUser>() on b.CreatedBy equals c.IdUser
                                    where a.IdUnit == (IdUnit == 0 ? a.IdUnit : IdUnit)
                                    && b.Status == StatusProsesKontenConstant.Published
                                    && b.Jenis == JenisDataKontenUnitConstant.Profil
                                    select new { a.NamaSingkat, b.IdUnitKegiatan, b.Judul, b.Keterangan, b.Url, b.CreatedDate, c.FirstName, c.MiddleName, c.LastName, b.FileImage };
                    ret.Profile = new List<WebProfileUnitModel>();
                    foreach (var item in tbProfile.OrderByDescending(x => x.IdUnitKegiatan))
                    {
                        ret.Profile.Add(new WebProfileUnitModel
                        {
                            Judul = item.Judul,
                            Keterangan = item.Keterangan,
                            Url = item.Url,
                            Unit = item.NamaSingkat.ToLower(),
                            FileImage = item.FileImage,
                            CreatedBy = userAppService.SetFullName(item.FirstName, item.MiddleName, item.LastName),
                            CreatedDate = dateTimeService.DateToLongString(_languageCode, item.CreatedDate)
                        });
                    }

                    #endregion

                    #region Kegiatan
                    var tbKegiatans = from a in conn.GetList<TbUnit>()
                                      join b in conn.GetList<TbUnitKegiatan>() on a.IdUnit equals b.IdUnit
                                      join c in conn.GetList<TbUser>() on b.CreatedBy equals c.IdUser
                                      where a.IdUnit == (IdUnit == 0 ? a.IdUnit : IdUnit)
                                      && b.Status == StatusProsesKontenConstant.Published
                                      && b.Jenis == JenisDataKontenUnitConstant.Default
                                      select new { a.NamaSingkat, b.IdUnitKegiatan, b.Judul, b.Keterangan, b.Url, b.CreatedDate, c.FirstName, c.MiddleName, c.LastName, b.FileImage };
                    ret.Kegiatan = new List<WebKegiatanModel>();
                    foreach (var item in tbKegiatans.OrderByDescending(x => x.IdUnitKegiatan))
                    {
                        string keteranganSingkat = StripHTML(item.Keterangan);
                        if (keteranganSingkat.Length >= 170)
                            keteranganSingkat = keteranganSingkat.Substring(0, 170) + "...";
                        string keteranganSingkatBener = StripHTML(item.Keterangan);
                        if (keteranganSingkatBener.Length >= 170)
                            keteranganSingkatBener = keteranganSingkatBener.Substring(0, 60) + "...";
                        ret.Kegiatan.Add(new WebKegiatanModel
                        {
                            Judul = item.Judul,
                            KeteranganSingkat = keteranganSingkat,
                            KeteranganSingkatBener = keteranganSingkatBener,
                            Keterangan = item.Keterangan,
                            Url = item.Url,
                            Unit = item.NamaSingkat.ToLower(),
                            FileImage = item.FileImage,
                            CreatedBy = userAppService.SetFullName(item.FirstName, item.MiddleName, item.LastName),
                            CreatedDate = item.CreatedDate.ToString("dd MMM"),
                            CreatedYear = item.CreatedDate.ToString("yyyy")
                        });
                    }
                    #endregion

                    #region Ekskul
                    var tbEkskuls = from a in conn.GetList<TbUnit>()
                                    join b in conn.GetList<TbUnitEkskul>() on a.IdUnit equals b.IdUnit
                                    join c in conn.GetList<TbUser>() on b.CreatedBy equals c.IdUser
                                    where a.IdUnit == (IdUnit == 0 ? a.IdUnit : IdUnit)
                                    && b.Status == StatusProsesKontenConstant.Published
                                    select new { a.NamaSingkat, b.IdUnitEkskul, b.Judul, b.Deskripsi, b.CreatedDate, c.FirstName, c.MiddleName, c.LastName, b.FileImage };
                    ret.Ekskul = new List<WebKegiatanModel>();
                    foreach (var item in tbEkskuls.OrderByDescending(x => x.IdUnitEkskul))
                    {
                        string keteranganSingkat = StripHTML(item.Deskripsi);
                        if (keteranganSingkat.Length >= 170)
                            keteranganSingkat = keteranganSingkat.Substring(0, 170) + "...";
                        string keteranganSingkatBener = StripHTML(item.Deskripsi);
                        if (keteranganSingkatBener.Length >= 170)
                            keteranganSingkatBener = keteranganSingkatBener.Substring(0, 60) + "...";
                        ret.Ekskul.Add(new WebKegiatanModel
                        {
                            Id = item.IdUnitEkskul,
                            Judul = item.Judul,
                            KeteranganSingkat = keteranganSingkat,
                            KeteranganSingkatBener = keteranganSingkatBener,
                            Keterangan = item.Deskripsi,
                            Unit = item.NamaSingkat.ToLower(),
                            FileImage = item.FileImage,
                            CreatedBy = userAppService.SetFullName(item.FirstName, item.MiddleName, item.LastName),
                            CreatedDate = item.CreatedDate.ToString("dd MMM"),
                            CreatedYear = item.CreatedDate.ToString("yyyy")
                        });
                    }
                    #endregion

                    #region Staff
                    var tbStaffs = from a in conn.GetList<TbUnitPegawai>()
                                   join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                   join c in conn.GetList<TbUser>() on b.IdPegawai equals c.IdUser
                                   join d in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals d.IdJenisJabatan
                                   where a.IdUnit == (IdUnit == 0 ? a.IdUnit : IdUnit) && c.Status == StatusDataConstant.Aktif
                                   //    && d.Level == LevelJabatanPegawaiConstant.StafUnit
                                   //    || d.Level == LevelJabatanPegawaiConstant.ManajemenUnit
                                   select new { c.FirstName, c.MiddleName, c.LastName, c.ProfileFile, NamaJabatan = d.Nama, b.Facebook, b.Twitter, b.Instagram, b.Youtube, b.Gelar };
                    ret.Staff = new List<WebPegawaiModel>();
                    foreach (var item in tbStaffs.OrderBy(x => x.FirstName))
                    {
                        ret.Staff.Add(new WebPegawaiModel
                        {
                            Nama = userAppService.SetFullName(item.FirstName, item.MiddleName, item.LastName),
                            Jabatan = item.NamaJabatan,
                            FileFoto = item.ProfileFile,
                            Gelar = item.Gelar,
                            Facebook = string.IsNullOrEmpty(item.Facebook) ? tbSekolah.Facebook : item.Facebook,
                            Twitter = string.IsNullOrEmpty(item.Twitter) ? tbSekolah.Twitter : item.Twitter,
                            Instagram = string.IsNullOrEmpty(item.Instagram) ? tbSekolah.Instagram : item.Instagram,
                            Youtube = string.IsNullOrEmpty(item.Youtube) ? tbSekolah.Youtube : item.Youtube,
                        });
                    }
                    #endregion

                    #region Gambar
                    var tbGambars = from a in conn.GetList<TbUnit>()
                                    join b in conn.GetList<TbUnitGaleri>() on a.IdUnit equals b.IdUnit
                                    where a.IdUnit == (IdUnit == 0 ? a.IdUnit : IdUnit) && b.IdJenisGaleri == JenisGaleriConstant.Image
                                    && b.Status == StatusProsesKontenConstant.Published
                                    select new { b.IdUnitGaleri, b.Judul, b.Keterangan, b.FileUrl, b.CreatedDate };
                    ret.GaleriImage = new List<WebGaleriImageModel>();
                    foreach (var item in tbGambars.OrderByDescending(x => x.IdUnitGaleri))
                    {
                        ret.GaleriImage.Add(new WebGaleriImageModel
                        {
                            IdGaleri = item.IdUnitGaleri,
                            Judul = item.Judul,
                            Konten = item.Keterangan,
                            FileGaleri = item.FileUrl,
                            CreatedDate = dateTimeService.DateToLongString(_languageCode, item.CreatedDate)
                        });
                    }
                    #endregion

                    #region Video
                    var tbVideos = from a in conn.GetList<TbUnit>()
                                   join b in conn.GetList<TbUnitGaleri>() on a.IdUnit equals b.IdUnit
                                   where a.IdUnit == (IdUnit == 0 ? a.IdUnit : IdUnit) && b.IdJenisGaleri == JenisGaleriConstant.Video
                                    && b.Status == StatusProsesKontenConstant.Published
                                   select new { b.IdUnitGaleri, b.Judul, b.Keterangan, b.FileUrl, b.CreatedDate };
                    ret.GaleriVideo = new List<WebGaleriVideoModel>();
                    foreach (var item in tbVideos.OrderByDescending(x => x.IdUnitGaleri))
                    {
                        ret.GaleriVideo.Add(new WebGaleriVideoModel
                        {
                            IdGaleri = item.IdUnitGaleri,
                            Judul = item.Judul,
                            Konten = item.Keterangan,
                            UrlGaleri = item.FileUrl,
                            CreatedDate = dateTimeService.DateToLongString(_languageCode, item.CreatedDate)
                        });
                    }
                    #endregion

                    #region TotSiswa
                    var totSiswa = (from a in conn.GetList<TbKelasRombel>()
                                    join b in conn.GetList<TbKelasParalel>() on a.IdKelasParalel equals b.IdKelasParalel
                                    join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                    select new { a, c }).ToList();

                    int totSiswaTkit = 0;
                    int totSiswaSdit = 0;
                    int totSiswaSmpit = 0;
                    int totSiswaSmait = 0;
                    foreach (var item in totSiswa)
                    {
                        if (item.c.IdUnit == UnitConstant.Tk) totSiswaTkit += 1;
                        if (item.c.IdUnit == UnitConstant.Sd) totSiswaSdit += 1;
                        if (item.c.IdUnit == UnitConstant.Smp) totSiswaSmpit += 1;
                        if (item.c.IdUnit == UnitConstant.Sma) totSiswaSmait += 1;
                    }
                    ret.Tkit.TotSiswa = totSiswaTkit;
                    ret.Sdit.TotSiswa = totSiswaSdit;
                    ret.Smpit.TotSiswa = totSiswaSmpit;
                    ret.Smait.TotSiswa = totSiswaSmait;
                    #endregion

                    #region MotoUnit
                    var dataUnits = conn.GetList<TbUnit>().ToList();
                    foreach (var item in dataUnits)
                    {
                        if (item.IdUnit == UnitConstant.Tk)
                        {
                            ret.Tkit.FileImageLogo = item.Logo;

                            ret.Tkit.Detail.NamaYayasan = item.NamaYayasan;
                            ret.Tkit.Detail.Kurikulum = item.Kurikulum;
                            ret.Tkit.Detail.Moto = item.Moto;
                            ret.Tkit.Detail.LuasBangunan = item.LuasBangunan;
                            ret.Tkit.Detail.LuasTanah = item.LuasTanah;
                            ret.Tkit.Detail.Nama = item.Nama;
                            ret.Tkit.Detail.NamaSingkat = item.NamaSingkat;
                            ret.Tkit.Detail.Naungan = item.Naungan;
                            ret.Tkit.Detail.NoFaximili = item.NoFaximili;
                            ret.Tkit.Detail.NoSkPendirian = item.NoSkPendirian;
                            ret.Tkit.Detail.Nspn = item.Nspn;
                            ret.Tkit.Detail.NoTelepon = item.NoTelepon;
                            ret.Tkit.Detail.Email = item.Email;
                            ret.Tkit.Detail.SliderJudul = item.SliderJudul;
                            ret.Tkit.Detail.SliderKeterangan = item.SliderKeterangan;
                            ret.Tkit.Detail.Profile = item.Profile;
                            ret.Tkit.Detail.SliderBanner = item.SliderBanner;
                            ret.Tkit.Detail.SliderImage = item.SliderImage;
                            ret.Tkit.Detail.Thumbnail = item.Thumbnail;
                            ret.Tkit.Detail.ThumbnailBesar = item.ThumbnailBesar;
                            ret.Tkit.Detail.VisiMisi = item.VisiMisi;
                            ret.Tkit.Detail.StrukturOrganisasi = item.StrukturOrganisasi;
                        }
                        if (item.IdUnit == UnitConstant.Sd)
                        {
                            ret.Sdit.FileImageLogo = item.Logo;

                            ret.Sdit.Detail.NamaYayasan = item.NamaYayasan;
                            ret.Sdit.Detail.Kurikulum = item.Kurikulum;
                            ret.Sdit.Detail.Moto = item.Moto;
                            ret.Sdit.Detail.LuasBangunan = item.LuasBangunan;
                            ret.Sdit.Detail.LuasTanah = item.LuasTanah;
                            ret.Sdit.Detail.Nama = item.Nama;
                            ret.Sdit.Detail.NamaSingkat = item.NamaSingkat;
                            ret.Sdit.Detail.Naungan = item.Naungan;
                            ret.Sdit.Detail.NoFaximili = item.NoFaximili;
                            ret.Sdit.Detail.NoSkPendirian = item.NoSkPendirian;
                            ret.Sdit.Detail.Nspn = item.Nspn;
                            ret.Sdit.Detail.NoTelepon = item.NoTelepon;
                            ret.Sdit.Detail.Email = item.Email;
                            ret.Sdit.Detail.SliderJudul = item.SliderJudul;
                            ret.Sdit.Detail.SliderKeterangan = item.SliderKeterangan;
                            ret.Sdit.Detail.Profile = item.Profile;
                            ret.Sdit.Detail.SliderBanner = item.SliderBanner;
                            ret.Sdit.Detail.SliderImage = item.SliderImage;
                            ret.Sdit.Detail.Thumbnail = item.Thumbnail;
                            ret.Sdit.Detail.ThumbnailBesar = item.ThumbnailBesar;
                            ret.Sdit.Detail.VisiMisi = item.VisiMisi;
                            ret.Sdit.Detail.StrukturOrganisasi = item.StrukturOrganisasi;
                        }
                        if (item.IdUnit == UnitConstant.Smp)
                        {
                            ret.Smpit.FileImageLogo = item.Logo;

                            ret.Smpit.Detail.NamaYayasan = item.NamaYayasan;
                            ret.Smpit.Detail.Kurikulum = item.Kurikulum;
                            ret.Smpit.Detail.Moto = item.Moto;
                            ret.Smpit.Detail.LuasBangunan = item.LuasBangunan;
                            ret.Smpit.Detail.LuasTanah = item.LuasTanah;
                            ret.Smpit.Detail.Nama = item.Nama;
                            ret.Smpit.Detail.NamaSingkat = item.NamaSingkat;
                            ret.Smpit.Detail.Naungan = item.Naungan;
                            ret.Smpit.Detail.NoFaximili = item.NoFaximili;
                            ret.Smpit.Detail.NoSkPendirian = item.NoSkPendirian;
                            ret.Smpit.Detail.Nspn = item.Nspn;
                            ret.Smpit.Detail.NoTelepon = item.NoTelepon;
                            ret.Smpit.Detail.Email = item.Email;
                            ret.Smpit.Detail.SliderJudul = item.SliderJudul;
                            ret.Smpit.Detail.SliderKeterangan = item.SliderKeterangan;
                            ret.Smpit.Detail.Profile = item.Profile;
                            ret.Smpit.Detail.SliderBanner = item.SliderBanner;
                            ret.Smpit.Detail.SliderImage = item.SliderImage;
                            ret.Smpit.Detail.Thumbnail = item.Thumbnail;
                            ret.Smpit.Detail.ThumbnailBesar = item.ThumbnailBesar;
                            ret.Smpit.Detail.VisiMisi = item.VisiMisi;
                            ret.Smpit.Detail.StrukturOrganisasi = item.StrukturOrganisasi;
                        }
                        if (item.IdUnit == UnitConstant.Sma)
                        {
                            ret.Smait.FileImageLogo = item.Logo;

                            ret.Smait.Detail.NamaYayasan = item.NamaYayasan;
                            ret.Smait.Detail.Kurikulum = item.Kurikulum;
                            ret.Smait.Detail.Moto = item.Moto;
                            ret.Smait.Detail.LuasBangunan = item.LuasBangunan;
                            ret.Smait.Detail.LuasTanah = item.LuasTanah;
                            ret.Smait.Detail.Nama = item.Nama;
                            ret.Smait.Detail.NamaSingkat = item.NamaSingkat;
                            ret.Smait.Detail.Naungan = item.Naungan;
                            ret.Smait.Detail.NoFaximili = item.NoFaximili;
                            ret.Smait.Detail.NoSkPendirian = item.NoSkPendirian;
                            ret.Smait.Detail.Nspn = item.Nspn;
                            ret.Smait.Detail.NoTelepon = item.NoTelepon;
                            ret.Smait.Detail.Email = item.Email;
                            ret.Smait.Detail.SliderJudul = item.SliderJudul;
                            ret.Smait.Detail.SliderKeterangan = item.SliderKeterangan;
                            ret.Smait.Detail.Profile = item.Profile;
                            ret.Smait.Detail.SliderBanner = item.SliderBanner;
                            ret.Smait.Detail.SliderImage = item.SliderImage;
                            ret.Smait.Detail.Thumbnail = item.Thumbnail;
                            ret.Smait.Detail.ThumbnailBesar = item.ThumbnailBesar;
                            ret.Smait.Detail.VisiMisi = item.VisiMisi;
                            ret.Smait.Detail.StrukturOrganisasi = item.StrukturOrganisasi;
                        }
                    }
                    #endregion

                    #region Kepsek
                    var kepseks = (from a in conn.GetList<TbPegawai>()
                                   join b in conn.GetList<TbUser>() on a.IdPegawai equals b.IdUser
                                   join c in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals c.IdPegawai
                                   select new { a, b, c }).ToList();


                    foreach (var item in kepseks)
                    {
                        if (item.c.IdUnit == UnitConstant.Tk)
                        {
                            if (item.a.IdJenisJabatan == JenisJabatanPegawaiContant.KaUnit)
                            {
                                ret.Tkit.Kepsek.Nama = userAppService.SetFullName(item.b.FirstName, item.b.MiddleName, item.b.LastName);
                                ret.Tkit.Kepsek.Jabatan = "Kepala Sekolah <br />" + UnitConstant.DictUnit[item.c.IdUnit];
                                ret.Tkit.Kepsek.FileImage = item.b.ProfileFile;
                                ret.Tkit.Kepsek.Sambutan = item.a.Sambutan;
                                ret.Tkit.Kepsek.Gelar = item.a.Gelar;
                                ret.Tkit.Kepsek.Facebook = string.IsNullOrEmpty(item.a.Facebook) ? tbSekolah.Facebook : item.a.Facebook;
                                ret.Tkit.Kepsek.Twitter = string.IsNullOrEmpty(item.a.Twitter) ? tbSekolah.Twitter : item.a.Twitter;
                                ret.Tkit.Kepsek.Instagram = string.IsNullOrEmpty(item.a.Instagram) ? tbSekolah.Instagram : item.a.Instagram;
                                ret.Tkit.Kepsek.Youtube = string.IsNullOrEmpty(item.a.Youtube) ? tbSekolah.Youtube : item.a.Youtube;
                            }
                        }
                        if (item.c.IdUnit == UnitConstant.Sd)
                        {
                            if (item.a.IdJenisJabatan == JenisJabatanPegawaiContant.KaUnit)
                            {
                                ret.Sdit.Kepsek.Nama = userAppService.SetFullName(item.b.FirstName, item.b.MiddleName, item.b.LastName);
                                ret.Sdit.Kepsek.Jabatan = "Kepala Sekolah <br />" + UnitConstant.DictUnit[item.c.IdUnit];
                                ret.Sdit.Kepsek.FileImage = item.b.ProfileFile;
                                ret.Sdit.Kepsek.Sambutan = item.a.Sambutan;
                                ret.Sdit.Kepsek.Gelar = item.a.Gelar;
                                ret.Sdit.Kepsek.Facebook = string.IsNullOrEmpty(item.a.Facebook) ? tbSekolah.Facebook : item.a.Facebook;
                                ret.Sdit.Kepsek.Twitter = string.IsNullOrEmpty(item.a.Twitter) ? tbSekolah.Twitter : item.a.Twitter;
                                ret.Sdit.Kepsek.Instagram = string.IsNullOrEmpty(item.a.Instagram) ? tbSekolah.Instagram : item.a.Instagram;
                                ret.Sdit.Kepsek.Youtube = string.IsNullOrEmpty(item.a.Youtube) ? tbSekolah.Youtube : item.a.Youtube;
                            }
                        }
                        if (item.c.IdUnit == UnitConstant.Smp)
                        {
                            if (item.a.IdJenisJabatan == JenisJabatanPegawaiContant.KaUnit)
                            {
                                ret.Smpit.Kepsek.Nama = userAppService.SetFullName(item.b.FirstName, item.b.MiddleName, item.b.LastName);
                                ret.Smpit.Kepsek.Jabatan = "Kepala Sekolah <br />" + UnitConstant.DictUnit[item.c.IdUnit];
                                ret.Smpit.Kepsek.FileImage = item.b.ProfileFile;
                                ret.Smpit.Kepsek.Sambutan = item.a.Sambutan;
                                ret.Smpit.Kepsek.Gelar = item.a.Gelar;
                                ret.Smpit.Kepsek.Facebook = string.IsNullOrEmpty(item.a.Facebook) ? tbSekolah.Facebook : item.a.Facebook;
                                ret.Smpit.Kepsek.Twitter = string.IsNullOrEmpty(item.a.Twitter) ? tbSekolah.Twitter : item.a.Twitter;
                                ret.Smpit.Kepsek.Instagram = string.IsNullOrEmpty(item.a.Instagram) ? tbSekolah.Instagram : item.a.Instagram;
                                ret.Smpit.Kepsek.Youtube = string.IsNullOrEmpty(item.a.Youtube) ? tbSekolah.Youtube : item.a.Youtube;
                            }
                        }
                        if (item.c.IdUnit == UnitConstant.Sma)
                        {
                            if (item.a.IdJenisJabatan == JenisJabatanPegawaiContant.KaUnit)
                            {
                                ret.Smait.Kepsek.Nama = userAppService.SetFullName(item.b.FirstName, item.b.MiddleName, item.b.LastName);
                                ret.Smait.Kepsek.Jabatan = "Kepala Sekolah <br />" + UnitConstant.DictUnit[item.c.IdUnit];
                                ret.Smait.Kepsek.FileImage = item.b.ProfileFile;
                                ret.Smait.Kepsek.Sambutan = item.a.Sambutan;
                                ret.Smait.Kepsek.Gelar = item.a.Gelar;
                                ret.Smait.Kepsek.Facebook = string.IsNullOrEmpty(item.a.Facebook) ? tbSekolah.Facebook : item.a.Facebook;
                                ret.Smait.Kepsek.Twitter = string.IsNullOrEmpty(item.a.Twitter) ? tbSekolah.Twitter : item.a.Twitter;
                                ret.Smait.Kepsek.Instagram = string.IsNullOrEmpty(item.a.Instagram) ? tbSekolah.Instagram : item.a.Instagram;
                                ret.Smait.Kepsek.Youtube = string.IsNullOrEmpty(item.a.Youtube) ? tbSekolah.Youtube : item.a.Youtube;
                            }
                        }


                    }
                    #endregion

                    #region TotAlumni
                    var alumnis = conn.GetList<TaSiswa>("where kelas in ('VI', 'IX', 'XII')").Count();
                    ret.TotAlumni = alumnis;
                    #endregion

                    #region TotAlumni
                    var siswaPrestasis = conn.GetList<TbSiswaPrestasi>().GroupBy(x => x.IdSiswa).Count();
                    ret.TotSiswaPrestasi = siswaPrestasis;
                    #endregion
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPropen", ex);
                return null;
            }
        }

        public List<WebKegiatanModel> GetAllUnitKegiatans(string Search, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var datas = (from a in conn.GetList<TbUnit>()
                                 join b in conn.GetList<TbUnitKegiatan>() on a.IdUnit equals b.IdUnit
                                 join c in conn.GetList<TbUser>() on b.CreatedBy equals c.IdUser
                                 where b.Status == StatusProsesKontenConstant.Published
                                 && b.Jenis == JenisDataKontenUnitConstant.Default
                                 select new { a.NamaSingkat, b.IdUnitKegiatan, b.Judul, b.Keterangan, b.Url, b.CreatedDate, c.FirstName, c.MiddleName, c.LastName, b.FileImage }).Where(x => x.Judul.Contains(Search) || x.Keterangan.Contains(Search) || x.FirstName.Contains(Search)).ToList();
                    if (datas.Count == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    // datas = datas.Where(x => x.Judul.Contains(Search) || x.Keterangan.Contains(Search) || x.FirstName.Contains(Search)).ToList();
                    // if (datas.Count == 0)
                    // {
                    //     oMessage = "data tidak ada";
                    //     return null;
                    // }
                    List<WebKegiatanModel> ret = new List<WebKegiatanModel>();
                    foreach (var item in datas)
                    {
                        ret.Add(new WebKegiatanModel
                        {
                            Judul = item.Judul,
                            Keterangan = item.Keterangan,
                            Url = item.Url,
                            FileImage = item.FileImage,
                            CreatedBy = userAppService.SetFullName(item.FirstName, item.MiddleName, item.LastName),
                            CreatedDate = dateTimeService.DateToLongString(_languageCode, item.CreatedDate)
                        });
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetAllUnitKegiatans", ex);
                return null;
            }
        }

        public WebPpdbListInfoModel GetPpdbInfo(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                WebPpdbListInfoModel ret = new WebPpdbListInfoModel();
                using (var conn = commonService.DbConnection())
                {
                    var tbSekolah = (from a in conn.GetList<TbSekolah>()
                                     select a).FirstOrDefault();
                    if (tbSekolah == null) { oMessage = "data tidak ada"; return null; }

                    #region Aturan
                    var tbAturans = from a in conn.GetList<TbPpdbInfo>()
                                    join b in conn.GetList<TbPpdb>() on a.IdPpdb equals b.IdPpdb
                                    where a.IdJenisPpdbInfo == JenisPpdbInfoConstant.Aturan
                                    select new { a.IdPpdbInfo, a.Judul, a.Konten, a.FileImage };
                    ret.Aturan = new List<WebKontenModel>();
                    foreach (var items in tbAturans.OrderByDescending(x => x.IdPpdbInfo))
                    {
                        ret.Aturan.Add(new WebKontenModel
                        {
                            IdKonten = items.IdPpdbInfo,
                            Judul = items.Judul,
                            Konten = items.Konten,
                            FileImage = items.FileImage,
                        });
                    }

                    #endregion

                    #region Alur
                    var tbAlurs = from a in conn.GetList<TbPpdbInfo>()
                                  join b in conn.GetList<TbPpdb>() on a.IdPpdb equals b.IdPpdb
                                  where a.IdJenisPpdbInfo == JenisPpdbInfoConstant.Alur
                                  select new { a.IdPpdbInfo, a.Judul, a.Konten, a.FileImage };
                    ret.Alur = new List<WebKontenModel>();
                    foreach (var items in tbAlurs.OrderByDescending(x => x.IdPpdbInfo))
                    {
                        ret.Alur.Add(new WebKontenModel
                        {
                            IdKonten = items.IdPpdbInfo,
                            Judul = items.Judul,
                            Konten = items.Konten,
                            FileImage = items.FileImage,
                        });
                    }
                    #endregion

                    #region Biaya
                    var tbBiayas = from a in conn.GetList<TbPpdbInfo>()
                                   join b in conn.GetList<TbPpdb>() on a.IdPpdb equals b.IdPpdb
                                   where a.IdJenisPpdbInfo == JenisPpdbInfoConstant.Biaya
                                   select new { a.IdPpdbInfo, a.Judul, a.Konten, a.FileImage };
                    ret.Biaya = new List<WebKontenModel>();
                    foreach (var items in tbBiayas.OrderByDescending(x => x.IdPpdbInfo))
                    {
                        ret.Biaya.Add(new WebKontenModel
                        {
                            IdKonten = items.IdPpdbInfo,
                            Judul = items.Judul,
                            Konten = items.Konten,
                            FileImage = items.FileImage,
                        });
                    }
                    #endregion

                    #region Pengumuman
                    var tbPengumumans = from a in conn.GetList<TbPpdbInfo>()
                                        join b in conn.GetList<TbPpdb>() on a.IdPpdb equals b.IdPpdb
                                        where a.IdJenisPpdbInfo == JenisPpdbInfoConstant.Pengumuman
                                        select new { a.IdPpdbInfo, a.Judul, a.Konten, a.FileImage };
                    ret.Pengumuman = new List<WebKontenModel>();
                    foreach (var items in tbPengumumans.OrderByDescending(x => x.IdPpdbInfo))
                    {
                        ret.Pengumuman.Add(new WebKontenModel
                        {
                            IdKonten = items.IdPpdbInfo,
                            Judul = items.Judul,
                            Konten = items.Konten,
                            FileImage = items.FileImage,
                        });
                    }
                    #endregion
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbInfo", ex);
                return null;
            }
        }

        public WebPropenModel GetPropenDkm(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                WebPropenModel ret = new WebPropenModel();
                using (var conn = commonService.DbConnection())
                {
                    var tbSekolah = (from a in conn.GetList<TbSekolah>()
                                     select a).FirstOrDefault();
                    if (tbSekolah == null) { oMessage = "data tidak ada"; return null; }

                    #region Profile
                    var tbProfile = from a in conn.GetList<TbUnit>()
                                    join b in conn.GetList<TbUnitKegiatan>() on a.IdUnit equals b.IdUnit
                                    join c in conn.GetList<TbUser>() on b.CreatedBy equals c.IdUser
                                    where a.IdUnit == UnitConstant.DKM
                                    && b.Status == StatusProsesKontenConstant.Published
                                    && b.Jenis == JenisDataKontenUnitConstant.Profil
                                    select new { b.IdUnitKegiatan, b.Judul, b.Keterangan, b.Url, b.CreatedDate, c.FirstName, c.MiddleName, c.LastName, b.FileImage };
                    ret.Profile = new List<WebProfileUnitModel>();
                    foreach (var item in tbProfile.OrderByDescending(x => x.IdUnitKegiatan))
                    {
                        ret.Profile.Add(new WebProfileUnitModel
                        {
                            Judul = item.Judul,
                            Keterangan = item.Keterangan,
                            Url = item.Url,
                            FileImage = item.FileImage,
                            CreatedBy = userAppService.SetFullName(item.FirstName, item.MiddleName, item.LastName),
                            CreatedDate = dateTimeService.DateToLongString(_languageCode, item.CreatedDate)
                        });
                    }
                    #endregion

                    #region Kegiatan
                    var tbKegiatans = from a in conn.GetList<TbUnit>()
                                      join b in conn.GetList<TbUnitKegiatan>() on a.IdUnit equals b.IdUnit
                                      join c in conn.GetList<TbUser>() on b.CreatedBy equals c.IdUser
                                      where a.IdUnit == UnitConstant.DKM
                                      && b.Status == StatusProsesKontenConstant.Published
                                      && b.Jenis == JenisDataKontenUnitConstant.Profil
                                      select new { b.IdUnitKegiatan, b.Judul, b.Keterangan, b.Url, b.CreatedDate, c.FirstName, c.MiddleName, c.LastName, b.FileImage };
                    ret.Kegiatan = new List<WebKegiatanModel>();
                    foreach (var item in tbKegiatans.OrderByDescending(x => x.IdUnitKegiatan))
                    {
                        ret.Kegiatan.Add(new WebKegiatanModel
                        {
                            Judul = item.Judul,
                            Keterangan = item.Keterangan,
                            Url = item.Url,
                            FileImage = item.FileImage,
                            CreatedBy = userAppService.SetFullName(item.FirstName, item.MiddleName, item.LastName),
                            CreatedDate = dateTimeService.DateToLongString(_languageCode, item.CreatedDate)
                        });
                    }
                    #endregion

                    #region Staff
                    var tbStaffs = from a in conn.GetList<TbUnitPegawai>()
                                   join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                   join c in conn.GetList<TbUser>() on b.IdPegawai equals c.IdUser
                                   join d in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals d.IdJenisJabatan
                                   where a.IdUnit == UnitConstant.Sd
                                   & d.Level == LevelJabatanPegawaiConstant.StafUnit
                                   select new { c.FirstName, c.MiddleName, c.LastName, NamaJabatan = d.Nama, c.ProfileFile };
                    ret.Staff = new List<WebPegawaiModel>();
                    foreach (var item in tbStaffs)
                    {
                        ret.Staff.Add(new WebPegawaiModel
                        {
                            Nama = userAppService.SetFullName(item.FirstName, item.MiddleName, item.LastName),
                            Jabatan = item.NamaJabatan,
                            FileFoto = item.ProfileFile,
                        });
                    }
                    #endregion

                    #region Gambar
                    var tbGambars = from a in conn.GetList<TbUnit>()
                                    join b in conn.GetList<TbUnitGaleri>() on a.IdUnit equals b.IdUnit
                                    where a.IdUnit == UnitConstant.Sd && b.IdJenisGaleri == JenisGaleriConstant.Image
                                    select new { b.IdUnitGaleri, b.Judul, b.Keterangan, b.FileUrl, b.CreatedDate };
                    ret.GaleriImage = new List<WebGaleriImageModel>();
                    foreach (var item in tbGambars.OrderByDescending(x => x.IdUnitGaleri))
                    {
                        ret.GaleriImage.Add(new WebGaleriImageModel
                        {
                            IdGaleri = item.IdUnitGaleri,
                            Judul = item.Judul,
                            Konten = item.Keterangan,
                            FileGaleri = item.FileUrl,
                            CreatedDate = dateTimeService.DateToLongString(_languageCode, item.CreatedDate)
                        });
                    }
                    #endregion

                    #region Video
                    var tbVideos = from a in conn.GetList<TbUnit>()
                                   join b in conn.GetList<TbUnitGaleri>() on a.IdUnit equals b.IdUnit
                                   where a.IdUnit == UnitConstant.Sd && b.IdJenisGaleri == JenisGaleriConstant.Video
                                   select new { b.IdUnitGaleri, b.Judul, b.Keterangan, b.FileUrl, b.CreatedDate };
                    ret.GaleriVideo = new List<WebGaleriVideoModel>();
                    foreach (var item in tbVideos.OrderByDescending(x => x.IdUnitGaleri))
                    {
                        ret.GaleriVideo.Add(new WebGaleriVideoModel
                        {
                            IdGaleri = item.IdUnitGaleri,
                            Judul = item.Judul,
                            Konten = item.Keterangan,
                            UrlGaleri = item.FileUrl,
                            CreatedDate = dateTimeService.DateToLongString(_languageCode, item.CreatedDate)
                        });
                    }
                    #endregion

                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPropenDkm", ex);
                return null;
            }
        }

        public WebKegiatanByUrlModel GetKegiatanByUrl(string Url, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                WebKegiatanByUrlModel ret = new WebKegiatanByUrlModel();
                using (var conn = commonService.DbConnection())
                {
                    #region KegiatanByUrl
                    var tbKegiatanByUrls = (from a in conn.GetList<TbUnit>()
                                            join b in conn.GetList<TbUnitKegiatan>() on a.IdUnit equals b.IdUnit
                                            join c in conn.GetList<TbUser>() on b.CreatedBy equals c.IdUser
                                            where b.Url == String.Format(Url) && b.Status == StatusProsesKontenConstant.Published
                                            select new { b.Judul, b.Keterangan, b.Url, b.CreatedDate, c.FirstName, c.MiddleName, c.LastName, b.FileImage }).FirstOrDefault();
                    if (tbKegiatanByUrls != null)
                    {
                        ret.KegiatanByUrl = new WebKegiatanModel
                        {
                            Judul = tbKegiatanByUrls.Judul,
                            Keterangan = tbKegiatanByUrls.Keterangan,
                            Url = tbKegiatanByUrls.Url,
                            FileImage = tbKegiatanByUrls.FileImage,
                            CreatedBy = userAppService.SetFullName(tbKegiatanByUrls.FirstName, tbKegiatanByUrls.MiddleName, tbKegiatanByUrls.LastName),
                            CreatedDate = dateTimeService.DateToLongString(_languageCode, tbKegiatanByUrls.CreatedDate)
                        };
                    }
                    else
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }

                    #endregion
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKegiatanByUrl", ex);
                return null;
            }
        }

        public List<SiswaRaporModel> GetSiswaRapors(string Nis, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbSiswa>()
                               join b in conn.GetList<TbSiswaRapor>() on a.IdSiswa equals b.IdSiswa
                               join c in conn.GetList<TbJenisRapor>() on b.IdJenisRapor equals c.IdJenisRapor
                               where a.Nis == Nis
                               select new { a, b, c };
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    List<SiswaRaporModel> ret = new List<SiswaRaporModel>();
                    foreach (var item in data)
                    {
                        SiswaRaporModel m = new SiswaRaporModel
                        {
                            IdSiswa = item.a.IdSiswa,
                            NamaSiswa = item.a.Nama,
                            IdJenisRapor = item.c.IdJenisRapor,
                            FileRapor = item.b.FileRapor,
                            JenisRapor = item.c.Nama
                        };
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswaRapors", ex);
                return null;

            }
        }

        public SiswaRaporModel GetSiswaRapor(int IdSiswa, int IdJenisRapor, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = (from a in conn.GetList<TbSiswaRapor>()
                                join b in conn.GetList<TbJenisRapor>() on a.IdJenisRapor equals b.IdJenisRapor
                                where a.IdSiswa == IdSiswa & a.IdJenisRapor == IdJenisRapor
                                select new { a, b }).FirstOrDefault();
                    if (data == null) { oMessage = "data tidak ada"; return null; }
                    SiswaRaporModel ret = new SiswaRaporModel
                    {
                        IdSiswa = data.a.IdSiswa,
                        IdJenisRapor = data.a.IdJenisRapor,
                        FileRapor = data.a.FileRapor,
                        JenisRapor = data.b.Nama
                    };
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSiswaRapor", ex);
                return null;

            }
        }


        public WebSekolahModel GetProfSekolah(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {

                    var DataSekolah = (from a in conn.GetList<TbSekolah>()
                                       select a).FirstOrDefault();
                    if (DataSekolah == null) { oMessage = "data tidak ada"; return null; }
                    WebSekolahModel ret = new WebSekolahModel();
                    ret.NamaSekolah = DataSekolah.Nama;
                    ret.NamaSekolahSingkat = DataSekolah.NamaSingkat;
                    ret.KodePos = DataSekolah.KodePos;
                    ret.NoTelp = DataSekolah.NoTelepon;
                    ret.NoFax = DataSekolah.NoFaximili;
                    ret.EmailSekolah = DataSekolah.Email;
                    ret.Alamat = DataSekolah.Alamat;
                    ret.Facebook = DataSekolah.Facebook;
                    ret.Twitter = DataSekolah.Twitter;
                    ret.Instagram = DataSekolah.Instagram;
                    ret.Youtube = DataSekolah.Youtube;

                    // galeri sekolah
                    var DataGaleris = (from a in conn.GetList<TbSekolahGaleri>()
                                       join b in conn.GetList<TbSekolah>() on a.IdSekolah equals b.IdSekolah
                                       select new { a }).ToList();
                    // if (DataGaleris == null || DataGaleris.Count == 0)
                    // {
                    //     oMessage = "Data Galeri Tidak Ada";
                    //     return null;
                    // }
                    ret.GaleriSekolah = new List<WebSekolahGaleriModel>();
                    foreach (var DataGaleri in DataGaleris)
                    {
                        WebSekolahGaleriModel modGalari = new WebSekolahGaleriModel();
                        modGalari.Judul = DataGaleri.a.Judul;
                        modGalari.Keterangan = DataGaleri.a.Keterangan;
                        modGalari.IdJenisGaleri = JenisGaleriConstant.DictJenisGaleri[DataGaleri.a.IdJenisGaleri];

                        ret.GaleriSekolah.Add(modGalari);

                    }

                    var tbSekolahVisiMisis = from a in conn.GetList<TbSekolahVisiMisi>()
                                             where a.IdSekolah == DataSekolah.IdSekolah
                                             select a;
                    ret.VisiMisi = new List<WebVisiMisiModel>();
                    foreach (var item in tbSekolahVisiMisis.OrderBy(x => x.IdJenisVisiMisi))
                    {
                        WebVisiMisiModel mod = new WebVisiMisiModel();
                        mod.Ikon = item.FileIkon;
                        mod.Judul = item.Judul;
                        mod.Keterangan = item.Konten;
                        ret.VisiMisi.Add(mod);
                    }

                    return ret;
                }
            }
            catch (Exception ex)
            {

                oMessage = commonService.GetErrorMessage(ServiceName + "GetProfSekolah", ex);
                return null;
            }
        }

        public WebSekolahVisiMisi GetProfileSekolah(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    WebSekolahVisiMisi ret = new WebSekolahVisiMisi();

                    var DataSekolah = (from a in conn.GetList<TbSekolah>()
                                       select a).FirstOrDefault();


                    if (DataSekolah == null) { oMessage = "data tidak ada"; return null; }

                    ret.NamaSekolah = DataSekolah.Nama;
                    ret.NamaSekolahSingkat = DataSekolah.NamaSingkat;
                    ret.Alamat = DataSekolah.Alamat;
                    ret.EmailSekolah = DataSekolah.Email;
                    ret.NoTelp = DataSekolah.NoTelepon;
                    ret.Facebook = DataSekolah.Facebook;
                    ret.Twitter = DataSekolah.Twitter;
                    ret.Instagram = DataSekolah.Instagram;
                    ret.Youtube = DataSekolah.Youtube;
                    ret.Profile = DataSekolah.Profile;
                    ret.Video = DataSekolah.Video;

                    var VisiMisiKurikulums = from a in conn.GetList<TbSekolahVisiMisi>()
                                             where a.IdSekolah == DataSekolah.IdSekolah
                                             select a;
                    foreach (var item in VisiMisiKurikulums)
                    {
                        if (item.IdJenisVisiMisi == JenisVisiMisiConstant.Visi)
                        {
                            ret.StrVisi = new WebVisiMisiModel();
                            ret.StrVisi.Ikon = item.FileIkon;
                            ret.StrVisi.Judul = item.Judul;
                            ret.StrVisi.Keterangan = item.Konten;
                        }
                        if (item.IdJenisVisiMisi == JenisVisiMisiConstant.Misi)
                        {
                            ret.StrMisi = new WebVisiMisiModel();
                            ret.StrMisi.Ikon = item.FileIkon;
                            ret.StrMisi.Judul = item.Judul;
                            ret.StrMisi.Keterangan = item.Konten;
                        }
                        if (item.IdJenisVisiMisi == JenisVisiMisiConstant.Kurikulum)
                        {
                            ret.StrKurikulum = new WebVisiMisiModel();
                            ret.StrKurikulum.Ikon = item.FileIkon;
                            ret.StrKurikulum.Judul = item.Judul;
                            ret.StrKurikulum.Keterangan = item.Konten;
                        }
                    }

                    var Visis = from a in conn.GetList<TbSekolahVisiMisi>()
                                where a.IdSekolah == DataSekolah.IdSekolah && a.IdJenisVisiMisi == JenisVisiMisiConstant.Visi
                                select a;
                    if (Visis == null) { oMessage = "data tidak ada"; return null; }

                    ret.Visi = new List<WebVisiMisiModel>();
                    foreach (var Visi in Visis)
                    {
                        WebVisiMisiModel mod = new WebVisiMisiModel();
                        mod.Ikon = Visi.FileIkon;
                        mod.Judul = Visi.Judul;
                        mod.Keterangan = Visi.Konten;
                        ret.Visi.Add(mod);
                    }

                    var Misis = from a in conn.GetList<TbSekolahVisiMisi>()
                                where a.IdSekolah == DataSekolah.IdSekolah && a.IdJenisVisiMisi == JenisVisiMisiConstant.Misi
                                select a;
                    ret.Misi = new List<WebVisiMisiModel>();
                    foreach (var Misi in Misis)
                    {
                        WebVisiMisiModel mod = new WebVisiMisiModel();
                        mod.Ikon = Misi.FileIkon;
                        mod.Judul = Misi.Judul;
                        mod.Keterangan = Misi.Konten;
                        ret.Misi.Add(mod);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetProfileSekolah", ex);
                return null;
            }
        }

        public List<WebPegawaiModel> GetJabatanPegawai(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    List<WebPegawaiModel> ret = new List<WebPegawaiModel>();
                    var JabatanPegawais = (from a in conn.GetList<TbPegawai>()
                                           join b in conn.GetList<TbUser>() on a.IdPegawai equals b.IdUser
                                           join c in conn.GetList<TbJenisJabatan>() on a.IdJenisJabatan equals c.IdJenisJabatan
                                           where a.IdJenisJabatan == JenisJabatanPegawaiContant.Yay
                                           select new { a, b, c }
                                          ).ToList();
                    if (JabatanPegawais == null || JabatanPegawais.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    foreach (var JabatanPegawai in JabatanPegawais)
                    {
                        WebPegawaiModel mod = new WebPegawaiModel();

                        mod.Nama = userAppService.SetFullName(JabatanPegawai.b.FirstName, JabatanPegawai.b.MiddleName, JabatanPegawai.b.LastName);
                        mod.FileFoto = JabatanPegawai.b.ProfileFile;
                        mod.Jabatan = JabatanPegawai.c.Nama;

                        ret.Add(mod);
                    }

                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJabatanPegawai", ex);
                return null;
            }
        }

        public List<WebGaleriVideoModel> GetGaleriSekolah(int JenisGaleri, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    List<WebGaleriVideoModel> ret = new List<WebGaleriVideoModel>();
                    var Galeris = (from a in conn.GetList<TbSekolahGaleri>()
                                   where a.IdJenisGaleri == JenisGaleri
                                   select a).ToList();
                    if (Galeris == null || Galeris.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    foreach (var Galeri in Galeris)
                    {
                        WebGaleriVideoModel mod = new WebGaleriVideoModel();
                        mod.IdGaleri = Galeri.IdSekolahGaleri;
                        mod.Judul = Galeri.Judul;
                        mod.Konten = Galeri.Keterangan;
                        mod.UrlGaleri = Galeri.FileUrl;
                        mod.CreatedDate = dateTimeService.DateToLongString(_languageCode, Galeri.CreatedDate);
                        ret.Add(mod);
                    }

                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetGaleriSekolah", ex);
                return null;
            }
        }

        public WebSekolahModel GetKegiatanSekolah(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    WebSekolahModel ret = new WebSekolahModel();
                    ret.KegiatanSekolah = new List<WebSekolahKegiatanModel>();

                    var DataSekolah = (from a in conn.GetList<TbSekolah>()
                                       select a).FirstOrDefault();

                    var Kegiatans = (from a in conn.GetList<TbSekolah>()
                                     join b in conn.GetList<TbSekolahKegiatan>() on a.IdSekolah equals b.IdSekolah
                                     join c in conn.GetList<TbUser>() on b.CreatedBy equals c.IdUser
                                     select new { a, b, c }).ToList();

                    if (Kegiatans == null || Kegiatans.Count == 0) { oMessage = "data tidak ada"; return null; }

                    ret.NamaSekolah = DataSekolah.Nama;
                    ret.NamaSekolahSingkat = DataSekolah.NamaSingkat;
                    ret.Alamat = DataSekolah.Alamat;
                    ret.EmailSekolah = DataSekolah.Email;
                    ret.NoTelp = DataSekolah.NoTelepon;
                    foreach (var Kegiatan in Kegiatans)
                    {
                        WebSekolahKegiatanModel mod = new WebSekolahKegiatanModel();
                        mod.Judul = Kegiatan.b.Judul;
                        mod.Keterangan = Kegiatan.b.Keterangan;
                        mod.Url = Kegiatan.b.Url;
                        mod.CreatedBy = userAppService.SetFullName(Kegiatan.c.FirstName, Kegiatan.c.MiddleName, Kegiatan.c.LastName);
                        mod.CreatedDate = dateTimeService.DateToLongString(_languageCode, Kegiatan.b.CreatedDate);
                        ret.KegiatanSekolah.Add(mod);
                    }

                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKegiatanSekolah", ex);
                return null;
            }
        }
        public ApplSettingModel GetApplSetting(string code, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    ApplSettingModel ret = new ApplSettingModel();
                    var data = conn.Get<TbApplSetting>(code);
                    if (data == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    ret.Code = data.Code;
                    ret.Name = data.Name;
                    ret.ValueType = data.ValueType;
                    ret.ValueDate = data.ValueDate.ToString();
                    ret.ValueNumber = data.ValueNumber ?? 0;
                    ret.ValueString = data.ValueString;

                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetApplSetting", ex);
                return null;
            }
        }

        public List<WebGaleriImageModel> GetGaleriUnit(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbUnitGaleri>().ToList();
                    List<WebGaleriImageModel> ret = new List<WebGaleriImageModel>();
                    foreach (var item in data)
                    {
                        if (item.IdJenisGaleri == JenisGaleriConstant.Image)
                        {
                            ret.Add(new WebGaleriImageModel
                            {
                                IdGaleri = item.IdUnitGaleri,
                                Judul = item.Judul,
                                Konten = item.Keterangan,
                                FileGaleri = item.FileUrl,
                                CreatedDate = item.CreatedDate.ToString("dd MMM yyyy")
                            });
                        }
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetGaleriUnit", ex);
                return null;
            }
        }

        public WebUnitKepsekModel GetPegawaiByUsername(string Username, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                WebUnitKepsekModel ret = new WebUnitKepsekModel();
                using (var conn = commonService.DbConnection())
                {
                    var tbSekolah = (from a in conn.GetList<TbSekolah>()
                                     select a).FirstOrDefault();
                    if (tbSekolah == null) { oMessage = "data tidak ada"; return null; }
                    var user = (from a in conn.GetList<TbPegawai>()
                                join b in conn.GetList<TbUser>() on a.IdPegawai equals b.IdUser
                                join c in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals c.IdPegawai
                                where b.Username == Username
                                select new { a, b, c }).FirstOrDefault();
                    if (user == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    ret.Nama = userAppService.SetFullName(user.b.FirstName, user.b.MiddleName, user.b.LastName);
                    // ret.Jabatan = "Kepala Sekolah " + UnitConstant.DictUnit[user.c.IdUnit];
                    ret.FileImage = user.b.ProfileFile;
                    ret.Sambutan = user.a.Sambutan;
                    ret.Facebook = string.IsNullOrEmpty(user.a.Facebook) ? tbSekolah.Facebook : user.a.Facebook;
                    ret.Twitter = string.IsNullOrEmpty(user.a.Twitter) ? tbSekolah.Twitter : user.a.Twitter;
                    ret.Instagram = string.IsNullOrEmpty(user.a.Instagram) ? tbSekolah.Instagram : user.a.Instagram;
                    ret.Youtube = string.IsNullOrEmpty(user.a.Youtube) ? tbSekolah.Youtube : user.a.Youtube;
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPegawaiByUsername", ex);
                return null;
            }
        }
        public List<KalenderPendidikanModel> GetKalenderPendidikan(int Semester, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                List<KalenderPendidikanModel> ret = new List<KalenderPendidikanModel>();
                using (var conn = commonService.DbConnection())
                {
                    var tbKalenderPendidikan = conn.GetList<TbKalenderPendidikan>().Where(x => x.Semester == Semester).ToList();
                    foreach (var item in tbKalenderPendidikan)
                    {
                        ret.Add(new KalenderPendidikanModel
                        {
                            IdKalenderPendidikan = item.IdKalenderPendidikan,
                            Keterangan = item.Keterangan,
                            Semester = item.Semester,
                            FileImage = item.FileImage,
                        });
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKalenderPendidikan", ex);
                return null;
            }
        }

        public List<SekolahPrestasiModel> GetSekolahPrestasi(int IdJenisGaleri, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    var tbSekolahPrestasis = conn.GetList<TbSekolahPrestasi>().Where(x => x.IdJenisGaleri == IdJenisGaleri).ToList();

                    List<SekolahPrestasiModel> ret = new List<SekolahPrestasiModel>();
                    foreach (var item in tbSekolahPrestasis.OrderByDescending(x => x.IdSekolahPrestasi))
                    {
                        ret.Add(new SekolahPrestasiModel
                        {
                            IdSekolahPrestasi = item.IdSekolahPrestasi,
                            IdJenisGaleri = item.IdJenisGaleri,
                            Judul = item.Judul,
                            Keterangan = item.Keterangan,
                            FileUrl = item.FileUrl
                        });
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSekolahPrestasi", ex);
                return null;
            }
        }

        public List<SekolahFasilitasModel> GetSekolahFasilitas(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    var tbSekolahFasilitas = conn.GetList<TbSekolahFasilitas>().ToList();

                    List<SekolahFasilitasModel> ret = new List<SekolahFasilitasModel>();
                    foreach (var item in tbSekolahFasilitas.OrderByDescending(x => x.IdSekolahFasilitas))
                    {
                        ret.Add(new SekolahFasilitasModel
                        {
                            IdSekolahFasilitas = item.IdSekolahFasilitas,
                            Judul = item.Judul,
                            Deskripsi = item.Deskripsi,
                            FileImage = item.FileImage
                        });
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetSekolahFasilitas", ex);
                return null;
            }
        }
        public string AddTestimoni(WebTestimoniAddModel Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    TbTestimoni tbTestimoni = new TbTestimoni
                    {
                        Nama = Data.Nama,
                        Sebagai = Data.Sebagai,
                        Pesan = Data.Pesan,
                        Url = Data.Url,
                        Status = StatusDataConstant.TidakAktif,
                        CreatedDate = dateTimeService.GetCurrdate()
                    };
                    if (Data.FotoUser != null)
                    {
                        var pathUpload = AppSetting.PathTestimoni;

                        tbTestimoni.FotoUser = Path.GetRandomFileName() + Path.GetExtension(Data.FotoUser.FileName);
                        using (var fileStream = new FileStream(Path.Combine(pathUpload, tbTestimoni.FotoUser), FileMode.Create, FileAccess.Write))
                        {
                            Data.FotoUser.CopyTo(fileStream);
                        }
                    }
                    conn.Insert(tbTestimoni);
                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "AddTestimoni", ex);
                return null;
            }
        }
        public List<TestimoniModel> GetTestimonis(out string oMessage)
        {
            List<TestimoniModel> ret = schService.GetTestimonis(out oMessage);
            if (string.IsNullOrEmpty(oMessage))
            {
                ret = ret.Where(x => x.Status == StatusDataConstant.Aktif).ToList();
            }
            else
            {
                ret = new List<TestimoniModel>();
            }
            return ret;
        }
        public WebKegiatanModel GetEkskul(int IdUnitEkskul, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var item = (from a in conn.GetList<TbUnit>()
                                join b in conn.GetList<TbUnitEkskul>() on a.IdUnit equals b.IdUnit
                                join c in conn.GetList<TbUser>() on b.CreatedBy equals c.IdUser
                                where b.IdUnitEkskul == IdUnitEkskul
                                && b.Status == StatusProsesKontenConstant.Published
                                select new { a.NamaSingkat, b.IdUnitEkskul, b.Judul, b.Deskripsi, b.CreatedDate, c.FirstName, c.MiddleName, c.LastName, b.FileImage }).FirstOrDefault();
                    string keteranganSingkat = StripHTML(item.Deskripsi);
                    if (keteranganSingkat.Length >= 170)
                        keteranganSingkat = keteranganSingkat.Substring(0, 170) + "...";
                    string keteranganSingkatBener = StripHTML(item.Deskripsi);
                    if (keteranganSingkatBener.Length >= 170)
                        keteranganSingkatBener = keteranganSingkatBener.Substring(0, 60) + "...";
                    WebKegiatanModel ret = new WebKegiatanModel
                    {
                        Judul = item.Judul,
                        KeteranganSingkat = keteranganSingkat,
                        KeteranganSingkatBener = keteranganSingkatBener,
                        Keterangan = item.Deskripsi,
                        Unit = item.NamaSingkat.ToLower(),
                        FileImage = item.FileImage,
                        CreatedBy = userAppService.SetFullName(item.FirstName, item.MiddleName, item.LastName),
                        CreatedDate = item.CreatedDate.ToString("dd MMM"),
                        CreatedYear = item.CreatedDate.ToString("yyyy")
                    };
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetEkskul", ex);
                return null;
            }
        }
        public List<UnitBannerReadModel> GetUnitBanner(int IdUnit, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var datas = conn.GetList<TbUnitBanner>().Where(x => x.IdUnit == IdUnit).ToList();
                    List<UnitBannerReadModel> ret = new();
                    foreach (var item in datas.OrderByDescending(x => x.IdUnitBanner))
                    {
                        ret.Add(new UnitBannerReadModel
                        {
                            IdUnitBanner = item.IdUnitBanner,
                            IdUnit = item.IdUnit,
                            FileImage = item.FileImage
                        });
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetUnitBanner", ex);
                return null;
            }
        }

    }
}
