﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Dapper;
using Microsoft.AspNetCore.Http;
using RestSharp;
using Swg.Entities.His;
using Swg.Entities.kbm;
using Swg.Entities.Kbm;
using Swg.Entities.Lmg;
using Swg.Entities.Public;
using Swg.Entities.Ref;
using Swg.Models;
using Swg.Sch.Shared.Constants;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;

namespace Swg.Sch.Services
{
    public class ELearningService : IELearningService
    {
        private string _languageCode = "id";
        private readonly string ServiceName = "Swg.Sch.Services.ELearningService.";
        private readonly ICommonService commonService;
        private readonly IUserAppService userAppService;
        private readonly IDateTimeService dateTimeService;
        private readonly ISchService schService;
        private readonly ISiswaService siswaService;
        public ELearningService(
            ICommonService CommonService,
            IDateTimeService DateTimeService,
            IUserAppService UserAppService,
            ISchService SchService,
            ISiswaService SiswaService)
        {
            commonService = CommonService;
            userAppService = UserAppService;
            dateTimeService = DateTimeService;
            schService = SchService;
            siswaService = SiswaService;
        }
        private string PushNotif(string IdDevice, string Judul, string Konten)
        {
            var client = new RestClient("https://onesignal.com/api/v1/notifications");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            string param = "{\"app_id\": \"8ff8cf94-4642-45ae-9f77-e80aca03e548\",\"contents\": {\"id\": \"" + Konten + "\"},\"headings\": {\"id\": \"" + Judul + "\"},\"content_available\": true,\"priority\": 10,\"include_player_ids\": [\"" + IdDevice + "\"]}";
            request.AddHeader("Content-Type", "application/json; charset=utf-8");
            request.AddHeader("Authorization", "Basic MjI0NjViNGItZmE5MS00NDIzLWIwNTUtOTdiMThlMDEwN2Iz");
            request.AddParameter("application/json; charset=utf-8", param, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return null;
        }
        private KbmSoalModel GetKbmMateriSoal(IDbConnection conn, int IdKbmMateri, int IdJenisSoal, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                var tbSoal = (from a in conn.GetList<TbKbmSoal>()
                              where a.IdKbmMateri == IdKbmMateri
                              & a.IdJenisSoal == IdJenisSoal
                              select a).FirstOrDefault();
                if (tbSoal == null) { return null; }

                var ret = new KbmSoalModel
                {
                    IdJenisSoal = tbSoal.IdJenisSoal,
                    IdJenisJawaban = tbSoal.IdJenisJawaban,
                    JenisSoal = JenisKbmSoalConstant.DictJenisKbmSoal[tbSoal.IdJenisSoal],
                    JenisJawaban = JenisPathFileConstant.DictJenisPathFile[tbSoal.IdJenisJawaban]
                };
                if (!string.IsNullOrEmpty(tbSoal.NamaFile))
                {
                    ret.NamaFile = tbSoal.NamaFile;
                }
                else
                {
                    ret.NamaUrl = tbSoal.NamaUrl;
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKbmBySiswa", ex);
                return null;
            }
        }
        private List<KbmMediaModel> GetKbmMedias(IDbConnection conn, int IdKbmMateri, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                var kbmMedias = from a in conn.GetList<TbKbmMedia>()
                                where a.IdKbmMateri == IdKbmMateri
                                select a;
                List<KbmMediaModel> ret = new List<KbmMediaModel>();
                if (kbmMedias != null & kbmMedias.Count() > 0)
                {
                    foreach (var kbmMedia in kbmMedias)
                    {
                        KbmMediaModel m = new KbmMediaModel();
                        m.IdKbmMedia = kbmMedia.IdKbmMedia;
                        if (kbmMedia.NamaFile != null)
                        {
                            m.IdJenisPathFile = JenisPathFileConstant.File;
                            m.PathFileUrl = kbmMedia.NamaFile;
                        }
                        else
                        {
                            m.IdJenisPathFile = JenisPathFileConstant.Url;
                            m.PathFileUrl = kbmMedia.NamaUrl;
                        }
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKbmBySiswa", ex);
                return null;
            }
        }
        private KbmJawabanSusulanModel GetJawabanSusulan(IDbConnection conn, int IdKbmMateri, int IdSiswa, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                var tbJawabanSusulan = (from a in conn.GetList<TbKbmJawabanSusulan>()
                                        where a.IdKbmMateri == IdKbmMateri
                                        & a.IdSiswa == IdSiswa
                                        select a).FirstOrDefault();
                if (tbJawabanSusulan == null) { return null; }

                var ret = new KbmJawabanSusulanModel
                {
                    Catatan = tbJawabanSusulan.Catatan
                };
                if (!string.IsNullOrEmpty(tbJawabanSusulan.NamaFile))
                {
                    ret.IdJenisPathFile = JenisPathFileConstant.File;
                    ret.NamaFile = tbJawabanSusulan.NamaFile;
                }
                else
                {
                    ret.IdJenisPathFile = JenisPathFileConstant.File;
                    ret.NamaUrl = tbJawabanSusulan.NamaUrl;
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJawabanSusulan", ex);
                return null;
            }
        }
        public List<ComboModel> GetJenisBantuan()
        {
            return (from a in JenisKbmTugasBantuanOrtuConstant.DictJenisKbmTugasBantuanOrtu
                    select new ComboModel { Id = a.Key, Nama = a.Value }).OrderBy(x => x.Id).ToList();

        }
        public List<KbmSiswaListModel> GetKbmBySiswa(int IdSiswa, string Tanggal, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    DateTime currDate = DateTime.Now;
                    DateTime NextFirstDate = dateTimeService.GetFirstDate(currDate.AddDays(7));
                    if (!string.IsNullOrEmpty(Tanggal))
                    {
                        currDate = dateTimeService.StrToDateTime(Tanggal);
                    }
                    if (currDate >= NextFirstDate)
                    {
                        oMessage = string.Format("KBM belum aktif untuk tanggal {0}", dateTimeService.DateToLongString(_languageCode, currDate));
                        return null;
                    }

                    DateTime minDate = dateTimeService.GetFirstDate(currDate);
                    DateTime maxDate = dateTimeService.GetLastDate(currDate);

                    var data = from materi in conn.GetList<TbKbmMateri>()
                               join kelaParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelaParalel.IdKelasParalel
                               join kelas in conn.GetList<TbKelas>() on kelaParalel.IdKelas equals kelas.IdKelas
                               join mapel in conn.GetList<TbMapel>() on materi.IdMapel equals mapel.IdMapel
                               join pegawai in conn.GetList<TbUser>() on materi.IdPegawai equals pegawai.IdUser
                               join jadwal in conn.GetList<TbKbmJadwal>() on materi.IdKbmJadwal equals jadwal.IdKbmJadwal
                               join rombel in conn.GetList<TbKelasRombel>() on kelaParalel.IdKelasParalel equals rombel.IdKelasParalel
                               join siswa in conn.GetList<TbSiswa>() on rombel.IdSiswa equals siswa.IdSiswa
                               join jawabanSusulan in conn.GetList<TbKbmJawabanSusulan>() on new { materi.IdKbmMateri, siswa.IdSiswa } equals new { jawabanSusulan.IdKbmMateri, jawabanSusulan.IdSiswa } into jawabanSusulan1
                               from jawabanSusulan in jawabanSusulan1.DefaultIfEmpty()
                               join jawaban in conn.GetList<TbKbmJawaban>() on new { materi.IdKbmMateri, rombel.IdSiswa } equals new { jawaban.IdKbmMateri, jawaban.IdSiswa } into jawaban1
                               from jawaban in jawaban1.DefaultIfEmpty()
                               where rombel.IdSiswa == IdSiswa
                               & materi.Tanggal >= minDate & materi.Tanggal <= maxDate
                               select new { materi, kelaParalel, kelas, mapel, pegawai, jadwal, rombel, siswa, jawabanSusulan, jawaban };
                    if (data == null || data.Count() == 0) { oMessage = "data kbm tidak ada"; return null; }
                    List<KbmSiswaListModel> ret = new List<KbmSiswaListModel>();
                    foreach (var item in data.OrderBy(x => x.materi.Tanggal))
                    {
                        KbmSiswaListModel m = new KbmSiswaListModel();
                        m.NamaSiswa = item.siswa.Nama;
                        m.IdKbmMateri = item.materi.IdKbmMateri;
                        m.Guru = userAppService.SetFullName(item.pegawai.FirstName, item.pegawai.MiddleName, item.pegawai.LastName);
                        m.NamaKelasParalel = item.kelaParalel.Nama;
                        m.NamaMapel = item.mapel.Nama;
                        m.Tanggal = item.materi.Tanggal.ToString("dd-MM-yyyy");
                        m.Hari = dateTimeService.GetLongDayName("id", (int)item.materi.Tanggal.DayOfWeek);
                        m.JamMulai = item.jadwal.JamMulai.ToString();
                        m.JamSelesai = item.jadwal.JamSelesai.ToString();
                        m.JudulMateri = item.materi.Judul;
                        var libur = schService.GetHariLiburByUnit(item.kelas.IdUnit, item.materi.Tanggal.ToString("dd-MM-yyyy"), out oMessage);
                        if (string.IsNullOrEmpty(oMessage))
                        {
                            if (libur != null)
                            {
                                m.NamaMapel = string.Format("LIBUR {0}", libur.Nama);
                            }
                        }
                        if (item.jawaban != null)
                        {
                            m.IdJenisSoal = item.jawaban.IdJenisSoal;
                            m.IsRemedial = (int)item.jawaban.IsRemedial;
                            if (item.jawaban.NilaiAngka != null)
                            {
                                m.NilaiAngka = (double)item.jawaban.NilaiAngka;
                            }
                            else
                            {
                                m.NilaiHuruf = item.jawaban.NilaiHuruf;
                            }
                            m.Catatan = item.jawaban.Catatan;
                            m.StatusMeeting = item.jawaban.Hadir == 0 ? "Tidak Hadir" : "Hadir";

                            if (!string.IsNullOrEmpty(item.jawaban.NamaFile))
                            {
                                m.IdJenisPathFile = JenisPathFileConstant.File;
                                m.PathFileUrl = item.jawaban.NamaFile;
                            }
                            else
                            {
                                if (item.jawaban.NamaUrl != null)
                                {
                                    m.IdJenisPathFile = JenisPathFileConstant.Url;
                                    m.PathFileUrl = item.jawaban.NamaUrl;
                                }
                            }
                        }
                        m.JawabanSusulan = GetJawabanSusulan(conn, item.materi.IdKbmMateri, item.siswa.IdSiswa, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;

                        ret.Add(m);
                    }

                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKbmBySiswa", ex);
                return null;
            }
        }
        public KbmSiswaModel GetKbmBySiswa(int IdSiswa, int IdKbmMateri, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var item = (from materi in conn.GetList<TbKbmMateri>()
                                join kelaParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelaParalel.IdKelasParalel
                                join kelas in conn.GetList<TbKelas>() on kelaParalel.IdKelas equals kelas.IdKelas
                                join mapel in conn.GetList<TbMapel>() on materi.IdMapel equals mapel.IdMapel
                                join pegawai in conn.GetList<TbUser>() on materi.IdPegawai equals pegawai.IdUser
                                join jadwal in conn.GetList<TbKbmJadwal>() on materi.IdKbmJadwal equals jadwal.IdKbmJadwal
                                join rombel in conn.GetList<TbKelasRombel>() on kelaParalel.IdKelasParalel equals rombel.IdKelasParalel
                                join jawaban in conn.GetList<TbKbmJawaban>() on new { materi.IdKbmMateri, rombel.IdSiswa } equals new { jawaban.IdKbmMateri, jawaban.IdSiswa } into tugas1
                                from jawaban in tugas1.DefaultIfEmpty()
                                where rombel.IdSiswa == IdSiswa
                                & materi.IdKbmMateri == IdKbmMateri
                                select new { materi, kelaParalel, kelas, mapel, pegawai, jadwal, rombel, jawaban }).FirstOrDefault();
                    if (item == null) { oMessage = "data kbm tidak ada"; return null; }
                    KbmSiswaModel ret = new KbmSiswaModel
                    {
                        SoalReguler = new KbmSoalModel(),
                        SoalRemedial = new KbmSoalModel(),
                        SoalSusulan = new KbmSoalModel(),
                        FileKbmMedias = new List<KbmMediaModel>(),
                        TugasSiswas = new List<TugasSiswaList>()
                    };
                    ret.IdUnit = item.kelas.IdUnit;
                    ret.IdKbmMateri = item.materi.IdKbmMateri;
                    ret.Guru = userAppService.SetFullName(item.pegawai.FirstName, item.pegawai.MiddleName, item.pegawai.LastName);
                    ret.NamaKelasParalel = item.kelaParalel.Nama;
                    ret.NamaMapel = item.mapel.Nama;
                    ret.Tanggal = dateTimeService.DateToLongString(_languageCode, item.materi.Tanggal);
                    ret.Hari = dateTimeService.GetLongDayName(_languageCode, (int)item.materi.Tanggal.DayOfWeek);
                    ret.JamMulai = item.jadwal.JamMulai.ToString();
                    ret.JamSelesai = item.jadwal.JamSelesai.ToString();
                    ret.JudulMateri = item.materi.Judul;

                    var libur = schService.GetHariLiburByUnit(item.kelas.IdUnit, item.materi.Tanggal.ToString("dd-MM-yyyy"), out oMessage);
                    if (string.IsNullOrEmpty(oMessage))
                    {
                        if (libur != null)
                        {
                            ret.NamaMapel = string.Format("LIBUR {0}", libur.Nama);
                        }
                    }
                    if (item.jawaban != null)
                    {
                        ret.IdJenisSoal = item.jawaban.IdJenisSoal;
                        ret.IsRemedial = (int)item.jawaban.IsRemedial;

                        if (item.jawaban.NilaiAngka != null)
                        {
                            ret.NilaiAngka = (double)item.jawaban.NilaiAngka;
                        }
                        else
                        {
                            ret.NilaiHuruf = item.jawaban.NilaiHuruf;
                        }
                        ret.Catatan = item.jawaban.Catatan;
                        ret.StatusMeeting = item.jawaban.Hadir == 0 ? "Hadir" : "Tidak Hadir";
                        if (!string.IsNullOrEmpty(item.jawaban.NamaFile))
                        {
                            ret.IdJenisPathFile = JenisPathFileConstant.File;
                            ret.PathFileUrl = item.jawaban.NamaFile;
                        }
                        else
                        {
                            ret.IdJenisPathFile = JenisPathFileConstant.Url;
                            ret.PathFileUrl = item.jawaban.NamaUrl;
                        }
                    }


                    ret.FotoGuru = item.pegawai.ProfileFile;
                    ret.NamaFileMateri = item.materi.NamaFileMateri;
                    ret.Deskripsi = item.materi.Deskripsi;
                    ret.DeskripsiTugas = item.materi.DeskripsiTugas;
                    ret.NamaUrlMeeting = item.materi.NamaUrlMeeting;

                    ret.SoalReguler = GetKbmMateriSoal(conn, IdKbmMateri, JenisKbmSoalConstant.Reguler, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    ret.SoalRemedial = GetKbmMateriSoal(conn, IdKbmMateri, JenisKbmSoalConstant.Remedial, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    ret.SoalSusulan = GetKbmMateriSoal(conn, IdKbmMateri, JenisKbmSoalConstant.Susulan, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;

                    ret.FileKbmMedias = GetKbmMedias(conn, IdKbmMateri, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;

                    int _totUrl = 0;
                    int _totFile = 0;
                    foreach (var itemx in ret.FileKbmMedias)
                    {
                        if (itemx.IdJenisPathFile == JenisPathFileConstant.Url) _totUrl += 1;
                        if (itemx.IdJenisPathFile == JenisPathFileConstant.File) _totFile += 1;
                    }

                    ret.TotFileKbmUrl = _totUrl;
                    ret.TotFileKbmFile = _totFile;

                    var tugass = from a in conn.GetList<TbKbmMateri>()
                                 join c in conn.GetList<TbKelasRombel>() on a.IdKelasParalel equals c.IdKelasParalel
                                 join d in conn.GetList<TbSiswa>() on c.IdSiswa equals d.IdSiswa
                                 //  join e in conn.GetList<TbKbmJawaban>() on a.IdKbmMateri equals e.IdKbmMateri into e1
                                 //  from e in e1.DefaultIfEmpty()
                                 where a.IdKbmMateri == IdKbmMateri & d.Status == StatusSiswa.Aktif
                                 select new { d.Nama };
                    if (tugass != null & tugass.Count() > 0)
                    {
                        foreach (var tugas in tugass.OrderBy(x => x.Nama))
                        {
                            TugasSiswaList m = new TugasSiswaList();
                            m.NamaSiswa = tugas.Nama;
                            m.TugasStatus = null;
                            // m.TugasStatus = "Belum Mengerjakan";
                            // if (tugas.e != null)
                            //     m.TugasStatus = "Sudah Mengerjakan";
                            ret.TugasSiswas.Add(m);
                        }
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKbmBySiswa", ex);
                return null;
            }
        }
        public List<KbmGuruListModel> GetKbmByPegawai(int IdPegawai, string Tanggal, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {

                    DateTime currDate = DateTime.Now.AddDays(7);
                    DateTime NextFirstDate = dateTimeService.GetFirstDate(currDate.AddDays(7));
                    if (!string.IsNullOrEmpty(Tanggal))
                    {
                        currDate = dateTimeService.StrToDateTime(Tanggal);
                    }
                    if (currDate >= NextFirstDate)
                    {
                        oMessage = string.Format("kbm belum aktif untuk tanggal {0}", dateTimeService.DateToLongString(_languageCode, currDate));
                        return null;
                    }
                    DateTime minDate = dateTimeService.GetFirstDate(currDate);
                    DateTime maxDate = dateTimeService.GetLastDate(currDate);
                    var data = from materi in conn.GetList<TbKbmMateri>()
                               join kelaParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelaParalel.IdKelasParalel
                               join kelas in conn.GetList<TbKelas>() on kelaParalel.IdKelas equals kelas.IdKelas
                               join mapel in conn.GetList<TbMapel>() on materi.IdMapel equals mapel.IdMapel
                               join pegawai in conn.GetList<TbUser>() on materi.IdPegawai equals pegawai.IdUser
                               join jadwal in conn.GetList<TbKbmJadwal>() on materi.IdKbmJadwal equals jadwal.IdKbmJadwal
                               join userc in conn.GetList<TbUser>() on materi.CreatedBy equals userc.IdUser into user1
                               from userc in user1.DefaultIfEmpty()
                               join useru in conn.GetList<TbUser>() on materi.UpdatedBy equals useru.IdUser into user2
                               from useru in user2.DefaultIfEmpty()
                               where materi.IdPegawai == IdPegawai
                               & materi.Tanggal >= minDate & materi.Tanggal <= maxDate
                               select new { materi, kelaParalel, kelas, mapel, pegawai, jadwal, userc, useru };

                    if (data == null || data.Count() == 0) { oMessage = "data kbm tidak ada"; return null; }
                    List<KbmGuruListModel> ret = new List<KbmGuruListModel>();
                    foreach (var item in data.OrderBy(x => x.materi.Tanggal))
                    {
                        KbmGuruListModel m = new KbmGuruListModel();
                        m.IdKbmMateri = item.materi.IdKbmMateri;
                        m.Guru = userAppService.SetFullName(item.pegawai.FirstName, item.pegawai.MiddleName, item.pegawai.LastName);
                        m.NamaKelasParalel = item.kelaParalel.Nama;
                        m.NamaMapel = item.mapel.Nama;
                        m.Tanggal = dateTimeService.DateToLongString(_languageCode, item.materi.Tanggal);
                        m.Hari = dateTimeService.GetLongDayName(_languageCode, (int)item.materi.Tanggal.DayOfWeek);
                        m.JamMulai = item.jadwal.JamMulai.ToString();
                        m.JamSelesai = item.jadwal.JamSelesai.ToString();
                        m.JudulMateri = item.materi.Judul;
                        m.CreatedBy = item.userc == null ? "" : userAppService.SetFullName(item.userc.FirstName, item.userc.MiddleName, item.userc.LastName);
                        m.CreatedDate = item.userc == null ? "" : dateTimeService.DateToLongString(_languageCode, (DateTime)item.materi.CreatedDate);
                        m.UpdatedBy = item.useru == null ? "" : userAppService.SetFullName(item.useru.FirstName, item.useru.MiddleName, item.useru.LastName);
                        m.UpdatedDate = item.useru == null ? "" : dateTimeService.DateToLongString(_languageCode, (DateTime)item.materi.UpdatedDate);

                        var libur = schService.GetHariLiburByUnit(item.kelas.IdUnit, item.materi.Tanggal.ToString("dd-MM-yyyy"), out oMessage);
                        if (string.IsNullOrEmpty(oMessage))
                        {
                            if (libur != null)
                            {
                                m.NamaMapel = string.Format("LIBUR {0}", libur.Nama);
                            }
                        }

                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKbmByPegawai", ex);
                return null;

            }
        }
        public KbmGuruModel GetKbmByPegawai(int IdKbmMateri, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var item = (from materi in conn.GetList<TbKbmMateri>()
                                join kelaParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelaParalel.IdKelasParalel
                                join kelas in conn.GetList<TbKelas>() on kelaParalel.IdKelas equals kelas.IdKelas
                                join mapel in conn.GetList<TbMapel>() on materi.IdMapel equals mapel.IdMapel
                                join pegawai in conn.GetList<TbUser>() on materi.IdPegawai equals pegawai.IdUser
                                join jadwal in conn.GetList<TbKbmJadwal>() on materi.IdKbmJadwal equals jadwal.IdKbmJadwal
                                join userc in conn.GetList<TbUser>() on materi.CreatedBy equals userc.IdUser into user1
                                from userc in user1.DefaultIfEmpty()
                                join useru in conn.GetList<TbUser>() on materi.UpdatedBy equals useru.IdUser into user2
                                from useru in user2.DefaultIfEmpty()
                                where materi.IdKbmMateri == IdKbmMateri
                                select new { materi, kelaParalel, kelas, mapel, pegawai, jadwal, userc, useru }).FirstOrDefault();
                    if (item == null) { oMessage = "data kbm tidak ada"; return null; }
                    KbmGuruModel ret = new KbmGuruModel
                    {
                        SoalReguler = new KbmSoalModel(),
                        SoalRemedial = new KbmSoalModel(),
                        SoalSusulan = new KbmSoalModel(),
                        FileKbmMedias = new List<KbmMediaModel>()
                    };
                    ret.IdKbmMateri = item.materi.IdKbmMateri;
                    ret.Guru = userAppService.SetFullName(item.pegawai.FirstName, item.pegawai.MiddleName, item.pegawai.LastName);
                    ret.NamaKelasParalel = item.kelaParalel.Nama;
                    ret.NamaMapel = item.mapel.Nama;
                    ret.Tanggal = dateTimeService.DateToLongString(_languageCode, item.materi.Tanggal);
                    ret.Hari = dateTimeService.GetLongDayName(_languageCode, (int)item.materi.Tanggal.DayOfWeek);
                    ret.JamMulai = item.jadwal.JamMulai.ToString();
                    ret.JamSelesai = item.jadwal.JamSelesai.ToString();
                    ret.JudulMateri = item.materi.Judul;
                    ret.NamaFileMateri = item.materi.NamaFileMateri;
                    ret.Deskripsi = item.materi.Deskripsi;
                    ret.DeskripsiTugas = item.materi.DeskripsiTugas;
                    ret.NamaUrlMeeting = item.materi.NamaUrlMeeting;

                    var libur = schService.GetHariLiburByUnit(item.kelas.IdUnit, item.materi.Tanggal.ToString("dd-MM-yyyy"), out oMessage);
                    if (string.IsNullOrEmpty(oMessage))
                    {
                        if (libur != null)
                        {
                            ret.NamaMapel = string.Format("LIBUR {0}", libur.Nama);
                        }
                    }

                    ret.SoalReguler = GetKbmMateriSoal(conn, IdKbmMateri, JenisKbmSoalConstant.Reguler, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    ret.SoalRemedial = GetKbmMateriSoal(conn, IdKbmMateri, JenisKbmSoalConstant.Remedial, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    ret.SoalSusulan = GetKbmMateriSoal(conn, IdKbmMateri, JenisKbmSoalConstant.Susulan, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;

                    ret.FileKbmMedias = GetKbmMedias(conn, IdKbmMateri, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;

                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKbmByPegawai", ex);
                return null;
            }
        }
        public List<KbmSiswaListModel> GetKbmTugasSiswas(int IdKbmMateri, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {

                    var data = from materi in conn.GetList<TbKbmMateri>()
                               join kelaParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelaParalel.IdKelasParalel
                               join kelas in conn.GetList<TbKelas>() on kelaParalel.IdKelas equals kelas.IdKelas
                               join mapel in conn.GetList<TbMapel>() on materi.IdMapel equals mapel.IdMapel
                               join pegawai in conn.GetList<TbUser>() on materi.IdPegawai equals pegawai.IdUser
                               join jadwal in conn.GetList<TbKbmJadwal>() on materi.IdKbmJadwal equals jadwal.IdKbmJadwal
                               join rombel in conn.GetList<TbKelasRombel>() on kelaParalel.IdKelasParalel equals rombel.IdKelasParalel
                               join siswa in conn.GetList<TbSiswa>() on rombel.IdSiswa equals siswa.IdSiswa

                               join jawabanSusulan in conn.GetList<TbKbmJawabanSusulan>() on new { materi.IdKbmMateri, siswa.IdSiswa } equals new { jawabanSusulan.IdKbmMateri, jawabanSusulan.IdSiswa } into jawabanSusulan1
                               from jawabanSusulan in jawabanSusulan1.DefaultIfEmpty()

                               join jawaban in conn.GetList<TbKbmJawaban>() on new { materi.IdKbmMateri, siswa.IdSiswa } equals new { jawaban.IdKbmMateri, jawaban.IdSiswa } into jawaban1
                               from jawaban in jawaban1.DefaultIfEmpty()
                               where materi.IdKbmMateri == IdKbmMateri & siswa.Status == StatusSiswa.Aktif
                               select new { materi, kelaParalel, kelas, mapel, pegawai, jadwal, rombel, siswa, jawabanSusulan, jawaban };
                    if (data == null || data.Count() == 0) { oMessage = "data kbm tidak ada"; return null; }
                    List<KbmSiswaListModel> ret = new List<KbmSiswaListModel>();
                    foreach (var item in data.OrderBy(x => x.siswa.Nama))
                    {
                        KbmSiswaListModel m = new KbmSiswaListModel();
                        m.IdSiswa = item.siswa.IdSiswa;
                        m.Nis = item.siswa.Nis;
                        m.NamaSiswa = item.siswa.Nama;
                        m.IdKbmMateri = item.materi.IdKbmMateri;
                        m.Guru = userAppService.SetFullName(item.pegawai.FirstName, item.pegawai.MiddleName, item.pegawai.LastName);
                        m.NamaKelasParalel = item.kelaParalel.Nama;
                        m.NamaMapel = item.mapel.Nama;
                        m.Tanggal = dateTimeService.DateToLongString(_languageCode, item.materi.Tanggal);
                        m.Hari = dateTimeService.GetLongDayName(_languageCode, (int)item.materi.Tanggal.DayOfWeek);
                        m.JamMulai = item.jadwal.JamMulai.ToString();
                        m.JamSelesai = item.jadwal.JamSelesai.ToString();
                        m.JudulMateri = item.materi.Judul;
                        m.NamaUrlMeeting = item.materi.NamaUrlMeeting;
                        var libur = schService.GetHariLiburByUnit(item.kelas.IdUnit, item.materi.Tanggal.ToString("dd-MM-yyyy"), out oMessage);
                        if (string.IsNullOrEmpty(oMessage))
                        {
                            if (libur != null)
                            {
                                m.NamaMapel = string.Format("LIBUR {0}", libur.Nama);
                            }
                        }
                        if (item.jawaban != null)
                        {
                            m.IdJenisSoal = item.jawaban.IdJenisSoal;
                            m.IsRemedial = (int)item.jawaban.IsRemedial;
                            if (item.jawaban.NilaiAngka != null & item.jawaban.NilaiAngka != 0)
                            {
                                m.NilaiAngka = (double)item.jawaban.NilaiAngka;
                            }
                            m.NilaiHuruf = item.jawaban.NilaiHuruf;

                            m.Catatan = item.jawaban.Catatan;
                            m.StatusMeeting = item.jawaban.Hadir == -1 ? "Tidak Hadir" : "Hadir";
                            if (!string.IsNullOrEmpty(item.jawaban.NamaFile))
                            {
                                m.IdJenisPathFile = JenisPathFileConstant.File;
                                m.PathFileUrl = item.jawaban.NamaFile;
                            }
                            else
                            {
                                if (item.jawaban.NamaUrl != null)
                                {
                                    m.IdJenisPathFile = JenisPathFileConstant.Url;
                                    m.PathFileUrl = item.jawaban.NamaUrl;
                                }
                            }
                        }
                        m.JawabanSusulan = GetJawabanSusulan(conn, item.materi.IdKbmMateri, item.siswa.IdSiswa, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;

                        ret.Add(m);
                    }

                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKbmTugasSiswas", ex);
                return null;
            }
        }
        private string UpdateMateri(IDbConnection conn, int IdPegawai, int IdKbmMateri, KbmUpdateMateriModel Data, IFormFile FileMateri, List<KbmUpdateSoalModel> FileSoals, List<KbmUpdateMateriMediaModel> FileMedias)
        {
            try
            {
                var tbKbmMateri = conn.Get<TbKbmMateri>(IdKbmMateri);
                if (tbKbmMateri == null) return "data kbm tidak ada";
                DateTime minDate = dateTimeService.GetFirstDate(DateTime.Now.AddDays(7));
                DateTime maxDate = dateTimeService.GetLastDate(DateTime.Now.AddDays(7));
                if (tbKbmMateri.Tanggal.Date < minDate.Date)
                    return "data kbm sudah tidak bisa di edit";
                if (tbKbmMateri.Tanggal.Date > maxDate.Date)
                    return "data kbm belum bisa di edit";

                tbKbmMateri.Judul = Data.Judul;
                tbKbmMateri.Deskripsi = Data.Deskripsi;
                tbKbmMateri.DeskripsiTugas = Data.DeskripsiTugas;
                if (tbKbmMateri.CreatedBy == null)
                {
                    tbKbmMateri.CreatedBy = IdPegawai;
                    tbKbmMateri.CreatedDate = dateTimeService.GetCurrdate();
                }
                else
                {
                    tbKbmMateri.UpdatedBy = IdPegawai;
                    tbKbmMateri.UpdatedDate = dateTimeService.GetCurrdate();
                }
                if (FileMateri != null)
                {
                    tbKbmMateri.NamaFileMateri = string.Format("materi_{0}{1}", IdKbmMateri.ToString(), Path.GetExtension(FileMateri.FileName));
                    if (File.Exists(Path.Combine(AppSetting.PathElearning, tbKbmMateri.NamaFileMateri)))
                        File.Delete(Path.Combine(AppSetting.PathElearning, tbKbmMateri.NamaFileMateri));
                    using (var fileStream = new FileStream(Path.Combine(AppSetting.PathElearning, tbKbmMateri.NamaFileMateri), FileMode.Create))
                    {
                        FileMateri.CopyTo(fileStream);
                    }
                }
                conn.Update(tbKbmMateri);

                if (FileSoals != null & FileSoals.Count > 0)
                {
                    for (int i = 0; i < FileSoals.Count; i++)
                    {
                        var tbKbmSoal = (from a in conn.GetList<TbKbmSoal>()
                                         where a.IdKbmMateri == IdKbmMateri
                                         & a.IdJenisSoal == FileSoals[i].IdJenisSoal
                                         select a).FirstOrDefault();
                        if (tbKbmSoal != null)
                        {
                            if (FileSoals[i].FileSoal != null)
                            {
                                if (!string.IsNullOrEmpty(tbKbmSoal.NamaFile))
                                    if (File.Exists(Path.Combine(AppSetting.PathElearning, tbKbmSoal.NamaFile)))
                                        File.Delete(Path.Combine(AppSetting.PathElearning, tbKbmSoal.NamaFile));
                            }

                            // tbKbmSoal.NamaFile = null;
                            // tbKbmSoal.NamaUrl = null;
                            if (!string.IsNullOrEmpty(FileSoals[i].NamaUrl))
                            {
                                tbKbmSoal.NamaFile = null;
                                tbKbmSoal.NamaUrl = FileSoals[i].NamaUrl;
                            }
                            else
                            {
                                if (FileSoals[i].FileSoal != null)
                                {
                                    tbKbmSoal.NamaUrl = null;
                                    tbKbmSoal.NamaFile = string.Format("{0}_{1}{2}", IdKbmMateri.ToString(), FileSoals[i].IdJenisSoal.ToString(), Path.GetExtension(FileSoals[i].FileSoal.FileName));
                                    using (var fileStream = new FileStream(Path.Combine(AppSetting.PathElearning, tbKbmSoal.NamaFile), FileMode.Create))
                                    {
                                        FileSoals[i].FileSoal.CopyTo(fileStream);
                                    }
                                }

                            }
                            int idJenisPathFile = FileSoals[i].IdJenisPathFile;
                            switch (idJenisPathFile)
                            {
                                case JenisPathFileConstant.TidakKeduanya:
                                    idJenisPathFile = JenisPathFileConstant.File;
                                    break;
                                default:
                                    break;
                            }
                            tbKbmSoal.IdJenisJawaban = idJenisPathFile;
                            conn.Update(tbKbmSoal);
                        }
                        else
                        {
                            tbKbmSoal = new TbKbmSoal
                            {
                                IdKbmMateri = IdKbmMateri,
                                IdJenisSoal = FileSoals[i].IdJenisSoal,
                                NamaFile = null,
                                NamaUrl = null,
                                IdJenisJawaban = FileSoals[i].IdJenisPathFile
                            };
                            if (!string.IsNullOrEmpty(FileSoals[i].NamaUrl))
                            {
                                tbKbmSoal.NamaUrl = FileSoals[i].NamaUrl;
                            }
                            else
                            {
                                if (FileSoals[i].FileSoal != null)
                                {
                                    tbKbmSoal.NamaFile = string.Format("{0}_{1}{2}", IdKbmMateri.ToString(), FileSoals[i].IdJenisSoal.ToString(), Path.GetExtension(FileSoals[i].FileSoal.FileName));
                                    using (var fileStream = new FileStream(Path.Combine(AppSetting.PathElearning, tbKbmSoal.NamaFile), FileMode.Create))
                                    {
                                        FileSoals[i].FileSoal.CopyTo(fileStream);
                                    }
                                }
                            }
                            conn.Insert(tbKbmSoal);
                        }
                    }
                }

                if (FileMedias != null & FileMedias.Count() > 0)
                {
                    for (int i = 0; i < FileMedias.Count; i++)
                    {
                        TbKbmMedia tbKbmMedia = new TbKbmMedia
                        {
                            IdKbmMateri = IdKbmMateri
                        };
                        if (FileMedias[i].IdJenisPathFile == JenisPathFileConstant.File)
                        {
                            tbKbmMedia.NamaFile = string.Format("{0}_{1}{2}", IdKbmMateri.ToString(), i.ToString(), Path.GetExtension(FileMedias[i].FileMedia.FileName));
                            using (var fileStream = new FileStream(Path.Combine(AppSetting.PathElearning, tbKbmMedia.NamaFile), FileMode.Create))
                            {
                                FileMedias[i].FileMedia.CopyTo(fileStream);
                            }
                        }
                        else
                        {
                            tbKbmMedia.NamaUrl = FileMedias[i].NamaUrl;
                        }
                        conn.Insert(tbKbmMedia);
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "Private UpdateMateri", ex);
            }
        }
        public string UpdateMateri(int IdPegawai, int IdKbmMateri, KbmUpdateMateriModel Data, IFormFile FileMateri, List<KbmUpdateSoalModel> FileSoals, List<KbmUpdateMateriMediaModel> FileMedias)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string Err = UpdateMateri(conn, IdPegawai, IdKbmMateri, Data, FileMateri, FileSoals, FileMedias);
                        if (!string.IsNullOrEmpty(Err))
                            return Err;
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UpdateMateri", ex);
            }
        }
        public string UpdateMateri(int IdPegawai, List<int> IdKbmMateris, KbmUpdateMateriModel Data, IFormFile FileMateri, List<KbmUpdateSoalModel> FileSoals, List<KbmUpdateMateriMediaModel> FileMedias)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        for (int i = 0; i < IdKbmMateris.Count(); i++)
                        {
                            string Err = UpdateMateri(conn, IdPegawai, IdKbmMateris[i], Data, FileMateri, FileSoals, FileMedias);
                            if (!string.IsNullOrEmpty(Err))
                                return Err;
                        }
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UpdateMateri", ex);
            }
        }
        public string UploadJawaban(int IdSiswa, int IdKbmMateri, int IdJenisSoal, int IdJenisBantuanOrtu, string NamaUrl, IFormFile FileUpload)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var data = (from soal in conn.GetList<TbKbmSoal>()
                                    join materi in conn.GetList<TbKbmMateri>() on soal.IdKbmMateri equals materi.IdKbmMateri
                                    join mapel in conn.GetList<TbMapel>() on materi.IdMapel equals mapel.IdMapel
                                    join kelasParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelasParalel.IdKelasParalel
                                    join rombel in conn.GetList<TbKelasRombel>() on kelasParalel.IdKelasParalel equals rombel.IdKelasParalel
                                    join siswa in conn.GetList<TbSiswa>() on rombel.IdSiswa equals siswa.IdSiswa
                                    where soal.IdKbmMateri == IdKbmMateri & soal.IdJenisSoal == IdJenisSoal
                                    & siswa.IdSiswa == IdSiswa
                                    select new { soal, materi, mapel, kelasParalel, rombel, siswa }).FirstOrDefault();
                        if (data == null)
                        {
                            return string.Format("Ananda {0} tidak ada di kelas {1} untuk mata pelajaran {2}", data.siswa.Nama, data.kelasParalel.Nama, data.mapel.Nama);
                        }

                        DateTime minDate = dateTimeService.GetFirstDate(data.materi.Tanggal);
                        DateTime maxDate = dateTimeService.GetLastDate(data.materi.Tanggal);

                        if (dateTimeService.GetCurrdate().Date < minDate.Date)
                            return "jawaban belum bisa diupload";
                        if (dateTimeService.GetCurrdate().Date > maxDate.Date)
                            return "jawaban sudah tidak bisa diupload";

                        bool isInsert = false;
                        TbKbmJawaban tbKbmJawaban = (from a in conn.GetList<TbKbmJawaban>()
                                                     where a.IdSiswa == IdSiswa
                                                     & a.IdKbmMateri == IdKbmMateri
                                                     select a).FirstOrDefault();
                        if (tbKbmJawaban == null)
                        {
                            isInsert = true;
                            tbKbmJawaban = new TbKbmJawaban
                            {
                                CreatedBy = IdSiswa,
                                CreatedDate = dateTimeService.GetCurrdate()
                            };
                        }
                        else
                        {
                            tbKbmJawaban.UpdatedBy = IdSiswa;
                            tbKbmJawaban.UpdatedDate = dateTimeService.GetCurrdate();
                            if (tbKbmJawaban.NilaiAngka != null)
                            {
                                if (tbKbmJawaban.IsRemedial != 1)
                                    return "Soal sudah dinilai";
                            }
                            if (!string.IsNullOrEmpty(tbKbmJawaban.NilaiHuruf))
                            {
                                if (tbKbmJawaban.IsRemedial != 1)
                                    return "Soal sudah dinilai";
                            }
                        }
                        tbKbmJawaban = new TbKbmJawaban
                        {
                            IdSiswa = IdSiswa,
                            IdKbmMateri = IdKbmMateri,
                            IdJenisBantuanOrtu = IdJenisBantuanOrtu,
                            IdJenisSoal = data.soal.IdJenisSoal
                        };

                        if (data.soal.IdJenisJawaban == JenisPathFileConstant.File)
                        {
                            if (FileUpload == null & FileUpload.Length == 0)
                                return "jawaban harus dalam bentuk file";
                            tbKbmJawaban.NamaFile = string.Format("{0}_{1}{2}", IdKbmMateri.ToString(), IdSiswa.ToString(), Path.GetExtension(FileUpload.FileName));
                            tbKbmJawaban.NamaUrl = null;

                            if (File.Exists(Path.Combine(AppSetting.PathElearning, tbKbmJawaban.NamaFile)))
                                File.Delete(Path.Combine(AppSetting.PathElearning, tbKbmJawaban.NamaFile));
                            using (var fileStream = new FileStream(Path.Combine(AppSetting.PathElearning, tbKbmJawaban.NamaFile), FileMode.Create))
                            {
                                FileUpload.CopyTo(fileStream);
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(NamaUrl))
                                return "jawaban harus diisi!";
                            //return "jawaban harus dalam bentuk link url video";
                            tbKbmJawaban.NamaFile = null;
                            tbKbmJawaban.NamaUrl = NamaUrl;
                        }
                        if (isInsert)
                        {
                            conn.Insert(tbKbmJawaban);
                        }
                        else
                        {
                            conn.Update(tbKbmJawaban);
                        }
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UploadJawaban", ex);

            }
        }


        public string UploadJawaban(int IdSiswa, int IdKbmMateri, int IdJenisSoal, int IdJenisBantuanOrtu, string NamaUrl, List<IFormFile> FileUploads)
        {
            try
            {
                ///VALIDASI MAX FILE
                if (FileUploads.Count() > 5)
                    return "Maksimal upload 5 file!";
                for (int i = 0; i < FileUploads.Count(); i++)
                {
                    int b = (int)FileUploads[i].Length;
                    int kb = b / 1024;
                    int mb = kb / 1024;
                    int gb = mb / 1024;

                    if (mb > 5)
                        return "Ukuran file (" + FileUploads[i].FileName + ") terlalu besar, maksimal ukuran file 5MB";
                }
                ///END VALIDASI MAX FILE
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbKbmSoal = from a in conn.GetList<TbKbmSoal>()
                                        join b in conn.GetList<TbKbmMateri>() on a.IdKbmMateri equals b.IdKbmMateri
                                        where b.IdKbmMateri == IdKbmMateri
                                        select a;
                        if (tbKbmSoal == null || tbKbmSoal.Count() == 0)
                        {
                            IdJenisSoal = JenisKbmSoalConstant.Reguler;
                            TbKbmSoal item = new TbKbmSoal
                            {
                                IdKbmMateri = IdKbmMateri,
                                IdJenisSoal = IdJenisSoal,
                                IdJenisJawaban = JenisPathFileConstant.Rubrik
                            };
                            conn.Insert(item);
                        }

                        var data = (from soal in conn.GetList<TbKbmSoal>()
                                    join materi in conn.GetList<TbKbmMateri>() on soal.IdKbmMateri equals materi.IdKbmMateri
                                    join mapel in conn.GetList<TbMapel>() on materi.IdMapel equals mapel.IdMapel
                                    join kelasParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelasParalel.IdKelasParalel
                                    join rombel in conn.GetList<TbKelasRombel>() on kelasParalel.IdKelasParalel equals rombel.IdKelasParalel
                                    join siswa in conn.GetList<TbSiswa>() on rombel.IdSiswa equals siswa.IdSiswa
                                    where soal.IdKbmMateri == IdKbmMateri & soal.IdJenisSoal == IdJenisSoal
                                    & siswa.IdSiswa == IdSiswa
                                    select new { soal, materi, mapel, kelasParalel, rombel, siswa }).FirstOrDefault();
                        if (data == null)
                        {
                            return string.Format("Ananda {0} tidak ada di kelas {1} untuk mata pelajaran {2}", data.siswa.Nama, data.kelasParalel.Nama, data.mapel.Nama);
                        }

                        DateTime minDate = dateTimeService.GetFirstDate(data.materi.Tanggal);
                        DateTime maxDate = dateTimeService.GetLastDate(data.materi.Tanggal);

                        if (dateTimeService.GetCurrdate().Date < minDate.Date)
                            return "jawaban belum bisa diupload";
                        if (dateTimeService.GetCurrdate().Date > maxDate.Date)
                            return "jawaban sudah tidak bisa diupload";

                        bool isInsert = false;
                        TbKbmJawaban tbKbmJawaban = (from a in conn.GetList<TbKbmJawaban>()
                                                     where a.IdSiswa == IdSiswa
                                                     & a.IdKbmMateri == IdKbmMateri
                                                     select a).FirstOrDefault();
                        if (tbKbmJawaban == null)
                        {
                            isInsert = true;
                            tbKbmJawaban = new TbKbmJawaban
                            {
                                CreatedBy = IdSiswa,
                                CreatedDate = dateTimeService.GetCurrdate()
                            };
                        }
                        else
                        {
                            tbKbmJawaban.UpdatedBy = IdSiswa;
                            tbKbmJawaban.UpdatedDate = dateTimeService.GetCurrdate();
                            if (tbKbmJawaban.NilaiAngka != null)
                            {
                                if (tbKbmJawaban.IsRemedial != 1)
                                    return "Soal sudah dinilai";
                            }
                            if (!string.IsNullOrEmpty(tbKbmJawaban.NilaiHuruf))
                            {
                                if (tbKbmJawaban.IsRemedial != 1)
                                    return "Soal sudah dinilai";
                            }
                        }
                        tbKbmJawaban = new TbKbmJawaban
                        {
                            IdSiswa = IdSiswa,
                            IdKbmMateri = IdKbmMateri,
                            IdJenisBantuanOrtu = IdJenisBantuanOrtu,
                            IdJenisSoal = data.soal.IdJenisSoal
                        };

                        if (data.soal.IdJenisJawaban == JenisPathFileConstant.File)
                        {
                            if (FileUploads != null & FileUploads.Count() > 0)
                            {
                                if (!string.IsNullOrEmpty(tbKbmJawaban.NamaFile))
                                {
                                    string[] files = tbKbmJawaban.NamaFile.Split(";");
                                    for (int i = 0; i < files.Length; i++)
                                    {
                                        if (File.Exists(Path.Combine(AppSetting.PathElearning, files[i])))
                                            File.Delete(Path.Combine(AppSetting.PathElearning, files[i]));
                                    }
                                }



                                string namaFile = string.Empty;
                                for (int i = 0; i < FileUploads.Count(); i++)
                                {
                                    string tempNamaFile = string.Empty;
                                    tempNamaFile = string.Format("{0}_{1}_{2}{3}", IdKbmMateri.ToString(), IdSiswa.ToString(), (i + 1).ToString(), Path.GetExtension(FileUploads[i].FileName));
                                    if (string.IsNullOrEmpty(namaFile))
                                    {
                                        namaFile = tempNamaFile;
                                    }
                                    else
                                    {
                                        namaFile += ";" + tempNamaFile;
                                    }
                                    using (var fileStream = new FileStream(Path.Combine(AppSetting.PathElearning, tempNamaFile), FileMode.Create))
                                    {
                                        FileUploads[i].CopyTo(fileStream);
                                    }
                                }
                                tbKbmJawaban.NamaFile = namaFile;
                                tbKbmJawaban.NamaUrl = null;
                            }
                            else
                            {
                                return "jawaban harus diupload dalam bentuk file";
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(NamaUrl))
                                return "jawaban harus diisi!";
                            //return "jawaban harus dalam bentuk link url video";
                            tbKbmJawaban.NamaFile = null;
                            tbKbmJawaban.NamaUrl = NamaUrl;
                        }
                        if (isInsert)
                        {
                            conn.Insert(tbKbmJawaban);
                            siswaService.PushInboxNotif(
                                IdSiswa,
                                "Berhasil Menyerahkan Tugas (" + data.mapel.Nama + ")",
                                "(Jam : " + dateTimeService.GetCurrdate().ToString("HH:mm") + ") Terimakasih, anda sudah mengerjakan tugas " + data.mapel.Nama.ToLower() + " dengan baik!",
                                TipeInboxConstant.RuangBelajar,
                                null
                            );
                        }
                        else
                        {
                            conn.Update(tbKbmJawaban);
                        }

                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UploadJawaban", ex);

            }
        }

        private string UpdateNilai(IDbConnection conn, int IdPegawai, int IdKbmMateri, int IdSiswa, double NilaiAngka, string NilaiHuruf, int IsRemedial, string Catatan, int Hadir)
        {
            try
            {
                var tbKbmJawaban = (from a in conn.GetList<TbKbmJawaban>()
                                    where a.IdKbmMateri == IdKbmMateri
                                    & a.IdSiswa == IdSiswa
                                    select a).FirstOrDefault();
                //tidak perlu melakukan pengecekan jawaban
                //guru memberi nilai walaupun siswa tidak upload jawaban
                bool isInsert = false;
                if (tbKbmJawaban == null)
                {
                    tbKbmJawaban = new TbKbmJawaban
                    {
                        IdSiswa = IdSiswa,
                        IdKbmMateri = IdKbmMateri,
                        CreatedBy = IdPegawai,
                        CreatedDate = dateTimeService.GetCurrdate(),
                        IdJenisBantuanOrtu = JenisKbmTugasBantuanOrtuConstant.Mandiri,
                        IdJenisSoal = JenisKbmSoalConstant.Reguler,
                        Hadir = Hadir
                    };
                    isInsert = true;
                }
                else
                {
                    tbKbmJawaban.Hadir = Hadir;
                    tbKbmJawaban.UpdatedBy = IdPegawai;
                    tbKbmJawaban.UpdatedDate = dateTimeService.GetCurrdate();
                }
                // if (NilaiAngka <= 0 & string.IsNullOrEmpty(NilaiHuruf))
                // if (string.IsNullOrEmpty(NilaiHuruf))
                //     return "nilai harus diisi";

                if (!string.IsNullOrEmpty(NilaiHuruf))
                {
                    tbKbmJawaban.NilaiHuruf = NilaiHuruf;
                }
                else
                {
                    tbKbmJawaban.NilaiAngka = NilaiAngka;
                }
                tbKbmJawaban.Catatan = Catatan;
                tbKbmJawaban.IsRemedial = IsRemedial;
                if (IsRemedial == 1)
                {
                    tbKbmJawaban.IdJenisSoal = JenisKbmSoalConstant.Remedial;
                }
                else
                {
                    tbKbmJawaban.IdJenisSoal = JenisKbmSoalConstant.Reguler;
                }


                var data = (from materi in conn.GetList<TbKbmMateri>()
                            join mapel in conn.GetList<TbMapel>() on materi.IdMapel equals mapel.IdMapel
                            join kelasParalel in conn.GetList<TbKelasParalel>() on materi.IdKelasParalel equals kelasParalel.IdKelasParalel
                            join rombel in conn.GetList<TbKelasRombel>() on kelasParalel.IdKelasParalel equals rombel.IdKelasParalel
                            join siswa in conn.GetList<TbSiswa>() on rombel.IdSiswa equals siswa.IdSiswa
                            where materi.IdKbmMateri == IdKbmMateri & siswa.IdSiswa == IdSiswa
                            select new { materi, mapel, kelasParalel, rombel, siswa }).FirstOrDefault();
                if (data == null)
                {
                    return string.Format("Ananda {0} tidak ada di kelas {1} untuk mata pelajaran {2}", data.siswa.Nama, data.kelasParalel.Nama, data.mapel.Nama);
                }

                if (isInsert)
                {
                    conn.Insert(tbKbmJawaban);
                }
                else
                {
                    conn.Update(tbKbmJawaban);
                }
                siswaService.PushInboxNotif(
                    IdSiswa,
                    "Tugas Sudah Dinilai (" + data.mapel.Nama + ")",
                    "(Jam : " + dateTimeService.GetCurrdate().ToString("HH:mm") + ") Yey, tugas " + data.mapel.Nama.ToLower() + " sudah dinilai!",
                    TipeInboxConstant.RuangBelajar,
                    null
                );
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UpdateNilai", ex);

            }
        }
        public string UpdateNilai(int IdPegawai, int IdKbmMateri, int IdSiswa, double NilaiAngka, string NilaiHuruf, int IsRemedial, string Catatan)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string err = UpdateNilai(conn, IdPegawai, IdKbmMateri, IdSiswa, NilaiAngka, NilaiHuruf, IsRemedial, Catatan, 1);
                        if (!string.IsNullOrEmpty(err)) return err;
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UpdateNilai", ex);
            }
        }
        public string UpdateAdbsensi(int IdPegawai, int IdKbmMateri, string NamaUrl, List<int> IdSiswas)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbKbmMateri = conn.Get<TbKbmMateri>(IdKbmMateri);
                        if (tbKbmMateri == null) return "data tidak ada";
                        tbKbmMateri.NamaUrlMeeting = NamaUrl;
                        tbKbmMateri.UpdatedBy = IdPegawai;
                        tbKbmMateri.UpdatedDate = dateTimeService.GetCurrdate();
                        conn.Update(tbKbmMateri);

                        for (int i = 0; i < IdSiswas.Count; i++)
                        {
                            var tbKbmJawaban = (from a in conn.GetList<TbKbmJawaban>()
                                                where a.IdKbmMateri == IdKbmMateri
                                                & a.IdSiswa == IdSiswas[i]
                                                select a).FirstOrDefault();
                            if (tbKbmJawaban != null)
                            {
                                tbKbmJawaban.Hadir = -1;
                                conn.Update(tbKbmJawaban);
                            }
                            else
                            {
                                tbKbmJawaban = new TbKbmJawaban
                                {
                                    IdSiswa = IdSiswas[i],
                                    IdJenisSoal = JenisKbmSoalConstant.Reguler,
                                    IdJenisBantuanOrtu = JenisKbmTugasBantuanOrtuConstant.Mandiri,
                                    IsRemedial = 0,
                                    IdKbmMateri = IdKbmMateri,
                                    Hadir = -1,
                                };
                                conn.Insert(tbKbmJawaban);
                            }
                        }
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UpdateAdbsensi", ex);
            }
        }
        public string UploadNilaiSiswa(int IdPegawai, List<SheetNilaiSiswa> Data)
        {
            try
            {
                if (Data == null) return "data upload kosong";

                int idKbmMater = Data[0].IdKBmMateri;
                string JenisNilai = Data[0].JenisNilai;
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string err = string.Empty;
                        if (!string.IsNullOrEmpty(Data[0].NamaUrlMeeting))
                        {
                            var tbKbmMateri = conn.Get<TbKbmMateri>(idKbmMater);
                            if (tbKbmMateri == null) return "data kbm materi tidak ada";
                            tbKbmMateri.NamaUrlMeeting = Data[0].NamaUrlMeeting;
                            conn.Update(tbKbmMateri);
                        }
                        foreach (var item in Data)
                        {
                            string tempErr = string.Empty;
                            var tbSiswa = (from a in conn.GetList<TbSiswa>()
                                           where a.Nis == item.Nis
                                           select a).FirstOrDefault();
                            if (tbSiswa == null)
                            {
                                tempErr = string.Format("nis {0} siswa tidak terdaftar", item.Nis) + Environment.NewLine;
                                continue;
                            }
                            if (item.JenisNilai.ToLower() == "huruf")
                            {
                                if (string.IsNullOrEmpty(item.NilaiHuruf))
                                {
                                    tempErr = string.Format("nis {0} siswa, nilai huruf salah", item.Nis) + Environment.NewLine;
                                    continue;
                                }
                            }
                            int isRemedial = 0;
                            if (!string.IsNullOrEmpty(item.Hasil))
                            {
                                if (item.Hasil.ToLower() == "remedial")
                                {
                                    isRemedial = 1;
                                }
                            }
                            int hadir = 1;
                            if (!string.IsNullOrEmpty(item.Hadir))
                            {
                                if (item.Hadir.ToLower() == "t")
                                {
                                    hadir = -1;
                                }
                            }

                            tempErr = UpdateNilai(conn, IdPegawai, idKbmMater, tbSiswa.IdSiswa, item.NilaiAngka, item.NilaiHuruf, isRemedial, item.Catatan, hadir);
                            if (!string.IsNullOrEmpty(tempErr)) err += tempErr;
                        }
                        if (!string.IsNullOrEmpty(err)) return err;
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UploadNilaiSiswa", ex);
            }
        }
        public string UploadJawabanSusulan(int IdPegawai, int IdSiswa, int IdKbmMateri, string Catatan, IFormFile FileJawaban)
        {
            try
            {
                if (FileJawaban == null) return "jawaban harus diupload";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbKbmJawabanSusulan = (from a in conn.GetList<TbKbmJawabanSusulan>()
                                                   where a.IdKbmMateri == IdKbmMateri
                                                   & a.IdSiswa == IdSiswa
                                                   select a).FirstOrDefault();
                        if (tbKbmJawabanSusulan != null)
                            conn.Delete(tbKbmJawabanSusulan);
                        string NamaFile = string.Format("susulan_{0}_{1}{2}", IdKbmMateri.ToString(), IdSiswa.ToString(), Path.GetExtension(FileJawaban.FileName));

                        if (File.Exists(Path.Combine(AppSetting.PathElearning, NamaFile)))
                            File.Delete(Path.Combine(AppSetting.PathElearning, NamaFile));
                        using (var fileStream = new FileStream(Path.Combine(AppSetting.PathElearning, NamaFile), FileMode.Create))
                        {
                            FileJawaban.CopyTo(fileStream);
                        }
                        tbKbmJawabanSusulan = new TbKbmJawabanSusulan
                        {
                            IdKbmMateri = IdKbmMateri,
                            IdSiswa = IdSiswa,
                            Catatan = Catatan,
                            CreatedBy = IdPegawai,
                            CreatedDate = dateTimeService.GetCurrdate(),
                            NamaFile = NamaFile,
                            UpdatedBy = IdPegawai,
                        };
                        conn.Insert(tbKbmJawabanSusulan);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UploadJawabanSusulan", ex);
            }
        }

        #region Evaluasi Harian Siswa
        public string DeleteJenisEvaluasiHarian(int IdJenisEvaluasiHarian)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbJenisEvaluasiHarian = conn.Get<TbJenisEvaluasiHarian>(IdJenisEvaluasiHarian);
                        if (tbJenisEvaluasiHarian == null)
                        {
                            return "data tidak ditemukan!";
                        }
                        conn.Delete(tbJenisEvaluasiHarian);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteJenisEvaluasiHarian", ex);
            }
        }
        public string JenisEvaluasiHarianAddEdit(EvalHarianModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {

                        bool isInsert = false;
                        var tbJenisEvaluasiHarian = conn.Get<TbJenisEvaluasiHarian>(Data.IdJenisEvaluasiHarian);
                        if (tbJenisEvaluasiHarian == null)
                        {
                            isInsert = true;
                            tbJenisEvaluasiHarian = new TbJenisEvaluasiHarian();
                        }

                        tbJenisEvaluasiHarian.Nama = Data.Nama;
                        tbJenisEvaluasiHarian.NamaSingkat = Data.NamaSingkat;
                        //tbJenisEvaluasiHarian.IdUnit = Data.IdUnit;
                        if (isInsert)
                        {
                            var maxJenisEvaluasiHarians = (from a in conn.GetList<TbJenisEvaluasiHarian>()
                                                           select new { a.IdJenisEvaluasiHarian }).OrderByDescending(g => g.IdJenisEvaluasiHarian).FirstOrDefault();
                            tbJenisEvaluasiHarian.IdJenisEvaluasiHarian = maxJenisEvaluasiHarians.IdJenisEvaluasiHarian + 1;
                            conn.Insert(tbJenisEvaluasiHarian);
                        }
                        else
                        {
                            tbJenisEvaluasiHarian.IdJenisEvaluasiHarian = Data.IdJenisEvaluasiHarian;
                            conn.Update(tbJenisEvaluasiHarian);
                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "JenisEvaluasiHarianAddEdit", ex);
            }
        }
        public string AddEvalHarianUnit(int IdUnit, List<int> IdJenisEvaluasiHarians)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var oldEvalHarianUnit = from a in conn.GetList<TbJenisEvaluasiHarianUnit>()
                                                where a.IdUnit == IdUnit
                                                select a;
                        foreach (var item in oldEvalHarianUnit)
                        {
                            conn.Delete(item);
                        }
                        if (IdJenisEvaluasiHarians != null & IdJenisEvaluasiHarians.Count() > 0)
                        {
                            foreach (var item in IdJenisEvaluasiHarians)
                            {
                                TbJenisEvaluasiHarianUnit tbEvalHarianUnit = new TbJenisEvaluasiHarianUnit { IdUnit = IdUnit, IdJenisEvaluasiHarian = item };
                                conn.Insert(tbEvalHarianUnit);
                            }
                        }
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddTasksRole", ex);
            }
        }
        public EvalHarianUnitModel GetJenisEvaluasiHariansByUnit(int IdUnit, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                oMessage = string.Empty;
                EvalHarianUnitModel ret = new EvalHarianUnitModel();
                ret.AssignEvals = new List<EvalHarianModel>();
                ret.NoAssignEvals = new List<EvalHarianModel>();
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    var unit = conn.Get<TbUnit>(IdUnit);
                    if (unit == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    ret.IdUnit = IdUnit;
                    ret.NamaUnit = unit.Nama;

                    var assignEvals = from a in conn.GetList<TbJenisEvaluasiHarian>()
                                      join b in conn.GetList<TbJenisEvaluasiHarianUnit>() on a.IdJenisEvaluasiHarian equals b.IdJenisEvaluasiHarian
                                      join c in conn.GetList<TbUnit>() on b.IdUnit equals c.IdUnit
                                      where b.IdUnit == IdUnit
                                      select new { a, b, c };
                    foreach (var item in assignEvals)
                    {
                        EvalHarianModel m = new EvalHarianModel
                        {
                            IdJenisEvaluasiHarian = item.a.IdJenisEvaluasiHarian,
                            Nama = item.a.Nama,
                            NamaSingkat = item.a.NamaSingkat
                        };
                        ret.AssignEvals.Add(m);
                    }


                    var noassignEvals = from a in conn.GetList<TbJenisEvaluasiHarian>()
                                        where !(from d in assignEvals
                                                select d.a.IdJenisEvaluasiHarian
                                              ).Contains(a.IdJenisEvaluasiHarian)
                                        select a;

                    foreach (var item in noassignEvals.OrderBy(x => x.IdJenisEvaluasiHarian))
                    {
                        EvalHarianModel m = new EvalHarianModel
                        {
                            IdJenisEvaluasiHarian = item.IdJenisEvaluasiHarian,
                            Nama = item.Nama,
                            NamaSingkat = item.NamaSingkat
                        };
                        ret.NoAssignEvals.Add(m);
                    }
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisEvaluasiHariansByUnit", ex);
                return null;
            }
        }
        public List<EvalHarianModel> GetJenisEvaluasiHarians(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbJenisEvaluasiHarian>();
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; };
                    List<EvalHarianModel> ret = new List<EvalHarianModel>();
                    foreach (var item in data.OrderBy(x => x.IdJenisEvaluasiHarian))
                    {
                        ret.Add(new EvalHarianModel
                        {
                            IdJenisEvaluasiHarian = item.IdJenisEvaluasiHarian,
                            Nama = item.Nama,
                            NamaSingkat = item.NamaSingkat
                        });
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisEvaluasiHarians", ex);
                return null;
            }
        }
        public List<ComboModel> GetJenisEvaluasiHarian(int IdSiswa, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbUnitSiswa = (from a in conn.GetList<TbKelas>()
                                       join b in conn.GetList<TbKelasParalel>() on a.IdKelas equals b.IdKelas
                                       join c in conn.GetList<TbKelasRombel>() on b.IdKelasParalel equals c.IdKelasParalel
                                       join d in conn.GetList<TbUnit>() on a.IdUnit equals d.IdUnit
                                       where c.IdSiswa == IdSiswa
                                       select new { a, b, c, d }).FirstOrDefault();


                    var data = from a in conn.GetList<TbJenisEvaluasiHarian>()
                               join b in conn.GetList<TbJenisEvaluasiHarianUnit>() on a.IdJenisEvaluasiHarian equals b.IdJenisEvaluasiHarian
                               join c in conn.GetList<TbUnit>() on b.IdUnit equals c.IdUnit
                               where b.IdUnit == tbUnitSiswa.d.IdUnit
                               select new { a, b, c };
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; };
                    List<ComboModel> ret = new List<ComboModel>();
                    foreach (var item in data)
                    {
                        ret.Add(new ComboModel
                        {
                            Id = item.a.IdJenisEvaluasiHarian,
                            Nama = item.a.Nama,
                            NamaSingkat = item.a.NamaSingkat
                        });
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisEvaluasiHarian", ex);
                return null;
            }
        }
        private TbKbmEvaluasiHarian GetEvaluasiHarian(List<TbKbmEvaluasiHarian> Data, DateTime Tanggal, int IdJenisEvaluasiHarian)
        {
            return (from a in Data
                    where a.IdJenisEvaluasiHarian == IdJenisEvaluasiHarian
                    & a.Tanggal.Date == Tanggal.Date
                    select a).FirstOrDefault();
        }
        public List<KbmEvalHarianListModel> GetKbmEvalHarians(int IdSiswa, string Tanggal, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                DateTime currDate = DateTime.Now;
                DateTime NextFirstDate = dateTimeService.GetFirstDate(currDate.AddDays(7));
                if (!string.IsNullOrEmpty(Tanggal))
                {
                    currDate = dateTimeService.StrToDateTime(Tanggal);
                }
                if (currDate >= NextFirstDate)
                {
                    oMessage = string.Format("kbm belum aktif untuk tanggal {0}", dateTimeService.DateToLongString(_languageCode, currDate));
                    return null;
                }

                DateTime minDate = dateTimeService.GetFirstDate(currDate);
                DateTime maxDate = dateTimeService.GetLastDate(currDate);
                List<KbmEvalHarianListModel> ret = new List<KbmEvalHarianListModel>();
                using (var conn = commonService.DbConnection())
                {
                    var myEvals = (from a in conn.GetList<TbKbmEvaluasiHarian>()
                                   where a.IdSiswa == IdSiswa
                                   & a.Tanggal.Date >= minDate.Date & a.Tanggal <= maxDate.Date
                                   select a);

                    var tbUnitSiswa = (from a in conn.GetList<TbKelas>()
                                       join b in conn.GetList<TbKelasParalel>() on a.IdKelas equals b.IdKelas
                                       join c in conn.GetList<TbKelasRombel>() on b.IdKelasParalel equals c.IdKelasParalel
                                       join d in conn.GetList<TbUnit>() on a.IdUnit equals d.IdUnit
                                       where c.IdSiswa == IdSiswa
                                       select new { a, b, c, d }).FirstOrDefault();

                    var tbJenisEvaluasiHarians = from a in conn.GetList<TbJenisEvaluasiHarian>()
                                                 join b in conn.GetList<TbJenisEvaluasiHarianUnit>() on a.IdJenisEvaluasiHarian equals b.IdJenisEvaluasiHarian
                                                 where b.IdUnit == tbUnitSiswa.d.IdUnit
                                                 select a;
                    foreach (var item in tbJenisEvaluasiHarians.OrderBy(x => x.IdJenisEvaluasiHarian))
                    {
                        KbmEvalHarianListModel m = new KbmEvalHarianListModel
                        {
                            JenisEvaluasi = item.Nama,
                            IdJenisEvaluasi = item.IdJenisEvaluasiHarian,
                            HariKe1Tanggal = minDate.ToString("dd-MM-yyyy"),
                            HariKe2Tanggal = minDate.AddDays(1).ToString("dd-MM-yyyy"),
                            HariKe3Tanggal = minDate.AddDays(2).ToString("dd-MM-yyyy"),
                            HariKe4Tanggal = minDate.AddDays(3).ToString("dd-MM-yyyy"),
                            HariKe5Tanggal = minDate.AddDays(4).ToString("dd-MM-yyyy"),
                            HariKe6Tanggal = minDate.AddDays(5).ToString("dd-MM-yyyy"),
                            HariKe7Tanggal = minDate.AddDays(6).ToString("dd-MM-yyyy")
                        };
                        if (myEvals != null & myEvals.Count() > 0)
                        {
                            TbKbmEvaluasiHarian evaluasi = GetEvaluasiHarian(myEvals.ToList(), minDate, item.IdJenisEvaluasiHarian);
                            m.HariKe1 = evaluasi == null ? string.Empty : evaluasi.IsDone;

                            evaluasi = GetEvaluasiHarian(myEvals.ToList(), minDate.AddDays(1), item.IdJenisEvaluasiHarian);
                            m.HariKe2 = evaluasi == null ? string.Empty : evaluasi.IsDone;
                            evaluasi = GetEvaluasiHarian(myEvals.ToList(), minDate.AddDays(2), item.IdJenisEvaluasiHarian);
                            m.HariKe3 = evaluasi == null ? string.Empty : evaluasi.IsDone;

                            evaluasi = GetEvaluasiHarian(myEvals.ToList(), minDate.AddDays(3), item.IdJenisEvaluasiHarian);
                            m.HariKe4 = evaluasi == null ? string.Empty : evaluasi.IsDone;

                            evaluasi = GetEvaluasiHarian(myEvals.ToList(), minDate.AddDays(4), item.IdJenisEvaluasiHarian);
                            m.HariKe5 = evaluasi == null ? string.Empty : evaluasi.IsDone;

                            evaluasi = GetEvaluasiHarian(myEvals.ToList(), minDate.AddDays(5), item.IdJenisEvaluasiHarian);
                            m.HariKe6 = evaluasi == null ? string.Empty : evaluasi.IsDone;

                            evaluasi = GetEvaluasiHarian(myEvals.ToList(), minDate.AddDays(6), item.IdJenisEvaluasiHarian);
                            m.HariKe7 = evaluasi == null ? string.Empty : evaluasi.IsDone;

                            m.IdJenisEvaluasi = item.IdJenisEvaluasiHarian;
                            m.HariKe1Tanggal = minDate.ToString("dd-MM-yyyy");
                            m.HariKe2Tanggal = minDate.AddDays(1).ToString("dd-MM-yyyy");
                            m.HariKe3Tanggal = minDate.AddDays(2).ToString("dd-MM-yyyy");
                            m.HariKe4Tanggal = minDate.AddDays(3).ToString("dd-MM-yyyy");
                            m.HariKe5Tanggal = minDate.AddDays(4).ToString("dd-MM-yyyy");
                            m.HariKe6Tanggal = minDate.AddDays(5).ToString("dd-MM-yyyy");
                            m.HariKe7Tanggal = minDate.AddDays(6).ToString("dd-MM-yyyy");
                        }

                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKbmEvalHarians", ex);
                return null;

            }
        }
        public KbmEvalHarianModel GetKbmEvalHarian(int IdSiswa, string Tanggal, int IdJenisEvaluasiHarian, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                DateTime currDate = DateTime.Now;
                using (var conn = commonService.DbConnection())
                {
                    if (!string.IsNullOrEmpty(Tanggal))
                        currDate = dateTimeService.StrToDateTime(Tanggal);

                    var item = (from a in conn.GetList<TbKbmEvaluasiHarian>()
                                where a.IdSiswa == IdSiswa
                                & a.IdJenisEvaluasiHarian == IdJenisEvaluasiHarian
                                & a.Tanggal.Date == currDate.Date
                                select a).FirstOrDefault();
                    if (item == null) { oMessage = "data tidak ada"; return null; }
                    return new KbmEvalHarianModel
                    {
                        IsDone = item.IsDone,
                        Catatan = item.Catatan
                    };
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKbmEvalHarian", ex);
                return null;
            }
        }
        public string EvaluasiHarianAdd(int IdSiswa, List<KbmEvalHarianAddModel> Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        foreach (var item in Data)
                        {
                            switch (item.IsDone)
                            {
                                case "Y":
                                case "T":
                                    break;
                                default:
                                    return "IsDone harus Y atau T";
                            }
                            //if (string.IsNullOrEmpty(item.Catatan)) return "catatan harus diisi";

                            var tbSiswa = conn.Get<TbSiswa>(IdSiswa);
                            if (tbSiswa == null) return "data siswa tidak ada";
                            var tbJenisEvaluasiHarian = conn.Get<TbJenisEvaluasiHarian>(item.IdJenisEvaluasiHarian);
                            if (tbJenisEvaluasiHarian == null) return "jenis evaluasi tidak ada";
                            bool isInsert = false;
                            DateTime currDate = DateTime.Now;
                            DateTime minDate = dateTimeService.GetFirstDate(currDate);
                            DateTime maxDate = dateTimeService.GetLastDate(currDate);

                            if (!string.IsNullOrEmpty(item.Tanggal))
                                currDate = dateTimeService.StrToDateTime(item.Tanggal);
                            if (!string.IsNullOrEmpty(item.Tanggal))
                            {
                                currDate = dateTimeService.StrToDateTime(item.Tanggal);
                            }
                            if (currDate.Date > maxDate.Date)
                            {
                                return string.Format("kbm belum aktif untuk tanggal {0}", dateTimeService.DateToLongString(_languageCode, currDate));
                            }
                            if (currDate.Date < minDate.Date)
                            {
                                return string.Format("kbm sudah tidak aktif untuk tanggal {0}", dateTimeService.DateToLongString(_languageCode, currDate));
                            }

                            var tbKbmEvaluasiHarian = (from a in conn.GetList<TbKbmEvaluasiHarian>()
                                                       where a.IdSiswa == IdSiswa
                                                       & a.IdJenisEvaluasiHarian == item.IdJenisEvaluasiHarian
                                                       & a.Tanggal.Date == currDate.Date
                                                       select a).FirstOrDefault();

                            if (tbKbmEvaluasiHarian == null)
                            {
                                isInsert = true;
                                tbKbmEvaluasiHarian = new TbKbmEvaluasiHarian();
                            }
                            tbKbmEvaluasiHarian.IdSiswa = IdSiswa;
                            tbKbmEvaluasiHarian.IdJenisEvaluasiHarian = item.IdJenisEvaluasiHarian;
                            tbKbmEvaluasiHarian.Tanggal = currDate;
                            tbKbmEvaluasiHarian.IsDone = item.IsDone;
                            tbKbmEvaluasiHarian.Catatan = item.Catatan;
                            if (isInsert)
                            {
                                conn.Insert(tbKbmEvaluasiHarian);
                            }
                            else
                            {
                                conn.Update(tbKbmEvaluasiHarian);
                            }

                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }

            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EvaluasiHarianAdd", ex);
            }
        }
        #endregion Evaluasi Harian Siswa

        #region Evaluasi Harian Pegawai
        public string DeleteJenisEvaluasiHarianPegawai(int IdJenisEvaluasiHarianPegawai)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbJenisEvaluasiHarianPegawai = conn.Get<TbJenisEvaluasiHarianPegawai>(IdJenisEvaluasiHarianPegawai);
                        if (tbJenisEvaluasiHarianPegawai == null)
                        {
                            return "data tidak ditemukan!";
                        }
                        conn.Delete(tbJenisEvaluasiHarianPegawai);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteJenisEvaluasiHarianPegawai", ex);
            }
        }
        public string JenisEvaluasiHarianAddEditPegawai(EvalHarianPegawaiModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {

                        bool isInsert = false;
                        var tbJenisEvaluasiHarianPegawai = conn.Get<TbJenisEvaluasiHarianPegawai>(Data.IdJenisEvaluasiHarianPegawai);
                        if (tbJenisEvaluasiHarianPegawai == null)
                        {
                            isInsert = true;
                            tbJenisEvaluasiHarianPegawai = new TbJenisEvaluasiHarianPegawai();
                        }

                        tbJenisEvaluasiHarianPegawai.Nama = Data.Nama;
                        tbJenisEvaluasiHarianPegawai.NamaSingkat = Data.NamaSingkat;
                        //tbJenisEvaluasiHarian.IdUnit = Data.IdUnit;
                        if (isInsert)
                        {
                            var maxJenisEvaluasiHarianPegawais = (from a in conn.GetList<TbJenisEvaluasiHarianPegawai>()
                                                                  select new { a.IdJenisEvaluasiHarianPegawai }).OrderByDescending(g => g.IdJenisEvaluasiHarianPegawai).FirstOrDefault();
                            if (maxJenisEvaluasiHarianPegawais == null)
                            {
                                tbJenisEvaluasiHarianPegawai.IdJenisEvaluasiHarianPegawai = 1;
                            }
                            else
                            {
                                tbJenisEvaluasiHarianPegawai.IdJenisEvaluasiHarianPegawai = maxJenisEvaluasiHarianPegawais.IdJenisEvaluasiHarianPegawai + 1;
                            }
                            conn.Insert(tbJenisEvaluasiHarianPegawai);
                        }
                        else
                        {
                            tbJenisEvaluasiHarianPegawai.IdJenisEvaluasiHarianPegawai = Data.IdJenisEvaluasiHarianPegawai;
                            conn.Update(tbJenisEvaluasiHarianPegawai);
                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "JenisEvaluasiHarianAddEditPegawai", ex);
            }
        }
        public string AddEvalHarianUnitPegawai(int IdUnit, List<int> IdJenisEvaluasiHarianPegawais)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var oldEvalHarianUnitPegawai = from a in conn.GetList<TbJenisEvaluasiHarianUnitPegawai>()
                                                       where a.IdUnit == IdUnit
                                                       select a;
                        foreach (var item in oldEvalHarianUnitPegawai)
                        {
                            conn.Delete(item);
                        }
                        if (IdJenisEvaluasiHarianPegawais != null & IdJenisEvaluasiHarianPegawais.Count() > 0)
                        {
                            foreach (var item in IdJenisEvaluasiHarianPegawais)
                            {
                                TbJenisEvaluasiHarianUnitPegawai tbEvalHarianUnitPegawai = new TbJenisEvaluasiHarianUnitPegawai { IdUnit = IdUnit, IdJenisEvaluasiHarianPegawai = item };
                                conn.Insert(tbEvalHarianUnitPegawai);
                            }
                        }
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddEvalHarianUnitPegawai", ex);
            }
        }
        public EvalHarianUnitPegawaiModel GetJenisEvaluasiHariansByUnitPegawai(int IdUnit, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                oMessage = string.Empty;
                EvalHarianUnitPegawaiModel ret = new EvalHarianUnitPegawaiModel();
                ret.AssignEvals = new List<EvalHarianPegawaiModel>();
                ret.NoAssignEvals = new List<EvalHarianPegawaiModel>();
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    var unit = conn.Get<TbUnit>(IdUnit);
                    if (unit == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    ret.IdUnit = IdUnit;
                    ret.NamaUnit = unit.Nama;

                    var assignEvals = from a in conn.GetList<TbJenisEvaluasiHarianPegawai>()
                                      join b in conn.GetList<TbJenisEvaluasiHarianUnitPegawai>() on a.IdJenisEvaluasiHarianPegawai equals b.IdJenisEvaluasiHarianPegawai
                                      join c in conn.GetList<TbUnit>() on b.IdUnit equals c.IdUnit
                                      where b.IdUnit == IdUnit
                                      select new { a, b, c };
                    foreach (var item in assignEvals)
                    {
                        EvalHarianPegawaiModel m = new EvalHarianPegawaiModel
                        {
                            IdJenisEvaluasiHarianPegawai = item.a.IdJenisEvaluasiHarianPegawai,
                            Nama = item.a.Nama,
                            NamaSingkat = item.a.NamaSingkat
                        };
                        ret.AssignEvals.Add(m);
                    }


                    var noassignEvals = from a in conn.GetList<TbJenisEvaluasiHarianPegawai>()
                                        where !(from d in assignEvals
                                                select d.a.IdJenisEvaluasiHarianPegawai
                                              ).Contains(a.IdJenisEvaluasiHarianPegawai)
                                        select a;

                    foreach (var item in noassignEvals.OrderBy(x => x.IdJenisEvaluasiHarianPegawai))
                    {
                        EvalHarianPegawaiModel m = new EvalHarianPegawaiModel
                        {
                            IdJenisEvaluasiHarianPegawai = item.IdJenisEvaluasiHarianPegawai,
                            Nama = item.Nama,
                            NamaSingkat = item.NamaSingkat
                        };
                        ret.NoAssignEvals.Add(m);
                    }
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisEvaluasiHariansByUnitPegawai", ex);
                return null;
            }
        }
        public List<EvalHarianPegawaiModel> GetJenisEvaluasiHarianPegawais(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbJenisEvaluasiHarianPegawai>();
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; };
                    List<EvalHarianPegawaiModel> ret = new List<EvalHarianPegawaiModel>();
                    foreach (var item in data.OrderBy(x => x.IdJenisEvaluasiHarianPegawai))
                    {
                        ret.Add(new EvalHarianPegawaiModel
                        {
                            IdJenisEvaluasiHarianPegawai = item.IdJenisEvaluasiHarianPegawai,
                            Nama = item.Nama,
                            NamaSingkat = item.NamaSingkat
                        });
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisEvaluasiHarianPegawais", ex);
                return null;
            }
        }
        public List<ComboModel> GetJenisEvaluasiHarianPegawai(int IdPegawai, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbUnitPegawai = (from a in conn.GetList<TbPegawai>()
                                         join b in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals b.IdPegawai
                                         join c in conn.GetList<TbUnit>() on b.IdUnit equals c.IdUnit
                                         where b.IdPegawai == IdPegawai
                                         select new { a, b, c }).FirstOrDefault();


                    var data = from a in conn.GetList<TbJenisEvaluasiHarianPegawai>()
                               join b in conn.GetList<TbJenisEvaluasiHarianUnitPegawai>() on a.IdJenisEvaluasiHarianPegawai equals b.IdJenisEvaluasiHarianPegawai
                               join c in conn.GetList<TbUnit>() on b.IdUnit equals c.IdUnit
                               where b.IdUnit == tbUnitPegawai.c.IdUnit
                               select new { a, b, c };
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; };
                    List<ComboModel> ret = new List<ComboModel>();
                    foreach (var item in data)
                    {
                        ret.Add(new ComboModel
                        {
                            Id = item.a.IdJenisEvaluasiHarianPegawai,
                            Nama = item.a.Nama,
                            NamaSingkat = item.a.NamaSingkat
                        });
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisEvaluasiHarianPegawai", ex);
                return null;
            }
        }
        private TbKbmEvaluasiHarianPegawai GetEvaluasiHarianPegawai(List<TbKbmEvaluasiHarianPegawai> Data, DateTime Tanggal, int IdJenisEvaluasiHarianPegawai)
        {
            return (from a in Data
                    where a.IdJenisEvaluasiHarianPegawai == IdJenisEvaluasiHarianPegawai
                    & a.Tanggal.Date == Tanggal.Date
                    select a).FirstOrDefault();
        }
        public List<KbmEvalHarianListPegawaiModel> GetKbmEvalHarianPegawais(int IdPegawai, string Tanggal, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                DateTime currDate = DateTime.Now;
                DateTime NextFirstDate = dateTimeService.GetFirstDate(currDate.AddDays(7));
                if (!string.IsNullOrEmpty(Tanggal))
                {
                    currDate = dateTimeService.StrToDateTime(Tanggal);
                }
                if (currDate >= NextFirstDate)
                {
                    oMessage = string.Format("kbm belum aktif untuk tanggal {0}", dateTimeService.DateToLongString(_languageCode, currDate));
                    return null;
                }

                DateTime minDate = dateTimeService.GetFirstDate(currDate);
                DateTime maxDate = dateTimeService.GetLastDate(currDate);
                List<KbmEvalHarianListPegawaiModel> ret = new List<KbmEvalHarianListPegawaiModel>();
                using (var conn = commonService.DbConnection())
                {
                    var myEvals = (from a in conn.GetList<TbKbmEvaluasiHarianPegawai>()
                                   where a.IdPegawai == IdPegawai
                                   & a.Tanggal.Date >= minDate.Date & a.Tanggal <= maxDate.Date
                                   select a);

                    var tbUnitPegawai = (from a in conn.GetList<TbPegawai>()
                                         join b in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals b.IdPegawai
                                         join c in conn.GetList<TbUnit>() on b.IdUnit equals c.IdUnit
                                         where b.IdPegawai == IdPegawai
                                         select new { a, b, c }).FirstOrDefault();

                    var tbJenisEvaluasiHarianPegawais = from a in conn.GetList<TbJenisEvaluasiHarianPegawai>()
                                                        join b in conn.GetList<TbJenisEvaluasiHarianUnitPegawai>() on a.IdJenisEvaluasiHarianPegawai equals b.IdJenisEvaluasiHarianPegawai
                                                        where b.IdUnit == tbUnitPegawai.c.IdUnit
                                                        select a;
                    foreach (var item in tbJenisEvaluasiHarianPegawais.OrderBy(x => x.Nama))
                    {
                        KbmEvalHarianListPegawaiModel m = new KbmEvalHarianListPegawaiModel
                        {
                            JenisEvaluasiPegawai = item.Nama,
                            IdJenisEvaluasiPegawai = item.IdJenisEvaluasiHarianPegawai,
                            HariKe1Tanggal = minDate.ToString("dd-MM-yyyy"),
                            HariKe2Tanggal = minDate.AddDays(1).ToString("dd-MM-yyyy"),
                            HariKe3Tanggal = minDate.AddDays(2).ToString("dd-MM-yyyy"),
                            HariKe4Tanggal = minDate.AddDays(3).ToString("dd-MM-yyyy"),
                            HariKe5Tanggal = minDate.AddDays(4).ToString("dd-MM-yyyy"),
                            HariKe6Tanggal = minDate.AddDays(5).ToString("dd-MM-yyyy"),
                            HariKe7Tanggal = minDate.AddDays(6).ToString("dd-MM-yyyy")
                        };
                        if (myEvals != null & myEvals.Count() > 0)
                        {
                            TbKbmEvaluasiHarianPegawai evaluasi = GetEvaluasiHarianPegawai(myEvals.ToList(), minDate, item.IdJenisEvaluasiHarianPegawai);
                            m.HariKe1 = evaluasi == null ? string.Empty : evaluasi.IsDone;

                            evaluasi = GetEvaluasiHarianPegawai(myEvals.ToList(), minDate.AddDays(1), item.IdJenisEvaluasiHarianPegawai);
                            m.HariKe2 = evaluasi == null ? string.Empty : evaluasi.IsDone;
                            evaluasi = GetEvaluasiHarianPegawai(myEvals.ToList(), minDate.AddDays(2), item.IdJenisEvaluasiHarianPegawai);
                            m.HariKe3 = evaluasi == null ? string.Empty : evaluasi.IsDone;

                            evaluasi = GetEvaluasiHarianPegawai(myEvals.ToList(), minDate.AddDays(3), item.IdJenisEvaluasiHarianPegawai);
                            m.HariKe4 = evaluasi == null ? string.Empty : evaluasi.IsDone;

                            evaluasi = GetEvaluasiHarianPegawai(myEvals.ToList(), minDate.AddDays(4), item.IdJenisEvaluasiHarianPegawai);
                            m.HariKe5 = evaluasi == null ? string.Empty : evaluasi.IsDone;

                            evaluasi = GetEvaluasiHarianPegawai(myEvals.ToList(), minDate.AddDays(5), item.IdJenisEvaluasiHarianPegawai);
                            m.HariKe6 = evaluasi == null ? string.Empty : evaluasi.IsDone;

                            evaluasi = GetEvaluasiHarianPegawai(myEvals.ToList(), minDate.AddDays(6), item.IdJenisEvaluasiHarianPegawai);
                            m.HariKe7 = evaluasi == null ? string.Empty : evaluasi.IsDone;

                            m.IdJenisEvaluasiPegawai = item.IdJenisEvaluasiHarianPegawai;
                            m.HariKe1Tanggal = minDate.ToString("dd-MM-yyyy");
                            m.HariKe2Tanggal = minDate.AddDays(1).ToString("dd-MM-yyyy");
                            m.HariKe3Tanggal = minDate.AddDays(2).ToString("dd-MM-yyyy");
                            m.HariKe4Tanggal = minDate.AddDays(3).ToString("dd-MM-yyyy");
                            m.HariKe5Tanggal = minDate.AddDays(4).ToString("dd-MM-yyyy");
                            m.HariKe6Tanggal = minDate.AddDays(5).ToString("dd-MM-yyyy");
                            m.HariKe7Tanggal = minDate.AddDays(6).ToString("dd-MM-yyyy");
                        }

                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKbmEvalHarianPegawais", ex);
                return null;

            }
        }
        public KbmEvalHarianPegawaiModel GetKbmEvalHarianPegawai(int IdPegawai, string Tanggal, int IdJenisEvaluasiHarianPegawai, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                DateTime currDate = DateTime.Now;
                using (var conn = commonService.DbConnection())
                {
                    if (!string.IsNullOrEmpty(Tanggal))
                        currDate = dateTimeService.StrToDateTime(Tanggal);

                    var item = (from a in conn.GetList<TbKbmEvaluasiHarianPegawai>()
                                where a.IdPegawai == IdPegawai
                                & a.IdJenisEvaluasiHarianPegawai == IdJenisEvaluasiHarianPegawai
                                & a.Tanggal.Date == currDate.Date
                                select a).FirstOrDefault();
                    if (item == null) { oMessage = "data tidak ada"; return null; }
                    return new KbmEvalHarianPegawaiModel
                    {
                        IsDone = item.IsDone,
                        Catatan = item.Catatan
                    };
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKbmEvalHarianPegawai", ex);
                return null;
            }
        }
        public string EvaluasiHarianAddPegawai(int IdPegawai, List<KbmEvalHarianAddPegawaiModel> Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        foreach (var item in Data)
                        {
                            switch (item.IsDone)
                            {
                                case "Y":
                                case "T":
                                    break;
                                default:
                                    return "IsDone harus Y atau T";
                            }
                            //if (string.IsNullOrEmpty(item.Catatan)) return "catatan harus diisi";

                            var tbPegawai = conn.Get<TbPegawai>(IdPegawai);
                            if (tbPegawai == null) return "data siswa tidak ada";
                            var tbJenisEvaluasiHarianPegawai = conn.Get<TbJenisEvaluasiHarianPegawai>(item.IdJenisEvaluasiHarianPegawai);
                            if (tbJenisEvaluasiHarianPegawai == null) return "jenis evaluasi tidak ada";
                            bool isInsert = false;
                            DateTime currDate = DateTime.Now;
                            DateTime minDate = dateTimeService.GetFirstDate(currDate);
                            DateTime maxDate = dateTimeService.GetLastDate(currDate);

                            if (!string.IsNullOrEmpty(item.Tanggal))
                                currDate = dateTimeService.StrToDateTime(item.Tanggal);
                            if (!string.IsNullOrEmpty(item.Tanggal))
                            {
                                currDate = dateTimeService.StrToDateTime(item.Tanggal);
                            }
                            if (currDate.Date > maxDate.Date)
                            {
                                return string.Format("kbm belum aktif untuk tanggal {0}", dateTimeService.DateToLongString(_languageCode, currDate));
                            }
                            if (currDate.Date < minDate.Date)
                            {
                                return string.Format("kbm sudah tidak aktif untuk tanggal {0}", dateTimeService.DateToLongString(_languageCode, currDate));
                            }

                            var tbKbmEvaluasiHarianPegawai = (from a in conn.GetList<TbKbmEvaluasiHarianPegawai>()
                                                              where a.IdPegawai == IdPegawai
                                                              & a.IdJenisEvaluasiHarianPegawai == item.IdJenisEvaluasiHarianPegawai
                                                              & a.Tanggal.Date == currDate.Date
                                                              select a).FirstOrDefault();

                            if (tbKbmEvaluasiHarianPegawai == null)
                            {
                                isInsert = true;
                                tbKbmEvaluasiHarianPegawai = new TbKbmEvaluasiHarianPegawai();
                            }
                            tbKbmEvaluasiHarianPegawai.IdPegawai = IdPegawai;
                            tbKbmEvaluasiHarianPegawai.IdJenisEvaluasiHarianPegawai = item.IdJenisEvaluasiHarianPegawai;
                            tbKbmEvaluasiHarianPegawai.Tanggal = currDate;
                            tbKbmEvaluasiHarianPegawai.IsDone = item.IsDone;
                            tbKbmEvaluasiHarianPegawai.Catatan = item.Catatan;
                            if (isInsert)
                            {
                                conn.Insert(tbKbmEvaluasiHarianPegawai);
                            }
                            else
                            {
                                conn.Update(tbKbmEvaluasiHarianPegawai);
                            }

                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }

            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EvaluasiHarianAddPegawai", ex);
            }
        }

        #endregion

        public string UpdateKelompokQuran(int IdUser, KelasQuranModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbKelasQuran tbKelasQuran = conn.Get<TbKelasQuran>(Data.IdKelasQuran);
                        int idKelasQuran = 0;
                        if (tbKelasQuran == null)
                        {
                            var tbKelasQurans = conn.GetList<TbKbmJadwal>();
                            if (tbKelasQurans != null || tbKelasQurans.Count() > 0)
                                idKelasQuran = tbKelasQurans.Count();

                            idKelasQuran++;
                            tbKelasQuran = new TbKelasQuran
                            {
                                IdKelasQuran = idKelasQuran,
                                IdPegawai = Data.IdPegawai,
                                Nama = Data.Nama,
                                Kode = Data.Kode
                            };
                            conn.Insert(tbKelasQuran);

                            string hari = dateTimeService.GetLongDayName(_languageCode, Data.HariKe);
                            int idKbmJadwal = schService.InsertKbmJadwal(conn, hari, Data.JamMulai, Data.JamSelesai, out string oMessage);
                            if (!string.IsNullOrEmpty(oMessage))
                                return oMessage;
                            TbKelasQuranJadwal tbKelasQuranJadwal = (from a in conn.GetList<TbKelasQuranJadwal>()
                                                                     where a.IdKelasQuran == idKelasQuran
                                                                     & a.IdKbmJadwal == idKbmJadwal
                                                                     select a).FirstOrDefault();
                            if (tbKelasQuranJadwal == null)
                            {
                                tbKelasQuranJadwal = new TbKelasQuranJadwal
                                {
                                    IdKbmJadwal = idKbmJadwal,
                                    IdKelasQuran = idKelasQuran
                                };
                                conn.Insert(tbKelasQuranJadwal);
                            }
                        }
                        else
                        {
                            tbKelasQuran.IdPegawai = Data.IdPegawai;
                            tbKelasQuran.Nama = Data.Nama;
                            tbKelasQuran.Kode = Data.Kode;
                            conn.Update(tbKelasQuran);
                        }

                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UpdateKelompokQuran", ex);
            }
        }
        public string UpdateKelompokQuranSiswa(int IdUser, int IdKelasQuran, List<int> IdSiswas)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        for (int i = 0; i < IdSiswas.Count(); i++)
                        {
                            TbKelasQuranSiswa tbKelasQuranSiswa = (from a in conn.GetList<TbKelasQuranSiswa>()
                                                                   where a.IdSiswa == IdSiswas[i]
                                                                   select a).FirstOrDefault();
                            if (tbKelasQuranSiswa == null)
                            {
                                tbKelasQuranSiswa = new TbKelasQuranSiswa
                                {
                                    IdKelasQuran = IdKelasQuran,
                                    IdSiswa = IdSiswas[i]
                                };
                                conn.Insert(tbKelasQuranSiswa);
                            }
                            else
                            {
                                tbKelasQuranSiswa.IdKelasQuran = IdKelasQuran;
                                conn.Update(tbKelasQuranSiswa);
                            }
                        }
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UpdateKelompokQuranSiswa", ex); ;
            }
        }

        public string AddKelompokQuranExcel(int IdUser, KelasQuranAllExcelModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        //pindahin
                        var tbKelasQuran = (from a in conn.GetList<TbKelasQuranSiswa>()
                                            join b in conn.GetList<TbKelasQuran>() on a.IdKelasQuran equals b.IdKelasQuran
                                            join c in conn.GetList<TbUser>() on b.IdPegawai equals c.IdUser
                                            join d in conn.GetList<TbSiswa>() on a.IdSiswa equals d.IdSiswa
                                            select new { a, b, c, d }).ToList();
                        if (tbKelasQuran != null)
                        {
                            foreach (var item in tbKelasQuran.OrderBy(x => x.b.IdKelasQuran))
                            {
                                TaKelasQuranSiswa taKelasQuranSiswa = new TaKelasQuranSiswa
                                {
                                    IdTahunAjaran = schService.GetTahunAjaran(out string oMessage).IdTahunAjaran,
                                    Nis = item.d.Nis,
                                    NamaPegawai = userAppService.SetFullName(item.c.FirstName, item.c.MiddleName, item.c.LastName),
                                    NamaKelasQuran = item.b.Nama,
                                    ArchivedBy = IdUser,
                                    ArchivedDate = dateTimeService.GetCurrdate()
                                };
                                conn.Insert(taKelasQuranSiswa);
                            }
                        }
                        var tbKelasQuranProgres = (from a in conn.GetList<TbKelasQuranProgres>()
                                                   join b in conn.GetList<TbKelasQuran>() on a.IdKelasQuran equals b.IdKelasQuran
                                                   join c in conn.GetList<TbJenisJilidQuran>() on a.IdJenisJilidQuran equals c.IdJenisJilidQuran
                                                   join d in conn.GetList<TbSiswa>() on a.IdSiswa equals d.IdSiswa
                                                   select new { a, b, c, d }).ToList();
                        if (tbKelasQuranProgres != null)
                        {
                            foreach (var item in tbKelasQuranProgres.OrderBy(x => x.b.IdKelasQuran))
                            {
                                TaKelasQuranProgres taKelasQuranProgres = new TaKelasQuranProgres
                                {
                                    IdTahunAjaran = schService.GetTahunAjaran(out string oMessage).IdTahunAjaran,
                                    Nis = item.d.Nis,
                                    NamaKelasQuran = item.b.Nama,
                                    JenisJilidQuran = item.c.Nama,
                                    Catatan = item.a.Catatan,
                                    Halaman = item.a.Halaman,
                                    Tanggal = item.a.Tanggal,
                                    CreatedBy = item.a.CreatedBy,
                                    CreatedDate = item.a.CreatedDate,
                                    UpdatedBy = item.a.UpdatedBy,
                                    UpdatedDate = item.a.UpdatedDate,
                                    ArchivedBy = IdUser,
                                    ArchivedDate = dateTimeService.GetCurrdate()
                                };
                                conn.Insert(taKelasQuranProgres);
                            }
                        }

                        var tbKelasQuranUjian = (from a in conn.GetList<TbKelasQuranUjian>()
                                                 join b in conn.GetList<TbKelasQuran>() on a.IdKelasQuran equals b.IdKelasQuran
                                                 join c in conn.GetList<TbJenisJilidQuran>() on a.IdJenisJilidQuran equals c.IdJenisJilidQuran
                                                 join d in conn.GetList<TbSiswa>() on a.IdSiswa equals d.IdSiswa
                                                 select new { a, b, c, d }).ToList();
                        if (tbKelasQuranUjian != null)
                        {
                            foreach (var item in tbKelasQuranUjian.OrderBy(x => x.b.IdKelasQuran))
                            {
                                TaKelasQuranUjian taKelasQuranUjian = new TaKelasQuranUjian
                                {
                                    IdTahunAjaran = schService.GetTahunAjaran(out string oMessage).IdTahunAjaran,
                                    Nis = item.d.Nis,
                                    NamaKelasQuran = item.b.Nama,
                                    JenisJilidQuran = item.c.Nama,
                                    Catatan = item.a.Catatan,
                                    Tanggal = item.a.Tanggal,
                                    Hasil = item.a.Hasil,
                                    NilaiHuruf = item.a.NilaiHuruf,
                                    CreatedBy = item.a.CreatedBy,
                                    CreatedDate = item.a.CreatedDate,
                                    UpdatedBy = item.a.UpdatedBy,
                                    UpdatedDate = item.a.UpdatedDate,
                                    ArchivedBy = IdUser,
                                    ArchivedDate = dateTimeService.GetCurrdate()
                                };
                                conn.Insert(taKelasQuranUjian);
                            }
                        }

                        //delete
                        var xtbKelasQuranSiswa = conn.GetList<TbKelasQuranSiswa>().ToList();
                        foreach (var item in xtbKelasQuranSiswa)
                        {
                            conn.Delete(item);
                        }
                        var xtbKelasQuranUjian = conn.GetList<TbKelasQuranUjian>().ToList();
                        foreach (var item in xtbKelasQuranUjian)
                        {
                            conn.Delete(item);
                        }
                        var xtbKelasQuranProgres = conn.GetList<TbKelasQuranProgres>().ToList();
                        foreach (var item in xtbKelasQuranProgres)
                        {
                            conn.Delete(item);
                        }
                        var xtbKelasQuranJadwal = conn.GetList<TbKelasQuranJadwal>().ToList();
                        foreach (var item in xtbKelasQuranJadwal)
                        {
                            conn.Delete(item);
                        }
                        var xtbKelasQuran = conn.GetList<TbKelasQuran>().ToList();
                        foreach (var item in xtbKelasQuran)
                        {
                            conn.Delete(item);
                        }

                        foreach (var item in Data.KelasQuran)
                        {
                            var tbPegawai = conn.GetList<TbPegawai>().Where(x => x.Nip == item.NipGuru).FirstOrDefault();
                            if (tbPegawai == null) return "NIP " + item.NipGuru + " tidak ditemukan";
                            var tbKelasQuranx = new TbKelasQuran
                            {
                                IdPegawai = tbPegawai.IdPegawai,
                                Kode = item.Kode,
                                Nama = item.NamaKelompok
                            };
                            conn.Insert(tbKelasQuranx);
                        }

                        foreach (var item in Data.KelasQuranJadwal)
                        {
                            var tbKelasQuranx = conn.GetList<TbKelasQuran>().Where(x => x.Kode == item.Kode).FirstOrDefault();
                            if (tbKelasQuranx == null) return "KODE " + item.Kode + " tidak ditemukan";

                            int idKbmJadwal = schService.InsertKbmJadwal(conn, item.Hari, item.JamMulai, item.JamSelesai, out string oMessage);
                            TbKelasQuranJadwal tbKelasQuranJadwal = (from a in conn.GetList<TbKelasQuranJadwal>()
                                                                     where a.IdKelasQuran == tbKelasQuranx.IdKelasQuran
                                                                     & a.IdKbmJadwal == idKbmJadwal
                                                                     select a).FirstOrDefault();
                            if (tbKelasQuranJadwal == null)
                            {
                                tbKelasQuranJadwal = new TbKelasQuranJadwal
                                {
                                    IdKbmJadwal = idKbmJadwal,
                                    IdKelasQuran = tbKelasQuranx.IdKelasQuran
                                };
                                conn.Insert(tbKelasQuranJadwal);
                            }
                        }

                        foreach (var item in Data.KelasQuranSiswa)
                        {
                            var tbKelasQuranxx = conn.GetList<TbKelasQuran>().Where(x => x.Kode == item.Kode).FirstOrDefault();
                            if (tbKelasQuranxx == null) return "KODE " + item.Kode + " tidak ditemukan";
                            var tbSiswa = conn.GetList<TbSiswa>().Where(x => x.Nis == item.Nis).FirstOrDefault();
                            if (tbSiswa == null) return "NIS " + item.Nis + " tidak ditemukan";

                            var tbKelasQuranSiswa = new TbKelasQuranSiswa
                            {
                                IdKelasQuran = tbKelasQuranxx.IdKelasQuran,
                                IdSiswa = tbSiswa.IdSiswa
                            };
                            conn.Insert(tbKelasQuranSiswa);
                        }

                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddKelompokQuranExcel", ex);
            }
        }
        public string AddKelompokQuranJadwalExcel(int IdUser, List<KelasQuranJadwalExcelModel> Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        foreach (var item in Data)
                        {
                            var tbKelasQuran = conn.GetList<TbKelasQuran>().Where(x => x.Kode == item.Kode).FirstOrDefault();
                            if (tbKelasQuran == null) return "KODE " + item.Kode + " tidak ditemukan";

                            int idKbmJadwal = schService.InsertKbmJadwal(conn, item.Hari, item.JamMulai, item.JamSelesai, out string oMessage);
                            TbKelasQuranJadwal tbKelasQuranJadwal = (from a in conn.GetList<TbKelasQuranJadwal>()
                                                                     where a.IdKelasQuran == tbKelasQuran.IdKelasQuran
                                                                     & a.IdKbmJadwal == idKbmJadwal
                                                                     select a).FirstOrDefault();
                            if (tbKelasQuranJadwal == null)
                            {
                                tbKelasQuranJadwal = new TbKelasQuranJadwal
                                {
                                    IdKbmJadwal = idKbmJadwal,
                                    IdKelasQuran = tbKelasQuran.IdKelasQuran
                                };
                                conn.Insert(tbKelasQuranJadwal);
                            }
                        }
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddKelompokQuranJadwalExcel", ex);
            }
        }
        public string AddKelompokQuranSiswaExcel(int IdUser, List<KelasQuranSiswaExcelModel> Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        foreach (var item in Data)
                        {
                            var tbKelasQuran = conn.GetList<TbKelasQuran>().Where(x => x.Kode == item.Kode).FirstOrDefault();
                            if (tbKelasQuran == null) return "KODE " + item.Kode + " tidak ditemukan";
                            var tbSiswa = conn.GetList<TbSiswa>().Where(x => x.Nis == item.Nis).FirstOrDefault();
                            if (tbSiswa == null) return "NIS " + item.Nis + " tidak ditemukan";

                            var tbKelasQuranSiswa = new TbKelasQuranSiswa
                            {
                                IdKelasQuran = tbKelasQuran.IdKelasQuran,
                                IdSiswa = tbSiswa.IdSiswa
                            };
                            conn.Insert(tbKelasQuranSiswa);
                        }
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddKelompokQuranSiswaExcel", ex);
            }
        }

        public List<KelasQuranModel> GetKelompokQuran(int IdUser, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                DateTime currDate = dateTimeService.GetCurrdate();
                DateTime NextFirstDate = dateTimeService.GetFirstDate(currDate.AddDays(7)).Date;
                DateTime NextLastDate = dateTimeService.GetLastDate(currDate.AddDays(7)).Date;

                using (var conn = commonService.DbConnection())
                {
                    bool isAdmin = false;
                    var tbUserRoles = from a in conn.GetList<TbUserRole>()
                                      where a.IdUser == IdUser
                                      select a;
                    foreach (var item in tbUserRoles)
                    {
                        if (item.IdRole == 1)
                            isAdmin = true;
                    }
                    int idUnit = 0;
                    if (!isAdmin)
                    {
                        var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                             where a.IdPegawai == IdUser
                                             select a).FirstOrDefault();
                        if (tbUnitPegawai == null) { oMessage = "pegawai belum dipilih unit"; return null; }
                        idUnit = tbUnitPegawai.IdUnit;
                    }

                    var data = (from a in conn.GetList<TbKelasQuran>()
                                join b in conn.GetList<TbKelasQuranJadwal>() on a.IdKelasQuran equals b.IdKelasQuran
                                join c in conn.GetList<TbKbmJadwal>() on b.IdKbmJadwal equals c.IdKbmJadwal
                                join d in conn.GetList<TbUser>() on a.IdPegawai equals d.IdUser
                                join e in conn.GetList<TbUnitPegawai>() on a.IdPegawai equals e.IdPegawai
                                where e.IdUnit == (isAdmin ? e.IdUnit : idUnit)
                                select new { a, b, c, d }).ToList();

                    if (data == null) { oMessage = "data kbm kelas quran tidak ada"; return null; }
                    List<KelasQuranModel> ret = new List<KelasQuranModel>();
                    foreach (var item in data)
                    {
                        KelasQuranModel m = new KelasQuranModel();
                        m.IdKelasQuran = item.a.IdKelasQuran;
                        m.IdPegawai = item.a.IdPegawai;
                        m.NamaGuru = m.NamaGuru = userAppService.SetFullName(item.d.FirstName, item.d.MiddleName, item.d.LastName);
                        m.HariKe = item.c.HariKe;
                        m.Hari = dateTimeService.GetLongDayName(_languageCode, item.c.HariKe);
                        m.JamMulai = item.c.JamMulai.ToString();
                        m.JamSelesai = item.c.JamSelesai.ToString();
                        m.Kode = item.a.Kode;
                        m.Nama = item.a.Nama;
                        m.NamaSingkat = item.a.Kode;
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelompokQuran", ex);
                return null;
            }
        }

        public List<KelompokQuranSiswaModel> GetKelompokQuranSiswa(int IdKelasQuran, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = (from kelas_quran_siswa in conn.GetList<TbKelasQuranSiswa>()
                                join kelas_quran in conn.GetList<TbKelasQuran>() on kelas_quran_siswa.IdKelasQuran equals kelas_quran.IdKelasQuran
                                join siswa in conn.GetList<TbSiswa>() on kelas_quran_siswa.IdSiswa equals siswa.IdSiswa
                                where kelas_quran_siswa.IdKelasQuran == IdKelasQuran
                                select new { kelas_quran_siswa, kelas_quran, siswa }).ToList();
                    if (data == null) { oMessage = "data kbm kelas quran tidak ada"; return null; }
                    List<KelompokQuranSiswaModel> ret = new List<KelompokQuranSiswaModel>();
                    foreach (var item in data)
                    {
                        KelompokQuranSiswaModel m = new KelompokQuranSiswaModel();
                        m.IdKelasQuran = item.kelas_quran.IdKelasQuran;
                        m.IdSiswa = item.siswa.IdSiswa;
                        m.NamaSiswa = item.siswa.Nama;
                        m.Nama = item.kelas_quran.Nama;
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKelompokQuran", ex);
                return null;
            }
        }

        public string DeleteFileMateri(int IdUser, int IdKbmMateri)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.Get<TbKbmMateri>(IdKbmMateri);
                    if (data == null) return null;
                    if (data.NamaFileMateri != null)
                    {
                        if (File.Exists(Path.Combine(AppSetting.PathElearning, data.NamaFileMateri)))
                            File.Delete(Path.Combine(AppSetting.PathElearning, data.NamaFileMateri));

                        data.NamaFileMateri = null;
                        conn.Update(data);
                    }
                }
                return String.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteFileMateri", ex);
            }
        }
        public string DeleteFileSoal(int IdUser, int IdKbmMateri, int IdJenisSoal)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = (from a in conn.GetList<TbKbmSoal>()
                                where a.IdKbmMateri == IdKbmMateri & a.IdJenisSoal == IdJenisSoal
                                select a).FirstOrDefault();
                    if (data == null) return null;
                    if (data.NamaFile != null)
                    {
                        if (File.Exists(Path.Combine(AppSetting.PathElearning, data.NamaFile)))
                            File.Delete(Path.Combine(AppSetting.PathElearning, data.NamaFile));

                        data.NamaFile = null;
                        conn.Update(data);
                    }
                }
                return String.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteFileSoal", ex);
            }
        }


        public string DeleteFileMedia(int IdUser, int IdKbmMedia)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = (from a in conn.GetList<TbKbmMedia>()
                                where a.IdKbmMedia == IdKbmMedia
                                select a).FirstOrDefault();
                    if (data == null) return null;
                    if (data.NamaFile != null)
                    {
                        if (File.Exists(Path.Combine(AppSetting.PathElearning, data.NamaFile)))
                            File.Delete(Path.Combine(AppSetting.PathElearning, data.NamaFile));
                    }
                    conn.Delete(data);
                }
                return String.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteFileMedia", ex);
            }
        }

        public string ResetMateriSoal(int IdUser, int IdKbmMateri)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbKbmMateri = (from a in conn.GetList<TbKbmMateri>()
                                       where a.IdKbmMateri == IdKbmMateri
                                       select a).FirstOrDefault();
                    if (tbKbmMateri == null) return "data materi tidak ada";
                    tbKbmMateri.Deskripsi = null;
                    tbKbmMateri.DeskripsiTugas = null;

                    if (tbKbmMateri.NamaFileMateri != null)
                    {
                        if (File.Exists(Path.Combine(AppSetting.PathElearning, tbKbmMateri.NamaFileMateri)))
                            File.Delete(Path.Combine(AppSetting.PathElearning, tbKbmMateri.NamaFileMateri));
                    }
                    tbKbmMateri.NamaFileMateri = null;
                    tbKbmMateri.Judul = null;
                    conn.Update(tbKbmMateri);

                    var tbKbmMedia = (from a in conn.GetList<TbKbmMedia>()
                                      where a.IdKbmMateri == IdKbmMateri
                                      select a).ToList();
                    if (tbKbmMedia != null)
                    {
                        foreach (var item in tbKbmMedia)
                        {
                            if (item.NamaFile != null)
                            {
                                if (File.Exists(Path.Combine(AppSetting.PathElearning, item.NamaFile)))
                                    File.Delete(Path.Combine(AppSetting.PathElearning, item.NamaFile));
                            }
                            conn.Delete(item);
                        }

                    }
                    var tbKbmSoal = (from a in conn.GetList<TbKbmSoal>()
                                     where a.IdKbmMateri == IdKbmMateri
                                     select a).ToList();
                    if (tbKbmSoal != null || tbKbmSoal.Count() != 0)
                    {
                        foreach (var item in tbKbmSoal)
                        {
                            if (item.NamaFile != null)
                            {
                                if (File.Exists(Path.Combine(AppSetting.PathElearning, item.NamaFile)))
                                    File.Delete(Path.Combine(AppSetting.PathElearning, item.NamaFile));
                            }
                            conn.Delete(item);
                        }
                    }
                    return String.Empty;
                }
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "ResetMateriSoal", ex);
            }
        }
        public string SetJadwalKelasParalelNextWeek(int IdUser, List<SheetJadwalKbmModel> Data)
        {
            try
            {
                DateTime currDate = DateTime.Now;
                currDate = currDate.AddDays(7).Date;

                DateTime minDate = dateTimeService.GetFirstDate(currDate);
                DateTime maxDate = dateTimeService.GetLastDate(currDate);

                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        for (int i = 0; i < 7; i++)
                        {
                            var tbKbMateriOdls = from a in conn.GetList<TbKbmMateri>()
                                                 where a.Tanggal == minDate.AddDays(i).Date
                                                 select a;
                            foreach (var tbKbmMateriOld in tbKbMateriOdls)
                            {
                                var cekMedia = (from a in conn.GetList<TbKbmMedia>()
                                                where a.IdKbmMateri == tbKbmMateriOld.IdKbmMateri
                                                select a).ToList();
                                foreach (var item in cekMedia)
                                {
                                    if (item.NamaFile != null)
                                    {
                                        if (File.Exists(Path.Combine(AppSetting.PathElearning, item.NamaFile)))
                                            File.Delete(Path.Combine(AppSetting.PathElearning, item.NamaFile));
                                    }
                                    conn.Delete(item);
                                }

                                var cekSoal = (from a in conn.GetList<TbKbmSoal>()
                                               where a.IdKbmMateri == tbKbmMateriOld.IdKbmMateri
                                               select a).ToList();
                                foreach (var item in cekSoal)
                                {
                                    if (item.NamaFile != null)
                                    {
                                        if (File.Exists(Path.Combine(AppSetting.PathElearning, item.NamaFile)))
                                            File.Delete(Path.Combine(AppSetting.PathElearning, item.NamaFile));
                                    }
                                    conn.Delete(item);
                                }

                                if (tbKbmMateriOld.NamaFileMateri != null)
                                {
                                    if (File.Exists(Path.Combine(AppSetting.PathElearning, tbKbmMateriOld.NamaFileMateri)))
                                        File.Delete(Path.Combine(AppSetting.PathElearning, tbKbmMateriOld.NamaFileMateri));
                                }
                                conn.Delete(tbKbmMateriOld);
                            }
                        }


                        foreach (var itemJadwal in Data)
                        {
                            var kelasParalel = (from a in conn.GetList<TbKelasParalel>()
                                                where a.Nama == itemJadwal.KelasParalel
                                                select a).FirstOrDefault();
                            if (kelasParalel == null) return string.Format("kelas paralel {0} tidak ada", itemJadwal.KelasParalel);

                            var data = from kbm in conn.GetList<TbKbm>()
                                       join jadwal in conn.GetList<TbKbmJadwal>() on kbm.IdKbmJadwal equals jadwal.IdKbmJadwal
                                       where kbm.IdKelompok == itemJadwal.Kelompok
                                       & kbm.IdKelasParalel == kelasParalel.IdKelasParalel
                                       select new { kbm, jadwal };
                            if (data == null || data.Count() == 0)
                                return string.Format("kategori {0} kelas paralel {1} tidak ada", itemJadwal.Kelompok, itemJadwal.KelasParalel);


                            foreach (var item in data)
                            {
                                DateTime tanggal = minDate.AddDays(item.jadwal.HariKe);

                                TbKbmMateri tbKbmMateri = new TbKbmMateri
                                {
                                    CreatedBy = IdUser,
                                    CreatedDate = dateTimeService.GetCurrdate(),
                                    IdKbmJadwal = item.kbm.IdKbmJadwal,
                                    IdKelasParalel = item.kbm.IdKelasParalel,
                                    IdMapel = item.kbm.IdMapel,
                                    IdPegawai = item.kbm.IdPegawai,
                                    IdKelompok = item.kbm.IdKelompok,
                                    Tanggal = tanggal
                                };
                                conn.Insert(tbKbmMateri);
                            }
                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetJadwalKelasParalel", ex);
            }
        }
        public string SetJadwalKelasParalelNextWeek(int IdUser, int Kelompok, List<int> IdKelasParalels)
        {
            try
            {
                DateTime currDate = DateTime.Now;
                currDate = currDate.AddDays(7).Date;

                DateTime minDate = dateTimeService.GetFirstDate(currDate);
                DateTime maxDate = dateTimeService.GetLastDate(currDate);

                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {

                        for (int i = 0; i < IdKelasParalels.Count(); i++)
                        {

                            var data = from kbm in conn.GetList<TbKbm>()
                                       join jadwal in conn.GetList<TbKbmJadwal>() on kbm.IdKbmJadwal equals jadwal.IdKbmJadwal
                                       where kbm.IdKelompok == Kelompok
                                       & kbm.IdKelasParalel == IdKelasParalels[i]
                                       select new { kbm, jadwal };
                            var tglLama = data.GroupBy(x => x.jadwal)
                            .Select(y => y.First())
                            .ToList();
                            foreach (var item in tglLama)
                            {
                                var tbKbMateriOlds = from a1 in conn.GetList<TbKbmMateri>()
                                                     where a1.IdKelasParalel == IdKelasParalels[i]
                                                     & a1.Tanggal == minDate.AddDays(item.jadwal.HariKe)
                                                     select a1;


                                foreach (var tbKbMateriOld in tbKbMateriOlds)
                                {
                                    var tbKbmMedias = from a in conn.GetList<TbKbmMedia>()
                                                      where a.IdKbmMateri == tbKbMateriOld.IdKbmMateri
                                                      select a;
                                    foreach (var tbKbmMedia in tbKbmMedias)
                                    {
                                        conn.Delete(tbKbmMedia);
                                    }

                                    var tbKbmSoals = from a in conn.GetList<TbKbmSoal>()
                                                     where a.IdKbmMateri == tbKbMateriOld.IdKbmMateri
                                                     select a;
                                    foreach (var tbKbmSoal in tbKbmSoals)
                                    {
                                        conn.Delete(tbKbmSoal);
                                    }
                                    conn.Delete(tbKbMateriOld);
                                }
                            }
                            foreach (var item in data)
                            {
                                DateTime tanggal = minDate.AddDays(item.jadwal.HariKe);

                                TbKbmMateri tbKbmMateri = new TbKbmMateri
                                {
                                    CreatedBy = IdUser,
                                    CreatedDate = dateTimeService.GetCurrdate(),
                                    IdKbmJadwal = item.kbm.IdKbmJadwal,
                                    IdKelasParalel = item.kbm.IdKelasParalel,
                                    IdMapel = item.kbm.IdMapel,
                                    IdPegawai = item.kbm.IdPegawai,
                                    IdKelompok = item.kbm.IdKelompok,
                                    Tanggal = tanggal
                                };
                                conn.Insert(tbKbmMateri);
                            }
                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetJadwalKelasParalel", ex);
            }
        }
        public string SetJadwalKelasParalel(int IdUser, int Kelompok, List<int> IdKelasParalels)
        {
            try
            {
                DateTime currDate = DateTime.Now;
                currDate = currDate.AddDays(14).Date;

                DateTime minDate = dateTimeService.GetFirstDate(currDate);
                DateTime maxDate = dateTimeService.GetLastDate(currDate);

                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {

                        for (int i = 0; i < IdKelasParalels.Count(); i++)
                        {

                            var data = from kbm in conn.GetList<TbKbm>()
                                       join jadwal in conn.GetList<TbKbmJadwal>() on kbm.IdKbmJadwal equals jadwal.IdKbmJadwal
                                       where kbm.IdKelompok == Kelompok
                                       & kbm.IdKelasParalel == IdKelasParalels[i]
                                       select new { kbm, jadwal };
                            var tglLama = data.GroupBy(x => x.jadwal)
                            .Select(y => y.First())
                            .ToList();
                            foreach (var item in tglLama)
                            {
                                var tbKbMateriOlds = from a1 in conn.GetList<TbKbmMateri>()
                                                     where a1.IdKelasParalel == IdKelasParalels[i]
                                                     & a1.Tanggal == minDate.AddDays(item.jadwal.HariKe)
                                                     select a1;
                                foreach (var tbKbMateriOld in tbKbMateriOlds)
                                {
                                    conn.Delete(tbKbMateriOld);
                                }
                            }
                            foreach (var item in data)
                            {
                                DateTime tanggal = minDate.AddDays(item.jadwal.HariKe);

                                TbKbmMateri tbKbmMateri = new TbKbmMateri
                                {
                                    CreatedBy = IdUser,
                                    CreatedDate = dateTimeService.GetCurrdate(),
                                    IdKbmJadwal = item.kbm.IdKbmJadwal,
                                    IdKelasParalel = item.kbm.IdKelasParalel,
                                    IdMapel = item.kbm.IdMapel,
                                    IdPegawai = item.kbm.IdPegawai,
                                    IdKelompok = item.kbm.IdKelompok,
                                    Tanggal = tanggal
                                };
                                conn.Insert(tbKbmMateri);
                            }
                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetJadwalKelasParalel", ex);
            }
        }
        public List<SheetJadwalKbmModel> GetKbms(int IdUser, int IdKelompok, out string oMessage)
        {
            try
            {
                List<SheetJadwalKbmModel> ret = new List<SheetJadwalKbmModel>();
                using (var conn = commonService.DbConnection())
                {
                    oMessage = schService.GetLevelJabatanUnitPegawai(conn, IdUser, out int LevelJabatan, out int IdUnit);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    bool canReadAll = false;
                    if (LevelJabatan == LevelJabatanPegawaiConstant.Administrator) canReadAll = true;

                    var data = from kbm in conn.GetList<TbKbm>()
                               join kelaParalel in conn.GetList<TbKelasParalel>() on kbm.IdKelasParalel equals kelaParalel.IdKelasParalel
                               join mapel in conn.GetList<TbMapel>() on kbm.IdMapel equals mapel.IdMapel
                               join user in conn.GetList<TbUser>() on kbm.IdPegawai equals user.IdUser
                               join jadwal in conn.GetList<TbKbmJadwal>() on kbm.IdKbmJadwal equals jadwal.IdKbmJadwal
                               join kelas in conn.GetList<TbKelas>() on kelaParalel.IdKelas equals kelas.IdKelas
                               join pegawai in conn.GetList<TbPegawai>() on kbm.IdPegawai equals pegawai.IdPegawai
                               where kbm.IdKelompok == IdKelompok
                               & kelas.IdUnit == (canReadAll ? kelas.IdUnit : IdUnit)
                               select new { kbm, kelaParalel, mapel, user, jadwal, pegawai };
                    foreach (var item in data.OrderBy(x => x.kbm.IdKelasParalel))
                    {
                        SheetJadwalKbmModel m = new SheetJadwalKbmModel
                        {
                            Hari = dateTimeService.GetLongDayName(_languageCode, item.jadwal.HariKe),
                            JamMulai = item.jadwal.JamMulai.ToString(),
                            JamSelesai = item.jadwal.JamSelesai.ToString(),
                            KelasParalel = item.kelaParalel.Nama,
                            Kelompok = IdKelompok,
                            NamaGuru = userAppService.SetFullName(item.user.FirstName, item.user.MiddleName, item.user.LastName),
                            NamaMapel = item.mapel.Nama,
                            NipGuru = item.pegawai.Nip
                        };
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKbms", ex);
                return null;
            }
        }
        public List<SheetJadwalKbmModel> GetKbmsByKelas(int IdUser, int IdKelompok, int IdKelas, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<SheetJadwalKbmModel> ret = new List<SheetJadwalKbmModel>();
                using (var conn = commonService.DbConnection())
                {
                    var data = from kbm in conn.GetList<TbKbm>()
                               join kelaParalel in conn.GetList<TbKelasParalel>() on kbm.IdKelasParalel equals kelaParalel.IdKelasParalel
                               join mapel in conn.GetList<TbMapel>() on kbm.IdMapel equals mapel.IdMapel
                               join user in conn.GetList<TbUser>() on kbm.IdPegawai equals user.IdUser
                               join jadwal in conn.GetList<TbKbmJadwal>() on kbm.IdKbmJadwal equals jadwal.IdKbmJadwal
                               join kelas in conn.GetList<TbKelas>() on kelaParalel.IdKelas equals kelas.IdKelas
                               join pegawai in conn.GetList<TbPegawai>() on kbm.IdPegawai equals pegawai.IdPegawai
                               where kbm.IdKelompok == IdKelompok
                               & kelas.IdKelas == IdKelas
                               select new { kbm, kelaParalel, mapel, user, jadwal, pegawai };
                    foreach (var item in data.OrderBy(x => x.jadwal.HariKe).ThenBy(x => x.jadwal.JamMulai))
                    {
                        SheetJadwalKbmModel m = new SheetJadwalKbmModel
                        {
                            Hari = dateTimeService.GetLongDayName(_languageCode, item.jadwal.HariKe),
                            JamMulai = item.jadwal.JamMulai.ToString(),
                            JamSelesai = item.jadwal.JamSelesai.ToString(),
                            KelasParalel = item.kelaParalel.Nama,
                            Kelompok = IdKelompok,
                            NamaGuru = userAppService.SetFullName(item.user.FirstName, item.user.MiddleName, item.user.LastName),
                            NamaMapel = item.mapel.Nama,
                            NipGuru = item.pegawai.Nip
                        };
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKbmsByKelasParalelKelompok", ex);
                return null;
            }

        }
        public List<SheetJadwalKbmModel> GetKbmsByKelasParalelKelompok(int IdKelasParalel, int IdKelompok, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<SheetJadwalKbmModel> ret = new List<SheetJadwalKbmModel>();
                using (var conn = commonService.DbConnection())
                {
                    var data = from kbm in conn.GetList<TbKbm>()
                               join kelaParalel in conn.GetList<TbKelasParalel>() on kbm.IdKelasParalel equals kelaParalel.IdKelasParalel
                               join mapel in conn.GetList<TbMapel>() on kbm.IdMapel equals mapel.IdMapel
                               join user in conn.GetList<TbUser>() on kbm.IdPegawai equals user.IdUser
                               join jadwal in conn.GetList<TbKbmJadwal>() on kbm.IdKbmJadwal equals jadwal.IdKbmJadwal
                               join kelas in conn.GetList<TbKelas>() on kelaParalel.IdKelas equals kelas.IdKelas
                               join pegawai in conn.GetList<TbPegawai>() on kbm.IdPegawai equals pegawai.IdPegawai
                               where kbm.IdKelompok == IdKelompok
                               & kelaParalel.IdKelasParalel == IdKelasParalel
                               select new { kbm, kelaParalel, mapel, user, jadwal, pegawai };
                    foreach (var item in data.OrderBy(x => x.jadwal.HariKe).ThenBy(x => x.jadwal.JamMulai))
                    {
                        SheetJadwalKbmModel m = new SheetJadwalKbmModel
                        {
                            Hari = dateTimeService.GetLongDayName(_languageCode, item.jadwal.HariKe),
                            JamMulai = item.jadwal.JamMulai.ToString(),
                            JamSelesai = item.jadwal.JamSelesai.ToString(),
                            KelasParalel = item.kelaParalel.Nama,
                            Kelompok = IdKelompok,
                            NamaGuru = userAppService.SetFullName(item.user.FirstName, item.user.MiddleName, item.user.LastName),
                            NamaMapel = item.mapel.Nama,
                            NipGuru = item.pegawai.Nip
                        };
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKbmsByKelasParalelKelompok", ex);
                return null;
            }
        }
        public string UploadJadwalKbm(int IdUser, List<SheetJadwalKbmModel> Data)
        {
            string xdata = string.Empty;

            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var c = from a in Data
                                select a;

                        foreach (var item in Data.Select(x => new { x.KelasParalel, x.Kelompok }))
                        {
                            var kbmOlds = from a in conn.GetList<TbKbm>()
                                          join b in conn.GetList<TbKelasParalel>() on a.IdKelasParalel equals b.IdKelasParalel
                                          where a.IdKelompok == item.Kelompok
                                          & b.Nama == item.KelasParalel
                                          select a;
                            foreach (var kbmOld in kbmOlds)
                            {
                                conn.Delete(kbmOld);
                            }
                        }
                        string sql = "INSERT INTO kbm.tb_kbm(id_kelas_paralel, id_mapel, id_pegawai, id_kbm_jadwal, id_kelompok) VALUES (@IdKelasParalel, @IdMapel, @IdPegawai, @IdKbmJadwal, @IdKelompok)";
                        List<TbKbm> TbKbms = new List<TbKbm>();
                        bool isDuplikat = false;
                        string dataDuplikat = string.Empty;
                        foreach (var item in Data)
                        {
                            xdata = string.Empty;
                            xdata = item.KelasParalel + ";" + item.NipGuru + ";" + item.Hari + ";" + item.JamMulai + ";" + item.JamSelesai;
                            int idKbmJadwal = schService.InsertKbmJadwal(conn, item.Hari, item.JamMulai, item.JamSelesai, out string oMessage);
                            if (!string.IsNullOrEmpty(oMessage))
                                return oMessage;
                            xdata += "-->" + idKbmJadwal.ToString();
                            var kelasParalel = (from a in conn.GetList<TbKelasParalel>()
                                                where a.Nama == item.KelasParalel
                                                select a).FirstOrDefault();
                            int idKelasParalel = 0;
                            if (kelasParalel == null)
                                return string.Format("kelas paralel {0} tidak terdafatar", item.KelasParalel);
                            idKelasParalel = kelasParalel.IdKelasParalel;

                            int idPegawai = 0;
                            var pegawai = (from a in conn.GetList<TbPegawai>()
                                           where a.Nip == item.NipGuru
                                           select a).FirstOrDefault();
                            if (pegawai == null) return string.Format("nip guru {0} tidak terdafatar", item.NipGuru);
                            idPegawai = pegawai.IdPegawai;

                            int idMapel = 0;
                            var mapel = (from a in conn.GetList<TbMapel>()
                                         where a.Nama == item.NamaMapel
                                         select a).FirstOrDefault();
                            if (mapel == null) return string.Format("mata pelajaran {0} tidak terdafatar", item.NamaMapel);
                            idMapel = mapel.IdMapel;

                            TbKbm tbKbm = new TbKbm
                            {
                                IdKelasParalel = idKelasParalel,
                                IdMapel = idMapel,
                                IdPegawai = idPegawai,
                                IdKbmJadwal = idKbmJadwal,
                                IdKelompok = item.Kelompok
                            };
                            var x = (from a in TbKbms
                                     where a.IdKbmJadwal == tbKbm.IdKbmJadwal
                                     & a.IdMapel == tbKbm.IdMapel
                                     & a.IdPegawai == tbKbm.IdPegawai
                                     & a.IdKelasParalel == tbKbm.IdKelasParalel
                                     & a.IdKelompok == tbKbm.IdKelompok
                                     select a).FirstOrDefault();

                            if (x != null)
                            {
                                isDuplikat = true;
                                dataDuplikat += string.Format("{0} - {1} - {2} - {3} - {4} - {5} - (Duplikat) \n", item.KelasParalel, item.Hari, item.JamMulai, item.JamSelesai, item.NamaMapel, item.NamaGuru);
                            }

                            TbKbms.Add(tbKbm);
                        }
                        if (isDuplikat)
                        {
                            return dataDuplikat;
                        }

                        var affectedRows = conn.Execute(sql, TbKbms);
                        tx.Commit();
                        Console.WriteLine(DateTime.Now);
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UploadJadwalKbm" + " " + xdata, ex);
            }

        }
        public KbmJadwalUnitModel GetJadwalKbmsUnit(int IdUser, int IdUnit, string Tanggal, out string oMessage)
        {
            try
            {
                DateTime currDate = DateTime.Now;
                currDate = currDate.AddDays(14).Date;
                if (!string.IsNullOrEmpty(Tanggal))
                    currDate = dateTimeService.StrToDateTime(Tanggal);
                DateTime minDate = dateTimeService.GetFirstDate(currDate).Date;
                DateTime maxDate = dateTimeService.GetLastDate(currDate).Date;

                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = from unit in conn.GetList<TbUnit>()
                               join kelas in conn.GetList<TbKelas>() on unit.IdUnit equals kelas.IdUnit
                               join kelasParalel in conn.GetList<TbKelasParalel>() on kelas.IdKelas equals kelasParalel.IdKelas
                               where unit.IdUnit == IdUnit
                               select new { kelasParalel.IdKelasParalel, NamaKelas = kelas.Nama, NamaKelasParalel = kelasParalel.Nama };
                    KbmJadwalUnitModel ret = new KbmJadwalUnitModel
                    {
                        Periode = string.Format("Periode {0} s.d. {1} ", minDate.ToString("dd-MM-yyyy"), maxDate.ToString("dd-MM-yyyy")),
                        Jadwals = new List<KbmJadwalUnitListModel>()
                    };
                    foreach (var item in data.OrderBy(x => x.IdKelasParalel))
                    {
                        KbmJadwalUnitListModel m = new KbmJadwalUnitListModel
                        {
                            IdKelasParalel = item.IdKelasParalel,
                            Kelas = item.NamaKelas,
                            KelasParalel = item.NamaKelasParalel,
                            Kelompok = null,
                            Setting = "T"
                        };
                        var materi = (from a in conn.GetList<TbKbmMateri>()
                                      where a.IdKelasParalel == m.IdKelasParalel
                                      & (a.Tanggal >= minDate.Date & a.Tanggal <= maxDate.Date)
                                      select a).FirstOrDefault();
                        if (materi != null)
                        {
                            m.Kelompok = materi.IdKelompok;
                            m.Setting = "Y";
                        }
                        ret.Jadwals.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJadwalKbmsUnit", ex);
                return null;
            }
        }
        public List<MapelModel> GetMapels(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<MapelModel> ret = new List<MapelModel>();
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var data = from a in conn.GetList<TbMapel>()
                                   select a;
                        if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                        foreach (var item in data)
                        {
                            ret.Add(new MapelModel
                            {
                                IdMapel = item.IdMapel,
                                Kode = item.Kode,
                                Nama = item.Nama
                            });
                        }
                        return ret;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetMapels", ex);
                return null;
            }
        }
        public MapelModel GetMapel(int IdMapel, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                MapelModel ret = new MapelModel();
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var dt = (from a in conn.GetList<TbMapel>()
                                  where a.IdMapel == IdMapel
                                  select a).ToList();
                        if (dt == null || dt.Count() == 0)
                        {
                            oMessage = "data tidak ada";
                            return null;
                        }

                        var data = conn.Get<TbMapel>(IdMapel);
                        ret.IdMapel = data.IdMapel;
                        ret.Kode = data.Kode;
                        ret.Nama = data.Nama;
                        return ret;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetMapel", ex);
                return null;
            }
        }
        public string MapelAddEdit(int IdUser, MapelModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var cekTbMapel = (from a in conn.GetList<TbMapel>()
                                          where a.Kode == Data.Kode
                                          select a).ToList();
                        if (cekTbMapel.Count() == 0)
                        {
                            TbMapel tbMapel = new TbMapel();
                            var tbMapels = conn.GetList<TbMapel>();
                            if (tbMapels == null || tbMapels.Count() == 0)
                            {
                                tbMapel.IdMapel = 1;
                            }
                            else
                            {
                                // GET MAX ID + 1 
                                var maxSekolahKegiatan = (from a in conn.GetList<TbMapel>()
                                                          select new { a.IdMapel }).OrderByDescending(uk => uk.IdMapel).FirstOrDefault();
                                tbMapel.IdMapel = maxSekolahKegiatan.IdMapel + 1;
                            }
                            tbMapel.Kode = Data.Kode;
                            tbMapel.Nama = Data.Nama;
                            conn.Insert(tbMapel);
                        }
                        else
                        {
                            foreach (var item in cekTbMapel)
                            {
                                TbMapel tbMapel = conn.Get<TbMapel>(item.IdMapel);
                                if (tbMapel == null) return "data tidak ada";
                                tbMapel.Kode = Data.Kode;
                                tbMapel.Nama = Data.Nama;
                                conn.Update(tbMapel);
                            }
                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "MapelAddEdit", ex);
            }
        }

        public string MapelDelete(int IdUser, int IdMapel)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbMapel tbMapel = conn.Get<TbMapel>(IdMapel);
                        if (tbMapel == null)
                        {
                            return "data tidak ditemukan!";
                        }
                        conn.Delete(tbMapel);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "MapelDelete", ex);
            }
        }
        public string PenilaianDiriSiswaAdd(PenilaianDiriSiswaAddModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        conn.DeleteList<TbPenilaianDiriSiswa>(new { IdSiswa = Data.IdSiswa, IdTahunAjaran = Data.IdTahunAjaran, IdSemester = Data.IdSemester });
                        foreach (var item in Data.PenilaianDiris)
                        {
                            TbPenilaianDiriSiswa tbPenilaianDiriSiswa = new TbPenilaianDiriSiswa
                            {
                                Catatan = item.Catatan,
                                IdJenisPenilaianDiri = item.IdJenisPenilaianDiri,
                                IdSiswa = Data.IdSiswa,
                                IdTahunAjaran = Data.IdTahunAjaran,
                                IdSemester = Data.IdSemester,
                                Penilaian = item.Penilaian
                            };
                            conn.Insert(tbPenilaianDiriSiswa);
                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PenilaianDiriSiswaAdd", ex);
            }
        }
        public List<PenilaianDiriSiswaListModel> GetPenilaianDiriSiswa(int IdSiswa, int IdTahunAjaran, int IdSemester, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbJenisPenilaianDiri>();
                    List<PenilaianDiriSiswaListModel> ret = new List<PenilaianDiriSiswaListModel>();
                    foreach (var item in data.OrderBy(x=>x.IdJenisPenilaianDiri))
                    {
                        PenilaianDiriSiswaListModel m = new PenilaianDiriSiswaListModel
                        {
                            IdJenisPenilaianDiri = item.IdJenisPenilaianDiri,
                            JenisPenilaianDiri = item.Nama,
                            Penilaian = 0,
                            Catatan = string.Empty
                        };
                        var tbPenilaianDiriSiswa = (from a in conn.GetList<TbPenilaianDiriSiswa>()
                                                                    where a.IdTahunAjaran == IdTahunAjaran
                                                                    & a.IdSemester == IdSemester
                                                                    & a.IdSiswa == IdSiswa
                                                                    & a.IdJenisPenilaianDiri == item.IdJenisPenilaianDiri
                                                                    select a).FirstOrDefault();

                        if (tbPenilaianDiriSiswa != null) {
                            m.Catatan = tbPenilaianDiriSiswa.Catatan;
                            m.Penilaian = tbPenilaianDiriSiswa.Penilaian;
                        }

                        ret.Add(m);
                    }

                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage= commonService.GetErrorMessage(ServiceName + "GetPenilaianDiriSiswa", ex);
                return null;
            }
        }
    }
}
