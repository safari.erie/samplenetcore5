﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Dapper;
using Microsoft.AspNetCore.Http;
using Swg.Entities.Ppdb;
using Swg.Sch.Shared.Constants;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;
using Swg.Models;
using Swg.Entities.Lmg;
using Swg.Entities.Kbm;
using Swg.Entities.Ref;
using Swg.Entities.Public;
using Swg.Models.Constants;
using System.ComponentModel.DataAnnotations;

namespace Swg.Sch.Services
{
    public class PpdbService : IPpdbService
    {

        private string _languageCode = "id";
        private readonly string ServiceName = "Swg.Sch.Services.PpdbService.";
        private readonly ICommonService commonService;
        private readonly IUserAppService userAppService;
        private readonly IDateTimeService dateTimeService;
        private readonly ISchService schService;
        private readonly IEmailService emailService;
        private readonly IPdfService pdfService;
        private PpdbModelRead currPpdb = new PpdbModelRead();

        public PpdbService(
            ICommonService CommonService,
            IDateTimeService DateTimeService,
            IUserAppService UserAppService,
            ISchService SchService,
            IPdfService PdfService,
            IEmailService EmailService)
        {
            commonService = CommonService;
            userAppService = UserAppService;
            dateTimeService = DateTimeService;
            schService = SchService;
            emailService = EmailService;
            pdfService = PdfService;
            this.SetCurrentPPdb();

        }

        private void SetCurrentPPdb()
        {
            var ppdb = schService.GetPpdb(out string err);
            if (string.IsNullOrEmpty(err))
                currPpdb = ppdb;
        }
        public string UploadVA(int IdUser, List<PpdbVaSheetModel> Data)
        {
            try
            {
                string err = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                         where a.IdPegawai == IdUser
                                         select a).FirstOrDefault();
                    if (tbUnitPegawai == null)
                    {
                        err = "pegawai tidak terdaftar";
                        return null;
                    }
                    if (currPpdb == null) { err = "ppdb belum disetting"; return null; }

                    using (var tx = conn.BeginTransaction())
                    {
                        List<TbPpdbVa> tbPpdbVas = new List<TbPpdbVa>();
                        foreach (var item in Data)
                        {
                            TbPpdbVa tbPpdbVa = conn.Get<TbPpdbVa>(item.IdPpdbVa);
                            if (tbPpdbVa == null)
                            {
                                var idUnit = (from a in UnitConstant.DictUnit
                                              where a.Value == item.Unit
                                              select a.Key).FirstOrDefault();
                                if (idUnit != 0)
                                {
                                    tbPpdbVas.Add(new TbPpdbVa
                                    {
                                        IdPpdbVa = item.IdPpdbVa,
                                        IdUnit = idUnit,
                                        Status = StatusPpdbVaConstant.Aktif
                                    });
                                }
                                else
                                {
                                    err += string.Format("unit {0} tidak ada", item.Unit) + " \r\n ";
                                }
                            }
                            else
                            {
                                err += string.Format("nomor va {0} sudah ada", item.IdPpdbVa) + " \r\n ";
                            }
                        }
                        string sql = "INSERT INTO ppdb.tb_ppdb_va(";
                        sql += "	id_ppdb_va, id_unit, status)";
                        sql += "	VALUES (@IdPpdbVa, @IdUnit, @Status);";
                        var affectedRows = conn.Execute(sql, tbPpdbVas);
                        tx.Commit();
                        return err;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UploadVA", ex);
            }
        }
        public List<PpdbVaSheetModel> GetListVa(int IdUser, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                         where a.IdPegawai == IdUser
                                         select a).FirstOrDefault();
                    if (tbUnitPegawai == null)
                    {
                        oMessage = "pegawai tidak terdaftar";
                        return null;
                    }

                    var data = conn.GetList<TbPpdbVa>();
                    if (data.Count() == 0 || data == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    List<PpdbVaSheetModel> ret = new List<PpdbVaSheetModel>();
                    foreach (var item in data.OrderByDescending(x => x.IdUnit))
                    {
                        ret.Add(new PpdbVaSheetModel
                        {
                            IdPpdbVa = item.IdPpdbVa,
                            Unit = UnitConstant.DictUnit[item.IdUnit],
                            Status = StatusPpdbVaConstant.DictStatusBerkas[item.Status]
                        });
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "PpdbKelasDelete", ex);
                return null;
            }
        }
        public string PpdbAdd(PpdbModelAddEdit Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var oldPpdb = from a in conn.GetList<TbPpdb>()
                                      where a.Status == 1
                                      select a;
                        foreach (var item in oldPpdb)
                        {
                            item.Status = -1;
                            conn.Update(item);
                        }

                        TbPpdb tbPpdb = new TbPpdb();
                        tbPpdb.IdPpdb = Data.IdPpdb;
                        tbPpdb.IdKetua = Data.IdKetua;
                        tbPpdb.MinNoSk = Data.MinNoSk;
                        tbPpdb.TahunAjaran = Data.TahunAjaran;
                        tbPpdb.TanggalAwal = dateTimeService.StrToDateTime(Data.TanggalAwal);
                        tbPpdb.TanggalAkhir = dateTimeService.StrToDateTime(Data.TanggalAkhir);
                        tbPpdb.Status = 1;
                        string fileName = string.Empty;
                        if (Data.FilePetunjuk != null && Data.FilePetunjuk.Length > 0)
                        {
                            var pathUpload = AppSetting.PathApplFile;
                            fileName = Path.GetRandomFileName() + "." + Path.GetExtension(Data.FilePetunjuk.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, fileName), FileMode.Create, FileAccess.Write))
                            {
                                Data.FilePetunjuk.CopyTo(fileStream);
                            }
                        }
                        tbPpdb.FilePetunjuk = fileName;

                        conn.Insert(tbPpdb);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PpdbAdd", ex);
            }
        }
        public string PpdbEdit(PpdbModelAddEdit Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbPpdb tbPpdb = conn.Get<TbPpdb>(Data.IdPpdb);
                        if (tbPpdb == null)
                        {
                            return "id tidak ditemukan";
                        }

                        tbPpdb.IdKetua = Data.IdKetua;
                        tbPpdb.MinNoSk = Data.MinNoSk;
                        tbPpdb.TahunAjaran = Data.TahunAjaran;
                        tbPpdb.TanggalAwal = dateTimeService.StrToDateTime(Data.TanggalAwal);
                        tbPpdb.TanggalAkhir = dateTimeService.StrToDateTime(Data.TanggalAkhir);

                        if (Data.FilePetunjuk != null && Data.FilePetunjuk.Length > 0)
                        {
                            string fileName = string.Empty;
                            var pathUpload = AppSetting.PathApplFile;
                            if (tbPpdb.FilePetunjuk != "")
                            {
                                File.Delete(Path.Combine(pathUpload, tbPpdb.FilePetunjuk));
                            }

                            fileName = Path.GetRandomFileName() + "." + Path.GetExtension(Data.FilePetunjuk.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, fileName), FileMode.Create, FileAccess.Write))
                            {
                                Data.FilePetunjuk.CopyTo(fileStream);
                            }
                            tbPpdb.FilePetunjuk = fileName;
                        }

                        conn.Update(tbPpdb);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PpdbEdit", ex);
            }
        }
        public string PpdbDelete(int IdPpdb, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbPpdb tbPpdb = conn.Get<TbPpdb>(IdPpdb);
                        if (tbPpdb == null)
                        {
                            oMessage = String.Format("Data Tidak Ada");
                            return null;
                        }
                        if (tbPpdb.FilePetunjuk != "")
                        {
                            File.Delete(Path.Combine(AppSetting.PathApplFile, tbPpdb.FilePetunjuk));
                        }
                        conn.Delete(tbPpdb);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "PpdbDelete", ex);
                return null;
            }
        }
        public List<PpdbRedaksiEmailModel> GetPpdbRedaksiEmails(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var tbPpdb = conn.GetList<TbPpdb>().ToList();
                    if (tbPpdb == null) { oMessage = "ppdb belum disetting"; return null; }
                    //if (tbPpdb.Count() > 1) { oMessage = "data ppdb salah"; return null; }
                    var tbPpdbRedaksiEmail = conn.GetList<TbPpdbRedaksiEmail>();
                    if (tbPpdbRedaksiEmail.Count() == 0 || tbPpdbRedaksiEmail == null) { oMessage = "Data tidak ada"; return null; }
                    List<PpdbRedaksiEmailModel> ret = new List<PpdbRedaksiEmailModel>();
                    foreach (var item in tbPpdbRedaksiEmail.OrderByDescending(x => x.IdPpdbRedaksiEmail))
                    {
                        ret.Add(new PpdbRedaksiEmailModel
                        {
                            IdPpdbRedaksiEmail = item.IdPpdbRedaksiEmail,
                            IdPpdb = item.IdPpdb,
                            IdJenisRedaksiEmailPpdb = item.IdJenisRedaksiEmailPpdb,
                            JenisRedaksiEmailPpdb = JenisRedaksiEmailPpdbConstant.Dict[item.IdJenisRedaksiEmailPpdb],
                            MessageBody1 = item.MessageBody1,
                            MessageBody2 = item.MessageBody2
                        });
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbRedaksiEmails", ex);
                return null;
            }
        }
        public PpdbRedaksiEmailModel GetPpdbRedaksiEmail(int IdPpdbRedaksiEmail, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var tbPpdbRedaksiEmail = (from a in conn.GetList<TbPpdbRedaksiEmail>()
                                              where a.IdPpdbRedaksiEmail == IdPpdbRedaksiEmail
                                              select a).FirstOrDefault();
                    if (tbPpdbRedaksiEmail == null)
                    {
                        oMessage = "Data tidak ada";
                        return null;
                    }
                    PpdbRedaksiEmailModel ret = new PpdbRedaksiEmailModel
                    {
                        IdPpdbRedaksiEmail = tbPpdbRedaksiEmail.IdPpdbRedaksiEmail,
                        IdPpdb = tbPpdbRedaksiEmail.IdPpdb,
                        IdJenisRedaksiEmailPpdb = tbPpdbRedaksiEmail.IdJenisRedaksiEmailPpdb,
                        JenisRedaksiEmailPpdb = JenisRedaksiEmailPpdbConstant.Dict[tbPpdbRedaksiEmail.IdJenisRedaksiEmailPpdb],
                        MessageBody1 = tbPpdbRedaksiEmail.MessageBody1,
                        MessageBody2 = tbPpdbRedaksiEmail.MessageBody2
                    };
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbRedaksiEmail", ex);
                return null;
            }
        }
        public string PpdbRedaksiEmailAdd(PpdbRedaksiEmailModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbPpdb = conn.GetList<TbPpdb>().ToList();
                    if (tbPpdb == null) return "ppdb belum disetting";
                    if (tbPpdb.Count() > 1) return "data ppdb salah";
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbPpdbRedaksiEmail tbPpdbRedaksiEmail = new TbPpdbRedaksiEmail();
                        tbPpdbRedaksiEmail.IdPpdb = tbPpdb[0].IdPpdb;
                        tbPpdbRedaksiEmail.IdJenisRedaksiEmailPpdb = Data.IdJenisRedaksiEmailPpdb;
                        tbPpdbRedaksiEmail.MessageBody1 = Data.MessageBody1;
                        tbPpdbRedaksiEmail.MessageBody2 = Data.MessageBody2;

                        conn.Insert(tbPpdbRedaksiEmail);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PpdbRedaksiEmailAdd", ex);
            }
        }
        public string PpdbRedaksiEmailEdit(PpdbRedaksiEmailModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbPpdb = conn.GetList<TbPpdb>().ToList();
                    if (tbPpdb == null) return "ppdb belum disetting";
                    if (tbPpdb.Count() > 1) return "data ppdb salah";
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbPpdbRedaksiEmail = (from a in conn.GetList<TbPpdbRedaksiEmail>()
                                                  where a.IdPpdbRedaksiEmail == Data.IdPpdbRedaksiEmail
                                                  select a).FirstOrDefault();
                        if (tbPpdbRedaksiEmail == null)
                        {
                            return "id tidak ditemukan";
                        }
                        tbPpdbRedaksiEmail.IdPpdb = tbPpdb[0].IdPpdb;
                        tbPpdbRedaksiEmail.IdJenisRedaksiEmailPpdb = Data.IdJenisRedaksiEmailPpdb;
                        tbPpdbRedaksiEmail.MessageBody1 = Data.MessageBody1;
                        tbPpdbRedaksiEmail.MessageBody2 = Data.MessageBody2;

                        conn.Update(tbPpdbRedaksiEmail);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PpdbRedaksiEmailAdd", ex);
            }
        }
        public string PpdbRedaksiEmailDelete(int IdPpdbRedaksiEmail, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbPpdbRedaksiEmail = (from a in conn.GetList<TbPpdbRedaksiEmail>()
                                                  where a.IdPpdbRedaksiEmail == IdPpdbRedaksiEmail
                                                  select a).FirstOrDefault();
                        if (tbPpdbRedaksiEmail == null)
                        {
                            oMessage = String.Format("Data Tidak Ada");
                            return null;
                        }
                        conn.Delete(tbPpdbRedaksiEmail);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "PpdbRedaksiEmailDelete", ex);
                return null;
            }
        }
        public List<PpdbInfoModelRead> GetPpdbInfos(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var tbPpdb = conn.GetList<TbPpdb>().ToList();
                    if (tbPpdb == null) { oMessage = "ppdb belum disetting"; return null; }
                    // if (tbPpdb.Count() > 1) { oMessage = "data ppdb salah"; return null; }
                    var tbPpdbInfo = conn.GetList<TbPpdbInfo>();
                    if (tbPpdbInfo.Count() == 0 || tbPpdbInfo == null) { oMessage = "Data tidak ada"; return null; }
                    List<PpdbInfoModelRead> ret = new List<PpdbInfoModelRead>();
                    foreach (var item in tbPpdbInfo.OrderByDescending(x => x.IdPpdbInfo))
                    {
                        ret.Add(new PpdbInfoModelRead
                        {
                            IdPpdb = item.IdPpdb,
                            IdPpdbInfo = item.IdPpdbInfo,
                            IdJenisPpdbInfo = item.IdJenisPpdbInfo,
                            NamaTipe = JenisPpdbInfoConstant.DictJenisPpdbInfo[item.IdJenisPpdbInfo],
                            Judul = item.Judul,
                            Konten = item.Konten,
                            Url = item.Url,
                            FileImage = item.FileImage
                        });
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbInfos", ex);
                return null;
            }
        }
        public PpdbInfoModelRead GetPpdbInfo(int IdPpdbInfo, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var tbPpdbInfo = (from a in conn.GetList<TbPpdbInfo>()
                                      where a.IdPpdbInfo == IdPpdbInfo
                                      select a).FirstOrDefault();
                    if (tbPpdbInfo == null) { oMessage = "Data tidak ada"; return null; }
                    PpdbInfoModelRead ret = new PpdbInfoModelRead
                    {
                        IdPpdb = tbPpdbInfo.IdPpdb,
                        IdPpdbInfo = tbPpdbInfo.IdPpdbInfo,
                        IdJenisPpdbInfo = tbPpdbInfo.IdJenisPpdbInfo,
                        NamaTipe = JenisPpdbInfoConstant.DictJenisPpdbInfo[tbPpdbInfo.IdJenisPpdbInfo],
                        Judul = tbPpdbInfo.Judul,
                        Konten = tbPpdbInfo.Konten,
                        Url = tbPpdbInfo.Url,
                        FileImage = tbPpdbInfo.FileImage
                    };
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbInfo", ex);
                return null;
            }
        }
        public string PpdbInfoAdd(PpdbInfoModelAddEdit Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbPpdb = conn.GetList<TbPpdb>().ToList();
                    if (tbPpdb == null) return "ppdb belum disetting";
                    if (tbPpdb.Count() > 1) return "data ppdb salah";
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbPpdbInfo tbPpdbInfo = new TbPpdbInfo();
                        var tbPpdbInfos = conn.GetList<TbPpdbInfo>();
                        if (tbPpdbInfos == null || tbPpdbInfos.Count() == 0)
                        {
                            tbPpdbInfo.IdPpdbInfo = 1;
                        }
                        else
                        {
                            var maxPpdbInfo = (from a in conn.GetList<TbPpdbInfo>()
                                               select new { a.IdPpdbInfo }).OrderByDescending(uk => uk.IdPpdbInfo).FirstOrDefault();
                            tbPpdbInfo.IdPpdbInfo = maxPpdbInfo.IdPpdbInfo + 1;
                        }
                        tbPpdbInfo.IdPpdb = tbPpdb[0].IdPpdb;
                        tbPpdbInfo.IdJenisPpdbInfo = Data.IdJenisPpdbInfo;
                        tbPpdbInfo.Judul = Data.Judul;
                        tbPpdbInfo.Konten = Data.Konten;

                        string fileName = string.Empty;
                        if (Data.FileImage != null && Data.FileImage.Length > 0)
                        {
                            fileName = Path.GetRandomFileName() + Path.GetExtension(Data.FileImage.FileName);
                            using (var fileStream = new FileStream(Path.Combine(AppSetting.PathApplFile, fileName), FileMode.Create, FileAccess.Write))
                            {
                                Data.FileImage.CopyTo(fileStream);
                            }

                        }
                        tbPpdbInfo.FileImage = fileName;
                        if (Data.IdJenisPpdbInfo == JenisPpdbInfoConstant.Pengumuman)
                        {
                            tbPpdbInfo.Url = Guid.NewGuid().ToString();
                        }
                        else
                        {
                            tbPpdbInfo.Url = null;
                        }

                        conn.Insert(tbPpdbInfo);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PpdbInfoAdd", ex);
            }
        }
        public string PpdbInfoEdit(PpdbInfoModelAddEdit Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbPpdb = conn.GetList<TbPpdb>().ToList();
                    if (tbPpdb == null) return "ppdb belum disetting";
                    if (tbPpdb.Count() > 1) return "data ppdb salah";
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        try
                        {
                            TbPpdbInfo tbPpdbInfo = conn.Get<TbPpdbInfo>(Data.IdPpdbInfo);
                            if (tbPpdbInfo == null)
                            {
                                return "id tidak ditemukan";
                            }
                            tbPpdbInfo.IdPpdb = tbPpdb[0].IdPpdb;
                            tbPpdbInfo.IdJenisPpdbInfo = Data.IdJenisPpdbInfo;
                            tbPpdbInfo.Judul = Data.Judul;
                            tbPpdbInfo.Konten = Data.Konten;

                            if (Data.FileImage != null && Data.FileImage.Length > 0)
                            {
                                string fileName = string.Empty;
                                var pathUpload = AppSetting.PathApplFile;
                                if (tbPpdbInfo.FileImage != "")
                                {
                                    File.Delete(Path.Combine(pathUpload, tbPpdbInfo.FileImage));
                                }

                                fileName = Path.GetRandomFileName() + Path.GetExtension(Data.FileImage.FileName);
                                using (var fileStream = new FileStream(Path.Combine(pathUpload, fileName), FileMode.Create, FileAccess.Write))
                                {
                                    Data.FileImage.CopyTo(fileStream);
                                }
                                tbPpdbInfo.FileImage = fileName;
                            }

                            if (Data.IdJenisPpdbInfo == JenisPpdbInfoConstant.Pengumuman)
                            {
                                tbPpdbInfo.Url = Guid.NewGuid().ToString();
                            }
                            else
                            {
                                tbPpdbInfo.Url = null;
                            }

                            conn.Update(tbPpdbInfo);
                            tx.Commit();

                            return string.Empty;
                        }
                        catch (Exception ex)
                        {
                            tx.Rollback();
                            return commonService.GetErrorMessage(ServiceName + "PpdbInfoEdit", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PpdbInfoEdit", ex);
            }
        }
        public string PpdbInfoDelete(int IdPpdbInfo, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbPpdbInfo tbPpdbInfo = conn.Get<TbPpdbInfo>(IdPpdbInfo);
                        if (tbPpdbInfo == null)
                        {
                            oMessage = String.Format("Data Tidak Ada");
                            return null;
                        }
                        if (tbPpdbInfo.FileImage != "")
                        {
                            File.Delete(Path.Combine(AppSetting.PathApplFile, tbPpdbInfo.FileImage));
                        }
                        conn.Delete(tbPpdbInfo);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "PpdbInfoDelete", ex);
                return null;
            }
        }
        public List<PpdbBiayaModel> GetPpdbBiayas(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var tbPpdb = conn.GetList<TbPpdb>().ToList();
                    if (tbPpdb == null) { oMessage = "ppdb belum disetting"; return null; }
                    // if (tbPpdb.Count() > 1) { oMessage = "data ppdb salah"; return null; }
                    var tbPpdbBiaya = from a in conn.GetList<TbPpdbBiaya>()
                                      join b in conn.GetList<TbJenisBiayaPpdb>() on a.IdJenisBiayaPpdb equals b.IdJenisBiayaPpdb
                                      join c in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals c.IdPpdbKelas
                                      join d in conn.GetList<TbKelas>() on c.IdKelas equals d.IdKelas
                                      select new { a, b, c, d };
                    if (tbPpdbBiaya.Count() == 0 || tbPpdbBiaya == null) { oMessage = "Data tidak ada"; return null; }
                    List<PpdbBiayaModel> ret = new List<PpdbBiayaModel>();
                    foreach (var item in tbPpdbBiaya.OrderByDescending(x => x.a.IdPpdbBiaya))
                    {
                        PpdbBiayaModel m = new PpdbBiayaModel
                        {
                            IdPpdbBiaya = item.a.IdPpdbBiaya,
                            IdPpdb = item.a.IdPpdb,
                            IdJenisBiaya = item.a.IdJenisBiayaPpdb,
                            JenisBiaya = item.b.Nama,
                            Rp = item.a.Rp,
                            IdPpdbKelas = item.a.IdPpdbKelas,
                            IdUnit = item.d.IdUnit,
                            Unit = UnitConstant.DictUnit[item.d.IdUnit],
                            Kelas = item.d.Nama,
                        };
                        if (item.c.IdJenisPeminatan != 0)
                            m.Kelas += " " + JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb[(int)item.c.IdJenisPeminatan];
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbBiayas", ex);
                return null;
            }
        }
        public PpdbBiayaModel GetPpdbBiaya(int IdPpdbBiaya, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var tbPpdbBiaya = (from a in conn.GetList<TbPpdbBiaya>()
                                       join b in conn.GetList<TbJenisBiayaPpdb>() on a.IdJenisBiayaPpdb equals b.IdJenisBiayaPpdb
                                       join c in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals c.IdPpdbKelas
                                       join d in conn.GetList<TbKelas>() on c.IdKelas equals d.IdKelas
                                       where a.IdPpdbBiaya == IdPpdbBiaya
                                       select new { a, b, c, d }).FirstOrDefault();
                    if (tbPpdbBiaya == null) { oMessage = "data tidak ada"; return null; }
                    PpdbBiayaModel ret = new PpdbBiayaModel
                    {
                        IdPpdbBiaya = tbPpdbBiaya.a.IdPpdbBiaya,
                        IdPpdb = tbPpdbBiaya.a.IdPpdb,
                        IdJenisBiaya = tbPpdbBiaya.a.IdJenisBiayaPpdb,
                        JenisBiaya = tbPpdbBiaya.b.Nama,
                        Rp = tbPpdbBiaya.a.Rp,
                        IdPpdbKelas = tbPpdbBiaya.a.IdPpdbKelas,
                        IdUnit = tbPpdbBiaya.d.IdUnit,
                        Unit = UnitConstant.DictUnit[tbPpdbBiaya.d.IdUnit],
                        Kelas = tbPpdbBiaya.d.Nama,
                    };
                    if (tbPpdbBiaya.c.IdJenisPeminatan != 0)
                        ret.Kelas += " " + JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb[(int)tbPpdbBiaya.c.IdJenisPeminatan];
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbBiaya", ex);
                return null;
            }
        }
        public string PpdbBiayaAdd(int IdUser, PpdbBiayaAddEditModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                         where a.IdPegawai == IdUser
                                         select a).FirstOrDefault();
                    if (tbUnitPegawai == null)
                    {
                        return "pegawai tidak terdaftar";
                    }
                    var tbPpdb = conn.GetList<TbPpdb>().ToList();
                    if (tbPpdb == null) return "ppdb belum disetting";
                    // if (tbPpdb.Count() > 1) return "data ppdb salah";
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbPpdbBiaya tbPpdbBiaya = new TbPpdbBiaya();
                        tbPpdbBiaya.IdPpdb = currPpdb.IdPpdb;
                        tbPpdbBiaya.IdJenisBiayaPpdb = Data.IdJenisBiaya;
                        tbPpdbBiaya.IdPpdbKelas = Data.IdPpdbKelas;
                        tbPpdbBiaya.Rp = Data.Rp;

                        conn.Insert(tbPpdbBiaya);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PpdbBiayaAdd", ex);
            }
        }
        public string PpdbBiayaEdit(int IdUser, PpdbBiayaAddEditModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                         where a.IdPegawai == IdUser
                                         select a).FirstOrDefault();
                    if (tbUnitPegawai == null)
                    {
                        return "pegawai tidak terdaftar";
                    }
                    var tbPpdb = conn.GetList<TbPpdb>().ToList();
                    if (tbPpdb == null) return "ppdb belum disetting";
                    if (tbPpdb.Count() > 1) return "data ppdb salah";
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbPpdbBiaya = (from a in conn.GetList<TbPpdbBiaya>()
                                           where a.IdPpdbBiaya == Data.IdPpdbBiaya
                                           select a).FirstOrDefault();
                        if (tbPpdbBiaya == null)
                        {
                            return "id tidak ditemukan";
                        }
                        tbPpdbBiaya.IdPpdb = currPpdb.IdPpdb;
                        tbPpdbBiaya.IdPpdbKelas = Data.IdPpdbKelas;
                        tbPpdbBiaya.IdJenisBiayaPpdb = Data.IdJenisBiaya;
                        tbPpdbBiaya.Rp = Data.Rp;

                        conn.Update(tbPpdbBiaya);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PpdbBiayaEdit", ex);
            }
        }
        public string PpdbBiayaDelete(int IdUser, int IdPpdbBiaya, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                         where a.IdPegawai == IdUser
                                         select a).FirstOrDefault();
                    if (tbUnitPegawai == null)
                    {
                        return "pegawai tidak terdaftar";
                    }
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbPpdbBiaya = (from a in conn.GetList<TbPpdbBiaya>()
                                           where a.IdPpdbBiaya == IdPpdbBiaya
                                           select a).FirstOrDefault();
                        if (tbPpdbBiaya == null)
                        {
                            oMessage = String.Format("Data Tidak Ada");
                            return null;
                        }
                        conn.Delete(tbPpdbBiaya);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "PpdbBiayaDelete", ex);
                return null;
            }
        }
        public List<ComboModel> GetPpdbJenisBiaya(int IdUser, int IdJenisBiayaPpdb, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var tbJenisBiayaPpdb = (from a in conn.GetList<TbJenisBiayaPpdb>()
                                            where a.IdJenisBiayaPpdb == (IdJenisBiayaPpdb == 0 ? a.IdJenisBiayaPpdb : IdJenisBiayaPpdb)
                                            select a).ToList();
                    if (tbJenisBiayaPpdb == null || tbJenisBiayaPpdb.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    List<ComboModel> ret = new List<ComboModel>();
                    foreach (var item in tbJenisBiayaPpdb.OrderByDescending(x => x.IdJenisBiayaPpdb))
                    {
                        ret.Add(new ComboModel
                        {
                            Id = item.IdJenisBiayaPpdb,
                            Nama = item.Nama,
                            NamaSingkat = item.NamaSingkat,
                        });
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbJenisBiaya", ex);
                return null;
            }
        }
        public string PpdbJenisBiayaAdd(int IdUser, ComboModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                         where a.IdPegawai == IdUser
                                         select a).FirstOrDefault();
                    if (tbUnitPegawai == null)
                    {
                        return "pegawai tidak terdaftar";
                    }
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbJenisBiayaPpdb tbJenisBiayaPpdb = new TbJenisBiayaPpdb();
                        var maxJenisBiayaPpdb = (from a in conn.GetList<TbJenisBiayaPpdb>()
                                                 select new { a.IdJenisBiayaPpdb }).OrderByDescending(uk => uk.IdJenisBiayaPpdb).FirstOrDefault();
                        int MaxIdJenisBiayaPpdb = 0;
                        if (maxJenisBiayaPpdb == null)
                        {
                            MaxIdJenisBiayaPpdb = 1;
                        }
                        else
                        {
                            MaxIdJenisBiayaPpdb = maxJenisBiayaPpdb.IdJenisBiayaPpdb + 1;
                        }

                        tbJenisBiayaPpdb.IdJenisBiayaPpdb = MaxIdJenisBiayaPpdb;

                        tbJenisBiayaPpdb.Nama = Data.Nama;
                        tbJenisBiayaPpdb.NamaSingkat = Data.NamaSingkat;

                        conn.Insert(tbJenisBiayaPpdb);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "JenisBiayaPpdb", ex);
            }
        }
        public string PpdbJenisBiayaEdit(int IdUser, ComboModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                         where a.IdPegawai == IdUser
                                         select a).FirstOrDefault();
                    if (tbUnitPegawai == null)
                    {
                        return "pegawai tidak terdaftar";
                    }
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbJenisBiayaPpdb = (from a in conn.GetList<TbJenisBiayaPpdb>()
                                                where a.IdJenisBiayaPpdb == Data.Id
                                                select a).FirstOrDefault();
                        if (tbJenisBiayaPpdb == null)
                        {
                            return "id tidak ditemukan";
                        }
                        tbJenisBiayaPpdb.Nama = Data.Nama;
                        tbJenisBiayaPpdb.NamaSingkat = Data.NamaSingkat;

                        conn.Update(tbJenisBiayaPpdb);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PpdbJenisBiayaEdit", ex);
            }
        }
        public string PpdbJenisBiayaDelete(int IdUser, int IdJenisBiayaPpdb, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                         where a.IdPegawai == IdUser
                                         select a).FirstOrDefault();
                    if (tbUnitPegawai == null)
                    {
                        return "pegawai tidak terdaftar";
                    }
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbJenisBiayaPpdb tbJenisBiayaPpdb = (from a in conn.GetList<TbJenisBiayaPpdb>()
                                                             where a.IdJenisBiayaPpdb == IdJenisBiayaPpdb
                                                             select a).FirstOrDefault();
                        if (tbJenisBiayaPpdb == null)
                        {
                            oMessage = String.Format("Data Tidak Ada");
                            return null;
                        }
                        conn.Delete(tbJenisBiayaPpdb);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "PpdbKelasDelete", ex);
                return null;
            }
        }
        public List<PpdbSeragamListModel> GetPpdbSeragamByUnit(int IdUnit, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var tbPpdbSeragam = (from a in conn.GetList<TbPpdbSeragam>()
                                         where a.IdUnit == IdUnit
                                         select a).ToList();
                    if (tbPpdbSeragam == null || tbPpdbSeragam.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    List<PpdbSeragamListModel> ret = new List<PpdbSeragamListModel>();
                    foreach (var item in tbPpdbSeragam.OrderByDescending(x => x.IdPpdbSeragam))
                    {
                        ret.Add(new PpdbSeragamListModel
                        {
                            IdPpdbSeragam = item.IdPpdbSeragam,
                            Nama = item.Nama,
                            NamaSingkat = item.NamaSingkat,
                            IdUnit = item.IdUnit,
                            Unit = UnitConstant.DictUnit[item.IdUnit]
                        });
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbSeragams", ex);
                return null;
            }
        }
        public List<PpdbSeragamListModel> GetPpdbSeragams(int IdUser, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    bool isAdmin = false;
                    var tbUserRoles = from a in conn.GetList<TbUserRole>()
                                      where a.IdUser == IdUser
                                      select a;
                    foreach (var item in tbUserRoles)
                    {
                        if (item.IdRole == 1)
                            isAdmin = true;
                    }
                    int idUnit = 0;
                    if (!isAdmin)
                    {
                        var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                             where a.IdPegawai == IdUser
                                             select a).FirstOrDefault();
                        if (tbUnitPegawai == null) { oMessage = "pegawai belum dipilih unit"; return null; }
                        idUnit = tbUnitPegawai.IdUnit;
                    }
                    var tbPpdbSeragam = (from a in conn.GetList<TbPpdbSeragam>()
                                         where a.IdUnit == (isAdmin ? a.IdUnit : idUnit)
                                         select a).ToList();
                    if (tbPpdbSeragam == null || tbPpdbSeragam.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    List<PpdbSeragamListModel> ret = new List<PpdbSeragamListModel>();
                    foreach (var item in tbPpdbSeragam.OrderByDescending(x => x.IdPpdbSeragam))
                    {
                        ret.Add(new PpdbSeragamListModel
                        {
                            IdPpdbSeragam = item.IdPpdbSeragam,
                            Nama = item.Nama,
                            NamaSingkat = item.NamaSingkat,
                            IdUnit = item.IdUnit,
                            Unit = UnitConstant.DictUnit[item.IdUnit]
                        });
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbSeragams", ex);
                return null;
            }
        }
        public PpdbSeragamModel GetPpdbSeragam(int IdPpdbSeragam, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var tbPpdbSeragam = (from a in conn.GetList<TbPpdbSeragam>()
                                         where a.IdPpdbSeragam == IdPpdbSeragam
                                         select a).FirstOrDefault();
                    if (tbPpdbSeragam == null) { oMessage = "data tidak ada"; return null; }
                    PpdbSeragamModel ret = new PpdbSeragamModel
                    {
                        IdPpdbSeragam = tbPpdbSeragam.IdPpdbSeragam,
                        IdUnit = tbPpdbSeragam.IdUnit,
                        Nama = tbPpdbSeragam.Nama,
                        NamaSingkat = tbPpdbSeragam.NamaSingkat
                    };
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbSeragam", ex);
                return null;
            }
        }
        public string PpdbSeragamAdd(int IdUser, PpdbSeragamModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                         where a.IdPegawai == IdUser
                                         select a).FirstOrDefault();
                    if (tbUnitPegawai == null)
                    {
                        return "pegawai tidak terdaftar di unit";
                    }
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var maxPpdbSeragam = (from a in conn.GetList<TbPpdbSeragam>()
                                              select new { a.IdPpdbSeragam }).OrderByDescending(uk => uk.IdPpdbSeragam).FirstOrDefault();
                        int maxIdPpdbSeragam = 0;
                        if (maxPpdbSeragam == null)
                        {
                            maxIdPpdbSeragam = 1;
                        }
                        else
                        {
                            maxIdPpdbSeragam = maxPpdbSeragam.IdPpdbSeragam + 1;
                        }

                        TbPpdbSeragam tbPpdbSeragam = new TbPpdbSeragam
                        {
                            IdPpdbSeragam = maxIdPpdbSeragam,
                            Nama = Data.Nama,
                            NamaSingkat = Data.NamaSingkat,
                            IdUnit = Data.IdUnit
                        };

                        conn.Insert(tbPpdbSeragam);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PpdbSeragamAdd", ex);
            }
        }
        public string PpdbSeragamEdit(int IdUser, PpdbSeragamModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                         where a.IdPegawai == IdUser
                                         select a).FirstOrDefault();
                    if (tbUnitPegawai == null)
                    {
                        return "pegawai tidak terdaftar di unit";
                    }
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbPpdbSeragam = (from a in conn.GetList<TbPpdbSeragam>()
                                             where a.IdPpdbSeragam == Data.IdPpdbSeragam
                                             select a).FirstOrDefault();
                        if (tbPpdbSeragam == null)
                        {
                            return "IdPpdbSeragam tidak ditemukan";
                        }
                        tbPpdbSeragam.Nama = Data.Nama;
                        tbPpdbSeragam.NamaSingkat = Data.NamaSingkat;
                        tbPpdbSeragam.IdUnit = Data.IdUnit;

                        conn.Update(tbPpdbSeragam);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PpdbSeragamEdit", ex);
            }
        }
        public string PpdbSeragamDelete(int IdUser, int IdPpdbSeragam, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var tbUnitPegawai = (from a in conn.GetList<TbUnitPegawai>()
                                         where a.IdPegawai == IdUser
                                         select a).FirstOrDefault();
                    if (tbUnitPegawai == null)
                    {
                        return "pegawai tidak terdaftar di unit";
                    }
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbPpdbSeragam tbPpdbSeragam = (from a in conn.GetList<TbPpdbSeragam>()
                                                       where a.IdPpdbSeragam == IdPpdbSeragam
                                                       select a).FirstOrDefault();
                        if (tbPpdbSeragam == null)
                        {
                            oMessage = String.Format("Data Tidak Ada");
                            return null;
                        }
                        conn.Delete(tbPpdbSeragam);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "PpdbSeragamDelete", ex);
                return null;
            }
        }
        public List<PpdbKelasViewModel> GetPpdbKelass(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<PpdbKelasViewModel> ret = new List<PpdbKelasViewModel>();
                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbPpdbKelas>()
                               join b in conn.GetList<TbKelas>() on a.IdKelas equals b.IdKelas
                               where a.IdPpdb == currPpdb.IdPpdb
                               select new { a, b };
                    foreach (var item in data)
                    {
                        PpdbKelasViewModel m = new PpdbKelasViewModel
                        {
                            IdJenisPeminatan = item.a.IdJenisPeminatan,
                            IdUnit = item.b.IdUnit,
                            IdKelas = item.a.IdKelas,
                            KuotaDaftarL = item.a.KuotaDaftarL,
                            KuotaDaftarP = item.a.KuotaDaftarP,
                            KuotaDaftarPrestasi = item.a.KuotaDaftarPrestasi,
                            KuotaTerimaL = item.a.KuotaTerimaL,
                            IdPpdbKelas = item.a.IdPpdbKelas,
                            Kelas = item.b.Nama,
                            KuotaTerimaP = item.a.KuotaTerimaP,
                            MaxTanggalLahir = item.a.MaxTanggalLahir.ToString("dd-MM-yyyy")
                        };
                        if (item.a.IdJenisPeminatan != 0)
                            m.Kelas += " " + JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb[(int)item.a.IdJenisPeminatan];
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbKelass", ex);
                return null;

            }
        }
        public string PpdbKelasAdd(int IdUser, PpdbKelasModel Data)
        {
            try
            {
                if (currPpdb == null)
                    return "ppdb belum diset";

                if (string.IsNullOrEmpty(Data.MaxTanggalLahir))
                    Data.MaxTanggalLahir = "01-01-1900";

                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbPpdbKelas tbPpdbKelas = new TbPpdbKelas
                        {
                            IdKelas = Data.IdKelas,
                            IdPpdb = currPpdb.IdPpdb,
                            KuotaDaftarL = Data.KuotaDaftarL,
                            KuotaDaftarP = Data.KuotaDaftarP,
                            KuotaDaftarPrestasi = Data.KuotaDaftarPrestasi,
                            KuotaTerimaL = Data.KuotaTerimaL,
                            KuotaTerimaP = Data.KuotaTerimaP,
                            MaxTanggalLahir = dateTimeService.StrToDateTime(Data.MaxTanggalLahir)
                        };
                        if (Data.IdJenisPeminatan != null)
                        {
                            if (!commonService.DictCheck(JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb, (int)Data.IdJenisPeminatan))
                                return "jenis peminatan salah";
                            tbPpdbKelas.IdJenisPeminatan = (int)Data.IdJenisPeminatan;
                        }
                        else
                        {
                            tbPpdbKelas.IdJenisPeminatan = 0;
                        }
                        conn.Insert(tbPpdbKelas);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PpdbKelasAdd", ex);

            }
        }
        public string PpdbKelasEdit(int IdUser, PpdbKelasEditModel Data)
        {
            try
            {
                if (currPpdb == null)
                    return "ppdb belum diset";

                if (string.IsNullOrEmpty(Data.MaxTanggalLahir))
                    Data.MaxTanggalLahir = "01-01-1900";

                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbPpdbKelas tbPpdbKelas = conn.Get<TbPpdbKelas>(Data.IdPpdbKelas);
                        tbPpdbKelas.KuotaDaftarL = Data.KuotaDaftarL;
                        tbPpdbKelas.KuotaDaftarP = Data.KuotaDaftarP;
                        tbPpdbKelas.KuotaDaftarPrestasi = Data.KuotaDaftarPrestasi;
                        tbPpdbKelas.KuotaTerimaL = Data.KuotaTerimaL;
                        tbPpdbKelas.KuotaTerimaP = Data.KuotaTerimaP;
                        tbPpdbKelas.MaxTanggalLahir = dateTimeService.StrToDateTime(Data.MaxTanggalLahir);
                        if (Data.IdJenisPeminatan != null)
                        {
                            if (!commonService.DictCheck(JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb, (int)Data.IdJenisPeminatan))
                                return "jenis peminatan salah";
                            tbPpdbKelas.IdJenisPeminatan = (int)Data.IdJenisPeminatan;
                        }
                        else
                        {
                            tbPpdbKelas.IdJenisPeminatan = 0;
                        }
                        conn.Update(tbPpdbKelas);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PpdbKelasEdit", ex);

            }
        }
        public string PpdbKelasDelete(int Iduse, int IdPpdbKelas)
        {
            try
            {
                if (currPpdb == null)
                    return "ppdb belum diset";

                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbPpdbKelas tbPpdbKelas = conn.Get<TbPpdbKelas>(IdPpdbKelas);
                        conn.Delete(tbPpdbKelas);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PpdbKelasDelete", ex);

            }
        }
        public List<PpdbJadwalViewModel> GetPpdbJadwals(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<PpdbJadwalViewModel> ret = new List<PpdbJadwalViewModel>();
                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbPpdbJadwal>()
                               join b in conn.GetList<TbUnit>() on a.IdUnit equals b.IdUnit
                               where a.IdPpdb == currPpdb.IdPpdb
                               select new { a, b };
                    foreach (var item in data)
                    {
                        PpdbJadwalViewModel m = new PpdbJadwalViewModel
                        {
                            IdJalurPendaftaran = item.a.IdJalurPendaftaran,
                            IdJenisGelombangPpdb = item.a.IdJenisGelombangPpdb,
                            IdJenisPendaftaran = item.a.IdJenisPendaftaran,
                            IdUnit = item.a.IdUnit,
                            JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[item.a.IdJalurPendaftaran],
                            JenisGelombangPpdb = GelombangPpdbConstant.DictGelombangPpdb[item.a.IdJenisGelombangPpdb],
                            JenisPendaftaran = JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[item.a.IdJenisPendaftaran],
                            TanggalAkhirBayarDaftar = item.a.TanggalAkhirBayarDaftar.ToString("dd-MM-yyyy"),
                            TanggalAkhirBayarPendidikan = item.a.TanggalAkhirBayarPendidikan.ToString("dd-MM-yyyy"),
                            TanggalAkhirDaftar = item.a.TanggalAkhirDaftar.ToString("dd-MM-yyyy"),
                            TanggalAkhirObservasi = item.a.TanggalAkhirObservasi.ToString("dd-MM-yyyy"),
                            TanggalAwalBayarDaftar = item.a.TanggalAwalBayarDaftar.ToString("dd-MM-yyyy"),
                            TanggalAwalBayarPendidikan = item.a.TanggalAwalBayarPendidikan.ToString("dd-MM-yyyy"),
                            TanggalAwalDaftar = item.a.TanggalAwalDaftar.ToString("dd-MM-yyyy"),
                            TanggalAwalObservasi = item.a.TanggalAwalObservasi.ToString("dd-MM-yyyy"),
                            TanggalKonfirmasiCadangan = item.a.TanggalKonfirmasiCadangan.ToString("dd-MM-yyyy"),
                            Unit = item.b.Nama
                        };
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbJadwals", ex);
                return null;

            }
        }
        public string PpdbJadwalAdd(int IdUser, PpdbJadwalModel Data)
        {
            try
            {
                if (currPpdb == null)
                    return "ppdb belum diset";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    if (string.IsNullOrEmpty(Data.TanggalAwalDaftar)) return "Tanggal Awal Daftar kosong";
                    if (string.IsNullOrEmpty(Data.TanggalAkhirDaftar)) return "Tanggal Akhir Daftar kosong";
                    if (string.IsNullOrEmpty(Data.TanggalAwalBayarDaftar)) return "Tanggal Awal Bayar Daftar kosong";
                    if (string.IsNullOrEmpty(Data.TanggalAwalObservasi)) return "Tanggal Awal Observasi kosong";
                    if (string.IsNullOrEmpty(Data.TanggalAkhirObservasi)) return "Tanggal Akhir Observasi kosong";
                    if (string.IsNullOrEmpty(Data.TanggalAwalBayarPendidikan)) return "Tanggal Awal Bayar Pendidikan kosong";
                    if (string.IsNullOrEmpty(Data.TanggalAkhirBayarPendidikan)) return "Tanggal Akhir Bayar Pendidikan kosong";
                    if (string.IsNullOrEmpty(Data.TanggalKonfirmasiCadangan)) return "Tanggal Konfirmasi Cadangan kosong";

                    using (var tx = conn.BeginTransaction())
                    {
                        TbPpdbJadwal tbPpdbJadwal = new TbPpdbJadwal
                        {
                            IdPpdb = currPpdb.IdPpdb,
                            IdUnit = Data.IdUnit,
                            TanggalAwalDaftar = dateTimeService.StrToDateTime(Data.TanggalAwalDaftar),
                            TanggalAkhirDaftar = dateTimeService.StrToDateTime(Data.TanggalAkhirDaftar),
                            TanggalAwalBayarDaftar = dateTimeService.StrToDateTime(Data.TanggalAwalBayarDaftar),
                            TanggalAkhirBayarDaftar = dateTimeService.StrToDateTime(Data.TanggalAkhirBayarDaftar),
                            TanggalAwalObservasi = dateTimeService.StrToDateTime(Data.TanggalAwalObservasi),
                            TanggalAkhirObservasi = dateTimeService.StrToDateTime(Data.TanggalAkhirObservasi),
                            TanggalAwalBayarPendidikan = dateTimeService.StrToDateTime(Data.TanggalAwalBayarPendidikan),
                            TanggalAkhirBayarPendidikan = dateTimeService.StrToDateTime(Data.TanggalAkhirBayarPendidikan),
                            TanggalKonfirmasiCadangan = dateTimeService.StrToDateTime(Data.TanggalAkhirBayarDaftar)

                        };

                        if (!commonService.DictCheck(GelombangPpdbConstant.DictGelombangPpdb, (int)Data.IdJenisGelombangPpdb))
                            return "gelombang PPDB salah";
                        tbPpdbJadwal.IdJenisGelombangPpdb = Data.IdJenisGelombangPpdb;


                        if (!commonService.DictCheck(JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb, (int)Data.IdJenisPendaftaran))
                            return "jenis PPDB salah";
                        tbPpdbJadwal.IdJenisPendaftaran = Data.IdJenisPendaftaran;

                        if (!commonService.DictCheck(JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb, (int)Data.IdJalurPendaftaran))
                            return "jalur PPDB salah";
                        tbPpdbJadwal.IdJalurPendaftaran = Data.IdJalurPendaftaran;

                        conn.Insert(tbPpdbJadwal);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PpdbJadwalAdd", ex);

            }

        }
        public string PpdbJadwalEdit(int IdUser, PpdbJadwalEditModel Data)
        {
            try
            {
                if (currPpdb == null)
                    return "ppdb belum diset";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    if (string.IsNullOrEmpty(Data.TanggalAwalDaftar)) return "Tanggal Awal Daftar kosong";
                    if (string.IsNullOrEmpty(Data.TanggalAkhirDaftar)) return "Tanggal Akhir Daftar kosong";
                    if (string.IsNullOrEmpty(Data.TanggalAwalBayarDaftar)) return "Tanggal Awal Bayar Daftar kosong";
                    if (string.IsNullOrEmpty(Data.TanggalAwalObservasi)) return "Tanggal Awal Observasi kosong";
                    if (string.IsNullOrEmpty(Data.TanggalAkhirObservasi)) return "Tanggal Akhir Observasi kosong";
                    if (string.IsNullOrEmpty(Data.TanggalAwalBayarPendidikan)) return "Tanggal Awal Bayar Pendidikan kosong";
                    if (string.IsNullOrEmpty(Data.TanggalAkhirBayarPendidikan)) return "Tanggal Akhir Bayar Pendidikan kosong";
                    if (string.IsNullOrEmpty(Data.TanggalKonfirmasiCadangan)) return "Tanggal Konfirmasi Cadangan kosong";

                    using (var tx = conn.BeginTransaction())
                    {
                        TbPpdbJadwal tbPpdbJadwal = (from a in conn.GetList<TbPpdbJadwal>()
                                                     where a.IdPpdb == currPpdb.IdPpdb
                                                     & a.IdUnit == Data.IdUnit
                                                     & a.IdJenisGelombangPpdb == Data.IdJenisGelombangPpdb
                                                     & a.IdJenisPendaftaran == Data.IdJenisPendaftaran
                                                     & a.IdJalurPendaftaran == Data.IdJalurPendaftaran
                                                     select a).FirstOrDefault();
                        if (tbPpdbJadwal == null) return "data tidak ada";

                        tbPpdbJadwal.IdPpdb = currPpdb.IdPpdb;
                        tbPpdbJadwal.IdUnit = Data.IdUnit;
                        tbPpdbJadwal.TanggalAwalDaftar = dateTimeService.StrToDateTime(Data.TanggalAwalDaftar);
                        tbPpdbJadwal.TanggalAkhirDaftar = dateTimeService.StrToDateTime(Data.TanggalAkhirDaftar);
                        tbPpdbJadwal.TanggalAwalBayarDaftar = dateTimeService.StrToDateTime(Data.TanggalAwalBayarDaftar);
                        tbPpdbJadwal.TanggalAkhirBayarDaftar = dateTimeService.StrToDateTime(Data.TanggalAkhirBayarDaftar);
                        tbPpdbJadwal.TanggalAwalObservasi = dateTimeService.StrToDateTime(Data.TanggalAwalObservasi);
                        tbPpdbJadwal.TanggalAkhirObservasi = dateTimeService.StrToDateTime(Data.TanggalAkhirObservasi);
                        tbPpdbJadwal.TanggalAwalBayarPendidikan = dateTimeService.StrToDateTime(Data.TanggalAwalBayarPendidikan);
                        tbPpdbJadwal.TanggalAkhirBayarPendidikan = dateTimeService.StrToDateTime(Data.TanggalAkhirBayarPendidikan);
                        tbPpdbJadwal.TanggalKonfirmasiCadangan = dateTimeService.StrToDateTime(Data.TanggalAkhirBayarDaftar);

                        if (!commonService.DictCheck(GelombangPpdbConstant.DictGelombangPpdb, (int)Data.IdJenisGelombangPpdb))
                            return "gelombang PPDB salah";
                        tbPpdbJadwal.IdJenisGelombangPpdb = Data.IdJenisGelombangPpdb;


                        if (!commonService.DictCheck(JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb, (int)Data.IdJenisPendaftaran))
                            return "jenis PPDB salah";
                        tbPpdbJadwal.IdJenisPendaftaran = Data.IdJenisPendaftaran;

                        if (!commonService.DictCheck(JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb, (int)Data.IdJalurPendaftaran))
                            return "jalur PPDB salah";
                        tbPpdbJadwal.IdJalurPendaftaran = Data.IdJalurPendaftaran;

                        conn.Update(tbPpdbJadwal);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PpdbJadwalEdit", ex);

            }
        }
        public string PpdbJadwalDelete(int Iduser, int IdUnit, int IdJenisGelombangPpdb, int IdJenisPendaftaran, int IdJalurPendaftaran)
        {
            try
            {
                if (currPpdb == null)
                    return "ppdb belum diset";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();

                    using (var tx = conn.BeginTransaction())
                    {
                        TbPpdbJadwal tbPpdbJadwal = (from a in conn.GetList<TbPpdbJadwal>()
                                                     where a.IdPpdb == currPpdb.IdPpdb
                                                     & a.IdUnit == IdUnit
                                                     & a.IdJenisGelombangPpdb == IdJenisGelombangPpdb
                                                     & a.IdJenisPendaftaran == IdJenisPendaftaran
                                                     & a.IdJalurPendaftaran == IdJalurPendaftaran
                                                     select a).FirstOrDefault();
                        if (tbPpdbJadwal == null) return "data tidak ada";

                        conn.Delete(tbPpdbJadwal);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "PpdbJadwalDelete", ex);

            }
        }
        public List<PpdbDaftarModel> GetPpdbDaftars(int IdUser, int Status, out string oMessage)
        {
            oMessage = string.Empty;
            List<PpdbDaftarModel> ret = new List<PpdbDaftarModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var user = (from a in conn.GetList<TbUnitPegawai>()
                                join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                join d in conn.GetList<TbUserRole>() on a.IdPegawai equals d.IdUser
                                where a.IdPegawai == IdUser
                                select new { a, b, c, d }).FirstOrDefault();
                    if (user == null) { oMessage = "pengguna tidak terdaftar sebagai pegawai"; return null; }

                    bool isAdmin = false;
                    int[] _listUnit = new int[] { UnitConstant.DIR, UnitConstant.AKT, UnitConstant.FNC };
                    isAdmin = _listUnit.Contains(user.a.IdUnit);

                    if (!isAdmin)
                    {
                        int[] _lisRole = new int[] { RoleApplConstant.Admin, RoleApplConstant.Admin2 };
                        isAdmin = _lisRole.Contains(user.d.IdRole);
                    }

                    var data = from a in conn.GetList<TbPpdbDaftar>()
                               join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                               join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                               join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                               join e in conn.GetList<TbSekolah>() on d.IdSekolah equals e.IdSekolah
                               where a.Status == (Status == 0 ? a.Status : Status)
                               & c.IdUnit == (isAdmin ? c.IdUnit : user.a.IdUnit)
                               select new { a, b, c, d, e };
                    foreach (var item in data.OrderByDescending(x => x.a.CreatedDate))
                    {
                        string peminatan = string.Empty;
                        if (item.b.IdJenisPeminatan != null)
                            if (item.d.IdUnit == UnitConstant.Sma)
                            {
                                peminatan = JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb[(int)item.b.IdJenisPeminatan];
                            }
                        ret.Add(new PpdbDaftarModel
                        {
                            Email = item.a.Email,
                            IdJalurPendaftaran = item.a.IdJalurPendaftaran,
                            IdJenisKategoriPendaftaran = item.a.IdJenisKategoriPendaftaran,
                            IdJenisPendaftaran = item.a.IdJenisPendaftaran,
                            IdPpdbDaftar = item.a.IdPpdbDaftar,
                            IdPpdbKelas = item.a.IdPpdbKelas,
                            KdJenisKelamin = item.a.KdJenisKelamin,
                            Nama = item.a.Nama,
                            NoHandphone = item.a.NoHandphone,
                            Pin = item.a.Pin,
                            NikOrtu = item.a.NikOrtu,
                            NisSiswa = item.a.NisSiswa,
                            NisSodara = item.a.NisSodara,
                            Status = StatusPpdbDaftarConstant.Dict[item.a.Status],
                            JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[item.a.IdJalurPendaftaran],
                            JenisKategoriPendaftaran = KategoriPendaftaranPpdbConstant.DictKategoriPendaftaranPpdb[item.a.IdJenisKategoriPendaftaran],
                            JenisPendaftaran = JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[item.a.IdJenisPendaftaran],
                            TempatLahir = item.a.TempatLahir,
                            TanggalLahir = item.a.TanggalLahir.ToString("dd-MM-yyyy"),
                            Peminatan = peminatan,
                            Sekolah = item.e.Nama,
                            Unit = item.d.Nama,
                            Kelas = item.c.Nama
                        });
                    }
                }
                if (ret.Count == 0) { oMessage = "data tidak ada"; }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbDaftars", ex);
                return null;
            }
        }
        public PpdbBiayaDaftarModel GetPpdbDaftarBiaya(int IdPpdbDaftar, out string oMessage)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    return schService.GetPpdbDaftarBiaya(conn, IdPpdbDaftar, out oMessage);
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbDaftarBiaya", ex);
                return null;
            }
        }
        public string SetLunasBiayaDaftar(int IdUser, int IdPpdbDaftar, string TanggalLunas, string Catatan)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var user = (from a in conn.GetList<TbUnitPegawai>()
                                    join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                    join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                    where a.IdPegawai == IdUser
                                    select new { a, b, c }).FirstOrDefault();
                        if (user == null)
                            return "pengguna tidak terdaftar sebagai pegawai";

                        var tagPpdbDaftar = schService.GetPpdbDaftarBiaya(conn, IdPpdbDaftar, out string err);
                        if (!string.IsNullOrEmpty(err)) return err;
                        if (tagPpdbDaftar.StatusLunas == StatusLunasConstant.Dict[StatusLunasConstant.Lunas])
                            return "tagihan sudah lunas";
                        var dt = dateTimeService.StrToDateTime(TanggalLunas, out err);
                        if (!string.IsNullOrEmpty(err)) return err;

                        string refNo = string.Format("{0} {1} {2}", dateTimeService.GetCurrdate().ToString("yyyyMMddHH24mmss"), IdPpdbDaftar, IdUser);
                        err = schService.SetLunasBiayaDaftar(conn, IdUser, refNo, IdPpdbDaftar, (DateTime)dt, tagPpdbDaftar.RpTotal, Catatan);
                        if (!string.IsNullOrEmpty(err)) return err;
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "GetPpdbDaftarBiaya", ex);
            }
        }
        public string SetTidakDaftarTunggu(int IdUser, int IdPpdbDaftar)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var user = (from a in conn.GetList<TbUnitPegawai>()
                                    join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                    join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                    where a.IdPegawai == IdUser
                                    select new { a, b, c }).FirstOrDefault();
                        if (user == null)
                            return "pengguna tidak terdaftar sebagai pegawai";

                        var dataDaftar = (from a in conn.GetList<TbPpdbDaftar>()
                                          join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                          join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                          join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                          join e in conn.GetList<TbSekolah>() on d.IdSekolah equals e.IdSekolah
                                          where a.IdPpdbDaftar == IdPpdbDaftar
                                          select new { a, b, c, d, e }).FirstOrDefault();
                        if (dataDaftar == null) return "data tidak ada";
                        if (user.a.IdUnit != dataDaftar.c.IdUnit) return "unit pengguna tidak sesuai";
                        if (dataDaftar.a.Status != StatusPpdbDaftarConstant.DaftarTunggu) return "bukan daftar tunggu pendaftaran";

                        TbPpdbDaftar tbPpdbDaftarUpdate = dataDaftar.a;
                        tbPpdbDaftarUpdate.Status = StatusPpdbDaftarConstant.AktifVa;
                        conn.Update(tbPpdbDaftarUpdate);

                        string biayaPendaftaran = string.Empty;
                        TbPpdbBiayaDaftar tbPpdbBiayaDaftar = new TbPpdbBiayaDaftar
                        {
                            IdPpdbDaftar = dataDaftar.a.IdPpdbDaftar,
                            IsCicil = 0,
                            Status = StatusLunasConstant.BelumLunas,
                            RefNo = null,
                            TanggalLunas = null,
                            UpdatedBy = null,
                            UpdatedDate = null
                        };
                        conn.Insert(tbPpdbBiayaDaftar);

                        TbPpdbBiaya tbPpdbBiaya = (from a in conn.GetList<TbPpdbBiaya>()
                                                   where a.IdPpdb == currPpdb.IdPpdb
                                                   & a.IdJenisBiayaPpdb == 90 //hati2 soalnya dihardoce
                                                   select a).FirstOrDefault();
                        if (tbPpdbBiaya == null) return "biaya ppdb belum diset";

                        TbPpdbBiayaDaftarDetil tbPpdbBiayaDaftarDetil = new TbPpdbBiayaDaftarDetil
                        {
                            IdPpdbDaftar = dataDaftar.a.IdPpdbDaftar,
                            IdJenisBiayaPpdb = tbPpdbBiaya.IdJenisBiayaPpdb,
                            Rp = tbPpdbBiaya.Rp,
                            Catatan = null
                        };
                        biayaPendaftaran = commonService.SetStrRupiah(tbPpdbBiaya.Rp);
                        conn.Insert(tbPpdbBiayaDaftarDetil);

                        //send mail
                        string _messageBody1 = string.Empty;
                        string _messageBody2 = string.Empty;
                        string _subjects = "PPDB";

                        List<string> emailTos = new List<string>();
                        List<string> emailCcs = new List<string>();
                        List<string> _attachments = new List<string>();

                        int jenisRedaksiEmail = JenisRedaksiEmailPpdbConstant.Registrasi;
                        _subjects = JenisRedaksiEmailPpdbConstant.Dict[jenisRedaksiEmail];
                        string oMessage = schService.GetRedaksiEmailPpdbBody(jenisRedaksiEmail, out _messageBody1, out _messageBody2);
                        if (!string.IsNullOrEmpty(oMessage))
                            return string.Empty;
                        _messageBody1 = _messageBody1.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                        _messageBody2 = _messageBody2.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                        _messageBody2 += Environment.NewLine + string.Format("no va: {0}, biaya pendaftaran: Rp. {1}", dataDaftar.a.IdPpdbDaftar, biayaPendaftaran);


                        emailTos.Add(dataDaftar.a.Email);
                        emailCcs.Add("ppdb.sekolahattaufiqbogor@gmail.com");
                        string _emailMessage = schService.SetHtmlEmailPpdbMessage(
                            _messageBody1,
                            _messageBody2,
                            dataDaftar.a.IdPpdbDaftar,
                            dataDaftar.d.Nama,
                            JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[dataDaftar.a.IdJalurPendaftaran],
                            JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[dataDaftar.a.IdJenisPendaftaran],
                            dataDaftar.a.Nama,
                            dataDaftar.a.TanggalLahir.ToString("dd-MM-yyyy"),
                            currPpdb.TahunAjaran,
                            dataDaftar.e.Nama,
                            currPpdb.NamaKetua);
                        SendEmailModel email = new SendEmailModel
                        {
                            To = emailTos,
                            Cc = emailCcs,
                            Title = "Bapak/Ibu ",
                            Subject = _subjects,
                            Message = _emailMessage,
                            Attachments = _attachments
                        };
                        _ = emailService.SendHtmlEmailAsync(email);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetTidakDaftarTunggu", ex);
            }

        }
        public PpdbDaftarModel GetPpdbDaftar(int IdPpdbDaftar, out string oMessage)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    return schService.GetPpdbDaftar(conn, IdPpdbDaftar, out oMessage);
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbDaftar", ex);
                return null;
            }
        }
        public PpdbDaftarModel GetPpdbDaftar(int IdPpdbDaftar, string KodePin, out string oMessage)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    if (IdPpdbDaftar == 0)
                    {
                        oMessage = "nomor ppdb wajib diisi";
                        return null;
                    }
                    if (string.IsNullOrEmpty(KodePin))
                    {
                        oMessage = "kode pin wajib diisi";
                        return null;
                    }
                    var data = schService.GetPpdbDaftar(conn, IdPpdbDaftar, out oMessage);
                    if (data != null)
                    {
                        if (!string.IsNullOrEmpty(data.Pin))
                        {
                            if (data.Pin != KodePin)
                            {
                                oMessage = "PIN Anda Salah";
                                return null;
                            }
                        }
                    }

                    return data;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbDaftar", ex);
                return null;
            }
        }
        public List<PpdbSiswaListModel> GetPpdbSiswas(int IdUser, int Status, out string oMessage)
        {
            oMessage = string.Empty;
            List<PpdbSiswaListModel> ret = new List<PpdbSiswaListModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var user = (from a in conn.GetList<TbUnitPegawai>()
                                join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                join d in conn.GetList<TbUserRole>() on a.IdPegawai equals d.IdUser
                                where a.IdPegawai == IdUser
                                select new { a, b, c, d }).FirstOrDefault();
                    if (user == null) { oMessage = "pengguna tidak terdaftar sebagai pegawai"; return null; }

                    bool isAdmin = false;
                    int[] _listUnit = new int[] { UnitConstant.DIR, UnitConstant.AKT, UnitConstant.FNC };
                    isAdmin = _listUnit.Contains(user.a.IdUnit);

                    if (!isAdmin)
                    {
                        int[] _lisRole = new int[] { RoleApplConstant.Admin, RoleApplConstant.Admin2 };
                        isAdmin = _lisRole.Contains(user.d.IdRole);
                    }


                    string strWhereIn = string.Empty;
                    string strWhereNotExists = string.Empty;
                    switch (Status)
                    {
                        case StatusPpdbConstant.TestObservasi:
                            strWhereIn = string.Format("where status in ({0})", StatusPpdbConstant.Registrasi);
                            strWhereNotExists = string.Format("where status in ({0},{1})", StatusPpdbConstant.Pengesahan, StatusPpdbConstant.TestObservasi);
                            break;
                        case StatusPpdbConstant.Wawancara:
                            strWhereIn = string.Format("where status in ({0})", StatusPpdbConstant.Registrasi);
                            strWhereNotExists = string.Format("where status in ({0},{1})", StatusPpdbConstant.Pengesahan, StatusPpdbConstant.Wawancara);
                            break;
                        case StatusPpdbConstant.InputObservasi:
                            strWhereIn = string.Format("where status in ({0})", StatusPpdbConstant.TestObservasiWawancara);
                            strWhereNotExists = string.Format("where status in ({0},{1})", StatusPpdbConstant.Pengesahan, StatusPpdbConstant.InputObservasi);
                            break;
                        case StatusPpdbConstant.VerifikasiObservasi:
                            strWhereIn = string.Format("where status in ({0})", StatusPpdbConstant.InputObservasi);
                            strWhereNotExists = string.Format("where status in ({0},{1})", StatusPpdbConstant.Pengesahan, StatusPpdbConstant.VerifikasiObservasi);
                            break;
                        case StatusPpdbConstant.PelunasanBiayaPendidikan:
                            strWhereIn = string.Format("where status in ({0}) and status_proses in ({1})", StatusPpdbConstant.VerifikasiObservasi, StatusPpdbVerifikasiObservasiConstant.Diterima);
                            strWhereNotExists = string.Format("where status in ({0},{1})", StatusPpdbConstant.Pengesahan, StatusPpdbConstant.PelunasanBiayaPendidikan);
                            break;
                        case StatusPpdbConstant.Pengesahan:
                            strWhereIn = string.Format("where status in ({0})", StatusPpdbConstant.PelunasanBiayaPendidikan);
                            strWhereNotExists = string.Format("where status in ({0})", StatusPpdbConstant.Pengesahan);
                            break;
                        case -1: //untuk undir diri, ditangguhkan/cadangan, tidak diterima
                            strWhereIn = string.Format("where status in ({0}) and status_proses in ({1}, {2}, {3})",
                                StatusPpdbConstant.VerifikasiObservasi,
                                StatusPpdbVerifikasiObservasiConstant.UndurDiri,
                                StatusPpdbVerifikasiObservasiConstant.Ditangguhkan,
                                StatusPpdbVerifikasiObservasiConstant.TidakDiterima);
                            strWhereNotExists = string.Format("where status in ({0},{1})", StatusPpdbConstant.Pengesahan, StatusPpdbConstant.PelunasanBiayaPendidikan);
                            break;
                        default:
                            strWhereIn = string.Format("where status in ({0})", StatusPpdbConstant.Registrasi);
                            strWhereNotExists = string.Format("where status in ({0},{1},{2},{3},{4},{5},{6})",
                                StatusPpdbConstant.TestObservasi,
                                StatusPpdbConstant.Wawancara,
                                StatusPpdbConstant.TestObservasiWawancara,
                                StatusPpdbConstant.InputObservasi,
                                StatusPpdbConstant.VerifikasiObservasi,
                                StatusPpdbConstant.PelunasanBiayaPendidikan,
                                StatusPpdbConstant.Pengesahan
                                );
                            break;
                    }
                    var data = from a in conn.GetList<TbPpdbSiswa>()
                               join a1 in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals a1.IdPpdbKelas
                               join b in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals b.IdPpdbDaftar
                               join c in conn.GetList<TbKelas>() on a1.IdKelas equals c.IdKelas
                               join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                               where c.IdUnit == (isAdmin ? c.IdUnit : user.a.IdUnit)
                               & !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == b.IdPpdbDaftar)
                               select new { a, a1, b, c, d };
                    foreach (var item in data)
                    {
                        string peminatan = string.Empty;
                        if (item.a1.IdJenisPeminatan != null)
                            if (item.d.IdUnit == UnitConstant.Sma)
                            {
                                peminatan = JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb[(int)item.a1.IdJenisPeminatan];
                            }
                        PpdbSiswaListModel m = new PpdbSiswaListModel
                        {
                            IdPpdbDaftar = item.a.IdPpdbDaftar,
                            Unit = item.d.Nama,
                            Kelas = item.c.Nama,
                            Peminatan = peminatan,
                            JenisKategoriPendaftaran = KategoriPendaftaranPpdbConstant.DictKategoriPendaftaranPpdb[item.a.IdJenisKategoriPendaftaran],
                            JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[item.a.IdJalurPendaftaran],
                            JenisPendaftaran = JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[item.a.IdJenisPendaftaran],
                            Nama = item.a.Nama,
                            TanggalLahir = item.a.TanggalLahir.ToString("dd-MM-yyyy"),
                            Status = StatusPpdbConstant.Dict[item.b.Status],
                            SekolahAsal = item.a.SekolahAsal,
                            StatusProses = "",
                            NamaPegawai = "",
                            NamaSodara = ""
                        };

                        if (item.b.Status == StatusPpdbConstant.TestObservasi)
                        {
                            m.StatusProses = StatusPpdbTestObservasiConstant.Dict[item.b.StatusProses];
                        }
                        else if (item.b.Status == StatusPpdbConstant.Wawancara)
                        {
                            m.StatusProses = StatusPpdbWawancaraConstant.Dict[item.b.StatusProses];
                        }
                        else if (item.b.Status == StatusPpdbConstant.TestObservasiWawancara)
                        {
                            m.StatusProses = StatusPpdbTestObservasiWawancaraConstant.Dict[item.b.StatusProses];
                        }
                        else if (item.b.Status == StatusPpdbConstant.InputObservasi)
                        {
                            m.StatusProses = StatusPpdbHasilObservasiConstant.Dict[item.b.StatusProses];
                        }
                        else if (item.b.Status == StatusPpdbConstant.VerifikasiObservasi)
                        {
                            m.StatusProses = StatusPpdbVerifikasiObservasiConstant.Dict[item.b.StatusProses];
                        }
                        if (item.a.IdSiswa != null)
                            m.SekolahAsal = conn.GetList<TbSekolah>().FirstOrDefault().Nama;
                        if (item.a.IdSiswaSodara != null)
                            m.NamaSodara = conn.Get<TbSiswa>(item.a.IdSiswaSodara).Nama;
                        if (item.a.IdPegawai != null)
                        {
                            m.NamaPegawai = (from a in conn.GetList<TbUser>()
                                             where a.IdUser == item.a.IdPegawai
                                             select userAppService.SetFullName(a.FirstName, a.MiddleName, a.LastName)).FirstOrDefault();
                        }
                        ret.Add(m);
                    }
                }
                if (ret.Count == 0) { oMessage = "data tidak ada"; }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbSiswas", ex);
                return null;
            }
        }
        public PpdbSiswaModel GetPpdbSiswa(int IdPpdbDaftar, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    return schService.GetPpdbSiswa(conn, IdPpdbDaftar, out oMessage);
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbSiswas", ex);
                return null;
            }
        }
        private string SetHadirTestObservasiWawancara(IDbConnection conn, int IdUser, int IdPpdbDaftar, int Status)
        {
            try
            {
                TbPpdbSiswaStatus tbPpdbSiswaStatus = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                                       where a.IdPpdbDaftar == IdPpdbDaftar
                                                       & a.Status == Status
                                                       select a).FirstOrDefault();
                bool isInsert = false;
                if (tbPpdbSiswaStatus != null)
                {
                    TbPpdbSiswaStatus tbPpdbSiswaStatusObservasiWawancara = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                                                             where a.IdPpdbDaftar == IdPpdbDaftar
                                                                             & a.Status == StatusPpdbConstant.TestObservasiWawancara
                                                                             select a).FirstOrDefault();
                    if (tbPpdbSiswaStatusObservasiWawancara == null)
                    {
                        tbPpdbSiswaStatusObservasiWawancara = new TbPpdbSiswaStatus();
                        isInsert = true;
                    }
                    else
                    {
                        isInsert = false;
                    }
                    tbPpdbSiswaStatusObservasiWawancara.Status = StatusPpdbConstant.TestObservasiWawancara;
                    tbPpdbSiswaStatusObservasiWawancara.StatusProses = StatusPpdbTestObservasiWawancaraConstant.Hadir;
                    tbPpdbSiswaStatusObservasiWawancara.CreatedBy = IdUser;
                    tbPpdbSiswaStatusObservasiWawancara.CreatedDate = dateTimeService.GetCurrdate();
                    tbPpdbSiswaStatusObservasiWawancara.IdPpdbDaftar = IdPpdbDaftar;
                    if (isInsert)
                    {
                        conn.Insert(tbPpdbSiswaStatusObservasiWawancara);
                    }
                    else
                    {
                        conn.Update(tbPpdbSiswaStatusObservasiWawancara);
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetHadirTestObservasiWawancara", ex);
            }
        }
        public string SetHadirTestObservasi(int IdUser, int IdPpdbDaftar, string Catatan)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbPpdbSiswa tbPpdbSiswa = conn.Get<TbPpdbSiswa>(IdPpdbDaftar);
                        if (tbPpdbSiswa == null) return "data tidak ada";
                        TbPpdbSiswaStatus tbPpdbSiswaStatus = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                                               where a.IdPpdbDaftar == IdPpdbDaftar
                                                               & a.Status == StatusPpdbConstant.TestObservasi
                                                               select a).FirstOrDefault();
                        bool isInsert = false;
                        if (tbPpdbSiswaStatus == null)
                        {
                            tbPpdbSiswaStatus = new TbPpdbSiswaStatus();
                            isInsert = true;
                        }

                        tbPpdbSiswaStatus.Status = StatusPpdbConstant.TestObservasi;
                        tbPpdbSiswaStatus.StatusProses = StatusPpdbTestObservasiConstant.Hadir;
                        tbPpdbSiswaStatus.CreatedBy = IdUser;
                        tbPpdbSiswaStatus.CreatedDate = dateTimeService.GetCurrdate();
                        tbPpdbSiswaStatus.IdPpdbDaftar = IdPpdbDaftar;
                        tbPpdbSiswaStatus.CatatanProses = Catatan;
                        if (isInsert)
                        {
                            conn.Insert(tbPpdbSiswaStatus);
                        }
                        else
                        {
                            conn.Update(tbPpdbSiswaStatus);
                        }
                        string err = this.SetHadirTestObservasiWawancara(conn, IdUser, IdPpdbDaftar, StatusPpdbConstant.Wawancara);
                        if (!string.IsNullOrEmpty(err)) return err;
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetHadirTestObservasi", ex);
            }
        }
        public string SetHadirWawancara(int IdUser, int IdPpdbDaftar, string Catatan)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbPpdbSiswa tbPpdbSiswa = conn.Get<TbPpdbSiswa>(IdPpdbDaftar);
                        if (tbPpdbSiswa == null) return "data tidak ada";
                        TbPpdbSiswaStatus tbPpdbSiswaStatus = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                                               where a.IdPpdbDaftar == IdPpdbDaftar
                                                               & a.Status == StatusPpdbConstant.Wawancara
                                                               select a).FirstOrDefault();
                        bool isInsert = false;
                        if (tbPpdbSiswaStatus == null)
                        {
                            tbPpdbSiswaStatus = new TbPpdbSiswaStatus();
                            isInsert = true;
                        }

                        tbPpdbSiswaStatus.Status = StatusPpdbConstant.Wawancara;
                        tbPpdbSiswaStatus.StatusProses = StatusPpdbWawancaraConstant.Hadir;
                        tbPpdbSiswaStatus.CreatedBy = IdUser;
                        tbPpdbSiswaStatus.CreatedDate = dateTimeService.GetCurrdate();
                        tbPpdbSiswaStatus.IdPpdbDaftar = IdPpdbDaftar;
                        tbPpdbSiswaStatus.CatatanProses = Catatan;
                        if (isInsert)
                        {
                            conn.Insert(tbPpdbSiswaStatus);
                        }
                        else
                        {
                            conn.Update(tbPpdbSiswaStatus);
                        }
                        string err = this.SetHadirTestObservasiWawancara(conn, IdUser, IdPpdbDaftar, StatusPpdbConstant.TestObservasi);
                        if (!string.IsNullOrEmpty(err)) return err;
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetHadirTestObservasi", ex);
            }
        }
        private string InputObservasi(IDbConnection conn, int IdUser, int IdPpdbDaftar, int StatusHasilObservasi, string Catatan, List<PpdbBiayaSiswaDetilAddModel> Biayas, List<PpdbBiayaSiswaCicilAddModel> Cicils)
        {
            try
            {
                if (Biayas != null)
                {
                    TbPpdbBiayaSiswa tbPpdbBiayaSiswa = conn.Get<TbPpdbBiayaSiswa>(IdPpdbDaftar);
                    if (tbPpdbBiayaSiswa == null) return "data tidak ada";
                    tbPpdbBiayaSiswa.Catatan = "Edit Biaya Hasil Observasi";
                    if (Cicils.Count() != 0)
                        tbPpdbBiayaSiswa.IsCicil = 1;
                    conn.Update(tbPpdbBiayaSiswa);
                    foreach (var item in Biayas)
                    {
                        TbPpdbBiayaSiswaDetil tbPpdbBiayaSiswaDetil = (from a in conn.GetList<TbPpdbBiayaSiswaDetil>()
                                                                       where a.IdPpdbDaftar == IdPpdbDaftar
                                                                       & a.IdJenisBiayaPpdb == item.IdJenisBiayaPpdb
                                                                       select a).FirstOrDefault();
                        if (tbPpdbBiayaSiswaDetil != null)
                        {
                            conn.Delete(tbPpdbBiayaSiswaDetil);
                        }
                        tbPpdbBiayaSiswaDetil = new TbPpdbBiayaSiswaDetil
                        {
                            IdPpdbDaftar = IdPpdbDaftar,
                            IdJenisBiayaPpdb = item.IdJenisBiayaPpdb,
                            Catatan = item.Catatan,
                            Rp = item.Rp
                        };
                        conn.Insert(tbPpdbBiayaSiswaDetil);
                    }
                }
                var dataCicils = from a in conn.GetList<TbPpdbBiayaSiswaCicil>()
                                 where a.IdPpdbDaftar == IdPpdbDaftar
                                 select a;
                foreach (var item in dataCicils)
                {
                    conn.Delete(item);
                }
                if (Cicils.Count() != 0)
                {
                    double totCicil = Cicils.Sum(x => x.RpCilKe);
                    double totBiaya = Biayas.Sum(x => x.Rp);
                    if (totCicil != totBiaya)
                        return "total cicilan tidak sama dengan total biaya pendidikan";

                    foreach (var item in Cicils)
                    {
                        TbPpdbBiayaSiswaCicil tbPpdbBiayaSiswaCicil = new TbPpdbBiayaSiswaCicil
                        {
                            IdPpdbDaftar = IdPpdbDaftar,
                            CilKe = item.CilKe,
                            RpCilKe = item.RpCilKe,
                            TanggalCilKe = dateTimeService.StrToDateTime(item.TanggalCilKe),
                            Status = StatusLunasConstant.BelumLunas,
                            TanggalLunas = null
                        };
                        conn.Insert(tbPpdbBiayaSiswaCicil);
                    }
                }
                TbPpdbSiswaStatus tbPpdbSiswaStatus = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                                       where a.IdPpdbDaftar == IdPpdbDaftar
                                                       & a.Status == StatusPpdbConstant.InputObservasi
                                                       select a).FirstOrDefault();
                bool isInsert = false;
                if (tbPpdbSiswaStatus == null)
                {
                    tbPpdbSiswaStatus = new TbPpdbSiswaStatus();
                    isInsert = true;
                }

                tbPpdbSiswaStatus.Status = StatusPpdbConstant.InputObservasi;
                tbPpdbSiswaStatus.StatusProses = StatusHasilObservasi;
                tbPpdbSiswaStatus.CreatedBy = IdUser;
                tbPpdbSiswaStatus.CreatedDate = dateTimeService.GetCurrdate();
                tbPpdbSiswaStatus.IdPpdbDaftar = IdPpdbDaftar;
                tbPpdbSiswaStatus.CatatanProses = Catatan;
                if (isInsert)
                {
                    conn.Insert(tbPpdbSiswaStatus);
                }
                else
                {
                    conn.Update(tbPpdbSiswaStatus);
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "InputObservasi", ex);
            }
        }
        public string InputObservasi(int IdUser, int IdPpdbDaftar, int StatusHasilObservasi, string Catatan, List<PpdbBiayaSiswaDetilAddModel> Biayas, List<PpdbBiayaSiswaCicilAddModel> Cicils)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string err = this.InputObservasi(conn, IdUser, IdPpdbDaftar, StatusHasilObservasi, Catatan, Biayas, Cicils);
                        if (!string.IsNullOrEmpty(err)) return err;
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "InputObservasi", ex);
            }
        }
        public string VerifikasiObservasi(int IdUser, int IdPpdbDaftar, int StatusVerfikasiObservasi, string Catatan)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var dataDaftar = (from a in conn.GetList<TbPpdbSiswa>()
                                          join a1 in conn.GetList<TbPpdbBiayaSiswa>() on a.IdPpdbDaftar equals a1.IdPpdbDaftar
                                          join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                          join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                          join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                          join e in conn.GetList<TbSekolah>() on d.IdSekolah equals e.IdSekolah
                                          where a.IdPpdbDaftar == IdPpdbDaftar
                                          select new { a, a1, b, c, d, e }).FirstOrDefault();
                        if (dataDaftar == null) return "data tidak ada";

                        TbPpdbSiswaStatus tbPpdbSiswaStatus = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                                               where a.IdPpdbDaftar == IdPpdbDaftar
                                                               & a.Status == StatusPpdbConstant.VerifikasiObservasi
                                                               select a).FirstOrDefault();
                        bool isInsert = false;
                        if (tbPpdbSiswaStatus == null)
                        {
                            tbPpdbSiswaStatus = new TbPpdbSiswaStatus();
                            isInsert = true;
                        }

                        tbPpdbSiswaStatus.Status = StatusPpdbConstant.VerifikasiObservasi;
                        tbPpdbSiswaStatus.StatusProses = StatusVerfikasiObservasi;
                        tbPpdbSiswaStatus.CreatedBy = IdUser;
                        tbPpdbSiswaStatus.CreatedDate = dateTimeService.GetCurrdate();
                        tbPpdbSiswaStatus.IdPpdbDaftar = IdPpdbDaftar;
                        tbPpdbSiswaStatus.CatatanProses = Catatan;
                        if (isInsert)
                        {
                            conn.Insert(tbPpdbSiswaStatus);
                        }
                        else
                        {
                            conn.Update(tbPpdbSiswaStatus);
                        }
                        //send mail pelunasan biaya pendidikan
                        List<string> emailTos = new List<string>();
                        emailTos.Add(dataDaftar.a.Email);
                        var dataOrtus = from a in conn.GetList<TbPpdbSiswaOrtu>()
                                        where a.IdPpdbDaftar == IdPpdbDaftar
                                        select a;
                        foreach (var ortu in dataOrtus)
                        {
                            if (!string.IsNullOrEmpty(ortu.Email))
                            {
                                if (!emailTos.Contains(ortu.Email.ToLower()))
                                    emailTos.Add(ortu.Email.ToLower());
                            }
                        }
                        List<PpdbBiayaSiswaDetilModel> biayaPendidikans = new List<PpdbBiayaSiswaDetilModel>();
                        var dataBiayaDetils = from a in conn.GetList<TbPpdbBiayaSiswaDetil>()
                                              join b in conn.GetList<TbJenisBiayaPpdb>() on a.IdJenisBiayaPpdb equals b.IdJenisBiayaPpdb
                                              where a.IdPpdbDaftar == IdPpdbDaftar
                                              select new { a, b };
                        double totalBiaya = 0;
                        foreach (var item in dataBiayaDetils)
                        {
                            biayaPendidikans.Add(new PpdbBiayaSiswaDetilModel
                            {
                                Catatan = item.a.Catatan,
                                IdJenisBiayaPpdb = item.a.IdJenisBiayaPpdb,
                                JenisBiayaPpdb = item.b.Nama,
                                Rp = item.a.Rp
                            });
                            totalBiaya += item.a.Rp;
                        }

                        List<PpdbBiayaSiswaCicilAddModel> cicils = new List<PpdbBiayaSiswaCicilAddModel>();
                        if (dataDaftar.a1.IsCicil == 1)
                        {
                            var biayaCicils = from a in conn.GetList<TbPpdbBiayaSiswaCicil>()
                                              where a.IdPpdbDaftar == IdPpdbDaftar
                                              select a;
                            foreach (var biayaCicil in biayaCicils)
                            {
                                cicils.Add(new PpdbBiayaSiswaCicilAddModel
                                {
                                    CilKe = biayaCicil.CilKe,
                                    RpCilKe = biayaCicil.RpCilKe,
                                    TanggalCilKe = biayaCicil.TanggalCilKe.ToString("dd-MM-yyyy")
                                });
                            }
                        }
                        string _messageBody1 = string.Empty;
                        string _messageBody2 = string.Empty;
                        string _subjects = "PPDB";

                        List<string> emailCcs = new List<string>();
                        List<string> _attachments = new List<string>();

                        int jenisRedaksiEmail = 0;
                        switch (StatusVerfikasiObservasi)
                        {
                            case StatusPpdbVerifikasiObservasiConstant.Diterima:
                                jenisRedaksiEmail = JenisRedaksiEmailPpdbConstant.ObservasiDiterima;
                                if (dataDaftar.a1.IsCicil == 1)
                                    jenisRedaksiEmail = JenisRedaksiEmailPpdbConstant.ObservasiDiterimaCicil;
                                break;
                            case StatusPpdbVerifikasiObservasiConstant.Ditangguhkan:
                                jenisRedaksiEmail = JenisRedaksiEmailPpdbConstant.ObservasiDitangguhkan;
                                break;
                            case StatusPpdbVerifikasiObservasiConstant.TidakDiterima:
                                jenisRedaksiEmail = JenisRedaksiEmailPpdbConstant.ObservasiTidakDiterima;
                                break;
                            case StatusPpdbVerifikasiObservasiConstant.UndurDiri:
                                jenisRedaksiEmail = JenisRedaksiEmailPpdbConstant.ObservasiUndurDiri;
                                break;
                            default:
                                return "status hasil observasi salah";
                        }
                        _subjects = JenisRedaksiEmailPpdbConstant.Dict[jenisRedaksiEmail];
                        string oMessage = schService.GetRedaksiEmailPpdbBody(jenisRedaksiEmail, out _messageBody1, out _messageBody2);
                        if (!string.IsNullOrEmpty(oMessage))
                            return string.Empty;


                        if (jenisRedaksiEmail == JenisRedaksiEmailPpdbConstant.ObservasiDiterima)
                        {
                            _messageBody1 = _messageBody1.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);

                            var dataBiayaDetils_ = from a in conn.GetList<TbPpdbBiayaSiswaDetil>()
                                                   join b in conn.GetList<TbJenisBiayaPpdb>() on a.IdJenisBiayaPpdb equals b.IdJenisBiayaPpdb
                                                   where a.IdPpdbDaftar == IdPpdbDaftar
                                                   select new { a, b };
                            double total = 0;
                            foreach (var item in dataBiayaDetils_)
                            {
                                total += item.a.Rp;
                                string rp = commonService.SetStrRupiah(item.a.Rp).Replace(" ", "").Replace("Rp.", "");
                                _messageBody2 = _messageBody2.Replace("[" + item.b.NamaSingkat + "]", rp);
                            }
                            _messageBody2 = _messageBody2.Replace("[RpUp]", "");
                            _messageBody2 = _messageBody2.Replace("[RpBoks]", "");
                            _messageBody2 = _messageBody2.Replace("[RpSpp]", "");
                            _messageBody2 = _messageBody2.Replace("[RpInfaq]", "");
                            _messageBody2 = _messageBody2.Replace("[RpSeragam]", "");
                            _messageBody2 = _messageBody2.Replace("[RpWakaf]", "");
                            _messageBody2 = _messageBody2.Replace("[CatatanWakaf]", "");
                            string totals = commonService.SetStrRupiah(total).Replace(" ", "").Replace("Rp.", "");
                            _messageBody2 = _messageBody2.Replace("[TOTALRP]", totals);
                            // _attachments.Add(AppSetting.PathApplFile + "/tata cara pembayaran ppdb tajur.pdf");
                        }
                        else if (jenisRedaksiEmail == JenisRedaksiEmailPpdbConstant.ObservasiDiterimaCicil)
                        {
                            _messageBody2 = _messageBody2.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                            // _attachments.Add(AppSetting.PathApplFile + "/tata cara pembayaran ppdb tajur.pdf");
                        }
                        else if (jenisRedaksiEmail == JenisRedaksiEmailPpdbConstant.ObservasiDitangguhkan)
                        {
                            _messageBody2 = _messageBody2.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                            // _attachments.Add(AppSetting.PathApplFile + "/tata cara pembayaran ppdb tajur.pdf");
                        }
                        else if (jenisRedaksiEmail == JenisRedaksiEmailPpdbConstant.ObservasiTidakDiterima)
                        {
                            _messageBody2 = _messageBody2.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                        }
                        else if (jenisRedaksiEmail == JenisRedaksiEmailPpdbConstant.ObservasiUndurDiri)
                        {

                        }



                        emailCcs.Add("ppdb.sekolahattaufiqbogor@gmail.com");
                        string _emailMessage = schService.SetHtmlEmailPpdbMessage(
                            _messageBody1,
                            _messageBody2,
                            dataDaftar.a.IdPpdbDaftar,
                            dataDaftar.d.Nama,
                            JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[dataDaftar.a.IdJalurPendaftaran],
                            JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[dataDaftar.a.IdJenisPendaftaran],
                            dataDaftar.a.Nama,
                            dataDaftar.a.TanggalLahir.ToString("dd-MM-yyyy"),
                            currPpdb.TahunAjaran,
                            dataDaftar.e.Nama,
                            currPpdb.NamaKetua);

                        SendEmailModel email = new SendEmailModel
                        {
                            To = emailTos,
                            Cc = emailCcs,
                            Title = "Bapak/Ibu ",
                            Subject = _subjects,
                            Message = _emailMessage,
                            Attachments = _attachments
                        };
                        tx.Commit();
                        var _sentmail = emailService.SendHtmlEmailAsync(email);
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "VerifikasiObservasi", ex);
            }
        }
        public string SetLunasBiayaSiswa(int IdUser, int IdPpdbDaftar, string TanggalLunas, string Catatan, int CilKe)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var user = (from a in conn.GetList<TbUnitPegawai>()
                                    join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                    join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                    where a.IdPegawai == IdUser
                                    select new { a, b, c }).FirstOrDefault();
                        if (user == null)
                            return "pengguna tidak terdaftar sebagai pegawai";

                        var tagPpdbSiswa = schService.GetPpdbSiswa(conn, IdPpdbDaftar, out string err);
                        if (!string.IsNullOrEmpty(err))
                            if (tagPpdbSiswa.StatusBiaya == StatusLunasConstant.Dict[StatusLunasConstant.Lunas])
                                return "tagihan sudah lunas";
                        var dt = dateTimeService.StrToDateTime(TanggalLunas, out err);
                        if (!string.IsNullOrEmpty(err)) return err;

                        string _catatan = Catatan;
                        int? _cilKe = null;
                        if (CilKe > 0)
                        {
                            var cicil = (from a in conn.GetList<TbPpdbBiayaSiswaCicil>()
                                         where a.IdPpdbDaftar == IdPpdbDaftar
                                         & a.CilKe == CilKe
                                         select a).FirstOrDefault();
                            if (cicil == null) return "data cicilan tidak ada";
                            if (cicil.Status == StatusLunasConstant.Lunas) return string.Format("cicilan ke {0} sudah lunas", CilKe);
                            tagPpdbSiswa.RpBiayaTotal = cicil.RpCilKe;
                            _catatan += string.Format("({0})", CilKe);
                            _cilKe = CilKe;
                        }
                        string refNo = string.Format("{0} {1} {2}", dateTimeService.GetCurrdate().ToString("yyyyMMddHH24mmss"), IdPpdbDaftar, IdUser);
                        err = schService.SetLunasBiayaSiswaPrt(conn, IdUser, refNo, IdPpdbDaftar, (DateTime)dt, tagPpdbSiswa.RpBiayaTotal, _catatan, _cilKe);
                        if (!string.IsNullOrEmpty(err)) return err;
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetLunasBiayaSiswa", ex);
            }
        }
        public string BackToHadirObservasi(int IdUser, int IdPpdbDaftar)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var user = (from a in conn.GetList<TbUnitPegawai>()
                                    join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                    join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                    where a.IdPegawai == IdUser
                                    select new { a, b, c }).FirstOrDefault();
                        if (user == null)
                            return "pengguna tidak terdaftar sebagai pegawai";

                        var dataSiswa = (from a in conn.GetList<TbPpdbSiswa>()
                                         join b1 in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals b1.IdPpdbDaftar
                                         join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                         join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                         join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                         join e in conn.GetList<TbSekolah>() on d.IdSekolah equals e.IdSekolah
                                         where a.IdPpdbDaftar == IdPpdbDaftar
                                         & b1.Status == StatusPpdbConstant.VerifikasiObservasi
                                         select new { a, b, b1, c, d, e }).FirstOrDefault();
                        if (dataSiswa == null) return "data tidak ada";
                        if (user.a.IdUnit != dataSiswa.c.IdUnit) return "unit pengguna tidak sesuai";
                        List<int> StatusPendings = new List<int>();
                        StatusPendings.Add(StatusPpdbVerifikasiObservasiConstant.Ditangguhkan);
                        StatusPendings.Add(StatusPpdbVerifikasiObservasiConstant.TidakDiterima);
                        StatusPendings.Add(StatusPpdbVerifikasiObservasiConstant.UndurDiri);
                        if (!StatusPendings.Contains(dataSiswa.b1.StatusProses)) return "bukan daftar tunggu pendaftaran";

                        TbPpdbSiswaStatus tbPpdbSiswaStatus = dataSiswa.b1;
                        conn.Delete(tbPpdbSiswaStatus);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "BackToHadirObservasi", ex);
            }

        }
        public string Pengesahan(int IdUser, int IdPpdbDaftar)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var user = (from a in conn.GetList<TbUnitPegawai>()
                                    join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                    join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                    where a.IdPegawai == IdUser
                                    select new { a, b, c }).FirstOrDefault();
                        if (user == null)
                            return "pengguna tidak terdaftar sebagai pegawai";

                        var dataSiswa = (from a in conn.GetList<TbPpdbSiswa>()
                                         join a1 in conn.GetList<TbPpdbBiayaSiswa>() on a.IdPpdbDaftar equals a1.IdPpdbDaftar
                                         join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                         join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                         join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                         join e in conn.GetList<TbSekolah>() on d.IdSekolah equals e.IdSekolah
                                         where a.IdPpdbDaftar == IdPpdbDaftar
                                         select new { a, a1, b, c, d, e }).FirstOrDefault();
                        if (dataSiswa == null) return "data tidak ada";
                        if (user.a.IdUnit != dataSiswa.c.IdUnit) return "unit pengguna tidak sesuai";
                        if (dataSiswa.a1.Status == StatusLunasConstant.BelumLunas)
                            return "data belum lunas biaya pendidikan";
                        TbPpdbSiswa tbPpdbSiswa = dataSiswa.a;
                        tbPpdbSiswa.NoSkTerima = "";
                        conn.Update(tbPpdbSiswa);
                        TbPpdbSiswaStatus tbPpdbSiswaStatus = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                                               where a.IdPpdbDaftar == IdPpdbDaftar
                                                               & a.Status == StatusPpdbConstant.Pengesahan
                                                               select a).FirstOrDefault();
                        bool isInsert = false;
                        if (tbPpdbSiswaStatus == null)
                        {
                            tbPpdbSiswaStatus = new TbPpdbSiswaStatus();
                            isInsert = true;
                        }

                        tbPpdbSiswaStatus.Status = StatusPpdbConstant.Pengesahan;
                        tbPpdbSiswaStatus.StatusProses = 0;
                        tbPpdbSiswaStatus.CreatedBy = IdUser;
                        tbPpdbSiswaStatus.CreatedDate = dateTimeService.GetCurrdate();
                        tbPpdbSiswaStatus.IdPpdbDaftar = IdPpdbDaftar;
                        tbPpdbSiswaStatus.CatatanProses = null;
                        if (isInsert)
                        {
                            conn.Insert(tbPpdbSiswaStatus);
                        }
                        else
                        {
                            conn.Update(tbPpdbSiswaStatus);
                        }

                        string _messageBody1 = string.Empty;
                        string _messageBody2 = string.Empty;
                        string _subjects = "PPDB";

                        List<string> emailTos = new List<string>();
                        List<string> emailCcs = new List<string>();
                        List<string> _attachments = new List<string>();
                        TbPpdbDaftar tbPpdbDaftar = conn.Get<TbPpdbDaftar>(IdPpdbDaftar);
                        var dataOrtus = from a in conn.GetList<TbPpdbSiswaOrtu>()
                                        where a.IdPpdbDaftar == IdPpdbDaftar
                                        select a;
                        foreach (var ortu in dataOrtus)
                        {
                            if (!string.IsNullOrEmpty(ortu.Email))
                            {
                                if (!emailTos.Contains(ortu.Email.ToLower()))
                                    emailTos.Add(ortu.Email.ToLower());
                            }
                        }
                        int jenisRedaksiEmail = JenisRedaksiEmailPpdbConstant.PengesahanDiterima;
                        _subjects = JenisRedaksiEmailPpdbConstant.Dict[jenisRedaksiEmail];

                        string oMessage = schService.GetRedaksiEmailPpdbBody(jenisRedaksiEmail, out _messageBody1, out _messageBody2);
                        if (!string.IsNullOrEmpty(oMessage))
                            return oMessage;
                        _messageBody1 = _messageBody1.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                        _messageBody2 = _messageBody2.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);

                        if (!emailTos.Contains(tbPpdbDaftar.Email))
                            emailTos.Add(tbPpdbDaftar.Email);
                        emailCcs.Add("ppdb.sekolahattaufiqbogor@gmail.com"); //cc ke bagian keuangan
                        string _emailMessage = schService.SetHtmlEmailPpdbMessage(
                            _messageBody1,
                            _messageBody2,
                            dataSiswa.a.IdPpdbDaftar,
                            dataSiswa.c.Nama,
                            JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[dataSiswa.a.IdJalurPendaftaran],
                            JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[dataSiswa.a.IdJenisPendaftaran],
                            dataSiswa.a.Nama,
                            dataSiswa.a.TanggalLahir.ToString("dd-MM-yyyy"),
                            currPpdb.TahunAjaran,
                            dataSiswa.d.Nama,
                            currPpdb.NamaKetua);

                        SendEmailModel email = new SendEmailModel
                        {
                            To = emailTos,
                            Cc = emailCcs,
                            Title = "Bapak/Ibu ",
                            Subject = _subjects,
                            Message = _emailMessage,
                            Attachments = _attachments
                        };
                        _ = emailService.SendHtmlEmailAsync(email);

                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "BackToHadirObservasi", ex);
            }

        }

        public List<PpdbKuisionerModel> GetPertanyaanKuisioner(int IdUnit, int IdJenisPendaftaran, int IdJalurPendaftaran, int IdJenisPertanyaan, out string oMessage)
        {
            oMessage = string.Empty;
            List<PpdbKuisionerModel> ret = new List<PpdbKuisionerModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = (from a in conn.GetList<TbPpdbKuisionerPertanyaan>()
                                join b in conn.GetList<TbPpdbKuisioner>() on a.IdPpdbKuisionerPertanyaan equals b.IdPpdbKuisionerPertanyaan
                                where b.IdUnit == IdUnit
                                && b.IdJenisPendaftaran == IdJenisPendaftaran
                                && b.IdJalurPendaftaran == IdJalurPendaftaran
                                && a.IdJenisPertanyaan == IdJenisPertanyaan
                                select new { a, b }).ToList();
                    if (data.Count() == 0 || data == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    foreach (var item in data.OrderBy(x => x.a.IdPpdbKuisionerPertanyaan))
                    {
                        ret.Add(new PpdbKuisionerModel
                        {
                            IdUnit = IdUnit,
                            IdJenisPendaftaran = IdJenisPendaftaran,
                            IdJalurPendaftaran = IdJalurPendaftaran,
                            IdJenisPertanyaan = item.a.IdJenisPertanyaan,
                            IdPpdbKuisioner = item.b.IdPpdbKuisionerPertanyaan,
                            JenisPertanyaan = JenisPertanyaanKuisionerConstant.DictJenisPertanyaanKuisioner[item.a.IdJenisPertanyaan],
                            Pertanyaan = item.a.Pertanyaan
                        });
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPertanyaanKuisioner", ex);
                return null;
            }
        }

        public List<PpdbBiayaModel> GetPpdbBiayaByUnit(int IdPpdbKelas, out string oMessage)
        {
            oMessage = string.Empty;
            List<PpdbBiayaModel> ret = new List<PpdbBiayaModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = (from a in conn.GetList<TbPpdbBiaya>()
                                join b in conn.GetList<TbJenisBiayaPpdb>() on a.IdJenisBiayaPpdb equals b.IdJenisBiayaPpdb
                                where a.IdPpdb == currPpdb.IdPpdb && a.IdPpdbKelas == IdPpdbKelas
                                select new { a, b }).ToList();
                    if (data.Count() == 0 || data == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }

                    foreach (var item in data.OrderBy(x => x.a.IdJenisBiayaPpdb))
                    {
                        ret.Add(new PpdbBiayaModel
                        {
                            IdPpdb = currPpdb.IdPpdb,
                            IdJenisBiaya = item.a.IdJenisBiayaPpdb,
                            IdPpdbBiaya = item.a.IdPpdbBiaya,
                            IdPpdbKelas = item.a.IdPpdbKelas,
                            // Unit = UnitConstant.DictUnit[item.a.IdUnit],
                            JenisBiaya = item.b.Nama,
                            Rp = item.a.Rp
                        });
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbBiayaByUnit", ex);
                return null;
            }
        }

        public string ResendEmail(int IdPpdbDaftar, int IdProses)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var dataSiswa = (from a in conn.GetList<TbPpdbDaftar>()
                                     join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                     join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                     join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                     join e in conn.GetList<TbSekolah>() on d.IdSekolah equals e.IdSekolah
                                     where a.IdPpdbDaftar == IdPpdbDaftar
                                     select new { a, b, c, d, e }).FirstOrDefault();
                    if (dataSiswa == null) return "data tidak ada";

                    string _messageBody1 = string.Empty;
                    string _messageBody2 = string.Empty;
                    string _subjects = "PPDB";

                    List<string> emailTos = new List<string>();
                    List<string> emailCcs = new List<string>();
                    List<string> _attachments = new List<string>();
                    TbPpdbDaftar tbPpdbDaftar = conn.Get<TbPpdbDaftar>(IdPpdbDaftar);
                    var dataOrtus = from a in conn.GetList<TbPpdbSiswaOrtu>()
                                    where a.IdPpdbDaftar == IdPpdbDaftar
                                    select a;
                    foreach (var ortu in dataOrtus)
                    {
                        if (!string.IsNullOrEmpty(ortu.Email))
                        {
                            if (!emailTos.Contains(ortu.Email.ToLower()))
                                emailTos.Add(ortu.Email.ToLower());
                        }
                    }
                    int jenisRedaksiEmail = IdProses;
                    _subjects = JenisRedaksiEmailPpdbConstant.Dict[jenisRedaksiEmail];

                    string oMessage = schService.GetRedaksiEmailPpdbBody(jenisRedaksiEmail, out _messageBody1, out _messageBody2);
                    if (!string.IsNullOrEmpty(oMessage))
                        return oMessage;

                    if (IdProses == JenisRedaksiEmailPpdbConstant.ObservasiDiterima)
                    {
                        _messageBody1 = _messageBody1.Replace("[PIN]", dataSiswa.a.Pin);
                        List<PpdbBiayaSiswaDetilModel> biayaPendidikans = new List<PpdbBiayaSiswaDetilModel>();
                        var dataBiayaDetils = from a in conn.GetList<TbPpdbBiayaSiswaDetil>()
                                              join b in conn.GetList<TbJenisBiayaPpdb>() on a.IdJenisBiayaPpdb equals b.IdJenisBiayaPpdb
                                              where a.IdPpdbDaftar == IdPpdbDaftar
                                              select new { a, b };
                        double total = 0;
                        foreach (var item in dataBiayaDetils)
                        {
                            total += item.a.Rp;
                            string rp = commonService.SetStrRupiah(item.a.Rp).Replace(" ", "").Replace("Rp.", "");
                            _messageBody2 = _messageBody2.Replace("[" + item.b.NamaSingkat + "]", rp);
                        }
                        _messageBody2 = _messageBody2.Replace("[RpUp]", "");
                        _messageBody2 = _messageBody2.Replace("[RpBoks]", "");
                        _messageBody2 = _messageBody2.Replace("[RpSpp]", "");
                        _messageBody2 = _messageBody2.Replace("[RpInfaq]", "");
                        _messageBody2 = _messageBody2.Replace("[RpSeragam]", "");
                        _messageBody2 = _messageBody2.Replace("[RpWakaf]", "");
                        _messageBody2 = _messageBody2.Replace("[CatatanWakaf]", "");
                        string totals = commonService.SetStrRupiah(total).Replace(" ", "").Replace("Rp.", "");
                        _messageBody2 = _messageBody2.Replace("[TOTALRP]", totals);

                        // _attachments.Add(AppSetting.PathApplFile + "/tata cara pembayaran ppdb tajur.pdf");

                    }
                    if (IdProses == JenisRedaksiEmailPpdbConstant.ObservasiDiterimaCicil)
                    {
                        _messageBody2 = _messageBody2.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                    }
                    if (IdProses == JenisRedaksiEmailPpdbConstant.ObservasiDitangguhkan)
                    {
                        _messageBody2 = _messageBody2.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                    }
                    if (IdProses == JenisRedaksiEmailPpdbConstant.ObservasiTidakDiterima)
                    {
                        _messageBody2 = _messageBody2.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                    }
                    if (IdProses == JenisRedaksiEmailPpdbConstant.ObservasiUndurDiri)
                    {
                        _messageBody2 = _messageBody2.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                    }




                    if (IdProses == JenisRedaksiEmailPpdbConstant.LunasDaftar)
                    {
                        _messageBody1 = _messageBody1.Replace("[NOVA]", dataSiswa.a.IdPpdbDaftar.ToString());
                        _messageBody1 = _messageBody1.Replace("[PIN]", dataSiswa.a.Pin);
                    }
                    if (IdProses == JenisRedaksiEmailPpdbConstant.JadwalObservasi)
                    {
                        var jadwalObservasis = (from a in conn.GetList<TbPpdbJadwalObservasi>()
                                                join b in conn.GetList<TbPpdbSiswaObservasi>() on a.IdPpdbJadwalObservasi equals b.IdPpdbJadwalObservasi
                                                join c in conn.GetList<TbUser>() on a.IdPegawai equals c.IdUser
                                                join d in conn.GetList<TbUser>() on a.IdPegawai equals d.IdUser
                                                where b.IdPpdbDaftar == IdPpdbDaftar
                                                select new { a, b, c, d }).FirstOrDefault();
                        string tanggalObservasi = jadwalObservasis.a.Tanggal.ToString("dd-MM-yyyy");
                        string jamAwalObservasi = jadwalObservasis.a.JamAwal.ToString();
                        string petugasObservasi = userAppService.SetFullName(jadwalObservasis.d.FirstName, jadwalObservasis.d.MiddleName, jadwalObservasis.d.LastName);
                        string emailPetugasObservasi = jadwalObservasis.c.Email;
                        string noHpPetugasObservasi = jadwalObservasis.d.MobileNumber;

                        var jadwalWawancaras = (from a in conn.GetList<TbPpdbJadwalWawancara>()
                                                join b in conn.GetList<TbPpdbSiswaWawancara>() on a.IdPpdbJadwalWawancara equals b.IdPpdbJadwalWawancara
                                                join c in conn.GetList<TbUser>() on a.IdPegawai equals c.IdUser
                                                join d in conn.GetList<TbUser>() on a.IdPegawai equals d.IdUser
                                                where b.IdPpdbDaftar == IdPpdbDaftar
                                                select new { a, b, c, d }).FirstOrDefault();
                        string tanggalWawancara = jadwalWawancaras.a.Tanggal.ToString("dd-MM-yyyy");
                        string jamAwalWawancara = jadwalWawancaras.a.JamAwal.ToString();
                        string petugasWawancara = userAppService.SetFullName(jadwalWawancaras.d.FirstName, jadwalWawancaras.d.MiddleName, jadwalWawancaras.d.LastName);
                        string emailPetugasWawancara = jadwalWawancaras.c.Email;
                        string noHpPetugasWawancara = jadwalWawancaras.d.MobileNumber;


                        _messageBody1 = _messageBody1.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                        _messageBody2 = _messageBody2.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);

                        // _messageBody2 = _messageBody2.Replace("[WCRTANGGAL]", tanggalWawancara);
                        // _messageBody2 = _messageBody2.Replace("[WCRWAKTU]", String.Format("{0}", jamAwalWawancara));
                        _messageBody2 = _messageBody2.Replace("[WCRTANGGAL]", " "); //hapus jangan lupa
                        _messageBody2 = _messageBody2.Replace("[WCRWAKTU]", " "); //hapus jangan lupa
                        _messageBody2 = _messageBody2.Replace("[WCRPIC]", petugasWawancara);
                        _messageBody2 = _messageBody2.Replace("[WCREMAIL]", emailPetugasWawancara);
                        _messageBody2 = _messageBody2.Replace("[WCRNOHP]", noHpPetugasWawancara);

                        // _messageBody2 = _messageBody2.Replace("[OBSTANGGAL]", tanggalObservasi);
                        // _messageBody2 = _messageBody2.Replace("[OBSWAKTU]", String.Format("{0}", jamAwalObservasi));
                        _messageBody2 = _messageBody2.Replace("[OBSTANGGAL]", " "); //hapus jangan lupa
                        _messageBody2 = _messageBody2.Replace("[OBSWAKTU]", " "); //hapus jangan lupa
                        _messageBody2 = _messageBody2.Replace("[OBSPIC]", petugasObservasi);
                        _messageBody2 = _messageBody2.Replace("[OBSEMAIL]", emailPetugasObservasi);
                        _messageBody2 = _messageBody2.Replace("[OBSNOHP]", noHpPetugasObservasi);
                    }
                    if (IdProses == JenisRedaksiEmailPpdbConstant.LunasPendidikan)
                    {
                        _messageBody1 = _messageBody1.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                        _messageBody2 = _messageBody2.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                    }
                    if (IdProses == JenisRedaksiEmailPpdbConstant.PengesahanDiterima)
                    {
                        _messageBody1 = _messageBody1.Replace("[TAHUNPELAJARAN]", currPpdb.TahunAjaran);
                    }
                    if (!emailTos.Contains(tbPpdbDaftar.Email))
                        emailTos.Add(tbPpdbDaftar.Email);
                    emailCcs.Add("ppdb.sekolahattaufiqbogor@gmail.com"); //cc ke bagian keuangan
                    string _emailMessage = schService.SetHtmlEmailPpdbMessage(
                        _messageBody1,
                        _messageBody2,
                        dataSiswa.a.IdPpdbDaftar,
                        dataSiswa.c.Nama,
                        JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[dataSiswa.a.IdJalurPendaftaran],
                        JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[dataSiswa.a.IdJenisPendaftaran],
                        dataSiswa.a.Nama,
                        dataSiswa.a.TanggalLahir.ToString("dd-MM-yyyy"),
                        currPpdb.TahunAjaran,
                        dataSiswa.d.Nama,
                        currPpdb.NamaKetua);

                    SendEmailModel email = new SendEmailModel
                    {
                        To = emailTos,
                        Cc = emailCcs,
                        Title = "Bapak/Ibu ",
                        Subject = _subjects,
                        Message = _emailMessage,
                        Attachments = _attachments
                    };
                    var abc = emailService.SendHtmlEmailAsync(email);
                    return null;
                }
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "ResendEmail", ex);
            }
        }

        public string EditPpdbDaftar(int IdUser, int IdPpdbDaftar, PpdbDaftarAddModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var user = (from a in conn.GetList<TbUnitPegawai>()
                                    join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                    join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                    where a.IdPegawai == IdUser
                                    select new { a, b, c }).FirstOrDefault();
                        if (user == null)
                            return "pengguna tidak terdaftar sebagai pegawai";

                        var tbPpdbDaftar = conn.Get<TbPpdbDaftar>(IdPpdbDaftar);
                        if (tbPpdbDaftar == null)
                            return "data tidak ada";
                        tbPpdbDaftar.Nama = Data.Nama;
                        tbPpdbDaftar.KdJenisKelamin = Data.KdJenisKelamin;
                        tbPpdbDaftar.TempatLahir = Data.TempatLahir;
                        tbPpdbDaftar.TanggalLahir = dateTimeService.StrToDateTime(Data.TanggalLahir);
                        tbPpdbDaftar.Email = Data.Email;
                        tbPpdbDaftar.NoHandphone = Data.NoHandphone;
                        tbPpdbDaftar.NikOrtu = Data.NikOrtu;
                        conn.Update(tbPpdbDaftar);

                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditPpdbDaftar", ex);
            }
        }

        public string EditPpdbSiswa(
            int IdUser,
            PpdbSiswaModel Siswa,
            List<PpdbSiswaOrtuAddModel> Ortus,
            List<PpdbSiswaRaporAddModel> Rapors,
            List<PpdbSiswaPrestasiAddModel> Prestasis,
            IFormFile FileFoto,
            IFormFile FileKtpIbu,
            IFormFile FileKtpAyah,
            IFormFile FileKk,
            IFormFile FileAkte,
            IFormFile FileSuratKesehatan,
            IFormFile FileBebasNarkoba,
            List<IFormFile> FileRapors,
            List<IFormFile> FilePrestasis
            )
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {

                        var user = (from a in conn.GetList<TbUnitPegawai>()
                                    join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                    join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                    where a.IdPegawai == IdUser
                                    select new { a, b, c }).FirstOrDefault();
                        if (user == null)
                            return "pengguna tidak terdaftar sebagai pegawai";

                        // daftar
                        var tbPpdbDaftar = conn.Get<TbPpdbDaftar>(Siswa.IdPpdbDaftar);
                        if (tbPpdbDaftar == null)
                            return "data tidak ada";
                        tbPpdbDaftar.IdJenisKategoriPendaftaran = Siswa.IdJenisKategoriPendaftaran;
                        tbPpdbDaftar.IdJalurPendaftaran = Siswa.IdJalurPendaftaran;
                        tbPpdbDaftar.IdJenisPendaftaran = Siswa.IdJenisPendaftaran;
                        tbPpdbDaftar.IdPpdbKelas = Siswa.IdPpdbKelas;
                        tbPpdbDaftar.Nama = Siswa.Nama;
                        tbPpdbDaftar.KdJenisKelamin = Siswa.KdJenisKelamin;
                        tbPpdbDaftar.TempatLahir = Siswa.TempatLahir;
                        tbPpdbDaftar.TanggalLahir = dateTimeService.StrToDateTime(Siswa.TanggalLahir);
                        tbPpdbDaftar.Email = Siswa.Email;
                        tbPpdbDaftar.NoHandphone = Siswa.NoHandphone;
                        conn.Update(tbPpdbDaftar);

                        // siswa
                        var tbPpdbSiswa = conn.Get<TbPpdbSiswa>(Siswa.IdPpdbDaftar);
                        if (tbPpdbSiswa == null)
                            return "data tidak ada";

                        tbPpdbSiswa.IdJenisKategoriPendaftaran = Siswa.IdJenisKategoriPendaftaran;
                        tbPpdbSiswa.IdJalurPendaftaran = Siswa.IdJalurPendaftaran;
                        tbPpdbSiswa.IdJenisPendaftaran = Siswa.IdJenisPendaftaran;
                        tbPpdbSiswa.IdPpdbKelas = Siswa.IdPpdbKelas;
                        tbPpdbSiswa.IdPpdbSeragam = Siswa.IdPpdbSeragam;
                        tbPpdbSiswa.Nik = Siswa.Nik;
                        tbPpdbSiswa.Nisn = Siswa.Nisn;
                        tbPpdbSiswa.Nama = Siswa.Nama;
                        tbPpdbSiswa.TanggalLahir = dateTimeService.StrToDateTime(Siswa.TanggalLahir);
                        tbPpdbSiswa.TempatLahir = Siswa.TempatLahir;
                        tbPpdbSiswa.IdAgama = Siswa.IdAgama;
                        tbPpdbSiswa.IdKewarganegaraan = Siswa.IdKewarganegaraan;
                        tbPpdbSiswa.KdJenisKelamin = Siswa.KdJenisKelamin;
                        tbPpdbSiswa.KdGolonganDarah = Siswa.KdGolonganDarah;
                        tbPpdbSiswa.AlamatTinggal = Siswa.AlamatTinggal;
                        tbPpdbSiswa.IdTinggal = Siswa.IdTinggal;
                        tbPpdbSiswa.Email = Siswa.Email;
                        tbPpdbSiswa.NoHandphone = Siswa.NoHandphone;
                        tbPpdbSiswa.NoDarurat = Siswa.NoDarurat;
                        tbPpdbSiswa.TinggiBadan = Siswa.TinggiBadan;
                        tbPpdbSiswa.BeratBadan = Siswa.BeratBadan;
                        tbPpdbSiswa.AnakKe = Siswa.AnakKe;
                        tbPpdbSiswa.JumlahSodaraKandung = Siswa.JumlahSodaraKandung;
                        tbPpdbSiswa.JumlahSodaraTiri = Siswa.JumlahSodaraTiri;
                        tbPpdbSiswa.JumlahSodaraAngkat = Siswa.JumlahSodaraAngkat;
                        tbPpdbSiswa.IdTransportasi = Siswa.IdTransportasi;
                        tbPpdbSiswa.IdBahasa = Siswa.IdBahasa;
                        tbPpdbSiswa.JarakKeSekolah = Siswa.JarakKeSekolah;
                        tbPpdbSiswa.WaktuTempuh = Siswa.WaktuTempuh;
                        tbPpdbSiswa.PenyakitDiderita = Siswa.PenyakitDiderita;
                        tbPpdbSiswa.KelainanJasmani = Siswa.KelainanJasmani;
                        tbPpdbSiswa.SekolahAsal = Siswa.SekolahAsal;
                        tbPpdbSiswa.AlamatSekolahAsal = Siswa.AlamatSekolahAsal;
                        tbPpdbSiswa.NspnSekolahAsal = Siswa.NspnSekolahAsal;


                        //ortu
                        foreach (var ortu in Ortus)
                        {
                            var tbPpdbSiswaOrtu = (from a in conn.GetList<TbPpdbSiswaOrtu>()
                                                   where a.IdPpdbDaftar == Siswa.IdPpdbDaftar
                                                   && a.IdTipe == ortu.IdTipe
                                                   select a).FirstOrDefault();
                            TbPpdbSiswaOrtu tbPpdbDaftarOrtu = new TbPpdbSiswaOrtu
                            {
                                IdPpdbDaftar = tbPpdbSiswa.IdPpdbDaftar,
                                IdTipe = ortu.IdTipe,
                                Nama = ortu.Nama,
                                Alamat = ortu.Alamat,
                                Nik = ortu.Nik,
                                IdJenisPekerjaan = ortu.IdJenisPekerjaan,
                                NamaInstansi = ortu.NamaInstansi,
                                Jabatan = ortu.Jabatan,
                                Email = ortu.Email,
                                NoHandphone = ortu.NoHandphone,
                                NoTelpRumah = ortu.NoTelpRumah,
                                IdAgama = ortu.IdAgama,
                                IdKewarganegaraan = ortu.IdKewarganegaraan,
                                IdJenisJenjangPendidikan = ortu.IdJenjangPendidikan,
                                IdStatusPenikahan = ortu.IdStatusPenikahan,
                                TempatLahir = ortu.TempatLahir,
                                TanggalLahir = dateTimeService.StrToDateTime(ortu.TanggalLahir),
                                IdStatusHidup = ortu.IdStatusHidup
                            };
                            if (ortu.IdStatusHidup != 1)
                                tbPpdbDaftarOrtu.TanggalMeninggal = dateTimeService.StrToDateTime(ortu.TanggalMeninggal);

                            conn.Update(tbPpdbDaftarOrtu);

                            var dtPegawai = (from a in conn.GetList<TbPegawai>()
                                             where a.Nik == ortu.Nik
                                             select a).FirstOrDefault();
                            if (dtPegawai != null)
                                tbPpdbSiswa.IdPegawai = dtPegawai.IdPegawai;
                        }

                        conn.Update(tbPpdbSiswa);


                        string _fileName = string.Empty;
                        string _pathFileUpload = AppSetting.PathPpdbBerkas;
                        if (FileFoto != null)
                        {
                            var Foto = Siswa.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.PasFoto];
                            string[] getsFoto = Directory.GetFiles(_pathFileUpload, string.Format("{0}.*", Foto));
                            Foto = getsFoto.Count() != 0 ? new DirectoryInfo(getsFoto[0]).Name : null;
                            if (Foto != null)
                                File.Delete(Path.Combine(_pathFileUpload, Foto));

                            _fileName = string.Format("{0}{1}",
                                    tbPpdbSiswa.IdPpdbDaftar,
                                    NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.PasFoto]);
                            _fileName += Path.GetExtension(FileFoto.FileName);
                            _fileName = Path.Combine(_pathFileUpload, _fileName);
                            using (var fileStream = new FileStream(_fileName, FileMode.Create))
                            {
                                FileFoto.CopyTo(fileStream);
                            }
                        }
                        if (FileKtpIbu != null)
                        {
                            var Foto = Siswa.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KtpIbu];
                            string[] getsFoto = Directory.GetFiles(_pathFileUpload, string.Format("{0}.*", Foto));
                            Foto = getsFoto.Count() != 0 ? new DirectoryInfo(getsFoto[0]).Name : null;
                            if (Foto != null)
                                File.Delete(Path.Combine(_pathFileUpload, Foto));

                            _fileName = string.Format("{0}{1}",
                                    tbPpdbSiswa.IdPpdbDaftar,
                                    NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KtpIbu]);
                            _fileName += Path.GetExtension(FileKtpIbu.FileName);
                            _fileName = Path.Combine(_pathFileUpload, _fileName);
                            using (var fileStream = new FileStream(_fileName, FileMode.Create))
                            {
                                FileKtpIbu.CopyTo(fileStream);
                            }
                        }
                        if (FileKtpAyah != null)
                        {
                            var Foto = Siswa.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KtpAyah];
                            string[] getsFoto = Directory.GetFiles(_pathFileUpload, string.Format("{0}.*", Foto));
                            Foto = getsFoto.Count() != 0 ? new DirectoryInfo(getsFoto[0]).Name : null;
                            if (Foto != null)
                                File.Delete(Path.Combine(_pathFileUpload, Foto));

                            _fileName = string.Format("{0}{1}",
                                    tbPpdbSiswa.IdPpdbDaftar,
                                    NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KtpAyah]);
                            _fileName += Path.GetExtension(FileKtpAyah.FileName);
                            _fileName = Path.Combine(_pathFileUpload, _fileName);
                            using (var fileStream = new FileStream(_fileName, FileMode.Create))
                            {
                                FileKtpAyah.CopyTo(fileStream);
                            }
                        }
                        if (FileKk != null)
                        {
                            var Foto = Siswa.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuKeluarga];
                            string[] getsFoto = Directory.GetFiles(_pathFileUpload, string.Format("{0}.*", Foto));
                            Foto = getsFoto.Count() != 0 ? new DirectoryInfo(getsFoto[0]).Name : null;
                            if (Foto != null)
                                File.Delete(Path.Combine(_pathFileUpload, Foto));

                            _fileName = string.Format("{0}{1}",
                                    tbPpdbSiswa.IdPpdbDaftar,
                                    NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuKeluarga]);
                            _fileName += Path.GetExtension(FileKk.FileName);
                            _fileName = Path.Combine(_pathFileUpload, _fileName);
                            using (var fileStream = new FileStream(_fileName, FileMode.Create))
                            {
                                FileKk.CopyTo(fileStream);
                            }
                        }
                        if (FileAkte != null)
                        {
                            var Foto = Siswa.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.Akte];
                            string[] getsFoto = Directory.GetFiles(_pathFileUpload, string.Format("{0}.*", Foto));
                            Foto = getsFoto.Count() != 0 ? new DirectoryInfo(getsFoto[0]).Name : null;
                            if (Foto != null)
                                File.Delete(Path.Combine(_pathFileUpload, Foto));

                            _fileName = string.Format("{0}{1}",
                                    tbPpdbSiswa.IdPpdbDaftar,
                                    NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.Akte]);
                            _fileName += Path.GetExtension(FileAkte.FileName);
                            _fileName = Path.Combine(_pathFileUpload, _fileName);
                            using (var fileStream = new FileStream(_fileName, FileMode.Create))
                            {
                                FileAkte.CopyTo(fileStream);
                            }
                        }
                        if (FileSuratKesehatan != null)
                        {
                            var Foto = Siswa.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.SuratKesehatan];
                            string[] getsFoto = Directory.GetFiles(_pathFileUpload, string.Format("{0}.*", Foto));
                            Foto = getsFoto.Count() != 0 ? new DirectoryInfo(getsFoto[0]).Name : null;
                            if (Foto != null)
                                File.Delete(Path.Combine(_pathFileUpload, Foto));

                            _fileName = string.Format("{0}{1}",
                                    tbPpdbSiswa.IdPpdbDaftar,
                                    NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.SuratKesehatan]);
                            _fileName += Path.GetExtension(FileSuratKesehatan.FileName);
                            _fileName = Path.Combine(_pathFileUpload, _fileName);
                            using (var fileStream = new FileStream(_fileName, FileMode.Create))
                            {
                                FileSuratKesehatan.CopyTo(fileStream);
                            }
                        }
                        if (FileBebasNarkoba != null)
                        {
                            var Foto = Siswa.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KetBebasNarkoba];
                            string[] getsFoto = Directory.GetFiles(_pathFileUpload, string.Format("{0}.*", Foto));
                            Foto = getsFoto.Count() != 0 ? new DirectoryInfo(getsFoto[0]).Name : null;
                            if (Foto != null)
                                File.Delete(Path.Combine(_pathFileUpload, Foto));

                            _fileName = string.Format("{0}{1}",
                                    tbPpdbSiswa.IdPpdbDaftar,
                                    NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KetBebasNarkoba]);
                            _fileName += Path.GetExtension(FileBebasNarkoba.FileName);
                            _fileName = Path.Combine(_pathFileUpload, _fileName);
                            using (var fileStream = new FileStream(_fileName, FileMode.Create))
                            {
                                FileBebasNarkoba.CopyTo(fileStream);
                            }
                        }

                        tx.Commit();
                        return string.Empty;
                    }

                }
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditPpdbSiswa", ex);
            }
        }
        public List<SheetPpdbDaftarSiswaModel> GetPpdbSiswas(int IdUser, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<SheetPpdbDaftarSiswaModel> ret = new List<SheetPpdbDaftarSiswaModel>();
                using (var conn = commonService.DbConnection())
                {
                    var user = (from a in conn.GetList<TbUnitPegawai>()
                                join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                join d in conn.GetList<TbUser>() on b.IdPegawai equals d.IdUser
                                where a.IdPegawai == IdUser
                                select new { a, b, c, d }).FirstOrDefault();
                    if (user == null) { oMessage = "pengguna tidak terdaftar sebagai pegawai"; return null; }

                    bool isAdmin = false;
                    int[] _listUnit = new int[] { UnitConstant.DIR, UnitConstant.AKT, UnitConstant.FNC };
                    isAdmin = _listUnit.Contains(user.a.IdUnit);


                    string judul = string.Format("Daftar Calon Siswa PPDB {0} Tahun {1}", isAdmin ? "Semua Unit" : "Unit " + user.c.Nama, currPpdb.TahunAjaran);
                    string downloadOleh = string.Format("Didownload Tanggal: {0}, Jam: {1} WIB, Oleh: {2}", dateTimeService.DateToLongString(_languageCode, dateTimeService.GetCurrdate()), dateTimeService.GetCurrdate().ToString("HH:mm:ss"), userAppService.SetFullName(user.d.FirstName, user.d.MiddleName, user.d.LastName));

                    var data = from a in conn.GetList<TbPpdbSiswa>()
                               join a1 in conn.GetList<TbPpdbSiswaStatus>(string.Format("where status = {0}", StatusPpdbConstant.Registrasi)) on a.IdPpdbDaftar equals a1.IdPpdbDaftar
                               join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                               join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                               join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                               join e in conn.GetList<TbSekolah>() on d.IdSekolah equals e.IdSekolah
                               join f in conn.GetList<TbPpdbSiswaStatus>(string.Format("where status = {0}", StatusPpdbConstant.Wawancara)) on a.IdPpdbDaftar equals f.IdPpdbDaftar into f1
                               from f in f1.DefaultIfEmpty()
                               join g in conn.GetList<TbPpdbSiswaStatus>(string.Format("where status = {0}", StatusPpdbConstant.TestObservasi)) on a.IdPpdbDaftar equals g.IdPpdbDaftar into g1
                               from g in g1.DefaultIfEmpty()
                               join h in conn.GetList<TbPpdbSiswaStatus>(string.Format("where status = {0}", StatusPpdbConstant.InputObservasi)) on a.IdPpdbDaftar equals h.IdPpdbDaftar into h1
                               from h in g1.DefaultIfEmpty()
                               join i in conn.GetList<TbPpdbSiswaStatus>(string.Format("where status = {0}", StatusPpdbConstant.VerifikasiObservasi)) on a.IdPpdbDaftar equals i.IdPpdbDaftar into i1
                               from i in g1.DefaultIfEmpty()
                               join j in conn.GetList<TbPpdbSiswaStatus>(string.Format("where status = {0}", StatusPpdbConstant.PelunasanBiayaPendidikan)) on a.IdPpdbDaftar equals j.IdPpdbDaftar into j1
                               from j in g1.DefaultIfEmpty()
                               join k in conn.GetList<TbPpdbSeragam>() on a.IdPpdbSeragam equals k.IdPpdbSeragam
                               where c.IdUnit == (isAdmin ? c.IdUnit : user.a.IdUnit)
                               select new { a, a1, b, c, d, e, f, g, h, i, j, k };
                    foreach (var item in data.OrderBy(x => x.c.IdUnit).ThenBy(x => x.b.IdKelas))
                    {
                        SheetPpdbDaftarSiswaModel m = new SheetPpdbDaftarSiswaModel
                        {
                            Judul = judul,
                            DownloadOleh = downloadOleh,
                            Jawabans = new List<string>(),
                            Komitmens = new List<string>()
                        };
                        m.IdPpdbDaftar = item.a.IdPpdbDaftar.ToString();
                        m.JenisKategoriPendaftaran = KategoriPendaftaranPpdbConstant.DictKategoriPendaftaranPpdb[item.a.IdJenisKategoriPendaftaran];
                        m.JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[item.a.IdJalurPendaftaran];
                        m.JenisPendaftaran = JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[item.a.IdJenisPendaftaran];
                        m.Kelas = string.Format("{0} - {1}", item.d.Nama, item.c.Nama);
                        m.Nik = item.a.Nik;
                        m.Nisn = item.a.Nisn;
                        m.Nama = item.a.Nama;
                        m.NamaPanggilan = item.a.NamaPanggilan;
                        m.TempatLahir = item.a.TempatLahir;
                        m.TanggalLahir = item.a.TanggalLahir.ToString("dd-MM-yyyy");
                        m.IdAgama = AgamaConstant.Dict[item.a.IdAgama];
                        m.IdKewarganegaraan = JenisWargaNegaraConstant.Dict[item.a.IdKewarganegaraan];
                        m.KdJenisKelamin = item.a.KdJenisKelamin;
                        m.KdGolonganDarah = item.a.KdGolonganDarah;
                        m.AlamatTinggal = item.a.AlamatTinggal;
                        m.IdTinggal = JenisTinggalSiswaConstant.DictJenisTinggalSiswa[item.a.IdTinggal];
                        m.Email = item.a.Email;
                        m.NoHandphone = item.a.NoHandphone;
                        m.NoDarurat = item.a.NoDarurat;
                        m.Bahasa = JenisBahasaConstant.DictJenisBahasa[(int)item.a.IdBahasa];
                        m.TinggiBadan = item.a.TinggiBadan == null ? 0 : (int)item.a.TinggiBadan;
                        m.BeratBadan = item.a.BeratBadan == null ? 0 : (int)item.a.BeratBadan;
                        m.AnakKe = item.a.AnakKe == null ? 0 : (int)item.a.AnakKe;
                        m.JumlahSodaraKandung = item.a.JumlahSodaraKandung == null ? 0 : (int)item.a.JumlahSodaraKandung;
                        m.JumlahSodaraTiri = item.a.JumlahSodaraTiri == null ? 0 : (int)item.a.JumlahSodaraTiri;
                        m.JumlahSodaraAngkat = item.a.JumlahSodaraAngkat == null ? 0 : (int)item.a.JumlahSodaraAngkat;
                        m.Transportasi = JenisTransportasiConstant.DictJenisTransportasi[item.a.IdTransportasi];
                        m.JarakKeSekolah = item.a.JarakKeSekolah == null ? 0 : (int)item.a.JarakKeSekolah;
                        m.WaktuTempuh = item.a.WaktuTempuh == null ? 0 : (int)item.a.WaktuTempuh;
                        m.PenyakitDiderita = item.a.PenyakitDiderita;
                        m.KelainanJasmani = item.a.KelainanJasmani;
                        m.SekolahAsal = item.a.SekolahAsal;
                        m.AlamatSekolahAsal = item.a.AlamatSekolahAsal;
                        m.NspnSekolahAsal = item.a.NspnSekolahAsal;
                        m.UkuranSeragam = item.k.Nama + " - " + item.k.NamaSingkat;
                        var ortus = from a in conn.GetList<TbPpdbSiswaOrtu>()
                                    join b in conn.GetList<TbJenisJenjangPendidikan>() on a.IdJenisJenjangPendidikan equals b.IdJenisJenjangPendidikan
                                    join c in conn.GetList<TbJenisPekerjaan>() on a.IdJenisPekerjaan equals c.IdJenisPekerjaan
                                    where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                    select new { a, b, c };
                        foreach (var ortu in ortus)
                        {
                            if (ortu.a.IdTipe == 2)//ayah
                            {
                                m.AyahNama = ortu.a.Nama;
                                m.AyahAlamat = ortu.a.Alamat;
                                m.AyahNik = ortu.a.Nik;
                                m.AyahJenisPekerjaan = ortu.c.Nama;
                                m.AyahNamaInstansi = ortu.a.NamaInstansi;
                                m.AyahJabatan = ortu.a.Jabatan;
                                m.AyahEmail = ortu.a.Email;
                                m.AyahNoHandphone = ortu.a.NoHandphone;
                                m.AyahNoTelpRumah = ortu.a.NoTelpRumah;
                                m.AyahAgama = AgamaConstant.Dict[ortu.a.IdAgama];
                                m.AyahKewarganegaraan = JenisWargaNegaraConstant.Dict[ortu.a.IdKewarganegaraan];
                                m.AyahJenjangPendidikan = ortu.b.Nama;
                                switch (ortu.a.IdStatusPenikahan)
                                {
                                    case 1:
                                        m.AyahIdStatusPenikahan = "Kawin";
                                        break;
                                    case 2:
                                        m.AyahIdStatusPenikahan = "Cerai";
                                        break;
                                    case 3:
                                        m.AyahIdStatusPenikahan = "Cerai Mati";
                                        break;
                                    default:
                                        break;
                                }
                                m.AyahTempatLahir = ortu.a.TempatLahir;
                                m.AyahTanggalLahir = ortu.a.TanggalLahir.ToString("dd-MM-yyyy");
                                if (ortu.a.IdStatusHidup != 1)
                                {
                                    m.AyahIdStatusHidup = "Meninggal";
                                    m.AyahTanggalMeninggal = ((DateTime)ortu.a.TanggalMeninggal).ToString("dd-MM-yyyy");
                                }
                            }
                            else if (ortu.a.IdTipe == 1)//ibu
                            {
                                m.IbuNama = ortu.a.Nama;
                                m.IbuAlamat = ortu.a.Alamat;
                                m.IbuNik = ortu.a.Nik;
                                m.IbuJenisPekerjaan = ortu.c.Nama;
                                m.IbuNamaInstansi = ortu.a.NamaInstansi;
                                m.IbuJabatan = ortu.a.Jabatan;
                                m.IbuEmail = ortu.a.Email;
                                m.IbuNoHandphone = ortu.a.NoHandphone;
                                m.IbuNoTelpRumah = ortu.a.NoTelpRumah;
                                m.IbuAgama = AgamaConstant.Dict[ortu.a.IdAgama];
                                m.IbuKewarganegaraan = JenisWargaNegaraConstant.Dict[ortu.a.IdKewarganegaraan];
                                m.IbuJenjangPendidikan = ortu.b.Nama;
                                switch (ortu.a.IdStatusPenikahan)
                                {
                                    case 1:
                                        m.IbuIdStatusPenikahan = "Kawin";
                                        break;
                                    case 2:
                                        m.IbuIdStatusPenikahan = "Cerai";
                                        break;
                                    case 3:
                                        m.IbuIdStatusPenikahan = "Cerai Mati";
                                        break;
                                    default:
                                        break;
                                }
                                m.IbuTempatLahir = ortu.a.TempatLahir;
                                m.IbuTanggalLahir = ortu.a.TanggalLahir.ToString("dd-MM-yyyy");
                                if (ortu.a.IdStatusHidup != 1)
                                {
                                    m.IbuIdStatusHidup = "Meninggal";
                                    m.IbuTanggalMeninggal = ((DateTime)ortu.a.TanggalMeninggal).ToString("dd-MM-yyyy");
                                }
                            }
                            else //wali
                            {
                                m.WaliNama = ortu.a.Nama;
                                m.WaliAlamat = ortu.a.Alamat;
                                m.WaliNik = ortu.a.Nik;
                                m.WaliJenisPekerjaan = ortu.c.Nama;
                                m.WaliNamaInstansi = ortu.a.NamaInstansi;
                                m.WaliJabatan = ortu.a.Jabatan;
                                m.WaliEmail = ortu.a.Email;
                                m.WaliNoHandphone = ortu.a.NoHandphone;
                                m.WaliNoTelpRumah = ortu.a.NoTelpRumah;
                                m.WaliAgama = AgamaConstant.Dict[ortu.a.IdAgama];
                                m.WaliKewarganegaraan = JenisWargaNegaraConstant.Dict[ortu.a.IdKewarganegaraan];
                                m.WaliJenjangPendidikan = ortu.b.Nama;
                                switch (ortu.a.IdStatusPenikahan)
                                {
                                    case 1:
                                        m.WaliIdStatusPenikahan = "Kawin";
                                        break;
                                    case 2:
                                        m.WaliIdStatusPenikahan = "Cerai";
                                        break;
                                    case 3:
                                        m.WaliIdStatusPenikahan = "Cerai Mati";
                                        break;
                                    default:
                                        break;
                                }
                                m.WaliTempatLahir = ortu.a.TempatLahir;
                                m.WaliTanggalLahir = ortu.a.TanggalLahir.ToString("dd-MM-yyyy");
                                if (ortu.a.IdStatusHidup != 1)
                                {
                                    m.WaliIdStatusHidup = "Meninggal";
                                    m.WaliTanggalMeninggal = ((DateTime)ortu.a.TanggalMeninggal).ToString("dd-MM-yyyy");
                                }
                            }
                        }
                        var rapors = from a in conn.GetList<TbPpdbSiswaRapor>()
                                     where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                     select a;
                        foreach (var rapor in rapors.OrderBy(x => x.Semester))
                        {
                            switch (rapor.Mapel)
                            {
                                case "IPS":
                                    if (!string.IsNullOrEmpty(m.RaporIps))
                                        m.RaporIps += ", ";
                                    m.RaporIps += string.Format("Sem {0}: {1}", rapor.Semester, rapor.Nilai);
                                    break;
                                case "IPA":
                                    if (!string.IsNullOrEmpty(m.RaporIpa))
                                        m.RaporIpa += ", ";
                                    m.RaporIpa += string.Format("Sem {0}: {1}", rapor.Semester, rapor.Nilai);
                                    break;
                                case "B. Indonesia":
                                    if (!string.IsNullOrEmpty(m.RaporIndo))
                                        m.RaporIndo += ", ";
                                    m.RaporIndo += string.Format("Sem {0}: {1}", rapor.Semester, rapor.Nilai);
                                    break;
                                case "Matematika":
                                    if (!string.IsNullOrEmpty(m.RaporMatematika))
                                        m.RaporMatematika += ", ";
                                    m.RaporMatematika += string.Format("Sem {0}: {1}", rapor.Semester, rapor.Nilai);
                                    break;
                                case "B. Inggris":
                                    if (!string.IsNullOrEmpty(m.RaporEng))
                                        m.RaporEng += ", ";
                                    m.RaporEng += string.Format("Sem {0}: {1}", rapor.Semester, rapor.Nilai);
                                    break;
                                default:
                                    break;
                            }
                        }
                        var prestasis = from a in conn.GetList<TbPpdbSiswaPrestasi>()
                                        where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                        select a;
                        foreach (var prestasi in prestasis)
                        {
                            //hanya diambil salah satu
                            m.JenisPrestasi = JenisPrestasiConstant.DictJenisPrestasi[prestasi.IdJenisPrestasi];
                            m.JenisTingkatPrestasi = TingkatPrestasiSiswaConstant.DictTingkatPrestasiSiswa[prestasi.IdJenisTingkatPrestasi];
                            m.Tahun = prestasi.Tahun.ToString();
                            m.NamaPenyelenggara = prestasi.NamaPenyelenggara;
                            m.Keterangan = prestasi.Keterangan;
                        }

                        var jawabanKuisioners = from a in conn.GetList<TbPpdbKuisionerPertanyaan>()
                                                join b in conn.GetList<TbPpdbKuisioner>() on a.IdPpdbKuisionerPertanyaan equals b.IdPpdbKuisionerPertanyaan
                                                join c in conn.GetList<TbPpdbKuisionerJawaban>(string.Format("where id_ppdb_daftar = {0}", item.a.IdPpdbDaftar)) on b.IdPpdbKuisionerPertanyaan equals c.IdPpdbKuisionerPertanyaan into c1
                                                from c in c1.DefaultIfEmpty()
                                                where b.IdUnit == item.d.IdUnit
                                                & b.IdJenisPendaftaran == item.a.IdJenisPendaftaran
                                                & b.IdJalurPendaftaran == item.a.IdJalurPendaftaran
                                                select new { a, b, c };

                        foreach (var jawab in jawabanKuisioners.OrderBy(x => x.a.IdPpdbKuisionerPertanyaan))
                        {
                            string jawaban = jawab.c.OpsiJawaban;
                            jawaban += jawab.c.Jawaban;
                            m.Jawabans.Add(jawaban);
                        }

                        //m.Komitmens = item.a.Komitmens; belum tahu darimana
                        var biayas = from a in conn.GetList<TbPpdbBiayaSiswa>()
                                     join b in conn.GetList<TbPpdbBiayaSiswaDetil>() on a.IdPpdbDaftar equals b.IdPpdbDaftar
                                     join c in conn.GetList<TbJenisBiayaPpdb>() on b.IdJenisBiayaPpdb equals c.IdJenisBiayaPpdb
                                     where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                     select new { a, b, c };
                        foreach (var biaya in biayas)
                        {
                            if (biaya.c.NamaSingkat == "RpUp")
                                m.UangPangkal = biaya.b.Rp;
                            if (biaya.c.NamaSingkat == "RpBoks")
                                m.Boks = biaya.b.Rp;
                            if (biaya.c.NamaSingkat == "RpSpp")
                                m.Spp = biaya.b.Rp;
                            if (biaya.c.NamaSingkat == "RpInfaq")
                                m.Infaq = biaya.b.Rp;
                            if (biaya.c.NamaSingkat == "RpSeragam")
                                m.Seragam = biaya.b.Rp;
                            if (biaya.c.NamaSingkat == "RpWakaf")
                                m.Wakaf = biaya.b.Rp;
                            if (biaya.a.IsCicil == 1)
                                m.IsCicil = "YA";
                        }

                        m.LastStatus = StatusPpdbConstant.Dict[StatusPpdbConstant.Registrasi];
                        m.LastStatusProses = "";
                        m.LastCatatan = "";

                        m.TanggalDaftar = "";
                        m.TanggalBayarDaftar = "";
                        m.TanggalInputFormulir = item.a1.CreatedDate.ToString("dd-MM-yyyy");
                        if (item.f != null)
                        {
                            m.TanggalWawancara = item.f.CreatedDate.ToString("dd-MM-yyyy");
                            m.LastStatus = StatusPpdbConstant.Dict[StatusPpdbConstant.Wawancara];
                            m.LastStatusProses = StatusPpdbWawancaraConstant.Dict[item.f.StatusProses];
                            m.LastCatatan = item.f.CatatanProses;
                        }
                        if (item.g != null)
                        {
                            m.TanggalTestObservasi = item.g.CreatedDate.ToString("dd-MM-yyyy");
                            m.LastStatus = StatusPpdbConstant.Dict[StatusPpdbConstant.TestObservasi];
                            m.LastStatusProses = StatusPpdbTestObservasiConstant.Dict[item.g.StatusProses];
                            m.LastCatatan = item.g.CatatanProses;
                        }
                        if (item.h != null)
                        {
                            m.TanggalInputObservasi = item.h.CreatedDate.ToString("dd-MM-yyyy");
                            m.LastStatus = StatusPpdbConstant.Dict[StatusPpdbConstant.InputObservasi];
                            m.LastStatusProses = StatusPpdbHasilObservasiConstant.Dict[item.h.StatusProses];
                            m.LastCatatan = item.h.CatatanProses;
                        }
                        if (item.i != null)
                        {
                            m.TanggalVerifikasiObservasi = item.i.CreatedDate.ToString("dd-MM-yyyy");
                            m.LastStatus = StatusPpdbConstant.Dict[StatusPpdbConstant.VerifikasiObservasi];
                            m.LastStatusProses = StatusPpdbVerifikasiObservasiConstant.Dict[item.i.StatusProses];
                            m.LastCatatan = item.i.CatatanProses;
                        }
                        if (item.j != null)
                        {
                            m.TanggalBayarPendidikan = item.j.CreatedDate.ToString("dd-MM-yyyy");
                            m.LastStatus = StatusPpdbConstant.Dict[StatusPpdbConstant.PelunasanBiayaPendidikan];
                            m.LastStatusProses = "";
                            m.LastCatatan = "";
                        }
                        if (item.a.IdPegawai != null)
                            m.IsAnakPegawai = "YA";
                        if (item.a.IdSiswa != null)
                        {
                            var tbSiswa = conn.Get<TbSiswa>(item.a.IdSiswa);
                            if (tbSiswa != null)
                                m.NisSiswa = tbSiswa.Nis;
                        }
                        if (item.a.IdSiswaSodara != null)
                        {
                            var tbSiswa = conn.Get<TbSiswa>(item.a.IdSiswaSodara);
                            if (tbSiswa != null)
                                m.NisSodara = tbSiswa.Nis;
                        }

                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbSiswas", ex);
                return null;
            }
        }

        public List<PpdbSiswaListModel> GetPpdbSiswasByAdmin(int IdUser, out string oMessage)
        {
            try
            {
                List<PpdbSiswaListModel> ret = new List<PpdbSiswaListModel>();
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var user = (from a in conn.GetList<TbUnitPegawai>()
                                join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                join d in conn.GetList<TbUserRole>() on a.IdPegawai equals d.IdUser
                                where a.IdPegawai == IdUser
                                select new { a, b, c, d }).FirstOrDefault();
                    if (user == null) { oMessage = "pengguna tidak terdaftar sebagai pegawai"; return null; }

                    bool isAdmin = false;
                    int[] _listUnit = new int[] { UnitConstant.DIR, UnitConstant.AKT, UnitConstant.FNC };
                    isAdmin = _listUnit.Contains(user.a.IdUnit);

                    if (!isAdmin)
                    {
                        int[] _lisRole = new int[] { RoleApplConstant.Admin, RoleApplConstant.Admin2 };
                        isAdmin = _lisRole.Contains(user.d.IdRole);
                    }

                    var data = from a in conn.GetList<TbPpdbSiswa>()
                               join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                               join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                               join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                               where c.IdUnit == (isAdmin ? c.IdUnit : user.a.IdUnit)
                               select new { a, b, c, d };
                    foreach (var item in data)
                    {
                        // var tbStatusPpdb = (from a in conn.GetList<TbPpdbSiswaStatus>()
                        //                     where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                        //                     select a).LastOrDefault();
                        var tbStatusPpdb = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                            where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                            select new
                                            {
                                                a.Status,
                                                a.StatusProses,
                                                maxStatus = (a.Status < 10 ? (a.Status * 10) : a.Status)
                                            }).OrderByDescending(x => x.maxStatus).FirstOrDefault();
                        string peminatan = string.Empty;
                        if (item.b.IdJenisPeminatan != null)
                            if (item.d.IdUnit == UnitConstant.Sma)
                            {
                                peminatan = JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb[(int)item.b.IdJenisPeminatan];
                            }
                        PpdbSiswaListModel m = new PpdbSiswaListModel
                        {
                            IdPpdbDaftar = item.a.IdPpdbDaftar,
                            Unit = item.d.Nama,
                            Kelas = item.c.Nama,
                            Peminatan = peminatan,
                            JenisKategoriPendaftaran = KategoriPendaftaranPpdbConstant.DictKategoriPendaftaranPpdb[item.a.IdJenisKategoriPendaftaran],
                            JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[item.a.IdJalurPendaftaran],
                            JenisPendaftaran = JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[item.a.IdJenisPendaftaran],
                            Nama = item.a.Nama,
                            TanggalLahir = item.a.TanggalLahir.ToString("dd-MM-yyyy"),
                            Status = StatusPpdbConstant.Dict[tbStatusPpdb.Status],
                            SekolahAsal = item.a.SekolahAsal,
                            StatusProses = "",
                            NamaPegawai = "",
                            NamaSodara = ""
                        };

                        if (tbStatusPpdb.Status == StatusPpdbConstant.TestObservasi)
                        {
                            m.StatusProses = StatusPpdbTestObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                        }
                        else if (tbStatusPpdb.Status == StatusPpdbConstant.Wawancara)
                        {
                            m.StatusProses = StatusPpdbWawancaraConstant.Dict[tbStatusPpdb.StatusProses];
                        }
                        else if (tbStatusPpdb.Status == StatusPpdbConstant.TestObservasiWawancara)
                        {
                            m.StatusProses = StatusPpdbTestObservasiWawancaraConstant.Dict[tbStatusPpdb.StatusProses];
                        }
                        else if (tbStatusPpdb.Status == StatusPpdbConstant.InputObservasi)
                        {
                            m.StatusProses = StatusPpdbHasilObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                        }
                        else if (tbStatusPpdb.Status == StatusPpdbConstant.VerifikasiObservasi)
                        {
                            m.StatusProses = StatusPpdbVerifikasiObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                        }
                        if (item.a.IdSiswa != null)
                            m.SekolahAsal = conn.GetList<TbSekolah>().FirstOrDefault().Nama;
                        if (item.a.IdSiswaSodara != null)
                            m.NamaSodara = conn.Get<TbSiswa>(item.a.IdSiswaSodara).Nama;
                        if (item.a.IdPegawai != null)
                        {
                            m.NamaPegawai = (from a in conn.GetList<TbUser>()
                                             where a.IdUser == item.a.IdPegawai
                                             select userAppService.SetFullName(a.FirstName, a.MiddleName, a.LastName)).FirstOrDefault();
                        }
                        ret.Add(m);
                    }
                    if (ret.Count == 0) { oMessage = "data tidak ada"; }
                    return ret;
                    // return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbSiswasByAdmin", ex);
                return null;
            }
        }

        public List<PpdbSiswaObservasiWawancara> GetPpdbSiswaObservasiWawancaras(int IdUser, out string oMessage)
        {
            try
            {
                List<PpdbSiswaObservasiWawancara> ret = new List<PpdbSiswaObservasiWawancara>();
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var user = (from a in conn.GetList<TbUnitPegawai>()
                                join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                join d in conn.GetList<TbUserRole>() on a.IdPegawai equals d.IdUser
                                where a.IdPegawai == IdUser
                                select new { a, b, c, d }).FirstOrDefault();
                    if (user == null) { oMessage = "pengguna tidak terdaftar sebagai pegawai"; return null; }

                    bool isAdmin = false;
                    int[] _listUnit = new int[] { UnitConstant.DIR, UnitConstant.AKT, UnitConstant.FNC };
                    isAdmin = _listUnit.Contains(user.a.IdUnit);

                    if (!isAdmin)
                    {
                        int[] _lisRole = new int[] { RoleApplConstant.Admin, RoleApplConstant.Admin2 };
                        isAdmin = _lisRole.Contains(user.d.IdRole);
                    }


                    var data = from a in conn.GetList<TbPpdbSiswa>()
                               join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                               join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                               join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                               join e in conn.GetList<TbPpdbSiswaObservasi>() on a.IdPpdbDaftar equals e.IdPpdbDaftar
                               join f in conn.GetList<TbPpdbSiswaWawancara>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                               join g in conn.GetList<TbPpdbJadwalObservasi>() on e.IdPpdbJadwalObservasi equals g.IdPpdbJadwalObservasi
                               join h in conn.GetList<TbPpdbJadwalWawancara>() on f.IdPpdbJadwalWawancara equals h.IdPpdbJadwalWawancara
                               join userObservasi in conn.GetList<TbUser>() on g.IdPegawai equals userObservasi.IdUser
                               join userWawancara in conn.GetList<TbUser>() on h.IdPegawai equals userWawancara.IdUser
                               join profileObservasi in conn.GetList<TbUser>() on g.IdPegawai equals profileObservasi.IdUser
                               join profileWawancara in conn.GetList<TbUser>() on h.IdPegawai equals profileWawancara.IdUser
                               where c.IdUnit == (isAdmin ? c.IdUnit : user.a.IdUnit)
                               select new { a, b, c, d, g, h, userObservasi, userWawancara, profileObservasi, profileWawancara };
                    foreach (var item in data.OrderBy(x => x.a.IdPpdbDaftar))
                    {
                        // var tbStatusPpdb = (from a in conn.GetList<TbPpdbSiswaStatus>()
                        //                     where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                        //                     select a).LastOrDefault();

                        var tbStatusPpdb = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                            where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                            select new
                                            {
                                                a.Status,
                                                a.StatusProses,
                                                maxStatus = (a.Status < 10 ? (a.Status * 10) : a.Status)
                                            }).OrderByDescending(x => x.maxStatus).FirstOrDefault();
                        string peminatan = string.Empty;
                        if (item.b.IdJenisPeminatan != null)
                            if (item.d.IdUnit == UnitConstant.Sma)
                            {
                                peminatan = JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb[(int)item.b.IdJenisPeminatan];
                            }

                        PpdbSiswaObservasiWawancara m = new PpdbSiswaObservasiWawancara
                        {
                            IdPpdbDaftar = item.a.IdPpdbDaftar,
                            Unit = item.d.Nama,
                            Kelas = item.c.Nama,
                            Peminatan = peminatan,
                            JenisKategoriPendaftaran = KategoriPendaftaranPpdbConstant.DictKategoriPendaftaranPpdb[item.a.IdJenisKategoriPendaftaran],
                            JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[item.a.IdJalurPendaftaran],
                            JenisPendaftaran = JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[item.a.IdJenisPendaftaran],
                            Nama = item.a.Nama,
                            TanggalLahir = item.a.TanggalLahir.ToString("dd-MM-yyyy"),
                            Status = StatusPpdbConstant.Dict[tbStatusPpdb.Status],
                            SekolahAsal = item.a.SekolahAsal,
                            StatusProses = "",
                            NamaPegawai = "",
                            NamaSodara = "",

                            NamaPetugasObservasi = userAppService.SetFullName(item.profileObservasi.FirstName, item.profileObservasi.MiddleName, item.profileObservasi.LastName),
                            NamaPetugasWawancara = userAppService.SetFullName(item.profileWawancara.FirstName, item.profileWawancara.MiddleName, item.profileWawancara.LastName),
                            EmailPetugasObservasi = item.userObservasi.Email,
                            EmailPetugasWawancara = item.userWawancara.Email,
                            NoHpPetugasObservasi = item.profileObservasi.PhoneNumber,
                            NoHpPetugasWawancara = item.profileWawancara.PhoneNumber,
                            TanggalObservasi = item.g.Tanggal.ToString("dd-MM-yyyy"),
                            TanggalWawancara = item.h.Tanggal.ToString("dd-MM-yyyy"),
                            JamObservasi = item.g.JamAwal.ToString(),
                            JamWawancara = item.h.JamAwal.ToString(),
                        };

                        if (tbStatusPpdb.Status == StatusPpdbConstant.TestObservasi)
                        {
                            m.StatusProses = StatusPpdbTestObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                        }
                        else if (tbStatusPpdb.Status == StatusPpdbConstant.Wawancara)
                        {
                            m.StatusProses = StatusPpdbWawancaraConstant.Dict[tbStatusPpdb.StatusProses];
                        }
                        else if (tbStatusPpdb.Status == StatusPpdbConstant.TestObservasiWawancara)
                        {
                            m.StatusProses = StatusPpdbTestObservasiWawancaraConstant.Dict[tbStatusPpdb.StatusProses];
                        }
                        else if (tbStatusPpdb.Status == StatusPpdbConstant.InputObservasi)
                        {
                            m.StatusProses = StatusPpdbHasilObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                        }
                        else if (tbStatusPpdb.Status == StatusPpdbConstant.VerifikasiObservasi)
                        {
                            m.StatusProses = StatusPpdbVerifikasiObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                        }
                        if (item.a.IdSiswa != null)
                            m.SekolahAsal = conn.GetList<TbSekolah>().FirstOrDefault().Nama;
                        if (item.a.IdSiswaSodara != null)
                            m.NamaSodara = conn.Get<TbSiswa>(item.a.IdSiswaSodara).Nama;
                        if (item.a.IdPegawai != null)
                        {
                            m.NamaPegawai = (from a in conn.GetList<TbUser>()
                                             where a.IdUser == item.a.IdPegawai
                                             select userAppService.SetFullName(a.FirstName, a.MiddleName, a.LastName)).FirstOrDefault();
                        }
                        ret.Add(m);
                    }
                    if (ret.Count == 0) { oMessage = "data tidak ada"; }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbSiswaObservasiWawancara", ex);
                return null;
            }
        }

        public List<PpdbSiswaObservasiWawancara> GetPpdbSiswaObservasiWawancaras(int IdUser, int Tipe, out string oMessage)
        {
            try
            {
                List<PpdbSiswaObservasiWawancara> ret = new List<PpdbSiswaObservasiWawancara>();
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    var user = (from a in conn.GetList<TbUnitPegawai>()
                                join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                join d in conn.GetList<TbUserRole>() on a.IdPegawai equals d.IdUser
                                where a.IdPegawai == IdUser
                                select new { a, b, c, d }).FirstOrDefault();
                    if (user == null) { oMessage = "pengguna tidak terdaftar sebagai pegawai"; return null; }

                    bool isAdmin = false;
                    int[] _listUnit = new int[] { UnitConstant.DIR, UnitConstant.AKT, UnitConstant.FNC };
                    isAdmin = _listUnit.Contains(user.a.IdUnit);

                    if (!isAdmin)
                    {
                        int[] _lisRole = new int[] { RoleApplConstant.Admin, RoleApplConstant.Admin2 };
                        isAdmin = _lisRole.Contains(user.d.IdRole);
                    }

                    bool isTrue = false;
                    string strWhereIn = string.Empty;
                    string strWhereNotExists = string.Empty;
                    strWhereIn = string.Format("where status in ({0})", StatusPpdbConstant.Registrasi);
                    strWhereNotExists = string.Format("where status in ({0},{1})", StatusPpdbConstant.Pengesahan, StatusPpdbConstant.Wawancara);
                    if (Tipe == StatusPpdbConstant.TestObservasi)
                    {
                        strWhereIn = string.Format("where status in ({0})", StatusPpdbConstant.Registrasi);
                        strWhereNotExists = string.Format("where status in ({0},{1})", StatusPpdbConstant.Pengesahan, StatusPpdbConstant.TestObservasi);
                        isTrue = true;
                    }

                    var data = from a in conn.GetList<TbPpdbSiswa>()
                               join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                               join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                               join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                               join e in conn.GetList<TbPpdbSiswaObservasi>() on a.IdPpdbDaftar equals e.IdPpdbDaftar
                               join f in conn.GetList<TbPpdbSiswaWawancara>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                               join g in conn.GetList<TbPpdbJadwalObservasi>() on e.IdPpdbJadwalObservasi equals g.IdPpdbJadwalObservasi
                               join h in conn.GetList<TbPpdbJadwalWawancara>() on f.IdPpdbJadwalWawancara equals h.IdPpdbJadwalWawancara
                               join userObservasi in conn.GetList<TbUser>() on g.IdPegawai equals userObservasi.IdUser
                               join userWawancara in conn.GetList<TbUser>() on h.IdPegawai equals userWawancara.IdUser
                               join profileObservasi in conn.GetList<TbUser>() on g.IdPegawai equals profileObservasi.IdUser
                               join profileWawancara in conn.GetList<TbUser>() on h.IdPegawai equals profileWawancara.IdUser
                               join i in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals i.IdPpdbDaftar
                               where c.IdUnit == (isAdmin ? c.IdUnit : user.a.IdUnit)
                               && (isTrue ? userObservasi.IdUser == IdUser : userWawancara.IdUser == IdUser)
                                & !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                               select new { a, b, c, d, g, h, userObservasi, userWawancara, profileObservasi, profileWawancara };

                    if (user.c.Level == LevelJabatanPegawaiConstant.ManajemenUnit)
                    {
                        data = from a in conn.GetList<TbPpdbSiswa>()
                               join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                               join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                               join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                               join e in conn.GetList<TbPpdbSiswaObservasi>() on a.IdPpdbDaftar equals e.IdPpdbDaftar
                               join f in conn.GetList<TbPpdbSiswaWawancara>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                               join g in conn.GetList<TbPpdbJadwalObservasi>() on e.IdPpdbJadwalObservasi equals g.IdPpdbJadwalObservasi
                               join h in conn.GetList<TbPpdbJadwalWawancara>() on f.IdPpdbJadwalWawancara equals h.IdPpdbJadwalWawancara
                               join userObservasi in conn.GetList<TbUser>() on g.IdPegawai equals userObservasi.IdUser
                               join userWawancara in conn.GetList<TbUser>() on h.IdPegawai equals userWawancara.IdUser
                               join profileObservasi in conn.GetList<TbUser>() on g.IdPegawai equals profileObservasi.IdUser
                               join profileWawancara in conn.GetList<TbUser>() on h.IdPegawai equals profileWawancara.IdUser
                               join i in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals i.IdPpdbDaftar
                               where c.IdUnit == (isAdmin ? c.IdUnit : user.a.IdUnit)
                                & !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                               select new { a, b, c, d, g, h, userObservasi, userWawancara, profileObservasi, profileWawancara };
                    }
                    foreach (var item in data.OrderBy(x => x.a.IdPpdbDaftar))
                    {
                        // var tbStatusPpdb = (from a in conn.GetList<TbPpdbSiswaStatus>()
                        //                     where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                        //                     select a).LastOrDefault();

                        var tbStatusPpdb = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                            where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                            select new
                                            {
                                                a.Status,
                                                a.StatusProses,
                                                a.CatatanProses,
                                                maxStatus = (a.Status < 10 ? (a.Status * 10) : a.Status)
                                            }).OrderByDescending(x => x.maxStatus).FirstOrDefault();
                        string peminatan = string.Empty;
                        if (item.b.IdJenisPeminatan != null)
                            if (item.d.IdUnit == UnitConstant.Sma)
                            {
                                peminatan = JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb[(int)item.b.IdJenisPeminatan];
                            }

                        PpdbSiswaObservasiWawancara m = new PpdbSiswaObservasiWawancara
                        {
                            IdPpdbDaftar = item.a.IdPpdbDaftar,
                            Unit = item.d.Nama,
                            Kelas = item.c.Nama,
                            Peminatan = peminatan,
                            JenisKategoriPendaftaran = KategoriPendaftaranPpdbConstant.DictKategoriPendaftaranPpdb[item.a.IdJenisKategoriPendaftaran],
                            JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[item.a.IdJalurPendaftaran],
                            JenisPendaftaran = JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[item.a.IdJenisPendaftaran],
                            Nama = item.a.Nama,
                            TanggalLahir = item.a.TanggalLahir.ToString("dd-MM-yyyy"),
                            Status = StatusPpdbConstant.Dict[tbStatusPpdb.Status],
                            SekolahAsal = item.a.SekolahAsal,
                            NoHandphoneSiswa = item.a.NoHandphone,
                            StatusProses = "",
                            NamaPegawai = "",
                            NamaSodara = "",

                            NamaPetugasObservasi = userAppService.SetFullName(item.profileObservasi.FirstName, item.profileObservasi.MiddleName, item.profileObservasi.LastName),
                            NamaPetugasWawancara = userAppService.SetFullName(item.profileWawancara.FirstName, item.profileWawancara.MiddleName, item.profileWawancara.LastName),
                            EmailPetugasObservasi = item.userObservasi.Email,
                            EmailPetugasWawancara = item.userWawancara.Email,
                            NoHpPetugasObservasi = item.profileObservasi.PhoneNumber,
                            NoHpPetugasWawancara = item.profileWawancara.PhoneNumber,
                            TanggalObservasi = item.g.Tanggal.ToString("dd-MM-yyyy"),
                            TanggalWawancara = item.h.Tanggal.ToString("dd-MM-yyyy"),
                            JamObservasi = item.g.JamAwal.ToString(),
                            JamWawancara = item.h.JamAwal.ToString(),
                        };

                        if (tbStatusPpdb.Status == StatusPpdbConstant.TestObservasi)
                        {
                            m.StatusProses = StatusPpdbTestObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                        }
                        else if (tbStatusPpdb.Status == StatusPpdbConstant.Wawancara)
                        {
                            m.StatusProses = StatusPpdbWawancaraConstant.Dict[tbStatusPpdb.StatusProses];
                        }
                        else if (tbStatusPpdb.Status == StatusPpdbConstant.TestObservasiWawancara)
                        {
                            m.StatusProses = StatusPpdbTestObservasiWawancaraConstant.Dict[tbStatusPpdb.StatusProses];
                        }
                        else if (tbStatusPpdb.Status == StatusPpdbConstant.InputObservasi)
                        {
                            m.StatusProses = StatusPpdbHasilObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                        }
                        else if (tbStatusPpdb.Status == StatusPpdbConstant.VerifikasiObservasi)
                        {
                            m.StatusProses = StatusPpdbVerifikasiObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                        }
                        m.CatatanProses = tbStatusPpdb.CatatanProses;
                        if (item.a.IdSiswa != null)
                            m.SekolahAsal = conn.GetList<TbSekolah>().FirstOrDefault().Nama;
                        if (item.a.IdSiswaSodara != null)
                            m.NamaSodara = conn.Get<TbSiswa>(item.a.IdSiswaSodara).Nama;
                        if (item.a.IdPegawai != null)
                        {
                            m.NamaPegawai = (from a in conn.GetList<TbUser>()
                                             where a.IdUser == item.a.IdPegawai
                                             select userAppService.SetFullName(a.FirstName, a.MiddleName, a.LastName)).FirstOrDefault();
                        }
                        ret.Add(m);
                    }
                    if (ret.Count == 0) { oMessage = "data tidak ada"; }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbSiswaObservasiWawancara", ex);
                return null;
            }
        }


        public string UploadFileAkteKk(int IdPpdbDaftar, IFormFile FileAkte, IFormFile FileKK)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.Get<TbPpdbDaftar>(IdPpdbDaftar);
                    if (data == null) return "data tidak ada";

                    if (FileAkte == null) return "file akte wajib di upload";
                    if (FileKK == null) return "file kartu keluarga wajib di upload";

                    string _fileName = string.Empty;
                    string _pathFileUpload = AppSetting.PathPpdbBerkas;
                    if (FileAkte != null)
                    {
                        var Foto = data.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.Akte];
                        string[] getsFoto = Directory.GetFiles(_pathFileUpload, string.Format("{0}.*", Foto));
                        Foto = getsFoto.Count() != 0 ? new DirectoryInfo(getsFoto[0]).Name : null;
                        if (Foto != null)
                            File.Delete(Path.Combine(_pathFileUpload, Foto));

                        _fileName = string.Format("{0}{1}",
                                data.IdPpdbDaftar,
                                NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.Akte]);
                        _fileName += Path.GetExtension(FileAkte.FileName);
                        _fileName = Path.Combine(_pathFileUpload, _fileName);
                        using (var fileStream = new FileStream(_fileName, FileMode.Create))
                        {
                            FileAkte.CopyTo(fileStream);
                        }
                    }

                    if (FileKK != null)
                    {
                        var Foto = data.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuKeluarga];
                        string[] getsFoto = Directory.GetFiles(_pathFileUpload, string.Format("{0}.*", Foto));
                        Foto = getsFoto.Count() != 0 ? new DirectoryInfo(getsFoto[0]).Name : null;
                        if (Foto != null)
                            File.Delete(Path.Combine(_pathFileUpload, Foto));

                        _fileName = string.Format("{0}{1}",
                                data.IdPpdbDaftar,
                                NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuKeluarga]);
                        _fileName += Path.GetExtension(FileKK.FileName);
                        _fileName = Path.Combine(_pathFileUpload, _fileName);
                        using (var fileStream = new FileStream(_fileName, FileMode.Create))
                        {
                            FileKK.CopyTo(fileStream);
                        }
                    }



                    return null;
                }
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UploadFileAkteKk", ex);
            }
        }

        public string UploadAbsensiObservasi(int IdUser, List<PpdbAbsensiSheetModel> Data)
        {
            try
            {
                foreach (var item in Data)
                {
                    if (item.Status == "Hadir")
                        SetHadirTestObservasi(IdUser, item.IdPpdbDaftar, item.Catatan);
                }
                return null;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UploadAbsensiObservasi", ex);
            }
        }

        public string UploadAbsensiWawancara(int IdUser, List<PpdbAbsensiSheetModel> Data)
        {
            try
            {
                foreach (var item in Data)
                {
                    if (item.Status == "Hadir")
                        SetHadirWawancara(IdUser, item.IdPpdbDaftar, item.Catatan);
                }
                return null;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UploadAbsensiWawancara", ex);
            }
        }

        private List<PpdbSiswaModel> GetPpdbSiswaDatas(IDbConnection conn, int IdUser, int Status, out string oMessage)
        {
            oMessage = string.Empty;
            List<PpdbSiswaModel> retx = new List<PpdbSiswaModel>();
            try
            {
                var user = (from a in conn.GetList<TbUnitPegawai>()
                            join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                            join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                            where a.IdPegawai == IdUser
                            select new { a, b, c }).FirstOrDefault();
                if (user == null) { oMessage = "pengguna tidak terdaftar sebagai pegawai"; return null; }

                bool isAdmin = false;
                int[] _listUnit = new int[] { UnitConstant.DIR, UnitConstant.AKT, UnitConstant.FNC };
                isAdmin = _listUnit.Contains(user.a.IdUnit);

                string strWhereIn = string.Empty;
                string strWhereNotExists = string.Empty;
                switch (Status)
                {
                    case StatusPpdbConstant.TestObservasi:
                        strWhereIn = string.Format("where status in ({0})", StatusPpdbConstant.Registrasi);
                        strWhereNotExists = string.Format("where status in ({0},{1})", StatusPpdbConstant.Pengesahan, StatusPpdbConstant.TestObservasi);
                        break;
                    case StatusPpdbConstant.Wawancara:
                        strWhereIn = string.Format("where status in ({0})", StatusPpdbConstant.Registrasi);
                        strWhereNotExists = string.Format("where status in ({0},{1})", StatusPpdbConstant.Pengesahan, StatusPpdbConstant.Wawancara);
                        break;
                    case StatusPpdbConstant.InputObservasi:
                        strWhereIn = string.Format("where status in ({0})", StatusPpdbConstant.TestObservasiWawancara);
                        strWhereNotExists = string.Format("where status in ({0},{1})", StatusPpdbConstant.Pengesahan, StatusPpdbConstant.InputObservasi);
                        break;
                    case StatusPpdbConstant.VerifikasiObservasi:
                        strWhereIn = string.Format("where status in ({0})", StatusPpdbConstant.InputObservasi);
                        strWhereNotExists = string.Format("where status in ({0},{1})", StatusPpdbConstant.Pengesahan, StatusPpdbConstant.VerifikasiObservasi);
                        break;
                    case StatusPpdbConstant.PelunasanBiayaPendidikan:
                        strWhereIn = string.Format("where status in ({0}) and status_proses in ({1})", StatusPpdbConstant.VerifikasiObservasi, StatusPpdbVerifikasiObservasiConstant.Diterima);
                        strWhereNotExists = string.Format("where status in ({0},{1})", StatusPpdbConstant.Pengesahan, StatusPpdbConstant.PelunasanBiayaPendidikan);
                        break;
                    case StatusPpdbConstant.Pengesahan:
                        strWhereIn = string.Format("where status in ({0})", StatusPpdbConstant.PelunasanBiayaPendidikan);
                        strWhereNotExists = string.Format("where status in ({0})", StatusPpdbConstant.Pengesahan);
                        break;
                    case -1: //untuk undir diri, ditangguhkan/cadangan, tidak diterima
                        strWhereIn = string.Format("where status in ({0}) and status_proses in ({1}, {2}, {3})",
                            StatusPpdbConstant.VerifikasiObservasi,
                            StatusPpdbVerifikasiObservasiConstant.UndurDiri,
                            StatusPpdbVerifikasiObservasiConstant.Ditangguhkan,
                            StatusPpdbVerifikasiObservasiConstant.TidakDiterima);
                        strWhereNotExists = string.Format("where status in ({0},{1})", StatusPpdbConstant.Pengesahan, StatusPpdbConstant.PelunasanBiayaPendidikan);
                        break;
                    default:
                        strWhereIn = string.Format("where status in ({0})", StatusPpdbConstant.Registrasi);
                        strWhereNotExists = string.Format("where status in ({0},{1},{2},{3},{4},{5},{6})",
                            StatusPpdbConstant.TestObservasi,
                            StatusPpdbConstant.Wawancara,
                            StatusPpdbConstant.TestObservasiWawancara,
                            StatusPpdbConstant.InputObservasi,
                            StatusPpdbConstant.VerifikasiObservasi,
                            StatusPpdbConstant.PelunasanBiayaPendidikan,
                            StatusPpdbConstant.Pengesahan
                            );
                        break;
                }

                var data = (from a in conn.GetList<TbPpdbSiswa>()
                            join b1 in conn.GetList<TbPpdbBiayaSiswa>() on a.IdPpdbDaftar equals b1.IdPpdbDaftar
                            join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                            join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                            join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                            join e in conn.GetList<TbSekolah>() on d.IdSekolah equals e.IdSekolah
                            join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                            where c.IdUnit == (isAdmin ? c.IdUnit : user.a.IdUnit)
                            & !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                            select new { a, b, b1, c, d, e }).ToList();
                if (data == null) { oMessage = "data tidak ada"; return null; }

                foreach (var item in data)
                {
                    PpdbSiswaModel ret = new PpdbSiswaModel
                    {
                        TagPpdb = new List<PpdbBiayaSiswaDetilModel>(),
                        TagCicil = new List<PpdbBiayaSiswaCicilModel>(),
                        DataOrtu = new List<PpdbSiswaOrtuAddModel>(),
                        Prestasi = new List<PpdbSiswaPrestasiModel>(),
                        Rapor = new List<PpdbSiswaRaporModel>(),
                    };
                    //prestasi
                    var tbPpdbSiswaPrestasi = (from a in conn.GetList<TbPpdbSiswaPrestasi>()
                                               where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                               select a).ToList();
                    foreach (var value in tbPpdbSiswaPrestasi)
                    {
                        var FileSertifikat = value.FileSertifikat;
                        string[] getFileSertifikat = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileSertifikat));
                        FileSertifikat = getFileSertifikat.Count() != 0 ? new DirectoryInfo(getFileSertifikat[0]).Name : null;

                        ret.Prestasi.Add(new PpdbSiswaPrestasiModel
                        {
                            IdPpdbSiswaPrestasi = value.IdPpdbSiswaPrestasi,
                            IdPpdbDaftar = value.IdPpdbDaftar,
                            IdJenisPrestasi = value.IdJenisPrestasi,
                            IdJenisTingkatPrestasi = value.IdJenisTingkatPrestasi,
                            JenisPrestasi = JenisPrestasiConstant.DictJenisPrestasi[value.IdJenisPrestasi],
                            JenisTingkatPrestasi = TingkatPrestasiSiswaConstant.DictTingkatPrestasiSiswa[value.IdJenisTingkatPrestasi],
                            Tahun = value.Tahun,
                            NamaPenyelenggara = value.NamaPenyelenggara,
                            Keterangan = value.Keterangan,
                            FileSertifikat = FileSertifikat,
                        });

                    }
                    //rapor
                    var tbPpdbSiswaRapor = (from a in conn.GetList<TbPpdbSiswaRapor>()
                                            where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                            select a).ToList();
                    foreach (var value in tbPpdbSiswaRapor)
                    {
                        var FileRapor = value.FileRapor;
                        string[] getFileRapor = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileRapor));
                        FileRapor = getFileRapor.Count() != 0 ? new DirectoryInfo(getFileRapor[0]).Name : null;

                        ret.Rapor.Add(new PpdbSiswaRaporModel
                        {
                            Semester = value.Semester,
                            Mapel = value.Mapel,
                            Nilai = value.Nilai,
                            FileRapor = FileRapor
                        });
                    }

                    ret.IdPpdbDaftar = item.a.IdPpdbDaftar;
                    ret.IdPpdbSeragam = item.a.IdPpdbSeragam;
                    ret.Nik = item.a.Nik;
                    ret.Nisn = item.a.Nisn;
                    ret.Nama = item.a.Nama;
                    ret.NamaPanggilan = item.a.NamaPanggilan;
                    ret.TempatLahir = item.a.TempatLahir;
                    ret.IdAgama = item.a.IdAgama;
                    ret.IdKewarganegaraan = item.a.IdKewarganegaraan;
                    ret.KdJenisKelamin = item.a.KdJenisKelamin;
                    ret.KdGolonganDarah = item.a.KdGolonganDarah;
                    ret.AlamatTinggal = item.a.AlamatTinggal;
                    ret.IdTinggal = item.a.IdTinggal;
                    ret.Email = item.a.Email;
                    ret.NoHandphone = item.a.NoHandphone;
                    ret.NoDarurat = item.a.NoDarurat;
                    ret.IdBahasa = item.a.IdBahasa == null ? 0 : (int)item.a.IdBahasa;
                    ret.TinggiBadan = item.a.TinggiBadan == null ? 0 : (int)item.a.TinggiBadan;
                    ret.BeratBadan = item.a.BeratBadan == null ? 0 : (int)item.a.BeratBadan;
                    ret.AnakKe = item.a.AnakKe == null ? 0 : (int)item.a.AnakKe;
                    ret.JumlahSodaraKandung = item.a.JumlahSodaraKandung == null ? 0 : (int)item.a.JumlahSodaraKandung;
                    ret.JumlahSodaraTiri = item.a.JumlahSodaraTiri == null ? 0 : (int)item.a.JumlahSodaraTiri;
                    ret.JumlahSodaraAngkat = item.a.JumlahSodaraAngkat == null ? 0 : (int)item.a.JumlahSodaraAngkat;
                    ret.IdTransportasi = item.a.IdTransportasi;
                    ret.JarakKeSekolah = item.a.JarakKeSekolah == null ? 0 : (int)item.a.JarakKeSekolah;
                    ret.WaktuTempuh = item.a.WaktuTempuh == null ? 0 : (int)item.a.WaktuTempuh;
                    ret.PenyakitDiderita = item.a.PenyakitDiderita;
                    ret.KelainanJasmani = item.a.KelainanJasmani;
                    ret.SekolahAsal = item.a.SekolahAsal;
                    ret.AlamatSekolahAsal = item.a.AlamatSekolahAsal;
                    ret.NspnSekolahAsal = item.a.NspnSekolahAsal;
                    ret.IdPpdbKelas = item.a.IdPpdbKelas;
                    ret.IdJenisKategoriPendaftaran = item.a.IdJenisKategoriPendaftaran;
                    ret.IdJenisPendaftaran = item.a.IdJenisPendaftaran;
                    ret.IdJalurPendaftaran = item.a.IdJalurPendaftaran;
                    ret.TanggalLahir = item.a.TanggalLahir.ToString("dd-MM-yyyy");
                    ret.IdSiswa = item.a.IdSiswa;
                    ret.IdPegawai = item.a.IdPegawai;
                    ret.IdSiswaSodara = item.a.IdSiswaSodara;
                    ret.Unit = item.d.Nama;
                    ret.Kelas = item.c.Nama;
                    string peminatan = string.Empty;
                    if (item.b.IdJenisPeminatan != null)
                        if (item.d.IdUnit == UnitConstant.Sma)
                        {
                            peminatan = JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb[(int)item.b.IdJenisPeminatan];
                        }
                    ret.Peminatan = peminatan;
                    ret.JenisKategoriPendaftaran = KategoriPendaftaranPpdbConstant.DictKategoriPendaftaranPpdb[item.a.IdJenisKategoriPendaftaran];
                    ret.JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[item.a.IdJalurPendaftaran];
                    ret.JenisPendaftaran = JenisPendaftaranPpdbConstant.DictJenisPendaftaranPpdb[item.a.IdJenisPendaftaran];
                    if (item.a.IdSiswa != null)
                        ret.SekolahAsal = conn.GetList<TbSekolah>().FirstOrDefault().Nama;
                    if (item.a.IdSiswaSodara != null)
                        ret.NamaSodara = conn.Get<TbSiswa>(item.a.IdSiswaSodara).Nama;
                    if (item.a.IdPegawai != null)
                    {
                        ret.NamaPegawai = (from a in conn.GetList<TbUser>()
                                           where a.IdUser == item.a.IdPegawai
                                           select userAppService.SetFullName(a.FirstName, a.MiddleName, a.LastName)).FirstOrDefault();
                    }
                    // var tbPpdbSiswaStatus = (from a in conn.GetList<TbPpdbSiswaStatus>()
                    //                          where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                    //                          select a).LastOrDefault();
                    var tbPpdbSiswaStatus = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                             where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                             select new
                                             {
                                                 a.Status,
                                                 a.StatusProses,
                                                 a.CatatanProses,
                                                 maxStatus = (a.Status < 10 ? (a.Status * 10) : a.Status)
                                             }).OrderByDescending(x => x.maxStatus).FirstOrDefault();

                    ret.Status = StatusPpdbConstant.Dict[tbPpdbSiswaStatus.Status];

                    var tbPpdbSiswaStatusCatatan = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                                    where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                                    && a.Status == StatusPpdbConstant.Wawancara
                                                    select a).FirstOrDefault();
                    ret.CatatanProses = tbPpdbSiswaStatusCatatan.CatatanProses;
                    if (tbPpdbSiswaStatus.Status == StatusPpdbConstant.TestObservasi)
                    {
                        ret.StatusProses = StatusPpdbTestObservasiConstant.Dict[tbPpdbSiswaStatus.StatusProses];
                    }
                    else if (tbPpdbSiswaStatus.Status == StatusPpdbConstant.Wawancara)
                    {
                        ret.StatusProses = StatusPpdbWawancaraConstant.Dict[tbPpdbSiswaStatus.StatusProses];
                    }
                    else if (tbPpdbSiswaStatus.Status == StatusPpdbConstant.TestObservasiWawancara)
                    {
                        ret.StatusProses = StatusPpdbTestObservasiWawancaraConstant.Dict[tbPpdbSiswaStatus.StatusProses];
                    }
                    else if (tbPpdbSiswaStatus.Status == StatusPpdbConstant.InputObservasi)
                    {
                        ret.StatusProses = StatusPpdbHasilObservasiConstant.Dict[tbPpdbSiswaStatus.StatusProses];
                    }
                    else if (tbPpdbSiswaStatus.Status == StatusPpdbConstant.VerifikasiObservasi)
                    {
                        ret.StatusProses = StatusPpdbVerifikasiObservasiConstant.Dict[tbPpdbSiswaStatus.StatusProses];
                    }

                    ret.IsCicil = 0;
                    ret.IsCicil = item.b1.IsCicil;
                    ret.RpBiayaTotal = 0;
                    ret.StatusBiaya = StatusLunasConstant.Dict[item.b1.Status];
                    ret.CatatanBiaya = item.b1.Catatan;
                    ret.RefNo = item.b1.RefNo;
                    if (item.b1.TanggalLunas != null)
                        ret.TanggalLunas = ((DateTime)item.b1.TanggalLunas).ToString("dd-MM-yyyy HH24:mm:ss");
                    var biayas = from a in conn.GetList<TbPpdbBiayaSiswaDetil>()
                                 join b in conn.GetList<TbJenisBiayaPpdb>() on a.IdJenisBiayaPpdb equals b.IdJenisBiayaPpdb
                                 where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                 select new { a, b };
                    foreach (var biaya in biayas)
                    {
                        ret.TagPpdb.Add(new PpdbBiayaSiswaDetilModel
                        {
                            IdJenisBiayaPpdb = biaya.a.IdJenisBiayaPpdb,
                            JenisBiayaPpdb = biaya.b.Nama,
                            Catatan = biaya.a.Catatan,
                            Rp = biaya.a.Rp
                        });
                        ret.RpBiayaTotal += biaya.a.Rp;
                    }
                    var biayaCicils = from a in conn.GetList<TbPpdbBiayaSiswaCicil>()
                                      where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                      select a;
                    foreach (var biayaCicil in biayaCicils.OrderBy(x => x.CilKe))
                    {
                        ret.TagCicil.Add(new PpdbBiayaSiswaCicilModel
                        {
                            CilKe = biayaCicil.CilKe,
                            RpCilKe = biayaCicil.RpCilKe,
                            Status = StatusLunasConstant.Dict[biayaCicil.Status],
                            TanggalCilKe = biayaCicil.TanggalCilKe.ToString("dd-MM-yyyy"),
                            TanggalLunas = biayaCicil.TanggalLunas == null ? "" : ((DateTime)biayaCicil.TanggalLunas).ToString("dd-MM-yyyy")
                        });
                    }

                    //ortu
                    var dataOrtus = from a in conn.GetList<TbPpdbSiswaOrtu>()
                                    where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                    select a;
                    foreach (var dataOrtu in dataOrtus)
                    {
                        ret.DataOrtu.Add(new PpdbSiswaOrtuAddModel
                        {
                            IdTipe = dataOrtu.IdTipe,
                            Nama = dataOrtu.Nama,
                            Alamat = dataOrtu.Alamat,
                            Nik = dataOrtu.Nik,
                            IdJenisPekerjaan = dataOrtu.IdJenisPekerjaan,
                            NamaInstansi = dataOrtu.NamaInstansi,
                            Jabatan = dataOrtu.Jabatan,
                            Email = dataOrtu.Email,
                            NoHandphone = dataOrtu.NoHandphone,
                            NoTelpRumah = dataOrtu.NoTelpRumah,
                            IdAgama = dataOrtu.IdAgama,
                            IdKewarganegaraan = dataOrtu.IdKewarganegaraan,
                            IdJenjangPendidikan = dataOrtu.IdJenisJenjangPendidikan,
                            IdStatusPenikahan = dataOrtu.IdStatusPenikahan,
                            TempatLahir = dataOrtu.TempatLahir,
                            TanggalLahir = dataOrtu.TanggalLahir.ToString("dd-MM-yyyy"),
                            IdStatusHidup = dataOrtu.IdStatusHidup,
                            TanggalMeninggal = dataOrtu.TanggalMeninggal.ToString("dd-MM-yyyy"),
                        });
                    }

                    //file
                    var Foto = item.a.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.PasFoto];
                    string[] getsFoto = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", Foto));
                    Foto = getsFoto.Count() != 0 ? new DirectoryInfo(getsFoto[0]).Name : null;
                    ret.Foto = Foto;

                    var FileKtpIbu = item.a.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KtpIbu];
                    string[] getsFileKtpIbu = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileKtpIbu));
                    FileKtpIbu = getsFileKtpIbu.Count() != 0 ? new DirectoryInfo(getsFileKtpIbu[0]).Name : null;
                    ret.FileKtpIbu = FileKtpIbu;

                    var FileKtpAyah = item.a.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KtpAyah];
                    string[] getsFileKtpAyah = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileKtpAyah));
                    FileKtpAyah = getsFileKtpAyah.Count() != 0 ? new DirectoryInfo(getsFileKtpAyah[0]).Name : null;
                    ret.FileKtpAyah = FileKtpAyah;

                    var FileKartuKeluarga = item.a.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuKeluarga];
                    string[] getsFileKartuKeluarga = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileKartuKeluarga));
                    FileKartuKeluarga = getsFileKartuKeluarga.Count() != 0 ? new DirectoryInfo(getsFileKartuKeluarga[0]).Name : null;
                    ret.FileKartuKeluarga = FileKartuKeluarga;

                    var FileAkte = item.a.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.Akte];
                    string[] getsFileAkte = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileAkte));
                    FileAkte = getsFileAkte.Count() != 0 ? new DirectoryInfo(getsFileAkte[0]).Name : null;
                    ret.FileAkte = FileAkte;

                    var FileSuratKesehatan = item.a.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.SuratKesehatan];
                    string[] getsFileSuratKesehatan = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileSuratKesehatan));
                    FileSuratKesehatan = getsFileSuratKesehatan.Count() != 0 ? new DirectoryInfo(getsFileSuratKesehatan[0]).Name : null;
                    ret.FileSuratKesehatan = FileSuratKesehatan;

                    var FileBebasNarkoba = item.a.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KetBebasNarkoba];
                    string[] getsFileBebasNarkoba = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileBebasNarkoba));
                    FileBebasNarkoba = getsFileBebasNarkoba.Count() != 0 ? new DirectoryInfo(getsFileBebasNarkoba[0]).Name : null;
                    ret.FileBebasNarkoba = FileBebasNarkoba;

                    var FileKartuPeserta = item.a.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuPeserta];
                    string[] getsFileKartuPeserta = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileKartuPeserta));
                    FileKartuPeserta = getsFileKartuPeserta.Count() != 0 ? new DirectoryInfo(getsFileKartuPeserta[0]).Name : null;
                    ret.FileKartuPeserta = FileKartuPeserta;

                    var SuratPerjanjianCicilan = item.a.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.SuratPerjanjianCicilan];
                    string[] getsSuratPerjanjianCicilan = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", SuratPerjanjianCicilan));
                    SuratPerjanjianCicilan = getsSuratPerjanjianCicilan.Count() != 0 ? new DirectoryInfo(getsSuratPerjanjianCicilan[0]).Name : null;
                    ret.FilePerjanjianCicil = SuratPerjanjianCicilan;

                    retx.Add(ret);
                }


                return retx;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbSiswasx", ex);
                return null;
            }
        }

        public List<PpdbSiswaModel> GetPpdbSiswaDatas(int IdUser, int Status, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    return this.GetPpdbSiswaDatas(conn, IdUser, Status, out oMessage);
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbSiswaDatas", ex);
                return null;
            }
        }

        public string UploadInputObservasiMassal(int IdUser, List<PpdbInputObservasiSheetModel> Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        foreach (var item in Data)
                        {
                            int status = -1;
                            if (item.StatusHasilObservasi == "LULUS") status = 1;

                            string err = this.InputObservasi(conn, IdUser, item.IdPpdbDaftar, status, item.Catatan, item.Biayas, item.Cicils);
                            if (!string.IsNullOrEmpty(err)) return err;
                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }

                return null;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UploadInputObservasiMassal", ex);
            }
        }

        public SheetLaporanGelombangPpdbModel GetLaporanGelombangPpdb(int IdUser, string TanggalAwal, string TanggalAkhir, SheetLaporanStatikGelombangPpdbModel Data, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                SheetLaporanGelombangPpdbModel ret = new SheetLaporanGelombangPpdbModel();
                using (var conn = commonService.DbConnection())
                {
                    var user = (from a in conn.GetList<TbUnitPegawai>()
                                join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                join d in conn.GetList<TbUser>() on b.IdPegawai equals d.IdUser
                                where a.IdPegawai == IdUser
                                select new { a, b, c, d }).FirstOrDefault();
                    if (user == null) { oMessage = "pengguna tidak terdaftar sebagai pegawai"; return null; }


                    #region KUOTA PENDAFTARAN
                    var kuotaDaftarTkA_L = (from a in conn.GetList<TbPpdbDaftar>()
                                            join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                            join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                            where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                            && c.IdKelas == 104 // Tk-A
                                            && a.KdJenisKelamin == "L"
                                            select a).Count();

                    var kuotaDaftarTkA_P = (from a in conn.GetList<TbPpdbDaftar>()
                                            join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                            join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                            where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                            && c.IdKelas == 104 // Tk-A
                                            && a.KdJenisKelamin == "P"
                                            select a).Count();

                    var kuotaDaftarTkB_P = (from a in conn.GetList<TbPpdbDaftar>()
                                            join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                            join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                            where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                            && c.IdKelas == 105 // Tk-B
                                            && a.KdJenisKelamin == "L"
                                            select a).Count();

                    var kuotaDaftarTkB_L = (from a in conn.GetList<TbPpdbDaftar>()
                                            join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                            join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                            where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                            && c.IdKelas == 105 // Tk-B
                                            && a.KdJenisKelamin == "P"
                                            select a).Count();

                    var kuotaDaftarSd_P = (from a in conn.GetList<TbPpdbDaftar>()
                                           join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                           join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                           join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                           where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                           && d.IdUnit == UnitConstant.Sd
                                           && a.KdJenisKelamin == "P"
                                           select a).Count();

                    var kuotaDaftarSd_L = (from a in conn.GetList<TbPpdbDaftar>()
                                           join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                           join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                           join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                           where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                           && d.IdUnit == UnitConstant.Sd
                                           && a.KdJenisKelamin == "L"
                                           select a).Count();

                    var kuotaDaftarSmp_P = (from a in conn.GetList<TbPpdbDaftar>()
                                            join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                            join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                            join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                            where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                            && d.IdUnit == UnitConstant.Smp
                                            && a.KdJenisKelamin == "P"
                                            select a).Count();

                    var kuotaDaftarSmp_L = (from a in conn.GetList<TbPpdbDaftar>()
                                            join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                            join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                            join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                            where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                            && d.IdUnit == UnitConstant.Smp
                                            && a.KdJenisKelamin == "L"
                                            select a).Count();

                    var kuotaDaftarSmaIpa_P = (from a in conn.GetList<TbPpdbDaftar>()
                                               join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                               join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                               join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                               where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                               && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPA
                                               && d.IdUnit == UnitConstant.Sma
                                               && a.KdJenisKelamin == "P"
                                               select a).Count();

                    var kuotaDaftarSmaIpa_L = (from a in conn.GetList<TbPpdbDaftar>()
                                               join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                               join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                               join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                               where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                               && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPA
                                               && d.IdUnit == UnitConstant.Sma
                                               && a.KdJenisKelamin == "L"
                                               select a).Count();

                    var kuotaDaftarSmaIps_P = (from a in conn.GetList<TbPpdbDaftar>()
                                               join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                               join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                               join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                               where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                               && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPS
                                               && d.IdUnit == UnitConstant.Sma
                                               && a.KdJenisKelamin == "P"
                                               select a).Count();

                    var kuotaDaftarSmaIps_L = (from a in conn.GetList<TbPpdbDaftar>()
                                               join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                               join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                               join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                               where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                               && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPS
                                               && d.IdUnit == UnitConstant.Sma
                                               && a.KdJenisKelamin == "L"
                                               select a).Count();
                    #endregion

                    #region KUOTA DAFTAR TUNGGU
                    var kuotaDaftarTungguTkA_P = (from a in conn.GetList<TbPpdbDaftar>()
                                                  join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                  join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                  where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                  && c.IdKelas == 104 // Tk-A
                                                  && a.KdJenisKelamin == "P"
                                                  && a.Status == StatusPpdbDaftarConstant.DaftarTunggu
                                                  select a).Count();

                    var kuotaDaftarTungguTkA_L = (from a in conn.GetList<TbPpdbDaftar>()
                                                  join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                  join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                  where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                  && c.IdKelas == 104 // Tk-A
                                                  && a.KdJenisKelamin == "L"
                                                  && a.Status == StatusPpdbDaftarConstant.DaftarTunggu
                                                  select a).Count();

                    var kuotaDaftarTungguTkB_P = (from a in conn.GetList<TbPpdbDaftar>()
                                                  join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                  join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                  where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                  && c.IdKelas == 105 // Tk-B
                                                  && a.KdJenisKelamin == "P"
                                                  && a.Status == StatusPpdbDaftarConstant.DaftarTunggu
                                                  select a).Count();

                    var kuotaDaftarTungguTkB_L = (from a in conn.GetList<TbPpdbDaftar>()
                                                  join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                  join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                  where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                  && c.IdKelas == 105 // Tk-B
                                                  && a.KdJenisKelamin == "L"
                                                  && a.Status == StatusPpdbDaftarConstant.DaftarTunggu
                                                  select a).Count();

                    var kuotaDaftarTungguSd_P = (from a in conn.GetList<TbPpdbDaftar>()
                                                 join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                 join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                 join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                                 where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                 && d.IdUnit == UnitConstant.Sd
                                                 && a.KdJenisKelamin == "P"
                                                 && a.Status == StatusPpdbDaftarConstant.DaftarTunggu
                                                 select a).Count();

                    var kuotaDaftarTungguSd_L = (from a in conn.GetList<TbPpdbDaftar>()
                                                 join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                 join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                 join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                                 where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                 && d.IdUnit == UnitConstant.Sd
                                                 && a.KdJenisKelamin == "L"
                                                 && a.Status == StatusPpdbDaftarConstant.DaftarTunggu
                                                 select a).Count();

                    var kuotaDaftarTungguSmp_P = (from a in conn.GetList<TbPpdbDaftar>()
                                                  join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                  join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                  join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                                  where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                  && d.IdUnit == UnitConstant.Smp
                                                  && a.KdJenisKelamin == "P"
                                                  && a.Status == StatusPpdbDaftarConstant.DaftarTunggu
                                                  select a).Count();

                    var kuotaDaftarTungguSmp_L = (from a in conn.GetList<TbPpdbDaftar>()
                                                  join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                  join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                  join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                                  where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                  && d.IdUnit == UnitConstant.Smp
                                                  && a.KdJenisKelamin == "L"
                                                  && a.Status == StatusPpdbDaftarConstant.DaftarTunggu
                                                  select a).Count();

                    var kuotaDaftarTungguSmaIpa_P = (from a in conn.GetList<TbPpdbDaftar>()
                                                     join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                     join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                     join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                                     where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                     && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPA
                                                     && d.IdUnit == UnitConstant.Sma
                                                     && a.KdJenisKelamin == "P"
                                                     && a.Status == StatusPpdbDaftarConstant.DaftarTunggu
                                                     select a).Count();

                    var kuotaDaftarTungguSmaIpa_L = (from a in conn.GetList<TbPpdbDaftar>()
                                                     join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                     join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                     join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                                     where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                     && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPA
                                                     && d.IdUnit == UnitConstant.Sma
                                                     && a.KdJenisKelamin == "L"
                                                     && a.Status == StatusPpdbDaftarConstant.DaftarTunggu
                                                     select a).Count();

                    var kuotaDaftarTungguSmaIps_P = (from a in conn.GetList<TbPpdbDaftar>()
                                                     join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                     join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                     join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                                     where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                     && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPS
                                                     && d.IdUnit == UnitConstant.Sma
                                                     && a.KdJenisKelamin == "P"
                                                     && a.Status == StatusPpdbDaftarConstant.DaftarTunggu
                                                     select a).Count();

                    var kuotaDaftarTungguSmaIps_L = (from a in conn.GetList<TbPpdbDaftar>()
                                                     join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                     join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                     join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                                     where (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                     && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPS
                                                     && d.IdUnit == UnitConstant.Sma
                                                     && a.KdJenisKelamin == "L"
                                                     && a.Status == StatusPpdbDaftarConstant.DaftarTunggu
                                                     select a).Count();
                    #endregion

                    #region KUOTA LULUS OBSERVASI
                    var kuotaLulusObservasiTkA_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                    join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                    join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                    join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                    join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                    where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                    && c.IdKelas == 104 // Tk-A
                                                    && a.KdJenisKelamin == "P"
                                                    && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                    && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima
                                                    select a).Count();

                    var kuotaLulusObservasiTkA_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                    join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                    join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                    join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                    join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                    where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                    && c.IdKelas == 104 // Tk-A
                                                    && a.KdJenisKelamin == "L"
                                                    && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                    && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima
                                                    select a).Count();

                    var kuotaLulusObservasiTkB_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                    join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                    join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                    join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                    join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                    where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                    && c.IdKelas == 105 // Tk-B
                                                    && a.KdJenisKelamin == "P"
                                                    && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                    && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima
                                                    select a).Count();

                    var kuotaLulusObservasiTkB_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                    join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                    join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                    join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                    join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                    where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                    && c.IdKelas == 105 // Tk-B
                                                    && a.KdJenisKelamin == "L"
                                                    && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                    && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima
                                                    select a).Count();

                    var kuotaLulusObservasiSd_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                   join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                   join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                   join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                   join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                   join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                   where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                   && g.IdUnit == UnitConstant.Sd
                                                   && a.KdJenisKelamin == "P"
                                                   && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                   && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima
                                                   select a).Count();

                    var kuotaLulusObservasiSd_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                   join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                   join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                   join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                   join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                   join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                   where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                   && g.IdUnit == UnitConstant.Sd
                                                   && a.KdJenisKelamin == "L"
                                                   && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                   && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima
                                                   select a).Count();

                    var kuotaLulusObservasiSmp_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                    join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                    join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                    join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                    join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                    join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                    where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                    && g.IdUnit == UnitConstant.Smp
                                                    && a.KdJenisKelamin == "P"
                                                    && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                    && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima
                                                    select a).Count();

                    var kuotaLulusObservasiSmp_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                    join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                    join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                    join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                    join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                    join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                    where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                    && g.IdUnit == UnitConstant.Smp
                                                    && a.KdJenisKelamin == "L"
                                                    && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                    && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima
                                                    select a).Count();

                    var kuotaLulusObservasiSmaIpa_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                       join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                       join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                       join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                       join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                       join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                       where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                       && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPA
                                                       && g.IdUnit == UnitConstant.Sma
                                                       && a.KdJenisKelamin == "P"
                                                       && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                       && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima
                                                       select a).Count();

                    var kuotaLulusObservasiSmaIpa_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                       join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                       join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                       join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                       join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                       join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                       where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                       && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPA
                                                       && g.IdUnit == UnitConstant.Sma
                                                       && a.KdJenisKelamin == "L"
                                                       && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                       && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima
                                                       select a).Count();

                    var kuotaLulusObservasiSmaIps_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                       join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                       join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                       join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                       join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                       join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                       where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                       && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPS
                                                       && g.IdUnit == UnitConstant.Sma
                                                       && a.KdJenisKelamin == "P"
                                                       && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                       && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima
                                                       select a).Count();

                    var kuotaLulusObservasiSmaIps_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                       join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                       join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                       join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                       join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                       join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                       where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                       && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPS
                                                       && g.IdUnit == UnitConstant.Sma
                                                       && a.KdJenisKelamin == "L"
                                                       && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                       && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima
                                                       select a).Count();
                    #endregion

                    #region KUOTA TIDAK LULUS OBSERVASI
                    var strWhereIn = string.Format("where status_proses in ({0},{1}) and status in ({2})", StatusPpdbVerifikasiObservasiConstant.TidakDiterima, StatusPpdbVerifikasiObservasiConstant.UndurDiri, StatusPpdbConstant.VerifikasiObservasi);
                    var strWhereNotExists = string.Format("where status_proses in ({0},{1})", StatusPpdbVerifikasiObservasiConstant.Diterima, StatusPpdbVerifikasiObservasiConstant.Ditangguhkan);
                    var kuotaTidakLulusObservasiTkA_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                         join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                         join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                         join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                         join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                         where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                         && c.IdKelas == 104 // Tk-A
                                                         && a.KdJenisKelamin == "P"
                                                         && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                         select a).Count();

                    var kuotaTidakLulusObservasiTkA_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                         join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                         join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                         join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                         join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                         where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                         && c.IdKelas == 104 // Tk-A
                                                         && a.KdJenisKelamin == "L"
                                                         && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                         select a).Count();

                    var kuotaTidakLulusObservasiTkB_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                         join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                         join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                         join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                         join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                         where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                         && c.IdKelas == 105 // Tk-B
                                                         && a.KdJenisKelamin == "P"
                                                         && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                         select a).Count();

                    var kuotaTidakLulusObservasiTkB_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                         join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                         join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                         join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                         join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                         where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                         && c.IdKelas == 105 // Tk-B
                                                         && a.KdJenisKelamin == "L"
                                                         && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                         select a).Count();

                    var kuotaTidakLulusObservasiSd_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                        join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                        join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                        join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                        join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                        join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                        where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                        && g.IdUnit == UnitConstant.Sd
                                                        && a.KdJenisKelamin == "P"
                                                        && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                        select a).Count();

                    var kuotaTidakLulusObservasiSd_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                        join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                        join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                        join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                        join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                        join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                        where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                        && g.IdUnit == UnitConstant.Sd
                                                        && a.KdJenisKelamin == "L"
                                                        && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                        select a).Count();

                    var kuotaTidakLulusObservasiSmp_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                         join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                         join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                         join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                         join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                         join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                         where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                         && g.IdUnit == UnitConstant.Smp
                                                         && a.KdJenisKelamin == "P"
                                                         //  && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                         select a).Count();

                    var kuotaTidakLulusObservasiSmp_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                         join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                         join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                         join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                         join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                         join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                         where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                         && g.IdUnit == UnitConstant.Smp
                                                         && a.KdJenisKelamin == "L"
                                                         //  && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                         select a).Count();

                    var kuotaTidakLulusObservasiSmaIpa_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                            join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                            join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                            join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                            join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                            join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                            where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                            && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPA
                                                            && g.IdUnit == UnitConstant.Sma
                                                            && a.KdJenisKelamin == "P"
                                                            && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                            select a).Count();

                    var kuotaTidakLulusObservasiSmaIpa_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                            join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                            join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                            join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                            join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                            join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                            where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                            && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPA
                                                            && g.IdUnit == UnitConstant.Sma
                                                            && a.KdJenisKelamin == "L"
                                                            && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                            select a).Count();

                    var kuotaTidakLulusObservasiSmaIps_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                            join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                            join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                            join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                            join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                            join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                            where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                            && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPS
                                                            && g.IdUnit == UnitConstant.Sma
                                                            && a.KdJenisKelamin == "P"
                                                            && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                            select a).Count();

                    var kuotaTidakLulusObservasiSmaIps_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                            join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                            join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                            join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                            join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                            join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                            where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                            && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPS
                                                            && g.IdUnit == UnitConstant.Sma
                                                            && a.KdJenisKelamin == "L"
                                                            && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                            select a).Count();

                    #endregion

                    #region KUOTA CADANGAN OBSERVASI
                    var kuotaCadanganObservasiTkA_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                       join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                       join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                       join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                       join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                       where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                       && c.IdKelas == 104 // Tk-A
                                                       && a.KdJenisKelamin == "P"
                                                       && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                       && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan
                                                       select a).Count();

                    var kuotaCadanganObservasiTkA_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                       join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                       join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                       join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                       join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                       where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                       && c.IdKelas == 104 // Tk-A
                                                       && a.KdJenisKelamin == "L"
                                                       && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                       && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan
                                                       select a).Count();

                    var kuotaCadanganObservasiTkB_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                       join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                       join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                       join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                       join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                       where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                       && c.IdKelas == 105 // Tk-B
                                                       && a.KdJenisKelamin == "P"
                                                       && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                       && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan
                                                       select a).Count();

                    var kuotaCadanganObservasiTkB_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                       join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                       join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                       join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                       join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                       where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                       && c.IdKelas == 105 // Tk-B
                                                       && a.KdJenisKelamin == "L"
                                                       && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                       && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan
                                                       select a).Count();

                    var kuotaCadanganObservasiSd_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                      join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                      join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                      join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                      join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                      join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                      where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                      && g.IdUnit == UnitConstant.Sd
                                                      && a.KdJenisKelamin == "P"
                                                      && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                      && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan
                                                      select a).Count();

                    var kuotaCadanganObservasiSd_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                      join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                      join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                      join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                      join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                      join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                      where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                      && g.IdUnit == UnitConstant.Sd
                                                      && a.KdJenisKelamin == "L"
                                                      && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                      && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan
                                                      select a).Count();

                    var kuotaCadanganObservasiSmp_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                       join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                       join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                       join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                       join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                       join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                       where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                       && g.IdUnit == UnitConstant.Smp
                                                       && a.KdJenisKelamin == "P"
                                                       && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                       && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan
                                                       select a).Count();

                    var kuotaCadanganObservasiSmp_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                       join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                       join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                       join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                       join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                       join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                       where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                       && g.IdUnit == UnitConstant.Smp
                                                       && a.KdJenisKelamin == "L"
                                                       && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                       && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan
                                                       select a).Count();

                    var kuotaCadanganObservasiSmaIpa_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                          join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                          join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                          join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                          join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                          join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                          where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                           && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPA
                                                          && g.IdUnit == UnitConstant.Smp
                                                          && a.KdJenisKelamin == "P"
                                                          && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                          && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan
                                                          select a).Count();

                    var kuotaCadanganObservasiSmaIpa_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                          join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                          join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                          join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                          join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                          join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                          where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                           && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPA
                                                          && g.IdUnit == UnitConstant.Smp
                                                          && a.KdJenisKelamin == "L"
                                                          && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                          && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan
                                                          select a).Count();

                    var kuotaCadanganObservasiSmaIps_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                          join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                          join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                          join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                          join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                          join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                          where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                           && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPS
                                                          && g.IdUnit == UnitConstant.Smp
                                                          && a.KdJenisKelamin == "P"
                                                          && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                          && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan
                                                          select a).Count();

                    var kuotaCadanganObservasiSmaIps_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                          join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                          join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                          join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                          join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                          join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                          where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                           && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPS
                                                          && g.IdUnit == UnitConstant.Smp
                                                          && a.KdJenisKelamin == "L"
                                                          && f.Status == StatusPpdbConstant.VerifikasiObservasi
                                                          && f.StatusProses == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan
                                                          select a).Count();
                    #endregion

                    #region KUOTA BIAYA PENDIDIKAN
                    strWhereIn = string.Format("where status in ({0}) and status_proses in ({1})", StatusPpdbConstant.VerifikasiObservasi, StatusPpdbVerifikasiObservasiConstant.Diterima);
                    strWhereNotExists = string.Format("where status in ({0})", StatusPpdbConstant.Pengesahan);
                    var kuotaBiayaPendidikanTkA_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                     join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                     join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                     join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                     join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                     where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                     && c.IdKelas == 104 // Tk-A
                                                     && a.KdJenisKelamin == "P"
                                                     && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                     select a).Count();

                    var kuotaBiayaPendidikanTkA_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                     join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                     join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                     join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                     join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                     where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                     && c.IdKelas == 104 // Tk-A
                                                     && a.KdJenisKelamin == "L"
                                                     && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                     select a).Count();

                    var kuotaBiayaPendidikanTkB_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                     join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                     join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                     join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                     join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                     where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                     && c.IdKelas == 105 // Tk-B
                                                     && a.KdJenisKelamin == "P"
                                                     && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                     select a).Count();

                    var kuotaBiayaPendidikanTkB_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                     join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                     join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                     join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                     join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                     where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                     && c.IdKelas == 105 // Tk-B
                                                     && a.KdJenisKelamin == "L"
                                                     && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                     select a).Count();

                    var kuotaBiayaPendidikanSd_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                    join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                    join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                    join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                    join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                    join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                    where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                    && g.IdUnit == UnitConstant.Sd
                                                    && a.KdJenisKelamin == "P"
                                                    && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                    select a).Count();

                    var kuotaBiayaPendidikanSd_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                    join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                    join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                    join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                    join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                    join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                    where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                    && g.IdUnit == UnitConstant.Sd
                                                    && a.KdJenisKelamin == "L"
                                                    && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                    select a).Count();

                    var kuotaBiayaPendidikanSmp_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                     join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                     join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                     join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                     join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                     join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                     where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                     && g.IdUnit == UnitConstant.Smp
                                                     && a.KdJenisKelamin == "P"
                                                     && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                     select a).Count();

                    var kuotaBiayaPendidikanSmp_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                     join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                     join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                     join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                     join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                     join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                     where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                     && g.IdUnit == UnitConstant.Smp
                                                     && a.KdJenisKelamin == "L"
                                                     && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                     select a).Count();

                    var kuotaBiayaPendidikanSmaIpa_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                        join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                        join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                        join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                        join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                        join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                        where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                        && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPA
                                                        && g.IdUnit == UnitConstant.Sma
                                                        && a.KdJenisKelamin == "P"
                                                        && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                        select a).Count();

                    var kuotaBiayaPendidikanSmaIpa_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                        join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                        join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                        join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                        join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                        join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                        where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                        && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPA
                                                        && g.IdUnit == UnitConstant.Sma
                                                        && a.KdJenisKelamin == "L"
                                                        && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                        select a).Count();

                    var kuotaBiayaPendidikanSmaIps_P = (from a in conn.GetList<TbPpdbSiswa>()
                                                        join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                        join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                        join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                        join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                        join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                        where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                        && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPS
                                                        && g.IdUnit == UnitConstant.Sma
                                                        && a.KdJenisKelamin == "P"
                                                        && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                        select a).Count();

                    var kuotaBiayaPendidikanSmaIps_L = (from a in conn.GetList<TbPpdbSiswa>()
                                                        join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                                        join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                                        join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                                                        join f in conn.GetList<TbPpdbSiswaStatus>(strWhereIn) on a.IdPpdbDaftar equals f.IdPpdbDaftar
                                                        join g in conn.GetList<TbUnit>() on c.IdUnit equals g.IdUnit
                                                        where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                                        && b.IdJenisPeminatan == JenisPeminatanPpdbConstant.IPS
                                                        && g.IdUnit == UnitConstant.Sma
                                                        && a.KdJenisKelamin == "L"
                                                        && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                                                        select a).Count();
                    #endregion

                    // var kuotaBiayaPendidikanTkA_P = (from a in conn.GetList<TbPpdbSiswa>()
                    //                                  join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                    //                                  join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                    //                                  join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                    //                                  join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                    //                                  where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                    //                                  && c.IdKelas == 104 // Tk-A
                    //                                  && a.KdJenisKelamin == "P"
                    //                                  && f.Status == StatusPpdbConstant.PelunasanBiayaPendidikan
                    //                                  && f.StatusProses == StatusLunasConstant.BelumLunas
                    //                                  select a).Count();

                    // var kuotaBiayaPendidikanTkA_L = (from a in conn.GetList<TbPpdbSiswa>()
                    //                                  join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                    //                                  join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                    //                                  join d in conn.GetList<TbPpdbDaftar>() on a.IdPpdbDaftar equals d.IdPpdbDaftar
                    //                                  join f in conn.GetList<TbPpdbSiswaStatus>() on a.IdPpdbDaftar equals f.IdPpdbDaftar
                    //                                  where (d.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && d.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                    //                                  && c.IdKelas == 104 // Tk-A
                    //                                  && a.KdJenisKelamin == "L"
                    //                                  && f.Status == StatusPpdbConstant.PelunasanBiayaPendidikan
                    //                                  && f.StatusProses == StatusLunasConstant.BelumLunas
                    //                                  select a).Count();

                    ret.StatikTotalPpdb = new SheetLaporanStatikGelombangPpdbModel
                    {
                        KuotaPpdbTkA = new SheetLaporanJkPpdbModel
                        {
                            TotL = Data.KuotaPpdbTkA.TotL,
                            TotP = Data.KuotaPpdbTkA.TotP,
                        },
                        KuotaPpdbTkB = new SheetLaporanJkPpdbModel
                        {
                            TotL = Data.KuotaPpdbTkB.TotL,
                            TotP = Data.KuotaPpdbTkB.TotP,
                        },
                        KuotaPpdbSd = new SheetLaporanJkPpdbModel
                        {
                            TotL = Data.KuotaPpdbSd.TotL,
                            TotP = Data.KuotaPpdbSd.TotP,
                        },
                        KuotaPpdbSmp = new SheetLaporanJkPpdbModel
                        {
                            TotL = Data.KuotaPpdbSmp.TotL,
                            TotP = Data.KuotaPpdbSmp.TotP,
                        },
                        KuotaPpdbSmaIpa = new SheetLaporanJkPpdbModel
                        {
                            TotL = Data.KuotaPpdbSmaIpa.TotL,
                            TotP = Data.KuotaPpdbSmaIpa.TotP,
                        },
                        KuotaPpdbSmaIps = new SheetLaporanJkPpdbModel
                        {
                            TotL = Data.KuotaPpdbSmaIps.TotL,
                            TotP = Data.KuotaPpdbSmaIps.TotP,
                        },
                        KuotaDaftarTkA = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaDaftarTkA_L,
                            TotP = kuotaDaftarTkA_P,
                        },
                        KuotaDaftarTkB = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaDaftarTkB_L,
                            TotP = kuotaDaftarTkB_P,
                        },
                        KuotaDaftarSd = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaDaftarSd_L,
                            TotP = kuotaDaftarSd_P,
                        },
                        KuotaDaftarSmp = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaDaftarSmp_L,
                            TotP = kuotaDaftarSmp_P,
                        },
                        KuotaDaftarSmaIpa = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaDaftarSmaIpa_L,
                            TotP = kuotaDaftarSmaIpa_P,
                        },
                        KuotaDaftarSmaIps = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaDaftarSmaIps_L,
                            TotP = kuotaDaftarSmaIps_P,
                        },
                        KuotaDaftarTungguTkA = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaDaftarTungguTkA_L,
                            TotP = kuotaDaftarTungguTkA_P,
                        },
                        KuotaDaftarTungguTkB = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaDaftarTungguTkB_L,
                            TotP = kuotaDaftarTungguTkB_P,
                        },
                        KuotaDaftarTungguSd = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaDaftarTungguSd_L,
                            TotP = kuotaDaftarTungguSd_P,
                        },
                        KuotaDaftarTungguSmp = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaDaftarTungguSmp_L,
                            TotP = kuotaDaftarTungguSmp_P,
                        },
                        KuotaDaftarTungguSmaIpa = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaDaftarTungguSmaIpa_L,
                            TotP = kuotaDaftarTungguSmaIpa_P,
                        },
                        KuotaDaftarTungguSmaIps = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaDaftarTungguSmaIps_L,
                            TotP = kuotaDaftarTungguSmaIps_P,
                        },

                        KuotaLulusObservasiTkA = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaLulusObservasiTkA_L,
                            TotP = kuotaLulusObservasiTkA_P,
                        },
                        KuotaLulusObservasiTkB = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaLulusObservasiTkB_L,
                            TotP = kuotaLulusObservasiTkB_P,
                        },
                        KuotaLulusObservasiSd = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaLulusObservasiSd_L,
                            TotP = kuotaLulusObservasiSd_P,
                        },
                        KuotaLulusObservasiSmp = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaLulusObservasiSmp_L,
                            TotP = kuotaLulusObservasiSmp_P,
                        },
                        KuotaLulusObservasiSmaIpa = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaLulusObservasiSmaIpa_L,
                            TotP = kuotaLulusObservasiSmaIpa_P,
                        },
                        KuotaLulusObservasiSmaIps = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaLulusObservasiSmaIps_L,
                            TotP = kuotaLulusObservasiSmaIps_P,
                        },

                        KuotaTidakLulusObservasiTkA = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaTidakLulusObservasiTkA_L,
                            TotP = kuotaTidakLulusObservasiTkA_P,
                        },
                        KuotaTidakLulusObservasiTkB = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaTidakLulusObservasiTkB_L,
                            TotP = kuotaTidakLulusObservasiTkB_P,
                        },
                        KuotaTidakLulusObservasiSd = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaTidakLulusObservasiSd_L,
                            TotP = kuotaTidakLulusObservasiSd_P,
                        },
                        KuotaTidakLulusObservasiSmp = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaTidakLulusObservasiSmp_L,
                            TotP = kuotaTidakLulusObservasiSmp_P,
                        },
                        KuotaTidakLulusObservasiSmaIpa = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaTidakLulusObservasiSmaIpa_L,
                            TotP = kuotaTidakLulusObservasiSmaIpa_P,
                        },
                        KuotaTidakLulusObservasiSmaIps = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaTidakLulusObservasiSmaIps_L,
                            TotP = kuotaTidakLulusObservasiSmaIps_P,
                        },

                        KuotaCadanganObservasiTkA = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaCadanganObservasiTkA_L,
                            TotP = kuotaCadanganObservasiTkA_P,
                        },
                        KuotaCadanganObservasiTkB = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaCadanganObservasiTkB_L,
                            TotP = kuotaCadanganObservasiTkB_P,
                        },
                        KuotaCadanganObservasiSd = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaCadanganObservasiSd_L,
                            TotP = kuotaCadanganObservasiSd_P,
                        },
                        KuotaCadanganObservasiSmp = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaCadanganObservasiSmp_L,
                            TotP = kuotaCadanganObservasiSmp_P,
                        },
                        KuotaCadanganObservasiSmaIpa = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaCadanganObservasiSmaIpa_L,
                            TotP = kuotaCadanganObservasiSmaIpa_P,
                        },
                        KuotaCadanganObservasiSmaIps = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaCadanganObservasiSmaIps_L,
                            TotP = kuotaCadanganObservasiSmaIps_P,
                        },

                        KuotaBiayaPendidikanTkA = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaBiayaPendidikanTkA_L,
                            TotP = kuotaBiayaPendidikanTkA_P,
                        },
                        KuotaBiayaPendidikanTkB = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaBiayaPendidikanTkB_L,
                            TotP = kuotaBiayaPendidikanTkB_P,
                        },
                        KuotaBiayaPendidikanSd = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaBiayaPendidikanSd_L,
                            TotP = kuotaBiayaPendidikanSd_P,
                        },
                        KuotaBiayaPendidikanSmp = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaBiayaPendidikanSmp_L,
                            TotP = kuotaBiayaPendidikanSmp_P,
                        },
                        KuotaBiayaPendidikanSmaIpa = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaBiayaPendidikanSmaIpa_L,
                            TotP = kuotaBiayaPendidikanSmaIpa_P,
                        },
                        KuotaBiayaPendidikanSmaIps = new SheetLaporanJkPpdbModel
                        {
                            TotL = kuotaBiayaPendidikanSmaIps_L,
                            TotP = kuotaBiayaPendidikanSmaIps_P,
                        },

                    };


                    var dataTk = (from a in conn.GetList<TbPpdbDaftar>()
                                  join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                  join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                  join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                  join f in conn.GetList<TbPpdbSiswa>() on a.IdPpdbDaftar equals f.IdPpdbDaftar into f1
                                  from f in f1.DefaultIfEmpty()
                                  where d.IdUnit == UnitConstant.Tk
                                  && (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                  select new { a, b, c, d, f }).ToList();
                    ret.LapSiswaTk = new List<SheetLaporanSiswaGelombangPpdbModel>();
                    foreach (var item in dataTk.OrderBy(x => x.c.IdUnit).ThenBy(x => x.b.IdKelas))
                    {
                        var tbStatusPpdb = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                            where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                            select new
                                            {
                                                a.Status,
                                                a.StatusProses,
                                                maxStatus = (a.Status < 10 ? (a.Status * 10) : a.Status)
                                            }).OrderByDescending(x => x.maxStatus).FirstOrDefault();

                        SheetLaporanSiswaGelombangPpdbModel m = new SheetLaporanSiswaGelombangPpdbModel();
                        m.IdPpdbDaftar = item.a.IdPpdbDaftar.ToString();
                        m.JenjangPendidikan = UnitConstant.DictUnit[item.d.IdUnit].ToUpper() + " " + item.c.Nama.ToUpper();
                        m.JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[item.a.IdJalurPendaftaran].ToUpper();
                        m.Nis = item.a.NisSiswa;
                        m.Nama = item.a.Nama.ToUpper();
                        m.JenisKelamin = item.a.KdJenisKelamin;
                        m.TempatLahir = item.a.TempatLahir.ToUpper();
                        m.TanggalLahir = item.a.TanggalLahir.ToString("dd-MM-yyyy");

                        if (item.f != null)
                            m.Alamat = item.f.AlamatTinggal.ToUpper();

                        // m.NamaPewakaf = item.a.AlamatTinggal;
                        // m.HubunganSiswa = item.a.AlamatTinggal;

                        if (tbStatusPpdb != null)
                        {
                            if (tbStatusPpdb.Status == StatusPpdbConstant.TestObservasi)
                            {
                                m.StatusPpdb = StatusPpdbTestObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                            }
                            else if (tbStatusPpdb.Status == StatusPpdbConstant.Wawancara)
                            {
                                m.StatusPpdb = StatusPpdbWawancaraConstant.Dict[tbStatusPpdb.StatusProses];
                            }
                            else if (tbStatusPpdb.Status == StatusPpdbConstant.TestObservasiWawancara)
                            {
                                m.StatusPpdb = StatusPpdbTestObservasiWawancaraConstant.Dict[tbStatusPpdb.StatusProses];
                            }
                            else if (tbStatusPpdb.Status == StatusPpdbConstant.InputObservasi)
                            {
                                m.StatusPpdb = StatusPpdbHasilObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                            }
                            else if (tbStatusPpdb.Status == StatusPpdbConstant.VerifikasiObservasi)
                            {
                                m.StatusPpdb = StatusPpdbVerifikasiObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                                // if (tbStatusPpdb.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima)
                                // {
                                //     m.StatusPpdb = "Menunggu Diproses.";
                                // }
                            }
                            else
                            {
                                m.StatusPpdb = "Menunggu Diproses";
                            }

                            m.StatusProses = StatusPpdbConstant.Dict[tbStatusPpdb.Status];
                            // if (tbStatusPpdb.Status == StatusPpdbConstant.VerifikasiObservasi)
                            //     m.StatusProses = StatusPpdbConstant.Dict[StatusPpdbConstant.PelunasanBiayaPendidikan];
                        }


                        var ortus = from a in conn.GetList<TbPpdbSiswaOrtu>()
                                    join b in conn.GetList<TbJenisJenjangPendidikan>() on a.IdJenisJenjangPendidikan equals b.IdJenisJenjangPendidikan
                                    join c in conn.GetList<TbJenisPekerjaan>() on a.IdJenisPekerjaan equals c.IdJenisPekerjaan
                                    where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                    select new { a, b, c };
                        foreach (var ortu in ortus)
                        {
                            if (ortu.a.IdTipe == 2)//ayah
                            {
                                m.NamaAyah = ortu.a.Nama.ToUpper();
                                m.EmailAyah = ortu.a.Email.ToUpper();
                                m.NoHpAyah = ortu.a.NoHandphone;
                            }
                            else if (ortu.a.IdTipe == 1)//ibu
                            {
                                m.NamaIbu = ortu.a.Nama.ToUpper();
                                m.EmailIbu = ortu.a.Email.ToUpper();
                                m.NoHpIbu = ortu.a.NoHandphone;
                            }
                        }

                        ret.LapSiswaTk.Add(m);
                    }

                    var dataSd = (from a in conn.GetList<TbPpdbDaftar>()
                                  join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                  join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                  join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                  join f in conn.GetList<TbPpdbSiswa>() on a.IdPpdbDaftar equals f.IdPpdbDaftar into f1
                                  from f in f1.DefaultIfEmpty()
                                  where d.IdUnit == UnitConstant.Sd
                                  && (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                  select new { a, b, c, d, f }).ToList();
                    ret.LapSiswaSd = new List<SheetLaporanSiswaGelombangPpdbModel>();
                    foreach (var item in dataSd.OrderBy(x => x.c.IdUnit).ThenBy(x => x.b.IdKelas))
                    {
                        var tbStatusPpdb = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                            where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                            select new
                                            {
                                                a.Status,
                                                a.StatusProses,
                                                maxStatus = (a.Status < 10 ? (a.Status * 10) : a.Status)
                                            }).OrderByDescending(x => x.maxStatus).FirstOrDefault();

                        SheetLaporanSiswaGelombangPpdbModel m = new SheetLaporanSiswaGelombangPpdbModel();
                        m.IdPpdbDaftar = item.a.IdPpdbDaftar.ToString();
                        m.JenjangPendidikan = UnitConstant.DictUnit[item.d.IdUnit].ToUpper() + " " + item.c.Nama.ToUpper();
                        m.JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[item.a.IdJalurPendaftaran].ToUpper();
                        m.Nis = item.a.NisSiswa;
                        m.Nama = item.a.Nama.ToUpper();
                        m.JenisKelamin = item.a.KdJenisKelamin;
                        m.TempatLahir = item.a.TempatLahir.ToUpper();
                        m.TanggalLahir = item.a.TanggalLahir.ToString("dd-MM-yyyy");

                        if (item.f != null)
                            m.Alamat = item.f.AlamatTinggal.ToUpper();

                        // m.NamaPewakaf = item.a.AlamatTinggal;
                        // m.HubunganSiswa = item.a.AlamatTinggal;

                        if (tbStatusPpdb != null)
                        {
                            if (tbStatusPpdb.Status == StatusPpdbConstant.TestObservasi)
                            {
                                m.StatusPpdb = StatusPpdbTestObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                            }
                            else if (tbStatusPpdb.Status == StatusPpdbConstant.Wawancara)
                            {
                                m.StatusPpdb = StatusPpdbWawancaraConstant.Dict[tbStatusPpdb.StatusProses];
                            }
                            else if (tbStatusPpdb.Status == StatusPpdbConstant.TestObservasiWawancara)
                            {
                                m.StatusPpdb = StatusPpdbTestObservasiWawancaraConstant.Dict[tbStatusPpdb.StatusProses];
                            }
                            else if (tbStatusPpdb.Status == StatusPpdbConstant.InputObservasi)
                            {
                                m.StatusPpdb = StatusPpdbHasilObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                            }
                            else if (tbStatusPpdb.Status == StatusPpdbConstant.VerifikasiObservasi)
                            {
                                m.StatusPpdb = StatusPpdbVerifikasiObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                                // if (tbStatusPpdb.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima)
                                // {
                                //     m.StatusPpdb = "Menunggu Diproses.";
                                // }
                            }
                            else
                            {
                                m.StatusPpdb = "Menunggu Diproses";
                            }

                            m.StatusProses = StatusPpdbConstant.Dict[tbStatusPpdb.Status];
                            // if (tbStatusPpdb.Status == StatusPpdbConstant.VerifikasiObservasi)
                            //     m.StatusProses = StatusPpdbConstant.Dict[StatusPpdbConstant.PelunasanBiayaPendidikan];
                        }


                        var ortus = from a in conn.GetList<TbPpdbSiswaOrtu>()
                                    join b in conn.GetList<TbJenisJenjangPendidikan>() on a.IdJenisJenjangPendidikan equals b.IdJenisJenjangPendidikan
                                    join c in conn.GetList<TbJenisPekerjaan>() on a.IdJenisPekerjaan equals c.IdJenisPekerjaan
                                    where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                    select new { a, b, c };
                        foreach (var ortu in ortus)
                        {
                            if (ortu.a.IdTipe == 2)//ayah
                            {
                                m.NamaAyah = ortu.a.Nama.ToUpper();
                                m.EmailAyah = ortu.a.Email.ToUpper();
                                m.NoHpAyah = ortu.a.NoHandphone;
                            }
                            else if (ortu.a.IdTipe == 1)//ibu
                            {
                                m.NamaIbu = ortu.a.Nama.ToUpper();
                                m.EmailIbu = ortu.a.Email.ToUpper();
                                m.NoHpIbu = ortu.a.NoHandphone;
                            }
                        }

                        ret.LapSiswaSd.Add(m);
                    }

                    var dataSmp = (from a in conn.GetList<TbPpdbDaftar>()
                                   join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                   join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                   join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                   join f in conn.GetList<TbPpdbSiswa>() on a.IdPpdbDaftar equals f.IdPpdbDaftar into f1
                                   from f in f1.DefaultIfEmpty()
                                   where d.IdUnit == UnitConstant.Smp
                                   && (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                   select new { a, b, c, d, f }).ToList();
                    ret.LapSiswaSmp = new List<SheetLaporanSiswaGelombangPpdbModel>();
                    foreach (var item in dataSmp.OrderBy(x => x.c.IdUnit).ThenBy(x => x.b.IdKelas))
                    {
                        var tbStatusPpdb = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                            where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                            select new
                                            {
                                                a.Status,
                                                a.StatusProses,
                                                maxStatus = (a.Status < 10 ? (a.Status * 10) : a.Status)
                                            }).OrderByDescending(x => x.maxStatus).FirstOrDefault();

                        SheetLaporanSiswaGelombangPpdbModel m = new SheetLaporanSiswaGelombangPpdbModel();
                        m.IdPpdbDaftar = item.a.IdPpdbDaftar.ToString();
                        m.JenjangPendidikan = UnitConstant.DictUnit[item.d.IdUnit].ToUpper() + " " + item.c.Nama.ToUpper();
                        m.JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[item.a.IdJalurPendaftaran].ToUpper();
                        m.Nis = item.a.NisSiswa;
                        m.Nama = item.a.Nama.ToUpper();
                        m.JenisKelamin = item.a.KdJenisKelamin;
                        m.TempatLahir = item.a.TempatLahir.ToUpper();
                        m.TanggalLahir = item.a.TanggalLahir.ToString("dd-MM-yyyy");

                        if (item.f != null)
                            m.Alamat = item.f.AlamatTinggal.ToUpper();

                        // m.NamaPewakaf = item.a.AlamatTinggal;
                        // m.HubunganSiswa = item.a.AlamatTinggal;

                        if (tbStatusPpdb != null)
                        {
                            if (tbStatusPpdb.Status == StatusPpdbConstant.TestObservasi)
                            {
                                m.StatusPpdb = StatusPpdbTestObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                            }
                            else if (tbStatusPpdb.Status == StatusPpdbConstant.Wawancara)
                            {
                                m.StatusPpdb = StatusPpdbWawancaraConstant.Dict[tbStatusPpdb.StatusProses];
                            }
                            else if (tbStatusPpdb.Status == StatusPpdbConstant.TestObservasiWawancara)
                            {
                                m.StatusPpdb = StatusPpdbTestObservasiWawancaraConstant.Dict[tbStatusPpdb.StatusProses];
                            }
                            else if (tbStatusPpdb.Status == StatusPpdbConstant.InputObservasi)
                            {
                                m.StatusPpdb = StatusPpdbHasilObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                            }
                            else if (tbStatusPpdb.Status == StatusPpdbConstant.VerifikasiObservasi)
                            {
                                m.StatusPpdb = StatusPpdbVerifikasiObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                                // if (tbStatusPpdb.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima)
                                // {
                                //     m.StatusPpdb = "Menunggu Diproses.";
                                // }
                            }
                            else
                            {
                                m.StatusPpdb = "Menunggu Diproses";
                            }

                            m.StatusProses = StatusPpdbConstant.Dict[tbStatusPpdb.Status];
                            // if (tbStatusPpdb.Status == StatusPpdbConstant.VerifikasiObservasi)
                            //     m.StatusProses = StatusPpdbConstant.Dict[StatusPpdbConstant.PelunasanBiayaPendidikan];
                        }


                        var ortus = from a in conn.GetList<TbPpdbSiswaOrtu>()
                                    join b in conn.GetList<TbJenisJenjangPendidikan>() on a.IdJenisJenjangPendidikan equals b.IdJenisJenjangPendidikan
                                    join c in conn.GetList<TbJenisPekerjaan>() on a.IdJenisPekerjaan equals c.IdJenisPekerjaan
                                    where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                    select new { a, b, c };
                        foreach (var ortu in ortus)
                        {
                            if (ortu.a.IdTipe == 2)//ayah
                            {
                                m.NamaAyah = ortu.a.Nama.ToUpper();
                                m.EmailAyah = ortu.a.Email.ToUpper();
                                m.NoHpAyah = ortu.a.NoHandphone;
                            }
                            else if (ortu.a.IdTipe == 1)//ibu
                            {
                                m.NamaIbu = ortu.a.Nama.ToUpper();
                                m.EmailIbu = ortu.a.Email.ToUpper();
                                m.NoHpIbu = ortu.a.NoHandphone;
                            }
                        }

                        ret.LapSiswaSmp.Add(m);
                    }

                    var dataSma = (from a in conn.GetList<TbPpdbDaftar>()
                                   join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                   join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                   join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                   join f in conn.GetList<TbPpdbSiswa>() on a.IdPpdbDaftar equals f.IdPpdbDaftar into f1
                                   from f in f1.DefaultIfEmpty()
                                   where d.IdUnit == UnitConstant.Sma
                                   && (a.CreatedDate >= dateTimeService.StrToDateTime(TanggalAwal) && a.CreatedDate <= dateTimeService.StrToDateTime(TanggalAkhir))
                                   select new { a, b, c, d, f }).ToList();
                    ret.LapSiswaSma = new List<SheetLaporanSiswaGelombangPpdbModel>();
                    foreach (var item in dataSma.OrderBy(x => x.c.IdUnit).ThenBy(x => x.b.IdKelas))
                    {
                        var tbStatusPpdb = (from a in conn.GetList<TbPpdbSiswaStatus>()
                                            where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                            select new
                                            {
                                                a.Status,
                                                a.StatusProses,
                                                maxStatus = (a.Status < 10 ? (a.Status * 10) : a.Status)
                                            }).OrderByDescending(x => x.maxStatus).FirstOrDefault();

                        SheetLaporanSiswaGelombangPpdbModel m = new SheetLaporanSiswaGelombangPpdbModel();
                        m.IdPpdbDaftar = item.a.IdPpdbDaftar.ToString();
                        m.JenjangPendidikan = UnitConstant.DictUnit[item.d.IdUnit].ToUpper() + " " + item.c.Nama.ToUpper();
                        m.JurusanPendidikan = JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb[(int)item.b.IdJenisPeminatan];
                        m.JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[item.a.IdJalurPendaftaran].ToUpper();
                        m.Nis = item.a.NisSiswa;
                        m.Nama = item.a.Nama.ToUpper();
                        m.JenisKelamin = item.a.KdJenisKelamin;
                        m.TempatLahir = item.a.TempatLahir.ToUpper();
                        m.TanggalLahir = item.a.TanggalLahir.ToString("dd-MM-yyyy");

                        if (item.f != null)
                            m.Alamat = item.f.AlamatTinggal.ToUpper();

                        // m.NamaPewakaf = item.a.AlamatTinggal;
                        // m.HubunganSiswa = item.a.AlamatTinggal;

                        if (tbStatusPpdb != null)
                        {
                            if (tbStatusPpdb.Status == StatusPpdbConstant.TestObservasi)
                            {
                                m.StatusPpdb = StatusPpdbTestObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                            }
                            else if (tbStatusPpdb.Status == StatusPpdbConstant.Wawancara)
                            {
                                m.StatusPpdb = StatusPpdbWawancaraConstant.Dict[tbStatusPpdb.StatusProses];
                            }
                            else if (tbStatusPpdb.Status == StatusPpdbConstant.TestObservasiWawancara)
                            {
                                m.StatusPpdb = StatusPpdbTestObservasiWawancaraConstant.Dict[tbStatusPpdb.StatusProses];
                            }
                            else if (tbStatusPpdb.Status == StatusPpdbConstant.InputObservasi)
                            {
                                m.StatusPpdb = StatusPpdbHasilObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                            }
                            else if (tbStatusPpdb.Status == StatusPpdbConstant.VerifikasiObservasi)
                            {
                                m.StatusPpdb = StatusPpdbVerifikasiObservasiConstant.Dict[tbStatusPpdb.StatusProses];
                                // if (tbStatusPpdb.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima)
                                // {
                                //     m.StatusPpdb = "Menunggu Diproses.";
                                // }
                            }
                            else
                            {
                                m.StatusPpdb = "Menunggu Diproses";
                            }

                            m.StatusProses = StatusPpdbConstant.Dict[tbStatusPpdb.Status];
                            // if (tbStatusPpdb.Status == StatusPpdbConstant.VerifikasiObservasi)
                            //     m.StatusProses = StatusPpdbConstant.Dict[StatusPpdbConstant.PelunasanBiayaPendidikan];
                        }


                        var ortus = from a in conn.GetList<TbPpdbSiswaOrtu>()
                                    join b in conn.GetList<TbJenisJenjangPendidikan>() on a.IdJenisJenjangPendidikan equals b.IdJenisJenjangPendidikan
                                    join c in conn.GetList<TbJenisPekerjaan>() on a.IdJenisPekerjaan equals c.IdJenisPekerjaan
                                    where a.IdPpdbDaftar == item.a.IdPpdbDaftar
                                    select new { a, b, c };
                        foreach (var ortu in ortus)
                        {
                            if (ortu.a.IdTipe == 2)//ayah
                            {
                                m.NamaAyah = ortu.a.Nama.ToUpper();
                                m.EmailAyah = ortu.a.Email.ToUpper();
                                m.NoHpAyah = ortu.a.NoHandphone;
                            }
                            else if (ortu.a.IdTipe == 1)//ibu
                            {
                                m.NamaIbu = ortu.a.Nama.ToUpper();
                                m.EmailIbu = ortu.a.Email.ToUpper();
                                m.NoHpIbu = ortu.a.NoHandphone;
                            }
                        }

                        ret.LapSiswaSma.Add(m);
                    }

                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetLaporanGelombangPpdb", ex);
                return null;
            }
        }
        private bool IsDaftarTunggu(IDbConnection conn, int IdPpdbDaftar)
        {
            bool cek = true;
            var item = (from a in conn.GetList<TbPpdbDaftar>()
                        where a.IdPpdbDaftar == IdPpdbDaftar
                        & a.Status == -1
                        select a).FirstOrDefault();
            if (item == null) cek = false;
            return cek;
        }

        private int? GetStatusProsesObservasi(IDbConnection conn, int IdPpdbDaftar)
        {
            var item = (from a in conn.GetList<TbPpdbSiswaStatus>()
                        where a.IdPpdbDaftar == IdPpdbDaftar
                        & a.Status == StatusPpdbConstant.InputObservasi
                        select a).FirstOrDefault();
            if (item == null) return null;
            return item.StatusProses;
        }
        private bool IsPpdbSiswa(IDbConnection conn, int IdPpdbDaftar)
        {
            bool cek = true;
            var item = (from a in conn.GetList<TbPpdbSiswaStatus>()
                        where a.IdPpdbDaftar == IdPpdbDaftar
                        select a).FirstOrDefault();
            if (item == null) cek = false;
            return cek;
        }
        private int? GetStatusProsesVerifikasiObservasi(IDbConnection conn, int IdPpdbDaftar)
        {
            var item = (from a in conn.GetList<TbPpdbSiswaStatus>()
                        where a.IdPpdbDaftar == IdPpdbDaftar
                        & a.Status == StatusPpdbConstant.VerifikasiObservasi
                        select a).FirstOrDefault();
            if (item == null) return null;
            return item.StatusProses;
        }
        private bool IsBayarPendidikan(IDbConnection conn, int IdPpdbDaftar)
        {
            bool cek = true;
            var item = (from a in conn.GetList<TbPpdbSiswaStatus>()
                        where a.IdPpdbDaftar == IdPpdbDaftar
                        && a.Status == StatusPpdbConstant.PelunasanBiayaPendidikan
                        && a.StatusProses == StatusLunasConstant.Lunas
                        select a).FirstOrDefault();
            if (item == null) cek = false;
            return cek;
        }
        private bool IsBayarPendidikanSiswa(IDbConnection conn, int IdPpdbDaftar)
        {
            bool cek = true;
            var item = (from a in conn.GetList<TbPpdbBiayaSiswa>()
                        where a.IdPpdbDaftar == IdPpdbDaftar
                        && a.Status == StatusLunasConstant.Lunas
                        select a).FirstOrDefault();
            if (item == null) cek = false;
            return cek;
        }

        private bool IsBayarPendaftaranLunas(IDbConnection conn, int IdPpdbDaftar)
        {
            bool cek = true;
            var item = (from a in conn.GetList<TbPpdbBiayaDaftar>()
                        where a.IdPpdbDaftar == IdPpdbDaftar
                        && a.Status == StatusLunasConstant.Lunas
                        select a).FirstOrDefault();
            if (item == null) cek = false;
            return cek;
        }
        private bool IsBayarPendaftaranBelumLunas(IDbConnection conn, int IdPpdbDaftar)
        {
            bool cek = true;
            var item = (from a in conn.GetList<TbPpdbBiayaDaftar>()
                        where a.IdPpdbDaftar == IdPpdbDaftar
                        && a.Status == StatusLunasConstant.BelumLunas
                        select a).FirstOrDefault();
            if (item == null) cek = false;
            return cek;
        }

        private int IsInputFormulir(IDbConnection conn, int IdPpdbDaftar)
        {
            int stat = 0;
            var item = (from a in conn.GetList<TbPpdbBiayaDaftar>()
                        where a.IdPpdbDaftar == IdPpdbDaftar
                        && a.Status == StatusLunasConstant.Lunas
                        select a).FirstOrDefault();
            if (item == null)
            {
                stat = -1;
                return stat;
            }

            var itemx = (from a in conn.GetList<TbPpdbSiswa>()
                         where a.IdPpdbDaftar == IdPpdbDaftar
                         select a).FirstOrDefault();
            if (itemx == null) stat = 1;

            return stat;
        }

        private bool IsIkutObservasi(IDbConnection conn, int IdPpdbDaftar)
        {
            bool cek = true;
            var item = (from a in conn.GetList<TbPpdbSiswaStatus>()
                        where a.IdPpdbDaftar == IdPpdbDaftar
                        && a.Status == StatusPpdbConstant.InputObservasi
                        select a).FirstOrDefault();
            if (item == null) cek = false;
            return cek;
        }

        private bool IsTidakDirekomendasikanObservasi(IDbConnection conn, int IdPpdbDaftar)
        {
            bool cek = true;
            var item = (from a in conn.GetList<TbPpdbSiswaStatus>()
                        where a.IdPpdbDaftar == IdPpdbDaftar
                        && a.Status == StatusPpdbConstant.InputObservasi
                        && a.StatusProses == StatusPpdbHasilObservasiConstant.TidakDirekomendasikan
                        select a).FirstOrDefault();
            if (item == null) cek = false;
            return cek;
        }

        private bool IsDirekomendasikanObservasi(IDbConnection conn, int IdPpdbDaftar)
        {
            bool cek = true;
            var item = (from a in conn.GetList<TbPpdbSiswaStatus>()
                        where a.IdPpdbDaftar == IdPpdbDaftar
                        && a.Status == StatusPpdbConstant.InputObservasi
                        && a.StatusProses == StatusPpdbHasilObservasiConstant.Direkomendasikan
                        select a).FirstOrDefault();
            if (item == null) cek = false;
            return cek;
        }

        private bool IsVerifHasilObservasiDiterima(IDbConnection conn, int IdPpdbDaftar)
        {
            bool cek = true;
            var item = (from a in conn.GetList<TbPpdbSiswaStatus>()
                        where a.IdPpdbDaftar == IdPpdbDaftar
                        && a.Status == StatusPpdbConstant.VerifikasiObservasi
                        && a.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima
                        select a).FirstOrDefault();
            if (item == null) cek = false;
            return cek;
        }

        private bool IsVerifHasilObservasiTidakDiterima(IDbConnection conn, int IdPpdbDaftar)
        {
            bool cek = true;

            var strWhereIn = string.Format("where status in ({0}) and status_proses in ({1},{2})", StatusPpdbConstant.VerifikasiObservasi, StatusPpdbVerifikasiObservasiConstant.TidakDiterima, StatusPpdbVerifikasiObservasiConstant.UndurDiri);
            var strWhereNotExists = string.Format("where status in ({0}) and status_proses in ({1},{2})", StatusPpdbConstant.VerifikasiObservasi, StatusPpdbVerifikasiObservasiConstant.Diterima, StatusPpdbVerifikasiObservasiConstant.Ditangguhkan);
            var item = (from a in conn.GetList<TbPpdbSiswaStatus>(strWhereIn)
                        where a.IdPpdbDaftar == IdPpdbDaftar
                        && !conn.GetList<TbPpdbSiswaStatus>(strWhereNotExists).Any(x => x.IdPpdbDaftar == a.IdPpdbDaftar)
                        select a).FirstOrDefault();

            // var item = (from a in conn.GetList<TbPpdbSiswaStatus>()
            //             where a.IdPpdbDaftar == IdPpdbDaftar
            //             && a.Status == StatusPpdbConstant.VerifikasiObservasi
            //             && a.StatusProses == StatusPpdbVerifikasiObservasiConstant.TidakDiterima
            //             && a.StatusProses == StatusPpdbVerifikasiObservasiConstant.UndurDiri
            //             select a).FirstOrDefault();
            if (item == null) cek = false;
            return cek;
        }

        private bool IsVerifHasilObservasiCadangan(IDbConnection conn, int IdPpdbDaftar)
        {
            bool cek = true;
            var item = (from a in conn.GetList<TbPpdbSiswaStatus>()
                        where a.IdPpdbDaftar == IdPpdbDaftar
                        && a.Status == StatusPpdbConstant.VerifikasiObservasi
                        && a.StatusProses == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan
                        select a).FirstOrDefault();
            if (item == null) cek = false;
            return cek;
        }

        private int IsTidakBayarPendidikanSiswa(IDbConnection conn, int IdPpdbDaftar)
        {
            int stat = 0;
            var item = (from a in conn.GetList<TbPpdbSiswaStatus>()
                        where a.IdPpdbDaftar == IdPpdbDaftar
                        && a.Status == StatusPpdbConstant.VerifikasiObservasi
                        && a.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima
                        select a).FirstOrDefault();
            if (item == null)
            {
                stat = -1;
                return stat;
            }

            var itemx = (from a in conn.GetList<TbPpdbBiayaSiswa>()
                         where a.IdPpdbDaftar == IdPpdbDaftar
                         && a.Status == StatusLunasConstant.BelumLunas
                         select a).FirstOrDefault();
            if (itemx == null) stat = 1;

            return stat;
        }

        private int IsPengesahan(IDbConnection conn, int IdPpdbDaftar)
        {
            int stat = 0;
            var item = (from a in conn.GetList<TbPpdbSiswaStatus>()
                        where a.IdPpdbDaftar == IdPpdbDaftar
                        && a.Status == StatusPpdbConstant.VerifikasiObservasi
                        && a.StatusProses == StatusPpdbVerifikasiObservasiConstant.Diterima
                        select a).FirstOrDefault();
            if (item == null)
            {
                stat = -1;
                return stat;
            }

            var itemx = (from a in conn.GetList<TbPpdbBiayaSiswa>()
                         where a.IdPpdbDaftar == IdPpdbDaftar
                         && a.Status == StatusLunasConstant.Lunas
                         select a).FirstOrDefault();
            if (itemx == null) stat = 1;

            return stat;
        }

        private PpdbLaporanListModel SetPpdbLaporanList(TbPpdbDaftar daftar, TbKelas kelas, int? IdJenisPeminatan, TbPpdbSiswa siswa, TbPpdbSiswaOrtu ayah, TbPpdbSiswaOrtu ibu, string StatusPpdb, string StatusProses)
        {
            PpdbLaporanListModel ret = new PpdbLaporanListModel();
            ret.IdPpdbDaftar = daftar.IdPpdbDaftar.ToString();
            ret.JenjangPendidikan = UnitConstant.DictUnit[kelas.IdUnit] + " - " + kelas.Nama;
            if (kelas.IdUnit == UnitConstant.Sma)
                ret.JurusanPendidikan = JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb[(int)IdJenisPeminatan];
            ret.JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[daftar.IdJalurPendaftaran].ToUpper();
            ret.Nis = daftar.NisSiswa;
            ret.Nama = daftar.Nama;
            ret.JenisKelamin = daftar.KdJenisKelamin;
            ret.TempatLahir = daftar.TempatLahir;
            ret.TanggalLahir = daftar.TanggalLahir.ToString("dd-MM-yyyy");
            ret.Alamat = siswa == null ? "" : siswa.AlamatTinggal;
            ret.NamaAyah = ayah == null ? "" : ayah.Nama;
            ret.NoHpAyah = ayah == null ? "" : ayah.NoHandphone;
            ret.EmailAyah = ayah == null ? "" : ayah.Email;
            ret.NamaIbu = ibu == null ? "" : ibu.Nama;
            ret.NoHpIbu = ibu == null ? "" : ibu.NoHandphone;
            ret.EmailIbu = ibu == null ? "" : ibu.Email;
            ret.NamaPewakaf = "";
            ret.HubunganSiswa = "";
            ret.StatusProses = StatusProses;
            ret.StatusPpdb = StatusPpdb;
            return ret;
        }

        private PpdbLaporanRekapModel SetLaporanRekap(IDbConnection conn, int IdPpdbDaftar, string KdJenisKelamin, PpdbLaporanRekapModel rekap)
        {
            PpdbLaporanRekapModel ret = rekap;
            bool isPpdbSiswa = IsPpdbSiswa(conn, IdPpdbDaftar);
            bool isDaftarTunggu = IsDaftarTunggu(conn, IdPpdbDaftar);
            bool isBayarPendaftaranLunas = IsBayarPendaftaranLunas(conn, IdPpdbDaftar);
            bool isBayarPendaftaranBelumLunas = IsBayarPendaftaranBelumLunas(conn, IdPpdbDaftar);
            int isInputFormulir = IsInputFormulir(conn, IdPpdbDaftar);
            bool isIkutObservasi = IsIkutObservasi(conn, IdPpdbDaftar);
            bool isTidakDirekomendasikanObservasi = IsTidakDirekomendasikanObservasi(conn, IdPpdbDaftar);
            bool isDirekomendasikanObservasi = IsDirekomendasikanObservasi(conn, IdPpdbDaftar);
            bool isVerifHasilObservasiDiterima = IsVerifHasilObservasiDiterima(conn, IdPpdbDaftar);
            bool isVerifHasilObservasiTidakDiterima = IsVerifHasilObservasiTidakDiterima(conn, IdPpdbDaftar);
            bool isVerifHasilObservasiCadangan = IsVerifHasilObservasiCadangan(conn, IdPpdbDaftar);
            int isTidakBayarPendidikanSiswa = IsTidakBayarPendidikanSiswa(conn, IdPpdbDaftar);
            int isPengesahan = IsPengesahan(conn, IdPpdbDaftar);


            int? statusProsesObservasi = GetStatusProsesObservasi(conn, IdPpdbDaftar);
            int? statusProsesVerfikasiObservasi = GetStatusProsesVerifikasiObservasi(conn, IdPpdbDaftar);
            bool isBayarPendidikan = IsBayarPendidikan(conn, IdPpdbDaftar);
            bool isBayarPendidikanSiswa = IsBayarPendidikanSiswa(conn, IdPpdbDaftar);
            if (KdJenisKelamin == "L")
            {
                ret.KuotaL += 1;

                if (isDaftarTunggu)
                    ret.DaftarTungguL += 1;
                if (!isDaftarTunggu)
                    ret.DaftarL += 1;

                if (isBayarPendaftaranBelumLunas)
                    ret.TidakBayarDaftarL += 1;

                if (isBayarPendaftaranLunas)
                    ret.BayarDaftarL += 1;

                if (isInputFormulir == 1)
                    ret.TidakInputFormulirL += 1;

                if (isIkutObservasi)
                    ret.IkutObservasiL += 1;

                if (isTidakDirekomendasikanObservasi)
                    ret.TidakDirekomendasikanObservasiL += 1;

                if (isDirekomendasikanObservasi)
                    ret.DirekomendasikanObservasiL += 1;

                if (isVerifHasilObservasiDiterima)
                    ret.VerifHasilObservasiDiterimaL += 1;
                if (isVerifHasilObservasiTidakDiterima)
                    ret.VerifHasilObservasiTidakDiterimaL += 1;
                if (isVerifHasilObservasiCadangan)
                    ret.VerifHasilObservasiCadanganL += 1;

                if (isTidakBayarPendidikanSiswa == 0)
                    ret.VerifTidakBayarPendidikanL += 1;
                if (isPengesahan == 0)
                    ret.PengesahanL += 1;


                if (isPpdbSiswa)
                {
                    if (statusProsesObservasi == StatusPpdbHasilObservasiConstant.Direkomendasikan)
                    {
                        ret.LulusObservasiL += 1;
                        if (!isBayarPendidikan)
                        {
                            if (!isBayarPendidikanSiswa)
                                ret.TidakBayarL += 1;
                        }
                    }
                    else if (statusProsesObservasi == StatusPpdbHasilObservasiConstant.TidakDirekomendasikan)
                    {
                        ret.TidakLulusObservasiL += 1;
                    }

                    if (statusProsesObservasi == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan)
                    {
                        ret.CadanganL += 1;
                    }
                    else if (statusProsesVerfikasiObservasi == StatusPpdbVerifikasiObservasiConstant.Diterima)
                    {
                        ret.TerimaL += 1;
                    }
                }

            }
            else
            {
                ret.KuotaP += 1;
                if (isDaftarTunggu)
                    ret.DaftarTungguP += 1;
                if (!isDaftarTunggu)
                    ret.DaftarP += 1;

                if (isBayarPendaftaranBelumLunas)
                    ret.TidakBayarDaftarP += 1;

                if (isBayarPendaftaranLunas)
                    ret.BayarDaftarP += 1;

                if (isInputFormulir == 1)
                    ret.TidakInputFormulirP += 1;

                if (isIkutObservasi)
                    ret.IkutObservasiP += 1;

                if (isTidakDirekomendasikanObservasi)
                    ret.TidakDirekomendasikanObservasiP += 1;

                if (isDirekomendasikanObservasi)
                    ret.DirekomendasikanObservasiP += 1;

                if (isVerifHasilObservasiDiterima)
                    ret.VerifHasilObservasiDiterimaP += 1;
                if (isVerifHasilObservasiTidakDiterima)
                    ret.VerifHasilObservasiTidakDiterimaP += 1;
                if (isVerifHasilObservasiCadangan)
                    ret.VerifHasilObservasiCadanganP += 1;

                if (isTidakBayarPendidikanSiswa == 0)
                    ret.VerifTidakBayarPendidikanP += 1;
                if (isPengesahan == 0)
                    ret.PengesahanP += 1;

                if (isPpdbSiswa)
                {
                    if (statusProsesObservasi == StatusPpdbHasilObservasiConstant.Direkomendasikan)
                    {
                        ret.LulusObservasiP += 1;
                        if (!isBayarPendidikan)
                        {
                            if (!isBayarPendidikanSiswa)
                                ret.TidakBayarP += 1;
                        }
                    }
                    else if (statusProsesObservasi == StatusPpdbHasilObservasiConstant.TidakDirekomendasikan)
                    {
                        ret.TidakLulusObservasiP += 1;
                    }


                    if (statusProsesObservasi == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan)
                    {
                        ret.CadanganP += 1;
                    }
                    else if (statusProsesVerfikasiObservasi == StatusPpdbVerifikasiObservasiConstant.Diterima)
                    {
                        ret.TerimaP += 1;
                    }
                }
            }
            return ret;
        }
        private PpdbLaporanRekapModel SetLaporanRekap(
                TbPpdbDaftar daftar,
                TbPpdbKelas ppdbKelas,
                TbKelas kelas,
                TbPpdbBiayaDaftar daftarBayar,
                TbPpdbSiswa ppdbSiswa,
                TbPpdbSiswaStatus inputOservasi,
                TbPpdbSiswaStatus verifikasiOservasi,
                TbPpdbSiswaStatus pelunasanBiayaPendidikan,
                TbPpdbSiswaStatus pengesahan,
                TbPpdbSiswaOrtu ayah,
                TbPpdbSiswaOrtu ibu,
                PpdbLaporanRekapModel rekap)
        {
            PpdbLaporanRekapModel ret = rekap;

            if (daftar.KdJenisKelamin == "L")
            {
                ret.KuotaL += 1;
                ret.DaftarL += 1;
                if (daftar.Status == 1) ret.DaftarTungguL += 1;


                if (ppdbSiswa != null)
                {
                    if (inputOservasi != null)
                    {
                        if (inputOservasi.StatusProses == StatusPpdbHasilObservasiConstant.Direkomendasikan)
                        {
                            ret.LulusObservasiL += 1;
                            if (pelunasanBiayaPendidikan == null)
                            {
                                ret.TidakBayarL += 1;
                            }
                        }
                        else
                        {
                            ret.TidakLulusObservasiL += 1;
                        }
                    }
                    if (verifikasiOservasi != null)
                    {
                        if (verifikasiOservasi.StatusProses == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan)
                        {
                            ret.CadanganL += 1;
                        }
                        else
                        {
                            ret.TerimaL += 1;
                        }
                    }
                }

            }
            else
            {
                ret.KuotaP += 1;
                ret.DaftarP += 1;
                if (daftar.Status == 1) ret.DaftarTungguP += 1;


                if (ppdbSiswa != null)
                {
                    if (inputOservasi != null)
                    {
                        if (inputOservasi.StatusProses == StatusPpdbHasilObservasiConstant.Direkomendasikan)
                        {
                            ret.LulusObservasiP += 1;
                            if (pelunasanBiayaPendidikan == null)
                            {
                                ret.TidakBayarP += 1;
                            }
                        }
                        else
                        {
                            ret.TidakLulusObservasiP += 1;
                        }
                    }
                    if (verifikasiOservasi != null)
                    {
                        if (verifikasiOservasi.StatusProses == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan)
                        {
                            ret.CadanganP += 1;
                        }
                        else
                        {
                            ret.TerimaP += 1;
                        }
                    }
                }
            }
            return ret;
        }
        private PpdbLaporanListModel SetPpdbLaporanList(TbPpdbDaftar daftar, TbKelas kelas, int? IdJenisPeminatan, TbPpdbSiswa siswa, TbPpdbSiswaOrtu ayah, TbPpdbSiswaOrtu ibu, List<StatusPpdbModel> StatusProsess)
        {
            PpdbLaporanListModel ret = new PpdbLaporanListModel();
            ret.IdPpdbDaftar = daftar.IdPpdbDaftar.ToString();
            ret.JenjangPendidikan = UnitConstant.DictUnit[kelas.IdUnit] + " - " + kelas.Nama;
            if (kelas.IdUnit == UnitConstant.Sma)
                ret.JurusanPendidikan = JenisPeminatanPpdbConstant.DictJenisPeminatanPpdb[(int)IdJenisPeminatan];
            ret.JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[daftar.IdJalurPendaftaran].ToUpper();
            ret.Nis = daftar.NisSiswa;
            ret.Nama = daftar.Nama;
            ret.JenisKelamin = daftar.KdJenisKelamin;
            ret.TempatLahir = daftar.TempatLahir;
            ret.TanggalLahir = daftar.TanggalLahir.ToString("dd-MM-yyyy");
            ret.Alamat = siswa == null ? "" : siswa.AlamatTinggal;
            ret.NamaAyah = ayah == null ? "" : ayah.Nama;
            ret.NoHpAyah = ayah == null ? "" : ayah.NoHandphone;
            ret.EmailAyah = ayah == null ? "" : ayah.Email;
            ret.NamaIbu = ibu == null ? "" : ibu.Nama;
            ret.NoHpIbu = ibu == null ? "" : ibu.NoHandphone;
            ret.EmailIbu = ibu == null ? "" : ibu.Email;
            ret.NamaPewakaf = "";
            ret.HubunganSiswa = "";
            ret.StatusProses = string.Empty;
            ret.StatusPpdb = string.Empty;
            ret.StatusProsess = StatusProsess;
            return ret;
        }


        private string GetStatusReal(IDbConnection conn, int IdPpdbDaftar)
        {
            bool isPpdbSiswa = IsPpdbSiswa(conn, IdPpdbDaftar);
            bool isDaftarTunggu = IsDaftarTunggu(conn, IdPpdbDaftar);
            int? statusProsesObservasi = GetStatusProsesObservasi(conn, IdPpdbDaftar);
            int? statusProsesVerfikasiObservasi = GetStatusProsesVerifikasiObservasi(conn, IdPpdbDaftar);
            bool isBayarPendidikan = IsBayarPendidikan(conn, IdPpdbDaftar);
            bool isBayarPendidikanSiswa = IsBayarPendidikanSiswa(conn, IdPpdbDaftar);

            string status = "";

            status = "Pendaftaran";
            if (isDaftarTunggu)
                status = "Daftar Tunggu Pendaftaran";


            if (isPpdbSiswa)
            {
                if (statusProsesObservasi == StatusPpdbHasilObservasiConstant.Direkomendasikan)
                {
                    status = "Verifikasi Hasil Observasi";
                    if (!isBayarPendidikan)
                    {
                        if (!isBayarPendidikanSiswa)
                            status = "Pelunasan Biaya Pendidikan";
                    }
                }
                else if (statusProsesObservasi == StatusPpdbHasilObservasiConstant.TidakDirekomendasikan)
                {
                    status = "Verifikasi Hasil Observasi";
                }

                if (statusProsesObservasi == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan)
                {
                    status = "Verifikasi Hasil Observasi";
                }
                else if (statusProsesVerfikasiObservasi == StatusPpdbVerifikasiObservasiConstant.Diterima)
                {
                    status = "Pelunasan Biaya Pendidikan";
                    if (isBayarPendidikanSiswa)
                        status = "Pengesahan";

                }
            }

            return status;
        }

        private string GetStatusProsesReal(IDbConnection conn, int IdPpdbDaftar)
        {
            bool isPpdbSiswa = IsPpdbSiswa(conn, IdPpdbDaftar);
            bool isDaftarTunggu = IsDaftarTunggu(conn, IdPpdbDaftar);
            int? statusProsesObservasi = GetStatusProsesObservasi(conn, IdPpdbDaftar);
            int? statusProsesVerfikasiObservasi = GetStatusProsesVerifikasiObservasi(conn, IdPpdbDaftar);
            bool isBayarPendidikan = IsBayarPendidikan(conn, IdPpdbDaftar);
            bool isBayarPendidikanSiswa = IsBayarPendidikanSiswa(conn, IdPpdbDaftar);

            string statusProses = "";

            statusProses = "Menunggu Pembayaran";
            if (isDaftarTunggu)
                statusProses = "Menunggu Diproses";


            if (isPpdbSiswa)
            {
                if (statusProsesObservasi == StatusPpdbHasilObservasiConstant.Direkomendasikan)
                {
                    statusProses = "Direkomendasikan";
                    if (!isBayarPendidikan)
                    {
                        if (!isBayarPendidikanSiswa)
                            statusProses = "Menunggu Pembayaran";
                    }
                }
                else if (statusProsesObservasi == StatusPpdbHasilObservasiConstant.TidakDirekomendasikan)
                {
                    statusProses = "Tidak Diterima";
                }

                if (statusProsesObservasi == StatusPpdbVerifikasiObservasiConstant.Ditangguhkan)
                {
                    statusProses = "Ditangguhkan/Cadangan";
                }
                else if (statusProsesVerfikasiObservasi == StatusPpdbVerifikasiObservasiConstant.Diterima)
                {
                    statusProses = "Menunggu Pembayaran";
                    if (isBayarPendidikanSiswa)
                        statusProses = "Menunggu Diproses";
                }
            }

            return statusProses;
        }
        public PpdbLaporanModel GetLaporanGelombangPpdbTest(int IdUser, string TanggalAwal, string TanggalAkhir, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                PpdbLaporanModel ret = new PpdbLaporanModel
                {
                    Rekap = new List<PpdbLaporanRekapModel>(),
                    Tk = new List<PpdbLaporanListModel>(),
                    Sd = new List<PpdbLaporanListModel>(),
                    Smp = new List<PpdbLaporanListModel>(),
                    Sma = new List<PpdbLaporanListModel>()
                };
                using (var conn = commonService.DbConnection())
                {
                    var user = (from a in conn.GetList<TbUnitPegawai>()
                                join b in conn.GetList<TbPegawai>() on a.IdPegawai equals b.IdPegawai
                                join c in conn.GetList<TbJenisJabatan>() on b.IdJenisJabatan equals c.IdJenisJabatan
                                join d in conn.GetList<TbUser>() on b.IdPegawai equals d.IdUser
                                where a.IdPegawai == IdUser
                                select new { a, b, c, d }).FirstOrDefault();
                    if (user == null) { oMessage = "pengguna tidak terdaftar sebagai pegawai"; return null; }

                    //public const int Registrasi = 1;
                    //public const int TestObservasi = 20;
                    //public const int Wawancara = 21;
                    //public const int TestObservasiWawancara = 22;
                    //public const int InputObservasi = 3;
                    //public const int VerifikasiObservasi = 4;
                    //public const int PelunasanBiayaPendidikan = 5;
                    //public const int Pengesahan = 6;
                    var data = from daftar in conn.GetList<TbPpdbDaftar>()
                               join ppdbKelas in conn.GetList<TbPpdbKelas>() on daftar.IdPpdbKelas equals ppdbKelas.IdPpdbKelas
                               join kelas in conn.GetList<TbKelas>() on ppdbKelas.IdKelas equals kelas.IdKelas
                               join daftarBayar in conn.GetList<TbPpdbBiayaDaftar>() on daftar.IdPpdbDaftar equals daftarBayar.IdPpdbDaftar into daftarBayar1
                               from daftarBayar in daftarBayar1.DefaultIfEmpty()
                               join ppdbSiswa in conn.GetList<TbPpdbSiswa>() on daftar.IdPpdbDaftar equals ppdbSiswa.IdPpdbDaftar into ppdbSiswa1
                               from ppdbSiswa in ppdbSiswa1.DefaultIfEmpty()
                               join inputOservasi in conn.GetList<TbPpdbSiswaStatus>(string.Format("where status = {0}", StatusPpdbConstant.InputObservasi)) on daftar.IdPpdbDaftar equals inputOservasi.IdPpdbDaftar into inputObservasi1
                               from inputOservasi in inputObservasi1.DefaultIfEmpty()
                               join verifikasiOservasi in conn.GetList<TbPpdbSiswaStatus>(string.Format("where status = {0}", StatusPpdbConstant.VerifikasiObservasi)) on daftar.IdPpdbDaftar equals verifikasiOservasi.IdPpdbDaftar into verifikasiOservasi1
                               from verifikasiOservasi in verifikasiOservasi1.DefaultIfEmpty()
                               join pelunasanBiayaPendidikan in conn.GetList<TbPpdbSiswaStatus>(string.Format("where status = {0}", StatusPpdbConstant.PelunasanBiayaPendidikan)) on daftar.IdPpdbDaftar equals pelunasanBiayaPendidikan.IdPpdbDaftar into pelunasanBiayaPendidikan1
                               from pelunasanBiayaPendidikan in pelunasanBiayaPendidikan1.DefaultIfEmpty()
                               join pengesahan in conn.GetList<TbPpdbSiswaStatus>(string.Format("where status = {0}", StatusPpdbConstant.Pengesahan)) on daftar.IdPpdbDaftar equals pengesahan.IdPpdbDaftar into pengesahan1
                               from pengesahan in pengesahan1.DefaultIfEmpty()
                               join ayah in conn.GetList<TbPpdbSiswaOrtu>("where id_tipe = 1") on daftar.IdPpdbDaftar equals ayah.IdPpdbDaftar into ayah1
                               from ayah in ayah1.DefaultIfEmpty()
                               join ibu in conn.GetList<TbPpdbSiswaOrtu>("where id_tipe = 2") on daftar.IdPpdbDaftar equals ibu.IdPpdbDaftar into ibu1
                               from ibu in ibu1.DefaultIfEmpty()
                               where daftar.CreatedDate.Date >= dateTimeService.StrToDateTime(TanggalAwal).Date
                               & daftar.CreatedDate.Date <= dateTimeService.StrToDateTime(TanggalAkhir).Date
                               select new
                               {
                                   daftar,
                                   ppdbKelas,
                                   kelas,
                                   ppdbSiswa,
                                   daftarBayar,
                                   inputOservasi,
                                   verifikasiOservasi,
                                   pelunasanBiayaPendidikan,
                                   pengesahan,
                                   ayah,
                                   ibu
                               };

                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    PpdbLaporanRekapModel rekapTkA = new PpdbLaporanRekapModel { Unit = "TK-A" };
                    PpdbLaporanRekapModel rekapTkB = new PpdbLaporanRekapModel { Unit = "TK-B" };
                    PpdbLaporanRekapModel rekapSd = new PpdbLaporanRekapModel { Unit = "SD" };
                    PpdbLaporanRekapModel rekapSmp = new PpdbLaporanRekapModel { Unit = "SMP" };
                    PpdbLaporanRekapModel rekapSmaIpa = new PpdbLaporanRekapModel { Unit = "SMA IPA" };
                    PpdbLaporanRekapModel rekapSmaIps = new PpdbLaporanRekapModel { Unit = "SMA IPS" };
                    foreach (var item in data)
                    {
                        PpdbLaporanListModel tk = new PpdbLaporanListModel();
                        PpdbLaporanListModel sd = new PpdbLaporanListModel();
                        PpdbLaporanListModel smp = new PpdbLaporanListModel();
                        PpdbLaporanListModel sma = new PpdbLaporanListModel();
                        List<StatusPpdbModel> statusProsess = new List<StatusPpdbModel>();
                        statusProsess.Add(new StatusPpdbModel
                        {
                            Status = "Daftar",//StatusPpdbConstant.Dict[item.daftar.Status],
                            StatusProses = item.daftar.Status == 1 ? "Pendaftaran" : "Daftar Tunggu",
                            Tanggal = item.daftar.CreatedDate.ToString("dd-MM-yyyy")
                        });
                        bool isBayarPendaftaranLunas = IsBayarPendaftaranLunas(conn, item.daftar.IdPpdbDaftar);
                        statusProsess.Add(new StatusPpdbModel
                        {
                            Status = "Bayar Pendaftaran",//StatusPpdbConstant.Dict[item.daftar.Status],
                            StatusProses = isBayarPendaftaranLunas ? "Lunas" : "Belum Lunas",
                            Tanggal = item.daftar.CreatedDate.ToString("dd-MM-yyyy")
                        });
                        if (item.inputOservasi != null)
                        {
                            statusProsess.Add(new StatusPpdbModel
                            {
                                Status = StatusPpdbConstant.Dict[item.inputOservasi.Status],
                                StatusProses = StatusPpdbHasilObservasiConstant.Dict[item.inputOservasi.StatusProses],
                                Tanggal = item.inputOservasi.CreatedDate.ToString("dd-MM-yyyy")
                            });
                        }
                        if (item.verifikasiOservasi != null)
                        {
                            statusProsess.Add(new StatusPpdbModel
                            {
                                Status = StatusPpdbConstant.Dict[item.verifikasiOservasi.Status],
                                StatusProses = StatusPpdbVerifikasiObservasiConstant.Dict[item.verifikasiOservasi.StatusProses],
                                Tanggal = item.verifikasiOservasi.CreatedDate.ToString("dd-MM-yyyy")
                            });
                        }
                        if (item.pelunasanBiayaPendidikan != null)
                        {
                            bool isBayarPendidikanSiswa = IsBayarPendidikanSiswa(conn, item.daftar.IdPpdbDaftar);
                            statusProsess.Add(new StatusPpdbModel
                            {
                                Status = StatusPpdbConstant.Dict[item.pelunasanBiayaPendidikan.Status],
                                StatusProses = isBayarPendidikanSiswa ? StatusLunasConstant.Dict[StatusLunasConstant.Lunas] : StatusLunasConstant.Dict[StatusLunasConstant.BelumLunas],
                                Tanggal = item.pelunasanBiayaPendidikan.CreatedDate.ToString("dd-MM-yyyy")
                            });
                        }
                        if (item.pengesahan != null)
                        {
                            statusProsess.Add(new StatusPpdbModel
                            {
                                Status = StatusPpdbConstant.Dict[item.pengesahan.Status],
                                StatusProses = "",
                                Tanggal = item.pengesahan.CreatedDate.ToString("dd-MM-yyyy")
                            });
                        }
                        if (item.kelas.IdUnit == UnitConstant.Tk)
                        {
                            ret.Tk.Add(SetPpdbLaporanList(item.daftar, item.kelas, null, item.ppdbSiswa, item.ayah, item.ibu, statusProsess));
                            if (item.kelas.IdKelas == 104)
                            {
                                rekapTkA = SetLaporanRekap(conn, item.daftar.IdPpdbDaftar, item.daftar.KdJenisKelamin, rekapTkA);
                            }
                            else
                            {
                                rekapTkB = SetLaporanRekap(conn, item.daftar.IdPpdbDaftar, item.daftar.KdJenisKelamin, rekapTkB);
                            }
                        }
                        else if (item.kelas.IdUnit == UnitConstant.Sd)
                        {
                            ret.Sd.Add(SetPpdbLaporanList(item.daftar, item.kelas, null, item.ppdbSiswa, item.ayah, item.ibu, statusProsess));
                            rekapSd = SetLaporanRekap(conn, item.daftar.IdPpdbDaftar, item.daftar.KdJenisKelamin, rekapSd);
                        }
                        else if (item.kelas.IdUnit == UnitConstant.Smp)
                        {
                            ret.Smp.Add(SetPpdbLaporanList(item.daftar, item.kelas, null, item.ppdbSiswa, item.ayah, item.ibu, statusProsess));
                            rekapSmp = SetLaporanRekap(conn, item.daftar.IdPpdbDaftar, item.daftar.KdJenisKelamin, rekapSmp);
                        }
                        else if (item.kelas.IdUnit == UnitConstant.Sma)
                        {
                            ret.Sma.Add(SetPpdbLaporanList(item.daftar, item.kelas, item.ppdbKelas.IdJenisPeminatan, item.ppdbSiswa, item.ayah, item.ibu, statusProsess));
                            if (item.ppdbKelas.IdJenisPeminatan == 1)
                            {
                                rekapSmaIpa = SetLaporanRekap(conn, item.daftar.IdPpdbDaftar, item.daftar.KdJenisKelamin, rekapSmaIpa);
                            }
                            else
                            {
                                rekapSmaIps = SetLaporanRekap(conn, item.daftar.IdPpdbDaftar, item.daftar.KdJenisKelamin, rekapSmaIps);
                            }
                        }

                    }
                    ret.Rekap.Add(rekapTkA);
                    ret.Rekap.Add(rekapTkB);
                    ret.Rekap.Add(rekapSd);
                    ret.Rekap.Add(rekapSmp);
                    ret.Rekap.Add(rekapSmaIpa);
                    ret.Rekap.Add(rekapSmaIps);
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetLaporanGelombangPpdb", ex);
                return null;
            }
        }
        #region PPDB DAFTAR PEMINAT
        public string DaftarPeminatPpdb(AddPpdbDaftarPeminat Data, out string oMessage)
        {
            oMessage = string.Empty;
            var context = new ValidationContext(Data, serviceProvider: null, items: null);
            var validationResults = new List<ValidationResult>();
            bool isValid = Validator.TryValidateObject(Data, context, validationResults, true);
            if (!isValid)
            {
                foreach (ValidationResult error in validationResults)
                {
                    oMessage = error.ErrorMessage;
                    return null;
                }
            }
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var cekEmail = conn.GetList<TbPpdbDaftarPeminat>().Where(x => x.Email == Data.Email).FirstOrDefault();
                    if (cekEmail != null)
                    {
                        oMessage = "email sudah terdaftar";
                        return null;
                    }
                    var cekNama = conn.GetList<TbPpdbDaftarPeminat>().Where(x => x.NamaLengkap == Data.NamaLengkap).FirstOrDefault();
                    if (cekNama != null)
                    {
                        oMessage = "nama siswa sudah terdaftar";
                        return null;
                    }
                    var cekNope = conn.GetList<TbPpdbDaftarPeminat>().Where(x => x.NomorHp == Data.NomorHp).FirstOrDefault();
                    if (cekNope != null)
                    {
                        oMessage = "nomor hp sudah terdaftar";
                        return null;
                    }
                    TbPpdbDaftarPeminat tbPpdbDaftarPeminat = new TbPpdbDaftarPeminat
                    {
                        NamaLengkap = Data.NamaLengkap.ToUpper(),
                        NamaAyah = Data.NamaAyah,
                        NamaIbu = Data.NamaIbu,
                        Email = Data.Email,
                        Kelas = Data.Kelas,
                        NomorHp = Data.NomorHp,
                        CreatedDate = dateTimeService.GetCurrdate()
                    };
                    conn.Insert(tbPpdbDaftarPeminat);

                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "DaftarPeminatPpdb", ex);
                return null;
            }
        }

        public List<PpdbDaftarPeminat> GetDaftarPeminatPpdbs(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var datas = conn.GetList<TbPpdbDaftarPeminat>().ToList();
                    if (datas.Count == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    List<PpdbDaftarPeminat> ret = new();
                    foreach (var data in datas.OrderByDescending(x => x.IdPpdbDaftarPeminat))
                    {
                        ret.Add(new PpdbDaftarPeminat
                        {
                            NamaLengkap = data.NamaLengkap.ToUpper(),
                            NamaAyah = data.NamaAyah,
                            NamaIbu = data.NamaIbu,
                            Email = data.Email,
                            Kelas = data.Kelas,
                            NomorHp = data.NomorHp,
                            CreatedDate = data.CreatedDate.ToString("dd-MM-yyyy")
                        });
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetDaftarPeminatPpdbs", ex);
                return null;
            }
        }

        public PpdbDaftarPeminat GetDaftarPeminatPpdb(int IdPpdbDaftarPeminat, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.Get<TbPpdbDaftarPeminat>(IdPpdbDaftarPeminat);
                    if (data == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    PpdbDaftarPeminat ret = new()
                    {
                        NamaLengkap = data.NamaLengkap.ToUpper(),
                        NamaAyah = data.NamaAyah,
                        NamaIbu = data.NamaIbu,
                        Email = data.Email,
                        Kelas = data.Kelas,
                        NomorHp = data.NomorHp,
                        CreatedDate = data.CreatedDate.ToString("dd-MM-yyyy")
                    };
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetDaftarPeminatPpdb", ex);
                return null;
            }
        }
        #endregion
        #region PPDB KONTEN

        public List<PpdbKontenListModel> GetPpdbKontens(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbPpdbKonten>().ToList();
                    if (data == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    List<PpdbKontenListModel> ret = new List<PpdbKontenListModel>();
                    foreach (var item in data.OrderBy(x => x.IdJenisPpdbKonten))
                    {
                        ret.Add(new PpdbKontenListModel
                        {
                            IdPpdbKonten = item.IdPpdbKonten,
                            JenisPpdbKonten = JenisPpdbKontenConstant.DictJenisPpdbKonten[item.IdJenisPpdbKonten],
                            Deskripsi = item.Deskripsi
                        });
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbKontens", ex);
                return null;
            }
        }

        public PpdbKontenByModel GetPpdbKonten(int IdPpdbKonten, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbPpdbKonten>().Where(x => x.IdPpdbKonten == IdPpdbKonten).FirstOrDefault();
                    if (data == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    PpdbKontenByModel ret = new PpdbKontenByModel
                    {
                        IdPpdbKonten = data.IdPpdbKonten,
                        IdJenisPpdbKonten = data.IdJenisPpdbKonten,
                        Deskripsi = data.Deskripsi
                    };

                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPpdbKonten", ex);
                return null;
            }
        }

        public string AddPpdbKonten(int IdJenisPpdbKonten, string Deskripsi, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    bool isJenisPpdbKonten = JenisPpdbKontenConstant.DictJenisPpdbKonten.Any(x => x.Key == IdJenisPpdbKonten);
                    if (!isJenisPpdbKonten)
                    {
                        oMessage = "jenis tidak ditemukan!";
                        return null;
                    }
                    TbPpdbKonten tbPpdbKonten = new TbPpdbKonten
                    {
                        IdJenisPpdbKonten = IdJenisPpdbKonten,
                        Deskripsi = Deskripsi
                    };
                    conn.Insert(tbPpdbKonten);
                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "AddPpdbKonten", ex);
                return null;
            }
        }

        public string EditPpdbKonten(int IdPpdbKonten, string Deskripsi, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbPpdbKonten>().Where(x => x.IdPpdbKonten == IdPpdbKonten).FirstOrDefault();
                    if (data == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    data.Deskripsi = Deskripsi;
                    conn.Update(data);
                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "EditPpdbKonten", ex);
                return null;
            }
        }

        public string UploadLembarKomitmen(int IdPpdbDaftar, string Pin, IFormFile FileLembarKomitmen, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbPpdbSiswa>().Where(x => x.IdPpdbDaftar == IdPpdbDaftar).FirstOrDefault();
                    if (data == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    var ppdbDaftar = conn.GetList<TbPpdbDaftar>().Where(x => x.IdPpdbDaftar == IdPpdbDaftar).FirstOrDefault();
                    if (Pin != ppdbDaftar.Pin)
                    {
                        oMessage = "pin tidak sesuai";
                        return null;
                    }
                    string fileName = string.Empty;
                    if (FileLembarKomitmen != null)
                    {
                        var pathUpload = AppSetting.PathPpdbBerkas;

                        fileName = string.Format("{0}_lembar_komitmen{1}", IdPpdbDaftar, Path.GetExtension(FileLembarKomitmen.FileName));
                        using (var fileStream = new FileStream(Path.Combine(pathUpload, fileName), FileMode.Create, FileAccess.Write))
                        {
                            FileLembarKomitmen.CopyTo(fileStream);
                        }
                    }
                    data.FileLembarKomitmen = fileName;
                    conn.Update(data);
                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "UploadLembarKomitmen", ex);
                return null;
            }
        }


        #endregion

        public string GenerateBaruNameTag(int IdPpdbDaftar, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = (from a in conn.GetList<TbPpdbSiswa>()
                                join b in conn.GetList<TbPpdbKelas>() on a.IdPpdbKelas equals b.IdPpdbKelas
                                join c in conn.GetList<TbKelas>() on b.IdKelas equals c.IdKelas
                                join d in conn.GetList<TbUnit>() on c.IdUnit equals d.IdUnit
                                where a.IdPpdbDaftar == IdPpdbDaftar
                                select new { a, b, c, d }).FirstOrDefault();
                    if (data == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    PpdbDaftarKartuModel dataNametag = new();
                    dataNametag.IdPpdbDaftar = IdPpdbDaftar;
                    dataNametag.Nama = data.a.Nama;
                    dataNametag.NamaPanggilan = data.a.NamaPanggilan;
                    dataNametag.JenisKategoriPendaftaran = KategoriPendaftaranPpdbConstant.DictKategoriPendaftaranPpdb[data.a.IdJenisKategoriPendaftaran];
                    dataNametag.JalurPendaftaran = JalurPendaftaranPpdbConstant.DictJalurPendaftaranPpdb[data.a.IdJalurPendaftaran];
                    dataNametag.NamaPanggilan = data.a.NamaPanggilan;

                    var _pathFile = AppSetting.PathPpdbBerkas;

                    var Foto = IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.PasFoto];
                    string[] getsFoto = Directory.GetFiles(_pathFile, string.Format("{0}.*", Foto));
                    Foto = getsFoto.Count() != 0 ? new DirectoryInfo(getsFoto[0]).Name : null;


                    string _fileName = Path.Combine(_pathFile, Foto);
                    dataNametag.Foto = _fileName;

                    string _fileNametag = null;
                    if (data.c.IdUnit == UnitConstant.Sma)
                        _fileNametag = pdfService.GenerateNameTagSma(dataNametag, _pathFile);


                    // string _fileNametag = $"{data.a.IdPpdbDaftar}{NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuPeserta]}.pdf";
                    string _emailMessage = string.Empty;
                    string _subjects = "Kartu Peserta PPDB";

                    List<string> emailTos = new List<string>();
                    List<string> emailCcs = new List<string>();
                    List<string> _attachments = new List<string>();
                    emailTos.Add(data.a.Email);
                    // emailTos.Add("salimajah01@gmail.com");
                    emailCcs.Add(AppSetting.OrgEmail);
                    _attachments.Add(_fileNametag);
                    _emailMessage = "Terlampir Kartu Peserta dibawah :";

                    SendEmailModel email = new SendEmailModel
                    {
                        To = emailTos,
                        Cc = emailCcs,
                        Title = "Bapak/Ibu ",
                        Subject = _subjects,
                        Message = _emailMessage,
                        Attachments = _attachments
                    };
                    _ = emailService.SendHtmlEmailAsync(email);

                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GenerateBaruNameTag", ex);
                return null;
            }
        }

        public string DownloadBerkas(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var ppdbSiswas = conn.GetList<TbPpdbSiswa>().ToList();
                    //inisialisasi path backup
                    var path = Path.Combine(AppSetting.PathApplFile, "BackupPpdb");
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);
                    foreach (var item in ppdbSiswas.OrderBy(x => x.IdPpdbDaftar))
                    {
                        ///buat folder masing2 idppdbdaftar
                        var pathPpdb = Path.Combine(path, item.IdPpdbDaftar.ToString());
                        if (!Directory.Exists(pathPpdb))
                            Directory.CreateDirectory(pathPpdb);

                        var Foto = item.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.PasFoto];
                        string[] getsFoto = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", Foto));
                        Foto = getsFoto.Count() != 0 ? new DirectoryInfo(getsFoto[0]).Name : null;
                        if (Foto != null)
                        {
                            if (!File.Exists(Path.Combine(pathPpdb, Foto)))
                            {
                                string sourceFile = Path.Combine(AppSetting.PathPpdbBerkas, Foto);
                                FileInfo f1 = new FileInfo(sourceFile);
                                if (f1.Exists)
                                    f1.CopyTo(Path.Combine(pathPpdb, Foto));
                            }
                        }

                        var FileKtpIbu = item.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KtpIbu];
                        string[] getsFileKtpIbu = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileKtpIbu));
                        FileKtpIbu = getsFileKtpIbu.Count() != 0 ? new DirectoryInfo(getsFileKtpIbu[0]).Name : null;
                        if (FileKtpIbu != null)
                        {
                            if (!File.Exists(Path.Combine(pathPpdb, FileKtpIbu)))
                            {
                                string sourceFile = Path.Combine(AppSetting.PathPpdbBerkas, FileKtpIbu);
                                FileInfo f1 = new FileInfo(sourceFile);
                                if (f1.Exists)
                                    f1.CopyTo(Path.Combine(pathPpdb, FileKtpIbu));
                            }
                        }

                        var FileKtpAyah = item.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KtpAyah];
                        string[] getsFileKtpAyah = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileKtpAyah));
                        FileKtpAyah = getsFileKtpAyah.Count() != 0 ? new DirectoryInfo(getsFileKtpAyah[0]).Name : null;
                        if (FileKtpAyah != null)
                        {
                            if (!File.Exists(Path.Combine(pathPpdb, FileKtpAyah)))
                            {
                                string sourceFile = Path.Combine(AppSetting.PathPpdbBerkas, FileKtpAyah);
                                FileInfo f1 = new FileInfo(sourceFile);
                                if (f1.Exists)
                                    f1.CopyTo(Path.Combine(pathPpdb, FileKtpAyah));
                            }
                        }

                        var FileKartuKeluarga = item.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuKeluarga];
                        string[] getsFileKartuKeluarga = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileKartuKeluarga));
                        FileKartuKeluarga = getsFileKartuKeluarga.Count() != 0 ? new DirectoryInfo(getsFileKartuKeluarga[0]).Name : null;
                        if (FileKartuKeluarga != null)
                        {
                            if (!File.Exists(Path.Combine(pathPpdb, FileKartuKeluarga)))
                            {
                                string sourceFile = Path.Combine(AppSetting.PathPpdbBerkas, FileKartuKeluarga);
                                FileInfo f1 = new FileInfo(sourceFile);
                                if (f1.Exists)
                                    f1.CopyTo(Path.Combine(pathPpdb, FileKartuKeluarga));
                            }
                        }

                        var FileAkte = item.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.Akte];
                        string[] getsFileAkte = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileAkte));
                        FileAkte = getsFileAkte.Count() != 0 ? new DirectoryInfo(getsFileAkte[0]).Name : null;
                        if (FileAkte != null)
                        {
                            if (!File.Exists(Path.Combine(pathPpdb, FileAkte)))
                            {
                                string sourceFile = Path.Combine(AppSetting.PathPpdbBerkas, FileAkte);
                                FileInfo f1 = new FileInfo(sourceFile);
                                if (f1.Exists)
                                    f1.CopyTo(Path.Combine(pathPpdb, FileAkte));
                            }
                        }

                        var FileSuratKesehatan = item.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.SuratKesehatan];
                        string[] getsFileSuratKesehatan = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileSuratKesehatan));
                        FileSuratKesehatan = getsFileSuratKesehatan.Count() != 0 ? new DirectoryInfo(getsFileSuratKesehatan[0]).Name : null;
                        if (FileSuratKesehatan != null)
                        {
                            if (!File.Exists(Path.Combine(pathPpdb, FileSuratKesehatan)))
                            {
                                string sourceFile = Path.Combine(AppSetting.PathPpdbBerkas, FileSuratKesehatan);
                                FileInfo f1 = new FileInfo(sourceFile);
                                if (f1.Exists)
                                    f1.CopyTo(Path.Combine(pathPpdb, FileSuratKesehatan));
                            }
                        }

                        var FileBebasNarkoba = item.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KetBebasNarkoba];
                        string[] getsFileBebasNarkoba = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileBebasNarkoba));
                        FileBebasNarkoba = getsFileBebasNarkoba.Count() != 0 ? new DirectoryInfo(getsFileBebasNarkoba[0]).Name : null;
                        if (FileBebasNarkoba != null)
                        {
                            if (!File.Exists(Path.Combine(pathPpdb, FileBebasNarkoba)))
                            {
                                string sourceFile = Path.Combine(AppSetting.PathPpdbBerkas, FileBebasNarkoba);
                                FileInfo f1 = new FileInfo(sourceFile);
                                if (f1.Exists)
                                    f1.CopyTo(Path.Combine(pathPpdb, FileBebasNarkoba));
                            }
                        }

                        var FileKartuPeserta = item.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuPeserta];
                        string[] getsFileKartuPeserta = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", FileKartuPeserta));
                        FileKartuPeserta = getsFileKartuPeserta.Count() != 0 ? new DirectoryInfo(getsFileKartuPeserta[0]).Name : null;
                        if (FileKartuPeserta != null)
                        {
                            if (!File.Exists(Path.Combine(pathPpdb, FileKartuPeserta)))
                            {
                                string sourceFile = Path.Combine(AppSetting.PathPpdbBerkas, FileKartuPeserta);
                                FileInfo f1 = new FileInfo(sourceFile);
                                if (f1.Exists)
                                    f1.CopyTo(Path.Combine(pathPpdb, FileKartuPeserta));
                            }
                        }

                        var SuratPerjanjianCicilan = item.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.SuratPerjanjianCicilan];
                        string[] getsSuratPerjanjianCicilan = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", SuratPerjanjianCicilan));
                        SuratPerjanjianCicilan = getsSuratPerjanjianCicilan.Count() != 0 ? new DirectoryInfo(getsSuratPerjanjianCicilan[0]).Name : null;
                        if (SuratPerjanjianCicilan != null)
                        {
                            if (!File.Exists(Path.Combine(pathPpdb, SuratPerjanjianCicilan)))
                            {
                                string sourceFile = Path.Combine(AppSetting.PathPpdbBerkas, SuratPerjanjianCicilan);
                                FileInfo f1 = new FileInfo(sourceFile);
                                if (f1.Exists)
                                    f1.CopyTo(Path.Combine(pathPpdb, SuratPerjanjianCicilan));
                            }
                        }

                        var KartuPeserta = item.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuPeserta];
                        string[] getsKartuPeserta = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", KartuPeserta));
                        KartuPeserta = getsKartuPeserta.Count() != 0 ? new DirectoryInfo(getsKartuPeserta[0]).Name : null;
                        if (KartuPeserta != null)
                        {
                            if (!File.Exists(Path.Combine(pathPpdb, KartuPeserta)))
                            {
                                string sourceFile = Path.Combine(AppSetting.PathPpdbBerkas, KartuPeserta);
                                FileInfo f1 = new FileInfo(sourceFile);
                                if (f1.Exists)
                                    f1.CopyTo(Path.Combine(pathPpdb, KartuPeserta));
                            }
                        }

                        if (item.FileLembarKomitmen != null)
                        {
                            if (!File.Exists(Path.Combine(pathPpdb, item.FileLembarKomitmen)))
                            {
                                string sourceFile = Path.Combine(AppSetting.PathPpdbBerkas, item.FileLembarKomitmen);
                                FileInfo f1 = new FileInfo(sourceFile);
                                if (f1.Exists)
                                    f1.CopyTo(Path.Combine(pathPpdb, item.FileLembarKomitmen));
                            }
                        }

                        var rapors = conn.GetList<TbPpdbSiswaRapor>().Where(x => x.IdPpdbDaftar == item.IdPpdbDaftar).ToList();
                        var pathRapor = Path.Combine(pathPpdb, "rapor");
                        if (!Directory.Exists(pathRapor))
                            Directory.CreateDirectory(pathRapor);
                        foreach (var rapor in rapors)
                        {
                            string[] getsRapor = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", rapor.FileRapor));
                            rapor.FileRapor = getsRapor.Count() != 0 ? new DirectoryInfo(getsRapor[0]).Name : null;
                            if (rapor.FileRapor != null)
                            {
                                if (!File.Exists(Path.Combine(pathRapor, rapor.FileRapor)))
                                {
                                    string sourceFile = Path.Combine(AppSetting.PathPpdbBerkas, rapor.FileRapor);
                                    FileInfo f1 = new FileInfo(sourceFile);
                                    if (f1.Exists)
                                        f1.CopyTo(Path.Combine(pathRapor, rapor.FileRapor));
                                }
                            }
                        }

                        var prestasis = conn.GetList<TbPpdbSiswaPrestasi>().Where(x => x.IdPpdbDaftar == item.IdPpdbDaftar).ToList();
                        var pathPrestasi = Path.Combine(pathPpdb, "prestasi");
                        if (!Directory.Exists(pathPrestasi))
                            Directory.CreateDirectory(pathPrestasi);
                        foreach (var prestasi in prestasis)
                        {
                            string[] getsSertifikat = Directory.GetFiles(AppSetting.PathPpdbBerkas, string.Format("{0}.*", prestasi.FileSertifikat));
                            prestasi.FileSertifikat = getsSertifikat.Count() != 0 ? new DirectoryInfo(getsSertifikat[0]).Name : null;
                            if (prestasi.FileSertifikat != null)
                            {
                                if (!File.Exists(Path.Combine(pathPrestasi, prestasi.FileSertifikat)))
                                {
                                    string sourceFile = Path.Combine(AppSetting.PathPpdbBerkas, prestasi.FileSertifikat);
                                    FileInfo f1 = new FileInfo(sourceFile);
                                    if (f1.Exists)
                                        f1.CopyTo(Path.Combine(pathPrestasi, prestasi.FileSertifikat));
                                }
                            }
                        }

                    }
                    return null;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "DownloadBerkas", ex);
                return null;
            }
        }
    }
}
