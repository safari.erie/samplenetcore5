using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_tahun_ajaran", Schema = "kbm")]
    public class TbTahunAjaran
    {
        [Key]
        [Column("id_tahun_ajaran")]
        public int IdTahunAjaran { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
        [Column("tanggal_mulai")]
        public DateTime TanggalMulai { get; set; }
        [Column("tanggal_selesai")]
        public DateTime TanggalSelesai { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("status_ppdb")]
        public int StatusPpdb { get; set; }
    }
}
