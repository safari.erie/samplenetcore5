using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_kelas_quran", Schema = "kbm")]
    public class TbKelasQuran
    {
        [Key]
        [Column("id_kelas_quran")]
        public int IdKelasQuran { get; set; }
        [Column("kode")]
        public string Kode { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("id_pegawai")]
        public int IdPegawai { get; set; }
    }
}
