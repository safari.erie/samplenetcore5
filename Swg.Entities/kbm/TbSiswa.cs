using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_siswa", Schema = "kbm")]
    public class TbSiswa
    {
        [Key, Required]
        [Column("id_siswa")]
        public int IdSiswa { get; set; }
        [Column("nis")]
        public string Nis { get; set; }
        [Column("nisn")]
        public string Nisn { get; set; }
        [Column("nik")]
        public string Nik { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_panggilan")]
        public string NamaPanggilan { get; set; }
        [Column("tempat_lahir")]
        public string TempatLahir { get; set; }
        [Column("tanggal_lahir")]
        public DateTime TanggalLahir { get; set; }
        [Column("id_agama")]
        public int IdAgama { get; set; }
        [Column("kd_jenis_kelamin")]
        public string KdJenisKelamin { get; set; }
        [Column("no_handphone")]
        public string NoHandphone { get; set; }
        [Column("no_darurat")]
        public string NoDarurat { get; set; }
        [Column("email")]
        public string Email { get; set; }
        [Column("alamat_tinggal")]
        public string AlamatTinggal { get; set; }
        [Column("alamat_ortu")]
        public string AlamatOrtu { get; set; }
        [Column("nik_ibu")]
        public string NikIbu { get; set; }
        [Column("nama_ibu")]
        public string NamaIbu { get; set; }
        [Column("id_jenis_pekerjaan_ibu")]
        public int IdJenisPekerjaanIbu { get; set; }
        [Column("nama_instansi_ibu")]
        public string NamaInstansiIbu { get; set; }
        [Column("nik_ayah")]
        public string NikAyah { get; set; }
        [Column("nama_ayah")]
        public string NamaAyah { get; set; }
        [Column("id_jenis_pekerjaan_ayah")]
        public int IdJenisPekerjaanAyah { get; set; }
        [Column("nama_instansi_ayah")]
        public string NamaInstansiAyah { get; set; }
        [Column("email_ortu")]
        public string EmailOrtu { get; set; }
        [Column("nis_lama")]
        public string NisLama { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("password")]
        public string Password { get; set; }
        [Column("pin")]
        public string Pin { get; set; }
        [Column("foto_profile")]
        public string FotoProfile { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
