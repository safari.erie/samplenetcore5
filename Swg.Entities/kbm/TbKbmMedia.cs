using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_kbm_media", Schema = "kbm")]
    public class TbKbmMedia
    {
        [Key]
        [Column("id_kbm_media")]
        public int IdKbmMedia { get; set; }
        [Column("id_kbm_materi")]
        public int IdKbmMateri { get; set; }
        [Column("nama_file")]
        public string NamaFile { get; set; }
        [Column("nama_url")]
        public string NamaUrl { get; set; }
    }
}
