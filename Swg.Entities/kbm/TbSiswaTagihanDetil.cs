using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_siswa_tagihan_detil", Schema = "kbm")]
    public class TbSiswaTagihanDetil
    {
        [Key, Required]
        [Column("id_siswa")]
        public int IdSiswa { get; set; }
        [Key, Required]
        [Column("periode")]
        public DateTime Periode { get; set; }
        [Key, Required]
        [Column("id_jenis_tagihan_siswa")]
        public int IdJenisTagihanSiswa { get; set; }
        [Column("rp")]
        public double Rp { get; set; }
    }
}
