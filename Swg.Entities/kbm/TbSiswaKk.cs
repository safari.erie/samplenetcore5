using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_siswa_kk", Schema = "kbm")]
    public class TbSiswaKk
    {
        [Key, Required]
        [Column("id_siswa")]
        public int IdSiswa { get; set; }
        [Column("no_kk")]
        public string NoKk { get; set; }
        [Column("alamat")]
        public string Alamat { get; set; }
        [Column("rt")]
        public string Rt { get; set; }
        [Column("rw")]
        public string Rw { get; set; }
        [Column("dusun")]
        public string Dusun { get; set; }
        [Column("id_wil_desa")]
        public Int64 IdWilDesa { get; set; }
        [Column("kd_pos")]
        public string KdPos { get; set; }
        [Column("no_telp")]
        public string NoTelp { get; set; }
        [Column("longitude")]
        public double Longitude { get; set; }
        [Column("latitude")]
        public double Latitude { get; set; }
    }
}
