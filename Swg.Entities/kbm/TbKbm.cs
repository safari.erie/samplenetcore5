using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_kbm", Schema = "kbm")]
    public class TbKbm
    {
        [Key, Required]
        [Column("id_kelas_paralel")]
        public int IdKelasParalel { get; set; }
        [Key, Required]
        [Column("id_mapel")]
        public int IdMapel { get; set; }
        [Key, Required]
        [Column("id_pegawai")]
        public int IdPegawai { get; set; }
        [Key, Required]
        [Column("id_kbm_jadwal")]
        public int IdKbmJadwal { get; set; }
        [Key, Required]
        [Column("id_kelompok")]
        public int IdKelompok { get; set; }
    }
}
