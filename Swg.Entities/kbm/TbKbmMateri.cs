using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_kbm_materi", Schema = "kbm")]
    public class TbKbmMateri
    {
        [Key]
        [Column("id_kbm_materi")]
        public int IdKbmMateri { get; set; }
        [Column("id_kelas_paralel")]
        public int IdKelasParalel { get; set; }
        [Column("id_mapel")]
        public int IdMapel { get; set; }
        [Column("id_pegawai")]
        public int IdPegawai { get; set; }
        [Column("id_kbm_jadwal")]
        public int IdKbmJadwal { get; set; }
        [Column("id_kelompok")]
        public int IdKelompok { get; set; }
        [Column("tanggal")]
        public DateTime Tanggal { get; set; }
        [Column("judul")]
        public string Judul { get; set; }
        [Column("deskripsi")]
        public string Deskripsi { get; set; }
        [Column("deskripsi_tugas")]
        public string DeskripsiTugas { get; set; }
        [Column("nama_file_materi")]
        public string NamaFileMateri { get; set; }
        [Column("nama_url_meeting")]
        public string NamaUrlMeeting { get; set; }
        [Column("created_by")]
        public int? CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime? CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
