using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_kelas_rombel", Schema = "kbm")]
    public class TbKelasRombel
    {
        [Key, Required]
        [Column("id_kelas_paralel")]
        public int IdKelasParalel { get; set; }
        [Key, Required]
        [Column("id_siswa")]
        public int IdSiswa { get; set; }
    }
}
