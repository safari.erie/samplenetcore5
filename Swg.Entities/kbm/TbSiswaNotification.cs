using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_siswa_notification", Schema = "kbm")]
    public class TbSiswaNotification
    {
        [Key, Required]
        [Column("id_siswa")]
        public int IdSiswa { get; set; }
        [Key, Required]
        [Column("token_fcm")]
        public string TokenFcm { get; set; }
        [Column("tipe")]
        public int Tipe { get; set; }
        [Column("user_agent")]
        public string UserAgent { get; set; }
    }
}
