using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_kelas_quran_progres", Schema = "kbm")]
    public class TbKelasQuranProgres
    {
        [Key, Required]
        [Column("id_kelas_quran")]
        public int IdKelasQuran { get; set; }
        [Key, Required]
        [Column("id_siswa")]
        public int IdSiswa { get; set; }
        [Key, Required]
        [Column("tanggal")]
        public DateTime Tanggal { get; set; }
        [Column("id_jenis_jilid_quran")]
        public int IdJenisJilidQuran { get; set; }
        [Column("halaman")]
        public int Halaman { get; set; }
        [Column("catatan")]
        public string Catatan { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
