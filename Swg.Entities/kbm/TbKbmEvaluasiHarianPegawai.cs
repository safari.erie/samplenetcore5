using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_kbm_evaluasi_harian_pegawai", Schema = "kbm")]
    public class TbKbmEvaluasiHarianPegawai
    {
        [Key]
        [Column("id_kbm_evaluasi_harian_pegawai")]
        public int IdKbmEvaluasiHarianPegawai { get; set; }
        [Column("id_pegawai")]
        public int IdPegawai { get; set; }
        [Column("id_jenis_evaluasi_harian_pegawai")]
        public int IdJenisEvaluasiHarianPegawai { get; set; }
        [Column("tanggal")]
        public DateTime Tanggal { get; set; }
        [Column("is_done")]
        public string IsDone { get; set; }
        [Column("catatan")]
        public string Catatan { get; set; }
    }
}
