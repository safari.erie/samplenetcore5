using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_kbm_soal", Schema = "kbm")]
    public class TbKbmSoal
    {
        [Key]
        [Column("id_kbm_soal")]
        public int IdKbmSoal { get; set; }
        [Column("id_kbm_materi")]
        public int IdKbmMateri { get; set; }
        [Column("id_jenis_soal")]
        public int IdJenisSoal { get; set; }
        [Column("id_jenis_jawaban")]
        public int IdJenisJawaban { get; set; }
        [Column("nama_file")]
        public string NamaFile { get; set; }
        [Column("nama_url")]
        public string NamaUrl { get; set; }
    }
}
