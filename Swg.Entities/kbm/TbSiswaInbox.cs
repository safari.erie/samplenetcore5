using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_siswa_inbox", Schema = "kbm")]
    public class TbSiswaInbox
    {
        [Key]
        [Column("id_siswa_inbox")]
        public int IdSiswaInbox { get; set; }
        [Column("id_siswa")]
        public int IdSiswa { get; set; }
        [Column("tipe")]
        public int Tipe { get; set; }
        [Column("judul")]
        public string Judul { get; set; }
        [Column("deskripsi")]
        public string Deskripsi { get; set; }
        [Column("is_read")]
        public bool IsRead { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("updated_date")]
        public DateTime UpdatedDate { get; set; }
    }
}
