using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_kelas_paralel", Schema = "kbm")]
    public class TbKelasParalel
    {
        [Key]
        [Column("id_kelas_paralel")]
        public int IdKelasParalel { get; set; }
        [Column("id_kelas")]
        public int IdKelas { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
