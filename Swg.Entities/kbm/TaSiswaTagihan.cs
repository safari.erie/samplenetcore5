﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Swg.Entities.kbm
{
    [Table("ta_siswa_tagihan", Schema = "kbm")]
    public class TaSiswaTagihan
    {
        [Key, Required]
        [Column("id_siswa")]
        public int IdSiswa { get; set; }
        [Key, Required]
        [Column("periode")]
        public DateTime Periode { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("ref_no")]
        public string RefNo { get; set; }
        [Column("min")]
        public DateTime Min { get; set; }
        [Column("max")]
        public DateTime Max { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
        [Column("catatan")]
        public string Catatan { get; set; }
        [Column("archived_by")]
        public int? ArchivedBy { get; set; }
        [Column("archived_date")]
        public DateTime? ArchivedDate { get; set; }
    }

}
