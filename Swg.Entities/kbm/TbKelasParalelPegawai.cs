using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_kelas_paralel_pegawai", Schema = "kbm")]
    public class TbKelasParalelPegawai
    {
        [Column("id_kelas_paralel")]
        public int IdKelasParalel { get; set; }
        [Column("id_pegawai")]
        public int IdPegawai { get; set; }
    }
}
