using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_kelas", Schema = "kbm")]
    public class TbKelas
    {
        [Key]
        [Column("id_kelas")]
        public int IdKelas { get; set; }
        [Column("id_unit")]
        public int IdUnit { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
