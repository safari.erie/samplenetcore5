using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_kbm_jadwal", Schema = "kbm")]
    public class TbKbmJadwal
    {
        [Key]
        [Column("id_kbm_jadwal")]
        public int IdKbmJadwal { get; set; }
        [Column("hari_ke")]
        public int HariKe { get; set; }
        [Column("jam_mulai")]
        public TimeSpan JamMulai { get; set; }
        [Column("jam_selesai")]
        public TimeSpan JamSelesai { get; set; }
    }
}
