﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Swg.Entities.kbm
{
    [Table("tb_siswa_profil", Schema = "kbm")]
    public class TbSiswaProfil
    {
        [Key, Required]
        [Column("id_siswa")]
        public int IdSiswa { get; set; }
        [Column("no_akta_lahir")]
        public string NoAktaLahir { get; set; }
        [Column("file_kk")]
        public string FileKk { get; set; }
        [Column("file_akta_lahir")]
        public string FileAktaLahir { get; set; }
        [Column("no_kip")]
        public string NoKip { get; set; }
        [Column("file_kip")]
        public string FileKip { get; set; }
        [Column("kd_golongan_darah")]
        public string KdGolonganDarah { get; set; }
        [Column("id_kewarganegaraan")]
        public int IdKewarganegaraan { get; set; }
        [Column("id_bahasa")]
        public int IdBahasa { get; set; }
        [Column("tinggi_badan")]
        public int TinggiBadan { get; set; }
        [Column("berat_badan")]
        public int BeratBadan { get; set; }
        [Column("lingkar_kepala")]
        public double LingkarKepala { get; set; }
        [Column("anak_ke")]
        public int AnakKe { get; set; }
        [Column("jumlah_sodara_kandung")]
        public int JumlahSodaraKandung { get; set; }
        [Column("jumlah_sodara_tiri")]
        public int JumlahSodaraTiri { get; set; }
        [Column("jumlah_sodara_angkat")]
        public int JumlahSodaraAngkat { get; set; }
        [Column("id_transportasi")]
        public int IdTransportasi { get; set; }
        [Column("jarak_sekolah")]
        public int JarakKeSekolah { get; set; }
        [Column("waktu_tempuh")]
        public double WaktuTempuh { get; set; }
        [Column("penyakit_diderita")]
        public string PenyakitDiderita { get; set; }
        [Column("kelainan_jasmani")]
        public string KelainanJasmani { get; set; }
        [Column("sekolah_asal")]
        public string SekolahAsal { get; set; }
        [Column("alamat_sekolah_asal")]
        public string AlamatSekolahAsal { get; set; }
        [Column("nspn_sekolah_asal")]
        public string NspnSekolahAsal { get; set; }
        [Column("pernah_tk_formal")]
        public bool PernahTkFormal { get; set; }
        [Column("pernah_tk_informal")]
        public bool PernahTkInformal { get; set; }
        [Column("id_hobi")]
        public int IdHobi { get; set; }
        [Column("id_cita2")]
        public int IdCita2 { get; set; }
        [Column("no_ujian")]
        public string NoUjian { get; set; }
        [Column("no_ijazah")]
        public string NoIjazah { get; set; }
        [Column("file_ijazah")]
        public string FileIjazah { get; set; }
        [Column("no_shusbn")]
        public string NoShusbn { get; set; }
        [Column("file_shusbn")]
        public string FileShusbn { get; set; }
        [Column("kelas_dapodik")]
        public string KelasDapodik { get; set; }
        [Column("berkebutuhan_khusus")]
        public string BerkebutuhanKhusus { get; set; }
        [Column("alamat_kk")]
        public string AlamatKk { get; set; }
        [Column("tanggal_lahir_ibu")]
        public DateTime TanggalLahirIbu { get; set; }
        [Column("tanggal_lahir_ayah")]
        public DateTime TanggalLahirAyah { get; set; }
        [Column("pendidikan_terakhir_ayah")]
        public string PendidikanTerakhirAyah { get; set; }
        [Column("pendidikan_terakhir_ibu")]
        public string PendidikanTerakhirIbu { get; set; }
        [Column("penghasilan_ayah")]
        public string PenghasilanAyah { get; set; }
        [Column("penghasilan_ibu")]
        public string PenghasilanIbu { get; set; }
        [Column("berkebutuhan_khusus_ayah")]
        public string BerkebutuhanKhususAyah { get; set; }
        [Column("berkebutuhan_khusus_ibu")]
        public string BerkebutuhanKhususIbu { get; set; }
        [Column("nomor_hp_ayah")]
        public string NomorHpAyah { get; set; }
        [Column("nomor_hp_ibu")]
        public string NomorHpIbu { get; set; }
        [Column("punya_wali")]
        public string PunyaWali { get; set; }
        [Column("nik_wali")]
        public string NikWali { get; set; }
        [Column("nama_wali")]
        public string NamaWali { get; set; }
        [Column("alamat_wali")]
        public string AlamatWali { get; set; }
        [Column("id_jenis_pekerjaan_wali")]
        public int IdJenisPekerjaanWali { get; set; }
        [Column("nama_instansi_wali")]
        public string NamaInstansiWali { get; set; }
        [Column("email_wali")]
        public string EmailWali { get; set; }
        [Column("no_handphone_wali")]
        public string NoHandphoneWali { get; set; }
        [Column("terima_kip")]
        public bool TerimaKip { get; set; }
        [Column("alasan_kip")]
        public string AlasanKip { get; set; }
    }
}
