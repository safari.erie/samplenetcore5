using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_kbm_jawaban_susulan", Schema = "kbm")]
    public class TbKbmJawabanSusulan
    {
        [Key, Required]
        [Column("id_kbm_materi")]
        public int IdKbmMateri { get; set; }
        [Key, Required]
        [Column("id_siswa")]
        public int IdSiswa { get; set; }
        [Column("nama_url")]
        public string NamaUrl { get; set; }
        [Column("nama_file")]
        public string NamaFile { get; set; }
        [Column("catatan")]
        public string Catatan { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime? CreatedDate { get; set; }
        [Column("updated_by")]
        public int UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
