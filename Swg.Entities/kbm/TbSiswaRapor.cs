using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_siswa_rapor", Schema = "kbm")]
    public class TbSiswaRapor
    {
        [Key, Required]
        [Column("id_siswa")]
        public int IdSiswa { get; set; }
        [Key, Required]
        [Column("id_jenis_rapor")]
        public int IdJenisRapor { get; set; }
        [Column("file_rapor")]
        public string FileRapor { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("status")]
        public int Status { get; set; }
    }
}
