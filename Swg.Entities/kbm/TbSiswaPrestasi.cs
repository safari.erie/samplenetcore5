using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_siswa_prestasi", Schema = "kbm")]
    public class TbSiswaPrestasi
    {
        [Key]
        [Column("id_siswa_prestasi")]
        public int IdSiswaPrestasi { get; set; }
        [Column("id_siswa")]
        public int IdSiswa { get; set; }
        [Column("id_jenis_prestasi")]
        public int IdJenisPrestasi { get; set; }
        [Column("tahun")]
        public int Tahun { get; set; }
        [Column("id_jenis_tingkat_prestasi")]
        public int IdJenisTingkatPrestasi { get; set; }
        [Column("nama_penyelenggara")]
        public string NamaPenyelenggara { get; set; }
        [Column("keterangan")]
        public string Keterangan { get; set; }
        [Column("file_sertifikat")]
        public string FileSertifikat { get; set; }
    }
}
