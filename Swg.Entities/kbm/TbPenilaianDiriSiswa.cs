﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Swg.Entities.kbm
{
    [Table("tb_penilaian_diri_siswa", Schema = "kbm")]
    public class TbPenilaianDiriSiswa
    {
        [Key]
        [Column("id_penilaian_diri_siswa")]
        public int IdPenilaianDiriSiswa { get; set; }
        [Column("id_tahun_ajaran")]
        public int IdTahunAjaran { get; set; }
        [Column("id_semester")]
        public int IdSemester { get; set; }
        [Column("id_siswa")]
        public int IdSiswa { get; set; }
        [Column("id_jenis_penilaian_diri")]
        public int IdJenisPenilaianDiri { get; set; }
        [Column("penilaian")]
        public int Penilaian { get; set; }
        [Column("catatan")]
        public string Catatan { get; set; }
    }
}
