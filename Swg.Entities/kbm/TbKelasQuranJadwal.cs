using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_kelas_quran_jadwal", Schema = "kbm")]
    public class TbKelasQuranJadwal
    {
        [Key, Required]
        [Column("id_kelas_quran")]
        public int IdKelasQuran { get; set; }
        [Key, Required]
        [Column("id_kbm_jadwal")]
        public int IdKbmJadwal { get; set; }
    }
}
