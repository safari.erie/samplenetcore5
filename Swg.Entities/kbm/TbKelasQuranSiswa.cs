using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Kbm
{
    [Table("tb_kelas_quran_siswa", Schema = "kbm")]
    public class TbKelasQuranSiswa
    {
        [Key, Required]
        [Column("id_kelas_quran")]
        public int IdKelasQuran { get; set; }
        [Key, Required]
        [Column("id_siswa")]
        public int IdSiswa { get; set; }
    }
}
