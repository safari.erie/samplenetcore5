using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_jenis_akun", Schema = "keu")]
    public class TbJenisAkun
    {
        [Key, Required]
        [Column("id_jenis_akun")]
        public int IdJenisAkun { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
