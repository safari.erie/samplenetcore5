using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_unit_prokeg_wajib_coa", Schema = "keu")]
    public class TbUnitProkegWajibCoa
    {
        [Key]
        [Column("id_unit_prokeg_wajib_coa")]
        public int IdUnitProkegWajibCoa { get; set; }
        [Column("id_unit_prokeg_wajib")]
        public int IdUnitProkegWajib { get; set; }
        [Column("id_coa")]
        public int IdCoa { get; set; }
        [Column("id_jenis_volume")]
        public int IdJenisVolume { get; set; }
        [Column("id_jenis_satuan")]
        public int IdJenisSatuan { get; set; }
        [Column("jumlah_satuan")]
        public double JumlahSatuan { get; set; }
        [Column("jumlah_volume")]
        public double JumlahVolume { get; set; }
        [Column("harga_satuan")]
        public double HargaSatuan { get; set; }
        [Column("catatan")]
        public string Catatan { get; set; }
    }
}
