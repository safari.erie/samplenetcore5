using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_jenis_status_pengajuan", Schema = "keu")]
    public class TbJenisStatusPengajuan
    {
        [Key, Required]
        [Column("id_jenis_status_pengajuan")]
        public int IdJenisStatusPengajuan { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
