using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_unit_prokeg", Schema = "keu")]
    public class TbUnitProkeg
    {
        [Key]
        [Column("id_unit_prokeg")]
        public int IdUnitProkeg { get; set; }
        [Column("id_tahun_ajaran")]
        public int IdTahunAjaran { get; set; }
        [Column("id_unit")]
        public int IdUnit { get; set; }
        [Column("rp")]
        public double Rp { get; set; }
        [Column("status")]
        public int Status { get; set; }
    }
}
