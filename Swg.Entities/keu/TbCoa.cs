using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_coa", Schema = "keu")]
    public class TbCoa
    {
        [Key, Required]
        [Column("id_coa")]
        public int IdCoa { get; set; }
        [Column("id_parent_coa")]
        public int? IdParentCoa { get; set; }
        [Column("id_jenis_akun")]
        public int IdJenisAkun { get; set; }
        [Column("id_jenis_transaksi")]
        public int IdJenisTransaksi { get; set; }
        [Column("kode")]
        public string Kode { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
    }
}
