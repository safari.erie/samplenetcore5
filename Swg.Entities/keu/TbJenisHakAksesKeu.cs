using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_jenis_hak_akses_keu", Schema = "keu")]
    public class TbJenisHakAksesKeu
    {
        [Key]
        [Column("id_jenis_hak_akses_keu")]
        public int IdJenisHakAksesKeu { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
