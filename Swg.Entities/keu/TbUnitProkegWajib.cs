using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_unit_prokeg_wajib", Schema = "keu")]
    public class TbUnitProkegWajib
    {
        [Key]
        [Column("id_unit_prokeg_wajib")]
        public int IdUnitProkegWajib { get; set; }
        [Column("id_unit_prokeg")]
        public int IdUnitProkeg { get; set; }
        [Column("id_kegiatan")]
        public int IdKegiatan { get; set; }
        [Column("tanggal")]
        public DateTime Tanggal { get; set; }
        [Column("rp")]
        public double Rp { get; set; }
    }
}
