using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_unit_coa_used", Schema = "keu")]
    public class TbUnitCoaUsed
    {
        [Key, Required]
        [Column("id_unit_coa_pagu")]
        public int IdUnitCoaPagu { get; set; }
        [Key, Required]
        [Column("id_coa_prokeg")]
        public int IdCoaProkeg { get; set; }
    }
}
