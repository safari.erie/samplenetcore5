using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_jenis_transaksi", Schema = "keu")]
    public class TbJenisTransaksi
    {
        [Key, Required]
        [Column("id_jenis_transaksi")]
        public int IdJenisTransaksi { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
