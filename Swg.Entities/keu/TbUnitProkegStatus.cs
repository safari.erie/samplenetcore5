using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_unit_prokeg_status", Schema = "keu")]
    public class TbUnitProkegStatus
    {
        [Key, Required]
        [Column("id_unit_prokeg")]
        public int IdUnitProkeg { get; set; }
        [Key, Required]
        [Column("status")]
        public int Status { get; set; }
        [Column("status_proses")]
        public int StatusProses { get; set; }
        [Column("catatan")]
        public string Catatan { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
