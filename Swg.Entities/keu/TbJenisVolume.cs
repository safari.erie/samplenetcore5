using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_jenis_volume", Schema = "keu")]
    public class TbJenisVolume
    {
        [Key, Required]
        [Column("id_jenis_volume")]
        public int IdJenisVolume { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
