using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_jenis_status_renpen", Schema = "keu")]
    public class TbJenisStatusRenpen
    {
        [Key, Required]
        [Column("id_jenis_status_renpen")]
        public int IdJenisStatusRenpen { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
