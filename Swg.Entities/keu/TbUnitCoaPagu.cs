using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_unit_coa_pagu", Schema = "keu")]
    public class TbUnitCoaPagu
    {
        [Key]
        [Column("id_unit_coa_pagu")]
        public int IdUnitCoaPagu { get; set; }
        [Column("id_unit")]
        public int IdUnit { get; set; }
        [Column("id_tahun_ajaran")]
        public int IdTahunAjaran { get; set; }
        [Column("id_coa_renpen")]
        public int IdCoaRenpen { get; set; }
        [Column("rp_pagu")]
        public double RpPagu { get; set; }
        [Column("rp_pagu_used")]
        public double RpPaguUsed { get; set; }
    }
}
