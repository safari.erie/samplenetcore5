using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_program_kegiatan", Schema = "keu")]
    public class TbProgramKegiatan
    {
        [Key]
        [Column("id_kegiatan")]
        public int IdKegiatan { get; set; }
        [Column("id_program")]
        public int IdProgram { get; set; }
        [Column("kode")]
        public string Kode { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
    }
}
