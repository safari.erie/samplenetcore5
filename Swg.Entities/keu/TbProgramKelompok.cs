using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_program_kelompok", Schema = "keu")]
    public class TbProgramKelompok
    {
        [Key]
        [Column("id_program_kelompok")]
        public int IdProgramKelompok { get; set; }
        [Column("kode")]
        public string Kode { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
    }
}
