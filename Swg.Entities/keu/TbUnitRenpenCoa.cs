using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_unit_renpen_coa", Schema = "keu")]
    public class TbUnitRenpenCoa
    {
        [Key, Required]
        [Column("id_unit_renpen")]
        public int IdUnitRenpen { get; set; }
        [Key, Required]
        [Column("id_coa")]
        public int IdCoa { get; set; }
        [Column("rp")]
        public double Rp { get; set; }
    }
}
