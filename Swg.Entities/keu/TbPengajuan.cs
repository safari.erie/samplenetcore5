using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_pengajuan", Schema = "keu")]
    public class TbPengajuan
    {
        [Key]
        [Column("id_pengajuan")]
        public int IdPengajuan { get; set; }
        [Column("id_unit_prokeg_wajib")]
        public int? IdUnitProkegWajib { get; set; }
        [Column("id_unit_prokeg_tambahan")]
        public int? IdUnitProkegTambahan { get; set; }
        [Column("tanggal_butuh")]
        public DateTime TanggalButuh { get; set; }
        [Column("nomor")]
        public string Nomor { get; set; }
        [Column("total_rp")]
        public double TotalRp { get; set; }
        [Column("total_rp_realisasi")]
        public double TotalRpRealisasi { get; set; }
        [Column("total_rp_dilaporkan")]
        public double TotalRpDilaporkan { get; set; }
        [Column("status")]
        public int Status { get; set; }
    }
}
