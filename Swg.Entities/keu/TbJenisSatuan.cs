using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_jenis_satuan", Schema = "keu")]
    public class TbJenisSatuan
    {
        [Key, Required]
        [Column("id_jenis_satuan")]
        public int IdJenisSatuan { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
