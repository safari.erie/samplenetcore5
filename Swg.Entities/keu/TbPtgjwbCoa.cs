using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_ptgjwb_coa", Schema = "keu")]
    public class TbPtgjwbCoa
    {
        [Key]
        [Column("id_ptgjwb_coa")]
        public int IdPtgjwbCoa { get; set; }
        [Column("id_pengajuan")]
        public int IdPengajuan { get; set; }
        [Column("id_coa")]
        public int IdCoa { get; set; }
        [Column("id_jenis_volume")]
        public int IdJenisVolume { get; set; }
        [Column("id_jenis_satuan")]
        public int IdJenisSatuan { get; set; }
        [Column("jumlah_satuan")]
        public double JumlahSatuan { get; set; }
        [Column("jumlah_volume")]
        public double JumlahVolume { get; set; }
        [Column("harga_satuan")]
        public double HargaSatuan { get; set; }
        [Column("catatan")]
        public string Catatan { get; set; }
        [Column("file_bukti")]
        public string FileBukti { get; set; }
    }
}
