using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_program_unit", Schema = "keu")]
    public class TbProgramUnit
    {
        [Key, Required]
        [Column("id_unit")]
        public int IdUnit { get; set; }
        [Key, Required]
        [Column("id_program")]
        public int IdProgram { get; set; }
    }
}
