using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_unit_renpen", Schema = "keu")]
    public class TbUnitRenpen
    {
        [Key]
        [Column("id_unit_renpen")]
        public int IdUnitRenpen { get; set; }
        [Column("id_unit")]
        public int IdUnit { get; set; }
        [Column("id_tahun_ajaran")]
        public int IdTahunAjaran { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("rp")]
        public double Rp { get; set; }
    }
}
