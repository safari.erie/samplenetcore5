using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_jenis_jabatan_hak_akses_keu", Schema = "keu")]
    public class TbJenisJabatanHakAksesKeu
    {
        [Key, Required]
        [Column("id_jenis_jabatan")]
        public int IdJenisJabatan { get; set; }
        [Key, Required]
        [Column("id_jenis_hak_akses_keu")]
        public int IdJenisHakAksesKeu { get; set; }
    }
}
