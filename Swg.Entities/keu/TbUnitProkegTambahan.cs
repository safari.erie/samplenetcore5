using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_unit_prokeg_tambahan", Schema = "keu")]
    public class TbUnitProkegTambahan
    {
        [Key]
        [Column("id_unit_prokeg_tambahan")]
        public int IdUnitProkegTambahan { get; set; }
        [Column("id_unit_prokeg")]
        public int IdUnitProkeg { get; set; }
        [Column("id_program")]
        public int IdProgram { get; set; }
        [Column("tanggal")]
        public DateTime Tanggal { get; set; }
        [Column("nama_kegiatan")]
        public string NamaKegiatan { get; set; }
        [Column("rp")]
        public double Rp { get; set; }
    }
}
