using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Keu
{
    [Table("tb_jenis_ptgjwb_status", Schema = "keu")]
    public class TbJenisPtgjwbStatus
    {
        [Key, Required]
        [Column("id_jenis_ptgjwb_status")]
        public int IdJenisPtgjwbStatus { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
