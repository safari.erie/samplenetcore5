using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_jenis_hari_libur", Schema = "ref")]
    public class TbJenisHariLibur
    {
        [Key, Required]
        [Column("id_jenis_hari_libur")]
        public int IdJenisHariLibur { get; set; }
        [Key, Required]
        [Column("tanggal")]
        public DateTime Tanggal { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
