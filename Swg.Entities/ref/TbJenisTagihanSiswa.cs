using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_jenis_tagihan_siswa", Schema = "ref")]
    public class TbJenisTagihanSiswa
    {
        [Key]
        [Column("id_jenis_tagihan_siswa")]
        public int IdJenisTagihanSiswa { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
