using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_wil_kabkot", Schema = "ref")]
    public class TbWilKabkot
    {
        [Key, Required]
        [Column("id_wil_kabkot")]
        public int IdWilKabkot { get; set; }
        [Column("kode")]
        public string Kode { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("id_wil_provinsi")]
        public int IdWilProvinsi { get; set; }
    }
}
