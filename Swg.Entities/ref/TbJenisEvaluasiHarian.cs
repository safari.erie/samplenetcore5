using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_jenis_evaluasi_harian", Schema = "ref")]
    public class TbJenisEvaluasiHarian
    {
        [Key]
        [Column("id_jenis_evaluasi_harian")]
        public int IdJenisEvaluasiHarian { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
