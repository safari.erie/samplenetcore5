using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_jenis_jenjang_pendidikan", Schema = "ref")]
    public class TbJenisJenjangPendidikan
    {
        [Key]
        [Column("id_jenis_jenjang_pendidikan")]
        public int IdJenisJenjangPendidikan { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
