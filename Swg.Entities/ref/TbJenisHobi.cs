﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Swg.Entities.Ref
{
    [Table("tb_jenis_hobi", Schema = "ref")]
    public class TbJenisHobi
    {
        [Key, Required]
        [Column("id_jenis_hobi")]
        public int IdJenisHobi { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
