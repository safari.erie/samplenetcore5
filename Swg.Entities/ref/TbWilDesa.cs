using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_wil_desa", Schema = "ref")]
    public class TbWilDesa
    {
        [Key, Required]
        [Column("id_wil_desa")]
        public Int64 IdWilDesa { get; set; }
        [Column("kode")]
        public string Kode { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("id_wil_kecamatan")]
        public int IdWilKecamatan { get; set; }
    }
}
