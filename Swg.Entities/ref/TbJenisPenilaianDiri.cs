﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Swg.Entities.Ref
{
    [Table("tb_jenis_penilaian_diri", Schema = "ref")]
    public class TbJenisPenilaianDiri
    {
        [Key]
        [Column("id_jenis_penilaian_diri")]
        public int IdJenisPenilaianDiri { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
    }
}
