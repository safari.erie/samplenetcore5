﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Swg.Entities.Ref
{
    [Table("tb_jenis_cita2", Schema = "ref")]
    public class TbJenisCita2
    {
        [Key, Required]
        [Column("id_jenis_cita2")]
        public int IdJenisCita2 { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }

    }
}
