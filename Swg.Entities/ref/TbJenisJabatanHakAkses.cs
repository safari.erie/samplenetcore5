using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_jenis_jabatan_hak_akses", Schema = "ref")]
    public class TbJenisJabatanHakAkses
    {
        [Key, Required]
        [Column("id_jenis_jabatan")]
        public int IdJenisJabatan { get; set; }
        [Key, Required]
        [Column("id_jenis_hak_akses")]
        public int IdJenisHakAkses { get; set; }
    }
}
