using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_jenis_evaluasi_harian_unit", Schema = "ref")]
    public class TbJenisEvaluasiHarianUnit
    {
        [Key, Required]
        [Column("id_jenis_evaluasi_harian")]
        public int IdJenisEvaluasiHarian { get; set; }
        [Key, Required]
        [Column("id_unit")]
        public int IdUnit { get; set; }
    }
}
