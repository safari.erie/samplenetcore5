using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_wil_kecamatan", Schema = "ref")]
    public class TbWilKecamatan
    {
        [Key, Required]
        [Column("id_wil_kecamatan")]
        public int IdWilKecamatan { get; set; }
        [Column("kode")]
        public string Kode { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("id_wil_kabkot")]
        public int IdWilKabkot { get; set; }
    }
}
