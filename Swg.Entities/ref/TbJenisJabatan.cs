using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_jenis_jabatan", Schema = "ref")]
    public class TbJenisJabatan
    {
        [Key]
        [Column("id_jenis_jabatan")]
        public int IdJenisJabatan { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
        [Column("level")]
        public int Level { get; set; }
    }
}
