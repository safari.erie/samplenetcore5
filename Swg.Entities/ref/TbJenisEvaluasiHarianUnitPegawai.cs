using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_jenis_evaluasi_harian_unit_pegawai", Schema = "ref")]
    public class TbJenisEvaluasiHarianUnitPegawai
    {
        [Key, Required]
        [Column("id_jenis_evaluasi_harian_pegawai")]
        public int IdJenisEvaluasiHarianPegawai { get; set; }
        [Key, Required]
        [Column("id_unit")]
        public int IdUnit { get; set; }
    }
}
