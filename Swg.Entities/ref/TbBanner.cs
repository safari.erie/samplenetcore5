using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_banner", Schema = "ref")]
    public class TbBanner
    {
        [Key]
        [Column("id_banner")]
        public int IdBanner { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("tipe")]
        public string Tipe { get; set; }
        [Column("file_image")]
        public string FileImage { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
    }
}
