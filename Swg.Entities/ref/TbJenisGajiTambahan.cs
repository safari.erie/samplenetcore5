using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_jenis_gaji_tambahan", Schema = "ref")]
    public class TbJenisGajiTambahan
    {
        [Key]
        [Column("id_jenis_gaji_tambahan")]
        public int IdJenisGajiTambahan { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
