using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_jenis_pekerjaan", Schema = "ref")]
    public class TbJenisPekerjaan
    {
        [Key]
        [Column("id_jenis_pekerjaan")]
        public int IdJenisPekerjaan { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
