using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_wil_provinsi", Schema = "ref")]
    public class TbWilProvinsi
    {
        [Key, Required]
        [Column("id_wil_provinsi")]
        public int IdWilProvinsi { get; set; }
        [Column("kode")]
        public string Kode { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
    }
}
