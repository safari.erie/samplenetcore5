using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_jenis_biaya_ppdb", Schema = "ref")]
    public class TbJenisBiayaPpdb
    {
        [Key]
        [Column("id_jenis_biaya_ppdb")]
        public int IdJenisBiayaPpdb { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
