using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_jenis_gaji_potongan", Schema = "ref")]
    public class TbJenisGajiPotongan
    {
        [Key]
        [Column("id_jenis_gaji_potongan")]
        public int IdJenisGajiPotongan { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
