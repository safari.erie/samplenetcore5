using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_jenis_jilid_quran", Schema = "ref")]
    public class TbJenisJilidQuran
    {
        [Key]
        [Column("id_jenis_jilid_quran")]
        public int IdJenisJilidQuran { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
