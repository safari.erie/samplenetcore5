using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_jenis_gaji_tidak_tetap", Schema = "ref")]
    public class TbJenisGajiTidakTetap
    {
        [Key]
        [Column("id_jenis_gaji_tidak_tetap")]
        public int IdJenisGajiTidakTetap { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
