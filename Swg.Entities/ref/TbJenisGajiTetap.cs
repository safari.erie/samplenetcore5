using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_jenis_gaji_tetap", Schema = "ref")]
    public class TbJenisGajiTetap
    {
        [Key]
        [Column("id_jenis_gaji_tetap")]
        public int IdJenisGajiTetap { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
