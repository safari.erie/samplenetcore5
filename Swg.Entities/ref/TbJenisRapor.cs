using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_jenis_rapor", Schema = "ref")]
    public class TbJenisRapor
    {
        [Key]
        [Column("id_jenis_rapor")]
        public int IdJenisRapor { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
