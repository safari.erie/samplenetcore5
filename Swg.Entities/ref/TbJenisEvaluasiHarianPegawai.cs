using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_jenis_evaluasi_harian_pegawai", Schema = "ref")]
    public class TbJenisEvaluasiHarianPegawai
    {
        [Key]
        [Column("id_jenis_evaluasi_harian_pegawai")]
        public int IdJenisEvaluasiHarianPegawai { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
