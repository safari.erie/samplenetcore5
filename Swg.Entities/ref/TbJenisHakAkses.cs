using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ref
{
    [Table("tb_jenis_hak_akses", Schema = "ref")]
    public class TbJenisHakAkses
    {
        [Key]
        [Column("id_jenis_hak_akses")]
        public int IdJenisHakAkses { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
