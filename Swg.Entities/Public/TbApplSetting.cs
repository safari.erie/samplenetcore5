﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Swg.Entities.Public
{
    [Table("tb_appl_setting", Schema = "public")]
    public class TbApplSetting
    {
        public TbApplSetting(string code, string name, int valueType, DateTime? valueDate, double? valueNumber, string valueString)
        {
            Code = code ?? throw new ArgumentNullException(nameof(code));
            Name = name ?? throw new ArgumentNullException(nameof(name));
            ValueType = valueType;
            ValueDate = valueDate;
            ValueNumber = valueNumber;
            ValueString = valueString;
        }

        [Key, Required]
        [Column("code")]
        public string Code { get; set; }
        [Column("name")]
        public string Name { get; set; }
        [Column("value_type")]
        public int ValueType { get; set; }
        [Column("value_date")]
        public DateTime? ValueDate { get; set; }
        [Column("value_number")]
        public double? ValueNumber { get; set; }
        [Column("value_string")]
        public string ValueString { get; set; }
    }
}
