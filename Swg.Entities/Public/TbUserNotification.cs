using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Public
{
    [Table("tb_user_notification", Schema = "public")]
    public class TbUserNotification
    {
        [Key, Required]
        [Column("id_user")]
        public int IdUser { get; set; }
        [Key, Required]
        [Column("token_fcm")]
        public string TokenFcm { get; set; }
    }
}
