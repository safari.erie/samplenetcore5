using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Etc
{
    [Table("tb_partner_history_payment", Schema = "etc")]
    public class TbPartnerHistoryPayment
    {
        [Key]
        [Column("id_partner_history_payment")]
        public int IdPartnerHistoryPayment { get; set; }
        [Column("kode_bayar")]
        public string KodeBayar { get; set; }
        [Column("tipe")]
        public string Tipe { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("kelas")]
        public string Kelas { get; set; }
        [Column("jumlah_bayar")]
        public double JumlahBayar { get; set; }
        [Column("catatan")]
        public string Catatan { get; set; }
        [Column("keterangan_gagal")]
        public string KeteranganGagal { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
    }
}
