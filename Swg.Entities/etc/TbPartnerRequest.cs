using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Etc
{
    [Table("tb_partner_request", Schema = "etc")]
    public class TbPartnerRequest
    {
        [Key]
        [Column("id_partner_request")]
        public int IdPartnerRequest { get; set; }
        [Column("kd_partner")]
        public string KdPartner { get; set; }
        [Column("method_name")]
        public string MethodName { get; set; }
        [Column("in_date")]
        public DateTime InDate { get; set; }
        [Column("response_status")]
        public string ResponseStatus { get; set; }
        [Column("response_message")]
        public string ResponseMessage { get; set; }
        [Column("response_result")]
        public string ResponseResult { get; set; }
    }
}
