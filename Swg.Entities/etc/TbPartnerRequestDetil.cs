using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Etc
{
    [Table("tb_partner_request_detil", Schema = "etc")]
    public class TbPartnerRequestDetil
    {
        [Key]
        [Column("id_partner_request_detil")]
        public int IdPartnerRequestDetil { get; set; }
        [Column("id_partner_request")]
        public int IdPartnerRequest { get; set; }
        [Column("param_name")]
        public string ParamName { get; set; }
        [Column("param_value")]
        public string ParamValue { get; set; }
    }
}
