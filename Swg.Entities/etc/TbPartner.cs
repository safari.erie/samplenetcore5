using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Etc
{
    [Table("tb_partner", Schema = "etc")]
    public class TbPartner
    {
        [Key, Required]
        [Column("id_user")]
        public int IdUser { get; set; }
        [Column("kd_partner")]
        public string KdPartner { get; set; }
        [Column("password")]
        public string Password { get; set; }
        [Column("partner_name")]
        public string PartnerName { get; set; }
        [Column("partner_ip")]
        public string PartnerIp { get; set; }
        [Column("is_login")]
        public int IsLogin { get; set; }
        [Column("last_login")]
        public DateTime? LastLogin { get; set; }
    }
}
