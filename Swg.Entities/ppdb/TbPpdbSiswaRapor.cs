using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_siswa_rapor", Schema = "ppdb")]
    public class TbPpdbSiswaRapor
    {
        [Key, Required]
        [Column("id_ppdb_daftar")]
        public int IdPpdbDaftar { get; set; }
        [Key, Required]
        [Column("semester")]
        public int Semester { get; set; }
        [Key, Required]
        [Column("mapel")]
        public string Mapel { get; set; }
        [Column("nilai")]
        public double Nilai { get; set; }
        [Column("file_rapor")]
        public string FileRapor { get; set; }
    }
}
