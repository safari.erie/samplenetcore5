using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_kuisioner_jawaban", Schema = "ppdb")]
    public class TbPpdbKuisionerJawaban
    {
        [Key, Required]
        [Column("id_ppdb_daftar")]
        public int IdPpdbDaftar { get; set; }
        [Key, Required]
        [Column("id_ppdb_kuisioner_pertanyaan")]
        public int IdPpdbKuisionerPertanyaan { get; set; }
        [Column("opsi_jawaban")]
        public string OpsiJawaban { get; set; }
        [Column("jawaban")]
        public string Jawaban { get; set; }
    }
}
