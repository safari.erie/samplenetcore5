using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_biaya_siswa", Schema = "ppdb")]
    public class TbPpdbBiayaSiswa
    {
        [Key, Required]
        [Column("id_ppdb_daftar")]
        public int IdPpdbDaftar { get; set; }
        [Column("catatan")]
        public string Catatan { get; set; }
        [Column("is_cicil")]
        public int IsCicil { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("ref_no")]
        public string RefNo { get; set; }
        [Column("tanggal_lunas")]
        public DateTime? TanggalLunas { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
