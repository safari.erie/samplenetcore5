using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb", Schema = "ppdb")]
    public class TbPpdb
    {
        [Key, Required]
        [Column("id_ppdb")]
        public int IdPpdb { get; set; }
        [Column("tahun_ajaran")]
        public string TahunAjaran { get; set; }
        [Column("tanggal_awal")]
        public DateTime TanggalAwal { get; set; }
        [Column("tanggal_akhir")]
        public DateTime TanggalAkhir { get; set; }
        [Column("id_ketua")]
        public int IdKetua { get; set; }
        [Column("min_no_sk")]
        public int MinNoSk { get; set; }
        [Column("file_petunjuk")]
        public string FilePetunjuk { get; set; }
        [Column("status")]
        public int Status { get; set; }
    }
}
