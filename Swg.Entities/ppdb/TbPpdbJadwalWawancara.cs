using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_jadwal_wawancara", Schema = "ppdb")]
    public class TbPpdbJadwalWawancara
    {
        [Key]
        [Column("id_ppdb_jadwal_wawancara")]
        public int IdPpdbJadwalWawancara { get; set; }
        [Column("id_unit")]
        public int IdUnit { get; set; }
        [Column("id_pegawai")]
        public int IdPegawai { get; set; }
        [Column("tanggal")]
        public DateTime Tanggal { get; set; }
        [Column("jam_awal")]
        public TimeSpan JamAwal { get; set; }
        [Column("jam_akhir")]
        public TimeSpan JamAkhir { get; set; }
        [Column("lokasi")]
        public string Lokasi { get; set; }
        [Column("url_link")]
        public string UrlLink { get; set; }
        [Column("jumlah_kuota")]
        public int JumlahKuota { get; set; }
        [Column("jumlah_siswa")]
        public int JumlahSiswa { get; set; }
    }
}
