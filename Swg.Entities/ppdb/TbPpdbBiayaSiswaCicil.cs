using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_biaya_siswa_cicil", Schema = "ppdb")]
    public class TbPpdbBiayaSiswaCicil
    {
        [Key, Required]
        [Column("id_ppdb_daftar")]
        public int IdPpdbDaftar { get; set; }
        [Key, Required]
        [Column("cil_ke")]
        public int CilKe { get; set; }
        [Column("rp_cil_ke")]
        public double RpCilKe { get; set; }
        [Column("tanggal_cil_ke")]
        public DateTime TanggalCilKe { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("tanggal_lunas")]
        public DateTime? TanggalLunas { get; set; }
    }
}
