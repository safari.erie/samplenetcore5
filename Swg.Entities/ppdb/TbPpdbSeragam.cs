using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_seragam", Schema = "ppdb")]
    public class TbPpdbSeragam
    {
        [Key]
        [Column("id_ppdb_seragam")]
        public int IdPpdbSeragam { get; set; }
        [Column("id_unit")]
        public int IdUnit { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
    }
}
