using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_redaksi_email", Schema = "ppdb")]
    public class TbPpdbRedaksiEmail
    {
        [Key]
        [Column("id_ppdb_redaksi_email")]
        public int IdPpdbRedaksiEmail { get; set; }
        [Column("id_ppdb")]
        public int IdPpdb { get; set; }
        [Column("id_jenis_redaksi_email_ppdb")]
        public int IdJenisRedaksiEmailPpdb { get; set; }
        [Column("message_body1")]
        public string MessageBody1 { get; set; }
        [Column("message_body2")]
        public string MessageBody2 { get; set; }
    }
}
