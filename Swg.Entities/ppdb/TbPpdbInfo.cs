using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_info", Schema = "ppdb")]
    public class TbPpdbInfo
    {
        [Key]
        [Column("id_ppdb_info")]
        public int IdPpdbInfo { get; set; }
        [Column("id_ppdb")]
        public int IdPpdb { get; set; }
        [Column("id_jenis_ppdb_info")]
        public int IdJenisPpdbInfo { get; set; }
        [Column("judul")]
        public string Judul { get; set; }
        [Column("konten")]
        public string Konten { get; set; }
        [Column("file_image")]
        public string FileImage { get; set; }
        [Column("url")]
        public string Url { get; set; }
    }
}
