using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_va", Schema = "ppdb")]
    public class TbPpdbVa
    {
        [Key]
        [Column("id_ppdb_va")]
        public int IdPpdbVa { get; set; }
        [Column("id_unit")]
        public int IdUnit { get; set; }
        [Column("status")]
        public int Status { get; set; }
    }
}
