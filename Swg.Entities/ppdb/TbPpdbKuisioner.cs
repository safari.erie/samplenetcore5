using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_kuisioner", Schema = "ppdb")]
    public class TbPpdbKuisioner
    {
        [Key, Required]
        [Column("id_unit")]
        public int IdUnit { get; set; }
        [Key, Required]
        [Column("id_jenis_pendaftaran")]
        public int IdJenisPendaftaran { get; set; }
        [Key, Required]
        [Column("id_jalur_pendaftaran")]
        public int IdJalurPendaftaran { get; set; }
        [Key, Required]
        [Column("id_ppdb_kuisioner_pertanyaan")]
        public int IdPpdbKuisionerPertanyaan { get; set; }
    }
}
