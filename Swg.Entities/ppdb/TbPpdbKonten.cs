using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_konten", Schema = "ppdb")]
    public class TbPpdbKonten
    {
        [Key]
        [Column("id_ppdb_konten")]
        public int IdPpdbKonten { get; set; }
        [Column("id_jenis_ppdb_konten")]
        public int IdJenisPpdbKonten { get; set; }
        [Column("deskripsi")]
        public string Deskripsi { get; set; }
    }
}
