using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_siswa_observasi", Schema = "ppdb")]
    public class TbPpdbSiswaObservasi
    {
        [Key, Required]
        [Column("id_ppdb_jadwal_observasi")]
        public int IdPpdbJadwalObservasi { get; set; }
        [Key, Required]
        [Column("id_ppdb_daftar")]
        public int IdPpdbDaftar { get; set; }
    }
}
