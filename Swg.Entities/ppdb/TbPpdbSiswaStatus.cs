using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_siswa_status", Schema = "ppdb")]
    public class TbPpdbSiswaStatus
    {
        [Key, Required]
        [Column("id_ppdb_daftar")]
        public int IdPpdbDaftar { get; set; }
        [Key, Required]
        [Column("status")]
        public int Status { get; set; }
        [Column("status_proses")]
        public int StatusProses { get; set; }
        [Column("catatan_proses")]
        public string CatatanProses { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
    }
}
