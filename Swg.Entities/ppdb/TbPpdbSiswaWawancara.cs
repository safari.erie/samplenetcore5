using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_siswa_wawancara", Schema = "ppdb")]
    public class TbPpdbSiswaWawancara
    {
        [Key, Required]
        [Column("id_ppdb_jadwal_wawancara")]
        public int IdPpdbJadwalWawancara { get; set; }
        [Key, Required]
        [Column("id_ppdb_daftar")]
        public int IdPpdbDaftar { get; set; }
    }
}
