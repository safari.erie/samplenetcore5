using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_daftar_peminat", Schema = "ppdb")]
    public class TbPpdbDaftarPeminat
    {
        [Key]
        [Column("id_ppdb_daftar_peminat")]
        public int IdPpdbDaftarPeminat { get; set; }
        [Column("nama_lengkap")]
        public string NamaLengkap { get; set; }
        [Column("kelas")]
        public string Kelas { get; set; }
        [Column("nama_ayah")]
        public string NamaAyah { get; set; }
        [Column("nama_ibu")]
        public string NamaIbu { get; set; }
        [Column("email")]
        public string Email { get; set; }
        [Column("nomor_hp")]
        public string NomorHp { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
    }
}
