using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_siswa_ortu", Schema = "ppdb")]
    public class TbPpdbSiswaOrtu
    {
        [Key, Required]
        [Column("id_ppdb_daftar")]
        public int IdPpdbDaftar { get; set; }
        [Key, Required]
        [Column("id_tipe")]
        public int IdTipe { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("alamat")]
        public string Alamat { get; set; }
        [Column("nik")]
        public string Nik { get; set; }
        [Column("id_jenis_pekerjaan")]
        public int IdJenisPekerjaan { get; set; }
        [Column("nama_instansi")]
        public string NamaInstansi { get; set; }
        [Column("jabatan")]
        public string Jabatan { get; set; }
        [Column("email")]
        public string Email { get; set; }
        [Column("no_handphone")]
        public string NoHandphone { get; set; }
        [Column("no_telp_rumah")]
        public string NoTelpRumah { get; set; }
        [Column("id_agama")]
        public int IdAgama { get; set; }
        [Column("id_kewarganegaraan")]
        public int IdKewarganegaraan { get; set; }
        [Column("id_jenis_jenjang_pendidikan")]
        public int IdJenisJenjangPendidikan { get; set; }
        [Column("id_status_penikahan")]
        public int IdStatusPenikahan { get; set; }
        [Column("tempat_lahir")]
        public string TempatLahir { get; set; }
        [Column("tanggal_lahir")]
        public DateTime TanggalLahir { get; set; }
        [Column("id_status_hidup")]
        public int IdStatusHidup { get; set; }
        [Column("tanggal_meninggal")]
        public DateTime TanggalMeninggal { get; set; }
    }
}
