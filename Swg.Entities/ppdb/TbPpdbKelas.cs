using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_kelas", Schema = "ppdb")]
    public class TbPpdbKelas
    {
        [Key]
        [Column("id_ppdb_kelas")]
        public int IdPpdbKelas { get; set; }
        [Column("id_ppdb")]
        public int IdPpdb { get; set; }
        [Column("id_kelas")]
        public int IdKelas { get; set; }
        [Column("id_jenis_peminatan")]
        public int? IdJenisPeminatan { get; set; }
        [Column("kuota_daftar_p")]
        public int KuotaDaftarP { get; set; }
        [Column("kuota_daftar_l")]
        public int KuotaDaftarL { get; set; }
        [Column("kuota_daftar_prestasi")]
        public int KuotaDaftarPrestasi { get; set; }
        [Column("kuota_terima_p")]
        public int KuotaTerimaP { get; set; }
        [Column("kuota_terima_l")]
        public int KuotaTerimaL { get; set; }
        [Column("max_tanggal_lahir")]
        public DateTime MaxTanggalLahir { get; set; }
    }
}
