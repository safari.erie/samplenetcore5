using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_biaya", Schema = "ppdb")]
    public class TbPpdbBiaya
    {
        [Key]
        [Column("id_ppdb_biaya")]
        public int IdPpdbBiaya { get; set; }
        [Column("id_ppdb_kelas")]
        public int IdPpdbKelas { get; set; }
        [Column("id_ppdb")]
        public int IdPpdb { get; set; }
        [Column("id_jenis_biaya_ppdb")]
        public int IdJenisBiayaPpdb { get; set; }
        [Column("rp")]
        public double Rp { get; set; }
    }
}
