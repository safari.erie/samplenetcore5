using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_jawaban_kuisioner", Schema = "ppdb")]
    public class TbPpdbJawabanKuisioner
    {
        [Key, Required]
        [Column("id_ppdb_daftar")]
        public int IdPpdbDaftar { get; set; }
        [Key, Required]
        [Column("id_pertanyaan_kuisioner")]
        public int IdPertanyaanKuisioner { get; set; }
        [Column("opsi_jawaban")]
        public string OpsiJawaban { get; set; }
        [Column("jawaban")]
        public string Jawaban { get; set; }
    }
}
