using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_jadwal", Schema = "ppdb")]
    public class TbPpdbJadwal
    {
        [Key, Required]
        [Column("id_ppdb")]
        public int IdPpdb { get; set; }
        [Key, Required]
        [Column("id_unit")]
        public int IdUnit { get; set; }
        [Key, Required]
        [Column("id_jenis_gelombang_ppdb")]
        public int IdJenisGelombangPpdb { get; set; }
        [Key, Required]
        [Column("id_jenis_pendaftaran")]
        public int IdJenisPendaftaran { get; set; }
        [Key, Required]
        [Column("id_jalur_pendaftaran")]
        public int IdJalurPendaftaran { get; set; }
        [Column("tanggal_awal_daftar")]
        public DateTime TanggalAwalDaftar { get; set; }
        [Column("tanggal_akhir_daftar")]
        public DateTime TanggalAkhirDaftar { get; set; }
        [Column("tanggal_awal_bayar_daftar")]
        public DateTime TanggalAwalBayarDaftar { get; set; }
        [Column("tanggal_akhir_bayar_daftar")]
        public DateTime TanggalAkhirBayarDaftar { get; set; }
        [Column("tanggal_awal_observasi")]
        public DateTime TanggalAwalObservasi { get; set; }
        [Column("tanggal_akhir_observasi")]
        public DateTime TanggalAkhirObservasi { get; set; }
        [Column("tanggal_awal_bayar_pendidikan")]
        public DateTime TanggalAwalBayarPendidikan { get; set; }
        [Column("tanggal_akhir_bayar_pendidikan")]
        public DateTime TanggalAkhirBayarPendidikan { get; set; }
        [Column("tanggal_konfirmasi_cadangan")]
        public DateTime TanggalKonfirmasiCadangan { get; set; }
    }
}
