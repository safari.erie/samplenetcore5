using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_siswa", Schema = "ppdb")]
    public class TbPpdbSiswa
    {
        [Key, Required]
        [Column("id_ppdb_daftar")]
        public int IdPpdbDaftar { get; set; }
        [Column("id_ppdb_kelas")]
        public int IdPpdbKelas { get; set; }
        [Column("id_jenis_kategori_pendaftaran")]
        public int IdJenisKategoriPendaftaran { get; set; }
        [Column("id_jenis_pendaftaran")]
        public int IdJenisPendaftaran { get; set; }
        [Column("id_jalur_pendaftaran")]
        public int IdJalurPendaftaran { get; set; }
        [Column("id_ppdb_seragam")]
        public int IdPpdbSeragam { get; set; }
        [Column("id_siswa")]
        public int? IdSiswa { get; set; }
        [Column("id_pegawai")]
        public int? IdPegawai { get; set; }
        [Column("id_siswa_sodara")]
        public int? IdSiswaSodara { get; set; }
        [Column("nik")]
        public string Nik { get; set; }
        [Column("nisn")]
        public string Nisn { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_panggilan")]
        public string NamaPanggilan { get; set; }
        [Column("tempat_lahir")]
        public string TempatLahir { get; set; }
        [Column("tanggal_lahir")]
        public DateTime TanggalLahir { get; set; }
        [Column("id_agama")]
        public int IdAgama { get; set; }
        [Column("id_kewarganegaraan")]
        public int IdKewarganegaraan { get; set; }
        [Column("kd_jenis_kelamin")]
        public string KdJenisKelamin { get; set; }
        [Column("kd_golongan_darah")]
        public string KdGolonganDarah { get; set; }
        [Column("alamat_tinggal")]
        public string AlamatTinggal { get; set; }
        [Column("id_tinggal")]
        public int IdTinggal { get; set; }
        [Column("email")]
        public string Email { get; set; }
        [Column("no_handphone")]
        public string NoHandphone { get; set; }
        [Column("no_darurat")]
        public string NoDarurat { get; set; }
        [Column("id_bahasa")]
        public int? IdBahasa { get; set; }
        [Column("tinggi_badan")]
        public int? TinggiBadan { get; set; }
        [Column("berat_badan")]
        public int? BeratBadan { get; set; }
        [Column("anak_ke")]
        public int? AnakKe { get; set; }
        [Column("jumlah_sodara_kandung")]
        public int? JumlahSodaraKandung { get; set; }
        [Column("jumlah_sodara_tiri")]
        public int? JumlahSodaraTiri { get; set; }
        [Column("jumlah_sodara_angkat")]
        public int? JumlahSodaraAngkat { get; set; }
        [Column("id_transportasi")]
        public int IdTransportasi { get; set; }
        [Column("jarak_ke_sekolah")]
        public int? JarakKeSekolah { get; set; }
        [Column("waktu_tempuh")]
        public double? WaktuTempuh { get; set; }
        [Column("penyakit_diderita")]
        public string PenyakitDiderita { get; set; }
        [Column("kelainan_jasmani")]
        public string KelainanJasmani { get; set; }
        [Column("sekolah_asal")]
        public string SekolahAsal { get; set; }
        [Column("alamat_sekolah_asal")]
        public string AlamatSekolahAsal { get; set; }
        [Column("nspn_sekolah_asal")]
        public string NspnSekolahAsal { get; set; }
        [Column("no_sk_terima")]
        public string NoSkTerima { get; set; }
        [Column("file_lembar_komitmen")]
        public string FileLembarKomitmen { get; set; }
        [Column("terima_kip")]
        public bool TerimaKip { get; set; }
        [Column("alasan_kip")]
        public string AlasanKip { get; set; }
        [Column("id_hobi")]
        public int IdHobi { get; set; }
        [Column("id_cita2")]
        public int IdCita2 { get; set; }
    }
}
