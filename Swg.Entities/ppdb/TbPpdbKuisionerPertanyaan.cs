using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_kuisioner_pertanyaan", Schema = "ppdb")]
    public class TbPpdbKuisionerPertanyaan
    {
        [Key]
        [Column("id_ppdb_kuisioner_pertanyaan")]
        public int IdPpdbKuisionerPertanyaan { get; set; }
        [Column("id_jenis_pertanyaan")]
        public int IdJenisPertanyaan { get; set; }
        [Column("pertanyaan")]
        public string Pertanyaan { get; set; }
    }
}
