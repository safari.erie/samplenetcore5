using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_biaya_siswa_detil", Schema = "ppdb")]
    public class TbPpdbBiayaSiswaDetil
    {
        [Key, Required]
        [Column("id_ppdb_daftar")]
        public int IdPpdbDaftar { get; set; }
        [Key, Required]
        [Column("id_jenis_biaya_ppdb")]
        public int IdJenisBiayaPpdb { get; set; }
        [Column("rp")]
        public double Rp { get; set; }
        [Column("catatan")]
        public string Catatan { get; set; }
    }
}
