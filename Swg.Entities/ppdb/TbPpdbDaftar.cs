using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_daftar", Schema = "ppdb")]
    public class TbPpdbDaftar
    {
        [Key, Required]
        [Column("id_ppdb_daftar")]
        public int IdPpdbDaftar { get; set; }
        [Column("id_ppdb_kelas")]
        public int IdPpdbKelas { get; set; }
        [Column("id_jenis_kategori_pendaftaran")]
        public int IdJenisKategoriPendaftaran { get; set; }
        [Column("id_jenis_pendaftaran")]
        public int IdJenisPendaftaran { get; set; }
        [Column("id_jalur_pendaftaran")]
        public int IdJalurPendaftaran { get; set; }
        [Column("nis_siswa")]
        public string NisSiswa { get; set; }
        [Column("nis_sodara")]
        public string NisSodara { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("kd_jenis_kelamin")]
        public string KdJenisKelamin { get; set; }
        [Column("tempat_lahir")]
        public string TempatLahir { get; set; }
        [Column("tanggal_lahir")]
        public DateTime TanggalLahir { get; set; }
        [Column("email")]
        public string Email { get; set; }
        [Column("no_handphone")]
        public string NoHandphone { get; set; }
        [Column("nik_ortu")]
        public string NikOrtu { get; set; }
        [Column("pin")]
        public string Pin { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
    }
}
