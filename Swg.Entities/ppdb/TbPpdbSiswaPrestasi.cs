using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Ppdb
{
    [Table("tb_ppdb_siswa_prestasi", Schema = "ppdb")]
    public class TbPpdbSiswaPrestasi
    {
        [Key]
        [Column("id_ppdb_siswa_prestasi")]
        public int IdPpdbSiswaPrestasi { get; set; }
        [Column("id_ppdb_daftar")]
        public int IdPpdbDaftar { get; set; }
        [Column("id_jenis_prestasi")]
        public int IdJenisPrestasi { get; set; }
        [Column("tahun")]
        public int Tahun { get; set; }
        [Column("id_jenis_tingkat_prestasi")]
        public int IdJenisTingkatPrestasi { get; set; }
        [Column("nama_penyelenggara")]
        public string NamaPenyelenggara { get; set; }
        [Column("keterangan")]
        public string Keterangan { get; set; }
        [Column("file_sertifikat")]
        public string FileSertifikat { get; set; }
    }
}
