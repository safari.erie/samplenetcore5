using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Lmg
{
    [Table("tb_pegawai_gaji", Schema = "lmg")]
    public class TbPegawaiGaji
    {
        [Key, Required]
        [Column("id_pegawai")]
        public int IdPegawai { get; set; }
        [Key, Required]
        [Column("periode")]
        public DateTime Periode { get; set; }
        [Column("jabatan")]
        public string Jabatan { get; set; }
        [Column("unit")]
        public string Unit { get; set; }
        [Column("jenis_pegawai")]
        public string JenisPegawai { get; set; }
        [Column("jenjang_pendidikan")]
        public string JenjangPendidikan { get; set; }
        [Column("golongan_pegawai")]
        public string GolonganPegawai { get; set; }
        [Column("agama")]
        public string Agama { get; set; }
        [Column("nama_institusi_pendidikan")]
        public string NamaInstitusiPendidikan { get; set; }
        [Column("nama_pegawai")]
        public string NamaPegawai { get; set; }
        [Column("nip")]
        public string Nip { get; set; }
        [Column("tanggal_masuk")]
        public DateTime TanggalMasuk { get; set; }
    }
}
