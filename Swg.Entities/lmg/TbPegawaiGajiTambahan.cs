using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Lmg
{
    [Table("tb_pegawai_gaji_tambahan", Schema = "lmg")]
    public class TbPegawaiGajiTambahan
    {
        [Key, Required]
        [Column("id_pegawai")]
        public int IdPegawai { get; set; }
        [Key, Required]
        [Column("periode")]
        public DateTime Periode { get; set; }
        [Key, Required]
        [Column("id_jenis_gaji_tambahan")]
        public int IdJenisGajiTambahan { get; set; }
        [Column("rp")]
        public double Rp { get; set; }
    }
}
