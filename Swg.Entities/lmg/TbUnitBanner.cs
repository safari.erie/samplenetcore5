using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Lmg
{
    [Table("tb_unit_banner", Schema = "lmg")]
    public class TbUnitBanner
    {
        [Key]
        [Column("id_unit_banner")]
        public int IdUnitBanner { get; set; }
        [Column("id_unit")]
        public int IdUnit { get; set; }
        [Column("file_image")]
        public string FileImage { get; set; }
    }
}
