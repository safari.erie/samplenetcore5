using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Lmg
{
    [Table("tb_pegawai", Schema = "lmg")]
    public class TbPegawai
    {
        [Key, Required]
        [Column("id_pegawai")]
        public int IdPegawai { get; set; }
        [Column("id_jenis_jabatan")]
        public int IdJenisJabatan { get; set; }
        [Column("id_jenis_pegawai")]
        public int IdJenisPegawai { get; set; }
        [Column("nip")]
        public string Nip { get; set; }
        [Column("nik")]
        public string Nik { get; set; }
        [Column("tempat_lahir")]
        public string TempatLahir { get; set; }
        [Column("tanggal_lahir")]
        public DateTime TanggalLahir { get; set; }
        [Column("kd_jenis_kelamin")]
        public string KdJenisKelamin { get; set; }
        [Column("id_agama")]
        public int IdAgama { get; set; }
        [Column("id_jenis_jenjang_pendidikan")]
        public int IdJenisJenjangPendidikan { get; set; }
        [Column("nama_institusi_pendidikan")]
        public string NamaInstitusiPendidikan { get; set; }
        [Column("nama_pasangan")]
        public string NamaPasangan { get; set; }
        [Column("no_darurat")]
        public string NoDarurat { get; set; }
        [Column("no_kk")]
        public string NoKk { get; set; }
        [Column("jurusan_pendidikan")]
        public string JurusanPendidikan { get; set; }
        [Column("aktifitas_dakwah")]
        public string AktifitasDakwah { get; set; }
        [Column("organisasi_masyarakat")]
        public string OrganisasiMasyarakat { get; set; }
        [Column("pengalaman_kerja")]
        public string PengalamanKerja { get; set; }
        [Column("kompetensi")]
        public string Kompetensi { get; set; }
        [Column("npwp")]
        public string Npwp { get; set; }
        [Column("facebook")]
        public string Facebook { get; set; }
        [Column("twitter")]
        public string Twitter { get; set; }
        [Column("instagram")]
        public string Instagram { get; set; }
        [Column("youtube")]
        public string Youtube { get; set; }
        [Column("sambutan")]
        public string Sambutan { get; set; }
        [Column("status_kawin")]
        public int StatusKawin { get; set; }
        [Column("jumlah_anak")]
        public int JumlahAnak { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("gelar")]
        public string Gelar { get; set; }
        [Column("tanggal_masuk")]
        public DateTime TanggalMasuk { get; set; }
        [Column("id_golongan_pegawai")]
        public int IdGolonganPegawai { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
