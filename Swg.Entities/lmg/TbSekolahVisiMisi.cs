using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Lmg
{
    [Table("tb_sekolah_visi_misi", Schema = "lmg")]
    public class TbSekolahVisiMisi
    {
        [Key]
        [Column("id_sekolah_visi_misi")]
        public int IdSekolahVisiMisi { get; set; }
        [Column("id_jenis_visi_misi")]
        public int IdJenisVisiMisi { get; set; }
        [Column("id_sekolah")]
        public int IdSekolah { get; set; }
        [Column("file_ikon")]
        public string FileIkon { get; set; }
        [Column("judul")]
        public string Judul { get; set; }
        [Column("konten")]
        public string Konten { get; set; }
    }
}
