using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Lmg
{
    [Table("tb_sekolah", Schema = "lmg")]
    public class TbSekolah
    {
        [Key]
        [Column("id_sekolah")]
        public int IdSekolah { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
        [Column("alamat")]
        public string Alamat { get; set; }
        [Column("kode_pos")]
        public string KodePos { get; set; }
        [Column("desa")]
        public string Desa { get; set; }
        [Column("kecamatan")]
        public string Kecamatan { get; set; }
        [Column("kabupaten")]
        public string Kabupaten { get; set; }
        [Column("provinsi")]
        public string Provinsi { get; set; }
        [Column("no_telepon")]
        public string NoTelepon { get; set; }
        [Column("no_faximili")]
        public string NoFaximili { get; set; }
        [Column("email")]
        public string Email { get; set; }
        [Column("web")]
        public string Web { get; set; }
        [Column("moto")]
        public string Moto { get; set; }
        [Column("logo")]
        public string Logo { get; set; }
        [Column("icon")]
        public string Icon { get; set; }
        [Column("foto_web")]
        public string FotoWeb { get; set; }
        [Column("video_web")]
        public string VideoWeb { get; set; }
        [Column("facebook")]
        public string Facebook { get; set; }
        [Column("twitter")]
        public string Twitter { get; set; }
        [Column("instagram")]
        public string Instagram { get; set; }
        [Column("youtube")]
        public string Youtube { get; set; }
        [Column("profile")]
        public string Profile { get; set; }
        [Column("video")]
        public string Video { get; set; }

    }
}
