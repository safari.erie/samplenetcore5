using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Lmg
{
    [Table("tb_unit_ekskul", Schema = "lmg")]
    public class TbUnitEkskul
    {
        [Key]
        [Column("id_unit_ekskul")]
        public int IdUnitEkskul { get; set; }
        [Column("id_unit")]
        public int IdUnit { get; set; }
        [Column("judul")]
        public string Judul { get; set; }
        [Column("deskripsi")]
        public string Deskripsi { get; set; }
        [Column("file_image")]
        public string FileImage { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
