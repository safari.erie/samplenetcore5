using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Lmg
{
    [Table("tb_unit", Schema = "lmg")]
    public class TbUnit
    {
        [Key]
        [Column("id_unit")]
        public int IdUnit { get; set; }
        [Column("id_sekolah")]
        public int IdSekolah { get; set; }
        [Column("nspn")]
        public string Nspn { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_singkat")]
        public string NamaSingkat { get; set; }
        [Column("alamat")]
        public string Alamat { get; set; }
        [Column("kode_pos")]
        public string KodePos { get; set; }
        [Column("desa")]
        public string Desa { get; set; }
        [Column("kecamatan")]
        public string Kecamatan { get; set; }
        [Column("kabupaten")]
        public string Kabupaten { get; set; }
        [Column("provinsi")]
        public string Provinsi { get; set; }
        [Column("no_telepon")]
        public string NoTelepon { get; set; }
        [Column("no_faximili")]
        public string NoFaximili { get; set; }
        [Column("email")]
        public string Email { get; set; }
        [Column("web")]
        public string Web { get; set; }
        [Column("status_unit")]
        public string StatusUnit { get; set; }
        [Column("waktu_penyelenggaraan")]
        public string WaktuPenyelenggaraan { get; set; }
        [Column("kurikulum")]
        public string Kurikulum { get; set; }
        [Column("naungan")]
        public string Naungan { get; set; }
        [Column("no_sk_pendirian")]
        public string NoSkPendirian { get; set; }
        [Column("tanggal_sk_pendirian")]
        public DateTime TanggalSkPendirian { get; set; }
        [Column("no_sk_operasional")]
        public string NoSkOperasional { get; set; }
        [Column("tanggal_sk_operasional")]
        public DateTime TanggalSkOperasional { get; set; }
        [Column("jenis_akreditasi")]
        public string JenisAkreditasi { get; set; }
        [Column("no_sk_akreditasi")]
        public string NoSkAkreditasi { get; set; }
        [Column("tanggal_sk_akreditasi")]
        public DateTime TanggalSkAkreditasi { get; set; }
        [Column("no_sertifikat_iso")]
        public string NoSertifikatIso { get; set; }
        [Column("nama_yayasan")]
        public string NamaYayasan { get; set; }
        [Column("luas_tanah")]
        public double LuasTanah { get; set; }
        [Column("luas_bangunan")]
        public double LuasBangunan { get; set; }
        [Column("provider_internet")]
        public string ProviderInternet { get; set; }
        [Column("bandwidth_internet")]
        public int BandwidthInternet { get; set; }
        [Column("sumber_listrik")]
        public string SumberListrik { get; set; }
        [Column("daya_listrik")]
        public int DayaListrik { get; set; }
        [Column("moto")]
        public string Moto { get; set; }
        [Column("logo")]
        public string Logo { get; set; }
        [Column("slider_banner")]
        public string SliderBanner { get; set; }
        [Column("slider_image")]
        public string SliderImage { get; set; }
        [Column("slider_judul")]
        public string SliderJudul { get; set; }
        [Column("slider_keterangan")]
        public string SliderKeterangan { get; set; }
        [Column("thumbnail")]
        public string Thumbnail { get; set; }
        [Column("thumbnail_besar")]
        public string ThumbnailBesar { get; set; }
        [Column("profile")]
        public string Profile { get; set; }
        [Column("struktur_organisasi")]
        public string StrukturOrganisasi { get; set; }
        [Column("visi_misi")]
        public string VisiMisi { get; set; }
    }
}
