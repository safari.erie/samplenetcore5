using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Lmg
{
    [Table("tb_sekolah_fasilitas", Schema = "lmg")]
    public class TbSekolahFasilitas
    {
        [Key]
        [Column("id_sekolah_fasilitas")]
        public int IdSekolahFasilitas { get; set; }
        [Column("judul")]
        public string Judul { get; set; }
        [Column("deskripsi")]
        public string Deskripsi { get; set; }
        [Column("file_image")]
        public string FileImage { get; set; }
    }
}
