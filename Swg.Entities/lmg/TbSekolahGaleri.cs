using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Lmg
{
    [Table("tb_sekolah_galeri", Schema = "lmg")]
    public class TbSekolahGaleri
    {
        [Key]
        [Column("id_sekolah_galeri")]
        public int IdSekolahGaleri { get; set; }
        [Column("id_sekolah")]
        public int IdSekolah { get; set; }
        [Column("id_jenis_galeri")]
        public int IdJenisGaleri { get; set; }
        [Column("judul")]
        public string Judul { get; set; }
        [Column("keterangan")]
        public string Keterangan { get; set; }
        [Column("file_url")]
        public string FileUrl { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
    }
}
