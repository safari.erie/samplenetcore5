using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Lmg
{
    [Table("tb_testimoni", Schema = "lmg")]
    public class TbTestimoni
    {
        [Key]
        [Column("id_testimoni")]
        public int IdTestimoni { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("sebagai")]
        public string Sebagai { get; set; }
        [Column("pesan")]
        public string Pesan { get; set; }
        [Column("foto_user")]
        public string FotoUser { get; set; }
        [Column("url")]
        public string Url { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
