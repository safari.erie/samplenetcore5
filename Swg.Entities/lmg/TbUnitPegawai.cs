using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Lmg
{
    [Table("tb_unit_pegawai", Schema = "lmg")]
    public class TbUnitPegawai
    {
        [Key, Required]
        [Column("id_unit")]
        public int IdUnit { get; set; }
        [Key, Required]
        [Column("id_pegawai")]
        public int IdPegawai { get; set; }
    }
}
