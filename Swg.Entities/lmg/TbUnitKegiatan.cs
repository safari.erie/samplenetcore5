using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Lmg
{
    [Table("tb_unit_kegiatan", Schema = "lmg")]
    public class TbUnitKegiatan
    {
        [Key]
        [Column("id_unit_kegiatan")]
        public int IdUnitKegiatan { get; set; }
        [Column("id_unit")]
        public int IdUnit { get; set; }
        [Column("judul")]
        public string Judul { get; set; }
        [Column("keterangan")]
        public string Keterangan { get; set; }
        [Column("file_image")]
        public string FileImage { get; set; }
        [Column("url")]
        public string Url { get; set; }
        [Column("jenis")]
        public int Jenis { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
