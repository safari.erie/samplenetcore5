using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Lmg
{
    [Table("tb_unit_galeri", Schema = "lmg")]
    public class TbUnitGaleri
    {
        [Key]
        [Column("id_unit_galeri")]
        public int IdUnitGaleri { get; set; }
        [Column("id_unit")]
        public int IdUnit { get; set; }
        [Column("id_jenis_galeri")]
        public int IdJenisGaleri { get; set; }
        [Column("judul")]
        public string Judul { get; set; }
        [Column("keterangan")]
        public string Keterangan { get; set; }
        [Column("file_url")]
        public string FileUrl { get; set; }
        [Column("jenis")]
        public int Jenis { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
