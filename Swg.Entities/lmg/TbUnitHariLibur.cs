using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Lmg
{
    [Table("tb_unit_hari_libur", Schema = "lmg")]
    public class TbUnitHariLibur
    {
        [Key, Required]
        [Column("id_unit")]
        public int IdUnit { get; set; }
        [Key, Required]
        [Column("id_jenis_hari_libur")]
        public int IdJenisHariLibur { get; set; }
    }
}
