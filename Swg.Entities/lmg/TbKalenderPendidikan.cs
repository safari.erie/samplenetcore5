using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Lmg
{
    [Table("tb_kalender_pendidikan", Schema = "lmg")]
    public class TbKalenderPendidikan
    {
        [Key]
        [Column("id_kalender_pendidikan")]
        public int IdKalenderPendidikan { get; set; }
        [Column("semester")]
        public int Semester { get; set; }
        [Column("keterangan")]
        public string Keterangan { get; set; }
        [Column("file_image")]
        public string FileImage { get; set; }
    }
}
