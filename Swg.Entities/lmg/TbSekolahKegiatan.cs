using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Lmg
{
    [Table("tb_sekolah_kegiatan", Schema = "lmg")]
    public class TbSekolahKegiatan
    {
        [Key]
        [Column("id_sekolah_kegiatan")]
        public int IdSekolahKegiatan { get; set; }
        [Column("id_sekolah")]
        public int IdSekolah { get; set; }
        [Column("judul")]
        public string Judul { get; set; }
        [Column("keterangan")]
        public string Keterangan { get; set; }
        [Column("url")]
        public string Url { get; set; }
        [Column("file_image")]
        public string FileImage { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
    }
}
