using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.Lmg
{
    [Table("tb_sekolah_prestasi", Schema = "lmg")]
    public class TbSekolahPrestasi
    {
        [Key]
        [Column("id_sekolah_prestasi")]
        public int IdSekolahPrestasi { get; set; }
        [Column("id_jenis_galeri")]
        public int IdJenisGaleri { get; set; }
        [Column("judul")]
        public string Judul { get; set; }
        [Column("keterangan")]
        public string Keterangan { get; set; }
        [Column("file_url")]
        public string FileUrl { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdateBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
