using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.His
{
    [Table("ta_kbm_jawaban", Schema = "his")]
    public class TaKbmJawaban
    {
        [Key]
        [Column("id_ta_kbm_jawaban")]
        public int IdTaKbmJawaban { get; set; }
        [Column("id_kbm_materi")]
        public int IdKbmMateri { get; set; }
        [Column("id_siswa")]
        public int IdSiswa { get; set; }
        [Column("id_jenis_soal")]
        public int IdJenisSoal { get; set; }
        [Column("id_jenis_bantuan_ortu")]
        public int IdJenisBantuanOrtu { get; set; }
        [Column("hadir")]
        public int Hadir { get; set; }
        [Column("nama_url")]
        public string NamaUrl { get; set; }
        [Column("nama_file")]
        public string NamaFile { get; set; }
        [Column("nilai_angka")]
        public double? NilaiAngka { get; set; }
        [Column("nilai_huruf")]
        public string NilaiHuruf { get; set; }
        [Column("is_remedial")]
        public int IsRemedial { get; set; }
        [Column("catatan")]
        public string Catatan { get; set; }
        [Column("created_by")]
        public int? CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime? CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
