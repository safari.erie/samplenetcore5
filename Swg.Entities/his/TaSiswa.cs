using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.His
{
    [Table("ta_siswa", Schema = "his")]
    public class TaSiswa
    {
        [Key, Required]
        [Column("id_tahun_ajaran")]
        public int IdTahunAjaran { get; set; }
        [Key, Required]
        [Column("nis")]
        public string Nis { get; set; }
        [Column("nisn")]
        public string Nisn { get; set; }
        [Column("nik")]
        public string Nik { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("nama_panggilan")]
        public string NamaPanggilan { get; set; }
        [Column("tempat_lahir")]
        public string TempatLahir { get; set; }
        [Column("tanggal_lahir")]
        public DateTime TanggalLahir { get; set; }
        [Column("agama")]
        public string Agama { get; set; }
        [Column("kd_jenis_kelamin")]
        public string KdJenisKelamin { get; set; }
        [Column("no_handphone")]
        public string NoHandphone { get; set; }
        [Column("no_darurat")]
        public string NoDarurat { get; set; }
        [Column("email")]
        public string Email { get; set; }
        [Column("alamat_tinggal")]
        public string AlamatTinggal { get; set; }
        [Column("alamat_ortu")]
        public string AlamatOrtu { get; set; }
        [Column("nik_ibu")]
        public string NikIbu { get; set; }
        [Column("nama_ibu")]
        public string NamaIbu { get; set; }
        [Column("jenis_pekerjaan_ibu")]
        public string JenisPekerjaanIbu { get; set; }
        [Column("nama_instansi_ibu")]
        public string NamaInstansiIbu { get; set; }
        [Column("nik_ayah")]
        public string NikAyah { get; set; }
        [Column("nama_ayah")]
        public string NamaAyah { get; set; }
        [Column("jenis_pekerjaan_ayah")]
        public string JenisPekerjaanAyah { get; set; }
        [Column("nama_instansi_ayah")]
        public string NamaInstansiAyah { get; set; }
        [Column("email_ortu")]
        public string EmailOrtu { get; set; }
        [Column("nis_lama")]
        public string NisLama { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("password")]
        public string Password { get; set; }
        [Column("pin")]
        public string Pin { get; set; }
        [Column("unit")]
        public string Unit { get; set; }
        [Column("kelas")]
        public string Kelas { get; set; }
        [Column("kelas_paralel")]
        public string KelasParalel { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
        [Column("archived_by")]
        public int ArchivedBy { get; set; }
        [Column("archived_date")]
        public DateTime ArchivedDate { get; set; }
    }
}
