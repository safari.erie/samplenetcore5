using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.His
{
    [Table("ta_kelas_quran_siswa", Schema = "his")]
    public class TaKelasQuranSiswa
    {
        [Key]
        [Column("id_ta_kelas_quran_siswa")]
        public int IdTaKelasQuranSiswa { get; set; }
        [Column("id_tahun_ajaran")]
        public int IdTahunAjaran { get; set; }
        [Column("nis")]
        public string Nis { get; set; }
        [Column("nama_kelas_quran")]
        public string NamaKelasQuran { get; set; }
        [Column("nama_pegawai")]
        public string NamaPegawai { get; set; }
        [Column("archived_by")]
        public int ArchivedBy { get; set; }
        [Column("archived_date")]
        public DateTime ArchivedDate { get; set; }
    }
}
