using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.His
{
    [Table("ta_siswa_prestasi", Schema = "his")]
    public class TaSiswaPrestasi
    {
        [Key]
        [Column("id_ta_siswa_prestasi")]
        public int IdTaSiswaPrestasi { get; set; }
        [Column("id_tahun_ajaran")]
        public int IdTahunAjaran { get; set; }
        [Column("nis")]
        public string Nis { get; set; }
        [Column("jenis_prestasi")]
        public string JenisPrestasi { get; set; }
        [Column("tahun")]
        public int Tahun { get; set; }
        [Column("jenis_tingkat_prestasi")]
        public string JenisTingkatPrestasi { get; set; }
        [Column("nama_penyelenggara")]
        public string NamaPenyelenggara { get; set; }
        [Column("keterangan")]
        public string Keterangan { get; set; }
        [Column("file_sertifikat")]
        public string FileSertifikat { get; set; }
    }
}
