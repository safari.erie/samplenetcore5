using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.His
{
    [Table("ta_kelas_quran_progres", Schema = "his")]
    public class TaKelasQuranProgres
    {
        [Key]
        [Column("id_ta_kelas_quran_progres")]
        public int IdTaKelasQuranProgres { get; set; }
        [Column("id_tahun_ajaran")]
        public int IdTahunAjaran { get; set; }
        [Column("nis")]
        public string Nis { get; set; }
        [Column("nama_kelas_quran")]
        public string NamaKelasQuran { get; set; }
        [Column("tanggal")]
        public DateTime Tanggal { get; set; }
        [Column("jenis_jilid_quran")]
        public string JenisJilidQuran { get; set; }
        [Column("halaman")]
        public int Halaman { get; set; }
        [Column("catatan")]
        public string Catatan { get; set; }
        [Column("created_by")]
        public int? CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime? CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
        [Column("archived_by")]
        public int ArchivedBy { get; set; }
        [Column("archived_date")]
        public DateTime ArchivedDate { get; set; }
    }
}
