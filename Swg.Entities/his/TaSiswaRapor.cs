using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.His
{
    [Table("ta_siswa_rapor", Schema = "his")]
    public class TaSiswaRapor
    {
        [Key, Required]
        [Column("id_tahun_ajaran")]
        public int IdTahunAjaran { get; set; }
        [Key, Required]
        [Column("nis")]
        public string Nis { get; set; }
        [Key, Required]
        [Column("jenis_rapor")]
        public string JenisRapor { get; set; }
        [Column("file_rapor")]
        public string FileRapor { get; set; }
    }
}
