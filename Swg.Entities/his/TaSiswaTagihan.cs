using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Entities.His
{
    [Table("ta_siswa_tagihan", Schema = "his")]
    public class TaSiswaTagihanx
    {
        [Key, Required]
        [Column("id_tahun_ajaran")]
        public int IdTahunAjaran { get; set; }
        [Key, Required]
        [Column("nis")]
        public string Nis { get; set; }
        [Key, Required]
        [Column("periode")]
        public DateTime Periode { get; set; }
        [Key, Required]
        [Column("jenis_tagihan_siswa")]
        public string JenisTagihanSiswa { get; set; }
        [Column("rp")]
        public double Rp { get; set; }
    }
}
