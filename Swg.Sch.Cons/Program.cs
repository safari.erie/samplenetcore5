﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Swg.Sch.Cons
{
    class Program
    {
        class TestWaktu
        {
            public DateTime Tanggal { get; set; }
            public TimeSpan JamAwal { get; set; }
            public TimeSpan JamAkhir { get; set; }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            List<TestWaktu> Jams = new List<TestWaktu>();
            Jams.Add(new TestWaktu
            {
                Tanggal = new DateTime(2020, 10, 10),
                JamAwal = new TimeSpan(9, 30, 0),
                JamAkhir = new TimeSpan(10, 0, 0)
            });
            Jams.Add(new TestWaktu
            {
                Tanggal = new DateTime(2020, 10, 11),
                JamAwal = new TimeSpan(9, 0, 0),
                JamAkhir = new TimeSpan(9, 30, 0)
            });
            Jams.Add(new TestWaktu
            {
                Tanggal = new DateTime(2020, 10, 11),
                JamAwal = new TimeSpan(9, 30, 0),
                JamAkhir = new TimeSpan(10, 0, 0)
            });
            Jams.Add(new TestWaktu
            {
                Tanggal = new DateTime(2020, 10, 10),
                JamAwal = new TimeSpan(9, 0, 0),
                JamAkhir = new TimeSpan(9, 30, 0)
            });

            string tanggal = "";
            string jamAwal = "";
            string jamAkhir = "";
            foreach (var item in Jams)
            {
                tanggal = item.Tanggal.ToString("dd-MM-yyyy");
                jamAwal = item.JamAwal.ToString("hh':'mm");
                jamAkhir = item.JamAkhir.ToString("hh':'mm");
                Console.WriteLine(string.Format("{0} - {1} - {2}", tanggal, jamAwal, jamAkhir));

            }
            Console.WriteLine("==============");
            tanggal = "";
            jamAwal = "";
            jamAkhir = "";
            foreach (var item in Jams.OrderBy(x=>x.Tanggal).ThenBy(x=>x.JamAwal))
            {
                tanggal = item.Tanggal.ToString("dd-MM-yyyy");
                jamAwal = item.JamAwal.ToString("hh':'mm");
                jamAkhir = item.JamAkhir.ToString("hh':'mm");
                break;
            }
            Console.WriteLine(string.Format("{0} - {1} - {2}", tanggal, jamAwal, jamAkhir));
            Console.ReadLine();
        }
    }
}
