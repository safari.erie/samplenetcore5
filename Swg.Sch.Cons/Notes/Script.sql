CREATE SCHEMA prt
    AUTHORIZATION postgres;
CREATE TABLE prt.tb_partner
(
    id_user integer NOT NULL,
    kd_partner character varying(32) COLLATE pg_catalog."default" NOT NULL,
    password character varying(128) COLLATE pg_catalog."default" NOT NULL,
    partner_name character varying(64) COLLATE pg_catalog."default" NOT NULL,
    partner_ip character varying(64) COLLATE pg_catalog."default" NOT NULL,
    is_login integer NOT NULL,
    last_login timestamp without time zone,
    CONSTRAINT pk_partner PRIMARY KEY (id_user),
    CONSTRAINT uq_partner UNIQUE (kd_partner),
    CONSTRAINT fk_partner2user FOREIGN KEY (id_user)
        REFERENCES public.tb_user (id_user) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE prt.tb_partner
    OWNER to postgres;

INSERT INTO prt.tb_partner (id_user, kd_partner, password, partner_name, partner_ip, is_login, last_login) VALUES (1, '20001', 'ViDaiK/K5eFXwqEif++YktxF+DFi48wgwGx5fTs+XEc=', 'Bank BSM', '127.0.0.0', 0, NULL);

CREATE TABLE prt.tb_partner_request
(
    id_partner_request serial NOT NULL,
    kd_partner character varying(32) COLLATE pg_catalog."default" NOT NULL,
    method_name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    in_date timestamp without time zone NOT NULL,
    response_status character varying(50) COLLATE pg_catalog."default" NOT NULL,
    response_message character varying(50) COLLATE pg_catalog."default",
    response_result json,
    CONSTRAINT pk_partner_request PRIMARY KEY (id_partner_request)
)

TABLESPACE pg_default;

ALTER TABLE prt.tb_partner_request
    OWNER to postgres;

CREATE TABLE prt.tb_partner_request_detil
(
    id_partner_request_detil serial NOT NULL,
    id_partner_request integer NOT NULL,
    param_name character varying(30) COLLATE pg_catalog."default",
    param_value character varying(100) COLLATE pg_catalog."default",
    CONSTRAINT pk_partner_request_detil PRIMARY KEY (id_partner_request_detil)
)

TABLESPACE pg_default;

ALTER TABLE prt.tb_partner_request_detil
    OWNER to postgres;