﻿using System;
namespace Swg.Sch.Adm.Models
{
    public class TestPpdbSiswaModel
    {
        public int IdPpdbKelas { get; set; }
        public int? IdSiswa { get; set; }
        public int? IdSiswaSaudara { get; set; }
        public int IdJenisKategoriPendaftaran { get; set; }
        public int IdJenisPendaftaran { get; set; }
        public int IdJalurPendaftaran { get; set; }
        public string NikSiswa { get; set; }
        public string Nisn { get; set; }
        public string Nama { get; set; }
        public string NamaPanggilan { get; set; }
        public string TempatLahir { get; set; }
        public string TanggalLahir { get; set; }
        public int IdAgama { get; set; }
        public int IdKewarganegaraan { get; set; }
        public string KdJenisKelamin { get; set; }
        public string KdGolonganDarah { get; set; }
        public string AlamatTinggal { get; set; }
        public int IdTinggal { get; set; }
        public string Email { get; set; }
        public string NoHandphone { get; set; }
        public string NoDarurat { get; set; }
        public int IdBahasa { get; set; }
        public int TinggiBadan { get; set; }
        public int BeratBadan { get; set; }
        public int AnakKe { get; set; }
        public int JumlahSodaraKandung { get; set; }
        public int JumlahSodaraTiri { get; set; }
        public int JumlahSodaraAngkat { get; set; }
        public int IdTransportasi { get; set; }
        public int JarakKeSekolah { get; set; }
        public double WaktuTempuh { get; set; }
        public string PenyakitDiderita { get; set; }
        public string KelainanJasmani { get; set; }
        public string SekolahAsal { get; set; }
        public string AlamatSekolahAsal { get; set; }
        public string NspnSekolahAsal { get; set; }
        public int IdPpdbSeragam { get; set; }
    }
    public class TestPpdbOrtuModel
    {
        public int IdTipe { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public string Nik { get; set; }
        public int IdJenisPekerjaan { get; set; }
        public string NamaInstansi { get; set; }
        public string Jabatan { get; set; }
        public string Email { get; set; }
        public string NoHandphone { get; set; }
        public string NoTelpRumah { get; set; }
        public int IdAgama { get; set; }
        public int IdKewarganegaraan { get; set; }
        public int IdJenjangPendidikan { get; set; }
        public int IdStatusPenikahan { get; set; }
        public string TempatLahir { get; set; }
        public string TanggalLahir { get; set; }
        public int IdStatusHidup { get; set; }
        public string TanggalMeninggal { get; set; }
    }
    public class TestPpdbPrestasiModel
    {
        public int IdJenisPrestasi { get; set; }
        public int Tahun { get; set; }
        public int IdJenisTingkatPrestasi { get; set; }
        public string NamaPenyelenggara { get; set; }
        public string Keterangan { get; set; }
    }
    public class TestPpdbRaportModel
    {
        public string Mapel { get; set; }
        public int Nilai { get; set; }
        public int Semester { get; set; }
    }
}
