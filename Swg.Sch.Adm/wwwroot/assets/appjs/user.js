﻿/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 14042020
================================================================================================================= */
// $("input[name='UsernameOrEmail']").val(sessionStorage.getItem("AuthUsername"));
// $("#SayHello").html("Hi " + sessionStorage.getItem("AuthFirstName"));

$(document).ready(function () {
    let session = sessionStorage.getItem("DataUser");
    if (session != null) {
        GetKelasParalelPegawai();
        GetKompetensiPegawai();
        $('[data-toggle="datepicker"]').mask('00-00-0000');
    }
})


function checkRolesPpdb(Roles) {
    var array1 = Roles;
    var array2 = [1, 5];
    array2 = array2.filter(function (item) {
        return array1.includes(item) ? true : false;
    });
    if (array2 == "") {
        $('#NavFormCariPpdb').fadeOut();
        $('#NavIconCariPpdb').fadeOut();
        $('#NavDownloadTemplate').fadeOut();
        $('#NavDownloadPpdb').fadeOut();
    } else {
        $('#NavFormCariPpdb').css('display', 'block');
        $('#NavIconCariPpdb').css('display', 'block');
        $('#NavDownloadTemplate').css('display', 'block');
        $('#NavDownloadPpdb').css('display', 'block');
    }
}

function UrlExists(url) {
    $.get(url)
        .done(function () {
            $('#InfoFotoProfile').hide();
            // exists code 
        }).fail(function () {
            $('#InfoFotoProfile').show();
            // not exists code
        })
}

$.getJSON(base_url + "/Users/GetUserUpdateProfile", (res) => {
    var data = res.Data;
    var roles = [];
    $.each(data.Roles, function (i, v) {
        roles.push(
            v.IdRole
        );
    });
    checkRolesPpdb(roles)
    $("#ThumbnailImage").removeAttr("src").attr("src", base_url + "/Asset/Files/Appl/User/" + data.ProfileFile);
    $("#PreviewFotoGuru").removeAttr("src").attr("src", base_url + "/Asset/Files/Appl/User/" + data.ProfileFile);

    UrlExists(base_url + "/Asset/Files/Appl/User/" + data.ProfileFile);
})

function ResetPassword() {
    $("#ModalFormResetPassword").modal("show");
}

function GantiPassword() {
    $("#ModalFormGantiPassword").modal("show");
    var Url = base_url + "/Users/GetUserUpdateProfile";
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var ResponseData = responsesuccess.Data;
                var FullName = (ResponseData.FirstName + " " + ResponseData.MiddleName + " " + ResponseData.LastName).toLowerCase().replace(/\b[a-z]/g, function (letter) {
                    return letter.toUpperCase();
                });
                $("#TitleFormGantiPassword").html("Ganti Password - " + SetFullName(ResponseData.FirstName, ResponseData.MiddleName, ResponseData.LastName));

            } else {
                swal({
                    title: 'Gagal :(',
                    text: responsesuccess.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function UpdateProfile() {
    $("#ModalFormUpdateProfile").modal("show");
    $('#FormUpdateProfile')[0].reset();
    $("#BtnSaveUpdateProfile").attr("onClick", "SaveUpdateProfile();");

    var Url = base_url + "/Users/GetUserUpdateProfile";
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var ResponseData = responsesuccess.Data;
                var FullName = (ResponseData.FirstName + " " + ResponseData.MiddleName + " " + ResponseData.LastName).toLowerCase().replace(/\b[a-z]/g, function (letter) {
                    return letter.toUpperCase();
                });
                $("#TitleFormUpdateProfile").html("Edit Profil - " + SetFullName(ResponseData.FirstName, ResponseData.MiddleName, ResponseData.LastName));
                $("input[name='Email']", "#FormUpdateProfile").val(ResponseData.Email);
                $("input[name='FirstName']", "#FormUpdateProfile").val(ResponseData.FirstName);
                $("input[name='MiddleName']", "#FormUpdateProfile").val(ResponseData.MiddleName);
                $("input[name='LastName']", "#FormUpdateProfile").val(ResponseData.LastName);
                $("textarea[name='Address']", "#FormUpdateProfile").val(ResponseData.Address);
                $("input[name='PhoneNumber']", "#FormUpdateProfile").val(ResponseData.PhoneNumber);
                $("input[name='MobileNumber']", "#FormUpdateProfile").val(ResponseData.MobileNumber);
                
                $("#ThumbnailImage", "#FormUpdateProfile").removeAttr("src").attr("src", base_url + "/FotoProfile/" + ResponseData.ProfileFile);
            } else {
                swal({
                    title: 'Gagal :(',
                    text: responsesuccess.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });

    var UrlPegawai = base_url + "/api/Pegawais/GetListPegawai";
    $.ajax({
        url: UrlPegawai,
        type: "GET",
        dataType: "json",
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {

                var ResponseData = responsesuccess.Data;
                var i = 0;
                var Jum = (ResponseData).length;
                for (i; i < Jum; i++) {
                    if (ResponseData[i].IdPegawai == JSON.parse(localStorage.getItem("DataUser")).IdUser) {
                        $('input[name="IdUnit"]', "#FormUpdateProfile").val(ResponseData[i].IdUnit);
                        $('input[name="Gelar"]', "#FormUpdateProfile").val(ResponseData[i].Gelar);
                        $('input[name="Nip"]', "#FormUpdateProfile").val(ResponseData[i].Nip);
                        $('input[name="IdJabatan"]', "#FormUpdateProfile").val(ResponseData[i].IdJabatan);
                        $('input[name="IdJenisPegawai"]', "#FormUpdateProfile").val(ResponseData[i].IdJenisPegawai);

                        $('input[name="IdPegawai"]', "#FormUpdateProfile").val(ResponseData[i].IdPegawai);
                        $('input[name="Nik"]', "#FormUpdateProfile").val(ResponseData[i].Nik);
                        $('input[name="TempatLahir"]', "#FormUpdateProfile").val(ResponseData[i].TempatLahir);
                        $('input[name="TanggalLahir"]', "#FormUpdateProfile").val(ResponseData[i].TanggalLahir);
                        ComboGetListJenjangPendidikan(function (obj) {
                            $('select#IdJenjangPendidikan', "#FormUpdateProfile").html(obj);
                        }, ResponseData[i].IdJenjangPendidikan);
                        ComboGetAgama(function (obj) {
                            $('select#IdAgama', "#FormUpdateProfile").html(obj);
                        }, ResponseData[i].IdAgama);
                        ComboGetJenisKelamin(function (obj) {
                            $("select[name='KdJenisKelamin']", "#FormUpdateProfile").html(obj);
                        }, ResponseData[i].KdJenisKelamin);
                        $('input[name="NamaInstitusiPendidikan"]', "#FormUpdateProfile").val(ResponseData[i].NamaInstitusiPendidikan);
                        $('input[name="NamaPasangan"]', "#FormUpdateProfile").val(ResponseData[i].NamaPasangan);
                        $('input[name="NoDarurat"]', "#FormUpdateProfile").val(ResponseData[i].NoDarurat);

                        const KelasDiampu = [];
                        $.each(ResponseData[i].KelasDiampu, (i, v) => {
                            KelasDiampu.push(
                                `${v.IdKelasParalel}`
                            );
                        })

                        const Kompetensi = [];
                        $.each(ResponseData[i].Kompetensi, (i, v) => {
                            Kompetensi.push(
                                `${v.IdKompetensi}`
                            );
                        })

                        $('select[name="IdKelasParalel[]"]', '#FormUpdateProfile').val(KelasDiampu).trigger('change');
                        $('select[name="IdKompetensi[]"]', '#FormUpdateProfile').val(Kompetensi).trigger('change');
                        $('input[name="NoKk"]', "#FormUpdateProfile").val(ResponseData[i].NoKk);
                        $('input[name="JurusanPendidikan"]', "#FormUpdateProfile").val(ResponseData[i].JurusanPendidikan);
                        $('textarea[name="AktifitasDakwah"]', "#FormUpdateProfile").val(ResponseData[i].AktifitasDakwah);
                        $('textarea[name="OrganisasiMasyarakat"]', "#FormUpdateProfile").val(ResponseData[i].OrganisasiMasyarakat);
                        $('textarea[name="PengalamanKerja"]', "#FormUpdateProfile").val(ResponseData[i].PengalamanKerja);
                        $('input[name="Npwp"]', "#FormUpdateProfile").val(ResponseData[i].Npwp);
                        $("input[name='Facebook']", "#FormUpdateProfile").val(ResponseData[i].Facebook);
                        $("input[name='Twitter']", "#FormUpdateProfile").val(ResponseData[i].Twitter);
                        $("input[name='Instagram']", "#FormUpdateProfile").val(ResponseData[i].Instagram);
                        $("input[name='Youtube']", "#FormUpdateProfile").val(ResponseData[i].Youtube);

                        ComboGetJenisStatusNikah(function (obj) {
                            $("select[name='StatusKawin']", '#FormUpdateProfile').html(obj);
                        }, ResponseData[i].StatusKawin);
                        $('select[name="JumlahAnak"]', '#FormUpdateProfile').val(ResponseData[i].JumlahAnak).trigger('change');


                    }
                }
            } else {
                swal({
                    title: 'Gagal :(',
                    text: responsesuccess.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}


$("input[type='file']", "#FormUpdateProfile").on("change", function () {
    if (this.files[0].size > 5000000) {
        swal({
            title: 'Gagal',
            text: 'Maksimal ukuran foto 5MB !',
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
        $(this).val('');
    }
});

function SaveUpdateProfile() {
    var IsSuccess = false;
    var TypeInput = true;
    $('[data-input-profile="wajib"]').each(function (i, v) {
        if ($(this).val() == "") {
            TypeInput = false;
            $('#FormRegister').addClass('was-validated');
            swal({
                title: 'Gagal',
                text:  $(this).attr("placeholder"),
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
            return i < 0;
        }
    })
    if (TypeInput) {
        
        var form = new FormData($('#FormUpdateProfile')[0]);
        $('select[name^="IdKelasParalel[]"] option:selected', '#FormUpdateProfile').each(function (i, v) {
            form.append('KelasDiampu[' + i + '].IdKelasParalel', v.value);
        });
        $('select[name^="IdKompetensi[]"] option:selected', '#FormUpdateProfile').each(function (i, v) {
            form.append('Kompetensi[' + i + '].IdKompetensi', v.value);
        });

        var Url = base_url + "/Users/UpdateProfile";
        $.ajax({
            url: Url,
            method: "POST",
            dataType: "json",
            data: form,
            contentType: false,
            cache: true,
            processData: false,
            // beforeSend: function (before) {
            //     ProgressBar("wait");
            // },
            success: function (responsesave) {
                if (responsesave.IsSuccess == true) {
                    IsSuccess = true;
                } else if (responsesave.IsSuccess == false) {
                    IsSuccess = false;
                    swal({
                        title: 'Gagal Edit Profil',
                        text: responsesave.ReturnMessage,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            },
            error: function (errorresponse) {
                swal({
                    title: 'Gagal Edit Profil',
                    text: errorresponse,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
                IsSuccess = false;
            }
        });
        setTimeout(() => {
                
            if (IsSuccess) {
                var UrlEditPegawai = base_url + "/api/Pegawais/PegawaiEdit";
                $.ajax({
                    url: UrlEditPegawai,
                    method: "POST",
                    dataType: "json",
                    data: form,
                    contentType: false,
                    cache: true,
                    processData: false,
                    beforeSend: function (before) {
                        ProgressBar("wait");
                    },
                    success: function (responsesave) {
                        ProgressBar("success");
                        if (responsesave.IsSuccess == true) {
                            $("#ModalFormUpdateProfile").modal("hide");
                            swal({
                                title: 'Berhasil',
                                text: 'Anda telah berhasil memperbarui profil !',
                                confirmButtonClass: 'btn-success text-white',
                                confirmButtonText: 'Oke, Mengerti',
                                type: 'success'
                            });
                        } else if (responsesave.IsSuccess == false) {
                            swal({
                                title: 'Gagal Edit Profil',
                                text: responsesave.ReturnMessage,
                                confirmButtonClass: 'btn-danger text-white',
                                confirmButtonText: 'Oke, Mengerti',
                                type: 'error'
                            });
                        }
                    },
                    error: function (errorresponse) {
                        ProgressBar("success");
                        swal({
                            title: 'Gagal Edit Profil',
                            text: errorresponse,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                });
            } else {
                $("#ModalFormUpdateProfile").modal("hide");
            }
        }, 500);
        
    }
        
}

function GetKelasParalelPegawai() {
    $.ajax({
        url: base_url + '/api/Pegawais/GetKelasParalelPegawai',
        type: 'GET',
        success: function (res) {
            if (res.IsSuccess) {
                var html = "";
                $.each(res.Data, (i, v) => {
                    html += `<option value='${v.Id}'>${v.Nama}</option>`;
                });
                $("select[name='IdKelasParalel[]']",'#FormUpdateProfile').html(html);
            } else {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (err, a, e) {
            swal({
                title: 'Error :(',
                text: JSON.stringify(err) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function GetKompetensiPegawai() {
    $.ajax({
        url: base_url + '/api/pegawais/GetKompetensiPegawai',
        type: 'GET',
        success: function (res) {
            if (res.IsSuccess) {
                var html = "";
                $.each(res.Data, (i, v) => {
                    html += `<option value='${v.Id}'>${v.Nama}</option>`;
                });
                $("select[name='IdKompetensi[]']",'#FormUpdateProfile').html(html);
            } else {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (err, a, e) {
            swal({
                title: 'Error :(',
                text: JSON.stringify(err) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function Logout() {
    swal({
            title: "Apakah Anda Yakin ?",
            text: "Anda akan keluar dari sesi login akun anda",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-success",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: false
        },
        function () {
            var Url = base_url + "/Users/Logout";
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        window.open(base_url + '/User/Login', '_self');
                        iziToast.success({
                            title: 'Berhasil Logout',
                            message: 'Kamu Berhasil Keluar Dari Akun',
                            position: 'topRight'
                        });
                    } else if (responsesuccess.IsSuccess == false) {
                        iziToast.error({
                            title: 'Gagal Logout',
                            message: 'Kamu Gagal Keluar Dari Akun',
                            position: 'topRight'
                        });
                    }
                },
                error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({
                        title: 'Error :(',
                        text: JSON.stringify(responserror) + " : " + e,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            });
        });
}

function SaveResetPassword() {
    var formObj = $('#FormResetPassword').serializeObject();
    var Url = base_url + "/Auth/ResetPassword?UsernameOrEmail=" + formObj.UsernameOrEmail;
    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $("#ModalFormResetPassword").modal("hide");
                swal({
                    title: 'Berhasil Reset Password',
                    text: "Silahkan Cek Email Anda",
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal Reset Password',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function SaveGantiPassword() {
    var formObj = $('#FormChangePassword').serializeObject();
    var Url = base_url + "/Users/ChangePassword?UsernameOrEmail=" + formObj.UsernameOrEmail + "&OldPassword=" + formObj.OldPassword + "&NewPassword1=" + formObj.NewPassword1 + "&NewPassword2=" + formObj.NewPassword2;
    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        processData: false,
        contentType: false,
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $("#ModalFormGantiPassword").modal("hide");
                swal({
                    title: 'Berhasil Merubah Password',
                    text: "",
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal Merubah Password',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (errorresponse) {
            swal({
                title: 'Gagal Merubah Password',
                text: errorresponse,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ShowPassword() {
    $("input[type='password']").attr("type", "text");
    $("button[onClick='ShowPassword();']").attr("onClick", "HidePassword();").html('<i class="fa fa-eye-slash fa-1x"></i>');
}

function HidePassword() {
    $("input[type='text'][name='Password']").attr("type", "password");
    $("button[onClick='HidePassword();']").attr("onClick", "ShowPassword();").html('<i class="fa fa-eye fa-1x"></i>');;
}

$(".toggle-password").click(function () {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});

function ProsesLogin() {
    var formObj = $('#FormLogin').serializeObject();
    var UsernameOrEmail = formObj.UserNameOrEmail;
    var Password = formObj.Password;
    var Url = base_url + "/Auth/Login?UserNameOrEmail=" + UsernameOrEmail + "&Password=" + Password;
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (before) {
            $("button[name='BtnLogin']").text('Sedang memuat, mohon tunggu...').attr("disabled", true);
            ProgressBar("wait");
        },
        success: function (responselogin) {
            ProgressBar("success");
            if (responselogin.IsSuccess == true) {
                localStorage.setItem('DataUser', JSON.stringify(responselogin.Data));
                sessionStorage.setItem('DataUser', JSON.stringify(responselogin.Data));
                iziToast.success({
                    title: "Login Berhasil Selamat Datang " + responselogin.Data.FirstName,
                    message: 'Anda akan diarahkan kehalaman utama',
                    position: 'topRight',
                    timeout: 3000
                });
                setTimeout(function () {
                    location.href = base_url + '/Home/Index';
                    $('#ProsesLoading').hide();
                    $('#BtnLoading').show();
                }, 3000);
            } else if (responselogin.IsSuccess == false) {
                $("button[name='BtnLogin']").html('<i class="fa fa-sign-in-alt"></i> Login').removeAttr("disabled");
                if (responselogin.ReturnMessage == "password sudah satu bulan tidak diganti, silahkan ganti password anda") {
                    swal({
                            title: "Silahkan Ganti Password",
                            text: "Demi keamanan akun milik anda, kami menyarankan anda untuk mengganti password sebulan sekali",
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonClass: "btn-warning text-white",
                            confirmButtonText: "Oke, Mengerti",
                            closeOnConfirm: false
                        },
                        function () {
                            location.href = base_url + '/Home/GantiPassword';
                        });
                } else {
                    iziToast.error({
                        title: "Login Gagal",
                        message: responselogin.ReturnMessage,
                        position: 'topRight',
                        timeout: 3000
                    });
                }
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}