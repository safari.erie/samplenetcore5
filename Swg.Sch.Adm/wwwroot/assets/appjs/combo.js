﻿function ComboGetRoles(handleData, IdRoles) {
    var Url = base_url + "/Users/GetRoles";
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var TipeData = jQuery.type(responsesuccess.Data);
                var HtmlCombo = "";
                HtmlCombo += "<option value='' style='display:none;'>- Pilih Salah Satu -</option>";
                if (TipeData == "array") {
                    var JmlJL = (responsesuccess.Data).length;
                    var Roles = IdRoles;
                    for (var ijl = 0; ijl < JmlJL; ijl++) {
                        if (responsesuccess.Data[ijl].IdRole != Roles[ijl]) {
                            HtmlCombo += "<option value='" + responsesuccess.Data[ijl].IdRole + "'>" + responsesuccess.Data[ijl].RoleName + "</option>";
                        }
                    }
                } else {
                    HtmlCombo += "<option value='" + responsesuccess.Data.IdRole + "' selected>" + responsesuccess.Data.RoleName + "</option>";
                }

                handleData(HtmlCombo);
                // umum nya seperti ini bentuk penamaan element combo nya : $("select[name='IdRole[]']").html(HtmlCombo);
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetRole(handleData, IdRole) {
    var Url = base_url + "/Users/GetRole?IdRole=" + parseInt(IdRole);
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var TipeData = jQuery.type(responsesuccess.Data);
                var HtmlCombo = "";
                HtmlCombo += "<option value='' style='display:none;'>- Pilih Salah Satu -</option>";
                if (TipeData == "array") {
                    var JmlJL = (responsesuccess.Data).length;
                    for (var ijl = 0; ijl < JmlJL; ijl++) {
                        if (responsesuccess.Data[ijl].IdRole == IdUser) {
                            HtmlCombo += "<option value='" + responsesuccess.Data[ijl].Id + "' selected>" + responsesuccess.Data[ijl].Nama + "</option>";
                        } else {
                            HtmlCombo += "<option value='" + responsesuccess.Data[ijl].Id + "'>" + responsesuccess.Data[ijl].Nama + "</option>";
                        }
                    }
                } else {
                    HtmlCombo += "<option value='" + responsesuccess.Data.IdRole + "'>" + responsesuccess.Data.RoleName + "</option>";
                }

                handleData(HtmlCombo);
                // umum nya seperti ini bentuk penamaan element combo nya : $("select[name='IdRoleSelected[]']").html(HtmlCombo);
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetApplTaskByRole(handleData, IdRole) {
    var Url = base_url + "/ApplTasks/GetApplTaskByRole?IdRole=" + parseInt(IdRole);
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var HtmlComboNoAssignTasks = "";
                var HtmlComboAssignTasks = "";
                var JmlNoAssignTasks = (responsesuccess.Data.NoAssignTasks).length;
                var JmlAssignTasks = (responsesuccess.Data.AssignTasks).length;

                for (var inat = 0; inat < JmlNoAssignTasks; inat++) {
                    HtmlComboNoAssignTasks += "<option value='" + responsesuccess.Data.NoAssignTasks[inat].IdApplTask + "'>" + responsesuccess.Data.NoAssignTasks[inat].ApplTaskName + "</option>";
                }
                for (var iat = 0; iat < JmlAssignTasks; iat++) {
                    HtmlComboAssignTasks += "<option value='" + responsesuccess.Data.AssignTasks[iat].IdApplTask + "' selected>" + responsesuccess.Data.AssignTasks[iat].ApplTaskName + "</option>";
                }

                handleData(HtmlComboNoAssignTasks, HtmlComboAssignTasks);

            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetParentMenu(handleData, IdApplTaskParent, IdAppl) {
    var Url = base_url + "/ApplTasks/GetApplTasks?IdAppl=" + IdAppl;
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var HtmlCombo = "";
                var JmlData = (responsesuccess.Data).length;
                HtmlCombo += "<option value=''>- Pilih Salah Satu -</option>";
                for (var idata = 0; idata < JmlData; idata++) {
                    if (responsesuccess.Data[idata].IdApplTaskParent == null) {
                        if (responsesuccess.Data[idata].IdApplTask == IdApplTaskParent) {
                            HtmlCombo += "<option value='" + responsesuccess.Data[idata].IdApplTask + "' selected>" + responsesuccess.Data[idata].ApplTaskName + "</option>";
                        } else {
                            HtmlCombo += "<option value='" + responsesuccess.Data[idata].IdApplTask + "'>" + responsesuccess.Data[idata].ApplTaskName + "</option>";
                        }
                    }
                }

                handleData(HtmlCombo);
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetJenisVisiMisi(handleData, IdJenisVisiMisi) {
    var Url = base_url + "/Sekolahs/GetJenisVisiMisi";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdJenisVisiMisi) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetJenisGaleri(handleData, IdJenisGaleri) {
    var Url = base_url + "/Sekolahs/GetJenisGaleri";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdJenisGaleri) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetJenisGolonganPegawai(handleData, IdGolonganPegawai) {
    var Url = base_url + "/api/Pegawais/GetJenisGolonganPegawai";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdGolonganPegawai) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetJenisPegawai(handleData, IdJenisPengurus) {
    var Url = base_url + "/Sekolahs/GetJenisPegawai";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdJenisPengurus) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetStatusData(handleData, IdStatusData) {
    var Url = base_url + "/Units/GetStatusData";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdStatusData) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetPegawai(handleData, IdPegawai) {
    var Url = base_url + "/Sekolahs/GetPegawais";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].IdPegawai == IdPegawai) {
                        html_combo += '<option value="' + responsesave.Data[i].IdPegawai + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].IdPegawai + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetSiswaByKelasParalel(handleData, IdKelasParalel, IdSiswa) {
    var Url = base_url + "/Siswas/GetSiswasByKelasParalel?IdKelasParalel=" + parseInt(IdKelasParalel);
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].IdSiswa == IdSiswa) {
                        html_combo += '<option value="' + responsesave.Data[i].IdSiswa + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].IdSiswa + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetKelompokQuran(handleData, IdKelasQuran) {
    var Url = base_url + "/api/Elearnings/GetKelompokQuran";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].IdKelasQuran == IdKelasQuran) {
                        html_combo += '<option value="' + responsesave.Data[i].IdKelasQuran + '" selected>' + responsesave.Data[i].Nama + ' - ' + responsesave.Data[i].NamaGuru + ' - ' + responsesave.Data[i].JamMulai + ' s/d ' + responsesave.Data[i].JamSelesai + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].IdKelasQuran + '">' + responsesave.Data[i].Nama + ' - ' + responsesave.Data[i].NamaGuru + ' - ' + responsesave.Data[i].JamMulai + ' s/d ' + responsesave.Data[i].JamSelesai + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetSiswa(handleData, IdSiswa) {
    var Url = base_url + "/Siswas/GetSiswas";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].IdSiswa == IdSiswa) {
                        html_combo += '<option value="' + responsesave.Data[i].IdSiswa + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].IdSiswa + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetUnit_Disabled(handleData, IdUnit) {
    var Url = base_url + "/Units/GetUnits";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].IdUnit == IdUnit) {
                        html_combo += '<option value="' + responsesave.Data[i].IdUnit + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].IdUnit + '" disabled>' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetUnit(handleData, IdUnit) {
    var Url = base_url + "/Units/GetUnits";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].IdUnit == IdUnit) {
                        html_combo += '<option value="' + responsesave.Data[i].IdUnit + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].IdUnit + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetJenisStatusNikah(handleData, Id) {
    var Url = base_url + "/Api/Pegawais/GetJenisStatusNikah";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == Id) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetUnitSklh(handleData, IdUnit) {
    var Url = base_url + "/Units/GetUnits";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < 4; i++) {
                    if (responsesave.Data[i].IdUnit == IdUnit) {
                        html_combo += '<option value="' + responsesave.Data[i].IdUnit + '" selected="true">' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].IdUnit + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetSekolah(handleData, IdSekolah) {
    var Url = base_url + "/Sekolahs/GetSekolah";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                html_combo += '<option value="' + responsesave.Data.IdSekolah + '" selected>' + responsesave.Data.Nama + '</option>';
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetKelas(handleData, IdUnit, IdKelas) {
    var Url = base_url + "/Kelass/GetKelass?IdUnit=" + IdUnit;
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].IdKelas == IdKelas) {
                        html_combo += '<option value="' + responsesave.Data[i].IdKelas + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].IdKelas + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetKelasParalel(handleData, IdKelas, IdKelasParalel) {
    var Url = base_url + "/Kelass/GetKelasParalels?IdKelas=" + IdKelas;
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].IdKelasParalel == IdKelasParalel) {
                        html_combo += '<option value="' + responsesave.Data[i].IdKelasParalel + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].IdKelasParalel + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetJenisKelamin(handleData, KdJenisKelamin) {
    var Url = base_url + "/Siswas/GetJenisKelamin";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Kode == KdJenisKelamin) {
                        html_combo += '<option value="' + responsesave.Data[i].Kode + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Kode + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetJenisPrestasiSiswa(handleData, IdJenisPrestasi) {
    var Url = base_url + "/Siswas/GetJenisPrestasiSiswa";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdJenisPrestasi) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetJenisPekerjaan(handleData, IdJenisPekerjaan) {
    var Url = base_url + "/Siswas/GetJenisPekerjaans";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdJenisPekerjaan) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetAgama(handleData, IdAgama) {
    var Url = base_url + "/Siswas/GetAgamas";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdAgama) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetTahunAjaran(handleData, IdTahunAjaran) {
    var Url = base_url + "/Siswas/GetTahunAjarans";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].IdTahunAjaran == IdTahunAjaran) {
                        html_combo += '<option value="' + responsesave.Data[i].IdTahunAjaran + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].IdTahunAjaran + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetListJenjangPendidikan(handleData, IdJenisPengurus) {
    var Url = base_url + "/api/Pegawais/GetListJenjangPendidikan";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdJenisPengurus) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetListJabatan(handleData, IdJenisPengurus) {
    var Url = base_url + "/api/Pegawais/GetListJabatan";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdJenisPengurus) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}


function ComboGetListUser(handleData, IdUser) {
    var Url = base_url + "/api/Pegawais/GetListUser";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].IdUser == IdUser) {
                        html_combo += '<option value="' + responsesave.Data[i].IdUser + '" selected>' + SetFullName(responsesave.Data[i].FirstName, responsesave.Data[i].MiddleName, responsesave.Data[i].LastName) + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].IdUser + '">' + SetFullName(responsesave.Data[i].FirstName, responsesave.Data[i].MiddleName, responsesave.Data[i].LastName) + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetBulan(handleData, IdBulan) {
    var html_combo = "";
    html_combo = '<option value="">- Pilih -</option>';
    var iBulan = 1;
    var Bulan = 0;
    for (var i = 1; i <= 12; i++) {
        var ArrayBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        var NamaBulan = ArrayBulan[Bulan];

        if (iBulan < 10) {
            iBulan = "0" + iBulan;
        }
        if (iBulan == IdBulan) {
            html_combo += '<option value="' + iBulan + '" selected>' + NamaBulan + '</option>';
        } else {
            html_combo += '<option value="' + iBulan + '">' + NamaBulan + '</option>';
        }
        iBulan++;
        Bulan++;
    }
    handleData(html_combo);
}

function ComboGetTahun(handleData, IdTahun) {
    var html_combo = "";
    html_combo = '<option value="">- Pilih -</option>';
    var d = new Date($.now());
    var TahunSekarang = d.getFullYear();
    var TahunMulai = 1999;
    for (TahunSekarang; TahunSekarang >= TahunMulai; TahunSekarang--) {
        if (IdTahun == null) {
            if (TahunSekarang == d.getFullYear()) {
                html_combo += '<option value="' + TahunSekarang + '" selected>' + TahunSekarang + '</option>';
            } else {
                html_combo += '<option value="' + TahunSekarang + '">' + TahunSekarang + '</option>';
            }
        } else {
            if (TahunSekarang == IdTahun) {
                html_combo += '<option value="' + TahunSekarang + '" selected>' + TahunSekarang + '</option>';
            } else {
                html_combo += '<option value="' + TahunSekarang + '">' + TahunSekarang + '</option>';
            }
        }
    }
    handleData(html_combo);
}

function ComboGetAppl(handleData, IdAppl) {
    var Url = base_url + "/ApplTasks/GetAppls";
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var HtmlCombo = "";
                var JmlData = (responsesuccess.Data).length;
                HtmlCombo += "<option value=''>- Pilih Salah Satu -</option>";
                for (var idata = 0; idata < JmlData; idata++) {
                    if (responsesuccess.Data[idata].IdAppl == IdAppl) {
                        HtmlCombo += "<option value='" + responsesuccess.Data[idata].IdAppl + "' selected>" + responsesuccess.Data[idata].ApplName + "</option>";
                    } else {
                        HtmlCombo += "<option value='" + responsesuccess.Data[idata].IdAppl + "'>" + responsesuccess.Data[idata].ApplName + "</option>";
                    }
                }

                handleData(HtmlCombo);
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetJilidQuran(handleData, IdJenisJilidQuran) {
    var Url = base_url + "/api/Elearnings/GetJilidQuran";
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var HtmlCombo = "";
                var JmlData = (responsesuccess.Data).length;
                HtmlCombo += "<option value=''>- Pilih Salah Satu -</option>";
                for (var idata = 0; idata < JmlData; idata++) {
                    if (responsesuccess.Data[idata].Id == IdJenisJilidQuran) {
                        HtmlCombo += "<option value='" + responsesuccess.Data[idata].Id + "' selected>" + responsesuccess.Data[idata].Nama + "</option>";
                    } else {
                        HtmlCombo += "<option value='" + responsesuccess.Data[idata].Id + "'>" + responsesuccess.Data[idata].Nama + "</option>";
                    }

                }
                handleData(HtmlCombo);
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetJenisEvaluasiHariansByUnit(handleData, IdUnit) {
    var Url = base_url + "/api/Elearnings/GetJenisEvaluasiHariansByUnit?IdUnit=" + parseInt(IdUnit);
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var HtmlComboNoAssignEvals = "";
                var HtmlComboAssignEvals = "";
                var JmlNoAssignEvals = (responsesuccess.Data.NoAssignEvals).length;
                var JmlAssignEvals = (responsesuccess.Data.AssignEvals).length;

                for (var inat = 0; inat < JmlNoAssignEvals; inat++) {
                    HtmlComboNoAssignEvals += "<option value='" + responsesuccess.Data.NoAssignEvals[inat].IdJenisEvaluasiHarian + "'>" + responsesuccess.Data.NoAssignEvals[inat].Nama + "</option>";
                }
                for (var iat = 0; iat < JmlAssignEvals; iat++) {
                    HtmlComboAssignEvals += "<option value='" + responsesuccess.Data.AssignEvals[iat].IdJenisEvaluasiHarian + "' selected>" + responsesuccess.Data.AssignEvals[iat].Nama + "</option>";
                }

                handleData(HtmlComboNoAssignEvals, HtmlComboAssignEvals);

            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetJenisEvaluasiHariansByUnitPegawai(handleData, IdUnit) {
    var Url = base_url + "/api/Elearnings/GetJenisEvaluasiHariansByUnitPegawai?IdUnit=" + parseInt(IdUnit);
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var HtmlComboNoAssignEvals = "";
                var HtmlComboAssignEvals = "";
                var JmlNoAssignEvals = (responsesuccess.Data.NoAssignEvals).length;
                var JmlAssignEvals = (responsesuccess.Data.AssignEvals).length;

                for (var inat = 0; inat < JmlNoAssignEvals; inat++) {
                    HtmlComboNoAssignEvals += "<option value='" + responsesuccess.Data.NoAssignEvals[inat].IdJenisEvaluasiHarianPegawai + "'>" + responsesuccess.Data.NoAssignEvals[inat].Nama + "</option>";
                }
                for (var iat = 0; iat < JmlAssignEvals; iat++) {
                    HtmlComboAssignEvals += "<option value='" + responsesuccess.Data.AssignEvals[iat].IdJenisEvaluasiHarianPegawai + "' selected>" + responsesuccess.Data.AssignEvals[iat].Nama + "</option>";
                }

                handleData(HtmlComboNoAssignEvals, HtmlComboAssignEvals);

            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetHariLibursByUnit(handleData, IdUnit) {
    var Url = base_url + "/api/Elearnings/GetHariLibursByUnit?IdUnit=" + parseInt(IdUnit);
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var HtmlComboNoAssignLiburs = "";
                var HtmlComboAssignLiburs = "";
                var JmlNoAssignLiburs = (responsesuccess.Data.NoAssignLiburs).length;
                var JmlAssignLiburs = (responsesuccess.Data.AssignLiburs).length;

                for (var inat = 0; inat < JmlNoAssignLiburs; inat++) {
                    HtmlComboNoAssignLiburs += "<option value='" + responsesuccess.Data.NoAssignLiburs[inat].IdHariLibur + "'>" + responsesuccess.Data.NoAssignLiburs[inat].Nama + "</option>";
                }
                for (var iat = 0; iat < JmlAssignLiburs; iat++) {
                    HtmlComboAssignLiburs += "<option value='" + responsesuccess.Data.AssignLiburs[iat].IdHariLibur + "' selected>" + responsesuccess.Data.AssignLiburs[iat].Nama + "</option>";
                }

                handleData(HtmlComboNoAssignLiburs, HtmlComboAssignLiburs);

            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetHasil(handleData, Hasil) {
    var ArrayValHasil = ["Tuntas", "TidakTuntas"];
    var ArrayTextHasil = ["Tuntas", "Tidak Tuntas"];

    var HtmlCombo = "";
    var JmlData = (ArrayValHasil).length;
    HtmlCombo += "<option value=''>- Pilih Salah Satu -</option>";
    for (var idata = 0; idata < JmlData; idata++) {
        if (ArrayValHasil[idata] == Hasil) {
            HtmlCombo += "<option value='" + ArrayValHasil[idata] + "' selected>" + ArrayTextHasil[idata] + "</option>";
        } else {
            HtmlCombo += "<option value='" + ArrayValHasil[idata] + "'>" + ArrayTextHasil[idata] + "</option>";
        }
    }
    handleData(HtmlCombo);
}

function ComboGetNilai(handleData, Nilai) {
    var ArrayValNilai = ["A", "B", "C", "D", "E"];
    var ArrayTextNilai = ["A", "B", "C", "D", "E"];

    var HtmlCombo = "";
    var JmlData = (ArrayValNilai).length;
    HtmlCombo += "<option value=''>- Pilih Salah Satu -</option>";
    for (var idata = 0; idata < JmlData; idata++) {
        if (ArrayValNilai[idata] == Nilai) {
            HtmlCombo += "<option value='" + ArrayValNilai[idata] + "' selected>" + ArrayTextNilai[idata] + "</option>";
        } else {
            HtmlCombo += "<option value='" + ArrayValNilai[idata] + "'>" + ArrayTextNilai[idata] + "</option>";
        }
    }
    handleData(HtmlCombo);
}


function ComboGetJenisDataKontenUnit(handleData, Nilai) {
    var ArrayValNilai = [0, 1];
    var ArrayTextNilai = ["Default", "Profil"];

    var HtmlCombo = "";
    var JmlData = (ArrayValNilai).length;
    HtmlCombo += "<option value=''>- Pilih Salah Satu -</option>";
    for (var idata = 0; idata < JmlData; idata++) {
        if (ArrayValNilai[idata] == Nilai) {
            HtmlCombo += "<option value='" + ArrayValNilai[idata] + "' selected>" + ArrayTextNilai[idata] + "</option>";
        } else {
            HtmlCombo += "<option value='" + ArrayValNilai[idata] + "'>" + ArrayTextNilai[idata] + "</option>";
        }
    }
    handleData(HtmlCombo);
}

function ComboGetSeragam(handleData, IdPpdbSeragam) {
    var Url = base_url + "/api/ppdbs/GetPpdbSeragam?idppdbseragam=" + IdPpdbSeragam;
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var HtmlCombo = "";
                HtmlCombo += "<option value='" + responsesuccess.Data.IdPpdbSeragam + "' selected>" + responsesuccess.Data.NamaSingkat + " - " + responsesuccess.Data.Nama + "</option>";
                handleData(HtmlCombo);
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetJenisPendaftarans(handleData, IdJenisPendaftaran) {
    var Url = base_url + "/api/ppdbs/GetJenisPendaftarans";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdJenisPendaftaran) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected="true">' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetJalurPendaftaran(handleData, IdJalurPendaftaran) {
    var Url = base_url + "/api/ppdbs/GetJalurPendaftarans";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdJalurPendaftaran) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected="true">' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetGelombangPpdbs(handleData, IdGelombangPpdb) {
    var Url = base_url + "/api/ppdbs/GetGelombangPpdbs";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdGelombangPpdb) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected="true">' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetJenisWargaNegara(handleData, IdKewarganegaraan) {
    var Url = base_url + "/api/ppdbs/GetJenisWargaNegara";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdKewarganegaraan) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetTinggalSiswa(handleData, IdTinggal) {
    var Url = base_url + "/api/ppdbs/GetTinggalSiswa";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdTinggal) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetJenisTransportasi(handleData, IdTransportasi) {
    var Url = base_url + "/api/ppdbs/GetJenisTransportasi";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdTransportasi) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetJenisBahasa(handleData, IdBahasa) {
    var Url = base_url + "/api/ppdbs/GetJenisBahasa";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdBahasa) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetStatusHasilObservasi(handleData, IdStatusObservasi) {
    var Url = base_url + "/api/ppdbs/GetStatusHasilObservasi";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdStatusObservasi) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function ComboGetStatusVerifikasiObservasi(handleData, IdStatusObservasi) {
    var Url = base_url + "/api/ppdbs/GetStatusVerifikasiObservasi";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdStatusObservasi) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

const GetKelasPpdbs = (handleData, IdPpdbKelas) => {
    var Url = base_url + "/api/ppdbs/GetKelasPpdbs";
    $.getJSON(Url, (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if(v.Id == IdPpdbKelas)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                
                html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
            handleData(html);
        } else console.log(res)
    });
}

const GetKategoriDaftars = (handleData, IdJenisKategoriPendaftaran) => {
    $.getJSON(base_url + "/api/ppdbs/GetKategoriDaftars", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if(v.Id == IdJenisKategoriPendaftaran)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                
                html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
            handleData(html);
        } else console.log(res)
    });
}

const GetPpdbSeragams = (handleData, IdPpdbSeragam) => {
    $.getJSON(base_url + "/api/ppdbs/GetPpdbSeragams", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if(v.IdPpdbSeragam == IdPpdbSeragam)
                    html += '<option value="' + v.IdPpdbSeragam + '" selected>'+v.NamaSingkat + ' - ' + v.Nama + '</option>';
                
                html += '<option value="' + v.IdPpdbSeragam + '">'+v.NamaSingkat + ' - ' + v.Nama + '</option>';
            });
            handleData(html);
        } else console.log(res)
    });
}

function ComboGetJenisRapors(handleData, IdJenisRapor) {
    var Url = base_url + "/api/UploadRapors/GetJenisRapors";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].IdJenisRapor == IdJenisRapor) {
                        html_combo += '<option value="' + responsesave.Data[i].IdJenisRapor + '" selected>' +responsesave.Data[i].Nama+ '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].IdJenisRapor + '">' +responsesave.Data[i].Nama+ '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

/// SECTION MODUL KEUANGAN ///
function ComboGetParentCoas(handleData, IdCoa) {
    var Url = base_url + "/Keuangans/GetCoasByParent?IdCoa="+parseInt(IdCoa);
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].IdCoa == IdCoa) {
                        html_combo += '<option value="' + responsesave.Data[i].IdCoa + '" selected>' + responsesave.Data[i].Kode + " - " + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].IdCoa + '">' + responsesave.Data[i].Kode + " - " + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }

        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function GetCoasByJenisAkun(IdJenisAkun,KodeCoa) {
    var Url = base_url + "/Keuangans/GetCoasByJenisAkun?IdJenisAkun=" + parseInt(IdJenisAkun);
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Kode == KodeCoa) {
                        html_combo += '<option value="' + responsesave.Data[i].Kode + '" selected>' + responsesave.Data[i].Kode + ' - ' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Kode + '">' + responsesave.Data[i].Kode + ' - ' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                $("select[name='KodeCoa']").html(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }

        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function GetCoaRenPens(handleData, IdCoa) {
    var Url = base_url + "/Keuangans/GetCoaRenPens";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdCoa) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Kode + ' - ' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Kode + ' - ' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }

        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function GetCoaProgKegs(handleData, IdCoa) {
    var Url = base_url + "/Keuangans/GetCoaProgKegs";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdCoa) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Kode + ' - ' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Kode + ' - ' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }

        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function GetCoaPengajuans(handleData,Id) {
    var Url = base_url + "/Keuangans/GetCoaPengajuans";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == Id) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }

        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function GetPengajuanPrograms(handleData,Id) {
    var Url = base_url + "/Keuangans/GetPengajuanPrograms";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == Id) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }

        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function GetProgramKegiatanWajibs(handleData,IdTahunAjaran,IdProgram,Id) {
    var Url = base_url + "/Keuangans/GetProgramKegiatanWajibs?IdTahunAjaran="+IdTahunAjaran+"&IdProgram="+IdProgram;
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == Id) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }

        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function GetProgramKegiatanTambahans(handleData,IdTahunAjaran,IdProgram,Id) {
    var Url = base_url + "/Keuangans/GetProgramKegiatanTambahans?IdTahunAjaran="+IdTahunAjaran+"&IdProgram="+IdProgram;
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == Id) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }

        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function GetJenisSatuans(handleData,IdJenisSatuan) {
    var Url = base_url + "/Keuangans/GetJenisSatuans";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdJenisSatuan) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }

        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function GetJenisVolumes(handleData,IdJenisVolume) {
    var Url = base_url + "/Keuangans/GetJenisVolumes";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == IdJenisVolume) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                handleData(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }

        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function ComboGetJenisTransaksiCoa(handleData,IdJenisTransaksi) {
    var Url = base_url + "/Keuangans/GetJenisTransaksiCoa";
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var HtmlCombo = "";
                var JmlData = (responsesuccess.Data).length;
                HtmlCombo += "<option value=''>- Pilih Salah Satu -</option>";
                for (var idata = 0; idata < JmlData; idata++) {
                    if (responsesuccess.Data[idata].Id == IdJenisTransaksi) {
                        HtmlCombo += "<option value='" + responsesuccess.Data[idata].Id + "' selected>" + responsesuccess.Data[idata].Nama + "</option>";
                    } else {
                        HtmlCombo += "<option value='" + responsesuccess.Data[idata].Id + "'>" + responsesuccess.Data[idata].Nama + "</option>";
                    }
                }
                handleData(HtmlCombo);
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function ComboGetJenisAkunCoa(handleData,IdJenisAkun) {
    var Url = base_url + "/Keuangans/GetJenisAkunCoa";
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var HtmlCombo = "";
                var JmlData = (responsesuccess.Data).length;
                HtmlCombo += "<option value=''>- Pilih Salah Satu -</option>";
                for (var idata = 0; idata < JmlData; idata++) {
                    if (responsesuccess.Data[idata].Id == IdJenisAkun) {
                        HtmlCombo += "<option value='" + responsesuccess.Data[idata].Id + "' selected>" + responsesuccess.Data[idata].Nama + "</option>";
                    } else {
                        HtmlCombo += "<option value='" + responsesuccess.Data[idata].Id + "'>" + responsesuccess.Data[idata].Nama + "</option>";
                    }
                    
                }
                handleData(HtmlCombo);
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

/// END MODUL KEUANGAN ///