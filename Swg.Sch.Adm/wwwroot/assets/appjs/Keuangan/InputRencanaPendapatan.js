/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 14042020
================================================================================================================= */
$("select[name='Tipe']").change(function () {
    if(this.value == 1){
        $("#BtnAksi").text("Set").attr("onClick", "InitRenpen()");
    } else if(this.value == 3){
        $("#BtnAksi").text("Tampilkan").attr("onClick", "GetRenPens()");
    }
});
ComboGetTahunAjaran(function (obj) {
    $("select[name='IdTahunAjaran']").html(obj);
});
function InitRenpen(){
    var Url = base_url + "/Keuangans/InitRenpen";
    var formData = new FormData($('#FormAksi')[0]);
    $.ajax({
        url: Url,
        type: "POST",
        data: formData,
        dataType: "json",
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                swal({ title: 'Berhasil', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesuccess.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function DownloadRenpen(IdUnitRenPen,Unit){
    var url = base_url + "/Keuangans/DownloadRenpen?IdUnitRenPen=" +IdUnitRenPen;
    window.open(url, '_blank');
}

function GetRenPens(){
    $("#MainTableEmpty").css("display","none");
    $("#MainTable").css("display","block");
    //////// START SECTION TABEL RENCANA PENDAPATAN ///////////
    $('#TabelRenpen tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelRenpen = $('#TabelRenpen').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/Keuangans/GetRenPens?IdTahunAjaran="+parseInt($("select[name='IdTahunAjaran'] option:selected").val()),
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Rencana Pendapatan',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    var JmlBtnAksi = full.Aksis.length;
                    var Data = '';
                    var Param = '';
                    for (var i = 0; i < JmlBtnAksi; i++) {
                        Param = "AksiTabelRenpen('" + full.Aksis[i] + "','" + full.IdUnitRenPen + "','" + full.Unit + "')";
                        Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + Param + '">' + full.Aksis[i] + '</button>';
                        Data += '&nbsp;&nbsp;';
                    }
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var ParamView = "TabelData('" + full.IdUnitRenPen + "')";
                    var Data = '<span onClick="' + ParamView + '">' + full.Unit + '</span>';
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var ParamView = "TabelData('" + full.IdUnitRenPen + "')";
                    var Data = '<span onClick="' + ParamView + '">' + full.TahunAjaran + '</span>';
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var ParamView = "TabelData('" + full.IdUnitRenPen + "')";
                    var Data = '<span onClick="' + ParamView + '">' + formatRupiah(full.Rp) + '</span>';
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var ParamView = "TabelData('" + full.IdUnitRenPen + "')";
                    var Data = '<span onClick="' + ParamView + '">' + full.Status + '</span>';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });
    TabelRenpen.columns().every(function () {
        var that = this;
    
        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    //////// START SECTION TABEL RENCANA PENDAPATAN ///////////
}


function AksiTabelRenpen(Aksi, IdUnitRenPen, Unit) {
    var Url = "";
    if (Aksi == "View") {
        TabelRenpenCoa(IdUnitRenPen, Unit);
    } else if (Aksi == "Download") {
        DownloadRenpen(IdUnitRenPen, Unit);
    } else if (Aksi == "Approve Mgr Keu") {
        Setujui(IdUnitRenPen, Unit, 'RenPenApproveDataByManKeu');
    } else if (Aksi == "Approve Direktur") {
        Setujui(IdUnitRenPen, Unit, 'RenPenApproveDataByDirektur');
    } else if (Aksi == "Set Sah") {
        Url = base_url + "/Keuangans/RenPenSetSah?IdUnitRenPen=" + IdUnitRenPen;
        SaveSah(IdUnitRenPen, Unit, Url);
    }  
}

function SaveSah(IdUnitRenPen, Unit, Url) {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan men-sah-kan Rencana Pendapatan unit " + Unit,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        $("#TabelRenpen").DataTable().ajax.reload();
                        swal({ title: 'Berhasil Sah-kan Rencana Pendapatan', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesuccess.IsSuccess == false) {
                        swal({ title: 'Gagal Sah-kan Rencana Pendapatan', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}

function Setujui(IdUnitRenPen,Unit,Api) {
    $("#ModalFormSetujui").modal("show");
    $("#ModalHeaderFormSetujui").text("Approve Rencana Pendapatan & Pagu");

    $("input[name='IdUnitRenPen']", "#FormSetujui").val(IdUnitRenPen)
    $("input[name='Unit']", "#FormSetujui").val(Unit)
    $("#BtnSetujui").attr("onClick", "SaveSetujui('" + Api +"')");
}

function TabelRenpenCoa(IdUnitRenPen) {
    $("#DivSectionAkun").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#DivSectionAkun").offset().top
    }, 50);
    //////// START SECTION TABEL RENPEN COA ///////////
    var Url = base_url + "/Keuangans/GetRenPenCoas?IdUnitRenPen=" + parseInt(IdUnitRenPen);
    console.log(Url);
    $('#TabelRenpenCoa tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelRenpenCoa = $('#TabelRenpenCoa').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
         "dom": "Bfrtip",
         buttons: [
             {
                 text: '<i class="fa fa-plus"></i> Tambah Akun',
                 className: 'btn btn-success btn-flat mr-2 mb-2 text-right float-right',
                 action: function (e, dt, node, config) {
                     TambahRenpenCoa(IdUnitRenPen);
                 },
                 init: function (api, node, config) {
                     $(node).removeClass('dt-button');
                 }
             }
         ],
        "ajax": {
            "url": Url,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Data COA',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        columnDefs: [
            { targets: [0], width: "auto", visible: true },
            { targets: [1], width: "auto", visible: true },
            { targets: [2], width: "auto", visible: true },
            { targets: [3], width: "auto", visible: true },
        ],
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    var ParamRenpenCoaEdit = "EditRenpenCoa('" + full.IdUnitRenPen + "','" + full.IdCoa + "')";
                    var ParamRenpenCoaHapus = "HapusRenpenCoa('" + full.IdUnitRenPen + "','" + full.IdCoa + "','" + full.KodeCoa + "')";
                    var Data = '';
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamRenpenCoaEdit + '"><i class="fa fa-pencil-alt"></i> Edit</button>';
                    Data += '&nbsp;&nbsp;';
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamRenpenCoaHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                    return Data;
                }
            },
            { "data": "KodeCoa" },
            { "data": "NamaCoa" },
            {
                "render": function (data, type, full, meta) {
                    return formatRupiah(full.Rp);
                }
            },
        ],
        "bDestroy": true
    });
    TabelRenpenCoa.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    //////// START SECTION TABEL RENPEN COA ///////////
}

function TambahRenpenCoa(IdUnitRenPen) {
    $("#ModalFormRenpenCoa").modal("show");
    $("#BtnSaveRenpenCoa").attr("onClick", "SaveRenpenCoa('TambahData')");
    $("#ModalHeaderFormRenpenCoa").text("Tambah Data COA");

    $("input[name='IdUnitRenPen']", "#FormRenpenCoa").val(IdUnitRenPen);
    GetCoaRenPens(function (HtmlCombo) {
        $("select[name='IdCoa']", "#FormRenpenCoa").html(HtmlCombo);
    });
}

function EditRenpenCoa(IdUnitRenpen,IdCoa) {
    $("#ModalFormRenpenCoa").modal("show");
    $("#BtnSaveRenpenCoa").attr("onClick", "SaveRenpenCoa('EditData')");
    var Url = base_url + "/Keuangans/GetRenPenCoa?IdUnitRenpen=" + parseInt(IdUnitRenpen)+"&IdCoa=" + parseInt(IdCoa);
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                $("#ModalHeaderFormRenpenCoa").text("Edit Data COA");
                $("input[name='IdUnitRenPen']", "#FormRenpenCoa").val(responsesuccess.Data.IdUnitRenPen);
                $("input[name='Rp']", "#FormRenpenCoa").val(responsesuccess.Data.Rp);

                GetCoaRenPens(function (HtmlCombo) {
                    $("select[name='IdCoa']", "#FormRenpenCoa").html(HtmlCombo);
                }, responsesuccess.Data.IdCoa);
                //
            } else if (responsesuccess.IsSuccess == false) {
                swal({ title: 'Gagal Menampilkan Rencana Pendapatan', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function SaveSetujui(Api) {
    var formObj = $('#FormSetujui').serializeObject();
    var Url = base_url + "/Keuangans/" + Api + "?IdUnitRenPen=" + formObj.IdUnitRenPen + "&StatusProses=" + formObj.StatusProses + "&Catatan=" + formObj.Catatan;
    console.log("nie ",Url);
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menyetujui Rencana Pendapatan unit " + formObj.Unit,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        $("#TabelRenpen").DataTable().ajax.reload();
                        $("#ModalFormSetujui").modal("hide");
                        swal({ title: 'Berhasil Setujui Rencana Pendapatan', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesuccess.IsSuccess == false) {
                        swal({ title: 'Gagal Setujui Rencana Pendapatan', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}


function SaveRenpenCoa(Tipe) {
    var formData = new FormData($('#FormRenpenCoa')[0]);
    var formObj = $('#FormRenpenCoa').serializeObject();
    if(Tipe == "TambahData"){
        var Url = base_url + "/Keuangans/RenPenAdd";
    } else if(Tipe == "EditData"){
        var Url = base_url + "/Keuangans/RenPenEdit";
    }
    $.ajax({
        url: Url,
        type: "POST",
        data: formData,
        dataType: "json",
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (responsebefore) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                TabelRenpenCoa(formObj.IdUnitRenPen);

                $('#FormRenpenCoa')[0].reset();
                $("#ModalFormRenpenCoa").modal("hide");
                swal({ title: 'Berhasil Simpan COA Rencana Pendapatan', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesuccess.IsSuccess == false) {
                swal({ title: 'Gagal Simpan COA Rencana Pendapatan', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
    
}

function HapusRenpenCoa(IdUnitRenpen, IdCoa, KodeCoa) {
    var formObj = $('#FormRenpenCoa').serializeObject();
    var Url = base_url + "/Keuangans/RenPenDelete?IdUnitRenpen=" + parseInt(IdUnitRenpen)+"&IdCoa=" + parseInt(IdCoa);
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus data coa " + KodeCoa,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        TabelRenpenCoa(IdUnitRenpen);
                        swal({ title: 'Berhasil Hapus Data COA', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesuccess.IsSuccess == false) {
                        swal({ title: 'Gagal Hapus Data COA', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}