/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 14042020
================================================================================================================= */

//////// START SECTION TABEL JENIS SATUAN ///////////
$('#TabelSatuan tfoot th').each(function () {
    var title = $(this).text();
    $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
});
var TabelSatuan = $('#TabelSatuan').DataTable({
    "paging": true,
    "searching": true,
    "ordering": false,
    "info": false,
    "pageLength": 5,
    "lengthChange": false,
    "scrollX": true,
    "processing": true,
    "ajax": {
        "url": base_url + "/Keuangans/GetJenisSatuans",
        "method": 'GET',
        "beforeSend": function (xhr) {
        },
        "dataSrc": function (json) {
            if (json.Data == null) {
                iziToast.error({
                    title: 'Gagal Menampilkan Data Tabel Jenis Satuan',
                    message: json.ReturnMessage,
                    position: 'topRight'
                });
                return json;
            } else {
                return json.Data;
            }
        }
    },
    columnDefs: [
        { targets: [0], width: "auto", visible: true, searchable: true },
        { targets: [1], width: "auto", visible: true, searchable: true },
        { targets: [2], width: "auto", visible: true, searchable: true },
    ],
    "columns": [
        { "data": "Id" },
        { "data": "Nama" },
        {
            "render": function (data, type, full, meta) {
                var ParamAksi = "'" + full.Id + "','" + full.Nama + "','" + full.NamaSingkat + "'";
                var aki_edit = 'onClick="EditData(' + ParamAksi + ');"';
                var aksi_hapus = 'onClick="HapusData(' + ParamAksi + ');"';
                data = '<button type="button" class="btn btn-primary" ' + aki_edit + '><i class="fa fa-pencil-alt"></i> Edit</button>&nbsp;&nbsp;<button type="button" class="btn btn-danger" ' + aksi_hapus + '><i class="fa fa-trash"></i> Hapus</button>';
                return data;
            }
        },
    ],
    "bDestroy": true
});
TabelSatuan.columns().every(function () {
    var that = this;

    $('input', this.footer()).on('keyup change clear', function () {
        if (that.search() !== this.value) {
            that
                .search(this.value)
                .draw();
        }
    });
});
$(".dataTables_filter").css("display", "none");
//////// START SECTION TABEL APLIKASI SETTING ///////////

function TambahData() {
    $("#FormSatuan").trigger("reset").css("display", "block");
    $("#ImgSearch").css("display", "none");

    $("#HeaderBody").html("Tambah Satuan");
    $("input[name='Id']").removeAttr("readonly");
    $("#BtnSave").attr("onClick", "SaveSatuan('TambahData')");
}
function EditData(Id,Nama,NamaSingkat) {
    $("#FormSatuan").trigger("reset").css("display", "block");
    $("#ImgSearch").css("display", "none");

    $("#HeaderBody").html("Edit Satuan");

    $("input[name='Id']").val(Id).attr("readonly", true);
    $("input[name='Nama']").val(Nama);
    $("input[name='NamaSingkat']").val(NamaSingkat);

    $("#BtnSave").attr("onClick", "SaveSatuan('EditData')");
}

function SaveSatuan(Tipe) {
    var formObj = $('#FormSatuan').serializeObject();

    if (Tipe == "EditData") {
        var NamaProses = "Edit Data";
        var Url = base_url + "/Keuangans/EditJenisSatuan?IdJenisSatuan=" + parseInt(formObj.Id) + "&Nama=" + formObj.Nama + "&NamaSingkat=" + formObj.NamaSingkat;
    } else if (Tipe == "TambahData") {
        var NamaProses = "Tambah Data";
        var Url = base_url + "/Keuangans/AddJenisSatuan?Nama=" + formObj.Nama + "&NamaSingkat=" + formObj.NamaSingkat;
    }

    $.ajax({
        url: Url,
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        dataType: "json",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $('#FormSatuan')[0].reset();
                $("#TabelSatuan").DataTable().ajax.reload();

                iziToast.success({
                    title: 'Berhasil ' + NamaProses+' Satuan',
                    message: responsesave.ReturnMessage,
                    position: 'topRight'
                });
            } else if (responsesave.IsSuccess == false) {
                iziToast.error({
                    title: 'Gagal ' + NamaProses + ' Satuan',
                    message: responsesave.ReturnMessage,
                    position: 'topRight'
                });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function HapusData(Id, Name) {
    var Url = base_url + "/Keuangans/DeleteJenisSatuan?IdJenisSatuan=" + parseInt(Id);
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus Satuan " + Name,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-success",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: true
    },
        function () {
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        $("#TabelSatuan").DataTable().ajax.reload();
                        iziToast.success({
                            title: 'Berhasil Hapus Data Satuan '+Name,
                            message: responsesuccess.ReturnMessage,
                            position: 'topRight'
                        });
                    } else if (responsesuccess.IsSuccess == false) {
                        iziToast.error({
                            title: 'Gagal Hapus Data Satuan ' + Name,
                            message: responsesuccess.ReturnMessage,
                            position: 'topRight'
                        });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}