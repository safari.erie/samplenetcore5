/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 23042020
================================================================================================================= */

//////// START SECTION TABEL COA ///////////
$('#TabelCoa tfoot th').each(function () {
    var title = $(this).text();
    $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
});
var TabelCoa = $('#TabelCoa').DataTable({
    "paging": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "pageLength": 10,
    "lengthChange": true,
    "scrollX": true,
    "processing": true,
    "ajax": {
        "url": base_url + "/Keuangans/GetCoasByJenisAkun?IdJenisAkun=1",
        "method": 'GET',
        "beforeSend": function (xhr) {
        },
        "dataSrc": function (json) {
            if (json.Data == null) {
                iziToast.error({
                    title: 'Gagal Menampilkan Data Tabel COA',
                    message: json.ReturnMessage,
                    position: 'topRight'
                });
                return json;
            } else {
                return json.Data;
            }
        }
    },
    columnDefs: [
        { targets: [0], width: "auto", visible: true },
        { targets: [1], width: "auto", visible: true },
        { targets: [2], width: "auto", visible: true },
        { targets: [3], width: "auto", visible: true },
        { targets: [4], width: "auto", visible: true },
    ],
    "columns": [
        {
            "data": "Kode",
            "render": function (data, type, full, meta) {
                var Data = "";
                var ParamPilih = "TabelCoaLv2('" + full.IdCoa + "')";
                /*var ParamEdit = "EditData('" + full.Kode + "')";*/
                var ParamHapus = "HapusData('" + full.IdCoa + "','" + full.Kode + "','1')";
                Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamPilih + '"><i class="fa fa-eye-alt"></i> Pilih</button>';
                Data += '&nbsp;&nbsp;&nbsp;';
                //Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-alt"></i> Edit</button>';
                //Data += '&nbsp;&nbsp;&nbsp;';
                Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                return Data;
            }
        },
        {
            "data": "JenisAkun",
            "render": function (data, type, full, meta) {
                return full.JenisAkun;
            }
        },
        {
            "data": "JenisTransaksi",
            "render": function (data, type, full, meta) {
                return full.JenisTransaksi;
            }
        },
        {
            "data": "Kode",
            "render": function (data, type, full, meta) {
                return full.Kode;
            }
        },
        {
            "data": "Nama",
            "render": function (data, type, full, meta) {
                return "<strong>"+full.Nama+"</strong>";
            }
        },
    ],
    "bDestroy": true
});
TabelCoa.columns().every(function () {
    var that = this;

    $('input', this.footer()).on('keyup change clear', function () {
        if (that.search() !== this.value) {
            that
                .search(this.value)
                .draw();
        }
    });
});
//////// START SECTION TABEL COA ///////////

function TabelCoaLv2(IdCoa) {
    $("#DivTabelCoaLv2").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#DivTabelCoaLv2").offset().top
    }, 50);
    $("#BtnTambahCoaLv2").attr("onClick", "TambahData('2','" + IdCoa + "');");
    //////// START SECTION TABEL COA UTAMA ///////////
    $('#TabelCoaLv2 tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelCoaLv2 = $('#TabelCoaLv2').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/Keuangans/GetCoasByParent?IdCoa=" + IdCoa,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data TABEL COA UTAMA',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        columnDefs: [
            { targets: [0], width: "auto", visible: true },
            { targets: [1], width: "auto", visible: true },
            { targets: [2], width: "auto", visible: true },
            { targets: [3], width: "auto", visible: true },
            { targets: [4], width: "auto", visible: true },
        ],
        "columns": [
            {
                "data": "Kode",
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamPilih = "TabelCoaLv3('" + full.IdCoa + "')";
                    /*var ParamEdit = "EditData('" + full.Kode + "')";*/
                    var ParamHapus = "HapusData('" + full.IdCoa + "','" + full.Kode + "','2','" + IdCoa + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamPilih + '"><i class="fa fa-eye-alt"></i> Pilih</button>';
                    Data += '&nbsp;&nbsp;&nbsp;';
                    //Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-alt"></i> Edit</button>';
                    //Data += '&nbsp;&nbsp;&nbsp;';
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                    return Data;
                }
            },
            {
                "data": "JenisAkun",
                "render": function (data, type, full, meta) {
                    return full.JenisAkun;
                }
            },
            {
                "data": "JenisTransaksi",
                "render": function (data, type, full, meta) {
                    return full.JenisTransaksi;
                }
            },
            {
                "data": "Kode",
                "render": function (data, type, full, meta) {
                    return full.Kode;
                }
            },
            {
                "data": "Nama",
                "render": function (data, type, full, meta) {
                    return "<strong>" + full.Nama + "</strong>";
                }
            },
        ],
        "bDestroy": true
    });
    TabelCoaLv2.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    //////// START SECTION TABEL COA UTAMA ///////////
}

function TabelCoaLv3(IdCoa) {
    $("#DivTabelCoaLv3").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#DivTabelCoaLv3").offset().top
    }, 50);
    $("#BtnTambahCoaLv3").attr("onClick", "TambahData('3','" + IdCoa + "');");
    //////// START SECTION TABEL COA MADYA ///////////
    $('#TabelCoaLv3 tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelCoaLv3 = $('#TabelCoaLv3').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/Keuangans/GetCoasByParent?IdCoa=" + IdCoa,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data TABEL COA MADYA',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        columnDefs: [
            { targets: [0], width: "auto", visible: true },
            { targets: [1], width: "auto", visible: true },
            { targets: [2], width: "auto", visible: true },
            { targets: [3], width: "auto", visible: true },
            { targets: [4], width: "auto", visible: true },
        ],
        "columns": [
            {
                "data": "Kode",
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamPilih = "TabelCoaLv4('" + full.IdCoa + "')";
                    /*var ParamEdit = "EditData('" + full.Kode + "')";*/
                    var ParamHapus = "HapusData('" + full.IdCoa + "','" + full.Kode + "','3','" + IdCoa + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamPilih + '"><i class="fa fa-eye-alt"></i> Pilih</button>';
                    Data += '&nbsp;&nbsp;&nbsp;';
                    //Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-alt"></i> Edit</button>';
                    //Data += '&nbsp;&nbsp;&nbsp;';
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                    return Data;
                }
            },
            {
                "data": "JenisAkun",
                "render": function (data, type, full, meta) {
                    return full.JenisAkun;
                }
            },
            {
                "data": "JenisTransaksi",
                "render": function (data, type, full, meta) {
                    return full.JenisTransaksi;
                }
            },
            {
                "data": "Kode",
                "render": function (data, type, full, meta) {
                    return full.Kode;
                }
            },
            {
                "data": "Nama",
                "render": function (data, type, full, meta) {
                    return "<strong>" + full.Nama + "</strong>";
                }
            },
        ],
        "bDestroy": true
    });
    TabelCoaLv3.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    //////// START SECTION TABEL COA MADYA ///////////
}

function TabelCoaLv4(IdCoa) {
    $("#DivTabelCoaLv4").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#DivTabelCoaLv4").offset().top
    }, 50);
    $("#BtnTambahCoaLv4").attr("onClick", "TambahData('4','" + IdCoa + "');");
    //////// START SECTION TABEL COA PRATAMA ///////////
    $('#TabelCoaLv4 tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelCoaLv4 = $('#TabelCoaLv4').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/Keuangans/GetCoasByParent?IdCoa=" + IdCoa,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data TABEL COA PRATAMA',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        columnDefs: [
            { targets: [0], width: "auto", visible: true },
            { targets: [1], width: "auto", visible: true },
            { targets: [2], width: "auto", visible: true },
            { targets: [3], width: "auto", visible: true },
            { targets: [4], width: "auto", visible: true },
        ],
        "columns": [
            {
                "data": "Kode",
                "render": function (data, type, full, meta) {
                    var Data = "";
                    // var ParamPilih = "TabelCoaLv5('" + full.IdCoa + "')";
                    var ParamHapus = "HapusData('" + full.IdCoa + "','" + full.Kode + "','4','" + IdCoa + "')";
                    // Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamPilih + '"><i class="fa fa-eye-alt"></i> Pilih</button>';
                    // Data += '&nbsp;&nbsp;&nbsp;';
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                    return Data;
                }
            },
            {
                "data": "JenisAkun",
                "render": function (data, type, full, meta) {
                    return full.JenisAkun;
                }
            },
            {
                "data": "JenisTransaksi",
                "render": function (data, type, full, meta) {
                    return full.JenisTransaksi;
                }
            },
            {
                "data": "Kode",
                "render": function (data, type, full, meta) {
                    return full.Kode;
                }
            },
            {
                "data": "Nama",
                "render": function (data, type, full, meta) {
                    return "<strong>" + full.Nama + "</strong>";
                }
            },
        ],
        "bDestroy": true
    });
    TabelCoaLv4.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    //////// START SECTION TABEL COA PRATAMA ///////////
}

function TabelCoaLv5(IdCoa) {
    $("#DivTabelCoaLv5").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#DivTabelCoaLv5").offset().top
    }, 50);
    $("#BtnTambahCoaLv5").attr("onClick", "TambahData('5','" + IdCoa + "');");
    //////// START SECTION TABEL COA BUKU BESAR ///////////
    $('#TabelCoaLv5 tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelCoaLv5 = $('#TabelCoaLv5').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/Keuangans/GetCoasByParent?IdCoa=" + IdCoa,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data TABEL COA BUKU BESAR',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        columnDefs: [
            { targets: [0], width: "auto", visible: true },
            { targets: [1], width: "auto", visible: true },
            { targets: [2], width: "auto", visible: true },
            { targets: [3], width: "auto", visible: true },
            { targets: [4], width: "auto", visible: true },
        ],
        "columns": [
            {
                "data": "Kode",
                "render": function (data, type, full, meta) {
                    var Data = "";
                    /*var ParamEdit = "EditData('" + full.Kode + "')";*/
                    var ParamHapus = "HapusData('" + full.IdCoa + "','" + full.Kode + "','5','" + IdCoa + "')";
                    //Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-alt"></i> Edit</button>';
                    //Data += '&nbsp;&nbsp;&nbsp;';
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                    return Data;
                }
            },
            {
                "data": "JenisAkun",
                "render": function (data, type, full, meta) {
                    return full.JenisAkun;
                }
            },
            {
                "data": "JenisTransaksi",
                "render": function (data, type, full, meta) {
                    return full.JenisTransaksi;
                }
            },
            {
                "data": "Kode",
                "render": function (data, type, full, meta) {
                    return full.Kode;
                }
            },
            {
                "data": "Nama",
                "render": function (data, type, full, meta) {
                    return "<strong>" + full.Nama + "</strong>";
                }
            },
        ],
        "bDestroy": true
    });
    TabelCoaLv5.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    //////// START SECTION TABEL COA BUKU BESAR ///////////
}


function TambahData(IdJenisAkun, IdParentCoa) {
    $("#ModalFormCoa").modal("show");
    $('#FormCoa')[0].reset();
    $("#BtnSaveCoa").attr("onClick", "SaveData('TambahData','" + IdJenisAkun + "','" + IdParentCoa + "');");
    $("#ModalHeaderFormCoa").html("Tambah COA");
    $("input[name='Kode']").removeAttr("readonly");
    $("input[name='IdParentCoa']", "#FormCoa").val(IdParentCoa).attr("readonly",true);
    //ComboGetParentCoas(function (obj) {
    //    $("select[name='IdParentCoa']").html(obj);
    //}, IdParentCoa);
    ComboGetJenisAkunCoa(function (obj) {
        $("select[name='IdJenisAkun']").html(obj);
    },IdJenisAkun);
    ComboGetJenisTransaksiCoa(function (obj) {
        $("select[name='IdJenisTransaksi']").html(obj);
    });
}

//function EditData(Kode) {
//    $("#ModalFormCoa").modal("show");
//    $('#FormCoa')[0].reset();
    
//    $("#BtnSaveCoa").attr("onClick", "SaveData('EditData');");
//    var Url = base_url + "/Keuangans/GetCoa?Kode=" + Kode;
//    $.ajax({
//        url: Url,
//        type: "GET",
//        dataType: "json",
//        beforeSend: function (beforesend) {
//            ProgressBar("wait");
//        },
//        success: function (responsesuccess) {
//            ProgressBar("success");
//            if (responsesuccess.IsSuccess == true) {
//                var ResponseData = responsesuccess.Data;
//                $("#ModalHeaderFormCoa").html("Edit COA " + ResponseData.Kode);
//                $("input[name='Kode']", "#FormCoa").val(ResponseData.Kode).attr("readonly",true);
//                $("input[name='Nama']", "#FormCoa").val(ResponseData.Nama);

//                ComboGetParentCoas(function (obj) {
//                    $("select[name='IdParentCoa']").html(obj);
//                }, ResponseData.IdParentCoa);

//                ComboGetJenisAkunCoa(function (obj) {
//                    $("select[name='IdJenisAkun']").html(obj);
//                }, ResponseData.IdJenisAkun);

//                ComboGetJenisTransaksiCoa(function (obj) {
//                    $("select[name='IdJenisTransaksi']").html(obj);
//                }, ResponseData.IdJenisTransaksi);

//            } else {
//                swal({ title: 'Gagal :(', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
//            }
//        }, error: function (responserror, a, e) {
//            ProgressBar("success");
//            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
//        }
//    });
//}

function SaveData(Tipe,TabelLevel,IdParentCoa) {
    var formObj = $('#FormCoa').serializeObject();
    if (Tipe == "TambahData") {
        var Url = base_url + "/Keuangans/AddCoa";
    } else if (Tipe == "EditData") {
        var Url = base_url + "/Keuangans/EditCoa";
    }
    var JsonData = JSON.stringify({
        "Kode": formObj.Kode,
        "IdParentCoa": formObj.IdParentCoa,
        "Nama": formObj.Nama,
        "IdJenisAkun": parseInt(formObj.IdJenisAkun),
        "IdJenisTransaksi": parseInt(formObj.IdJenisTransaksi)
    });
    $.ajax({
        url: Url,
        method: "POST",
        data: JsonData,
        dataType: "json",
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $('#FormCoa')[0].reset();
                $("#ModalFormCoa").modal("hide");
                if (TabelLevel == 1) {
                    TabelCoa.ajax.reload();
                } else if (TabelLevel == 2) {
                    TabelCoaLv2(IdParentCoa);
                } else if (TabelLevel == 3) {
                    TabelCoaLv3(IdParentCoa);
                } else if (TabelLevel == 4) {
                    TabelCoaLv4(IdParentCoa);
                } else if (TabelLevel == 5) {
                    TabelCoaLv5(IdParentCoa);
                }
                swal({ title: 'Berhasil Menyimpan Coa', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal Menyimpan Coa', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function HapusData(IdCoa,Kode,TabelLevel,IdParentCoa) {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus data COA dengan Kode " + Kode,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            var Url = base_url + "/Keuangans/DeleteCoa?IdCoa=" + IdCoa;
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesave) {
                    ProgressBar("success");
                    if (responsesave.IsSuccess == true) {
                        if (TabelLevel == 1) {
                            TabelCoa.ajax.reload();
                        } else if (TabelLevel == 2) {
                            TabelCoaLv2(IdParentCoa);
                        } else if (TabelLevel == 3) {
                            TabelCoaLv3(IdParentCoa);
                        } else if (TabelLevel == 4) {
                            TabelCoaLv4(IdParentCoa);
                        } else if (TabelLevel == 5) {
                            TabelCoaLv5(IdParentCoa);
                        } 
                        
                        swal({ title: 'Berhasil Menghapus COA', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesave.IsSuccess == false) {
                        swal({ title: 'Gagal Menghapus COA', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}