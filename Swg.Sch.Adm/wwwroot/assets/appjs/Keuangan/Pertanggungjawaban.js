/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 14042020
================================================================================================================= */
ComboGetTahunAjaran(function (obj) {
    $("select[name='IdTahunAjaran']").html(obj);
});

function TabelPertanggungjawaban(){
    var formObj = $('#FormAksi').serializeObject();
    $("#MainTableEmpty").css("display","none");
    $("#MainTable").css("display","block");
    //////// START SECTION TABEL PERTANGGUNGJAWABAN ///////////
    $('#TabelPertanggungjawaban tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelPertanggungjawaban = $('#TabelPertanggungjawaban').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/Keuangans/GetPertanggungjawabans?IdTahunAjaran="+parseInt(formObj.IdTahunAjaran),
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Pertanggungjawaban',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    var JmlBtnAksi = full.Aksis.length;
                    var Data = '';
                    var Param = '';
                    for (var i = 0; i < JmlBtnAksi; i++) {
                        Param = "AksiTabelPertanggungjawaban('" + full.Aksis[i] + "','" + full.IdPengajuan + "','" + full.NamaUnit + "','" + full.Program + "','" + full.Kegiatan + "','" + full.Nomor + "')";
                        Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + Param + '">' + full.Aksis[i] + '</button>';
                        Data += '&nbsp;&nbsp;';
                    }
                    return Data;
                }
            },
            { "data": "NamaUnit" },
            { "data": "Program" },
            { "data": "Kegiatan" },
            { "data": "Nomor" },
            {
                "render": function (data, type, full, meta) {
                    var Data = '<span>' + indoDate(full.TanggalButuh) + '</span>';
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = '<span>' + formatRupiah(full.TotalRp) + '</span>';
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = '<span>' + formatRupiah(full.TotalRealisasiRp) + '</span>';
                    return Data;
                }
            },
            { "data": "Status" },
            { "data": "TanggalDilaporkan" },
            {
                "render": function (data, type, full, meta) {
                    var Data = '<span>' + formatRupiah(full.TotalRpDilaporkan) + '</span>';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });
    TabelPertanggungjawaban.columns().every(function () {
        var that = this;
    
        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
    //////// START SECTION TABEL PERTANGGUNGJAWABAN ///////////
}

function AksiTabelPertanggungjawaban(Aksi, IdPengajuan, Unit, Program, Kegiatan, Nomor) {
    var Url = "";
    if (Aksi == "View") {
        TabelPertanggungjawabanCoa(IdPengajuan, Program, Kegiatan, Nomor);
    } else if (Aksi == "Download") {
        DownloadPertanggungjawaban(IdPengajuan, Nomor);
    } else if (Aksi == "Hapus") {
        HapusPertanggungjawaban(IdPengajuan,Unit,Nomor);
    } else if (Aksi == "Dilaporkan") {
        SetPertanggungjawaban(Aksi,IdPengajuan,Unit,Nomor);
    } else if (Aksi == "Dibukukan") {
        Setujui(IdPengajuan,Unit,Nomor,'SetDibukukan');
    } 
}

function DownloadPertanggungjawaban(IdPengajuan,Nomor){
    var url = base_url + "/Keuangans/DownloadPtgjwb?IdPengajuan=" +IdPengajuan;
    window.open(url, '_blank');
}

function Setujui(IdPengajuan,Unit,Nomor,Api) {
    $("#ModalFormSetujui").modal("show");
    $("#ModalHeaderFormSetujui").text("Pembukuan Pertanggungjawaban");

    $("input[name='IdPengajuan']", "#FormSetujui").val(IdPengajuan)
    $("input[name='Unit']", "#FormSetujui").val(Unit)
    $("input[name='Nomor']", "#FormSetujui").val(Nomor)
    $("#BtnSetujui").attr("onClick", "SaveSetujui('" + Api +"')");
}

function SaveSetujui(Api) {
    var formObj = $('#FormSetujui').serializeObject();
    var Url = base_url + "/Keuangans/" + Api + "?IdPengajuan=" + formObj.IdPengajuan + "&StatusProses=" + formObj.StatusProses + "&Catatan=" + formObj.Catatan;
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan mem-bukukan Pertanggungjawaban unit " + formObj.Unit + " nomor "+formObj.Nomor,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        TabelPertanggungjawaban();
                        $("#ModalFormSetujui").modal("hide");
                        swal({ title: 'Berhasil Bukukan Pertanggungjawaban', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesuccess.IsSuccess == false) {
                        swal({ title: 'Gagal Setujui Pertanggungjawaban', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}

function SetPertanggungjawaban(Aksi,IdPengajuan,Unit,Nomor) {
    var Proses = "";
    if(Aksi == "Dilaporkan"){
        Proses = "pelaporan";
        var Url = base_url + "/Keuangans/SetDilaporkan?IdPengajuan="+IdPengajuan;
    } 
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan melakukan "+Proses+" data pertanggungjawaban unit "+Unit+ " nomor " + Nomor,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        TabelPertanggungjawaban();
                        swal({ title: 'Berhasil melakukan '+Proses+' pertanggungjawaban', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesuccess.IsSuccess == false) {
                        swal({ title: 'Gagal melakukan '+Proses+' pertanggungjawaban', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
    
}

function HapusPertanggungjawaban(IdPengajuan,Unit,Nomor) {
    var Url = base_url + "/Keuangans/PertanggungjawabanDelete?IdPengajuan=" + parseInt(IdPengajuan);
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus data pertanggungjawaban unit "+Unit+ " nomor " + Nomor,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        TabelPertanggungjawaban();
                        swal({ title: 'Berhasil Hapus Pertanggungjawaban', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesuccess.IsSuccess == false) {
                        swal({ title: 'Gagal Hapus Pertanggungjawaban', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}

function TambahPertanggungjawaban(){
    $("#ModalPertanggungjawabanPengajuan").modal("show");
    $("#ModalHeaderPertanggungjawabanPengajuan").text("Tambah Pertanggungjawaban");

    $("#TablePengajuanEmpty").removeAttr("style");
    $("#TablePengajuan").css("display", "none");
    $("#DivSectionPengajuanCoa").css("display", "none");
}

function TabelPengajuan(){
    var formObj = $('#FormAksiPengajuan').serializeObject();
    $("#TablePengajuanEmpty").css("display","none");
    $("#TablePengajuan").css("display","block");
    //////// START SECTION TABEL PENGAJUAN BIAYA ///////////
    var Url = base_url + "/Keuangans/PtgjwbGetPengajuans?IdTahunAjaran="+parseInt(formObj.IdTahunAjaran);
    $('#TabelPengajuan tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelPengajuan = $('#TabelPengajuan').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": Url,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Pengajuan Biaya',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    var JmlBtnAksi = full.Aksis.length;
                    var Data = '';
                    var Param = '';
                    for (var i = 0; i < JmlBtnAksi; i++) {
                        Param = "AksiTabelPengajuan('" + full.Aksis[i] + "','" + full.IdPengajuan + "','" + full.Nomor + "','" + full.Program + "','" + full.Kegiatan + "')";
                        Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + Param + '">' + full.Aksis[i] + '</button>';
                        Data += '&nbsp;&nbsp;';
                    }
                    return Data;
                }
            },
            { "data": "NamaUnit" },
            { "data": "TahunAjaran" },
            { "data": "Nomor" },
            { "data": "Program" },
            { "data": "Kegiatan" },
            { "data": "TanggalButuh" },
            { "data": "Status" },
            { "data": "StatusProses" },
            {
                "render": function (data, type, full, meta) {
                    var Data = '<span>' + formatRupiah(full.TotalRp) + '</span>';
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = '<span>' + formatRupiah(full.TotalRealisasiRp) + '</span>';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });
    TabelPengajuan.columns().every(function () {
        var that = this;
    
        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
    //////// START SECTION TABEL PENGAJUAN BIAYA ///////////
}

function AksiTabelPengajuan(Aksi, IdPengajuan, Nomor, Program, Kegiatan) {
    if (Aksi == "View") {
        TabelPengajuanCoa(IdPengajuan,Program, Kegiatan);
    } else if (Aksi == "Download") {
        DownloadPengajuan(IdPengajuan, Nomor);
    } else if (Aksi == "Pilih") {
        PilihPengajuan(IdPengajuan,Nomor);
    }  
}

function PilihPengajuan(IdPengajuan,Nomor){
    var Url = base_url + "/Keuangans/SetPilihPengajuan?IdPengajuan=" + parseInt(IdPengajuan);
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda memilih pengajuan nomor " + Nomor + " untuk di tambah ke pertanggungjawaban",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        TabelPengajuan();
                        swal({ title: 'Berhasil Memilih Pengajuan', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesuccess.IsSuccess == false) {
                        swal({ title: 'Gagal Memilih Pengajuan', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}

function TabelPengajuanCoa(IdPengajuan,Program,Kegiatan) {
    $("#DivSectionPengajuanCoa").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#DivSectionPengajuanCoa").offset().top
    }, 50);
    $("#HeadingPengajuanBiayaCoa").text("Daftar Akun "+Program+" - "+Kegiatan)
    //////// START SECTION TABEL PENGAJUAN COA ///////////
    var Url = base_url + "/Keuangans/PtgjwbGetPengajuanCoas?IdPengajuan=" + parseInt(IdPengajuan);
    console.log(Url);
    $('#TabelPengajuanCoa tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelPengajuanCoa = $('#TabelPengajuanCoa').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": Url,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Data COA',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        columnDefs: [
            { targets: [0], width: "auto", visible: true },
            { targets: [1], width: "auto", visible: true },
            { targets: [2], width: "auto", visible: true },
            { targets: [3], width: "auto", visible: true },
            { targets: [4], width: "auto", visible: true },
            { targets: [5], width: "auto", visible: true },
            { targets: [6], width: "auto", visible: true },
            { targets: [7], width: "auto", visible: true },
        ],
        "columns": [
            { "data": "KodeCoa" },
            { "data": "NamaCoa" },
            { "data": "JumlahSatuan" },
            { "data": "Satuan" },
            { "data": "JumlahVolume" },
            { "data": "Volume" },
            {
                "render": function (data, type, full, meta) {
                    return formatRupiah(full.HargaSatuan);
                }
            },
            {
                "render": function (data, type, full, meta) {
                    return formatRupiah(full.Rp);
                }
            },
        ],
        "bDestroy": true
    });
    TabelPengajuanCoa.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    //////// START SECTION TABEL PENGAJUAN COA ///////////
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

/// END MODAL PERTANGGUNGJAWABAN PENGAJUAN ////

function TabelPertanggungjawabanCoa(IdPengajuan,Program,Kegiatan,Nomor) {
    $("#DivSectionAkun").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#DivSectionAkun").offset().top
    }, 50);
    $("#HeadingPertanggungjawabanCoa").text("Daftar Akun "+Program+" - "+Kegiatan+" Nomor "+Nomor)
    //////// START SECTION TABEL PERTANGGUNGJAWABAN COA ///////////
    var Url = base_url + "/Keuangans/GetPertanggungjawabanCoas?IdPengajuan=" + parseInt(IdPengajuan);
    console.log(Url);
    $('#TabelPertanggungjawabanCoa tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelPertanggungjawabanCoa = $('#TabelPertanggungjawabanCoa').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
         "dom": "Bfrtip",
         buttons: [
             {
                 text: '<i class="fa fa-plus"></i> Tambah Akun',
                 className: 'btn btn-success btn-flat mr-2 mb-2 text-right float-right',
                 action: function (e, dt, node, config) {
                     TambahPertanggungjawabanCoa(IdPengajuan,Program,Kegiatan,Nomor);
                 },
                 init: function (api, node, config) {
                     $(node).removeClass('dt-button');
                 }
             }
         ],
        "ajax": {
            "url": Url,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Tabel Data COA',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        columnDefs: [
            { targets: [0], width: "auto", visible: true },
            { targets: [1], width: "auto", visible: true },
            { targets: [2], width: "auto", visible: true },
            { targets: [3], width: "auto", visible: true },
            { targets: [4], width: "auto", visible: true },
            { targets: [5], width: "auto", visible: true },
            { targets: [6], width: "auto", visible: true },
            { targets: [7], width: "auto", visible: true },
            { targets: [8], width: "auto", visible: true },
        ],
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    var Param = "'"+IdPengajuan+"','"+full.IdPtgjwbCoa+"','"+full.IdCoa+"','"+full.Catatan+"','"+full.IdJenisSatuan+"','"+full.JumlahSatuan+"','"+full.IdJenisVolume+"','"+full.JumlahVolume+"','"+full.HargaSatuan+"','"+full.Rp+"','"+full.KodeCoa+"','"+full.NamaCoa+"','"+Program+"','"+Kegiatan+"','"+Nomor+"'";
                    var ParamRenpenCoaEdit = "EditPertanggungjawabanCoa(" + Param + ")";
                    var ParamRenpenCoaHapus = "HapusPertanggungjawabanCoa(" + Param + ")";
                    var Data = '';
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamRenpenCoaEdit + '"><i class="fa fa-pencil-alt"></i> Edit</button>';
                    Data += '&nbsp;&nbsp;';
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamRenpenCoaHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                    return Data;
                }
            },
            { "data": "KodeCoa" },
            { "data": "NamaCoa" },
            { "data": "JumlahSatuan" },
            { "data": "Satuan" },
            { "data": "JumlahVolume" },
            { "data": "Volume" },
            {
                "render": function (data, type, full, meta) {
                    return formatRupiah(full.HargaSatuan);
                }
            },
            {
                "render": function (data, type, full, meta) {
                    return formatRupiah(full.Rp);
                }
            },
        ],
        "bDestroy": true
    });
    TabelPertanggungjawabanCoa.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    //////// START SECTION TABEL PERTANGGUNGJAWABAN COA ///////////
}

function TambahPertanggungjawabanCoa(IdPengajuan,Program,Kegiatan,Nomor) {
    $('#FormPertanggungjawabanCoa')[0].reset();
    $("#ModalFormPertanggungjawabanCoa").modal("show");
    $("#BtnSavePertanggungjawabanCoa").attr("onClick", "SavePertanggungjawabanCoa('TambahData')");
    $("#ModalHeaderFormPertanggungjawabanCoa").text("Tambah COA Pertanggungjawaban");

    $("input[name='Program']", "#FormPertanggungjawabanCoa").val(Program);
    $("input[name='Kegiatan']", "#FormPertanggungjawabanCoa").val(Kegiatan);
    $("input[name='Nomor']", "#FormPertanggungjawabanCoa").val(Nomor);

    $("input[name='IdPengajuan']", "#FormPertanggungjawabanCoa").val(IdPengajuan);
    GetCoaPengajuans(function (HtmlCombo) {
        $("select[name='IdCoa']", "#FormPertanggungjawabanCoa").html(HtmlCombo);
    });
    GetJenisSatuans(function (HtmlCombo) {
        $("select[name='IdJenisSatuan']", "#FormPertanggungjawabanCoa").html(HtmlCombo);
    });
    GetJenisVolumes(function (HtmlCombo) {
        $("select[name='IdJenisVolume']", "#FormPertanggungjawabanCoa").html(HtmlCombo);
    });
}

function EditPertanggungjawabanCoa(
    IdPengajuan,
    IdPtgjwbCoa,
    IdCoa,
    Catatan,
    IdJenisSatuan,
    JumlahSatuan,
    IdJenisVolume,
    JumlahVolume,
    HargaSatuan,
    Rp,
    KodeCoa,
    NamaCoa,
    Program, Kegiatan, Nomor) {
    $('#FormPertanggungjawabanCoa')[0].reset();
    $("#ModalFormPertanggungjawabanCoa").modal("show");
    $("#BtnSavePertanggungjawabanCoa").attr("onClick", "SavePertanggungjawabanCoa('EditData')");
    
    $("#ModalHeaderFormPertanggungjawabanCoa").text("Edit COA Pertanggungjawaban");
    
    $("input[name='Program']", "#FormPertanggungjawabanCoa").val(Program);
    $("input[name='Kegiatan']", "#FormPertanggungjawabanCoa").val(Kegiatan);
    $("input[name='Nomor']", "#FormPertanggungjawabanCoa").val(Nomor);

    $("input[name='IdPengajuan']", "#FormPertanggungjawabanCoa").val(IdPengajuan);
    $("input[name='IdPtgjwbCoa']", "#FormPertanggungjawabanCoa").val(IdPtgjwbCoa);
    $("input[name='JumlahSatuan']", "#FormPertanggungjawabanCoa").val(JumlahSatuan);
    $("input[name='JumlahVolume']", "#FormPertanggungjawabanCoa").val(JumlahVolume);
    $("input[name='HargaSatuan']", "#FormPertanggungjawabanCoa").val(HargaSatuan);
    $("textarea[name='Catatan']", "#FormPertanggungjawabanCoa").val(Catatan);
    GetCoaPengajuans(function (HtmlCombo) {
        $("select[name='IdCoa']", "#FormPertanggungjawabanCoa").html(HtmlCombo);
    },IdCoa);
    GetJenisSatuans(function (HtmlCombo) {
        $("select[name='IdJenisSatuan']", "#FormPertanggungjawabanCoa").html(HtmlCombo);
    },IdJenisSatuan);
    GetJenisVolumes(function (HtmlCombo) {
        $("select[name='IdJenisVolume']", "#FormPertanggungjawabanCoa").html(HtmlCombo);
    },IdJenisVolume);
}

function SavePertanggungjawabanCoa(Tipe) {
    var formData = new FormData($('#FormPertanggungjawabanCoa')[0]);
    var formObj = $('#FormPertanggungjawabanCoa').serializeObject();
    if(Tipe == "TambahData"){
        var Url = base_url + "/Keuangans/PertanggungjawabanCoaAdd";
    } else if(Tipe == "EditData"){
        var Url = base_url + "/Keuangans/PertanggungjawabanCoaEdit";
    }
    $.ajax({
        url: Url,
        type: "POST",
        data: formData,
        dataType: "json",
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (responsebefore) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                TabelPertanggungjawabanCoa(formObj.IdPengajuan,formObj.Program,formObj.Kegiatan,formObj.Nomor);

                $('#FormPertanggungjawabanCoa')[0].reset();
                $("#ModalFormPertanggungjawabanCoa").modal("hide");
                swal({ title: 'Berhasil Simpan COA Pertanggungjawaban', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesuccess.IsSuccess == false) {
                swal({ title: 'Gagal Simpan COA Pertanggungjawaban', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
    
}

function HapusPertanggungjawabanCoa(IdPengajuan,
    IdPtgjwbCoa,
    IdCoa,
    Catatan,
    IdJenisSatuan,
    JumlahSatuan,
    IdJenisVolume,
    JumlahVolume,
    HargaSatuan,
    Rp,
    KodeCoa,
    NamaCoa,
    Program, Kegiatan, Nomor) {
    var Url = base_url + "/Keuangans/PertanggungjawabanCoaDelete?IdPtgjwbCoa=" + parseInt(IdPtgjwbCoa);
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus data coa " + KodeCoa,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        TabelPertanggungjawabanCoa(IdPengajuan,Program, Kegiatan, Nomor);
                        swal({ title: 'Berhasil Hapus COA Pertanggungjawaban', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesuccess.IsSuccess == false) {
                        swal({ title: 'Gagal Hapus COA Pertanggungjawaban', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}