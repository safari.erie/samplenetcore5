/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 14042020
================================================================================================================= */
ComboGetTahunAjaran(function (obj) {
    $("select[name='IdTahunAjaran']").html(obj);
});

ComboGetUnit(function (obj) {
    $("select[name='IdUnit']").html(obj);
});

function TabelPaguAnggaran(){
    var formObj = $('#FormAksi').serializeObject();
    $("#MainTableEmpty").css("display","none");
    $("#MainTable").css("display","block");
    //////// START SECTION TABEL PAGU ANGGARAN ///////////
    var Url = base_url + "/Keuangans/GetUnitCoaPagus?IdTahunAjaran="+parseInt(formObj.IdTahunAjaran)+"&IdUnit="+parseInt(formObj.IdUnit);
    $('#TabelPaguAnggaran tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelPaguAnggaran = $('#TabelPaguAnggaran').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 5,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": Url,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Pagu Anggaran',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    var Data = '';
                    var ParamView = "View('" + full.IdUnitCoaPagu + "','" + full.NamaCoaRenPen + "')";
                    var ParamEdit = "Edit('" + full.IdUnitCoaPagu + "','" + full.Unit + "','" + full.NamaCoaRenPen + "','" + full.RpPagu + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamView + '">View</button>';
                    Data += '&nbsp;&nbsp;';
                    Data += '<button type="button" class="btn btn-primary btn-sm" onClick="' + ParamEdit + '">Edit</button>';
                    return Data;
                }
            },
            { "data": "Unit" },
            { "data": "TahunAjaran" },
            { "data": "NamaCoaRenPen" },
            {
                "render": function (data, type, full, meta) {
                    return formatRupiah(full.RpPagu);
                }
            },
            {
                "render": function (data, type, full, meta) {
                    return formatRupiah(full.RpPaguUsed);
                }
            },
        ],
        "bDestroy": true
    });
    TabelPaguAnggaran.columns().every(function () {
        var that = this;
    
        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    //////// START SECTION TABEL PAGU ANGGARAN ///////////
}

function View(IdUnitCoaPagu,NamaCoaRenPen){
    $("#DaftarPengeluaranAkunPagu").css("display","block");
    $("#HeadingDaftarPengeluaranAkunPagu").text("Daftar Akun Pengeluaran (Biaya) untuk Akun Pagu "+NamaCoaRenPen);
    $('html, body').animate({
        scrollTop: $("#DaftarPengeluaranAkunPagu").offset().top
    }, 50);
    var Url = base_url + "/Keuangans/GetUnitCoaPaguProKegs?IdUnitCoaPagu=" + parseInt(IdUnitCoaPagu);
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var HtmlData = "";
                $.each(responsesuccess.Data, function( key, value ) {
                    HtmlData += value+"&nbsp;";
                    HtmlData += "<br>";
                 });

                $("#DataDaftarPengeluaranAkunPagu").html(HtmlData);
            } else if (responsesuccess.IsSuccess == false) {
                swal({ title: 'Gagal Menampilkan Daftar Akun Pengeluaran', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function Edit(IdUnitCoaPagu,Unit,NamaCoaRenPen,RpPagu){
    $("#ModalFormPaguAnggaran").modal("show");
    $("#BtnSavePaguAnggaran").attr("onClick", "SaveData('EditData')");

    $("#ModalHeaderFormPaguAnggaran").text("Edit Data Pagu Anggaran");
    $("input[name='IdUnitCoaPagu']", "#FormPaguAnggaran").val(IdUnitCoaPagu);
    $("input[name='Unit']", "#FormPaguAnggaran").val(Unit);
    $("input[name='Akun']", "#FormPaguAnggaran").val(NamaCoaRenPen);
    $("input[name='RpPagu']", "#FormPaguAnggaran").val(RpPagu);
}

function SaveData(Tipe) {
    var formData = new FormData($('#FormPaguAnggaran')[0]);
    var formObj = $('#FormAksi').serializeObject();
    if(Tipe == "EditData"){
        var Url = base_url + "/Keuangans/EditCoaPagu";
    }
    $.ajax({
        url: Url,
        type: "POST",
        data: formData,
        dataType: "json",
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (responsebefore) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                TabelPaguAnggaran(formObj.IdUnitCoaPagu,formObj.IdUnit);

                $('#FormPaguAnggaran')[0].reset();
                $("#ModalFormPaguAnggaran").modal("hide");
                swal({ title: 'Berhasil Simpan Pagu Anggaran', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesuccess.IsSuccess == false) {
                swal({ title: 'Gagal Simpan Pagu Anggaran', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
    
}