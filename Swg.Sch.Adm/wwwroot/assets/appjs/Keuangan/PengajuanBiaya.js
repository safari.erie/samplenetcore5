/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 14042020
================================================================================================================= */
ComboGetTahunAjaran(function (obj) {
    $("select[name='IdTahunAjaran']").html(obj);
});

function TabelPengajuan(){
    var formObj = $('#FormAksi').serializeObject();
    $("#MainTableEmpty").css("display","none");
    $("#MainTable").css("display","block");
    //////// START SECTION TABEL PENGAJUAN BIAYA ///////////
    $('#TabelPengajuan tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelPengajuan = $('#TabelPengajuan').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/Keuangans/GetPengajuans?IdTahunAjaran="+parseInt(formObj.IdTahunAjaran),
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Pengajuan Biaya',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    var JmlBtnAksi = full.Aksis.length;
                    var Data = '';
                    var Param = '';
                    for (var i = 0; i < JmlBtnAksi; i++) {
                        Param = "AksiTabelPengajuan('" + full.Aksis[i] + "','" + full.IdPengajuan + "','" + full.NamaUnit + "','" + full.Nomor + "','" + full.TanggalButuh + "')";
                        Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + Param + '">' + full.Aksis[i] + '</button>';
                        Data += '&nbsp;&nbsp;';
                    }
                    return Data;
                }
            },
            { "data": "NamaUnit" },
            { "data": "TahunAjaran" },
            { "data": "Nomor" },
            { "data": "Program" },
            { "data": "Kegiatan" },
            { "data": "TanggalButuh" },
            { "data": "Status" },
            { "data": "StatusProses" },
            {
                "render": function (data, type, full, meta) {
                    var Data = '<span>' + formatRupiah(full.TotalRp) + '</span>';
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = '<span>' + formatRupiah(full.TotalRealisasiRp) + '</span>';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });
    TabelPengajuan.columns().every(function () {
        var that = this;
    
        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    //////// START SECTION TABEL PENGAJUAN BIAYA ///////////
}

function AksiTabelPengajuan(Aksi, IdPengajuan, Unit, Nomor,Tanggal) {
    var Url = "";
    if (Aksi == "View") {
        TabelPengajuanCoa(IdPengajuan, Unit);
    } else if (Aksi == "Download") {
        DownloadPengajuan(IdPengajuan, Nomor);
    } else if (Aksi == "Edit") {
        EditPengajuan(IdPengajuan,Nomor,Tanggal);
    } else if (Aksi == "Hapus") {
        HapusPengajuan(IdPengajuan,Unit,Nomor);
    } else if (Aksi == "Approve Ka Unit") {
        Setujui(IdPengajuan, Unit, 'PengajuanApproveByKaUnit');
    } else if (Aksi == "Approve Mgr Unit") {
        Setujui(IdPengajuan, Unit, 'PengajuanApproveByManUnit');
    } else if (Aksi == "Approve Mgr Keu") {
        Setujui(IdPengajuan, Unit, 'PengajuanApproveByManKeu');
    } else if (Aksi == "Approve Direktur") {
        Setujui(IdPengajuan, Unit, 'PengajuanApproveByDirektur');
    } else if (Aksi == "Realisasikan") {
        Url = base_url + "/Keuangans/PengajuanSetRealisasi?IdPengajuan=" + IdPengajuan;
        SaveRealisasi(Unit, Url);
    }  
}

function DownloadPengajuan(IdPengajuan,Nomor){
    var url = base_url + "/Keuangans/DownloadPengajuan?IdPengajuan=" +IdPengajuan;
    window.open(url, '_blank');
}

function TambahPengajuan(){
    $("#SectionAdd").removeAttr("style")
    $("#SectionAddEdit").css("display","none")
    $("#ComboJenis").css("display","none")

    $("#ModalFormPengajuan").modal("show");
    $("#BtnSavePengajuan").attr("onClick", "SetCoaPengajuan()");
    $("#ModalHeaderFormPengajuan").text("Tambah Data Pengajuan");

    GetPengajuanPrograms(function (HtmlCombo) {
        $("select[name='IdProgram']", "#FormPengajuan").html(HtmlCombo);
    });

    $("select[name='IdProgram']").change(function () {
        if(this.value != ""){
            $("#ComboJenis").removeAttr("style")
        } else {
            $("#ComboJenis").css("display","none")
        }
    });

    $("input[name='Jenis']").change(function () {
        var formPengajuanObj = $('#FormPengajuan').serializeObject();
        if(formPengajuanObj.IdTahunAjaran != "" && formPengajuanObj.IdProgram != ""){
            $("#SectionAddEdit").removeAttr("style")

            if(this.value == "Wajib"){
                $("#ComboProgKegWajib").removeAttr("style")
                $("#ComboProgKegTambahan").css("display","none")
                GetProgramKegiatanWajibs(function (HtmlCombo) {
                    $("select[name='IdUnitProKegWajib']", "#FormPengajuan").html(HtmlCombo);
                },formPengajuanObj.IdTahunAjaran,formPengajuanObj.IdProgram);
            } else if(this.value == "Tambahan"){
                $("#ComboProgKegWajib").css("display","none")
                $("#ComboProgKegTambahan").removeAttr("style");
                GetProgramKegiatanTambahans(function (HtmlCombo) {
                    $("select[name='IdUnitProKegTambahan']", "#FormPengajuan").html(HtmlCombo);
                },formPengajuanObj.IdTahunAjaran,formPengajuanObj.IdProgram);
            }
        }
    });
}

function EditPengajuan(IdPengajuan,Nomor,Tanggal){
    $("#SectionAdd").css("display","none")
    $("#SectionAddEdit").removeAttr("style")

    $("#ModalFormPengajuan").modal("show");
    $("#BtnSavePengajuan").attr("onClick", "SavePengajuan()");
    $("#ModalHeaderFormPengajuan").text("Edit Data Pengajuan");

    $("input[name='IdPengajuan']", "#FormPengajuan").val(IdPengajuan);
    $("input[name='Nomor']", "#FormPengajuan").val(Nomor);
    $("input[name='Tanggal']", "#FormPengajuan").val(Tanggal);
}

function SavePengajuan(){
    var formData = new FormData($('#FormPengajuan')[0]);
    var Url = base_url + "/Keuangans/PengajuanEdit";
    $.ajax({
        url: Url,
        type: "POST",
        data: formData,
        dataType: "json",
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (responsebefore) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                TabelPengajuan();

                $('#FormPengajuan')[0].reset();
                $("#ModalFormPengajuan").modal("hide");
                swal({ title: 'Berhasil Menyimpan Pengajuan', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesuccess.IsSuccess == false) {
                swal({ title: 'Gagal Menyimpan Pengajuan', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function SetCoaPengajuan(){
    var formPengajuanObj = $('#FormPengajuan').serializeObject();
    var formData = new FormData($('#FormPengajuan')[0]);
    var Url = "";
    if(formPengajuanObj.Jenis == "Wajib"){
        Url = base_url + "/Keuangans/SetCoaPengajuanWajib";
    } else if(formPengajuanObj.Jenis == "Tambahan"){
        Url = base_url + "/Keuangans/SetCoaPengajuanTambahan";
    }
    $.ajax({
        url: Url,
        type: "POST",
        data: formData,
        dataType: "json",
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (responsebefore) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                TabelPengajuan();

                $('#FormPengajuan')[0].reset();
                $("#ModalFormPengajuan").modal("hide");
                swal({ title: 'Berhasil Set COA Pengajuan Biaya', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesuccess.IsSuccess == false) {
                swal({ title: 'Gagal Set COA Pengajuan Biaya', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function HapusPengajuan(IdPengajuan,Unit,Nomor) {
    var Url = base_url + "/Keuangans/PengajuanDelete?IdPengajuan=" + parseInt(IdPengajuan);
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus data pengajuan unit "+ Unit+" dengan nomor " + Nomor,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        TabelPengajuan();
                        swal({ title: 'Berhasil Hapus Pengajuan', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesuccess.IsSuccess == false) {
                        swal({ title: 'Gagal Hapus Pengajuan', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}

function SaveRealisasi(Unit, Url) {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan me-realisasi-kan Pengajuan Biaya unit " + Unit,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        TabelPengajuan();
                        swal({ title: 'Berhasil Me-realisasi-kan Pengajuan Biaya', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesuccess.IsSuccess == false) {
                        swal({ title: 'Gagal Me-realisasi-kan Pengajuan Biaya', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}

function Setujui(IdPengajuan,Unit,Api) {
    $("#ModalFormSetujui").modal("show");
    $("#ModalHeaderFormSetujui").text("Approve Pengajuan Biaya & Pagu");

    $("input[name='IdPengajuan']", "#FormSetujui").val(IdPengajuan)
    $("input[name='Unit']", "#FormSetujui").val(Unit)
    $("#BtnSetujui").attr("onClick", "SaveSetujui('" + Api +"')");
}

function SaveSetujui(Api) {
    var formObj = $('#FormSetujui').serializeObject();
    var Url = base_url + "/Keuangans/" + Api + "?IdPengajuan=" + formObj.IdPengajuan + "&StatusProses=" + formObj.StatusProses + "&Catatan=" + formObj.Catatan;
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menyetujui Pengajuan Biaya unit " + formObj.Unit,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        $("#TabelPengajuan").DataTable().ajax.reload();
                        $("#ModalFormSetujui").modal("hide");
                        swal({ title: 'Berhasil Setujui Pengajuan Biaya', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesuccess.IsSuccess == false) {
                        swal({ title: 'Gagal Setujui Pengajuan Biaya', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}


function TabelPengajuanCoa(IdPengajuan) {
    $("#DivSectionAkun").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#DivSectionAkun").offset().top
    }, 50);
    //////// START SECTION TABEL PENGAJUAN COA ///////////
    var Url = base_url + "/Keuangans/GetPengajuanCoas?IdPengajuan=" + parseInt(IdPengajuan);
    console.log(Url);
    $('#TabelPengajuanCoa tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelPengajuanCoa = $('#TabelPengajuanCoa').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
         "dom": "Bfrtip",
         buttons: [
             {
                 text: '<i class="fa fa-plus"></i> Tambah Akun',
                 className: 'btn btn-success btn-flat mr-2 mb-2 text-right float-right',
                 action: function (e, dt, node, config) {
                     TambahPengajuanCoa(IdPengajuan);
                 },
                 init: function (api, node, config) {
                     $(node).removeClass('dt-button');
                 }
             }
         ],
        "ajax": {
            "url": Url,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Data COA',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        columnDefs: [
            { targets: [0], width: "auto", visible: true },
            { targets: [1], width: "auto", visible: true },
            { targets: [2], width: "auto", visible: true },
            { targets: [3], width: "auto", visible: true },
            { targets: [4], width: "auto", visible: true },
            { targets: [5], width: "auto", visible: true },
            { targets: [6], width: "auto", visible: true },
            { targets: [7], width: "auto", visible: true },
            { targets: [8], width: "auto", visible: true },
        ],
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    var Param = "'"+IdPengajuan+"','"+full.IdPengajuanCoa+"','"+full.IdCoa+"','"+full.Catatan+"','"+full.IdJenisSatuan+"','"+full.JumlahSatuan+"','"+full.IdJenisVolume+"','"+full.JumlahVolume+"','"+full.HargaSatuan+"','"+full.Rp+"','"+full.KodeCoa+"','"+full.NamaCoa+"'";
                    var ParamRenpenCoaEdit = "EditPengajuanCoa(" + Param + ")";
                    var ParamRenpenCoaHapus = "HapusPengajuanCoa(" + Param + ")";
                    var Data = '';
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamRenpenCoaEdit + '"><i class="fa fa-pencil-alt"></i> Edit</button>';
                    Data += '&nbsp;&nbsp;';
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamRenpenCoaHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                    return Data;
                }
            },
            { "data": "KodeCoa" },
            { "data": "NamaCoa" },
            { "data": "JumlahSatuan" },
            { "data": "Satuan" },
            { "data": "JumlahVolume" },
            { "data": "Volume" },
            {
                "render": function (data, type, full, meta) {
                    return formatRupiah(full.HargaSatuan);
                }
            },
            {
                "render": function (data, type, full, meta) {
                    return formatRupiah(full.Rp);
                }
            },
        ],
        "bDestroy": true
    });
    TabelPengajuanCoa.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    //////// START SECTION TABEL PENGAJUAN COA ///////////
}

function TambahPengajuanCoa(IdPengajuan) {
    $('#FormPengajuanCoa')[0].reset();
    $("#ModalFormPengajuanCoa").modal("show");
    $("#BtnSavePengajuanCoa").attr("onClick", "SavePengajuanCoa('TambahData')");
    $("#ModalHeaderFormPengajuanCoa").text("Tambah COA Pengajuan");

    $("input[name='IdPengajuan']", "#FormPengajuanCoa").val(IdPengajuan);
    GetCoaPengajuans(function (HtmlCombo) {
        $("select[name='IdCoa']", "#FormPengajuanCoa").html(HtmlCombo);
    });
    GetJenisSatuans(function (HtmlCombo) {
        $("select[name='IdJenisSatuan']", "#FormPengajuanCoa").html(HtmlCombo);
    });
    GetJenisVolumes(function (HtmlCombo) {
        $("select[name='IdJenisVolume']", "#FormPengajuanCoa").html(HtmlCombo);
    });
}

function EditPengajuanCoa(
    IdPengajuan,
    IdPengajuanCoa,
    IdCoa,
    Catatan,
    IdJenisSatuan,
    JumlahSatuan,
    IdJenisVolume,
    JumlahVolume,
    HargaSatuan,
    Rp,
    KodeCoa,
    NamaCoa) {
    $('#FormPengajuanCoa')[0].reset();
    $("#ModalFormPengajuanCoa").modal("show");
    $("#BtnSavePengajuanCoa").attr("onClick", "SavePengajuanCoa('EditData')");
    
    $("#ModalHeaderFormPengajuanCoa").text("Edit COA Pengajuan");
    $("input[name='IdPengajuanCoa']", "#FormPengajuanCoa").val(IdPengajuanCoa);
    $("input[name='JumlahSatuan']", "#FormPengajuanCoa").val(JumlahSatuan);
    $("input[name='JumlahVolume']", "#FormPengajuanCoa").val(JumlahVolume);
    $("input[name='HargaSatuan']", "#FormPengajuanCoa").val(HargaSatuan);
    $("textarea[name='Catatan']", "#FormPengajuanCoa").val(Catatan);
    GetCoaPengajuans(function (HtmlCombo) {
        $("select[name='IdCoa']", "#FormPengajuanCoa").html(HtmlCombo);
    },IdCoa);
    GetJenisSatuans(function (HtmlCombo) {
        $("select[name='IdJenisSatuan']", "#FormPengajuanCoa").html(HtmlCombo);
    },IdJenisSatuan);
    GetJenisVolumes(function (HtmlCombo) {
        $("select[name='IdJenisVolume']", "#FormPengajuanCoa").html(HtmlCombo);
    },IdJenisVolume);
}

function SavePengajuanCoa(Tipe) {
    var formData = new FormData($('#FormPengajuanCoa')[0]);
    var formObj = $('#FormPengajuanCoa').serializeObject();
    if(Tipe == "TambahData"){
        var Url = base_url + "/Keuangans/PengajuanCoaAdd";
    } else if(Tipe == "EditData"){
        var Url = base_url + "/Keuangans/PengajuanCoaEdit";
    }
    $.ajax({
        url: Url,
        type: "POST",
        data: formData,
        dataType: "json",
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (responsebefore) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                TabelPengajuanCoa(formObj.IdPengajuan);

                $('#FormPengajuanCoa')[0].reset();
                $("#ModalFormPengajuanCoa").modal("hide");
                swal({ title: 'Berhasil Simpan COA Pengajuan Biaya', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesuccess.IsSuccess == false) {
                swal({ title: 'Gagal Simpan COA Pengajuan Biaya', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
    
}

function HapusPengajuanCoa(IdPengajuan,
    IdPengajuanCoa,
    IdCoa,
    Catatan,
    IdJenisSatuan,
    JumlahSatuan,
    IdJenisVolume,
    JumlahVolume,
    HargaSatuan,
    Rp,
    KodeCoa,
    NamaCoa) {
    var formObj = $('#FormPengajuanCoa').serializeObject();
    var Url = base_url + "/Keuangans/PengajuanCoaDelete?IdPengajuanCoa=" + parseInt(IdPengajuanCoa);
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus data coa " + KodeCoa,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        TabelPengajuanCoa(IdPengajuan);
                        swal({ title: 'Berhasil Hapus Data COA', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesuccess.IsSuccess == false) {
                        swal({ title: 'Gagal Hapus Data COA', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}