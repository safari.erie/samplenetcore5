/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 14042020
================================================================================================================= */
const ParamTabel = {
    TabelUnit:null,
    TabelProgram:null,
    TabelKegiatan:null,
    TabelKegiatanCoa:null
};
ComboGetTahunAjaran(function (obj) {
    $("select[name='IdTahunAjaran']").html(obj);
});
function TabelUnit(){
    $("#MainTableEmpty").css("display","none");
    $("#MainTable").css("display","block");
    //////// START SECTION TABEL UNIT ///////////
    var Url = base_url + "/Keuangans/GetKeuProKegUnits?IdTahunAjaran="+parseInt($("select[name='IdTahunAjaran'] option:selected").val());
    $('#TabelUnit tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelUnit = $('#TabelUnit').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": Url,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Unit',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    var JmlBtnAksi = full.Aksis.length;
                    var Data = '';
                    var Param = '';
                    for (var i = 0; i < JmlBtnAksi; i++) {
                        Param = "AksiTabelUnit('" + full.Aksis[i] + "','" + full.IdUnitProKeg + "','" + full.Unit + "')";
                        Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + Param + '">' + full.Aksis[i] + '</button>';
                        Data += '&nbsp;&nbsp;';
                    }
                    
                    return Data;
                }
            },
            { "data": "Unit" },
            { "data": "TahunAjaran" },
            {
                "render": function (data, type, full, meta) {
                    var Data = '<span>' + formatRupiah(full.Rp) + '</span>';
                    return Data;
                }
            },
            { "data": "Status" },
            { "data": "StatusProses" },
            { "data": "Catatan" },
        ],
        "bDestroy": true
    });
    TabelUnit.columns().every(function () {
        var that = this;
    
        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    //////// START SECTION TABEL UNIT ///////////
}


function AksiTabelUnit(Aksi, IdUnitProKeg, Unit) {
    var Url = "";
    if (Aksi == "View") {
        TabelProgram(IdUnitProKeg, Unit);
    } else if (Aksi == "Edit") {
        EditProgram(IdUnitProKeg, Unit);
    } else if (Aksi == "Hapus") {
        HapusProgram(IdUnitProKeg, Unit);
    } else if (Aksi == "Download") {
        DownloadProKeg(IdUnitProKeg, Unit);
    } else if (Aksi == "Approve Ka Unit") {
        Setujui(IdUnitProKeg, Unit,'ProKegApproveByKaUnit');
    } else if (Aksi == "Approve Mgr Unit") {
        Setujui(IdUnitProKeg, Unit, 'ProKegApproveByManUnit');
    } else if (Aksi == "Approve Mgr Keu") {
        Setujui(IdUnitProKeg, Unit, 'ProKegApproveByManKeu');
    } else if (Aksi == "Approve Direktur") {
        Setujui(IdUnitProKeg, Unit, 'ProKegApproveByDirektur');
    } else if (Aksi == "Set Sah") {
        Url = base_url + "/Keuangans/ProKegSetSah?IdUnitProKeg=" + IdUnitProKeg;
        SaveSah(IdUnitProKeg, Unit, Url);
    } 
}

function Setujui(IdUnitProKeg,Unit,Api) {
    $("#ModalFormSetujui").modal("show");
    $("#ModalHeaderFormSetujui").text("Approve Program Kegiatan Sekolah");

    $("input[name='IdUnitProKeg']", "#FormSetujui").val(IdUnitProKeg)
    $("input[name='Unit']", "#FormSetujui").val(Unit)
    $("#BtnSetujui").attr("onClick", "SaveSetujui('" + Api +"')");
}

function DownloadProKeg(IdUnitProKeg){
    var url = base_url + "/Keuangans/DownloadProKeg?IdUnitProKeg=" + IdUnitProKeg;
    window.open(url, '_blank');
}

function TabelProgram(IdUnitProKeg,Unit) {
    ParamTabel.TabelProgram = {
        IdUnitProKeg:IdUnitProKeg,
        Unit:Unit
    };
    $("#DivSectionProgram").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#DivSectionProgram").offset().top
    }, 50);
    $("#HeadingTabelProgram").text("Data Program - "+Unit)
    //////// START SECTION TABEL PROGRAM ///////////
    var Url = base_url + "/Keuangans/GetKeuProKegUnitPrograms?IdUnitProKeg=" + parseInt(IdUnitProKeg);
    $('#TabelProgram tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelProgram = $('#TabelProgram').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": Url,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Program',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        columnDefs: [
            { targets: [0], width: "auto", visible: true },
            { targets: [1], width: "auto", visible: true },
            { targets: [2], width: "auto", visible: true },
            { targets: [3], width: "auto", visible: true },
            { targets: [4], width: "auto", visible: true },
            { targets: [5], width: "auto", visible: true },
            { targets: [6], width: "auto", visible: true },
        ],
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    var JmlBtnAksi = full.Aksis.length;
                    var Data = '';
                    var Param = '';
                    for (var i = 0; i < JmlBtnAksi; i++) {
                        Param = "AksiTabelProgram('" + full.Aksis[i] + "','" + IdUnitProKeg + "','" + full.IdProgram + "','" + full.Program + "')";
                        Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + Param + '">' + full.Aksis[i] + '</button>';
                        Data += '&nbsp;&nbsp;';
                    }
                    return Data;
                }
            },
            { "data": "Program" },
            { "data": "JumlahKegiatanWajib" },
            {
                "render": function (data, type, full, meta) {
                    return formatRupiah(full.RpKegiatanWajib);
                }
            },
            { "data": "JumlahKegiatanTambahan" },
            {
                "render": function (data, type, full, meta) {
                    return formatRupiah(full.RpKegiatanTambahan);
                }
            },
            {
                "render": function (data, type, full, meta) {
                    return formatRupiah(full.RpTotal);
                }
            },
        ],
        "bDestroy": true
    });
    TabelProgram.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    //////// START SECTION TABEL PROGRAM ///////////
}

function AksiTabelProgram(Aksi,IdUnitProKeg,IdProgram,Program) {
    var Url = "";
    if (Aksi == "View") {
        TabelKegiatan(IdUnitProKeg,IdProgram, Program);
    } else if (Aksi == "Edit") {
        EditProgram(IdUnitProKeg,IdProgram, Program);
    } else if (Aksi == "Hapus") {
        HapusProgram(IdUnitProKeg,IdProgram, Program);
    }
}

function TabelKegiatan(IdUnitProKeg,IdProgram,Program) {
    ParamTabel.TabelKegiatan = {
        IdUnitProKeg:IdUnitProKeg,
        IdProgram:IdProgram,
        Program:Program
    };
    $("#DivSectionTabelKegiatan").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#DivSectionTabelKegiatan").offset().top
    }, 50);
    $("#HeadingTabelKegiatan").text("Data Kegiatan Unit - "+Program)
    //////// START SECTION TABEL KEGIATAN ///////////
    var Url = base_url + "/Keuangans/GetKeuProKegUnitKegiatans?IdUnitProKeg=" + parseInt(IdUnitProKeg)+"&IdProgram=" + parseInt(IdProgram);
    $('#TabelKegiatan tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelKegiatan = $('#TabelKegiatan').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
         "dom": "Bfrtip",
         buttons: [
             {
                 text: '<i class="fa fa-plus"></i> Tambah',
                 className: 'btn btn-success btn-flat mr-2 mb-2 text-right float-right',
                 action: function (e, dt, node, config) {
                     TambahUnitKegiatan(IdUnitProKeg,IdProgram);
                 },
                 init: function (api, node, config) {
                     $(node).removeClass('dt-button');
                 }
             }
         ],
        "ajax": {
            "url": Url,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Kegiatan Unit',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        columnDefs: [
            { targets: [0], width: "auto", visible: true },
            { targets: [1], width: "auto", visible: true },
            { targets: [2], width: "auto", visible: true },
            { targets: [3], width: "auto", visible: true },
        ],
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    var JmlBtnAksi = full.Aksis.length;
                    var Data = '';
                    for (var i = 0; i < JmlBtnAksi; i++) {
                        Param = "AksiTabelKegiatan('" + full.Aksis[i] + "','" + IdUnitProKeg + "','" + IdProgram + "','" + Program + "','" + full.IdUnitProkegWajib + "','" + full.IdUnitProkegTambahan + "','" + full.Jenis + "','" + full.Kegiatan + "','" + full.Tanggal + "')";
                        Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + Param + '">' + full.Aksis[i] + '</button>';
                        Data += '&nbsp;&nbsp;';
                    }
                
                    return Data;
                }
            },
            { "data": "Jenis" },
            { "data": "Kegiatan" },
            {
                "render": function (data, type, full, meta) {
                    return formatRupiah(full.Rp);
                }
            },
        ],
        "bDestroy": true
    });
    TabelKegiatan.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    //////// START SECTION TABEL KEGIATAN ///////////
}

function AksiTabelKegiatan(Aksi,IdUnitProKeg,IdProgram,Program,IdUnitProkegWajib,IdUnitProkegTambahan,Jenis,Kegiatan,Tanggal) {
    var Url = "";
    if (Aksi == "View") {
        TabelKegiatanCoa(IdUnitProkegWajib,IdUnitProkegTambahan,Jenis,Kegiatan);
    } else if (Aksi == "Edit") {
        EditUnitKegiatan(IdUnitProKeg,IdProgram,Program,IdUnitProkegTambahan,Kegiatan,Tanggal);
    } else if (Aksi == "Hapus") {
        HapusUnitKegiatan(IdUnitProKeg,IdProgram,Program,IdUnitProkegTambahan,Kegiatan);
    }
}

function TabelKegiatanCoa(IdUnitProkegWajib,IdUnitProkegTambahan,Jenis,Kegiatan) {
    ParamTabel.TabelKegiatanCoa = {
        IdUnitProkegWajib:IdUnitProkegWajib,
        IdUnitProkegTambahan:IdUnitProkegTambahan,
        Jenis:Jenis,
        Kegiatan:Kegiatan
    };
    $("#DivSectionKegiatanCoa").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#DivSectionKegiatanCoa").offset().top
    }, 50);
    $("#HeadingTabelKegiatanCoa").text("Data COA Kegiatan Unit - "+Kegiatan )
    //////// START SECTION TABEL KEGIATAN COA ///////////
    var Url = "";
    if(Jenis == "WAJIB"){
        Url = base_url + "/Keuangans/GetKeuKegiatanWajibCoas?IdUnitProkegWajib=" + parseInt(IdUnitProkegWajib);
    } else if(Jenis == "TAMBAHAN"){
        Url = base_url + "/Keuangans/GetKeuKegiatanTambahanCoas?IdUnitProkegTambahan=" + parseInt(IdUnitProkegTambahan);
    }
    
    $('#TabelKegiatanCoa tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelKegiatanCoa = $('#TabelKegiatanCoa').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
         "dom": "Bfrtip",
         buttons: [
             {
                 text: '<i class="fa fa-plus"></i> Tambah',
                 className: 'btn btn-success btn-flat mr-2 mb-2 text-right float-right',
                 action: function (e, dt, node, config) {
                     TambahUnitKegiatanCoa(IdUnitProkegWajib,IdUnitProkegTambahan,Jenis,Kegiatan);
                 },
                 init: function (api, node, config) {
                     $(node).removeClass('dt-button');
                 }
             }
         ],
        "ajax": {
            "url": Url,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Kegiatan Coa Unit',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        columnDefs: [
            { targets: [0], width: "auto", visible: true },
            { targets: [1], width: "auto", visible: true },
            { targets: [2], width: "auto", visible: true },
            { targets: [3], width: "auto", visible: true },
            { targets: [4], width: "auto", visible: true },
            { targets: [5], width: "auto", visible: true },
            { targets: [6], width: "auto", visible: true },
            { targets: [7], width: "auto", visible: true },
            { targets: [8], width: "auto", visible: true },
        ],
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    var JmlBtnAksi = full.Aksis.length;
                    var Data = '';
                    Data += '&nbsp;&nbsp;';
                    for (var i = 0; i < JmlBtnAksi; i++) {
                        if(Jenis == "WAJIB"){
                            Param = "AksiTabelKegiatanWajibCoa('" + full.Aksis[i] + "','" + full.IdUnitProkegWajibCoa + "','" + IdUnitProkegWajib + "','" + IdUnitProkegTambahan + "','" + Jenis + "','" + Kegiatan + "','" + full.KodeCoa + "')";
                        } else if(Jenis == "TAMBAHAN"){
                            Param = "AksiTabelKegiatanTambahanCoa('" + full.Aksis[i] + "','" + full.IdUnitProkegTambahanCoa + "','" + IdUnitProkegWajib + "','" + IdUnitProkegTambahan + "','" + Jenis + "','" + Kegiatan + "','" + full.KodeCoa + "')";
                        } 
                        Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + Param + '">' + full.Aksis[i] + '</button>';
                        Data += '&nbsp;&nbsp;';
                    }
                
                    return Data;
                }
            },
            { "data": "KodeCoa" },
            { "data": "NamaCoa" },
            { "data": "JumlahSatuan" },
            { "data": "Satuan" },
            { "data": "JumlahVolume" },
            { "data": "Volume" },
            {
                "render": function (data, type, full, meta) {
                    return formatRupiah(full.HargaSatuan);
                }
            },
            {
                "render": function (data, type, full, meta) {
                    return formatRupiah(full.Rp);
                }
            },
        ],
        "bDestroy": true
    });
    TabelKegiatanCoa.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    //////// START SECTION TABEL KEGIATAN COA ///////////
}

function AksiTabelKegiatanWajibCoa(Aksi,IdUnitProkegWajibCoa,IdUnitProkegWajib,IdUnitProkegTambahan,Jenis,Kegiatan,KodeCoa) {
    var Url = "";
    if (Aksi == "View") {
        EditUnitKegiatanWajibCoa(IdUnitProkegWajibCoa,IdUnitProkegWajib,Jenis,Kegiatan);
    } else if (Aksi == "Edit") {
        EditUnitKegiatanWajibCoa(IdUnitProkegWajibCoa,IdUnitProkegWajib,Jenis,Kegiatan);
    } else if (Aksi == "Hapus") {
        HapusUnitKegiatanWajibCoa(IdUnitProkegWajibCoa,IdUnitProkegWajib,Jenis,Kegiatan,KodeCoa);
    }
}

function AksiTabelKegiatanTambahanCoa(Aksi,IdUnitProkegTambahanCoa,IdUnitProkegWajib,IdUnitProkegTambahan,Jenis,Kegiatan,KodeCoa) {
    var Url = "";
    if (Aksi == "View") {
        EditUnitKegiatanTambahanCoa(IdUnitProkegTambahanCoa,IdUnitProkegTambahan,Jenis,Kegiatan);
    } else if (Aksi == "Edit") {
        EditUnitKegiatanTambahanCoa(IdUnitProkegTambahanCoa,IdUnitProkegTambahan,Jenis,Kegiatan);
    } else if (Aksi == "Hapus") {
        HapusUnitKegiatanTambahanCoa(IdUnitProkegTambahanCoa,IdUnitProkegTambahan,Jenis,Kegiatan,KodeCoa);
    }
}

function TambahUnitKegiatan(IdUnitProKeg,IdProgram) {
    $("#ModalFormUnitKegiatan").modal("show");
    $("#BtnSaveUnitKegiatan").attr("onClick", "SaveUnitKegiatan('TambahData')");
    $("#ModalHeaderFormUnitKegiatan").text("Tambah Data Unit Kegiatan Tambahan");

    $("input[name='IdUnitProKeg']", "#FormUnitKegiatan").val(IdUnitProKeg);
    $("input[name='IdProgram']", "#FormUnitKegiatan").val(IdProgram);
}

function TambahUnitKegiatanCoa(IdUnitProkegWajib,IdUnitProkegTambahan,Jenis,Kegiatan) {
    $('#FormUnitKegiatanCoa')[0].reset();
    $("#ModalFormUnitKegiatanCoa").modal("show");
    $("#BtnSaveUnitKegiatanCoa").attr("onClick", "SaveUnitKegiatanCoa('TambahData','"+IdUnitProkegWajib+"','"+IdUnitProkegTambahan+"','"+Jenis+"','"+Kegiatan+"')");
    $("#ModalHeaderFormUnitKegiatanCoa").text("Tambah Data Kegiatan Akun "+Jenis+" "+Kegiatan);
    
    $("input[name='IdUnitProkegWajib']", "#FormUnitKegiatanCoa").val(IdUnitProkegWajib);
    $("input[name='IdUnitProkegTambahan']", "#FormUnitKegiatanCoa").val(IdUnitProkegTambahan);
    GetCoaProgKegs(function (HtmlCombo) {
        $("select[name='IdCoa']", "#FormUnitKegiatanCoa").html(HtmlCombo);
    });
    GetJenisSatuans(function (HtmlCombo) {
        $("select[name='IdJenisSatuan']", "#FormUnitKegiatanCoa").html(HtmlCombo);
    });
    GetJenisVolumes(function (HtmlCombo) {
        $("select[name='IdJenisVolume']", "#FormUnitKegiatanCoa").html(HtmlCombo);
    });
}

function EditUnitKegiatan(IdUnitProKeg,IdProgram,Program,IdUnitProkegTambahan,Kegiatan,Tanggal) {
    $("#ModalFormUnitKegiatan").modal("show");
    $("#BtnSaveUnitKegiatan").attr("onClick", "SaveUnitKegiatan('EditData')");
    $("#ModalHeaderFormUnitKegiatan").text("Edit Data Unit Kegiatan Tambahan");
    $("input[name='IdUnitProKeg']", "#FormUnitKegiatan").val(IdUnitProKeg);
    $("input[name='IdProgram']", "#FormUnitKegiatan").val(IdProgram);
    $("input[name='IdUnitProkegTambahan']", "#FormUnitKegiatan").val(IdUnitProkegTambahan);
    $("input[name='NamaKegiatan']", "#FormUnitKegiatan").val(Kegiatan);
    $("input[name='Tanggal']", "#FormUnitKegiatan").val(Tanggal);
}

function EditUnitKegiatanWajibCoa(IdUnitProkegWajibCoa,IdUnitProkegWajib,Jenis,Kegiatan) {
    $('#FormUnitKegiatanCoa')[0].reset();
    $("#ModalFormUnitKegiatanCoa").modal("show");
    $("#BtnSaveUnitKegiatanCoa").attr("onClick","SaveUnitKegiatanCoa('EditData','"+IdUnitProkegWajib+"','"+null+"','"+Jenis+"','"+Kegiatan+"')");
    var Url = base_url + "/Keuangans/GetKeuKegiatanWajibCoa?IdUnitProkegWajibCoa=" + parseInt(IdUnitProkegWajibCoa);
    console.log("EditUnitKegiatanWajibCoa",Url)
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                $("#ModalHeaderFormUnitKegiatanCoa").text("Edit Data Kegiatan Akun Wajib");
                var ResponseData = responsesuccess.Data;
                $("input[name='IdUnitProkegWajibCoa']", "#FormUnitKegiatanCoa").val(ResponseData.IdUnitProkegWajibCoa);
                $("input[name='IdUnitProkegWajib']", "#FormUnitKegiatanCoa").val(IdUnitProkegWajib);
                $("input[name='JumlahSatuan']", "#FormUnitKegiatanCoa").val(ResponseData.JumlahSatuan);
                $("input[name='JumlahVolume']", "#FormUnitKegiatanCoa").val(ResponseData.JumlahVolume);
                $("input[name='Tanggal']", "#FormUnitKegiatanCoa").val(ResponseData.Tanggal);
                $("input[name='HargaSatuan']", "#FormUnitKegiatanCoa").val(ResponseData.HargaSatuan);
                $("textarea[name='Catatan']", "#FormUnitKegiatanCoa").val(ResponseData.Catatan);
                GetCoaProgKegs(function (HtmlCombo) {
                    $("select[name='IdCoa']", "#FormUnitKegiatanCoa").html(HtmlCombo);
                },ResponseData.IdCoa);
                GetJenisSatuans(function (HtmlCombo) {
                    $("select[name='IdJenisSatuan']", "#FormUnitKegiatanCoa").html(HtmlCombo);
                },ResponseData.IdJenisSatuan);
                GetJenisVolumes(function (HtmlCombo) {
                    $("select[name='IdJenisVolume']", "#FormUnitKegiatanCoa").html(HtmlCombo);
                },ResponseData.IdJenisVolume);
                //
            } else if (responsesuccess.IsSuccess == false) {
                swal({ title: 'Gagal Menampilkan Unit Kegiatan COA', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function EditUnitKegiatanTambahanCoa(IdUnitProkegTambahanCoa,IdUnitProkegTambahan,Jenis,Kegiatan) {
    $('#FormUnitKegiatanCoa')[0].reset();
    $("#ModalFormUnitKegiatanCoa").modal("show");
    $("#BtnSaveUnitKegiatanCoa").attr("onClick","SaveUnitKegiatanCoa('EditData','"+null+"','"+IdUnitProkegTambahan+"','"+Jenis+"','"+Kegiatan+"')");
    var Url = base_url + "/Keuangans/GetKeuKegiatanTambahanCoa?IdUnitProkegTambahanCoa=" + parseInt(IdUnitProkegTambahanCoa);
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                $("#ModalHeaderFormUnitKegiatanCoa").text("Edit Data Kegiatan Akun Tambahan");
                var ResponseData = responsesuccess.Data;
                $("input[name='IdUnitProkegTambahanCoa']", "#FormUnitKegiatanCoa").val(ResponseData.IdUnitProkegTambahanCoa);
                $("input[name='IdUnitProkegTambahan']", "#FormUnitKegiatanCoa").val(IdUnitProkegTambahan);
                $("input[name='JumlahSatuan']", "#FormUnitKegiatanCoa").val(ResponseData.JumlahSatuan);
                $("input[name='JumlahVolume']", "#FormUnitKegiatanCoa").val(ResponseData.JumlahVolume);
                $("input[name='Tanggal']", "#FormUnitKegiatanCoa").val(ResponseData.Tanggal);
                $("input[name='HargaSatuan']", "#FormUnitKegiatanCoa").val(ResponseData.HargaSatuan);
                $("textarea[name='Catatan']", "#FormUnitKegiatanCoa").val(ResponseData.Catatan);
                GetCoaProgKegs(function (HtmlCombo) {
                    $("select[name='IdCoa']", "#FormUnitKegiatanCoa").html(HtmlCombo);
                },ResponseData.IdCoa);
                GetJenisSatuans(function (HtmlCombo) {
                    $("select[name='IdJenisSatuan']", "#FormUnitKegiatanCoa").html(HtmlCombo);
                },ResponseData.IdJenisSatuan);
                GetJenisVolumes(function (HtmlCombo) {
                    $("select[name='IdJenisVolume']", "#FormUnitKegiatanCoa").html(HtmlCombo);
                },ResponseData.IdJenisVolume);
                //
            } else if (responsesuccess.IsSuccess == false) {
                swal({ title: 'Gagal Menampilkan Unit Kegiatan COA', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function SaveUnitKegiatanCoa(Aksi,IdUnitProkegWajib,IdUnitProkegTambahan,Jenis,Kegiatan) {
    var formData = new FormData($('#FormUnitKegiatanCoa')[0]);
    var Url = "";
    if (Aksi == "TambahData") {
        var Proses = "Tambah Data";
        if(Jenis == "WAJIB"){
            Url = base_url + "/Keuangans/KegiatanWajibCoaAdd";
        } else if(Jenis == "TAMBAHAN"){
            Url = base_url + "/Keuangans/KegiatanTambahanCoaAdd";
        }
    }  else if (Aksi == "EditData") {
        var Proses = "Edit Data"
        if(Jenis == "WAJIB"){
            Url = base_url + "/Keuangans/KegiatanWajibCoaEdit";
        } else if(Jenis == "TAMBAHAN"){
            Url = base_url + "/Keuangans/KegiatanTambahanCoaEdit";
        }
    }
    $.ajax({
        url: Url,
        type: "POST",
        data: formData,
        dataType: "json",
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (responsebefore) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var PTP = ParamTabel.TabelProgram;
                var PTK = ParamTabel.TabelKegiatan;
                TabelUnit();
                TabelProgram(PTP.IdUnitProKeg,PTP.Unit);
                TabelKegiatan(PTK.IdUnitProKeg,PTK.IdProgram,PTK.Program);
                TabelKegiatanCoa(IdUnitProkegWajib,IdUnitProkegTambahan,Jenis,Kegiatan);
                
                $('#FormUnitKegiatanCoa')[0].reset();
                $("#ModalFormUnitKegiatanCoa").modal("hide");
                swal({ title: 'Berhasil ' + Proses+' Unit Kegiatan COA', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesuccess.IsSuccess == false) {
                swal({ title: 'Gagal ' + Proses +' Unit Kegiatan COA', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            ProgressBar("success");
            swal({ title: 'Gagal ' + Proses +' Unit Kegiatan COA', text: jqXHR + " " + textStatus + " " + errorThrown, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function HapusUnitKegiatanWajibCoa(IdUnitProkegWajibCoa,IdUnitProkegWajib,Jenis,Kegiatan,KodeCoa) {
    var formObj = $('#FormUnitKegiatanCoa').serializeObject();
    var Url = base_url + "/Keuangans/KegiatanWajibCoaDelete?IdUnitProkegWajibCoa=" + parseInt(IdUnitProkegWajibCoa);
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus data coa " + KodeCoa,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        TabelKegiatanCoa(IdUnitProkegWajib,null,Jenis,Kegiatan);
                        $('#FormUnitKegiatanCoa')[0].reset();
                        swal({ title: 'Berhasil Hapus Unit Kegiatan Wajib COA', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesuccess.IsSuccess == false) {
                        swal({ title: 'Gagal Hapus Unit Kegiatan Wajib COA', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}

function HapusUnitKegiatanTambahanCoa(IdUnitProkegTambahanCoa,IdUnitProkegTambahan,Jenis,Kegiatan,KodeCoa) {
    var formObj = $('#FormUnitKegiatanCoa').serializeObject();
    var Url = base_url + "/Keuangans/KegiatanTambahanCoaDelete?IdUnitProkegTambahanCoa=" + parseInt(IdUnitProkegTambahanCoa);
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus data coa " + KodeCoa,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        TabelKegiatanCoa(null,IdUnitProkegTambahan,Jenis,Kegiatan);
                        $('#FormUnitKegiatanCoa')[0].reset();
                        swal({ title: 'Berhasil Hapus Unit Kegiatan Wajib COA', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesuccess.IsSuccess == false) {
                        swal({ title: 'Gagal Hapus Unit Kegiatan Wajib COA', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}

function SaveSetujui(Api) {
    var formObj = $('#FormSetujui').serializeObject();
    var Url = base_url + "/Keuangans/" + Api + "?IdUnitProKeg=" + formObj.IdUnitProKeg + "&StatusProses=" + formObj.StatusProses + "&Catatan=" + formObj.Catatan;
    console.log("NICH ",Url);
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menyetujui program kegiatan sekolah " + formObj.Unit,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        TabelUnit();
                        $("#ModalFormSetujui").modal("hide");
                        swal({ title: 'Berhasil Setujui Program Kegiatan Sekolah', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesuccess.IsSuccess == false) {
                        swal({ title: 'Gagal Setujui Program Kegiatan Sekolah', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}

function SaveSah(IdUnitProKeg, Unit, Url) {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan men-sah-kan program kegiatan sekolah unit " + Unit,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        TabelUnit();
                        swal({ title: 'Berhasil Sah-kan Program Kegiatan Sekolah', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesuccess.IsSuccess == false) {
                        swal({ title: 'Gagal Sah-kan Program Kegiatan Sekolah', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}

function SaveUnitKegiatan(Tipe) {
    var formData = new FormData($('#FormUnitKegiatan')[0]);
    var formObj = $('#FormUnitKegiatan').serializeObject();
    if(Tipe == "TambahData"){
        var Url = base_url + "/Keuangans/KegiatanTambahanAdd";
    } else if(Tipe == "EditData"){
        var Url = base_url + "/Keuangans/KegiatanTambahanEdit";
    }
    $.ajax({
        url: Url,
        type: "POST",
        data: formData,
        dataType: "json",
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (responsebefore) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                TabelKegiatan(formObj.IdUnitProKeg,formObj.IdProgram,formObj.Program);

                $('#FormUnitKegiatan')[0].reset();
                $("#ModalFormUnitKegiatan").modal("hide");
                swal({ title: 'Berhasil Simpan Data Unit Kegiatan', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesuccess.IsSuccess == false) {
                swal({ title: 'Gagal Simpan Data Unit Kegiatan', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function HapusUnitKegiatan(IdUnitProKeg,IdProgram,Program,IdUnitProkegTambahan,Kegiatan) {
    var Url = base_url + "/Keuangans/KegiatanTambahanDelete?IdUnitProkegTambahan=" + parseInt(IdUnitProkegTambahan);
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus data kegiatan " + Kegiatan,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesuccess) {
                    ProgressBar("success");
                    if (responsesuccess.IsSuccess == true) {
                        TabelKegiatan(IdUnitProKeg,IdProgram,Program);
                        swal({ title: 'Berhasil Hapus Data Unit Kegiatan Tambahan', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesuccess.IsSuccess == false) {
                        swal({ title: 'Gagal Hapus Data Unit Kegiatan Tambahan', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}