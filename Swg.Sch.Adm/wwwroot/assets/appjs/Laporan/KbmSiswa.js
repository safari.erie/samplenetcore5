$(document).ready(function () {
    TabelUnit(GetCurrentDate("TanggalOnly"));
})

const Laporan = '/api/laporans/'
const UrlService = {
    GetElKbmSiswa: base_url + Laporan + 'GetElKbmSiswa',
    GetElKbmSiswaKelas: base_url + Laporan + 'GetElKbmSiswaKelas',
    GetElKbmSiswaKelasParalel: base_url + Laporan + 'GetElKbmSiswaKelasParalel',
    GetElKbmSiswaList: base_url + Laporan + 'GetElKbmSiswaList',
};

function Pencarian() {
    var Tanggal = $('#TanggalPencarian').val();
    TabelUnit(Tanggal);
}

function TabelUnit(Tanggal = "") {
    $('#ModalPencarian').modal('hide');
    console.log(UrlService.GetElKbmSiswa + '?Tanggal=' + Tanggal)
    $('#TabelUnit tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });

    var TUnit = $('#TabelUnit').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": UrlService.GetElKbmSiswa + '?Tanggal=' + Tanggal,
            "method": 'GET',
            "async": 'false',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                console.log(json.Data);
                if (json.Data.Units == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Unit',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    $('#TanggalAwal').html(indoDate(json.Data.Periode.TanggalAwal));
                    $('#TanggalAkhir').html(indoDate(json.Data.Periode.TanggalAkhir));
                    return json.Data.Units;
                }
            }
        },
        "columns": [{
                "data": "Unit"
            },
            {
                "data": "JmlTotal"
            },
            {
                "data": "JmlJawab"
            },
            {
                "data": "JmlNoJawab"
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var getViewData = "TabelKelas('" + full.IdUnit + "','" + Tanggal + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + getViewData + '"><i class="fa fa-eye"></i> Lihat</button>';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });

    TUnit.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function TabelKelas(IdUnit, Tanggal = "") {
    $('#SecKelas').fadeIn();
    $('html, body').animate({
        scrollTop: $("#SecKelas").offset().top
    }, 50);
    $('#TabelKelas tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });

    var TKelas = $('#TabelKelas').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": UrlService.GetElKbmSiswaKelas + '?IdUnit=' + IdUnit + '&Tanggal=' + Tanggal,
            "method": 'GET',
            "async": 'false',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                console.log(json.Data);
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Kelas',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [{
                "data": "Unit"
            },
            {
                "data": "Kelas"
            },
            {
                "data": "JmlTotal"
            },
            {
                "data": "JmlJawab"
            },
            {
                "data": "JmlNoJawab"
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var getViewData = "TabelKelasParalel('" + full.IdKelas + "','" + Tanggal + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + getViewData + '"><i class="fa fa-eye"></i> Lihat</button>';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });

    TKelas.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function TabelKelasParalel(IdKelas, Tanggal = "") {
    $('#SecKelasParalel').fadeIn();
    $('html, body').animate({
        scrollTop: $("#SecKelasParalel").offset().top
    }, 50);
    $('#TabelKelasParalel tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });

    var TKelasParalel = $('#TabelKelasParalel').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": UrlService.GetElKbmSiswaKelasParalel + '?IdKelas=' + IdKelas + '&Tanggal=' + Tanggal,
            "method": 'GET',
            "async": 'false',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                console.log(json.Data);
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Kelas Paralel',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [{
                "data": "Unit"
            },
            {
                "data": "Kelas"
            },
            {
                "data": "KelasParalel"
            },
            {
                "data": "JmlTotal"
            },
            {
                "data": "JmlJawab"
            },
            {
                "data": "JmlNoJawab"
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var getViewData = "TabelTugasSudah('" + full.IdKelasParalel + "','" + Tanggal + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + getViewData + '"><i class="fa fa-eye"></i> Lihat</button>';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });

    TKelasParalel.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function TabelTugasSudah(IdKelasParalel, Tanggal) {
    $('#SecTugasSudah').fadeIn();
    $('html, body').animate({
        scrollTop: $("#SecTugasSudah").offset().top
    }, 50);
    TabelTugasBelum(IdKelasParalel, Tanggal);

    console.log(UrlService.GetElKbmSiswaList + '?IdKelasParalel=' + IdKelasParalel + '&Tanggal=' + Tanggal)
    $('#TabelTugasSudah tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });

    var TSudahJawab = $('#TabelTugasSudah').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": UrlService.GetElKbmSiswaList + '?IdKelasParalel=' + IdKelasParalel + '&Tanggal=' + Tanggal,
            "method": 'GET',
            "async": 'false',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                console.log(json.Data);
                if (json.Data.Jawab == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Siswa Sudah Jawab',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    $('#InfoJudul').html(json.Data.InfoJudul);
                    return json.Data.Jawab;
                }
            }
        },
        "columns": [{
                "data": "Nis"
            },
            {
                "data": "NamaSiswa"
            },
            {
                "data": "MataPelajaran"
            },
            {
                "data": "Hari"
            },
            {
                "data": "JamMulai"
            },
            {
                "data": "JamSelesai"
            },
            {
                "data": "FileJawaban"
            },
            {
                "data": "Nilai"
            },
        ],
        "bDestroy": true
    });

    TSudahJawab.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function TabelTugasBelum(IdKelasParalel, Tanggal) {
    $('#SecTugasBelum').fadeIn();
    $('html, body').animate({
        scrollTop: $("#SecTugasBelum").offset().top
    }, 50);
    console.log(UrlService.GetElKbmSiswaList + '?IdKelasParalel=' + IdKelasParalel + '&Tanggal=' + Tanggal)
    $('#TabelTugasBelum tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });

    var TBelumJawab = $('#TabelTugasBelum').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": UrlService.GetElKbmSiswaList + '?IdKelasParalel=' + IdKelasParalel + '&Tanggal=' + Tanggal,
            "method": 'GET',
            "async": 'false',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                console.log(json.Data);
                if (json.Data.NoJawab == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Siswa Tidak jawab',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data.NoJawab;
                }
            }
        },
        "columns": [{
                "data": "Nis"
            },
            {
                "data": "NamaSiswa"
            },
            {
                "data": "MataPelajaran"
            },
            {
                "data": "Hari"
            },
            {
                "data": "JamMulai"
            },
            {
                "data": "JamSelesai"
            },
            {
                "data": "FileJawaban"
            },
            {
                "data": "Nilai"
            },
        ],
        "bDestroy": true
    });

    TBelumJawab.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}