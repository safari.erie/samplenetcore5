$(document).ready(function () {
   
    ComboGetUnitSklh(function (obj) {
        $("select[name='IdUnit']").html(obj);
    });

})
const GenerateLaporan = () => {
    var formObj = $('#FormLaporanElearningProgressPegawai').serializeObject();

    var fd = new FormData();
    fd.append('IdUnit', formObj.IdUnit);
    fd.append('TanggalAwal', formObj.TanggalAwal);
    fd.append('TanggalAkhir', formObj.TanggalAkhir);

    var url = base_url + '/api/download/DownloadLaporanElearningProgressPegawai';

    $.ajax({
        url: url,
        method: 'POST',
        processData: false,
        contentType: false,
        data: fd,
        xhrFields: {
            responseType: 'blob'
        },
        beforeSend: function(xhr) {
            ProgressBar('wait');
        },
        success: function(message, textStatus, response) {
            ProgressBar('success');
            var header = response.getResponseHeader('Content-Disposition');
            var fileName = header.split("=")[1];
            var blob = new Blob([message]);
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = fileName;
            link.download = link.download.split(';')[0];
            console.log('link -> ', link.download)
            link.click();
        },
        error: function (err, a, e) {
            ProgressBar('success');
            iziToast.error({
                title: 'Terjadi Kesalahan',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    })

}

