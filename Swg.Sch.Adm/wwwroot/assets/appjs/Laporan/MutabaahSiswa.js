﻿$(document).ready(function () {
    Unit(GetCurrentDate("TanggalOnly"));
})
const laporan = '/api/Laporans/';
var TUnitHeader;
var TUnitKelasParalel;
var TMateriTugasInput;
var TMateriTugasNonInput;

var ajaxGlobRetrieve = function (url, params) {
    var dataArray = $.ajax({
        type: "GET",
        dataType: 'json',
        async: false,
        url: url + params,
    });

    var result = dataArray.responseJSON; //JSON.stringify
    return result;
}

const UrlService = {
    GetElEvalHarian: base_url + laporan + 'GetElEvalHarian',
    GetElEvalHarianKelas: base_url + laporan + 'GetElEvalHarianKelas',
    GetElEvalHarianKelasParalel: base_url + laporan + 'GetElEvalHarianKelasParalel',
    GetElEvalHarianList: base_url + laporan + 'GetElEvalHarianList',
    GetElEvalHarianSiswa: base_url + laporan + 'GetElEvalHarianSiswa',

}

function Pencarian() {
    var Tanggal = $('#TanggalPencarian').val();
    Unit(Tanggal);
}

/////////////////////////// TABEL 1 ////////////////////////////////
function Unit(tanggal = "") {
    $('#ModalPencarian').modal('hide');

    $('#TabelUnit tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });

    var TUnit = $('#TabelUnit').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": UrlService.GetElEvalHarian + "?Tanggal=" + tanggal,
            "method": 'GET',
            "async": 'false',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Evaluasi Harian',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    $('#TanggalAwal').html(indoDate(json.Data.Periode.TanggalAwal));
                    $('#TanggalAkhir').html(indoDate(json.Data.Periode.TanggalAkhir));
                    return json.Data.Units;
                }
            }
        },
        "columns": [

            {
                "data": "Unit"
            },
            {
                "data": "JmlTotal"
            },
            {
                "data": "JmlIsi"
            },
            {
                "data": "JmlNoIsi"
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var getViewData = "ShowEvaluasiHarianKelas('" + full.IdUnit + "','" + tanggal + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + getViewData + '"><i class="fa fa-eye"></i> Lihat</button>';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });

    TUnit.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");

}
/////////////////////////// END TABEL 1 ////////////////////////////////

/////////////////////////// TABEL 2 ////////////////////////////////
function ShowEvaluasiHarianKelas(IdUnit, Tanggal) {
    $("#EvalHarianKelas").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#EvalHarianKelas").offset().top
    }, 50);

    $('#TabelEvalHarianKelas tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });

    $('#EvalHarianKelas').show();
    param = '?IdUnit=' + IdUnit + "&Tanggal=" + Tanggal;
    var TabelEvalHarianKelas = $('#TabelEvalHarianKelas').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": UrlService.GetElEvalHarianKelas + param,
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Evaluasi Harian Kelas',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [

            {
                "data": "Unit"
            },
            {
                "data": "Kelas"
            },
            {
                "data": "JmlTotal"
            },
            {
                "data": "JmlIsi"
            },
            {
                "data": "JmlNoIsi"
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var getViewData = "ShowUnitKelasParalel('" + full.IdKelas + "','" + Tanggal + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + getViewData + '"><i class="fa fa-eye"></i> Lihat</button>';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });
    TabelEvalHarianKelas.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");

}
/////////////////////////// END TABEL 2 ////////////////////////////////

/////////////////////////// TABEL 3 ////////////////////////////////
function ShowUnitKelasParalel(IdKelas, Tanggal) {
    $("#EvalHarianKelasParalel").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#EvalHarianKelasParalel").offset().top
    }, 50);

    $('#TabelEvalHarianKelasParalel tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    param = '?IdKelas=' + IdKelas + "&Tanggal=" + Tanggal;
    var TabelEvalHarianKelasParalel = $('#TabelEvalHarianKelasParalel').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": UrlService.GetElEvalHarianKelasParalel + param,
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Evaluasi Harian Kelas Paralel',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [

            {
                "data": "Unit"
            },
            {
                "data": "Kelas"
            },
            {
                "data": "KelasParalel"
            },
            {
                "data": "JmlTotal"
            },
            {
                "data": "JmlIsi"
            },
            {
                "data": "JmlNoIsi"
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var getViewData = "ShowEvalHarianList('" + full.IdKelasParalel + "','" + Tanggal + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + getViewData + '"><i class="fa fa-eye"></i> Lihat</button>';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });

    TabelEvalHarianKelasParalel.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}
/////////////////////////// END TABEL 3 ////////////////////////////////

/////////////////////////// TABEL 4 ////////////////////////////////
function ShowEvalHarianList(IdKelasParalel, Tanggal) {
    $('#IdKelasParalelDownload').val(IdKelasParalel);
    $('#TanggalDownload').val(Tanggal);
    $("#EvalHarianList").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#EvalHarianList").offset().top
    }, 50);
    param = '?IdKelasParalel=' + IdKelasParalel + "&Tanggal=" + Tanggal;

    $('#TabelEvalHarianList tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });

    TabelEvalHarianList = $('#TabelEvalHarianList').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": false,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": UrlService.GetElEvalHarianList + param,
            "method": 'GET',
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Materi Evaluasi Harian',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [{
                "data": "Unit"
            },
            {
                "data": "Kelas"
            },
            {
                "data": "KelasParalel"
            },
            {
                "data": "NamaSiswa"
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var param = "ShowEvalHarianSiswa('" + full.IdSiswa + "','" + Tanggal + "','0','" + cekQuotes(full.NamaSiswa) + "')";
                    var ClassSpan = "";
                    if (full.Minggu == 0) {
                        ClassSpan = "badge-danger";
                    } else {
                        ClassSpan = "badge-success";
                    }
                    Data += '<span class="badge ' + ClassSpan + '" onClick="' + param + '"><i class="fa fa-eye-o"></i> ' + full.Minggu + '</span>';
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var param = "ShowEvalHarianSiswa('" + full.IdSiswa + "','" + Tanggal + "','1','" + cekQuotes(full.NamaSiswa) + "')";
                    var ClassSpan = "";
                    if (full.Senin == 0) {
                        ClassSpan = "badge-danger";
                    } else {
                        ClassSpan = "badge-success";
                    }
                    Data += '<span class="badge ' + ClassSpan + '" onClick="' + param + '"><i class="fa fa-eye-o"></i> ' + full.Senin + '</span>';
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var param = "ShowEvalHarianSiswa('" + full.IdSiswa + "','" + Tanggal + "','2','" + cekQuotes(full.NamaSiswa) + "')";
                    var ClassSpan = "";
                    if (full.Selasa == 0) {
                        ClassSpan = "badge-danger";
                    } else {
                        ClassSpan = "badge-success";
                    }
                    Data += '<span class="badge ' + ClassSpan + '" onClick="' + param + '"><i class="fa fa-eye-o"></i> ' + full.Selasa + '</span>';
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var param = "ShowEvalHarianSiswa('" + full.IdSiswa + "','" + Tanggal + "','3','" + cekQuotes(full.NamaSiswa) + "')";
                    var ClassSpan = "";
                    if (full.Rabu == 0) {
                        ClassSpan = "badge-danger";
                    } else {
                        ClassSpan = "badge-success";
                    }
                    Data += '<span class="badge ' + ClassSpan + '" onClick="' + param + '"><i class="fa fa-eye-o"></i> ' + full.Rabu + '</span>';
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var param = "ShowEvalHarianSiswa('" + full.IdSiswa + "','" + Tanggal + "','4','" + cekQuotes(full.NamaSiswa) + "')";
                    var ClassSpan = "";
                    if (full.Kamis == 0) {
                        ClassSpan = "badge-danger";
                    } else {
                        ClassSpan = "badge-success";
                    }
                    Data += '<span class="badge ' + ClassSpan + '" onClick="' + param + '"><i class="fa fa-eye-o"></i> ' + full.Kamis + '</span>';
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var param = "ShowEvalHarianSiswa('" + full.IdSiswa + "','" + Tanggal + "','5','" + cekQuotes(full.NamaSiswa) + "')";
                    var ClassSpan = "";
                    if (full.Jumat == 0) {
                        ClassSpan = "badge-danger";
                    } else {
                        ClassSpan = "badge-success";
                    }
                    Data += '<span class="badge ' + ClassSpan + '" onClick="' + param + '"><i class="fa fa-eye-o"></i> ' + full.Jumat + '</span>';
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var param = "ShowEvalHarianSiswa('" + full.IdSiswa + "','" + Tanggal + "','6','" + cekQuotes(full.NamaSiswa) + "')";
                    var ClassSpan = "";
                    if (full.Sabtu == 0) {
                        ClassSpan = "badge-danger";
                    } else {
                        ClassSpan = "badge-success";
                    }
                    Data += '<span class="badge ' + ClassSpan + '" onClick="' + param + '"><i class="fa fa-eye-o"></i> ' + full.Sabtu + '</span>';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });

    TabelEvalHarianList.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

/////////////////////////// TABEL 5 ////////////////////////////////
function ShowEvalHarianSiswa(IdSiswa, Tanggal, HariKe, NamaSiswa) {
    $("#EvalHarianSiswa").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#EvalHarianSiswa").offset().top
    }, 50);
    $('#JudulMutabaahSiswa').empty();
    $('#JudulMutabaahSiswa').append('Daftar Mutabaah Ananda <Strong> ' + NamaSiswa + ' </strong> Tanggal   : ' + Tanggal);
    param = '?IdSiswa=' + IdSiswa + "&Tanggal=" + Tanggal + "&HariKe=" + HariKe;

    $('#TabelEvalHarianSiswa tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });

    console.log(UrlService.GetElEvalHarianSiswa + param)

    var TabelEvalHarianSiswa = $('#TabelEvalHarianSiswa').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 100,
        "lengthChange": false,
        "scrollX": true,
        "processing": false,
        "ajax": {
            "url": UrlService.GetElEvalHarianSiswa + param,
            "method": 'GET',
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Evaluasi Harian Siswa',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data.Data;
                }
            }
        },
        columnDefs: [
            { targets: [0], width: "auto", visible: true },
            { targets: [1], width: "10%", visible: true },
            { targets: [2], width: "auto", visible: true },
        ],
        "columns": [{
                "data": "NamaMutabaah"
            },
            {
                "render": function (data, type, full, meta) {
                    var HtmlData = "";
                    if (full.YaTidak == "Y") {
                        HtmlData = "<i class='fa fa-check fa-2x' style='color:green'></i>";
                    } else {
                        HtmlData = "<i class='fa fa-close fa-2x' style='color:red'></i>";
                    }
                    return HtmlData;
                }
            },
            {
                "data": "Catatan"
            },
        ],
        "bDestroy": true
    });

    TabelEvalHarianSiswa.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function DownloadLapMutabaahSiswa() {
    var IdKelasParalel = $('#IdKelasParalelDownload').val(),
        Tanggal = $('#TanggalDownload').val();

    window.open(base_url + '/api/Download/DownloadMutabaahSiswa?IdKelasParalel='+IdKelasParalel+'&Tanggal='+Tanggal);
    // $.ajax({
    //     url : base_url + '/Download/DownloadMutabaahSiswa?IdKelasParalel='+IdKelasParalel+'&Tanggal='+Tanggal,
    //     type: 'GET',
    //     dataType: 'JSON',
    //     beforeSend:(e) => {
    //         ProgressBar("wait");
    //     },
    //     success: (res) => {
    //         ProgressBar("success");
    //         swal({
    //             title: 'Sukses',
    //             text: 'Berhasil Download',
    //             confirmButtonClass: 'btn-danger text-white',
    //             confirmButtonText: 'Oke, Mengerti',
    //             type: 'error'
    //         });
    //     }
    // })
}