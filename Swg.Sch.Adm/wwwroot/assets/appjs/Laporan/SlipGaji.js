﻿function BtnPrintGaji() {
    $("#PrintSlipGaji").printThis({
        debug: false, // show the iframe for debugging
        importCSS: true, // import parent page css
        importStyle: true, // import style tags
        printContainer: true, // print outer container/$.selector
        loadCSS: "", // path to additional css file - use an array [] for multiple
        pageTitle: "", // add title to print page
        removeInline: false, // remove inline styles from print elements
        removeInlineSelector: "*", // custom selectors to filter inline styles. removeInline must be true
        printDelay: 333, // variable print delay
        header: null, // prefix to html
        footer: null, // postfix to html
        base: false, // preserve the BASE tag or accept a string for the URL
        formValues: true, // preserve input/form values
        canvas: false, // copy canvas content
        doctypeString: '...', // enter a different doctype for older markup
        removeScripts: false, // remove script tags from print content
        copyTagClasses: false, // copy classes from the html & body tag
        beforePrintEvent: null, // function for printEvent in iframe
        beforePrint: null, // function called before iframe is filled
        afterPrint: null // function called before iframe is removed
    });
}

$(document).ready(function () {
    GetGajiBulans();
    function myFunction(x) {
        if (x.matches) {
            $('.mode-print-header').removeAttr('style');
            $('.mode-print-header').removeAttr('style');
        } else {
            $('.mode-print-header').css('width', '24%');
            $('.mode-print-header').css('float', 'left');

            $('.mode-print-content').css('width', '33%');
            $('.mode-print-content').css('float', 'left');

            $('.mode-print-content').css('width', '33%');
            $('.mode-print-content').css('float', 'left');
        }
    }

    var x = window.matchMedia("(max-width: 700px)")
    myFunction(x)
    x.addListener(myFunction)
})

const UrlService = {
    GetGajiBulans: base_url + '/api/laporans/GetGajiBulans',
    GetGajiBulanUnits: base_url + '/api/laporans/GetGajiBulanUnits',
    GetGajiBulanUnitPegawais: base_url + '/api/laporans/GetGajiBulanUnitPegawais',
    GetSlipGaji: base_url + '/api/laporans/LapGetSlipGaji',
};

function GetGajiBulans(Month = 0, Year = 0) {
    $('#TabelGajiBulans tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelGajiBulans = $('#TabelGajiBulans').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": `${UrlService.GetGajiBulans}?Month=${Month}&Year=${Year}`,
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [{
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamEdit = "GetGajiBulanUnits('" + full.Periode + "')";
                    Data += '<button type="button" class="btn btn-primary btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-list"></i> Lihat Detil</button>';
                    return Data;
                }
            },
            {
                "data": "Periode"
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = formatRupiah(full.GT_Total);
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = formatRupiah(full.GTT_Total);
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = formatRupiah(full.Pot_Total);
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = formatRupiah(full.Tam_Total);
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = formatRupiah(full.Total);
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });
    TabelGajiBulans.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

/// TABEL KE 2 ////
function GetGajiBulanUnits(Periode) {
    $("#DivTabelGajiBulanUnitPegawais").css("display", "none");
    $("#SecSlipGaji").css("display", "none");

    $("#DivTabelGajiBulanUnits").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#DivTabelGajiBulanUnits").offset().top
    }, 50);

    $('#TabelGajiBulanUnits tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelGajiBulanUnits = $('#TabelGajiBulanUnits').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": UrlService.GetGajiBulanUnits + "?Periode=" + Periode,
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [{
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamEdit = "GetGajiBulanUnitPegawais('" + full.Periode + "','" + full.IdUnit + "')";
                    Data += '<button type="button" class="btn btn-primary btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-list"></i> Lihat Detil</button>';
                    return Data;
                }
            },
            {
                "data": "Unit"
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = formatRupiah(full.GT_Total);
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = formatRupiah(full.GTT_Total);
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = formatRupiah(full.Pot_Total);
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = formatRupiah(full.Tam_Total);
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = formatRupiah(full.Total);
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });
    TabelGajiBulanUnits.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function GetGajiBulanUnitPegawais(Periode, IdUnit) {
    $("#SecSlipGaji").css("display", "none");

    $("#DivTabelGajiBulanUnitPegawais").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#DivTabelGajiBulanUnitPegawais").offset().top
    }, 50);

    $('#TabelGajiBulanUnitPegawais tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelGajiBulanUnitPegawais = $('#TabelGajiBulanUnitPegawais').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": UrlService.GetGajiBulanUnitPegawais + "?Periode=" + Periode + "&IdUnit=" + IdUnit,
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [{
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamEdit = "GetSlipGaji('" + full.IdPegawai + "','" + full.Periode + "','" + full.Total + "')";
                    Data += '<button type="button" class="btn btn-primary btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-eye"></i> Lihat Slip Gaji</button>';
                    return Data;
                }
            },
            {
                "data": "Periode"
            },
            {
                "data": "Nip"
            },
            {
                "data": "Nama"
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = formatRupiah(full.GT_Total);
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = formatRupiah(full.GTT_Total);
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = formatRupiah(full.Pot_Total);
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = formatRupiah(full.Tam_Total);
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = formatRupiah(full.Total);
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });
    TabelGajiBulanUnitPegawais.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function GetSlipGaji(IdPegawai, Periode, Total) {
    $.ajax({
        url: UrlService.GetSlipGaji + '?IdPegawai=' + IdPegawai + "&Periode=" + Periode,
        type: 'GET',
        dataType: 'JSON',
        beforeSend: function (xhr) {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            var data = res.Data;
            if (res.IsSuccess) {

                $('#SecSlipGaji').fadeIn();
                $('html, body').animate({
                    scrollTop: $("#SecSlipGaji").offset().top
                }, 50);

                $('#Nama').html(data.Nama);
                $('#Bulan').html(data.Bulan);
                $('#Unit').html(data.Unit);
                $('#PendidikanTerakhir').html(data.PendidikanTerakhir);
                $('#TanggalMasuk').html(indoDate(data.TanggalMasuk));
                $('#Golongan').html(data.Golongan);
                $('#JenisPegawai').html(data.JenisPegawai);
                $('#Mkg').html(data.Mkg);
                $('#Terbilang').html(data.Terbilang);

                var htmlGajiTetaps = '';
                $.each(data.GajiTetaps, ((i, v) => {
                    if (v.NoUrut == 99) {
                        htmlGajiTetaps +=
                            '<div class="col-8">' +
                            '    <span><b>→ ' + v.Keterangan + '</b></span>' +
                            '</div>' +
                            '<div class="col-4 text-right">' +
                            '    <span><b>' + formatRupiah(v.Rp) + '</b></span>' +
                            '</div>';
                    } else {
                        htmlGajiTetaps +=
                            '<div class="col-8">' +
                            '    <span>→ ' + v.Keterangan + '</span>' +
                            '</div>' +
                            '<div class="col-4 text-right">' +
                            '    <span>' + formatRupiah(v.Rp) + '</span>' +
                            '</div>';
                    }

                }));
                $('#GajiTetaps').html(htmlGajiTetaps);

                var htmlGajiTidakTetaps = '';
                $.each(data.GajiTidakTetaps, ((i, v) => {
                    if (v.NoUrut == 99) {
                        htmlGajiTidakTetaps +=
                            '<div class="col-8">' +
                            '    <span><b>→ ' + v.Keterangan + '</b></span>' +
                            '</div>' +
                            '<div class="col-4 text-right">' +
                            '    <span><b>' + formatRupiah(v.Rp) + '</b></span>' +
                            '</div>';
                    } else {
                        htmlGajiTidakTetaps +=
                            '<div class="col-8">' +
                            '    <span>→ ' + v.Keterangan + '</span>' +
                            '</div>' +
                            '<div class="col-4 text-right">' +
                            '    <span>' + formatRupiah(v.Rp) + '</span>' +
                            '</div>';
                    }
                }));
                $('#GajiTidakTetaps').html(htmlGajiTidakTetaps);

                var htmlPotongans = '';
                console.log(data.Potongans)
                $.each(data.Potongans, ((i, v) => {
                    if (v.NoUrut == 99) {
                        htmlPotongans +=
                            '<div class="col-8">' +
                            '    <span><b>→ ' + v.Keterangan + '</b></span>' +
                            '</div>' +
                            '<div class="col-4 text-right">' +
                            '    <span><b>' + formatRupiah(v.Rp) + '</b></span>' +
                            '</div>';
                    } else {
                        htmlPotongans +=
                            '<div class="col-8">' +
                            '    <span>→ ' + v.Keterangan + '</span>' +
                            '</div>' +
                            '<div class="col-4 text-right">' +
                            '    <span>' + formatRupiah(v.Rp) + '</span>' +
                            '</div>';
                    }
                }));
                $('#Potongans').html(htmlPotongans);

                var htmlTambahans = '';
                $.each(data.Tambahans, ((i, v) => {
                    if (v.NoUrut == 99) {
                        htmlTambahans +=
                            '<div class="col-8">' +
                            '    <span><b>→ ' + v.Keterangan + '</b></span>' +
                            '</div>' +
                            '<div class="col-4 text-right">' +
                            '    <span><b>' + formatRupiah(v.Rp) + '</b></span>' +
                            '</div>';
                    } else {
                        if(v.NoUrut != 9){
                            htmlTambahans +=
                                '<div class="col-8">' +
                                '    <span>→ ' + v.Keterangan + '</span>' +
                                '</div>' +
                                '<div class="col-4 text-right">' +
                                '    <span>' + formatRupiah(v.Rp) + '</span>' +
                                '</div>';
                        }
                    }
                }));
                $('#Tambahans').html(htmlTambahans);

                var htmlTambahansBeasiswa = "";
                var rpBeasiswaPendAnak = 0;
                $.each(data.Tambahans, ((i, v) => {
                    if(v.NoUrut == 9){
                        htmlTambahansBeasiswa +=
                            '<div class="col-8">' +
                            '    <span>→ ' + v.Keterangan + '</span>' +
                            '</div>' +
                            '<div class="col-4 text-right">' +
                            '    <span>' + formatRupiah(v.Rp) + '</span>' +
                            '</div>';
                            rpBeasiswaPendAnak += v.Rp;
                    }
                }));
                $("#BeasiswaPendidikanAnak").html(htmlTambahansBeasiswa);
                
                
                var TakeHomePay = data.TakeHomePay - rpBeasiswaPendAnak;
                var htmlTakeHomePay = "";
                htmlTakeHomePay +=
                            '<div class="col-8">' +
                            '    <span>→ Take Home Pay</span>' +
                            '</div>' +
                            '<div class="col-4 text-right">' +
                            '    <span>' + formatRupiah(TakeHomePay) + '</span>' +
                            '</div>';
                $('#TakeHomePay').html(htmlTakeHomePay);
                var TotalDiterima = data.TakeHomePay;
                $('#TotalDiterima').html(formatRupiah(TotalDiterima));
            } else {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

function ModalFilter(bool) {
    if (bool) {
        $('#ModalFilter').modal('show');

        ComboGetTahun(function (obj) {
            $("select[name='Year']").html(obj);
            console.log('ok ->', obj)
        });
    } else {
        $('#ModalFilter').modal('hide');
    }
}

function BtnFilter() {
    var Month = $('#Month option:selected').val(),
        Year = $('#Year option:selected').val();
    GetGajiBulans(Month, Year)
    ModalFilter(false)
}