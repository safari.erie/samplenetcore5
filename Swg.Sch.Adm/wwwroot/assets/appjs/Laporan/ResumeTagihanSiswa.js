const UrlSvc = {
    GetResumeSiswaTagihan: `${base_url}/Siswas/GetResumeSiswaTagihan`
}

function BtnCari() {
    var tglAwal = $('#TglAwal').val(),
        tglAkhir = $('#TglAkhir').val();
    
    $.ajax({
        url: `${UrlSvc.GetResumeSiswaTagihan}?TanggalAwal=${tglAwal}&TanggalAkhir=${tglAkhir}`,
        type: "GET",
        dataType: "json",
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            if (res.IsSuccess) {
                $('#InfoTotalAll12').html(formatRupiah(res.Data.TotalAll12));
                

                $('#InfoTotalSpp2').html(formatRupiah(res.Data.TotSpp2));
                $('#InfoTotalBoks2').html(formatRupiah(res.Data.TotBoks2));
                $('#InfoTotalJemputan2').html(formatRupiah(res.Data.TotJemputan2));
                $('#InfoKatering2').html(formatRupiah(res.Data.TotKatering2));
                $('#InfoKomite2').html(formatRupiah(res.Data.TotKomite2));
                $('#InfoAll2').html(formatRupiah(res.Data.TotAll2));

                $('#InfoTotalSpp1').html(formatRupiah(res.Data.TotSpp1));
                $('#InfoTotalBoks1').html(formatRupiah(res.Data.TotBoks1));
                $('#InfoTotalJemputan1').html(formatRupiah(res.Data.TotJemputan1));
                $('#InfoKatering1').html(formatRupiah(res.Data.TotKatering1));
                $('#InfoKomite1').html(formatRupiah(res.Data.TotKomite1));
                $('#InfoAll1').html(formatRupiah(res.Data.TotAll1));
            } else {
                swal({
                    title: 'Gagal',
                    text: responsesuccess.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error',
                text: JSON.stringify(responserror),
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }

    })
}