ComboGetSiswa(function (obj) {
    $("select[name='IdSiswa']").html(obj);
});

const UrlSvc = {
    GetLaporanRiwayatTransaksiBank: `${base_url}/Siswas/GetLaporanRiwayatTransaksiBank`
}

$("select[name='IdSiswa']", "#FormTabelLapSiswa").change(function () {
    $("#ImgEmpty").hide();
});
function remove_quotes(values1) {
	if(values1 == null) return null;		
	values1 = values1.replace(/"/g, "\\'");
	var values = values1.toString();
	var str = "";
	var blockList = ['"','\'','\\']; // This is the list of key words to be escaped
	var flag = 0;
	for(var i = 0;i<values.length;i++)
	{
		for(var j=0;j<blockList.length;j++)
		{
			if(values[i] == blockList[j])
			{
				flag = 1;
				break;
			}
		}
		if(flag == 0)
		str += values[i];
		else
		{
			str += '\\';
			str += values[i];
			flag = 0;
		}
	}
	return str;
}

TabelData(GetCurrentDateLocal("TanggalOnly"), GetCurrentDateLocal("TanggalOnly"))
function TabelData(TanggalAwal, TanggalAkhir) {
    $('#TabelData tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelData = $('#TabelData').DataTable({
            "paging": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "pageLength": 10,
            "lengthChange": true,
            "scrollX": true,
            "processing": true,
            "ajax": {
                "url": `${UrlSvc.GetLaporanRiwayatTransaksiBank}?TanggalAwal=${TanggalAwal}&TanggalAkhir=${TanggalAkhir}`,
                "method": 'GET',
                "beforeSend": function (xhr) {},
                "dataSrc": function (json) {
                    if (json.IsSuccess) {
                        return json.Data;
                    } else {
                        return json;
                    }
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return `<button type="button" class="btn btn-primary btn-sm" onclick="Print(
                            ${full.KodeBayar},
                            '${full.Status}',
                            '${remove_quotes(full.Nama)}',
                            '${full.Kelas}',
                            '${full.Tanggal}',
                            '${full.Catatan}',
                            ${full.JumlahBayar},
                           ' ${full.Terbilang}',
                            )" ${full.Status === 'BELUM LUNAS' ? 'disabled' : ''}>
                        <i class="fa fa-print"></i>  Print
                        </button> `;
                    }
                },
                {
                    "render": function (data, type, full, meta) {
                        return `<code>${full.KodeBayar}</code>`;
                    }
                },
                {
                    "render": function (data, type, full, meta) {
                        return full.Nama.toUpperCase();
                    }
                },
                {
                    "data": "Tipe"
                },
                {
                    "render": function (data, type, full, meta) {
                        return full.JumlahBayar.toLocaleString('id-ID');
                    }
                },
                {
                    "render": function (data, type, full, meta) {
                        var data = ``;
                        data = `<span class='badge badge-danger'>GAGAL</span>`;
                        if (full.Status === 'LUNAS') {
                            data = `<span class='badge badge-success'>LUNAS</span>`;
                        }
                        return data;
                    }
                },
                {
                    "data": "Tanggal"
                },
                {
                    "data": "KeteranganGagal"
                },
                
            ],
            "bDestroy": true
    });
    TabelData.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
}

function Print(
    KodeBayar,
    Status,
    Nama,
    Kelas,
    Tanggal,
    Catatan,
    JumlahBayar,
    Terbilang,
) {
    var doc = new jsPDF("landscape");
    doc.setFont("helvetica", "bold");
    doc.setFontSize(18);
    doc.text("Bank Syariah Indonesia", 150, 20, null, null, "center");
    doc.setFontSize(22);
    doc.setFont("courier", "normal");
    doc.text("Bukti Pembayaran Pendidikan", 150, 30, null, null, "center");
    doc.addImage("/assets/img/bsi.png", "PNG", 30, 13, 50, 17, "left");
    doc.addImage("/assets/img/atq.png", "PNG", 230, 13, 25, 25, "right");
    doc.setDrawColor(178, 178, 178);
    doc.setLineWidth(0.1);
    doc.line(20, 45, 280, 45);
    doc.setTextColor(47, 47, 47);

    doc.setFontSize(18);
    doc.text("TRANSAKSI ", 20, 58);
    doc.setFontSize(15);
    doc.text("KODE BAYAR", 20, 70);
    doc.text(`: ${KodeBayar}`, 90, 70);
    doc.text("STATUS", 20, 80);
    doc.text(`: ${Status}`, 90, 80);
    doc.text("INSITUSI", 20, 90);
    doc.text(": ISLAMIC CENTER ATTAUFIQ BOGOR", 90, 90);
    doc.text("NAMA", 20, 100);
    doc.text(`: ${Nama.toUpperCase()}`, 90, 100);
    doc.text("KELAS", 20, 110);
    doc.text(`: ${Kelas}`, 90, 110);
    doc.text("TANGGAL BAYAR", 20, 120);
    doc.text(`: ${Tanggal}`, 90, 120);

    doc.setFontSize(18);
    doc.text("ITEM PEMBAYARAN ", 20, 138);
    doc.setFontSize(15);
    doc.text("DESKRIPSI", 20, 150);
    doc.text(`: ${Catatan.toUpperCase()}`, 90, 150);
    doc.text("PEMBAYARAN", 20, 160);
    doc.text(`: ${JumlahBayar.toLocaleString('id-ID')}`, 90, 160);
    doc.setFont("courier", "bold");
    doc.text("TOTAL DIBAYAR ", 20, 170);
    doc.text(`: ${JumlahBayar.toLocaleString('id-ID')}`, 90, 170);
    doc.setFont("courier", "normal");
    doc.text(`(${Terbilang.toUpperCase()} )`, 19, 180);
    doc.setFont("courier", "normal");
    doc.setTextColor(255, 0, 0);
    doc.setFontSize(11);
    doc.text("*HARAP TANDA BUKTI INI DISIMPAN SEBAGAI BUKTI PEMBAYARAN YANG SAH ", 20, 193);
    doc.save(`buktibayar-${KodeBayar}-${Tanggal}.pdf`);
}

function ModalFilter() {
    $('#ModalFilter').modal('show');
}

function Filter() {
    var TanggalAwal = $('#TanggalAwal').val();
    var TanggalAkhir = $('#TanggalAkhir').val();
    TabelData(TanggalAwal, TanggalAkhir);
    $('#ModalFilter').modal('hide');
}
    