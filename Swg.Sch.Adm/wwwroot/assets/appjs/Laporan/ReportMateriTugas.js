﻿$(document).ready(function () {
    Unit()
})
const laporan = '/api/Laporans/';
var TUnitHeader;
var TUnitKelasParalel;
var TMateriTugasInput;
var TMateriTugasNonInput;

var ajaxGlobRetrieve = function (url, params) {
    var dataArray = $.ajax({
        type: "GET",
        dataType: 'json',
        async: false,
        url: url + params,
    });

    var result = dataArray.responseJSON; //JSON.stringify
    return result;
}

const svcMateri = {
    GetUnit: base_url + laporan + 'GetElMateriTugas',
    GetElMateriTugasKelas: base_url + laporan + 'GetElMateriTugasKelas',
    GetElMateriTugasKelasParalel: base_url + laporan + 'GetElMateriTugasKelasParalel',
    GetElMateriTugasList: base_url + laporan + 'GetElMateriTugasList',
}

function Pencarian() {
    var Tanggal = $('#TanggalPencarian').val();
    Unit(Tanggal);
}

function Unit(tanggal = "") {
    $('#ModalPencarian').modal('hide');

    $('#TabelUnit tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });

    var TUnit = $('#TabelUnit').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": svcMateri.GetUnit + "?Tanggal=" + tanggal,
            "method": 'GET',
            "async": 'false',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                console.log(json.Data);
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Unit',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    $('#TanggalAwal').html(indoDate(json.Data.Periode.TanggalAwal));
                    $('#TanggalAkhir').html(indoDate(json.Data.Periode.TanggalAkhir));
                    return json.Data.Units;
                }
            }
        },
        "columns": [

            {
                "data": "Unit"
            },
            {
                "data": "JmlTotal"
            },
            {
                "data": "JmlInput"
            },
            {
                "data": "JmlNoInput"
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var getViewData = "ShowMateriTugasKelas('" + full.IdUnit + "','" + tanggal + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + getViewData + '"><i class="fa fa-eye"></i> Lihat</button>';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });

    TUnit.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function ShowMateriTugasKelas(IdUnit, Tanggal) {
    $("#MateriTugasKelas").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#MateriTugasKelas").offset().top
    }, 50);
    $('#TabelMateriTugasKelas tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    param = '?IdUnit=' + IdUnit + "&Tanggal=" + Tanggal;
    console.log(param)
    TUnitHeader = $('#TabelMateriTugasKelas').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": svcMateri.GetElMateriTugasKelas + param,
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Materi Tugas Kelas',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        columnDefs: [{
                targets: [0],
                width: "auto",
                visible: true
            },
            {
                targets: [1],
                width: "auto",
                visible: true
            },
            {
                targets: [2],
                width: "auto",
                visible: true
            },
            {
                targets: [3],
                width: "auto",
                visible: true
            },
            {
                targets: [4],
                width: "auto",
                visible: true
            },
            {
                targets: [5],
                width: "auto",
                visible: true
            },
        ],
        "columns": [

            {
                "data": "Unit"
            },
            {
                "data": "Kelas"
            },
            {
                "data": "JmlTotal"
            },
            {
                "data": "JmlInput"
            },
            {
                "data": "JmlNoInput"
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var getViewData = "ShowUnitKelasParalel('" + full.IdKelas + "','" + Tanggal + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + getViewData + '"><i class="fa fa-eye"></i> Lihat</button>';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });


    TUnitHeader.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");

}

function ShowUnitKelasParalel(IdKelas, Tanggal) {
    $("#MateriTugasKelasParalel").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#MateriTugasKelasParalel").offset().top
    }, 50);
    $('#TabelMateriTugasKelasParalel tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    param = '?IdKelas=' + IdKelas + "&Tanggal=" + Tanggal;
    TUnitKelasParalel = $('#TabelMateriTugasKelasParalel').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": svcMateri.GetElMateriTugasKelasParalel + param,
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Materi Tugas Kelas',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [

            {
                "data": "Unit"
            },
            {
                "data": "Kelas"
            },
            {
                "data": "KelasParalel"
            },
            {
                "data": "JmlTotal"
            },
            {
                "data": "JmlInput"
            },
            {
                "data": "JmlNoInput"
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var getViewData = "ShowMateriTugas('" + full.IdKelasParalel + "','" + full.Unit + "','" + full.Kelas + "','" + full.KelasParalel + "','" + Tanggal + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + getViewData + '"><i class="fa fa-eye"></i> Lihat</button>';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });

    TUnitKelasParalel.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function ShowMateriTugas(IdKelasParalel, Unit, Kelas, KelasParalel, Tanggal) {
    $("#MateriTugasList").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#MateriTugasList").offset().top
    }, 50);
    $('#titleInput').empty();
    $('#titleInput').append('Daftar Pengajuan <Strong> SUDAH </strong> Input Materi Unit   : ' + Unit + "  Kelas : " + Kelas + " Kelas Paralel :" + KelasParalel);
    $('#titleNotInput').empty();
    $('#titleNotInput').append('Daftar Pengajuan <Strong> BELUM </strong> Input Materi Unit   : ' + Unit + "  Kelas : " + Kelas + " Kelas Paralel :" + KelasParalel);
    param = '?IdKelasParalel=' + IdKelasParalel + "&Tanggal=" + Tanggal;
    $('#TabelMateriTugasInputList tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });

    TMateriTugasInput = $('#TabelMateriTugasInputList').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": false,
        "scrollX": true,
        "processing": false,
        "ajax": {
            "url": svcMateri.GetElMateriTugasList + param,
            "method": 'GET',
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Materi Daftar Tugas',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data.Input;
                }
            }
        },

        columnDefs: [{
                targets: [0],
                width: "auto",
                visible: true
            },
            {
                targets: [1],
                width: "auto",
                visible: true
            },
            {
                targets: [2],
                width: "auto",
                visible: true
            },
            {
                targets: [3],
                width: "auto",
                visible: true
            },
            {
                targets: [4],
                width: "auto",
                visible: true
            },
            {
                targets: [5],
                width: "auto",
                visible: true
            },
            {
                targets: [6],
                width: "auto",
                visible: true
            },
        ],
        "columns": [{
                "data": "NamaGuru"
            },
            {
                "data": "MataPelajaran"
            },
            {
                "data": "Hari"
            },
            {
                "data": "JamMulai"
            },
            {
                "data": "JamSelesai"
            },
            {
                "data": "JudulMateri"
            },
            {
                "data": "FileMateri"
            },
        ],
        "bDestroy": true
    });

    TMateriTugasInput.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");

    $('#TabelMateriTugasNonInputList tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });

    TabelMateriTugasNonInputList = $('#TabelMateriTugasNonInputList').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": false,
        "scrollX": true,
        "processing": false,
        "ajax": {
            "url": svcMateri.GetElMateriTugasList + param,
            "method": 'GET',
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Materi Daftar Tugas',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data.NotInput;
                }
            }
        },

        columnDefs: [{
                targets: [0],
                width: "auto",
                visible: true
            },
            {
                targets: [1],
                width: "auto",
                visible: true
            },
            {
                targets: [2],
                width: "auto",
                visible: true
            },
            {
                targets: [3],
                width: "auto",
                visible: true
            },
            {
                targets: [4],
                width: "auto",
                visible: true
            },
            {
                targets: [5],
                width: "auto",
                visible: true
            },
            {
                targets: [6],
                width: "auto",
                visible: true
            },
        ],
        "columns": [{
                "data": "NamaGuru"
            },
            {
                "data": "MataPelajaran"
            },
            {
                "data": "Hari"
            },
            {
                "data": "JamMulai"
            },
            {
                "data": "JamSelesai"
            },
            {
                "data": "JudulMateri"
            },
            {
                "data": "FileMateri"
            },
        ],
        "bDestroy": true
    });

    TabelMateriTugasNonInputList.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}