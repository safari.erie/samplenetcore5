﻿$(document).ready(function () {
    Unit(GetCurrentDate("TanggalOnly"));
})

const laporan = '/api/Laporans/';
var TUnit;
var TUnitKelas;
var TUnitKelasParalel;
var TSiswaMapel;
var TKehadiran;
var TTidakHadir;


const svcKehadiran = {
    GetElAbsenSiswa: base_url + laporan + 'GetElAbsenSiswa',
    GetElAbsenSiswaKelas: base_url + laporan + 'GetElAbsenSiswaKelas',
    GetElAbsenSiswaKelasParalel: base_url + laporan + 'GetElAbsenSiswaKelasParalel',
    GetElAbsenSiswaMapel: base_url + laporan + 'GetElAbsenSiswaMapel',
    GetElAbsenSiswaList: base_url + laporan + 'GetElAbsenSiswaList',

}

function Pencarian() {
    var Tanggal = $('#TanggalPencarian').val();
    Unit(Tanggal);
}

function Unit(Tanggal = '') {
    let param = '?Tanggal=' + Tanggal;

    $('#ModalPencarian').modal('hide');

    $('#TabelUnit tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });

    var TUnit = $('#TabelUnit').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": svcKehadiran.GetElAbsenSiswa + param,
            "method": 'GET',
            "async": 'false',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                console.log(json.Data);
                if (json.Data.Units == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Unit',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    $('#TanggalAwal').html(json.Data.Periode.TanggalAwal);
                    $('#TanggalAkhir').html(json.Data.Periode.TanggalAkhir);
                    return json.Data.Units;
                }
            }
        },
        "columns": [

            {
                "data": "Unit"
            },
            {
                "data": "JmlTotal"
            },
            {
                "data": "JmlHadir"
            },
            {
                "data": "JmlNoHadir"
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var getViewData = "ShowKehadiranKelas('" + full.IdUnit + "','" + Tanggal + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + getViewData + '"><i class="fa fa-pencil-square-o"></i> Lihat</button>';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });

    TUnit.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function ShowKehadiranKelas(IdUnit, Tanggal = '') {
    let param = '?IdUnit=' + IdUnit + '&Tanggal=' + Tanggal;
    $('#SecKelas').show();
    $('html, body').animate({
        scrollTop: $("#SecKelas").offset().top
    }, 50);
    $('#TabelMateriTugasKelas tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });

    TUnitKelas = $('#TabelKelas').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": svcKehadiran.GetElAbsenSiswaKelas + param,
            "method": 'GET',
            "async": 'false',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                console.log(json.Data);
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Unit',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [

            {
                "data": "Unit"
            },
            {
                "data": "Kelas"
            },
            {
                "data": "JmlTotal"
            },
            {
                "data": "JmlHadir"
            },
            {
                "data": "JmlNoHadir"
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var getViewData = "ShowKehadiranKelasParalel('" + full.IdKelas + "','" + Tanggal + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + getViewData + '"><i class="fa fa-pencil-square-o"></i> Lihat</button>';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });

    TUnitKelas.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function ShowKehadiranKelasParalel(IdKelas, Tanggal) {
    let param = '?IdKelas=' + IdKelas + '&Tanggal=' + Tanggal;
    $('#SecKelasParalel').show();
    $('html, body').animate({
        scrollTop: $("#SecKelasParalel").offset().top
    }, 50);
    $('#TabelKelasParalel tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });

    TUnitKelasParalel = $('#TabelKelasParalel').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": svcKehadiran.GetElAbsenSiswaKelasParalel + param,
            "method": 'GET',
            "async": 'false',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Unit',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [

            {
                "data": "Unit"
            },
            {
                "data": "Kelas"
            },
            {
                "data": "KelasParalel"
            },
            {
                "data": "JmlTotal"
            },
            {
                "data": "JmlHadir"
            },
            {
                "data": "JmlNoHadir"
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var getViewData = "ShowSiswaMapel('" + full.IdKelasParalel + "','" + full.Unit + "','" + full.Kelas + "','" + full.KelasParalel + "','" + Tanggal + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + getViewData + '"><i class="fa fa-pencil-square-o"></i> Lihat</button>';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });

    TUnitKelasParalel.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function ShowSiswaMapel(IdKelasParalel, Unit, Kelas, KelasParalel, Tanggal) {
    let param = '?IdKelasParalel=' + IdKelasParalel + '&Tanggal=' + Tanggal;
    $('#SecSiswaMapelList').show();
    $('html, body').animate({
        scrollTop: $("#SecSiswaMapelList").offset().top
    }, 50);
    $('#TabelSiswaMapelList tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });

    TSiswaMapel = $('#TabelSiswaMapelList').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": svcKehadiran.GetElAbsenSiswaMapel + param,
            "method": 'GET',
            "async": 'false',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Unit',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [

            {
                "data": "Unit"
            },
            {
                "data": "Kelas"
            },
            {
                "data": "KelasParalel"
            },
            {
                "data": "NamaMapel"
            },
            {
                "data": "JmlTotal"
            },
            {
                "data": "JmlHadir"
            },
            {
                "data": "JmlNoHadir"
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var getViewData = "ShowAbsenSiswaList('" + full.IdKelasParalel + "','" + full.Unit + "','" + full.Kelas + "','" + full.KelasParalel + "','" + full.IdMapel + "','" + Tanggal + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + getViewData + '"><i class="fa fa-pencil-square-o"></i> Lihat</button>';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });

    TSiswaMapel.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function ShowAbsenSiswaList(IdKelasParalel, Unit, Kelas, KelasParalel, IdMapel, Tanggal) {
    $('#titleInput').empty();
    $('#titleInput').append('Daftar Siswa  <Strong> Hadir </strong> Kelas Daring Unit   : ' + Unit + "  Kelas : " + Kelas + " Kelas Paralel :" + KelasParalel);
    $('#titleTidakHadir').empty();
    $('#titleTidakHadir').append('Daftar Siswa  <Strong> Tidak Hadir </strong> Kelas Daring Unit   : ' + Unit + "  Kelas : " + Kelas + " Kelas Paralel :" + KelasParalel);

    let param = '?IdKelasParalel=' + IdKelasParalel + '&IdMapel=' + IdMapel + '&Tanggal=' + Tanggal;
    $('#SecSiswaHadir').show();
    $('#SecSiswaTidakHadir').show();
    $('html, body').animate({
        scrollTop: $("#SecSiswaHadir").offset().top
    }, 50);
    $('#TabelSiswaHadir tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });

    TKehadiran = $('#TabelSiswaHadir').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": svcKehadiran.GetElAbsenSiswaList + param,
            "method": 'GET',
            "async": 'false',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Unit',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data.Hadir;
                }
            }
        },
        "columns": [

            {
                "data": "NamaSiswa"
            },
            {
                "data": "MataPelajaran"
            },
            {
                "data": "Hari"
            },
            {
                "data": "JamMulai"
            },
            {
                "data": "JamSelesai"
            },

        ],
        "bDestroy": true
    });

    TKehadiran.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });


    $('#TabelSiswaHadir tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });

    TTidakHadir = $('#TabelSiswaTidakHadir').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": svcKehadiran.GetElAbsenSiswaList + param,
            "method": 'GET',
            "async": 'false',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Unit',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data.NoHadir;
                }
            }
        },
        "columns": [

            {
                "data": "NamaSiswa"
            },
            {
                "data": "MataPelajaran"
            },
            {
                "data": "Hari"
            },
            {
                "data": "JamMulai"
            },
            {
                "data": "JamSelesai"
            },

        ],
        "bDestroy": true
    });

    TTidakHadir.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");

}