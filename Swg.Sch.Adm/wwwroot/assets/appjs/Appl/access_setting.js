﻿/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 14042020
================================================================================================================= */

//////// START SECTION TABEL ACCESS SETTING ///////////
// $('#TabelAccessSetting tfoot th').each(function () {
//     var title = $(this).text();
//     $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
// });
// var TabelAccessSetting = $('#TabelAccessSetting').DataTable({
//     "paging": true,
//     "searching": true,
//     "ordering": false,
//     "info": false,
//     "pageLength": 5,
//     "lengthChange": false,
//     "scrollX": true,
//     "processing": true,
//     "ajax": {
//         "url": base_url + "/HakAksess/GetHakAksess",
//         "method": 'GET',
//         "beforeSend": function (xhr) {
//         },
//         "dataSrc": function (json) {
//             if (json.Data == null) {
//                 swal({ title: 'Gagal Menampilkan Data Access', text: json.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
//                 return json;
//             } else {
//                 return json.Data;
//             }
//         }
//     },
//     columnDefs: [
//         { targets: [0], width: "35%", visible: true },
//         { targets: [1], width: "35%", visible: true },
//         { targets: [2], width: "30%", visible: true },
//     ],
//     "columns": [
//         { "data": "IdHakAkses" },
//         {
//             "data": "NamaHakAkses",
//             "render": function (data, type, full, meta) {
//                 return full.NamaHakAkses;
//             }
//         },
//         {
//             "data": "IdHakAkses",
//             "render": function (data, type, full, meta) {
//                 var ParamRole = "'" + full.IdHakAkses + "','" + full.NamaHakAkses + "'";
//                 var aksi_set_menu = 'onClick="EditAccess(' + ParamRole + ');"';
//                 //var aksi_set_hak_akses = 'onClick="HapusAccess(' + ParamRole + ');"';
//                 data = '<button type="button" class="btn btn-info" ' + aksi_set_menu + '><i class="fa fa-pencil-alt"></i> Edit</button> ';
//                 return data;
//             }
//         },
//     ],
//     "bDestroy": true
// });
// TabelAccessSetting.columns().every(function () {
//     var that = this;

//     $('input', this.footer()).on('keyup change clear', function () {
//         if (that.search() !== this.value) {
//             that
//                 .search(this.value)
//                 .draw();
//         }
//     });
// });
// $(".dataTables_filter").css("display", "none");
//////// START SECTION TABEL ACCESS SETTING ///////////

$("#HeaderBody").html("Manajemen Access");
$("#CardBody").css({ "height": "500px" });

function EditAccess(IdAccess) {
    $('#FormSetAccess')[0].reset();
    $("#ImgSearch").css("display", "none");
    $("#FormSetAccess").css("display", "block");
    $("#HeaderBody").html("Edit Data - Manajemen Access");
    $("input[name='IdAccess']").attr("readonly", "readonly");

    $.ajax({
        url: base_url + "/HakAksess/GetHakAkses?IdHakAkses=" + parseInt(IdAccess),
        type: "GET",
        dataType: "json",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            $("input[name='IdAccess']").val(responsesuccess.Data.IdHakAkses);
            $("input[name='AccessName']").val(responsesuccess.Data.NamaHakAkses);
        }, error: function (errorresponse) {
            swal({ title: 'Gagal Menampilkan Data Access', text: errorresponse, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
    $("#BtnSave").attr("onClick", "save_access('EditData')");
}
function TambahAccess() {
    $('#FormSetAccess')[0].reset();
    $("#ImgSearch").css("display", "none");
    $("#FormSetAccess").css("display", "block");
    $("#HeaderBody").html("Tambah Data - Manajemen Access");
    $("input[name='IdAccess']").removeAttr("readonly");

    $("#BtnSave").attr("onClick","save_access('TambahData')");
}

function save_access(Tipe) {
    var formObj = $('#FormSetAccess').serializeObject();
    if (Tipe == "EditData") {
        var Url = base_url + "/HakAksess/EditHakAkses?IdHakAkses=" + parseInt(formObj.IdAccess) + "&NamaHakAkses=" + formObj.AccessName;
    } else if (Tipe == "TambahData") {
        var Url = base_url + "/HakAksess/AddHakAkses?IdHakAkses=" + parseInt(formObj.IdAccess) + "&NamaHakAkses=" + formObj.AccessName;
    }

    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $('#FormSetAccess')[0].reset();
                $("#TabelAccessSetting").DataTable().ajax.reload();
                swal({ title: 'Berhasil', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}
