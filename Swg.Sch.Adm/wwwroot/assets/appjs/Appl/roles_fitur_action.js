﻿

/* Created  : Eri Safari
 * CreatedDt : 20200405
 *
 * TODO
 * 1. 
 *
 * History
 * ==================================================================
 * NO   | Date      | Description
 * ==================================================================
 * 1      20200405    Starting File action From UI
 *
 *
 * */



$(document).ready(function () {

});


function EditAccess(idJabatan, namaJabatan) {
    $("#ImgSearch").css("display", "none");
    $("#FormSetData").css("display", "block");
    $("#SetHakAkses").css("display", "block");

    $("#LabelNoAssign").html("No Assign Accesses");
    $("#LabelAssign").html("Assign Accesses");
    $('#idJabatan').val(idJabatan);
    $("#HeaderBody").html("Set Hak Akses - " + namaJabatan);
    $('.ComboSumber').attr("name", "NoAssignAccesses[]");
    $('.ComboTerpilih').attr("name", "AssignAccesses[]");
    $("#BtnSave").attr("onClick", "save_data('SetHakAkses')");
    GenerateAksesFitur(function (HtmlComboNoAssignAccesses, HtmlComboAssignAccesses) {
        $("select[name='NoAssignAccesses[]']").empty();
        $("select[name='NoAssignAccesses[]']").append(HtmlComboNoAssignAccesses);

        $("select[name='AssignAccesses[]']").empty();
        $("select[name='AssignAccesses[]']").append(HtmlComboAssignAccesses);
    }, idJabatan);

}

function GenerateAksesFitur(handleData, IdRole) {
    var Url = base_url + "/HakAksess/GetHakAksesByJabatan?IdJabatanProyek=" + parseInt(IdRole);
    console.log(Url);

    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var HtmlComboNoAssignAccesses = "";
                var HtmlComboAssignAccesses = "";
                var JmlNoAssignAccesses = (responsesuccess.Data.NoHakAksess).length;
                var JmlAssignAccesses = (responsesuccess.Data.HakAksess).length;

                for (var inat = 0; inat < JmlNoAssignAccesses; inat++) {
                    HtmlComboNoAssignAccesses += "<option value='" + responsesuccess.Data.NoHakAksess[inat].IdHakAkses + "'>" + responsesuccess.Data.NoHakAksess[inat].NamaHakAkses + "</option>";
                }
                for (var iat = 0; iat < JmlAssignAccesses; iat++) {
                    HtmlComboAssignAccesses += "<option value='" + responsesuccess.Data.HakAksess[iat].IdHakAkses + "' selected>" + responsesuccess.Data.HakAksess[iat].NamaHakAkses + "</option>";
                }

                handleData(HtmlComboNoAssignAccesses, HtmlComboAssignAccesses);

            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function save_data(Tipe) {
    var formObj = $('#FormSetData').serializeObject();
    var id_jabatan = $('#idJabatan').val();
    if (Tipe == "SetHakAkses") {
        var IdAccesses = "";
        $('.ComboTerpilih').find('option').each(function (index, item) {
            IdAccesses += "&IdHakAksess=" + parseInt($(item).val());
        });
        var Url = base_url + "/HakAksess/AddHakAksesJabatan?IdJabatanProyek=" + parseInt(id_jabatan) + IdAccesses;
    }
    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $('#FormSetData')[0].reset();
                $("#TabelRoleSetting").DataTable().ajax.reload();
                window.open(location.href, '_self');
                swal({ title: 'Berhasil', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}