﻿

/* Created  : Eri Safari
 * CreatedDt : 20200405
 *
 * TODO
 * 1. 
 *
 * History
 * ==================================================================
 * NO   | Date      | Description
 * ==================================================================
 * 1      20200405    Starting File action for list data
 * 
 * 
 * sn == servicename
 *
 * */


var tableJabatan;
$(document).ready(function () {
    var sn_list_jabatan = "GetJabatanProyeks";
    var url_jabatan = url_projects + sn_list_jabatan;
    var dataJabatan = ajaxs(url_jabatan, "");
    tableJabatan = $('#Table-Jabatan').DataTable({
        "paging": true,
        "searching": true,
        "ordering": false,
        "info": false,
        "pageLength": 5,
        "lengthChange": false,
        "scrollX": true,
        "processing": true,
        "data": dataJabatan.Data,
        columnDefs: [
            { targets: [0], width: "15%", visible: true },
            { targets: [1], width: "55%", visible: true },
            { targets: [2], width: "30%", visible: true },
        ],
        "columns": [
            { "data": "Id" },
            {
                "data": "Nama",
                "render": function (data, type, full, meta) {
                    return full.Nama;
                }
            },
            {
                "data": "IdHakAkses",
                "render": function (data, type, full, meta) {
                    var ParamRole = "'" + full.Id + "','" + full.Nama +"'";
                    var aksi_set_menu = 'onClick="EditAccess(' + ParamRole + ');"';
                    data = '<button type="button" class="btn btn-info" ' + aksi_set_menu + '><i class="fa fa-pencil-alt"></i> Hak Akses</button> ';
                    return data;
                }
            },
        ],
        "bDestroy": true
    });

});
$(".dataTables_filter").css("display", "none");