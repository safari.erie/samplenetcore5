﻿$(document).ready(function () {
    DataList();
    ComboGetListUser(function (obj) {
        $('select#IdKetua').html(obj);
    });
    ComboGetTahunAjaran(function (obj) {
        $('select#TahunAjaran').html(obj);
    });
})

function DataEditor() {
    $('div#data').fadeIn();
    $('div#no-data').hide();
    $("#FormEditor")[0].reset();
    $('input[name="IdPpdb"]').val('');
    $('input[name="TanggalAwal"]').val('');
    $('input[name="TanggalAkhir"]').val('');
    $('#BtnDel').attr('onclick', 'Kembali();');
    $('#htmlDel').html('<i class="fa fa-arrow-alt-circle"></i> Kembali');
    $('span#FilePetunjuk').html('');
}

function DataList() {
    $.ajax({
        url: base_url + '/api/ppdbs/GetPpdb',
        type: 'GET',
        success: function (res) {
            if (res.IsSuccess) {
                $('div#no-data').hide();
                $('div#data').fadeIn();

                $('#BtnDel').attr('onclick', 'DataHapus();');
                $('#htmlDel').html('<i class="fa fa-times"></i> Hapus');

                $('input#IdPpdb').val(res.Data.IdPpdb);
                ComboGetTahunAjaran(function (obj) {
                    $('select#TahunAjaran').html(obj);
                }, res.Data.IdPpdb);
                ComboGetListUser(function (obj) {
                    $('select#IdKetua').html(obj);
                }, res.Data.IdKetua);
                $('input[name="MinNoSk"]').val(res.Data.MinNoSk);
                $('input[name="TahunAjaran"]').val(res.Data.TahunAjaran);
                $('input[name="TanggalAwal"]').val(res.Data.TanggalAwal);
                $('input[name="TanggalAkhir"]').val(res.Data.TanggalAkhir);
                $('span#FilePetunjuk').html("<a href='#' target='_blank' >" + res.Data.FilePetunjuk + "</a>");
            } else if (!res.IsSuccess) {
                $('div#empty-data').fadeIn();
                $('div#loading-data').hide();
                iziToast.error({
                    title: 'Gagal',
                    message: res.ReturnMessage,
                    position: 'topRight'
                });
            }
        },
        error: function (err, a, e) {
            iziToast.error({
                title: 'Gagal',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    })
}

function DataSave() {
    var CekIdPpdb = $('input#IdPpdb').val();
    var formObj = $('#FormEditor').serializeObject();
    var NamaTahunAjaran = $('#TahunAjaran option:selected').text();

    var fd = new FormData($('#FormEditor')[0]);
    if (CekIdPpdb != "") {
        var url = base_url + '/api/ppdbs/ppdbedit';
    } else {
        var url = base_url + '/api/ppdbs/ppdbadd';
    }
    // alert(NamaTahunAjaran)
    // return;
    fd.append("IdPpdb", parseInt(formObj.TahunAjaranx));
    fd.append("IdKetua", formObj.IdKetua);
    fd.append("MinNoSk", formObj.MinNoSk);
    fd.append("TahunAjaran", NamaTahunAjaran);
    fd.append("TanggalAwal", formObj.TanggalAwal);
    fd.append("TanggalAkhir", formObj.TanggalAkhir);
    fd.append('FilePetunjuk', $('input[type="file"][name="FilePetunjuk"]')[0].files[0]);
    $.ajax({
        "url": url,
        "method": "POST",
        "timeout": 0,
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": fd,
        "dataType": "JSON",
        "beforeSend": function (xhr) {
            ProgressBar("wait");
        },
        "success": function (res) {
            ProgressBar("success");
            if (res.IsSuccess) {
                DataList();
                iziToast.success({
                    title: 'Sukses',
                    message: 'Data berhasil disimpan',
                    position: 'topRight'
                });
            } else if (!res.IsSuccess) {
                iziToast.error({
                    title: 'Gagal',
                    message: res.ReturnMessage,
                    position: 'topRight'
                });
            }
        },
        "error": function (err, a, e) {
            ProgressBar("success");
            iziToast.error({
                title: 'Terjadi Kesalahan',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    })
}

function DataHapus() {
    var IdPpdb = $('#TahunAjaran option:selected').val();
    swal({
            title: "Apakah Anda Yakin ?",
            text: "Anda akan menghapus data ini",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: false
        },
        function () {
            var Url = base_url + "/api/ppdbs/ppdbdelete?idppdb=" + IdPpdb;
            $.ajax({
                url: Url,
                method: "get",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (res) {
                    ProgressBar("success");
                    if (res.IsSuccess == true) {
                        $('div#no-data').fadeIn();
                        $('div#data').hide();

                        $('div#empty-data').fadeIn();
                        $('div#loading-data').hide();

                        swal({
                            title: 'Sukses',
                            text: "Data berhasil dihapus",
                            confirmButtonClass: 'btn-success text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'success'
                        });
                    } else if (res.IsSuccess == false) {
                        swal({
                            title: 'Gagal',
                            text: res.ReturnMessage,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                },
                error: function (err, a, e) {
                    ProgressBar("success");
                    swal({
                        title: 'Terjadi Kesalahan',
                        text: JSON.stringify(err),
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            });
        });
}

function Kembali() {
    $('div#no-data').fadeIn();
    $('div#data').hide();

    $('div#empty-data').fadeIn();
    $('div#loading-data').hide();
}