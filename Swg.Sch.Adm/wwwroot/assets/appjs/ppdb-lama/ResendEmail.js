﻿$('#TabelResendEmail tfoot th').each(function () {
    var title = $(this).text();
    $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
});
var TabelResendEmail = $('#TabelResendEmail').DataTable({
    "paging": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "pageLength": 10,
    "lengthChange": true,
    "scrollX": true,
    "processing": true,
    "ajax": {
        "url": base_url + "/api/Ppdbs/GetPpdbDaftarList?Proses=Semua",
        "method": 'GET',
        "beforeSend": function (xhr) {
        },
        "dataSrc": function (json) {
            if (json.Data == null) {
                iziToast.error({
                    title: 'Gagal Menampilkan Data Tabel Resend Email',
                    message: json.ReturnMessage,
                    position: 'topRight'
                });
                return json;
            } else {
                iziToast.success({
                    title: 'Berhasil Menampilkan Data Tabel Resend Email',
                    message: json.ReturnMessage,
                    position: 'topRight'
                });
                return json.Data;
            }
        }
    },
    "columns": [
        {
            "render": function (data, type, full, meta) {
                var Data = "";
                var ParamEdit = "EditData('" + full.IdPpdbDaftar + "')";
                Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-paper-plane"></i> Kirim Email</button>';
                return Data;
            }
        },
        { "data": "IdPpdbDaftar" },
        { "data": "Unit" },
        { "data": "JenisPendaftaran" },
        { "data": "JalurPendaftaran" },
        { "data": "KategoriPendaftaran" },
        { "data": "Kelas" },
        { "data": "Nama" },
        {
            "render": function (data, type, full, meta) {
                
                return indoDate(full.TanggalLahir);
            }
        },
        { "data": "NamaIbu" },
        { "data": "NoHandphoneOrtu" },
        {
            "render": function (data, type, full, meta) {

                return indoDate(full.TanggalDaftar);
            }
        },
    ],
    "bDestroy": true
});
TabelResendEmail.columns().every(function () {
    var that = this;

    $('input', this.footer()).on('keyup change clear', function () {
        if (that.search() !== this.value) {
            that
                .search(this.value)
                .draw();
        }
    });
});
$(".dataTables_filter").css("display", "none");
$(".dataTables_length").css("display", "none");

function EditData(IdPpdbDaftar) {
    $("#ModalResendEmail").modal("show");
    $.ajax({
        url: base_url + '/api/ppdbs/GetPpdbDaftarList?Proses=Semua',
        type: 'GET',
        dataType: 'JSON',
        success: function (res) {
            if (res.IsSuccess) {
                for (var i = 0; i < res.Data.length; i++) {
                    if (res.Data[i].IdPpdbDaftar == IdPpdbDaftar) {
                        $("input[name='IdPpdbDaftar']","#FormKirimEmail").val(res.Data[i].IdPpdbDaftar); 
                        $("input[name='Nama']", "#FormKirimEmail").val(res.Data[i].Nama); 
                        $("input[name='Email']", "#FormKirimEmail").val(res.Data[i].EmailOrtu); 
                    }
                }
            } else if (!res.IsSuccess) {
                iziToast.error({
                    title: 'Gagal',
                    message: res.ReturnMessage,
                    position: 'topRight'
                });
            }
        },
        error: function (err, a, e) {
            iziToast.error({
                title: 'Terjadi Kesalahan',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    });
}

function KirimEmail() {
    var formObj = $('#FormKirimEmail').serializeObject();

    $.ajax({
        url: base_url + "/api/Ppdbs/ResendEmail?IdPpdbDaftar=" + parseInt(formObj.IdPpdbDaftar) + "&Proses=" + formObj.Proses,
        type: "GET",
        dataType: "json",
        success: function (responsesendmail) {
            if (responsesendmail.IsSuccess == true) {
                $("#FormKirimEmail")[0].reset();
                $("#ModalResendEmail").modal("hide");
                swal({ title: 'Berhasil Mengirim Email', text: "Berhasil mengirim email ke " + formObj.Email, confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesendmail.IsSuccess == false) {
                swal({ title: 'Gagal Mengirim Email', text: responsesendmail.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            swal({ title: 'Gagal Mengirim Email', text: jqXHR + " " + textStatus + " " + errorThrown, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}