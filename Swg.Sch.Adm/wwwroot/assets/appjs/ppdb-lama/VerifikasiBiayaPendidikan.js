﻿$(document).ready(function () {
    DataList()
})

function DataEditor(Id = null) {
    $('div#data').fadeIn();
    $('div#no-data').hide();

    $('div#EditorForm').fadeIn();
    $('div#ListData').hide();
    if (Id != null) {
        GetById(Id);
    } else {
        $("#FormEditor")[0].reset();
        $('input[name="IdPpdbBiaya"]').val('');

    }
}
var i = 0;

function DataList() {
    $('#TabelData tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelData = $('#TabelData').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/api/ppdbs/GetPpdbDaftarList?proses=Verifikasi Biaya Pendidikan",
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null || (json.Data).length == 0) {
                    $('div#empty-data').fadeIn();
                    $('div#loading-data').hide();
                    return json;
                } else {
                    $('div#no-data').hide();
                    $('div#data').fadeIn();
                    return json.Data;
                }
            }
        },
        "columns": [{
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamProses = "DataEditor('" + full.IdPpdbDaftar + "')";
                    var ParamBack = "ModalKembalikan('" + full.IdPpdbDaftar + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamProses + '"><i class="fa fa-rocket"></i> Proses</button>&nbsp; ';
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamBack + '"><i class="fa fa-truck"></i> Kembalikan</button>';
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var data = '';
                    if (full.StatusBayarPendidikan == 1)
                        data = '<span class="badge badge-success">Bayar</span>';
                    if (full.StatusBayarPendidikan == 2)
                        data = '<span class="badge badge-danger">Tidak Bayar</span>';
                    if (full.StatusBayarPendidikan == 0)
                        data = '<span class="badge badge-warning">Pending</span>';
                    return data;
                }
            },
            {
                "data": "IdPpdbDaftar"
            },
            {
                "data": "Unit"
            },
            {
                "data": "JenisPendaftaran"
            },
            {
                "data": "JalurPendaftaran"
            },
            {
                "data": "KategoriPendaftaran"
            },
            {
                "data": "Kelas"
            },
            {
                "data": "Nama"
            },
            {
                "render": function (data, type, full, meta) {

                    return indoDate(full.TanggalLahir);
                }
            },
            {
                "data": "NamaIbu"
            },
            {
                "data": "NoHandphoneOrtu"
            },
            {
                "render": function (data, type, full, meta) {

                    return indoDate(full.TanggalDaftar);
                }
            },
        ],
        "bDestroy": true
    });
    TabelData.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function DataSetLunasPendidikan() {
    var IdPpdbDaftar = $("input[name='IdPpdbDaftar']").val();
    var Catatan = $("textarea[name='Catatan']").val();
    $.ajax({
        url: base_url + "/api/Ppdbs/SetLunasPendidikan?IdPpdbDaftar=" + IdPpdbDaftar + "&Catatan=" + Catatan,
        type: "GET",
        dataType: "json",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            $("#TabelData").DataTable().ajax.reload();
            $('div#EditorForm').hide();
            $('div#ListData').fadeIn();
            $('#ModalProses').modal('hide');
            if (res.IsSuccess) {
                swal({
                    title: 'Sukses',
                    text: "Anda berhasil perbarui status menjadi lunas",
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
            } else if (!res.IsSuccess) {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (err, a, e) {
            ProgressBar("success");
            $('div#EditorForm').hide();
            $('div#ListData').fadeIn();
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(err),
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function DataSetTidakLunasPendidikan() {
    var IdPpdbDaftar = $("input[name='IdPpdbDaftar']").val();
    var Catatan = $("textarea[name='Catatan']").val();
    $.ajax({
        url: base_url + "/api/Ppdbs/SetTidakLunasPendidikan?IdPpdbDaftar=" + IdPpdbDaftar + "&Catatan=" + Catatan,
        type: "GET",
        dataType: "json",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            $("#TabelData").DataTable().ajax.reload();
            $('div#EditorForm').hide();
            $('div#ListData').fadeIn();
            $('#ModalProses').modal('hide');
            $("textarea[name='Catatan']").val('-');
            if (res.IsSuccess) {
                swal({
                    title: 'Sukses',
                    text: "Anda berhasil perbarui status menjadi tidak lunas",
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
            } else if (!res.IsSuccess) {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (err, a, e) {
            ProgressBar("success");
            $('div#EditorForm').hide();
            $('div#ListData').fadeIn();
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(err),
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function DataSetCicilPendidikan() {
    var IdPpdbDaftar = $("input[name='IdPpdbDaftar']").val();
    var Catatan = $("textarea[name='Catatan']").val();

    var fd = new FormData($('#FormCicil')[0]);
    var getCilKe = document.getElementsByName('CilKe[]');
    var getRpCilKe = document.getElementsByName('RpCilKe[]');
    var getTanggalCilKe = document.getElementsByName('TanggalCilKe[]');
    for (var i = 0; i < getCilKe.length; i++) {
        fd.append('Cicilans[' + i + '].' + 'CilKe', getCilKe[i].value);
        fd.append('Cicilans[' + i + '].' + 'RpCilKe', getRpCilKe[i].value);
        fd.append('Cicilans[' + i + '].' + 'TanggalCilKe', getTanggalCilKe[i].value);
    }
    fd.append('FilePerjanjian', $('input[type="file"][name="FilePerjanjian"]')[0].files[0]);
    $.ajax({
        url: base_url + "/api/Ppdbs/SetCicilPendidikan?IdPpdbDaftar=" + IdPpdbDaftar + "&Catatan=" + Catatan,
        method: "POST",
        timeout: 0,
        processData: false,
        mimeType: "multipart/form-data",
        contentType: false,
        data: fd,
        dataType: "JSON",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            $("#TabelData").DataTable().ajax.reload();
            $('div#EditorForm').hide();
            $('div#ListData').fadeIn();
            $('#ModalProses').modal('hide');
            if (res.IsSuccess) {
                swal({
                    title: 'Sukses',
                    text: "Anda berhasil perbarui status menjadi cicilan",
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
            } else if (!res.IsSuccess) {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (err, a, e) {
            ProgressBar("success");
            $('div#EditorForm').hide();
            $('div#ListData').fadeIn();
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(err),
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function DataProsesModal(Val) {
    if (Val == 'lunas') {
        $('div#alertTidakBayar').hide();
        $('div#secFormCicil').hide();
        $('#BtnProses').attr('onclick', 'DataSetLunasPendidikan()');
    } else if (Val == 'cicil') {
        $('div#alertTidakBayar').hide();
        $('div#secFormCicil').show();
        $('#BtnProses').attr('onclick', 'DataSetCicilPendidikan()');
    } else if (Val == 'tidaklunas') {
        $('div#alertTidakBayar').show();
        $('div#secFormCicil').hide();
        $('#BtnProses').attr('onclick', 'DataSetTidakLunasPendidikan()');
    }
    $('#ModalProses').modal('show');
}

function DataKembalikan() {
    var IdPpdbDaftar = $('input#IdPpdbDaftar', '#FormKembalikan').val();
    var IdProses = $('select#IdProses', '#FormKembalikan').val();
    $.ajax({
        url: base_url + '/api/ppdbs/backtostatus?idppdbdaftar=' + IdPpdbDaftar + '&ToStatus=' + IdProses,
        type: 'GET',
        dataType: 'JSON',
        beforeSend: function () {
            ProgressBar('wait');
        },
        success: function (res) {
            ProgressBar('success');
            $('#ModalKembalikan').modal('hide');
            $("#TabelData").DataTable().ajax.reload();
            if (res.IsSuccess) {
                swal({
                    title: 'Sukses',
                    text: "",
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
            } else if (!res.IsSuccess) {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (err, a, e) {
            ProgressBar("success");
            $('#ModalKembalikan').modal('hide');
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(err),
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

function ModalKembalikan(Id) {
    $('#ModalKembalikan').modal('show');
    GetById(Id)
}

function CreateFormCicil(Val = null) {
    i++;
    var html = "";
    html +=
        '<input type="hidden" class="form-control" name="CilKe[]" value="' + (i + 1) + '">' +
        '<div class="form-row">' +
        '    <div class="form-group col-md-6">' +
        '        <label for="">Cicilan Ke ' + (i + 1) + '</label>' +
        '        <input type="number" class="form-control" name="RpCilKe[]" placeholder="Ketikan Jumlah...">' +
        '    </div>' +
        '    <div class="form-group col-md-6">' +
        '        <label for="">Tanggal</label>' +
        '        <input type="text" class="form-control" id="TanggalCilKe" data-toggle="datepicker" name="TanggalCilKe[]" placeholder="dd/mm/yyyy" readonly="">' +
        '    </div>' +
        '</div>';
    $('div#htmlformCicil').append(html);
    $('span#hapusForm').html('<a href="#" onclick="ResetFormCicil()"><b>(Reset)</b></a>');
    SetDatepicker();

}

function ResetFormCicil() {
    $('div#htmlformCicil').empty();
    $('span#hapusForm').empty();
}

function GetById(Id) {
    scrollTop();
    $.ajax({
        url: base_url + '/api/ppdbs/GetPpdbDaftar?IdPpdbDaftar=' + Id,
        dataType: 'JSON',
        type: 'GET',
        success: function (res) {
            if (res.IsSuccess) {
                const {
                    IdPpdbDaftar,
                    IdJenisKategoriPendaftaran,
                    IdJenisPendaftaran,
                    IdJalurPendaftaran,
                    Agama,
                    AlamatOrtu,
                    AlamatTinggal,
                    Email,
                    EmailOrtu,
                    IdAgama,
                    IdJenisPekerjaanAyah,
                    IdJenisPekerjaanIbu,
                    IdJenisPrestasi,
                    IdKelas,
                    IdKelasParalel,
                    IdSiswa,
                    IdUnit,
                    JenisKelamin,
                    JenisPekerjaanAyah,
                    JenisPekerjaanIbu,
                    KdJenisKelamin,
                    Kelas,
                    KelasParalel,
                    Nama,
                    NamaAyah,
                    NamaIbu,
                    NamaInstansiAyah,
                    NamaInstansiIbu,
                    NamaPanggilan,
                    NikSiswa,
                    NikAyah,
                    NikIbu,
                    Nisn,
                    NoDarurat,
                    NoHandphoneOrtu,
                    Status,
                    StrStatus,
                    TanggalLahir,
                    TempatLahir,
                    Unit,
                } = res.Data;
                GetKategoriDaftars(IdJenisKategoriPendaftaran)
                GetJenisPendaftarans(IdJenisPendaftaran)
                GetJalurPendaftarans(IdJalurPendaftaran)
                ComboGetUnit(function (obj) {
                    $("select[name='IdUnit']").html(obj);
                }, IdUnit);

                ComboGetKelas(function (obj) {
                    $("select[name='IdKelas']").html(obj);
                }, IdUnit, IdKelas);

                ComboGetJenisKelamin(function (obj) {
                    $("select[name='KdJenisKelamin']").html(obj);
                }, KdJenisKelamin);

                ComboGetJenisPekerjaan(function (obj) {
                    $("select[name='IdJenisPekerjaanIbu']").html(obj);
                }, IdJenisPekerjaanIbu);

                ComboGetJenisPekerjaan(function (obj) {
                    $("select[name='IdJenisPekerjaanAyah']").html(obj);
                }, IdJenisPekerjaanAyah);

                $('input#IdPpdbDaftar').val(IdPpdbDaftar)
                $('input#NikSiswa').val(NikSiswa)
                $('input#Nama').val(Nama)
                $('input#NamaPanggilan').val(NamaPanggilan)
                $('input#KdJenisKelamin').val(KdJenisKelamin)
                $('input#AlamatTinggal').val(AlamatTinggal)
                $('select#IdAgama').val(IdAgama)
                $('input#TempatLahir').val(TempatLahir)
                $('input#TanggalLahir').val(TanggalLahir)
                $('input#NikIbu').val(NikIbu)
                $('input#NamaIbu').val(NamaIbu)
                $('select#IdJenisPekerjaanIbu').val(IdJenisPekerjaanIbu)
                $('input#NamaInstansiIbu').val(NamaInstansiIbu)
                $('input#NikAyah').val(NikAyah)
                $('input#NamaAyah').val(NamaAyah)
                $('select#IdJenisPekerjaanAyah').val(IdJenisPekerjaanAyah)
                $('input#NamaInstansiAyah').val(NamaInstansiAyah)
                $('input#AlamatOrtu').val(AlamatOrtu)
                $('input#NoHandphoneOrtu').val(NoHandphoneOrtu)
                $('input#EmailOrtu').val(EmailOrtu)

                $('input#IdPpdbDaftar', '#FormKembalikan').val(Id)
                $('input#NamaCalonSiswa', '#FormKembalikan').val(Nama)
                $('input#NamaProses', '#FormKembalikan').val(Status)
                $('input#Email', '#FormKembalikan').val(EmailOrtu)

                console.log(res)
            } else if (!res) {

            }
        },
        error: function (err, a, e) {

        }
    })
}

function GetKategoriDaftars(Id) {
    $.getJSON(base_url + "/api/ppdbs/GetKategoriDaftars", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.Id == Id)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                else
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('select[name="IdJenisKategoriPendaftaran"]').html(html);
    });
}

function GetJenisPendaftarans(Id) {
    $.getJSON(base_url + "/api/ppdbs/GetJenisPendaftarans", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.Id == Id)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                else
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('select[name="IdJenisPendaftaran"]').html(html);
    });
}

function GetJalurPendaftarans(Id) {
    $.getJSON(base_url + "/api/ppdbs/GetJalurPendaftarans", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.Id == Id)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                else
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('select[name="IdJalurPendaftaran"]').html(html);
    });
}

function Kembali() {
    $('div#EditorForm').hide();
    $('div#ListData').fadeIn();
}

function ValidateFile(Type) {
    if (Type == 'FilePerjanjian') {
        let file = document.querySelector("#FilePerjanjian");
        if (/\.(pdf)$/i.test(file.files[0].name) === false) {
            swal({
                title: 'Gagal',
                text: 'Format File Salah',
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
            $('input#FilePerjanjian').val('')
        }
    }
}

var xInputCicils = 1;

function CreateFormCicils() {
    var HtmlData = "";
    HtmlData +=
        '<div class="form-row" id="InputUrlVideo_' + xInputCicils + '">' +
        '<input type="hidden" class="form-control" name="CilKe[]" value="' + (xInputCicils + 1) + '">' +
        '    <div class="form-group col-md-6">' +
        '        <label for="">Cicilan Ke ' + (xInputCicils + 1) + '</label>&nbsp; &nbsp;<a href="javascript:void(0);" class="text-danger" onClick="DeleteFormCicils(' + xInputCicils + ');"><i class="fa fa-times"></i></a>' +
        '        <input type="number" class="form-control" name="RpCilKe[]" placeholder="Ketikan Jumlah...">' +
        '    </div>' +
        '    <div class="form-group col-md-6">' +
        '        <label for="">Tanggal</label>' +
        '        <input type="text" class="form-control" id="TanggalCilKe" data-toggle="datepicker" name="TanggalCilKe[]" placeholder="dd/mm/yyyy" readonly="">' +
        '    </div>' +
        '</div>';
    $("#htmlformCicil").append(HtmlData);
    xInputCicils++;
    SetDatepicker();

}

function DeleteFormCicils(IdBtn) {
    $("#InputUrlVideo_" + IdBtn).remove();
    xInputCicils--;
}