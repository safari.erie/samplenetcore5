﻿$(document).ready(function () {
    DataList()

    $('.FormPrestasis').each((i, v) => {
        console.log(i)
    })
})

function DataEditor(Id = null) {
    $('div#data').fadeIn();
    $('div#no-data').hide();

    $('div#EditorForm').fadeIn();
    $('div#ListData').hide();
    if (Id != null) {
        GetById(Id);
    } else {
        $("#FormEditor")[0].reset();
        $('input[name="IdPpdbBiaya"]').val('');

    }
}

function DataList() {
    $('#TabelData tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelData = $('#TabelData').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/api/ppdbs/GetPpdbDaftarList?proses=Aktivasi Va",
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null || (json.Data).length == 0) {
                    $('div#empty-data').fadeIn();
                    $('div#loading-data').hide();
                    return json;
                } else {
                    $('div#no-data').hide();
                    $('div#data').fadeIn();
                    return json.Data;
                }
            }
        },
        "columns": [{
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamAktivasiVa = "DataEditor('" + full.IdPpdbDaftar + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamAktivasiVa + '"><i class="fa fa-rocket"></i> Proses</button>';
                    return Data;
                }
            },
            {
                "data": "IdPpdbDaftar"
            },
            {
                "data": "Unit"
            },
            {
                "data": "JenisPendaftaran"
            },
            {
                "data": "JalurPendaftaran"
            },
            {
                "data": "KategoriPendaftaran"
            },
            {
                "data": "Kelas"
            },
            {
                "data": "Nama"
            },
            {
                "render": function (data, type, full, meta) {

                    return indoDate(full.TanggalLahir);
                }
            },
            {
                "data": "NamaIbu"
            },
            {
                "data": "NoHandphoneOrtu"
            },
            {
                "render": function (data, type, full, meta) {

                    return indoDate(full.TanggalDaftar);
                }
            },
        ],
        "bDestroy": true
    });
    TabelData.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function DataAktivasiVa() {
    var IdPpdbDaftar = $("input[name='IdPpdbDaftar']").val();

    swal({
            title: "Apakah Anda Yakin ?",
            text: "Anda akan proses berkas ini",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                url: base_url + "/api/Ppdbs/AktivasiVa?IdPpdbDaftar=" + IdPpdbDaftar,
                type: "GET",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (res) {
                    ProgressBar("success");
                    $("#TabelData").DataTable().ajax.reload();
                    $('div#EditorForm').hide();
                    $('div#ListData').fadeIn();
                    if (res.IsSuccess) {
                        swal({
                            title: 'Sukses',
                            text: "Anda berhasil verifikasi berkas",
                            confirmButtonClass: 'btn-success text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'success'
                        });
                    } else if (!res.IsSuccess) {
                        swal({
                            title: 'Gagal',
                            text: res.ReturnMessage,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                },
                error: function (err, a, e) {
                    ProgressBar("success");
                    $('div#EditorForm').hide();
                    $('div#ListData').fadeIn();
                    swal({
                        title: 'Terjadi Kesalahan',
                        text: JSON.stringify(err),
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            });
        });
}

function DataAktivasiVas() {
    swal({
            title: "Apakah Anda Yakin ?",
            text: "Anda akan proses semua berkas ini",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                url: base_url + "/api/Ppdbs/AktivasiVas?",
                type: "GET",
                dataType: "json",
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (res) {
                    ProgressBar("success");
                    $("#TabelData").DataTable().ajax.reload();
                    $('div#EditorForm').hide();
                    $('div#ListData').fadeIn();
                    if (res.IsSuccess) {
                        swal({
                            title: 'Sukses',
                            text: "Anda berhasil verifikasi berkas",
                            confirmButtonClass: 'btn-success text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'success'
                        });
                    } else if (!res.IsSuccess) {
                        swal({
                            title: 'Gagal',
                            text: res.ReturnMessage,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                },
                error: function (err, a, e) {
                    ProgressBar("success");
                    $('div#EditorForm').hide();
                    $('div#ListData').fadeIn();
                    swal({
                        title: 'Terjadi Kesalahan',
                        text: JSON.stringify(err),
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            });
        });
}

function GetById(Id) {
    scrollTop()
    $.ajax({
        url: base_url + '/api/ppdbs/GetPpdbDaftar?IdPpdbDaftar=' + Id,
        dataType: 'JSON',
        type: 'GET',
        success: function (res) {
            if (res.IsSuccess) {
                const {
                    IdPpdbDaftar,
                    IdJenisKategoriPendaftaran,
                    IdJenisPendaftaran,
                    IdJalurPendaftaran,
                    Agama,
                    AlamatOrtu,
                    AlamatTinggal,
                    Email,
                    EmailOrtu,
                    IdAgama,
                    IdJenisPekerjaanAyah,
                    IdJenisPekerjaanIbu,
                    IdJenisPrestasi,
                    IdKelas,
                    IdKelasParalel,
                    IdSiswa,
                    IdUnit,
                    JenisKelamin,
                    JenisPekerjaanAyah,
                    JenisPekerjaanIbu,
                    KdJenisKelamin,
                    Kelas,
                    KelasParalel,
                    Nama,
                    NamaAyah,
                    NamaIbu,
                    NamaInstansiAyah,
                    NamaInstansiIbu,
                    NamaPanggilan,
                    NikSiswa,
                    NikAyah,
                    NikIbu,
                    Nisn,
                    NoDarurat,
                    NoHandphoneOrtu,
                    Status,
                    StrStatus,
                    TanggalLahir,
                    TempatLahir,
                    Unit,
                    Prestasis
                } = res.Data;
                GetKategoriDaftars(IdJenisKategoriPendaftaran)
                GetJenisPendaftarans(IdJenisPendaftaran)
                GetJalurPendaftarans(IdJalurPendaftaran)
                ComboGetUnit(function (obj) {
                    $("select[name='IdUnit']").html(obj);
                }, IdUnit);

                ComboGetKelas(function (obj) {
                    $("select[name='IdKelas']").html(obj);
                }, IdUnit, IdKelas);

                if (Unit == "SMAIT AT Taufiq") {
                    $('#divIdKelasParalel').fadeIn();
                } else {
                    $('#divIdKelasParalel').hide();
                }

                ComboGetKelasParalel(function (obj) {
                    $("select[name='IdKelasParalel']").html(obj);
                }, IdKelas, IdKelasParalel);

                ComboGetJenisKelamin(function (obj) {
                    $("select[name='KdJenisKelamin']").html(obj);
                }, KdJenisKelamin);

                ComboGetJenisPekerjaan(function (obj) {
                    $("select[name='IdJenisPekerjaanIbu']").html(obj);
                }, IdJenisPekerjaanIbu);

                ComboGetJenisPekerjaan(function (obj) {
                    $("select[name='IdJenisPekerjaanAyah']").html(obj);
                }, IdJenisPekerjaanAyah);

                $('input#IdPpdbDaftar').val(IdPpdbDaftar)
                $('input#NikSiswa').val(NikSiswa)
                $('input#Nama').val(Nama)
                $('input#NamaPanggilan').val(NamaPanggilan)
                $('input#KdJenisKelamin').val(KdJenisKelamin)
                $('input#AlamatTinggal').val(AlamatTinggal)
                $('select#IdAgama').val(IdAgama)
                $('input#TempatLahir').val(TempatLahir)
                $('input#TanggalLahir').val(TanggalLahir)
                $('input#NikIbu').val(NikIbu)
                $('input#NamaIbu').val(NamaIbu)
                $('select#IdJenisPekerjaanIbu').val(IdJenisPekerjaanIbu)
                $('input#NamaInstansiIbu').val(NamaInstansiIbu)
                $('input#NikAyah').val(NikAyah)
                $('input#NamaAyah').val(NamaAyah)
                $('select#IdJenisPekerjaanAyah').val(IdJenisPekerjaanAyah)
                $('input#NamaInstansiAyah').val(NamaInstansiAyah)
                $('input#AlamatOrtu').val(AlamatOrtu)
                $('input#NoHandphoneOrtu').val(NoHandphoneOrtu)
                $('input#EmailOrtu').val(EmailOrtu)

                if (IdJalurPendaftaran == 1)
                    $('small#infoPrestasi').show();
                else
                    $('small#infoPrestasi').hide();


                $.each(Prestasis, (i, v) => {
                    i = i + 1;
                    CreateFormPrestasiss();
                    setTimeout((e) => {
                        GetJenisPrestasis(i, v.IdJenisPrestasi);
                        GetTingkatPrestasiSiswas(i, v.IdJenisTingkatPrestasi);
                        GetTahuns(i, v.Tahun);
                        $('#IdPpdbDaftarPrestasi' + i).val(v.IdPpdbDaftarPrestasi);
                        $('#NamaPenyelenggara' + i).val(v.NamaPenyelenggara);
                        $('#Keterangan' + i).val(v.Keterangan);
                    }, 1000)
                });
            } else if (!res) {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

function GetKategoriDaftars(Id) {
    $.getJSON(base_url + "/api/ppdbs/GetKategoriDaftars", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.Id == Id)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                else
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('select[name="IdJenisKategoriPendaftaran"]').html(html);
    });
}

function GetJenisPendaftarans(Id) {
    $.getJSON(base_url + "/api/ppdbs/GetJenisPendaftarans", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.Id == Id)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                else
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('select[name="IdJenisPendaftaran"]').html(html);
    });
}

function GetJalurPendaftarans(Id) {
    $.getJSON(base_url + "/api/ppdbs/GetJalurPendaftarans", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.Id == Id)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                else
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('select[name="IdJalurPendaftaran"]').html(html);
    });
}

function Kembali() {
    $('div#EditorForm').hide();
    $('div#ListData').fadeIn();
    $("#TabelData").DataTable().ajax.reload();
    $(".dataTables_scrollHeadInner, .table-bordered").css("width", "inherit");

    $.ajax({
        url: base_url + '/api/ppdbs/GetPpdbDaftar?IdPpdbDaftar=' + $('#IdPpdbDaftar').val(),
        dataType: 'JSON',
        type: 'GET',
        success: (res) => {
            $.each(res.Data.Prestasis, (i, v) => {
                i = i + 1;
                DeleteFormPrestasis(i)
            });
        }
    });
}


function GetJenisPrestasis(i, IdJenisPrestasi = 0) {
    $.getJSON(base_url + "/api/ppdbs/GetJenisPrestasis", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (IdJenisPrestasi == v.Id) {
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                } else {
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
                }
            });
        } else console.log(res)
        $('#IdJenisPrestasi' + i).html(html);
    });
}

function GetTingkatPrestasiSiswas(i, IdJenisTingkatPrestasi = 0) {
    $.getJSON(base_url + "/api/ppdbs/GetTingkatPrestasiSiswas", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (IdJenisTingkatPrestasi == v.Id) {
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                } else {
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
                }
            });
        } else console.log(res)
        $('#IdJenisTingkatPrestasi' + i).html(html);
        // $('select[name="IdJenisTingkatPrestasi[]"]').html(html);
    });
}

function GetTahuns(i, Tahun = 0) {
    var StartDate = (new Date).getFullYear() - 4;
    var EndDate = (new Date).getFullYear();
    var HtmlCombo = "";
    HtmlCombo += "<option value=''>Select One</option>";
    for (var ix = StartDate; ix <= EndDate; ix++) {
        if (Tahun == ix) {
            HtmlCombo += "<option value='" + ix + "' selected>" + ix + "</option>"; //jadikan selected
        } else {
            HtmlCombo += "<option value='" + ix + "'>" + ix + "</option>"; //jadikan selected
        }
    }
    $('#Tahun' + i).html(HtmlCombo)
    // $('select[name="Tahun[]"]').html(HtmlCombo)
}


var xInputPrestasis = 1;

function CreateFormPrestasiss() {
    var HtmlData = "";
    HtmlData +=
        '<div class="form-row FormPrestasis" id="InputPrestasis_' + xInputPrestasis + '">' +
        '    <input type="hidden" class="form-control" name="IdPpdbDaftarPrestasi[]" id="IdPpdbDaftarPrestasi' + xInputPrestasis + '"/>' +
        '    <div class="form-group col-md-4">' +
        '        <label for=""> Prestasi</label>' +
        '        <select disabled class="form-control" name="IdJenisPrestasi[]" id="IdJenisPrestasi' + xInputPrestasis + '" data-input="wajib-prestasis" required>' +
        '        </select>' +
        '        <div class="invalid-feedback">' +
        '            Kolom Wajib diisi.' +
        '        </div>' +
        '    </div>' +
        '    <div class="form-group col-md-4">' +
        '        <label for="">Tingkat Prestasi</label>' +
        '        <select disabled class="form-control" name="IdJenisTingkatPrestasi[]" id="IdJenisTingkatPrestasi' + xInputPrestasis + '" data-input="wajib-prestasis" required>' +
        '            <option value="">Select One</option>' +
        '        </select>' +
        '        <div class="invalid-feedback">' +
        '            Kolom Wajib diisi.' +
        '        </div>' +
        '    </div>' +
        '    <div class="form-group col-md-4">' +
        '        <label for="">Tahun</label>' +
        '        <select disabled class="form-control" name="Tahun[]" id="Tahun' + xInputPrestasis + '" data-input="wajib-prestasis" required>' +
        '            <option value="">Select One</option>' +
        '        </select>' +
        '        <div class="invalid-feedback">' +
        '            Kolom Wajib diisi.' +
        '        </div>' +
        '    </div>' +
        '    <div class="form-group col-md-6">' +
        '        <label for="">Nama Penyelenggara</label>' +
        '        <input disabled type="text" class="form-control" name="NamaPenyelenggara[]" id="NamaPenyelenggara' + xInputPrestasis + '" placeholder="Ketik Nama Penyelenggara..." data-input="wajib-prestasis" required />' +
        '        <div class="invalid-feedback">' +
        '            Kolom Wajib diisi.' +
        '        </div>' +
        '    </div>' +
        '    <div class="form-group col-md-6">' +
        '        <label for="">Keterangan</label> (optional)' +
        '        <input disabled type="text" class="form-control" name="Keterangan[]" id="Keterangan' + xInputPrestasis + '" placeholder="Ketik Keterangan..." />' +
        '    </div>' +
        '    <div class="form-group col-md-12"><hr /></div>' +
        '</div>';
    $("#formPrestasis").append(HtmlData);
    GetJenisPrestasis(xInputPrestasis);
    GetTingkatPrestasiSiswas(xInputPrestasis);
    GetTahuns(xInputPrestasis);
    xInputPrestasis++;
    SetDatepicker();

}

function DeleteFormPrestasis(IdBtn) {
    $("#InputPrestasis_" + IdBtn).remove();
    xInputPrestasis--;
}

function DownloadBerkasPrestasi() {
    var IdPpdbDaftar = $('#IdPpdbDaftar').val();
    var url = base_url + "/api/ppdbs/PrintBerkasPrestasi?idppdbdaftar=" + IdPpdbDaftar;
    window.open(url, '_blank');
}