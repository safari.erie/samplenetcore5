﻿$(document).ready(function () {
    DataList()
    GetJenisBiayaPpdbs()
    $('select[name="IdStatusObservasi"]').change(function (e) {
        if ($(this).val() == '1') {
            $('#secFormDiterima').fadeIn();
        } else {
            $('#secFormDiterima').hide();
        }
    })
})

function DataEditor(Id = null) {
    $('div#data').fadeIn();
    $('div#no-data').hide();

    $('div#EditorForm').fadeIn();
    $('div#ListData').hide();
    if (Id != null) {
        GetById(Id);
    } else {
        $("#FormEditor")[0].reset();
        $('input[name="IdPpdbBiaya"]').val('');

    }
}

function DataList() {
    $('#TabelData tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelData = $('#TabelData').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/api/ppdbs/GetPpdbDaftarList?proses=Tidak Lolos Observasi",
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null || (json.Data).length == 0) {
                    $('div#empty-data').fadeIn();
                    $('div#loading-data').hide();
                    return json;
                } else {
                    $('div#no-data').hide();
                    $('div#data').fadeIn();
                    return json.Data;
                }
            }
        },
        "columns": [{
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamAktivasiVa = "DataEditor('" + full.IdPpdbDaftar + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamAktivasiVa + '"><i class="fa fa-rocket"></i> Proses</button>';
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    if (full.StatusObservasi == -1)
                        data = '<div class="badge badge-danger">Tidak Observasi</div>';
                    if (full.StatusObservasi == 1)
                        data = '<div class="badge badge-success">Terima</div>';
                    if (full.StatusObservasi == 2)
                        data = '<div class="badge badge-danger">Tidak Diterima</div>';
                    if (full.StatusObservasi == 3)
                        data = '<div class="badge badge-warning">Cadangan</div>';
                    return data;
                }
            },
            {
                "data": "IdPpdbDaftar"
            },
            {
                "data": "Unit"
            },
            {
                "data": "JenisPendaftaran"
            },
            {
                "data": "JalurPendaftaran"
            },
            {
                "data": "KategoriPendaftaran"
            },
            {
                "data": "Kelas"
            },
            {
                "data": "Nama"
            },
            {
                "render": function (data, type, full, meta) {

                    return indoDate(full.TanggalLahir);
                }
            },
            {
                "data": "NamaIbu"
            },
            {
                "data": "NoHandphoneOrtu"
            },
            {
                "render": function (data, type, full, meta) {

                    return indoDate(full.TanggalDaftar);
                }
            },
        ],
        "bDestroy": true
    });
    TabelData.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function DataInputObservasi() {
    var fd = new FormData($('#FormProses')[0]);
    var formObj = $('#FormProses').serializeObject();

    fd.append('Data.IdPpdbDaftar', $('#IdPpdbDaftar').val());
    fd.append('Data.IdStatusObservasi', formObj.IdStatusObservasi);
    fd.append('Data.CatatanObservasi', formObj.CatatanObservasi);
    fd.append('Data.CatatanWawancara', formObj.CatatanWawancara);
    $("select[name='IdJenisBiayaPpdb[]'] option:selected").each(function (i, item) {
        fd.append('Data.Biayas[' + i + '].IdJenisBiayaPpdb', $(item).val());
    });
    $.ajax({
        url: base_url + "/api/Ppdbs/InputObservasi",
        type: "POST",
        dataType: "JSON",
        processData: false,
        contentType: false,
        data: fd,
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            $("#TabelData").DataTable().ajax.reload();
            $('div#EditorForm').hide();
            $('div#ListData').fadeIn();
            $('#ModalProses').modal('hide');
            if (res.IsSuccess) {
                swal({
                    title: 'Sukses',
                    text: "Anda berhasil input observasi",
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
            } else if (!res.IsSuccess) {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (err, a, e) {
            ProgressBar("success");
            $('div#EditorForm').hide();
            $('div#ListData').fadeIn();
            $('#ModalProses').modal('hide');
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(err),
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function DataSetTidakObservasi() {
    var IdPpdbDaftar = $('#IdPpdbDaftar').val();
    var Catatan = $('textarea#CatatanObservasiTidakDatang').val();
    $.ajax({
        url: base_url + "/api/Ppdbs/SetTidakObservasi?IdPpdbDaftar=" + IdPpdbDaftar + "&Catatan=" + Catatan,
        type: "GET",
        dataType: "json",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            $("#TabelData").DataTable().ajax.reload();
            $('div#EditorForm').hide();
            $('div#ListData').fadeIn();
            $('#ModalProses').modal('hide');
            if (res.IsSuccess) {
                swal({
                    title: 'Sukses',
                    text: "Anda berhasil perbarui status menjadi tidak observasi",
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
            } else if (!res.IsSuccess) {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (err, a, e) {
            ProgressBar("success");
            $('div#EditorForm').hide();
            $('div#ListData').fadeIn();
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(err),
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function DataProsesModal(Val) {
    if (Val == 'datang') {
        $('#divFormDatang').show();
        $('#divFormTidakDatang').hide();
        $('button#BtnProses').attr('onclick', 'DataInputObservasi()');
    } else if (Val == 'tidakdatang') {
        $('#divFormDatang').hide();
        $('#divFormTidakDatang').show();
        $('button#BtnProses').attr('onclick', 'DataSetTidakObservasi()');
    }
    $('#ModalProses').modal('show');
}

function GetById(Id) {
    scrollTop();
    $.ajax({
        url: base_url + '/api/ppdbs/GetPpdbDaftar?IdPpdbDaftar=' + Id,
        dataType: 'JSON',
        type: 'GET',
        success: function (res) {
            if (res.IsSuccess) {
                const {
                    IdPpdbDaftar,
                    IdJenisKategoriPendaftaran,
                    IdJenisPendaftaran,
                    IdJalurPendaftaran,
                    Agama,
                    AlamatOrtu,
                    AlamatTinggal,
                    Email,
                    EmailOrtu,
                    IdAgama,
                    IdJenisPekerjaanAyah,
                    IdJenisPekerjaanIbu,
                    IdJenisPrestasi,
                    IdKelas,
                    IdKelasParalel,
                    IdSiswa,
                    IdUnit,
                    JenisKelamin,
                    JenisPekerjaanAyah,
                    JenisPekerjaanIbu,
                    KdJenisKelamin,
                    Kelas,
                    KelasParalel,
                    Nama,
                    NamaAyah,
                    NamaIbu,
                    NamaInstansiAyah,
                    NamaInstansiIbu,
                    NamaPanggilan,
                    NikSiswa,
                    NikAyah,
                    NikIbu,
                    Nisn,
                    NoDarurat,
                    NoHandphoneOrtu,
                    Status,
                    StrStatus,
                    TanggalLahir,
                    TempatLahir,
                    Unit,
                    IdStatusObservasi,
                } = res.Data;
                GetKategoriDaftars(IdJenisKategoriPendaftaran)
                GetJenisPendaftarans(IdJenisPendaftaran)
                GetJalurPendaftarans(IdJalurPendaftaran)
                ComboGetUnit(function (obj) {
                    $("select[name='IdUnit']").html(obj);
                }, IdUnit);

                ComboGetKelas(function (obj) {
                    $("select[name='IdKelas']").html(obj);
                }, IdUnit, IdKelas);

                ComboGetJenisKelamin(function (obj) {
                    $("select[name='KdJenisKelamin']").html(obj);
                }, KdJenisKelamin);

                ComboGetJenisPekerjaan(function (obj) {
                    $("select[name='IdJenisPekerjaanIbu']").html(obj);
                }, IdJenisPekerjaanIbu);

                ComboGetJenisPekerjaan(function (obj) {
                    $("select[name='IdJenisPekerjaanAyah']").html(obj);
                }, IdJenisPekerjaanAyah);

                $('input#IdPpdbDaftar').val(IdPpdbDaftar)
                $('input#NikSiswa').val(NikSiswa)
                $('input#Nama').val(Nama)
                $('input#NamaPanggilan').val(NamaPanggilan)
                $('input#KdJenisKelamin').val(KdJenisKelamin)
                $('input#AlamatTinggal').val(AlamatTinggal)
                $('select#IdAgama').val(IdAgama)
                $('input#TempatLahir').val(TempatLahir)
                $('input#TanggalLahir').val(TanggalLahir)
                $('input#NikIbu').val(NikIbu)
                $('input#NamaIbu').val(NamaIbu)
                $('select#IdJenisPekerjaanIbu').val(IdJenisPekerjaanIbu)
                $('input#NamaInstansiIbu').val(NamaInstansiIbu)
                $('input#NikAyah').val(NikAyah)
                $('input#NamaAyah').val(NamaAyah)
                $('select#IdJenisPekerjaanAyah').val(IdJenisPekerjaanAyah)
                $('input#NamaInstansiAyah').val(NamaInstansiAyah)
                $('input#AlamatOrtu').val(AlamatOrtu)
                $('input#NoHandphoneOrtu').val(NoHandphoneOrtu)
                $('input#EmailOrtu').val(EmailOrtu)

                var AlertStatusObservasi = "";
                if (IdStatusObservasi == -1) {
                    AlertStatusObservasi +=
                        '<div class="alert alert-danger alert-has-icon">' +
                        '    <div class="alert-icon"><i class="fa fa-times"></i></div>' +
                        '    <div class="alert-body">' +
                        '        <div class="alert-title">Tidak Observasi</div>' +
                        '        Siswa/Siswi tidak datang saat observasi.' +
                        '    </div>' +
                        '</div>';
                } else if (IdStatusObservasi == 2) {
                    AlertStatusObservasi +=
                        '<div class="alert alert-danger alert-has-icon">' +
                        '    <div class="alert-icon"><i class="fa fa-user-times"></i></div>' +
                        '    <div class="alert-body">' +
                        '        <div class="alert-title">Tidak Diterima</div>' +
                        '        Siswa/Siswi tidak diterima saat input observasi.' +
                        '    </div>' +
                        '</div>';
                } else if (IdStatusObservasi == 3) {
                    AlertStatusObservasi +=
                        '<div class="alert alert-warning alert-has-icon">' +
                        '    <div class="alert-icon"><i class="fa fa-history"></i></div>' +
                        '    <div class="alert-body">' +
                        '        <div class="alert-title">Cadangan</div>' +
                        '        Siswa/Siswi saat ini masuk dalam status Cadangan.' +
                        '    </div>' +
                        '</div>';
                }
                $('div#alertStatusObservasi').html(AlertStatusObservasi)

                console.log(res)
            } else if (!res) {

            }
        },
        error: function (err, a, e) {

        }
    })
}

function GetJenisBiayaPpdbs() {
    $.ajax({
        url: base_url + '/api/ppdbs/GetJenisBiayaPpdbs',
        type: 'GET',
        dataType: 'JSON',
        success: function (res) {
            let html = "";
            $.each(res.Data, function (i, v) {
                html += "<option value='" + v.Id + "'>" + v.Nama + "</option>";
            });
            $('select[name="IdJenisBiayaPpdb[]"]').append(html);
        },
        error: function (err, a, e) {
            iziToast.error({
                title: 'Terjadi Kesalahan',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    })
}

function GetKategoriDaftars(Id) {
    $.getJSON(base_url + "/api/ppdbs/GetKategoriDaftars", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.Id == Id)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                else
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('select[name="IdJenisKategoriPendaftaran"]').html(html);
    });
}

function GetJenisPendaftarans(Id) {
    $.getJSON(base_url + "/api/ppdbs/GetJenisPendaftarans", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.Id == Id)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                else
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('select[name="IdJenisPendaftaran"]').html(html);
    });
}

function GetJalurPendaftarans(Id) {
    $.getJSON(base_url + "/api/ppdbs/GetJalurPendaftarans", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.Id == Id)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                else
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('select[name="IdJalurPendaftaran"]').html(html);
    });
}

function Kembali() {
    $('div#EditorForm').hide();
    $('div#ListData').fadeIn();
}