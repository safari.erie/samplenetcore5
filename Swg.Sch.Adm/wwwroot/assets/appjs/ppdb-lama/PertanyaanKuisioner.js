﻿/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 12082020
================================================================================================================= */

//////// START SECTION TABEL Pertanyaan Kuisioner ///////////
$('#TabelPertanyaanKuisioner tfoot th').each(function () {
    var title = $(this).text();
    $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
});
var TabelPertanyaanKuisioner = $('#TabelPertanyaanKuisioner').DataTable({
    "paging": false,
    "searching": true,
    "ordering": true,
    "info": true,
    "pageLength": 5,
    "lengthChange": true,
    "scrollX": true,
    "scrollY": "500px",
    "scrollCollapse": true,
    "processing": true,
    "ajax": {
        "url": base_url + "/api/Ppdbs/GetPertanyaanKuisioners",
        "method": 'GET',
        "beforeSend": function (xhr) {},
        "dataSrc": function (json) {
            if (json.Data == null) {
                iziToast.error({
                    title: 'Gagal Menampilkan Data Tabel Pertanyaan Kuisioner',
                    message: json.ReturnMessage,
                    position: 'topRight'
                });
                return json;
            } else {
                iziToast.success({
                    title: 'Berhasil Menampilkan Data Tabel Pertanyaan Kuisioner',
                    message: json.ReturnMessage,
                    position: 'topRight'
                });
                return json.Data;
            }
        }
    },
    "columns": [{
            "data": "JenisPertanyaan"
        },
        {
            "data": "Pertanyaan"
        },
    ],
    "bDestroy": true
});
TabelPertanyaanKuisioner.columns().every(function () {
    var that = this;

    $('input', this.footer()).on('keyup change clear', function () {
        if (that.search() !== this.value) {
            that
                .search(this.value)
                .draw();
        }
    });
});
$(".dataTables_filter").css("display", "none");
$(".dataTables_length").css("display", "none");
//////// START SECTION TABEL PERTANYAAN KUISIONER ///////////



function SaveData() {
    var form = new FormData($('#FormPertanyaanKuisioner')[0]);
    var Url = base_url + "/api/Ppdbs/UploadKuisioner";

    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        data: form,
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $('#FormPertanyaanKuisioner')[0].reset();
                TabelPertanyaanKuisioner.ajax.reload();
                swal({
                    title: 'Berhasil Menyimpan Pertanyaan Kuisioner',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal Menyimpan Pertanyaan Kuisioner',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}