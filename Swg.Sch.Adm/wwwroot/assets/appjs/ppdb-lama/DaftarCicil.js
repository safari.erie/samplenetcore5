﻿$(document).ready(function () {
    DataList()
    $('[data-toggle="datepicker"]').mask('00-00-0000');
})

function DataEditor(Id = null) {
    $('div#data').fadeIn();
    $('div#no-data').hide();

    $('div#EditorForm').fadeIn();
    $('div#ListData').hide();
    if (Id != null) {
        GetById(Id);
    } else {
        $("#FormEditor")[0].reset();
        $('input[name="IdPpdbBiaya"]').val('');

    }
}

function DataList() {
    $('#TabelData tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelData = $('#TabelData').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/api/ppdbs/GetPpdbDaftarList?proses=Daftar Cicil",
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null || (json.Data).length == 0) {
                    $('div#empty-data').fadeIn();
                    $('div#loading-data').hide();
                    return json;
                } else {
                    $('div#no-data').hide();
                    $('div#data').fadeIn();
                    return json.Data;
                }
            }
        },
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamProses = "DataEditor('" + full.IdPpdbDaftar + "')";
                    var ParamBack = "ModalKembalikan('" + full.IdPpdbDaftar + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamProses + '"><i class="fa fa-rocket"></i> Proses</button> &nbsp;' +
                        '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamBack + '"><i class="fa fa-truck"></i> Kembalikan</button>';
                    return Data;
                }
            },
            { "data": "IdPpdbDaftar" },
            { "data": "Unit" },
            { "data": "JenisPendaftaran" },
            { "data": "JalurPendaftaran" },
            { "data": "KategoriPendaftaran" },
            { "data": "Kelas" },
            { "data": "Nama" },
            {
                "render": function (data, type, full, meta) {

                    return indoDate(full.TanggalLahir);
                }
            },
            { "data": "NamaIbu" },
            { "data": "NoHandphoneOrtu" },
            {
                "render": function (data, type, full, meta) {

                    return indoDate(full.TanggalDaftar);
                }
            },
        ],
        "bDestroy": true
    });
    TabelData.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function DataSetCicilKePendidikan(IdPpdbDaftar, CilKe, RpCilKe) {
    var Catatan = $('textarea#Catatan').val();
    var TanggalBayar = $('input#TanggalBayar').val();
    $.ajax({
        url: base_url + '/api/ppdbs/SetCicilKePendidikan?IdPpdbDaftar=' + IdPpdbDaftar + '&CilKe=' + CilKe + '&RpCilKe=' + RpCilKe + '&TanggalBayar=' + TanggalBayar + '&Catatan=' + Catatan,
        type: 'GET',
        dataType: 'JSON',
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            $("#TabelData").DataTable().ajax.reload();
            $("#TabelDataCicil").DataTable().ajax.reload();
            $('#ModalCatatan').modal('hide');
            if (res.IsSuccess) {
                swal({ title: 'Sukses', text: "Anda berhasil memperbarui status cicilan menjadi lunas", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (!res.IsSuccess) {
                swal({ title: 'Gagal', text: res.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        },
        error: function (err, a, e) {
            ProgressBar("success");
            $('div#EditorForm').hide();
            $('div#ListData').fadeIn();
            $('#ModalProses').hide();
            swal({ title: 'Terjadi Kesalahan', text: JSON.stringify(err), confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    })
}

function DataProses(IdPpdbDaftar, CilKe, RpCilKe) {
    ModalProses(2)
    var Param = "DataSetCicilKePendidikan('" + IdPpdbDaftar + "','" + CilKe + "','" + RpCilKe + "')";
    $('#BtnProses').attr('onclick', Param)
}

function DataKembalikan() {
    var IdPpdbDaftar = $('input#IdPpdbDaftar', '#FormKembalikan').val();
    var IdProses = $('select#IdProses', '#FormKembalikan').val();
    $.ajax({
        url: base_url + '/api/ppdbs/backtostatus?idppdbdaftar=' + IdPpdbDaftar + '&ToStatus=' + IdProses,
        type: 'GET',
        dataType: 'JSON',
        beforeSend: function () {
            ProgressBar('wait');
        },
        success: function (res) {
            ProgressBar('success');
            $('#ModalKembalikan').modal('hide');
            $("#TabelData").DataTable().ajax.reload();
            if (res.IsSuccess) {
                swal({ title: 'Sukses', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (!res.IsSuccess) {
                swal({ title: 'Gagal', text: res.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        },
        error: function (err, a, e) {
            ProgressBar("success");
            $('#ModalKembalikan').modal('hide');
            swal({ title: 'Terjadi Kesalahan', text: JSON.stringify(err), confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    })
}

function ModalKembalikan(Id) {
    $('#ModalKembalikan').modal('show');
    GetById(Id)
    GetStatusPpdbs()
}

function GetStatusPpdbs() {
    $.getJSON(base_url + "/api/ppdbs/GetStatusPpdbs", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        }
        else console.log(res)
        $('select[name="IdProses"]').html(html);
    });
}

function GetJenisBiayaObservasi(Id) {
    $('#TabelDataCicil').DataTable({
        "paging": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/api/ppdbs/getcicilan?idppdbdaftar="+Id,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null || (json.Data).length == 0) {
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [
            { "data": "CilKe" },
            {
                "render": function (data, type, full, meta) {

                    return formatRupiah(full.RpCilKe);
                }
            },
            { "data": "Status" },
            {
                "render": function (data, type, full, meta) {
                    var Param = "DataProses('" + Id + "','" + full.CilKe + "','" + full.RpCilKe + "')";
                    var data = full.Status == "Lunas" ? '<i class="fa fa-check"></i>' : '<button type="button" class="btn btn-primary btn-sm" onclick="' + Param + '">Bayar</button>';
                    return data;
                }
            },
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            //console.log(aData.JenisData);
            $(nRow).find('td:eq(0)').css('font-weight', 'bold');
            $(nRow).find('td:eq(1)').css('font-weight', 'bold');
            $(nRow).find('td:eq(2)').css('font-weight', 'bold');
            $(nRow).find('td:eq(3)').css('font-weight', 'bold');
            if (aData.Status == "Lunas") {
                //cell background color
                $(nRow).find('td:eq(0)').css('background-color', '#d4ffd4');
                $(nRow).find('td:eq(0)').css('color', '#5e5f61');
                $(nRow).find('td:eq(1)').css('background-color', '#d4ffd4');
                $(nRow).find('td:eq(1)').css('color', '#5e5f61');
                $(nRow).find('td:eq(2)').css('background-color', '#d4ffd4');
                $(nRow).find('td:eq(2)').css('color', '#5e5f61');
                $(nRow).find('td:eq(3)').css('background-color', '#d4ffd4');
                $(nRow).find('td:eq(3)').css('color', '#5e5f61');
            } else if (aData.Status == "Tidak Lunas") {
                //cell background color
                $(nRow).find('td:eq(0)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(0)').css('color', '#5e5f61');
                $(nRow).find('td:eq(1)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(1)').css('color', '#5e5f61');
                $(nRow).find('td:eq(2)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(2)').css('color', '#5e5f61');
                $(nRow).find('td:eq(3)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(3)').css('color', '#5e5f61');
            }
        },
        "bDestroy": true
    });
    //$.ajax({
    //    url: base_url + '/api/ppdbs/getcicilan?idppdbdaftar=' + Id,
    //    type: 'GET',
    //    dataType: 'JSON',
    //    success: function (res) {
    //        if (res.IsSuccess) {
    //            let html = '<tr>' +
    //                '    <th>No</th>' +
    //                '    <th>Nama</th>' +
    //                '    <th>Jumlah</th>' +
    //                '    <th>Status</th>' +
    //                '    <th>#</th>' +
    //                '</tr>';
    //            $.each(res.Data, function (i, v) {
    //                var stylingTR = v.Status == "Lunas" ? "style=background-color:#d4ffd4" : "style=background-color:#ffceced4";
    //                var Param = "DataProses('" + Id + "','" + v.CilKe + "','" + v.RpCilKe + "')";
    //                var stylingBtn = v.Status == "Lunas" ? '<i class="fa fa-check"></i>' : '<button type="button" class="btn btn-primary btn-sm" onclick="' + Param + '">Bayar</button>';
    //                html +=
    //                    '<tr ' + stylingTR + '>' +
    //                    '    <td style="color: #5e5f61; font-weight: 700;">' + (i + 1) + '</td>' +
    //                    '    <td style="color: #5e5f61; font-weight: 700;">Cicilan Ke - '+v.CilKe+'</td>' +
    //                    '    <td style="color: #5e5f61; font-weight: 700;">' + formatRupiah(v.RpCilKe) + '</td>' +
    //                    '    <td style="color: #5e5f61; font-weight: 700;">' + v.Status + '</td>' +
    //                    '    <td>'+stylingBtn+'</td>' +
    //                    '</tr>';
    //            });
    //            $('#tableJenisBiaya').html(html);
    //        } else if (!res.IsSuccess) {

    //        }
    //    },
    //    error: function (err, a, e) {

    //    }
    //})
}

function GetById(Id) {
    scrollTop()
    $.ajax({
        url: base_url + '/api/ppdbs/GetPpdbDaftar?IdPpdbDaftar=' + Id,
        dataType: 'JSON',
        type: 'GET',
        success: function (res) {
            if (res.IsSuccess) {
                const {
                    IdPpdbDaftar,
                    IdJenisKategoriPendaftaran,
                    IdJenisPendaftaran,
                    IdJalurPendaftaran,
                    Agama,
                    AlamatOrtu,
                    AlamatTinggal,
                    Email,
                    EmailOrtu,
                    IdAgama,
                    IdJenisPekerjaanAyah,
                    IdJenisPekerjaanIbu,
                    IdJenisPrestasi,
                    IdKelas,
                    IdKelasParalel,
                    IdSiswa,
                    IdUnit,
                    JenisKelamin,
                    JenisPekerjaanAyah,
                    JenisPekerjaanIbu,
                    KdJenisKelamin,
                    Kelas,
                    KelasParalel,
                    Nama,
                    NamaAyah,
                    NamaIbu,
                    NamaInstansiAyah,
                    NamaInstansiIbu,
                    NamaPanggilan,
                    NikSiswa,
                    NikAyah,
                    NikIbu,
                    Nisn,
                    NoDarurat,
                    NoHandphoneOrtu,
                    Status,
                    StrStatus,
                    TanggalLahir,
                    TempatLahir,
                    Unit,
                } = res.Data;
                GetKategoriDaftars(IdJenisKategoriPendaftaran)
                GetJenisPendaftarans(IdJenisPendaftaran)
                GetJalurPendaftarans(IdJalurPendaftaran)
                ComboGetUnit(function (obj) {
                    $("select[name='IdUnit']").html(obj);
                }, IdUnit);

                ComboGetKelas(function (obj) {
                    $("select[name='IdKelas']").html(obj);
                }, IdUnit, IdKelas);

                ComboGetJenisKelamin(function (obj) {
                    $("select[name='KdJenisKelamin']").html(obj);
                }, KdJenisKelamin);

                ComboGetJenisPekerjaan(function (obj) {
                    $("select[name='IdJenisPekerjaanIbu']").html(obj);
                }, IdJenisPekerjaanIbu);

                ComboGetJenisPekerjaan(function (obj) {
                    $("select[name='IdJenisPekerjaanAyah']").html(obj);
                }, IdJenisPekerjaanAyah);

                $('input#IdPpdbDaftar').val(IdPpdbDaftar)
                $('input#NikSiswa').val(NikSiswa)
                $('input#Nama').val(Nama)
                $('input#NamaPanggilan').val(NamaPanggilan)
                $('input#KdJenisKelamin').val(KdJenisKelamin)
                $('input#AlamatTinggal').val(AlamatTinggal)
                $('select#IdAgama').val(IdAgama)
                $('input#TempatLahir').val(TempatLahir)
                $('input#TanggalLahir').val(TanggalLahir)
                $('input#NikIbu').val(NikIbu)
                $('input#NamaIbu').val(NamaIbu)
                $('select#IdJenisPekerjaanIbu').val(IdJenisPekerjaanIbu)
                $('input#NamaInstansiIbu').val(NamaInstansiIbu)
                $('input#NikAyah').val(NikAyah)
                $('input#NamaAyah').val(NamaAyah)
                $('select#IdJenisPekerjaanAyah').val(IdJenisPekerjaanAyah)
                $('input#NamaInstansiAyah').val(NamaInstansiAyah)
                $('input#AlamatOrtu').val(AlamatOrtu)
                $('input#NoHandphoneOrtu').val(NoHandphoneOrtu)
                $('input#EmailOrtu').val(EmailOrtu)
                GetJenisBiayaObservasi(IdPpdbDaftar)

                $('input#IdPpdbDaftar', '#FormKembalikan').val(Id)
                $('input#NamaCalonSiswa', '#FormKembalikan').val(Nama)
                $('input#NamaProses', '#FormKembalikan').val(Status)
                $('input#Email', '#FormKembalikan').val(EmailOrtu)

                console.log(res)
            } else if (!res) {

            }
        },
        error: function (err, a, e) {

        }
    })
}

function GetKategoriDaftars(Id) {
    $.getJSON(base_url + "/api/ppdbs/GetKategoriDaftars", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.Id == Id)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                else
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        }
        else console.log(res)
        $('select[name="IdJenisKategoriPendaftaran"]').html(html);
    });
}

function GetJenisPendaftarans(Id) {
    $.getJSON(base_url + "/api/ppdbs/GetJenisPendaftarans", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.Id == Id)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                else
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        }
        else console.log(res)
        $('select[name="IdJenisPendaftaran"]').html(html);
    });
}

function GetJalurPendaftarans(Id) {
    $.getJSON(base_url + "/api/ppdbs/GetJalurPendaftarans", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.Id == Id)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                else
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        }
        else console.log(res)
        $('select[name="IdJalurPendaftaran"]').html(html);
    });
}

function Kembali() {
    $('div#EditorForm').hide();
    $('div#ListData').fadeIn();
}

function ModalProses(Proses) {
    if (Proses == 1) {
        $('#ModalProses').modal('show');
    } else if (Proses == 2) {
        $('#ModalCatatan').modal('show');
    }
}