﻿$(document).ready(function () {
    DataList()
    var i = 0;
    GetJenisPrestasis(i);
    GetTingkatPrestasiSiswas(i);
    GetTahuns(i);
    $("select[name='IdJalurPendaftaran']").change((e) => {
        let Id = e.target.value;
        if (Id == 1) {
            ModalPrestasis(true);
        } else {
            ModalPrestasis(false);
            $('#FormPrestasis')[0].reset();
        }
    })
})
var i = 0;

function DataEditor(Id = null) {
    $('div#data').fadeIn();
    $('div#no-data').hide();

    $('div#EditorForm').fadeIn();
    $('div#ListData').hide();
    if (Id != null) {
        GetById(Id);
    } else {
        $("#FormEditor")[0].reset();
        $('input[name="IdPpdbBiaya"]').val('');

    }
}

function DataList() {
    $('#TabelData tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelData = $('#TabelData').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/api/ppdbs/GetPpdbDaftarList?proses=Semua",
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null || (json.Data).length == 0) {
                    $('div#empty-data').fadeIn();
                    $('div#loading-data').hide();
                    $('#htmlInfoError').html(json.ReturnMessage);
                    return json;
                } else {
                    $('div#no-data').hide();
                    $('div#data').fadeIn();
                    return json.Data;
                }
            }
        },
        "columns": [{
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamAktivasiVa = "DataEditor('" + full.IdPpdbDaftar + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamAktivasiVa + '"><i class="fa fa-pencil"></i> Edit</button> &nbsp;' +
                        '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamAktivasiVa + '"><i class="fa fa-history"></i> Hapus</button>';
                    return Data;
                }
            },
            {
                "data": "IdPpdbDaftar"
            },
            {
                "data": "Unit"
            },
            {
                "data": "JenisPendaftaran"
            },
            {
                "data": "JalurPendaftaran"
            },
            {
                "data": "KategoriPendaftaran"
            },
            {
                "data": "Kelas"
            },
            {
                "data": "Nama"
            },
            {
                "render": function (data, type, full, meta) {

                    return indoDate(full.TanggalLahir);
                }
            },
            {
                "data": "NamaIbu"
            },
            {
                "data": "NoHandphoneOrtu"
            },
            {
                "render": function (data, type, full, meta) {

                    return indoDate(full.TanggalDaftar);
                }
            },
        ],
        "bDestroy": true
    });
    TabelData.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function DataEditRegister() {
    var fd = new FormData($('#FormEditor')[0]);

    var getIdPpdbDaftarPrestasi = document.getElementsByName('IdPpdbDaftarPrestasi[]');
    var getIdJenisPrestasi = document.getElementsByName('IdJenisPrestasi[]');
    var getIdJenisTingkatPrestasi = document.getElementsByName('IdJenisTingkatPrestasi[]');
    var getTahun = document.getElementsByName('Tahun[]');
    var getNamaPenyelenggara = document.getElementsByName('NamaPenyelenggara[]');
    var getKeterangan = document.getElementsByName('Keterangan[]');
    if ($('select[name="IdJenisPrestasi[]"]').val() != 0) {
        for (var i = 0; i < getIdJenisPrestasi.length; i++) {
            fd.append('Prestasis[' + i + '].' + 'IdPpdbDaftarPrestasi', getIdPpdbDaftarPrestasi[i].value);
            fd.append('Prestasis[' + i + '].' + 'IdJenisPrestasi', getIdJenisPrestasi[i].value);
            fd.append('Prestasis[' + i + '].' + 'IdJenisTingkatPrestasi', getIdJenisTingkatPrestasi[i].value);
            fd.append('Prestasis[' + i + '].' + 'Tahun', getTahun[i].value);
            fd.append('Prestasis[' + i + '].' + 'NamaPenyelenggara', getNamaPenyelenggara[i].value);
            fd.append('Prestasis[' + i + '].' + 'Keterangan', getKeterangan[i].value);
        }
    }


    fd.append('FilePrestasi', $('input[type="file"][name="FilePrestasi"]')[0].files[0]);

    var settings = {
        "url": base_url + "/api/ppdbs/editregistrasi",
        "method": "POST",
        "timeout": 0,
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": fd,
        "dataType": "JSON",
        "beforeSend": (xhr) => {
            ProgressBar("wait");
        }
    };

    $.ajax(settings).done(function (res) {
        $("#TabelData").DataTable().ajax.reload();
        ProgressBar("success");
        if (res.IsSuccess) {
            // GetById($('input#IdPpdbDaftar').val());
            swal({
                title: 'Sukses',
                text: "Anda berhasil perbarui data",
                confirmButtonClass: 'btn-success text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'success'
            });
        } else if (!res.IsSuccess) {
            swal({
                title: 'Gagal',
                text: res.ReturnMessage,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function DataPrestasis() {
    // var TypeInput = true;
    // $('[data-input="wajib-prestasis"]').each(function () {
    //     if ($(this).val() == "") {
    //         TypeInput = false;
    //         $('#FormPrestasis').addClass('was-validated');
    //     }
    // })
    // if (TypeInput) {
    //     $('#modalPrestasis').modal('hide');
    // }
    $('#modalPrestasis').modal('hide');
}

function DataPrestasisReset() {
    $('div#formPrestasis').empty();
}

function GetById(Id) {
    $.ajax({
        url: base_url + '/api/ppdbs/GetPpdbDaftar?IdPpdbDaftar=' + Id,
        dataType: 'JSON',
        type: 'GET',
        success: function (res) {
            if (res.IsSuccess) {
                const {
                    IdPpdbDaftar,
                    IdJenisKategoriPendaftaran,
                    IdJenisPendaftaran,
                    IdJalurPendaftaran,
                    Agama,
                    AsalSekolah,
                    AlamatOrtu,
                    AlamatTinggal,
                    Email,
                    EmailOrtu,
                    IdAgama,
                    IdJenisPekerjaanAyah,
                    IdJenisPekerjaanIbu,
                    IdJenisPrestasi,
                    IdKelas,
                    IdKelasParalel,
                    IdSiswa,
                    IdUnit,
                    JenisKelamin,
                    JenisPekerjaanAyah,
                    JenisPekerjaanIbu,
                    JalurPendaftaran,
                    JenisKategoriPendaftaran,
                    JenisPendaftaran,
                    KdJenisKelamin,
                    Kelas,
                    KelasParalel,
                    Nama,
                    NamaAyah,
                    NamaIbu,
                    NamaInstansiAyah,
                    NamaInstansiIbu,
                    NamaPanggilan,
                    NikSiswa,
                    NikAyah,
                    NikIbu,
                    Nisn,
                    NoDarurat,
                    NoHandphoneOrtu,
                    Status,
                    StrStatus,
                    TanggalLahir,
                    TempatLahir,
                    Unit,
                    Prestasis,
                    Foto
                } = res.Data;
                GetKategoriDaftars(IdJenisKategoriPendaftaran)
                GetJenisPendaftarans(IdJenisPendaftaran)
                GetJalurPendaftarans(IdJalurPendaftaran)
                ComboGetUnit(function (obj) {
                    $("select[name='IdUnit']").html(obj);
                }, IdUnit);

                ComboGetKelas(function (obj) {
                    $("select[name='IdKelas']").html(obj);
                }, IdUnit, IdKelas);

                ComboGetJenisKelamin(function (obj) {
                    $("select[name='KdJenisKelamin']").html(obj);
                }, KdJenisKelamin);

                ComboGetJenisPekerjaan(function (obj) {
                    $("select[name='IdJenisPekerjaanIbu']").html(obj);
                }, IdJenisPekerjaanIbu);

                ComboGetJenisPekerjaan(function (obj) {
                    $("select[name='IdJenisPekerjaanAyah']").html(obj);
                }, IdJenisPekerjaanAyah);

                $('input#IdPpdbDaftar').val(IdPpdbDaftar)
                $('input#NikSiswa').val(NikSiswa)
                $('input#Nama').val(Nama)
                $('input#NamaPanggilan').val(NamaPanggilan)
                $('input#KdJenisKelamin').val(KdJenisKelamin)
                $('input#AlamatTinggal').val(AlamatTinggal)
                $('select#IdAgama').val(IdAgama)
                $('input#TempatLahir').val(TempatLahir)
                $('input#TanggalLahir').val(TanggalLahir)
                $('input#AsalSekolah').val(AsalSekolah)
                $('input#NikIbu').val(NikIbu)
                $('input#NamaIbu').val(NamaIbu)
                $('select#IdJenisPekerjaanIbu').val(IdJenisPekerjaanIbu)
                $('input#NamaInstansiIbu').val(NamaInstansiIbu)
                $('input#NikAyah').val(NikAyah)
                $('input#NamaAyah').val(NamaAyah)
                $('select#IdJenisPekerjaanAyah').val(IdJenisPekerjaanAyah)
                $('input#NamaInstansiAyah').val(NamaInstansiAyah)
                $('input#AlamatOrtu').val(AlamatOrtu)
                $('input#NoHandphoneOrtu').val(NoHandphoneOrtu)
                $('input#EmailOrtu').val(EmailOrtu)

                //$('span#htmlImageSiswa').html('<img alt="image" src="' + base_url + '/FileSiswa/' + IdPpdbDaftar + '.jpeg" class="rounded-circle author-box-picture">')
                $('span#htmlNamaSiswa').html(Nama)
                $('span#htmlKategori').html(JenisKategoriPendaftaran)
                $('span#htmlJenjangPendidikan').html(Unit)
                $('span#htmlKelas').html(Kelas)
                $('span#htmlJenisDaftar').html(JenisPendaftaran)

                if (IdJalurPendaftaran == 1)
                    $('small#infoPrestasi').show();
                else
                    $('small#infoPrestasi').hide();

                $.each(Prestasis, (i, v) => {
                    i = i + 1;
                    CreateFormPrestasiss();
                    setTimeout((e) => {
                        GetJenisPrestasis(i, v.IdJenisPrestasi);
                        GetTingkatPrestasiSiswas(i, v.IdJenisTingkatPrestasi);
                        GetTahuns(i, v.Tahun);
                        $('#IdPpdbDaftarPrestasi' + i).val(v.IdPpdbDaftarPrestasi);
                        $('#NamaPenyelenggara' + i).val(v.NamaPenyelenggara);
                        $('#Keterangan' + i).val(v.Keterangan);
                    }, 1000)
                });

                $('#PasFoto').attr('src', base_url + "/Asset/Files/Ppdb/Berkas/" + Foto);
                $('#linkHeaderEditPasFoto').attr('href', base_url + "/Asset/Files/Ppdb/Berkas/" + Foto);

                if (Prestasis != null) {
                    $.each(Prestasis, (i, v) => {
                        $('#linkDownloadPrestasi').fadeIn();
                        $('#linkDownloadPrestasi').attr('href', base_url + "/FilePrestasi/" + v.FilePrestasi);
                        return i < 0;
                    });
                } else {
                    $('#linkDownloadPrestasi').hide();
                }

            } else if (!res) {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

function CreateFormPrestasis() {
    i++
    let html = "";
    html += '<hr /><div class="form-row">' +
        '    <div class="form-group col-md-4">' +
        '        <label for="">Prestasi</label>' +
        '        <select class="form-control" name="IdJenisPrestasi[' + i + ']" id="validationCustom01" data-input="wajib-prestasis" required>' +
        '        </select>' +
        '        <div class="invalid-feedback">' +
        '            Kolom Wajib diisi.' +
        '        </div>' +
        '    </div>' +
        '    <div class="form-group col-md-4">' +
        '        <label for="">Tingkat Prestasi</label>' +
        '        <select class="form-control" name="IdJenisTingkatPrestasi[' + i + ']" id="validationCustom02" data-input="wajib-prestasis" required>' +
        '            <option value="">Select One</option>' +
        '        </select>' +
        '        <div class="invalid-feedback">' +
        '            Kolom Wajib diisi.' +
        '        </div>' +
        '    </div>' +
        '    <div class="form-group col-md-4">' +
        '        <label for="">Tahun</label>' +
        '        <select class="form-control" name="Tahun[' + i + ']" id="validationCustom03" data-input="wajib-prestasis" required>' +
        '            <option value="">Select One</option>' +
        '        </select>' +
        '        <div class="invalid-feedback">' +
        '            Kolom Wajib diisi.' +
        '        </div>' +
        '    </div>' +
        '    <div class="form-group col-md-6">' +
        '        <label for="">Nama Penyelenggara</label>' +
        '        <input type="text" class="form-control" name="NamaPenyelenggara[]" placeholder="Ketik Nama Penyelenggara..." data-input="wajib-prestasis" required />' +
        '        <div class="invalid-feedback">' +
        '            Kolom Wajib diisi.' +
        '        </div>' +
        '    </div>' +
        '    <div class="form-group col-md-6">' +
        '        <label for="">Keterangan</label> (optional)' +
        '        <input type="text" class="form-control" name="Keterangan[]" placeholder="Ketik Keterangan..." />' +
        '    </div>' +
        '</div>';
    $('div#formPrestasis').append(html);
    GetJenisPrestasis(i);
    GetTingkatPrestasiSiswas(i);
    GetTahuns(i);
}

function ModalPrestasis(bool) {
    // $('#FormPrestasis').removeClass('was-validated');
    if (bool) {
        // $('small#infoPrestasi').fadeIn();
        $('#modalPrestasis').modal('show');
    } else if (!bool) {
        // $('small#infoPrestasi').fadeOut();
        $('#modalPrestasis').modal('hide');
    }
}

function ModalBatalkanPrestasis() {
    ModalPrestasis(false);
    // $('select#IdJalurPendaftaran').val('');
    // $('#FormPrestasis')[0].reset();
}

function GetJenisPrestasis(i, IdJenisPrestasi = 0) {
    $.getJSON(base_url + "/api/ppdbs/GetJenisPrestasis", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (IdJenisPrestasi == v.Id) {
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                } else {
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
                }
            });
        } else console.log(res)
        $('#IdJenisPrestasi' + i).html(html);
    });
}

function GetTingkatPrestasiSiswas(i, IdJenisTingkatPrestasi = 0) {
    $.getJSON(base_url + "/api/ppdbs/GetTingkatPrestasiSiswas", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (IdJenisTingkatPrestasi == v.Id) {
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                } else {
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
                }
            });
        } else console.log(res)
        $('#IdJenisTingkatPrestasi' + i).html(html);
        // $('select[name="IdJenisTingkatPrestasi[]"]').html(html);
    });
}

function GetTahuns(i, Tahun = 0) {
    var StartDate = (new Date).getFullYear() - 4;
    var EndDate = (new Date).getFullYear();
    var HtmlCombo = "";
    HtmlCombo += "<option value=''>Select One</option>";
    for (var ix = StartDate; ix <= EndDate; ix++) {
        if (Tahun == ix) {
            HtmlCombo += "<option value='" + ix + "' selected>" + ix + "</option>"; //jadikan selected
        } else {
            HtmlCombo += "<option value='" + ix + "'>" + ix + "</option>"; //jadikan selected
        }
    }
    $('#Tahun' + i).html(HtmlCombo)
    // $('select[name="Tahun[]"]').html(HtmlCombo)
}

function GetKategoriDaftars(Id) {
    $.getJSON(base_url + "/api/ppdbs/GetKategoriDaftars", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.Id == Id)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                else
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('select[name="IdJenisKategoriPendaftaran"]').html(html);
    });
}

function GetJenisPendaftarans(Id) {
    $.getJSON(base_url + "/api/ppdbs/GetJenisPendaftarans", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.Id == Id)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                else
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('select[name="IdJenisPendaftaran"]').html(html);
    });
}

function GetJalurPendaftarans(Id) {
    $.getJSON(base_url + "/api/ppdbs/GetJalurPendaftarans", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.Id == Id)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                else
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('select[name="IdJalurPendaftaran"]').html(html);
    });
}

function Kembali() {
    $('div#EditorForm').hide();
    $('div#ListData').fadeIn();
    $("#TabelData").DataTable().ajax.reload();
    $(".dataTables_scrollHeadInner, .table-bordered").css("width", "inherit");

    $.ajax({
        url: base_url + '/api/ppdbs/GetPpdbDaftar?IdPpdbDaftar=' + $('#IdPpdbDaftar').val(),
        dataType: 'JSON',
        type: 'GET',
        success: (res) => {
            $.each(res.Data.Prestasis, (i, v) => {
                i = i + 1;
                DeleteFormPrestasis(i)
            });
        }
    });
}

var xInputPrestasis = 1;

function CreateFormPrestasiss() {
    var HtmlData = "";
    HtmlData +=
        '<div class="form-row" id="InputPrestasis' + xInputPrestasis + '">' +
        '    <input type="hidden" class="form-control" name="IdPpdbDaftarPrestasi[]" id="IdPpdbDaftarPrestasi' + xInputPrestasis + '"/>' +
        '    <div class="form-group col-md-4">' +
        '        <label for=""><a href="javascript:void(0);" class="text-danger" onClick="DeleteFormPrestasis(' + xInputPrestasis + ');"><i class="fa fa-times"></i></a> Prestasi</label>' +
        '        <select class="form-control" name="IdJenisPrestasi[]" id="IdJenisPrestasi' + xInputPrestasis + '" data-input="wajib-prestasis" required>' +
        '        </select>' +
        '        <div class="invalid-feedback">' +
        '            Kolom Wajib diisi.' +
        '        </div>' +
        '    </div>' +
        '    <div class="form-group col-md-4">' +
        '        <label for="">Tingkat Prestasi</label>' +
        '        <select class="form-control" name="IdJenisTingkatPrestasi[]" id="IdJenisTingkatPrestasi' + xInputPrestasis + '" data-input="wajib-prestasis" required>' +
        '            <option value="">Select One</option>' +
        '        </select>' +
        '        <div class="invalid-feedback">' +
        '            Kolom Wajib diisi.' +
        '        </div>' +
        '    </div>' +
        '    <div class="form-group col-md-4">' +
        '        <label for="">Tahun</label>' +
        '        <select class="form-control" name="Tahun[]" id="Tahun' + xInputPrestasis + '" data-input="wajib-prestasis" required>' +
        '            <option value="">Select One</option>' +
        '        </select>' +
        '        <div class="invalid-feedback">' +
        '            Kolom Wajib diisi.' +
        '        </div>' +
        '    </div>' +
        '    <div class="form-group col-md-6">' +
        '        <label for="">Nama Penyelenggara</label>' +
        '        <input type="text" class="form-control" name="NamaPenyelenggara[]" id="NamaPenyelenggara' + xInputPrestasis + '" placeholder="Ketik Nama Penyelenggara..." data-input="wajib-prestasis" required />' +
        '        <div class="invalid-feedback">' +
        '            Kolom Wajib diisi.' +
        '        </div>' +
        '    </div>' +
        '    <div class="form-group col-md-6">' +
        '        <label for="">Keterangan</label> (optional)' +
        '        <input type="text" class="form-control" name="Keterangan[]" id="Keterangan' + xInputPrestasis + '" placeholder="Ketik Keterangan..." />' +
        '    </div>' +
        '    <div class="form-group col-md-12"><hr /></div>' +
        '</div>';
    $("#formPrestasis").append(HtmlData);
    GetJenisPrestasis(xInputPrestasis);
    GetTingkatPrestasiSiswas(xInputPrestasis);
    GetTahuns(xInputPrestasis);
    xInputPrestasis++;
    SetDatepicker();

}

function DeleteFormPrestasis(IdBtn) {
    $("#InputPrestasis" + IdBtn).remove();
    xInputPrestasis--;
}

function DownloadBerkasPrestasi() {
    var IdPpdbDaftar = $('#IdPpdbDaftar').val();
    var url = base_url + "/api/ppdbs/PrintBerkasPrestasi?idppdbdaftar=" + IdPpdbDaftar;
    window.open(url, '_blank');
}