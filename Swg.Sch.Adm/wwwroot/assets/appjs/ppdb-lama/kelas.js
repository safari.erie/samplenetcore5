﻿$(document).ready(function () {
    DataList();
    GetJenisPendaftarans();
    GetJalurPendaftarans();
    GetGelombangPpdbs();
    ComboGetUnitSklh(function (obj) {
        $("select[name='IdUnit']", "#FormFilter").html(obj);
        $("select[name='IdUnit']", "#FormEditor").html(obj);
    });

    $("select[name='IdUnit']", "#FormEditor").change(function (e) {
        const select = e.target;
        const text = select.selectedOptions[0].text;
        if (text == "SMAIT AT Taufiq") {
            $('#divIdKelasParalel').fadeIn();
        } else {
            $('#divIdKelasParalel').hide();
            $("select[name='IdKelasParalel']").val('');
        }

        ComboGetKelas(function (obj) {
            $("select[name='IdKelas']", "#FormEditor").html(obj);
        }, e.target.value);


    });

    $("select[name='IdKelas']", "#FormEditor").change((e) => {
        var getUnit = $("select[name='IdUnit'] option:selected", "#FormEditor").text();
        if (getUnit == "SMAIT AT Taufiq") {
            ComboGetKelasParalel(function (obj) {
                $("select[name='IdKelasParalel']", "#FormEditor").html(obj);
            }, e.target.value);
        }

    })


    $("select[name='IdUnit']", "#FormFilter").change(function (e) {
        ComboGetKelas(function (obj) {
            $("select[name='IdKelas']", "#FormFilter").html(obj);
        }, e.target.value);
    });
})

function SearchUnit() {
    var IdKelas = $('select#IdKelas', '#FormFilter').val();
    DataList(IdKelas);
}

function DataEditor(Id = null) {
    $('div#data').fadeIn();
    $('div#no-data').hide();

    $('div#EditorForm').fadeIn();
    $('div#ListData').hide();
    if (Id != null) {
        GetById(Id);
    } else {
        $("#FormEditor")[0].reset();
    }
}

function DataList(IdKelas = 0) {
    $('#TabelData tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelData = $('#TabelData').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/api/ppdbs/GetPpdbSyarats?idkelas=" + IdKelas,
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    $('div#empty-data').fadeIn();
                    $('div#loading-data').hide();
                    return json;
                } else {
                    $('div#no-data').hide();
                    $('div#data').fadeIn();
                    return json.Data;
                }
            }
        },
        "columns": [{
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamEdit = "DataEditor('" + full.IdPpdbSyarat + "')";
                    var ParamHapus = "DataHapus('" + full.IdPpdbSyarat + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
                    Data += '&nbsp;&nbsp;&nbsp;';
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                    return Data;
                }
            },
            {
                "data": "Unit"
            },
            {
                "render": function (data, type, full, meta) {
                    var data = '';
                    if (full.KelasParalel != null) {
                        data += full.KelasParalel;
                    } else {
                        data += full.Kelas;
                    }

                    return data;
                }
            },
            {
                "data": "JenisPendaftaran"
            },
            {
                "data": "JenisGelombangPpdb"
            },
            {
                "data": "JalurPendaftaran"
            },
            {
                "render": function (data, type, full, meta) {
                    return indoDate(full.TanggalAwalDaftar);
                }
            },
            {
                "render": function (data, type, full, meta) {
                    return indoDate(full.TanggalAkhirDaftar);
                }
            },
        ],
        "bDestroy": true
    });
    TabelData.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function DataHapus(Id) {
    swal({
            title: "Apakah Anda Yakin ?",
            text: "Anda akan menghapus data ini",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: false
        },
        function () {
            var Url = base_url + "/api/ppdbs/PpdbSyaratDelete?IdPpdbSyarat=" + Id;
            $.ajax({
                url: Url,
                method: "get",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesave) {
                    ProgressBar("success");
                    if (responsesave.IsSuccess == true) {
                        $('#TabelData').DataTable().ajax.reload();
                        swal({
                            title: 'Sukses',
                            text: "Data berhasil dihapus",
                            confirmButtonClass: 'btn-success text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'success'
                        });
                    } else if (responsesave.IsSuccess == false) {
                        swal({
                            title: 'Gagal',
                            text: responsesave.ReturnMessage,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                },
                error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({
                        title: 'Terjadi Kesalahan',
                        text: JSON.stringify(responserror) + " : " + e,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            });
        });
}

function DataSave() {
    var formObj = $('#FormEditor').serializeObject();
    var fd = new FormData($('#FormEditor')[0]);
    if (formObj.IdPpdbSyarat != "") {
        var url = base_url + '/api/ppdbs/PpdbSyaratEdit';
    } else {
        var url = base_url + '/api/ppdbs/PpdbSyaratAdd';
    }
    $.ajax({
        "url": url,
        "method": "POST",
        "timeout": 0,
        "processData": false,
        "contentType": false,
        "data": fd,
        "dataType": "JSON",
        "success": function (res) {
            if (res.IsSuccess) {
                $('#TabelData').DataTable().ajax.reload();
                $('div#EditorForm').hide();
                $('div#ListData').fadeIn();

                iziToast.success({
                    title: 'Berhasil',
                    message: 'Data berhasil disimpan',
                    position: 'topRight'
                });
            } else if (!res.IsSuccess) {
                iziToast.error({
                    title: 'Gagal',
                    message: res.ReturnMessage,
                    position: 'topRight'
                });
            }
        },
        "error": function (err, a, e) {
            iziToast.error({
                title: 'Terjadi Kesalahan',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    })
}

function Kembali() {
    $('div#EditorForm').hide();
    $('div#ListData').fadeIn();
    $("#TabelData").DataTable().ajax.reload();
    $(".dataTables_scrollHeadInner, .table-bordered").css("width", "inherit");

}

function GetById(Id) {
    $.ajax({
        url: base_url + '/api/ppdbs/GetPpdbSyarat?IdPpdbSyarat=' + Id,
        type: 'GET',
        dataType: 'JSON',
        success: function (res) {
            if (res.IsSuccess) {
                $('input[name="IdPpdbSyarat"]').val(res.Data.IdPpdbSyarat);
                ComboGetUnit(function (obj) {
                    $("select[name='IdUnit']", "#FormEditor").html(obj);
                }, res.Data.IdUnit);
                ComboGetKelas(function (obj) {
                    $("select[name='IdKelas']", "#FormEditor").html(obj);
                }, res.Data.IdUnit, res.Data.IdKelas);
                $('#IdJenisPendaftaran').val(res.Data.IdJenisPendaftaran);
                $('#IdJalurPendaftaran').val(res.Data.IdJalurPendaftaran);
                $('#IdJenisGelombangPpdb').val(res.Data.IdJenisGelombangPpdb);
                $('input[name="TanggalAwalDaftar"]').val(res.Data.TanggalAwalDaftar);
                $('input[name="TanggalAkhirDaftar"]').val(res.Data.TanggalAkhirDaftar);
                $('input[name="TanggalAwalBayarDaftar"]').val(res.Data.TanggalAwalBayarDaftar);
                $('input[name="TanggalAkhirBayarDaftar"]').val(res.Data.TanggalAkhirBayarDaftar);
                $('input[name="TanggalAwalObservasi"]').val(res.Data.TanggalAwalObservasi);
                $('input[name="TanggalAkhirObservasi"]').val(res.Data.TanggalAkhirObservasi);
                $('input[name="TanggalAwalBayarPendidikan"]').val(res.Data.TanggalAwalBayarPendidikan);
                $('input[name="TanggalAkhirBayarPendidikan"]').val(res.Data.TanggalAkhirBayarPendidikan);
                $('input[name="MaxTanggalLahir"]').val(res.Data.MaxTanggalLahir);
                $('input[name="KuotaDaftarP"]').val(res.Data.KuotaDaftarP);
                $('input[name="KuotaDaftarL"]').val(res.Data.KuotaDaftarL);
                $('input[name="KuotaTerimaP"]').val(res.Data.KuotaTerimaP);
                $('input[name="KuotaTerimaL"]').val(res.Data.KuotaTerimaL);

                if (res.Data.Unit == "SMAIT AT Taufiq") {
                    ComboGetKelasParalel(function (obj) {
                        $("select[name='IdKelasParalel']", "#FormEditor").html(obj);
                    }, res.Data.IdKelas, res.Data.IdKelasParalel);
                    $('#divIdKelasParalel').fadeIn();
                } else {
                    $('#divIdKelasParalel').hide();
                    $("select[name='IdKelasParalel']").val('');
                }
            } else if (!res.IsSuccess) {
                iziToast.error({
                    title: 'Gagal',
                    message: res.ReturnMessage,
                    position: 'topRight'
                });
            }
        },
        error: function (err, a, e) {
            iziToast.error({
                title: 'Terjadi Kesalahan',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    })
}

function GetJenisPendaftarans() {
    $.ajax({
        url: base_url + '/api/ppdbs/GetJenisPendaftarans',
        type: 'GET',
        dataType: 'JSON',
        success: function (res) {
            let html = "";
            $.each(res.Data, function (i, v) {
                html += "<option value='" + v.Id + "'>" + v.Nama + "</option>";
            });
            $('#IdJenisPendaftaran').append(html);
        },
        error: function (err, a, e) {
            iziToast.error({
                title: 'Terjadi Kesalahan',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    })
}

function GetJalurPendaftarans() {
    $.ajax({
        url: base_url + '/api/ppdbs/GetJalurPendaftarans',
        type: 'GET',
        dataType: 'JSON',
        success: function (res) {
            let html = "";
            $.each(res.Data, function (i, v) {
                html += "<option value='" + v.Id + "'>" + v.Nama + "</option>";
            });
            $('#IdJalurPendaftaran').append(html);
        },
        error: function (err, a, e) {
            iziToast.error({
                title: 'Terjadi Kesalahan',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    })
}

function GetGelombangPpdbs() {
    $.ajax({
        url: base_url + '/api/ppdbs/GetGelombangPpdbs',
        type: 'GET',
        dataType: 'JSON',
        success: function (res) {
            let html = "";
            $.each(res.Data, function (i, v) {
                html += "<option value='" + v.Id + "'>" + v.Nama + "</option>";
            });
            $('#IdJenisGelombangPpdb').append(html);
        },
        error: function (err, a, e) {
            iziToast.error({
                title: 'Terjadi Kesalahan',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    })
}