﻿$(document).ready(function () {
    DataList();
})

function DataEditor(id = null) {
    $('div#data').fadeIn();
    $('div#no-data').hide();

    $('div#EditorForm').fadeIn();
    $('div#ListData').hide();
    if (id != null) {
        GetById(id);
    } else {
        $("#FormSekolahPpdbAturan")[0].reset();
        $('input#IdPpdbInfo').val('');
        $('textarea[name="Konten"]').summernote('code', '');
    }

}

function DataList() {
    $('#TabelSekolahPpdbAturan tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelSekolahPpdbAturan = $('#TabelSekolahPpdbAturan').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/api/ppdbs/GetPpdbInfos",
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    $('div#empty-data').fadeIn();
                    $('div#loading-data').hide();
                    return json;
                } else {
                    $('div#no-data').hide();
                    $('div#data').fadeIn();
                    return json.Data;
                }
            }
        },
        columnDefs: [{
                targets: [0],
                width: "auto",
                visible: true
            },
            {
                targets: [1],
                width: "auto",
                visible: true
            },
            {
                targets: [2],
                width: "auto",
                visible: true
            },
        ],
        "columns": [{
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamEdit = "DataEditor('" + full.IdPpdbInfo + "')";
                    var ParamHapus = "DataHapus('" + full.IdPpdbInfo + "','" + full.Judul + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
                    Data += '&nbsp;&nbsp;&nbsp;';
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                    return Data;
                }
            },
            {
                "data": "NamaTipe"
            },
            {
                "render": function (data, type, full, meta) {
                    if ((full.Judul).length >= 50) {
                        var potongText = (full.Judul).substr(0, 50);
                        var Data = potongText + "...";
                    } else {
                        var Data = full.Judul;
                    }
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = '<img alt="image" src="https://demo.getstisla.com/assets/img/avatar/avatar-5.png" class="rounded-circle" width="35" data-toggle="tooltip" title="Wildan Ahdian">';
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });
    TabelSekolahPpdbAturan.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function DataHapus(Id, Judul) {
    swal({
            title: "Apakah Anda Yakin ?",
            text: "Anda akan menghapus data " + Judul,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: false
        },
        function () {
            var Url = base_url + "/api/ppdbs/ppdbinfodelete?idppdbinfo=" + Id;
            $.ajax({
                url: Url,
                method: "get",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesave) {
                    ProgressBar("success");
                    if (responsesave.IsSuccess == true) {
                        $('#TabelSekolahPpdbAturan').DataTable().ajax.reload();
                        swal({
                            title: 'Berhasil Menghapus',
                            text: "",
                            confirmButtonClass: 'btn-success text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'success'
                        });
                    } else if (responsesave.IsSuccess == false) {
                        swal({
                            title: 'Gagal Menghapus',
                            text: responsesave.ReturnMessage,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                },
                error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({
                        title: 'Error :(',
                        text: JSON.stringify(responserror) + " : " + e,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            });
        });
}

function DataSave() {
    var CekIdPpdbInfo = $('input#IdPpdbInfo').val();

    var fd = new FormData($('#FormSekolahPpdbAturan')[0]);
    if (CekIdPpdbInfo != "") {
        fd.append('IdPpdbInfo', CekIdPpdbInfo);
        var url = base_url + '/api/ppdbs/ppdbinfoedit';
    } else {
        var url = base_url + '/api/ppdbs/ppdbinfoadd';
    }
    fd.append('IdPpdb', 1);
    fd.append('IdJenisPpdbInfo', $('select#IdJenisPpdbInfo').val());
    fd.append('Judul', $('input#Judul').val());
    fd.append('Konten', $('textarea#Konten').val());
    fd.append('FileImage', $('input[type="file"][name="FileImage"]')[0].files[0]);
    $.ajax({
        "url": url,
        "method": "POST",
        "timeout": 0,
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": fd,
        "dataType": "JSON",
        "success": function (res) {
            if (res.IsSuccess) {
                $('#TabelSekolahPpdbAturan').DataTable().ajax.reload();
                $('div#EditorForm').hide();
                $('div#ListData').fadeIn();

                iziToast.success({
                    title: 'Berhasil',
                    message: 'Data berhasil disimpan',
                    position: 'topRight'
                });
            } else if (!res.IsSuccess) {
                iziToast.error({
                    title: 'Gagal',
                    message: res.ReturnMessage,
                    position: 'topRight'
                });
            }
        },
        "error": function (err, a, e) {
            iziToast.error({
                title: 'Terjadi Kesalahan',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    })
}

function Kembali() {
    $('div#EditorForm').hide();
    $('div#ListData').fadeIn();
}

function GetById(id = null) {
    $.ajax({
        url: base_url + '/api/ppdbs/GetPpdbInfo?idppdbinfo=' + id,
        type: 'GET',
        success: function (res) {
            if (res.IsSuccess) {
                $('input[name="IdPpdbInfo"]').val(res.Data.IdPpdbInfo);
                $("#IdJenisPpdbInfo").val(res.Data.IdJenisPpdbInfo);
                $('input[name="Judul"]').val(res.Data.Judul);
                $('textarea[name="Konten"]').summernote('code', res.Data.Konten);
            } else if (!res.IsSuccess) {
                iziToast.error({
                    title: 'Gagal',
                    message: res.ReturnMessage,
                    position: 'topRight'
                });
            }
        },
        error: function (err, a, e) {
            iziToast.error({
                title: 'Terjadi Kesalahan',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    })
}