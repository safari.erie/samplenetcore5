﻿$(document).ready(function () {
    DataList()
})

function DataEditor(Id = null) {
    $('div#data').fadeIn();
    $('div#no-data').hide();

    $('div#EditorForm').fadeIn();
    $('div#ListData').hide();
    if (Id != null) {
        GetById(Id);
    } else {
        $("#FormEditor")[0].reset();
        $('input[name="IdPpdbBiaya"]').val('');

    }
}

function DataList() {
    $('#TabelData tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelData = $('#TabelData').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/api/ppdbs/GetPpdbDaftarList?proses=Verifikasi Observasi",
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null || (json.Data).length == 0) {
                    $('div#empty-data').fadeIn();
                    $('div#loading-data').hide();
                    return json;
                } else {
                    $('div#no-data').hide();
                    $('div#data').fadeIn();
                    return json.Data;
                }
            }
        },
        "columns": [{
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamAktivasiVa = "DataEditor('" + full.IdPpdbDaftar + "')";
                    var ParamBack = "ModalKembalikan('" + full.IdPpdbDaftar + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamAktivasiVa + '"><i class="fa fa-rocket"></i> Proses</button> &nbsp;';
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamBack + '"><i class="fa fa-truck"></i> Kembalikan</button>';
                    return Data;
                }
            },
            {
                "data": "IdPpdbDaftar"
            },
            {
                "data": "Unit"
            },
            {
                "data": "JenisPendaftaran"
            },
            {
                "data": "JalurPendaftaran"
            },
            {
                "data": "KategoriPendaftaran"
            },
            {
                "data": "Kelas"
            },
            {
                "data": "Nama"
            },
            {
                "render": function (data, type, full, meta) {

                    return indoDate(full.TanggalLahir);
                }
            },
            {
                "data": "NamaIbu"
            },
            {
                "data": "NoHandphoneOrtu"
            },
            {
                "render": function (data, type, full, meta) {

                    return indoDate(full.TanggalDaftar);
                }
            },
        ],
        "bDestroy": true
    });
    TabelData.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function DataVerifikasiHasilObservasi() {
    var IdPpdbDaftar = $("input[name='IdPpdbDaftar']").val();
    $.ajax({
        url: base_url + "/api/Ppdbs/VerifikasiHasilObservasi?IdPpdbDaftar=" + IdPpdbDaftar,
        type: "GET",
        dataType: "json",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            $("#TabelData").DataTable().ajax.reload();
            $('div#EditorForm').hide();
            $('div#ListData').fadeIn();
            $('#ModalProses').modal('hide');
            if (res.IsSuccess) {
                swal({
                    title: 'Sukses',
                    text: "Anda berhasil verifikasi observasi",
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
            } else if (!res.IsSuccess) {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (err, a, e) {
            ProgressBar("success");
            $('div#EditorForm').hide();
            $('div#ListData').fadeIn();
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(err),
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function DataKembalikan() {
    var IdPpdbDaftar = $('input#IdPpdbDaftar', '#FormKembalikan').val();
    var IdProses = $('select#IdProses', '#FormKembalikan').val();
    $.ajax({
        url: base_url + '/api/ppdbs/backtostatus?idppdbdaftar=' + IdPpdbDaftar + '&tostatus=' + IdProses,
        type: 'GET',
        dataType: 'JSON',
        beforeSend: function () {
            ProgressBar('wait');
        },
        success: function (res) {
            ProgressBar('success');
            $('#ModalKembalikan').modal('hide');
            $("#TabelData").DataTable().ajax.reload();
            if (res.IsSuccess) {
                swal({
                    title: 'Sukses',
                    text: "",
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
            } else if (!res.IsSuccess) {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (err, a, e) {
            ProgressBar("success");
            $('#ModalKembalikan').modal('hide');
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(err),
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

function ModalKembalikan(Id) {
    $('#ModalKembalikan').modal('show');
    GetById(Id)
}

function GetById(Id) {
    scrollTop()
    $.ajax({
        url: base_url + '/api/ppdbs/GetPpdbDaftar?IdPpdbDaftar=' + Id,
        dataType: 'JSON',
        type: 'GET',
        success: function (res) {
            if (res.IsSuccess) {
                const {
                    IdPpdbDaftar,
                    IdJenisKategoriPendaftaran,
                    IdJenisPendaftaran,
                    IdJalurPendaftaran,
                    Agama,
                    AlamatOrtu,
                    AlamatTinggal,
                    Email,
                    EmailOrtu,
                    IdAgama,
                    IdJenisPekerjaanAyah,
                    IdJenisPekerjaanIbu,
                    IdJenisPrestasi,
                    IdKelas,
                    IdKelasParalel,
                    IdSiswa,
                    IdUnit,
                    JenisKelamin,
                    JenisPekerjaanAyah,
                    JenisPekerjaanIbu,
                    KdJenisKelamin,
                    Kelas,
                    KelasParalel,
                    Nama,
                    NamaAyah,
                    NamaIbu,
                    NamaInstansiAyah,
                    NamaInstansiIbu,
                    NamaPanggilan,
                    NikSiswa,
                    NikAyah,
                    NikIbu,
                    Nisn,
                    NoDarurat,
                    NoHandphoneOrtu,
                    Status,
                    StrStatus,
                    TanggalLahir,
                    TempatLahir,
                    Unit,
                    IdStatusObservasi,
                } = res.Data;
                GetKategoriDaftars(IdJenisKategoriPendaftaran)
                GetJenisPendaftarans(IdJenisPendaftaran)
                GetJalurPendaftarans(IdJalurPendaftaran)
                ComboGetUnit(function (obj) {
                    $("select[name='IdUnit']").html(obj);
                }, IdUnit);

                ComboGetKelas(function (obj) {
                    $("select[name='IdKelas']").html(obj);
                }, IdUnit, IdKelas);

                ComboGetJenisKelamin(function (obj) {
                    $("select[name='KdJenisKelamin']").html(obj);
                }, KdJenisKelamin);

                ComboGetJenisPekerjaan(function (obj) {
                    $("select[name='IdJenisPekerjaanIbu']").html(obj);
                }, IdJenisPekerjaanIbu);

                ComboGetJenisPekerjaan(function (obj) {
                    $("select[name='IdJenisPekerjaanAyah']").html(obj);
                }, IdJenisPekerjaanAyah);

                $('input#IdPpdbDaftar').val(IdPpdbDaftar)
                $('input#NikSiswa').val(NikSiswa)
                $('input#Nama').val(Nama)
                $('input#NamaPanggilan').val(NamaPanggilan)
                $('input#KdJenisKelamin').val(KdJenisKelamin)
                $('input#AlamatTinggal').val(AlamatTinggal)
                $('select#IdAgama').val(IdAgama)
                $('input#TempatLahir').val(TempatLahir)
                $('input#TanggalLahir').val(TanggalLahir)
                $('input#NikIbu').val(NikIbu)
                $('input#NamaIbu').val(NamaIbu)
                $('select#IdJenisPekerjaanIbu').val(IdJenisPekerjaanIbu)
                $('input#NamaInstansiIbu').val(NamaInstansiIbu)
                $('input#NikAyah').val(NikAyah)
                $('input#NamaAyah').val(NamaAyah)
                $('select#IdJenisPekerjaanAyah').val(IdJenisPekerjaanAyah)
                $('input#NamaInstansiAyah').val(NamaInstansiAyah)
                $('input#AlamatOrtu').val(AlamatOrtu)
                $('input#NoHandphoneOrtu').val(NoHandphoneOrtu)
                $('input#EmailOrtu').val(EmailOrtu)
                let alertStatusObservasi = "";
                if (IdStatusObservasi == 1) {
                    alertStatusObservasi +=
                        '<div class="alert alert-success" role="alert">' +
                        '   <i class="fa fa-check"></i>  Calon Peserta Didik Berhasil Lolos Observasi' +
                        '</div>';
                } else if (IdStatusObservasi == 2) {
                    alertStatusObservasi +=
                        '<div class="alert alert-danger" role="alert">' +
                        '   <i class="fa fa-times"></i>  Calon Peserta Didik Tidak Diterima' +
                        '</div>';
                } else if (IdStatusObservasi == 3) {
                    alertStatusObservasi +=
                        '<div class="alert alert-warning" role="alert">' +
                        '    <i class="fa fa-clock-o"></i>  Calon Peserta Didik Masuk List Cadangan' +
                        '</div>';
                } else if (IdStatusObservasi == -1) {
                    alertStatusObservasi +=
                        '<div class="alert alert-danger" role="alert">' +
                        '     <i class="fa fa-times"></i>  Calon Peserta Didik Tidak Melakukan Observasi' +
                        '</div>';
                }
                $('div#alertStatusObservasi').html(alertStatusObservasi)
                $('#tableJenisBiaya').empty();
                GetJenisBiayaObservasi(IdPpdbDaftar)

                $('input#IdPpdbDaftar', '#FormKembalikan').val(Id)
                $('input#NamaCalonSiswa', '#FormKembalikan').val(Nama)
                $('input#NamaProses', '#FormKembalikan').val(Status)
                $('input#Email', '#FormKembalikan').val(EmailOrtu)

                console.log(res)
            } else if (!res) {

            }
        },
        error: function (err, a, e) {

        }
    })
}

function GetJenisBiayaObservasi(Id) {
    $.ajax({
        url: base_url + '/api/ppdbs/GetJenisBiayaObservasi?IdPpdbDaftar=' + Id,
        type: 'GET',
        dataType: 'JSON',
        success: function (res) {
            if (res.IsSuccess) {
                let html = '<tr>' +
                    '    <th>No</th>' +
                    '    <th>Nama</th>' +
                    '    <th>Jumlah</th>' +
                    '</tr>';
                $.each(res.Data, function (i, v) {
                    html +=
                        '<tr>' +
                        '    <td>' + (i + 1) + '</td>' +
                        '    <td>' + v.JenisBiayaPpdb + '</td>' +
                        '    <td>' + formatRupiah(v.Rp) + '</td>' +
                        '</tr>';
                });
                $('#tableJenisBiaya').append(html);
            } else if (!res.IsSuccess) {

            }
        },
        error: function (err, a, e) {

        }
    })
}

function GetKategoriDaftars(Id) {
    $.getJSON(base_url + "/api/ppdbs/GetKategoriDaftars", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.Id == Id)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                else
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('select[name="IdJenisKategoriPendaftaran"]').html(html);
    });
}

function GetJenisPendaftarans(Id) {
    $.getJSON(base_url + "/api/ppdbs/GetJenisPendaftarans", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.Id == Id)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                else
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('select[name="IdJenisPendaftaran"]').html(html);
    });
}

function GetJalurPendaftarans(Id) {
    $.getJSON(base_url + "/api/ppdbs/GetJalurPendaftarans", (res) => {
        let html = '<option value="">Select One</option>';
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.Id == Id)
                    html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
                else
                    html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            });
        } else console.log(res)
        $('select[name="IdJalurPendaftaran"]').html(html);
    });
}

function Kembali() {
    $('div#EditorForm').hide();
    $('div#ListData').fadeIn();
}