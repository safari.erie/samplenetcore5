﻿$(document).ready(function () {
    DataList();
    GetJenisRedaksi();
});

function DataEditor(id = null) {
    $('div#data').fadeIn();
    $('div#no-data').hide();

    $('div#EditorForm').fadeIn();
    $('div#ListData').hide();
    if (id != null) {
        GetById(id);
    } else {
        $("#FormEditor")[0].reset();
        $('input#IdPpdbRedaksiEmail').val('');
        $('textarea[name="MessageBody1"]').summernote('code', '');
        $('textarea[name="MessageBody2"]').summernote('code', '');
    }

}

function DataList() {
    $('#TabelData tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelData = $('#TabelData').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/api/ppdbs/GetPpdbRedaksiEmails?idppdb=1",
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    $('div#empty-data').fadeIn();
                    $('div#loading-data').hide();
                    return json;
                } else {
                    $('div#no-data').hide();
                    $('div#data').fadeIn();
                    return json.Data;
                }
            }
        },
        "columns": [{
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamEdit = "DataEditor('" + full.IdPpdbRedaksiEmail + "')";
                    var ParamHapus = "DataHapus('" + full.IdPpdbRedaksiEmail + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
                    Data += '&nbsp;&nbsp;&nbsp;';
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                    return Data;
                }
            },
            {
                "data": "JenisRedaksiEmailPpdb"
            },
            {
                "render": function (data, type, full, meta) {
                    if ((full.MessageBody1).length >= 50) {
                        var potongText = (full.MessageBody1).substr(0, 50);
                        var Data = potongText + "...";
                    } else {
                        var Data = full.MessageBody1;
                    }
                    return Data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    if ((full.MessageBody2).length >= 50) {
                        var potongText = (full.MessageBody2).substr(0, 50);
                        var Data = potongText + "...";
                    } else {
                        var Data = full.MessageBody2;
                    }
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });
    TabelData.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function DataHapus(Id) {
    swal({
            title: "Apakah Anda Yakin ?",
            text: "Anda akan menghapus data ini",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: false
        },
        function () {
            var Url = base_url + "/api/ppdbs/PpdbRedaksiEmailDelete?IdPpdbRedaksiEmail=" + Id;
            $.ajax({
                url: Url,
                method: "get",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesave) {
                    ProgressBar("success");
                    if (responsesave.IsSuccess == true) {
                        $('#TabelData').DataTable().ajax.reload();
                        swal({
                            title: 'Sukses',
                            text: "Data berhasil dihapus",
                            confirmButtonClass: 'btn-success text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'success'
                        });
                    } else if (responsesave.IsSuccess == false) {
                        swal({
                            title: 'Gagal',
                            text: responsesave.ReturnMessage,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                },
                error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({
                        title: 'Terjadi Kesalahan',
                        text: JSON.stringify(responserror) + " : " + e,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            });
        });
}

function DataSave() {
    var CekIdPpdbRedaksiEmail = $('input#IdPpdbRedaksiEmail').val();

    var fd = new FormData($('#FormEditor')[0]);
    if (CekIdPpdbRedaksiEmail != "") {
        var url = base_url + '/api/ppdbs/PpdbRedaksiEmailEdit';
    } else {
        var url = base_url + '/api/ppdbs/PpdbRedaksiEmailAdd';
    }
    $.ajax({
        "url": url,
        "method": "POST",
        "timeout": 0,
        "processData": false,
        "contentType": false,
        "data": fd,
        "dataType": "JSON",
        "success": function (res) {
            if (res.IsSuccess) {
                $('#TabelData').DataTable().ajax.reload();
                $('div#EditorForm').hide();
                $('div#ListData').fadeIn();

                iziToast.success({
                    title: 'Berhasil',
                    message: 'Data berhasil disimpan',
                    position: 'topRight'
                });
            } else if (!res.IsSuccess) {
                iziToast.error({
                    title: 'Gagal',
                    message: res.ReturnMessage,
                    position: 'topRight'
                });
            }
        },
        "error": function (err, a, e) {
            iziToast.error({
                title: 'Terjadi Kesalahan',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    })
}

function Kembali() {
    $('div#EditorForm').hide();
    $('div#ListData').fadeIn();
}

function GetById(id = null) {
    $.ajax({
        url: base_url + '/api/ppdbs/GetPpdbRedaksiEmail?IdPpdbRedaksiEmail=' + id,
        type: 'GET',
        success: function (res) {
            if (res.IsSuccess) {
                $('input[name="IdPpdbRedaksiEmail"]').val(res.Data.IdPpdbRedaksiEmail);
                $('input[name="IdPpdb"]').val(res.Data.IdPpdb);
                $("#IdJenisRedaksiEmailPpdb").val(res.Data.IdJenisRedaksiEmailPpdb);
                $('textarea[name="MessageBody1"]').summernote('code', res.Data.MessageBody1);
                $('textarea[name="MessageBody2"]').summernote('code', res.Data.MessageBody2);
            } else if (!res.IsSuccess) {
                iziToast.error({
                    title: 'Gagal',
                    message: res.ReturnMessage,
                    position: 'topRight'
                });
            }
        },
        error: function (err, a, e) {
            iziToast.error({
                title: 'Terjadi Kesalahan',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    })
}

function GetJenisRedaksi() {
    $.ajax({
        url: base_url + '/api/ppdbs/GetJenisRedaksiEmails',
        type: 'GET',
        dataType: 'JSON',
        success: function (res) {
            let htmlJenisRedaksi = "";
            $.each(res.Data, function (i, v) {
                htmlJenisRedaksi += "<option value='" + v.Id + "'>" + v.Nama + "</option>";
            });
            $('#IdJenisRedaksiEmailPpdb').append(htmlJenisRedaksi);
        },
        error: function (err, a, e) {
            iziToast.error({
                title: 'Terjadi Kesalahan',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    })
}