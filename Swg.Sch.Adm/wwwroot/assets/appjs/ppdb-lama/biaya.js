﻿$(document).ready(function () {
    DataList();
    GetJenisBiayaPpdbs();
    ComboGetUnitSklh(function (obj) {
        $("select[name='IdUnit']").html(obj);
    });
})

function DataEditor(IdPpdbBiaya = null) {
    $('div#data').fadeIn();
    $('div#no-data').hide();

    $('div#EditorForm').fadeIn();
    $('div#ListData').hide();
    if (IdPpdbBiaya != null) {
        GetById(IdPpdbBiaya);
    } else {
        $("#FormEditor")[0].reset();
        $('input[name="IdPpdbBiaya"]').val('');

    }
}

function DataList() {
    $('#TabelData tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelData = $('#TabelData').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/api/ppdbs/GetPpdbBiayas",
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    $('div#empty-data').fadeIn();
                    $('div#loading-data').hide();
                    return json;
                } else {
                    $('div#no-data').hide();
                    $('div#data').fadeIn();
                    return json.Data;
                }
            }
        },
        "columns": [{
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamEdit = "DataEditor('" + full.IdPpdbBiaya + "')";
                    var ParamHapus = "DataHapus('" + full.IdPpdbBiaya + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
                    Data += '&nbsp;&nbsp;&nbsp;';
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                    return Data;
                }
            },
            {
                "data": "Unit"
            },
            {
                "data": "JenisBiaya"
            },
            {
                "render": function (data, type, full, meta) {
                    var Data = formatRupiah(full.Rp);
                    return Data;
                }
            },
        ],
        "bDestroy": true
    });
    TabelData.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function DataHapus(IdPpdbBiaya) {
    swal({
            title: "Apakah Anda Yakin ?",
            text: "Anda akan menghapus data ini",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: false
        },
        function () {
            var Url = base_url + "/api/ppdbs/PpdbBiayadelete?IdPpdbBiaya=" + IdPpdbBiaya;
            $.ajax({
                url: Url,
                method: "get",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesave) {
                    ProgressBar("success");
                    if (responsesave.IsSuccess == true) {
                        $('#TabelData').DataTable().ajax.reload();
                        swal({
                            title: 'Sukses',
                            text: "Data berhasil dihapus",
                            confirmButtonClass: 'btn-success text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'success'
                        });
                    } else if (responsesave.IsSuccess == false) {
                        swal({
                            title: 'Gagal',
                            text: responsesave.ReturnMessage,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                },
                error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({
                        title: 'Terjadi Kesalahan',
                        text: JSON.stringify(responserror) + " : " + e,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            });
        });
}

function DataSave() {
    var formObj = $('#FormEditor').serializeObject();
    var fd = new FormData($('#FormEditor')[0]);
    if (formObj.IdPpdbBiaya != "") {
        var url = base_url + '/api/ppdbs/ppdbbiayaedit';
    } else {
        var url = base_url + '/api/ppdbs/ppdbbiayaadd';
    }
    $.ajax({
        "url": url,
        "method": "POST",
        "timeout": 0,
        "processData": false,
        "contentType": false,
        "data": fd,
        "dataType": "JSON",
        "success": function (res) {
            if (res.IsSuccess) {
                $('#TabelData').DataTable().ajax.reload();
                $('div#EditorForm').hide();
                $('div#ListData').fadeIn();

                iziToast.success({
                    title: 'Berhasil',
                    message: 'Data berhasil disimpan',
                    position: 'topRight'
                });
            } else if (!res.IsSuccess) {
                iziToast.error({
                    title: 'Gagal',
                    message: res.ReturnMessage,
                    position: 'topRight'
                });
            }
        },
        "error": function (err, a, e) {
            iziToast.error({
                title: 'Terjadi Kesalahan',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    })
}

function Kembali() {
    $('div#EditorForm').hide();
    $('div#ListData').fadeIn();
}

function GetById(IdPpdbBiaya) {
    $.ajax({
        url: base_url + '/api/ppdbs/GetPpdbBiaya?IdPpdbBiaya=' + IdPpdbBiaya,
        type: 'GET',
        dataType: 'JSON',
        success: function (res) {
            if (res.IsSuccess) {
                $('input[name="IdPpdbBiaya"]').val(res.Data.IdPpdbBiaya);
                $('select[name="IdUnit"]').val(res.Data.IdUnit);
                $('select[name="IdJenisBiaya"]').val(res.Data.IdJenisBiaya);
                $('input[name="Rp"]').val(res.Data.Rp);
            } else if (!res.IsSuccess) {
                iziToast.error({
                    title: 'Gagal',
                    message: res.ReturnMessage,
                    position: 'topRight'
                });
            }
        },
        error: function (err, a, e) {
            iziToast.error({
                title: 'Terjadi Kesalahan',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    })
}

function GetJenisBiayaPpdbs() {
    $.ajax({
        url: base_url + '/api/ppdbs/GetJenisBiayaPpdbs',
        type: 'GET',
        dataType: 'JSON',
        success: function (res) {
            let html = "";
            $.each(res.Data, function (i, v) {
                html += "<option value='" + v.Id + "'>" + v.Nama + "</option>";
            });
            $('#IdJenisBiaya').append(html);
        },
        error: function (err, a, e) {
            iziToast.error({
                title: 'Terjadi Kesalahan',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    })
}