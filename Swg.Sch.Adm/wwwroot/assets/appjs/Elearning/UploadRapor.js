﻿$(document).ready(function () {
    GetSiswaRapors();
});

const services = {
    UploadRapor: base_url + "/api/UploadRapors/UploadRapor",
    GetSiswaRapors: base_url + "/api/UploadRapors/GetSiswaRapors"
};

const GetSiswaRapors = () => {
    $("#TabelData tfoot th").each(function () {
        var title = $(this).text();
        $(this).html(
        '<input type="text" class="form-control" placeholder="CARI ' +
        title +
        '" />'
        );
    });
    var TabelData = $("#TabelData").DataTable({
        paging: true,
        searching: true,
        ordering: true,
        info: true,
        pageLength: 10,
        lengthChange: true,
        processing: true,
        ajax: {
        url: services.GetSiswaRapors,
        method: "GET",
        beforeSend: function (xhr) {},
        dataSrc: function (json) {
            if (json.Data == null) {
                iziToast.error({
                    title: 'Gagal',
                    message: json.ReturnMessage,
                    position: 'topRight'
                });
                $("div#empty-data").fadeIn();
                $("div#loading-data").hide();
                $("div#card-kelas-empty").fadeIn();
                $("div#card-kelas").hide();
                $("button#btnKelas").hide();
                $("span#htmlEmptyData").html(json.ReturnMessage);
                $("#ModalPencarian").modal("hide");
                return json;
            } else {
                $("div#card-kelas-empty").hide();
                $("div#no-data").hide();
                $("div#data").fadeIn();
                $("div#card-kelas").fadeIn();
                $("button#btnKelas").fadeIn();
                $("#ModalPencarian").modal("hide");
                return json.Data;
            }
        },
        },
        columns: [
            {
                render: function (data, type, full, meta) {
                    var data = "";
                    data = `<a href=${base_url}/Asset/Files/Siswa/Rapor/${full.FileRapor} class="btn btn-primary" target="_blank"><i class="fa fa-book"></i> Lihat </i>`;
                    return data;
                },
            },
            {
                data: "NamaSiswa",
            },
            {
                data: "JenisRapor",
            },
            {
                data: "CreatedBy",
            },
            {
                render: function (data, type, full, meta) {
                    var data = `<span class="badge badge-warning"><i class="fa fa-clock-o"></i> Menunggu Disetujui</span>`;
                    if (full.Status == "Aktif")
                        data = `<span class="badge badge-success"><i class="fa fa-check"></i> Berhasil Disetujui</span>`;
                    return data;
                },
            },
            {
                data: "FileRapor",
            },
        ],
        bDestroy: true,
    });
    TabelData.columns().every(function () {
        var that = this;

        $("input", this.footer()).on("keyup change clear", function () {
        if (that.search() !== this.value) {
            that.search(this.value).draw();
        }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

const UploadRaporSiswa = () => {
    var fd = new FormData($('#FormUploadRapor')[0]);
    var formObj = $('#FormUploadRapor').serializeObject();
    fd.append('FileRapor', $('input[type="file"][name="FileRapor"]')[0].files[0]);

    $.ajax({
        url: services.UploadRapor,
        method: 'POST',
        processData: false,
        contentType: false,
        data: fd,
        dataType: 'JSON',
        beforeSend: function () {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            var data = res.Data;
            if (res.IsSuccess) {
                swal({
                    title: 'Berhasil',
                    text: 'Selamat anda berhasil upload rapor siswa' ,
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
                GetSiswaRapors();
                $('#ModalUpload').modal('hide');
            } else if (!res.IsSuccess) {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}


function checkextension(selector) {
    var file = document.querySelector("#" + selector);
    if (/\.(zip)$/i.test(file.files[0].name) === false) {
        swal({ title: 'File Upload', text: "Pastikan file yang di upload harus zip", confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        $('#' + selector).val('');
    }
}

function ModalUpload() {
    $('#ModalUpload').modal('show');
}