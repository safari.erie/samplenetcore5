﻿/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 12082020
================================================================================================================= */

//////// START SECTION TABEL MAPEL ///////////
$('#TabelMapel tfoot th').each(function () {
    var title = $(this).text();
    $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
});
var TabelMapel = $('#TabelMapel').DataTable({
    "paging": false,
    "searching": true,
    "ordering": true,
    "info": true,
    "pageLength": 5,
    "lengthChange": true,
    "scrollX": true,
    "scrollY": "230px",
    "scrollCollapse": true,
    "processing": true,
    "ajax": {
        "url": base_url + "/api/Elearnings/GetMapels",
        "method": 'GET',
        "beforeSend": function (xhr) {
        },
        "dataSrc": function (json) {
            if (json.Data == null) {
                if (json.ReturnMessage != "data tidak ada") {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Mata Pelajaran',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                }
                return json;
            } else {
                return json.Data;
            }
        }
    },
    "columns": [
        {
            "render": function (data, type, full, meta) {
                var Data = "";
                var ParamEdit = "EditData('" + full.IdMapel + "')";
                var ParamHapus = "HapusData('" + full.IdMapel + "','" + cekQuotes(full.Nama) + "')";
                Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
                Data += '&nbsp;&nbsp;&nbsp;';
                Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                return Data;
            }
        },
        { "data": "Kode" },
        { "data": "Nama" },
    ],
    "bDestroy": true
});
TabelMapel.columns().every(function () {
    var that = this;

    $('input', this.footer()).on('keyup change clear', function () {
        if (that.search() !== this.value) {
            that
                .search(this.value)
                .draw();
        }
    });
});
$(".dataTables_filter").css("display", "none");
$(".dataTables_length").css("display", "none");
//////// START SECTION TABEL MATA PELAJARAN ///////////

function TambahData() {
    $('#FormMapel')[0].reset();
    $("#BtnSaveMapel").attr("onClick", "SaveData();");
    $(".modal-title").html("Tambah Mata Pelajaran");
    $("input[name='Kode']", "#FormMapel").removeAttr("readonly");
}

function EditData(IdMapel) {
    $('#FormMapel')[0].reset();
    $("#BtnSaveMapel").attr("onClick", "SaveData();");
    $(".modal-title").html("Edit Mata Pelajaran");
    var Url = base_url + "/api/Elearnings/GetMapel?IdMapel=" + IdMapel;
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var ResponseData = responsesuccess.Data;
                $("input[name='IdMapel']", "#FormMapel").val(ResponseData.IdMapel).attr("readonly", true);
                $("input[name='Kode']", "#FormMapel").val(ResponseData.Kode).attr("readonly",true);
                $("input[name='Nama']", "#FormMapel").val(ResponseData.Nama);
            } else {
                swal({ title: 'Gagal :(', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function SaveData() {
    var form = new FormData($('#FormMapel')[0]);
    var Url = base_url + "/api/Elearnings/MapelAddEdit";

    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        data: form,
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $('#FormMapel')[0].reset();
                TabelMapel.ajax.reload();
                swal({ title: 'Berhasil Menyimpan Mata Pelajaran', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal Menyimpan Mata Pelajaran', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function HapusData(IdMapel, Nama) {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus data Mata Pelajaran " + Nama,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            var Url = base_url + "/api/Elearnings/MapelDelete?IdMapel=" + IdMapel;
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesave) {
                    ProgressBar("success");
                    if (responsesave.IsSuccess == true) {
                        TabelMapel.ajax.reload();
                        swal({ title: 'Berhasil Menghapus Mata Pelajaran', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesave.IsSuccess == false) {
                        swal({ title: 'Gagal Menghapus Mata Pelajaran', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}
