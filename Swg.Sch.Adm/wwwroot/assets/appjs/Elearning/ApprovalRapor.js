$(document).ready(function () {
   
    ComboGetUnitSklh(function (obj) {
        $("select[name='IdUnit']").html(obj);
    });

    $("select[name='IdUnit']","#FormFilter").change(function (e) {
        ComboGetKelas(function (obj) {
            $("select[name='IdKelas']").html(obj);
        }, this.value);
    })

    $("select[name='IdKelas']", "#FormFilter").change(function (e) {
        ComboGetJenisRapors(function (obj) {
            $("select[name='IdJenisRapor']").html(obj);
        });
    })

    $("select[name='IdJenisRapor']", "#FormFilter").change(function (e) {
        $('#card-kelas').fadeIn();
        var IdKelas = $("select[name='IdKelas'] option:selected").val();
        GetApprovSiswaRapors(IdKelas, this.value);
    })

    $("input[name='CheckAllIdKelas']").change(function () {
        var status = this.checked;
        $("input[name='ProsesIdSiswa']").each(function () {
            this.checked = status;
        });
    });

    $("input[name='ProsesIdSiswa']").change(function () {
        if (this.checked == false) {
            $("input[name='CheckAllIdKelas']")[0].checked = false;
        }
        if ($("input[name='ProsesIdSiswa']:checked").length == $("input[name='IdKelas']").length) {
            $("input[name='CheckAllIdKelas']")[0].checked = true;
        }
    });
  
  
  });
  
  const Url = {
    GetApprovSiswaRapors: base_url + "/api/uploadrapors/GetApprovSiswaRapors",
    PublishRapors: base_url + "/api/uploadrapors/PublishRapors",
  };

const GetApprovSiswaRapors = (IdKelas, IdJenisRapor) => {
    $("#TabelKelas tfoot th").each(function () {
        var title = $(this).text();
        $(this).html(
        '<input type="text" class="form-control" placeholder="CARI ' +
        title +
        '" />'
        );
    });
    var TabelKelas = $("#TabelKelas").DataTable({
        paging: true,
        searching: true,
        ordering: true,
        info: true,
        pageLength: 10,
        lengthChange: true,
        processing: true,
        ajax: {
        url: Url.GetApprovSiswaRapors + "?IdKelas=" + IdKelas + "&IdJenisRapor=" + IdJenisRapor,
        method: "GET",
        beforeSend: function (xhr) {},
        dataSrc: function (json) {
            if (json.Data == null) {
            iziToast.error({
                title: 'Gagal',
                message: json.ReturnMessage,
                position: 'topRight'
            });
            $("div#empty-data").fadeIn();
            $("div#loading-data").hide();
            $("div#card-kelas-empty").fadeIn();
            $("div#card-kelas").hide();
            $("button#btnKelas").hide();
            $("span#htmlEmptyData").html(json.ReturnMessage);
            $("#ModalPencarian").modal("hide");
            return json;
            } else {
            $("div#card-kelas-empty").hide();
            $("div#no-data").hide();
            $("div#data").fadeIn();
            $("div#card-kelas").fadeIn();
            $("button#btnKelas").fadeIn();
            $("#ModalPencarian").modal("hide");
            return json.Data;
            }
        },
        },
        columns: [{
                "render": function (data, type, full, meta) {
                    var Value = full.IdSiswa;
                    var BtnLoop = "";
                    var Param = "'" + Value + "'";
                    var JumlahProses = (Value).length;
                    BtnLoop += '<input type="checkbox" name="ProsesIdSiswa" value="' + Value + '">&nbsp;&nbsp;';
                    for (var ijp = 0; ijp < JumlahProses; ijp++) {
                        var NamaProses = Value[ijp];
                        var paramtobereplaced = {
                            "&": "",
                            " ": ""
                        };
                        var NamaProsesJoin = NamaProses.replace(/&| /gi, function (matched) {
                            return paramtobereplaced[matched];
                        });
                        var NamaProsesFunction = "Btn" + NamaProsesJoin;
                        BtnLoop += '<button type="button" onClick="' + NamaProsesFunction + '(' + Param + ')" class="btn btn-info btn-sm"> ' + NamaProses + '</button>&nbsp;&nbsp;';
                    }
                    data = BtnLoop;
                    return data;
                }
        },
        {
            render: function (data, type, full, meta) {
                var data = "";
                data = `<a href=${base_url}/Asset/Files/Siswa/Rapor/${full.FileRapor} class="btn btn-primary" target="_blank"><i class="fa fa-book"></i> Lihat </i>`;
                return data;
            },
        },
            {
                data: "NamaSiswa",
            },
            {
                data: "JenisRapor",
            },
            {
                data: "CreatedBy",
            },
            {
                data: "Status",
            },
            {
                data: "FileRapor",
            },
            
        ],
        bDestroy: true,
    });
    TabelKelas.columns().every(function () {
        var that = this;

        $("input", this.footer()).on("keyup change clear", function () {
        if (that.search() !== this.value) {
            that.search(this.value).draw();
        }
        });
    });
}

const PublishRapors = () => {
    var IdJenisRapor = $('select[name^="IdJenisRapor"] option:selected').val();
    var IdSiswa = '';
    var CekValue = '';
    $("input[name='ProsesIdSiswa']:checked").each(function (i, item) {
        IdSiswa += "&IdSiswa=" + parseInt($(item).val());
        CekValue = parseInt($(item).val());
    });
    if (CekValue == "") {
        swal({
            title: 'Gagal',
            text: 'Silahkan pilih salah satu siswa !',
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
        return;
    }
    $.ajax({
        url: `${Url.PublishRapors}?IdJenisRapor=${IdJenisRapor}${IdSiswa}`,
        type: 'POST',
        beforeSend: function (xhr) {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            $("#TabelKelas").DataTable().ajax.reload();
            if (res.IsSuccess) {
                swal({
                    title: "Sukses",
                    text: "Selamat anda berhasil publish rapor kepada siswa",
                    confirmButtonClass: "btn-success text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "success",
                });
            } else {
                swal({
                    title: "Gagal",
                    text: res.ReturnMessage,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: "Terjadi Kesalahan",
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: "btn-danger text-white",
                confirmButtonText: "Oke, Mengerti",
                type: "error",
            });
        }
    })
}