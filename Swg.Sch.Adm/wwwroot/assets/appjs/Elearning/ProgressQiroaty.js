﻿const UrlService = {
    GetJilidQuran: base_url + '/api/elearnings/GetJilidQuran',
    GetKelasQuranSiswa: base_url + '/api/elearnings/GetKelasQuranSiswa',
    GetKelasQurans: base_url + '/api/elearnings/GetKelasQurans',
    GetKelasQuranSiswas: base_url + '/api/elearnings/GetKelasQuranSiswas',
    GetKelasQuranSiswaProgres: base_url + '/api/elearnings/GetKelasQuranSiswaProgres',
    GetKelasQuranSiswaUjian: base_url + '/api/elearnings/GetKelasQuranSiswaUjian',
    KelasQuranSiswaProgresAdd: base_url + '/api/elearnings/KelasQuranSiswaProgresAdd',
    KelasQuranSiswaUjianAdd: base_url + '/api/elearnings/KelasQuranSiswaUjianAdd',
}

function ModalCariByTanggal(JenisData) {
    $('#ModalTanggal').modal('show');
    if (JenisData == "KelasQuran") {
        $("button[id='BtnPencarianByTanggal']").attr("onClick", "PencarianByTanggal('KelasQuran')");
    }
}

function PencarianByTanggal(JenisData) {
    var Tanggal = $('#TanggalPencarian').val();
    if (JenisData == "KelasQuran") {
        GetTabelKelasQuran(Tanggal);
    } 
    $('#ModalTanggal').modal('hide');
}

GetTabelKelasQuran("");
function GetTabelKelasQuran(Tanggal) {
    $('#TabelKelasQuran tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelKelasQuran = $('#TabelKelasQuran').DataTable({
        "paging": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 100,
        "lengthChange": false,
        "processing": true,
        "ajax": {
            "url": UrlService.GetKelasQurans + "?Tanggal=" + Tanggal,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Kelas Quran',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    var Param = "'" + full.IdKelasQuran + "','" + full.Tanggal + "'";
                    data = '<button type="button" class="btn btn-primary btn-sm" onClick="GetTabelKelasQuranSiswa(' + Param + ');"><i class="fa fa-eye"></i> Pilih</button> ';
                    return data;
                }
            },
            { "data": "Hari" },
            { "data": "Tanggal" },
            { "data": "Kelompok" },
            { "data": "JumlahSiswa" },
            { "data": "JumlahInput" },
            { "data": "JumlahNoInput" },
            
        ],
        "bDestroy": true
    });
    TabelKelasQuran.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function GetTabelKelasQuranSiswa(IdKelasQuran, Tanggal) {
    sessionStorage.setItem("GetKelasQuranSiswas_IdKelasQuran", IdKelasQuran);
    sessionStorage.setItem("GetKelasQuranSiswas_Tanggal", Tanggal);
    $("#DivTabelKelasQuranSiswa").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#DivTabelKelasQuranSiswa").offset().top
    }, 200);
    $('#TabelKelasQuranSiswa tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelKelasQuranSiswa = $('#TabelKelasQuranSiswa').DataTable({
        "paging": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "pageLength": 100,
        "lengthChange": false,
        "processing": true,
        "scrollX": true,
        "scrollY": "450px",
        "scrollCollapse": true,
        "ajax": {
            "url": UrlService.GetKelasQuranSiswas + "?IdKelasQuran="+parseInt(IdKelasQuran)+"&Tanggal=" + Tanggal,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Kelas Quran Siswa',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    var data = "";
                    var Param = "'" + IdKelasQuran + "','" + Tanggal + "','" + remove_quotes(full.NamaSiswa) + "','" + full.IdSiswa + "','" + full.NamaKelasParalel + "'";
                    data += '<button type="button" class="btn btn-primary btn-sm" onClick="InputData(' + Param + ');"><i class="fa fa-pencil-square-o"></i> Input Data</button> ';
                    data += '<button type="button" class="btn btn-primary btn-sm" onClick="InputUjian(' + Param + ');"><i class="fa fa-pencil-square-o"></i> Input Ujian</button> ';
                    return data;
                }
            },
            { "data": "NamaSiswa" },
            { "data": "NamaKelasParalel" },
            { "data": "JilidQuran" },
            { "data": "Halaman" },
            { "data": "Catatan" },
            { "data": "StatusUjian" },

        ],
        "bDestroy": true
    });
    TabelKelasQuranSiswa.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}


function InputData(IdKelasQuran, Tanggal, NamaSiswa, IdSiswa, NamaKelasParalel) {
    var Url = UrlService.GetKelasQuranSiswaProgres + "?IdSiswa="+IdSiswa+"&IdKelasQuran=" + parseInt(IdKelasQuran) + "&Tanggal=" + Tanggal;
    console.log("Input Data "+Url);
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");

            $("#ModalFormKelasQuranSiswaProgress").modal("show");
            $("#ModalHeaderFormKelasQuranSiswaProgress").text("Form Progress Kelas Qur'an Siswa");

            $("input[name='IdSiswa']").val(IdSiswa);
            $("input[name='Tanggal']").val(Tanggal);
            $("input[name='IdKelasQuran']").val(IdKelasQuran);

            $("input[name='NamaSiswa']").val(NamaSiswa);
            $("input[name='NamaKelasParalel']").val(NamaKelasParalel);

            if(responsesuccess.IsSuccess == true){
                $("input[name='Halaman']").val(responsesuccess.Data.Halaman);
                $("textarea[name='Catatan']").val(responsesuccess.Data.Catatan);
                ComboGetJilidQuran(function (obj) {
                    $("select[name='IdJenisJilidQuran']").html(obj);
                }, responsesuccess.Data.IdJenisJilidQuran);
            } else {
                ComboGetJilidQuran(function (obj) {
                    $("select[name='IdJenisJilidQuran']").html(obj);
                });
            }
            
        }, error: function (errorresponse) {
            swal({ title: 'Gagal Menampilkan Form Progress Kelas Quran Siswa', text: errorresponse, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
    $("#BtnSaveInputData").attr("onClick", "SaveKelasQuranSiswaProgress();");
}

function InputUjian(IdKelasQuran, Tanggal, NamaSiswa, IdSiswa, NamaKelasParalel) {
    var Url = UrlService.GetKelasQuranSiswaUjian + "?IdSiswa="+IdSiswa+"&IdKelasQuran=" + parseInt(IdKelasQuran) + "&Tanggal=" + Tanggal;
    console.log("Input Ujian "+Url);
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            
            $("#ModalFormKelasQuranSiswaUjian").modal("show");
            $("#ModalHeaderFormKelasQuranSiswaUjian").text("Form Ujian Kelas Qur'an Siswa");

            $("input[name='IdSiswa']").val(IdSiswa);
            $("input[name='Tanggal']").val(Tanggal);
            $("input[name='IdKelasQuran']").val(IdKelasQuran);

            $("input[name='NamaSiswa']").val(NamaSiswa);
            $("input[name='NamaKelasParalel']").val(NamaKelasParalel);
            

            if(responsesuccess.IsSuccess == true){
                ComboGetJilidQuran(function (obj) {
                    $("select[name='IdJenisJilidQuran']").html(obj);
                }, responsesuccess.Data.IdJenisJilidQuran);
                $("textarea[name='Catatan']").val(responsesuccess.Data.Catatan);
                ComboGetHasil(function (obj) {
                    $("select[name='Hasil']").html(obj);
                }, responsesuccess.Data.Hasil);
                ComboGetNilai(function (obj) {
                    $("select[name='Nilai']").html(obj);
                }, responsesuccess.Data.Nilai);
            } else {
                ComboGetJilidQuran(function (obj) {
                    $("select[name='IdJenisJilidQuran']").html(obj);
                });
                ComboGetHasil(function (obj) {
                    $("select[name='Hasil']").html(obj);
                });
                ComboGetNilai(function (obj) {
                    $("select[name='Nilai']").html(obj);
                });
            }
            
        }, error: function (errorresponse) {
            swal({ title: 'Gagal Menampilkan Form Ujian Kelas Quran Siswa', text: errorresponse, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
    $("#BtnSaveInputUjian").attr("onClick", "SaveKelasQuranSiswaUjian();");
}

function SaveKelasQuranSiswaProgress() {
    var Url = UrlService.KelasQuranSiswaProgresAdd;
    var formData = new FormData($('#FormKelasQuranSiswaProgress')[0]);
    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        data: formData,
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $("#ModalFormKelasQuranSiswaProgress").modal("hide");
                $('#FormKelasQuranSiswaProgress')[0].reset();
                GetTabelKelasQuran(sessionStorage.getItem("GetKelasQuranSiswas_Tanggal"));
                GetTabelKelasQuranSiswa(sessionStorage.getItem("GetKelasQuranSiswas_IdKelasQuran"), sessionStorage.getItem("GetKelasQuranSiswas_Tanggal"));
                swal({ title: 'Berhasil', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function SaveKelasQuranSiswaUjian() {
    var Url = UrlService.KelasQuranSiswaUjianAdd;
    var formData = new FormData($('#FormKelasQuranSiswaUjian')[0]);
    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        data: formData,
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $("#ModalFormKelasQuranSiswaUjian").modal("hide");
                $('#FormKelasQuranSiswaUjian')[0].reset();
                GetTabelKelasQuran(sessionStorage.getItem("GetKelasQuranSiswas_Tanggal"));
                GetTabelKelasQuranSiswa(sessionStorage.getItem("GetKelasQuranSiswas_IdKelasQuran"), sessionStorage.getItem("GetKelasQuranSiswas_Tanggal"));
                swal({ title: 'Berhasil', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}