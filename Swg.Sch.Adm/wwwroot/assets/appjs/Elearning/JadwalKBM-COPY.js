﻿$(document).ready(function () {
    DataJadwalMapelGuru();
    $.fn.modal.Constructor.prototype._enforceFocus = function () {};

    // $.getJSON(base_url + '/api/elearnings/getunitpegawai', (res) => {
    //     if (res.IsSuccess) {
    //         ComboGetUnit_Disabled(function (obj) {
    //             $("select[name='UnitUnduh']").html(obj);
    //         }, res.Data.Unit[0].IdUnit);
    //         $("select[name='UnitUnduh']").val(res.Data.Unit[0].IdUnit).trigger('change');
    //         $("select[name='KelasUnduh']").fadeIn();
    //         ComboGetKelas(function (obj) {
    //             obj += '<option value="0"> Semua </option>';
    //             $("select[name='KelasUnduh']").html(obj);
    //         }, res.Data.Unit[0].IdUnit);
    //     } else {
    //         console.log('data unit pegawai tidak ada')
    //     }
    // })

    ComboGetUnit(function (obj) {
        $("select[name='IdUnit']").html(obj);
    });

    $("select[name='IdUnit']").change(function (e) {
        ComboGetKelas(function (obj) {
            $("select[name='IdKelas']").html(obj);
        }, this.value);
    })

    $("select[name='IdKelas']").change(function (e) {
        ComboGetKelasParalel(function (obj) {
            $("select[name='IdKelasParalel']").html(obj);
        }, this.value);
    })
});

const UrlService = {
    GetJadwalKbms: base_url + '/api/elearnings/GetJadwalKbms',
    GetJadwalKbm: base_url + '/api/elearnings/GetJadwalKbm',
    GetMapels: base_url + '/api/elearnings/GetMapels',
    UpdateJadwalKbm: base_url + '/api/elearnings/UpdateJadwalKbm',
    InputJadwalKbm: base_url + '/api/elearnings/InputJadwalKbm',
    DeleteJadwalKbm: base_url + '/api/elearnings/DeleteJadwalKbm',
    DownloadJadwalKbmUnit: base_url + '/api/download/DownloadJadwalKbmUnit',
    DownloadJadwalKbmKelas: base_url + '/api/download/DownloadJadwalKbmKelas',
}

function DataJadwalMapelGuru() {
    $("#TabelData tfoot th").each(function () {
        var title = $(this).text();
        $(this).html(
            '<input type="text" class="form-control" placeholder="CARI ' +
            title +
            '" />'
        );
    });
    var TabelData = $("#TabelData").DataTable({
        paging: true,
        searching: true,
        ordering: true,
        info: true,
        pageLength: 10,
        lengthChange: true,
        processing: true,
        ajax: {
            url: UrlService.GetJadwalKbms,
            method: "GET",
            beforeSend: function (xhr) {},
            dataSrc: function (json) {
                if (json.Data == null) {
                    $("div#empty-data").fadeIn();
                    $("div#loading-data").hide();
                    $("span#htmlEmptyData").html(json.ReturnMessage);
                    $("#ModalPencarian").modal("hide");
                    return json;
                } else {
                    $("div#no-data").hide();
                    $("div#data").fadeIn();
                    $("#ModalPencarian").modal("hide");
                    return json.Data;
                }
            },
        },
        columns: [{
                render: function (data, type, full, meta) {
                    var Data = "";
                    var ParamEdit = "";
                    var ParamHapus = "";
                    ParamEdit = "ModalJadwal('" + full.IdKbm + "','" + full.IdKbmMateri + "')";
                    ParamHapus = "DeleteJadwalKbm('" + full.IdKbm + "')";
                    Data += '<button type="button" class="btn btn-primary btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-cog"></i> Edit</button>&nbsp;&nbsp;';
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash-o"></i> Hapus</button>';
                    return Data;
                },
            },
            {
                data: "IdKbmMateri",
            },
            {
                render: function (data, type, full, meta) {
                    var onError = "this.onerror=null; this.src='" + base_url + "/assets/img/avatar/avatar-1.png'";
                    var data = "";
                    data += '<img alt="image" src="' + base_url + '/FotoProfile/' + full.FotoGuru + '" onerror="' + onError + '" class="rounded-circle" width="35" data-toggle="tooltip" style="height:35px;object-fit: cover;object-position: center;">';
                    return data;
                },
            },
            {
                data: "NamaGuru",
            },
            {
                data: "NamaKelasParalel",
            },
            {
                data: "NamaMapel",
            },
            {
                data: "Hari",
            },
            {
                render: function (data, type, full, meta) {
                    var data = "";
                    data += full.JamMulai + " s/d " + full.JamSelesai;
                    return data;
                },
            },
            {
                render: function (data, type, full, meta) {
                    var data = "";
                    data += indoDate(full.Tanggal);
                    return data;
                },
            },
        ],
        bDestroy: true,
    });
    TabelData.columns().every(function () {
        var that = this;

        $("input", this.footer()).on("keyup change clear", function () {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function ModalJadwal(IdKbm, IdKbmMateri) {
    $('#ModalInfo').modal('hide');
    $('#ModalJadwal').modal('show');
    $('#IdKbm', '#FormJadwal').val(IdKbm);
    if (IdKbm != 0) {
        $('#JadwalModalLabel').text('Perbarui Jadwal');
        $.ajax({
            url: UrlService.GetJadwalKbm + '?IdKbm=' + IdKbm + '&IdKbmMateri=' + IdKbmMateri,
            type: 'GET',
            dataType: 'JSON',
            beforeSend: function (xhr) {
                ProgressBar("wait");
            },
            success: function (res) {
                ProgressBar("success");
                if (res.IsSuccess) {
                    var data = res.Data;
                    $('#IdKbmJadwal', '#FormJadwal').val(data.IdKbmJadwal);

                    ComboGetUnit(function (obj) {
                        $("select[name='IdUnit']").html(obj);
                    }, data.IdUnit);

                    ComboGetKelas(function (obj) {
                        $("select[name='IdKelas']").html(obj);
                    }, data.IdUnit, data.IdKelas);

                    ComboGetKelasParalel(function (obj) {
                        $("select[name='IdKelasParalel']").html(obj);
                    }, data.IdKelas, data.IdKelasParalel);

                    GetMapels(data.IdMapel);

                    ComboGetListUser(function (obj) {
                        $('select#IdPegawai').html(obj);
                    }, data.IdPegawai);

                    $('#HariKe').val(data.HariKe);
                    $('#Tanggal').val(data.Tanggal);
                    $('#JamMulai').val(data.JamMulai);
                    $('#JamSelesai').val(data.JamSelesai);
                    $('#Periode').val(data.Periode);
                } else {

                }
            }
        })
    } else {
        $('#JadwalModalLabel').text('Tambah Jadwal');
        $('#FormJadwal')[0].reset();
        $('#IdKbmJadwal').val(0);
        GetMapels()
        ComboGetListUser(function (obj) {
            $('select#IdPegawai', '#FormJadwal').html(obj);
        });
    }
}

function ModalAddJadwal() {
    $('#ModalAddJadwal').modal('show');
    GetMapels()
    $('#FormAddJadwal')[0].reset();
    $("#IdUnit", "#FormAddJadwal").val("").trigger('change');
    $("#IdKelas", "#FormAddJadwal").select2("val", "");
    $("#IdKelasParalel", "#FormAddJadwal").select2("val", "");
    ComboGetListUser(function (obj) {
        $('select#IdPegawai', '#FormAddJadwal').html(obj);
    }, data.IdPegawai);
}

function ModalInfo() {
    $('#ModalInfo').modal('show');
}

function ModalUploadJadwal() {
    $('#ModalInfo').modal('hide');
    $('#ModalUploadJadwal').modal('show');
}

function GetMapels(Id) {
    $.ajax({
        url: UrlService.GetMapels,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                for (var i = 0; i < (responsesave.Data).length; i++) {
                    if (responsesave.Data[i].Id == Id) {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '" selected>' + responsesave.Data[i].Nama + '</option>';
                    } else {
                        html_combo += '<option value="' + responsesave.Data[i].Id + '">' + responsesave.Data[i].Nama + '</option>';
                    }
                }
                $('#IdMapel').html(html_combo);
                $('#IdMapel', '#FormAddJadwal').html(html_combo);
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function UpdateJadwalKbm() {
    var form = new FormData($('#FormJadwal')[0]);
    $.ajax({
        url: UrlService.UpdateJadwalKbm,
        type: 'POST',
        processData: false,
        contentType: false,
        data: form,
        dataType: 'JSON',
        beforeSend: function (xhr) {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            if (res.IsSuccess) {
                $('#ModalJadwal').modal('hide');
                swal({
                    title: 'Sukses',
                    text: 'Berhasil memperbarui data',
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
                $('#TabelData').DataTable().ajax.reload();
            } else {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

function InputJadwalKbm() {
    var form = new FormData($('#FormAddJadwal')[0]);
    $.ajax({
        url: UrlService.InputJadwalKbm,
        type: 'POST',
        processData: false,
        contentType: false,
        data: form,
        dataType: 'JSON',
        beforeSend: function (xhr) {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            if (res.IsSuccess) {
                $('#ModalAddJadwal').modal('hide');
                swal({
                    title: 'Sukses',
                    text: 'Berhasil menambah data',
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
                $('#FormAddJadwal')[0].reset();
                $('#TabelData').DataTable().ajax.reload();
            } else {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

function DeleteJadwalKbm(IdKbm) {
    swal({
            title: "Apakah Anda Yakin ?",
            text: "Anda akan hapus jadwal kbm ini",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                url: UrlService.DeleteJadwalKbm + "?IdKbm=" + IdKbm,
                method: "GET",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (res) {
                    ProgressBar("success");
                    if (res.IsSuccess == true) {
                        $('#TabelData').DataTable().ajax.reload();
                        swal({
                            title: 'Sukses',
                            text: "Berhasil Hapus Jadwal Kbm",
                            confirmButtonClass: 'btn-success text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'success'
                        });
                    } else if (res.IsSuccess == false) {
                        swal({
                            title: 'Gagal',
                            text: res.ReturnMessage,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                },
                error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({
                        title: 'Error :(',
                        text: JSON.stringify(responserror) + " : " + e,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            });
        });
}

function DownloadTemplate() {
    var IdUnit = $('#UnitUnduh option:selected').val();
    var IdKelas = $('#KelasUnduh option:selected').val();
    if (IdKelas == 0) {
        window.open(UrlService.DownloadJadwalKbmUnit + '?IdUnit=' + IdUnit, '_blank');
    } else {
        window.open(UrlService.DownloadJadwalKbmKelas + '?IdKelas=' + IdKelas, '_blank');
    }
}