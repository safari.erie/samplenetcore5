const UrlService = {
    UpdateKelompokQuran: base_url + '/api/elearnings/UpdateKelompokQuran',
    UpdateKelompokQuranSiswa: base_url + '/api/elearnings/UpdateKelompokQuranSiswa',
    GetKelompokQuran: base_url + '/api/elearnings/GetKelompokQuran',
    GetKelompokQuranSiswa: base_url + '/api/elearnings/GetKelompokQuranSiswa',
    UploadKelompokQiroaty: base_url + '/api/elearnings/UploadKelompokQiroaty',
}
//perbarui
function ComboHariKe(handleData, HariKe) {
    var HtmlData = "";
    var IdHari = 1;
    var NamaHariArray = ["Senin", "Selasa", "Rabu", "Kamis", "Jum'at"];
    for (var i = 0; i <= 4; i++) {
        if (IdHari == HariKe) {
            HtmlData += "<option value='" + IdHari + "' selected>" + NamaHariArray[i] + "</option>";
        } else {
            HtmlData += "<option value='" + IdHari + "'>" + NamaHariArray[i] + "</option>";
        }
        IdHari++;
    }
    return handleData(HtmlData);
}
$('#TabelKelasQuran tfoot th').each(function () {
    var title = $(this).text();
    $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
});
var TabelKelasQuran = $('#TabelKelasQuran').DataTable({
    "paging": true,
    "searching": true,
    "ordering": false,
    "info": true,
    "pageLength": 10,
    "lengthChange": false,
    "processing": true,
    "ajax": {
        "url": UrlService.GetKelompokQuran,
        "method": 'GET',
        "beforeSend": function (xhr) {
        },
        "dataSrc": function (json) {
            if (json.Data == null) {
                iziToast.error({
                    title: 'Gagal Menampilkan Data Tabel Kelas Quran',
                    message: json.ReturnMessage,
                    position: 'topRight'
                });
                return json;
            } else {
                return json.Data;
            }
        }
    },
    "columns": [
        {
            "render": function (data, type, full, meta) {
                var data = "";
                var Param = "'" + full.IdKelasQuran + "'";
                var ParamEdit = "'" + full.IdKelasQuran + "','" + full.Nama + "','" + full.NamaSingkat + "','" + full.IdPegawai + "','" + full.HariKe + "','" + full.JamMulai + "','" + full.JamSelesai + "'";
                data += '<button type="button" class="btn btn-info btn-sm" onClick="GetTabelKelasQuranSiswa(' + Param + ');"><i class="fa fa-eye"></i> Pilih</button> ';
                data += '&nbsp;&nbsp;';
                data += '<button type="button" class="btn btn-success btn-sm" onClick="EditKelompokQuran(' + ParamEdit + ');"><i class="fa fa-pencil-square-o"></i> Edit</button> ';
                return data;
            }
        },
        {
            "render": function (data, type, full, meta) {
                data = `<code>${full.NamaSingkat}</code>`
                return data;
            }
        },
        { "data": "Nama" },
        {
            "render": function (data, type, full, meta) {
                data = `${full.NamaGuru.toUpperCase()}`
                return data;
            }
        },
        { "data": "Hari" },
        {
            "render": function (data, type, full, meta) {
                data = full.JamMulai +" - "+full.JamSelesai;
                return data;
            }
        },
    ],
    "bDestroy": true
});
TabelKelasQuran.columns().every(function () {
    var that = this;

    $('input', this.footer()).on('keyup change clear', function () {
        if (that.search() !== this.value) {
            that
                .search(this.value)
                .draw();
        }
    });
});
$(".dataTables_filter").css("display", "none");
$(".dataTables_length").css("display", "none");

function GetTabelKelasQuranSiswa(IdKelasQuran) {
    $("#DivTabelKelasQuranSiswa").css("display", "block");
    $('html, body').animate({
        scrollTop: $("#DivTabelKelasQuranSiswa").offset().top
    }, 50);
    $('#TabelKelasQuranSiswa tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelKelasQuranSiswa = $('#TabelKelasQuranSiswa').DataTable({
        "paging": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "pageLength": 10,
        "lengthChange": false,
        "processing": true,
        "ajax": {
            "url": UrlService.GetKelompokQuranSiswa + "?IdKelasQuran=" + parseInt(IdKelasQuran),
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Kelas Quran Siswa',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    var data = "";
                    var Param = "'" + full.IdKelasQuran + "','" + full.IdSiswa + "'";
                    data += '<button type="button" class="btn btn-success btn-sm" onClick="EditKelompokQiroatySiswa(' + Param + ');"><i class="fa fa-pencil-square-o"></i> Edit</button> ';
                    return data;
                }
            },
            { "data": "Nama" },
            { "data": "NamaSiswa" },

        ],
        "bDestroy": true
    });
    TabelKelasQuranSiswa.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function TambahKelompokQuran() {
    $("#ModalHeaderFormBuatKelompokQiroaty").text("Form Tambah Kelompok Qiroaty");
    $("#ModalFormBuatKelompokQiroaty").modal("show");
    ComboGetPegawai(function (obj) {
        $("select[name='IdPegawai']").html(obj);
    });
    ComboHariKe(function (obj) {
        $("select[name='HariKe']").html(obj);
    });
}

function EditKelompokQuran(IdKelasQuran,Nama,NamaSingkat,IdPegawai,HariKe,JamMulai,JamSelesai) {
    $("#ModalHeaderFormBuatKelompokQiroaty").text("Form Edit Kelompok Qiroaty");
    $("#ModalFormBuatKelompokQiroaty").modal("show");
    $("input[name='IdKelasQuran']").val(IdKelasQuran);
    $("input[name='Nama']").val(Nama);
    $("input[name='Kode']").val(NamaSingkat);
    $("input[name='JamMulai']").val(JamMulai);
    $("input[name='JamSelesai']").val(JamSelesai);
    ComboHariKe(function (obj) {
        $("select[name='HariKe']").html(obj);
    }, HariKe);
    ComboGetPegawai(function (obj) {
        $("select[name='IdPegawai']").html(obj);
    },IdPegawai);
}

function TambahKelompokQiroatySiswa() {
    $("#ModalFormKelompokSiswa").modal("show");
    ComboGetKelompokQuran(function (obj) {
        $("select[name='IdKelasQuran']", "#FormKelompokSiswa").html(obj);
    });

    ComboGetSiswa(function (obj) {
        $("select[name='IdSiswa']").html(obj);
    });
}
function EditKelompokQiroatySiswa(IdKelasQuran,IdSiswa) {
    $("#ModalFormKelompokSiswa").modal("show");
    ComboGetKelompokQuran(function (obj) {
        $("select[name='IdKelasQuran']", "#FormKelompokSiswa").html(obj);
    }, IdKelasQuran);

    ComboGetSiswa(function (obj) {
        $("select[name='IdSiswa']").html(obj);
    }, IdSiswa);
}

function SaveBuatKelompokQiroaty() {
    var formData = new FormData($('#FormBuatKelompokQiroaty')[0]);
    var formObj = $('#FormBuatKelompokQiroaty').serializeObject();
    var Url = UrlService.UpdateKelompokQuran;
    $.ajax({
        url: Url,
        method: "POST",
        data: formData,
        dataType: "json",
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                TabelKelasQuran.ajax.reload();
                $("#ModalFormBuatKelompokQiroaty").modal("hide");
                $('#FormBuatKelompokQiroaty')[0].reset();
                swal({ title: 'Berhasil', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function SaveKelasQuranSiswa() {
    var formData = new FormData($('#FormKelompokSiswa')[0]);
    var formObj = $('#FormKelompokSiswa').serializeObject();
    var Url = UrlService.UpdateKelompokQuranSiswa;
    $.ajax({
        url: Url,
        method: "POST",
        data: formData,
        dataType: "json",
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                GetTabelKelasQuranSiswa(formObj.IdKelasQuran);
                $("#ModalFormKelompokSiswa").modal("hide");
                $('#FormKelompokSiswa')[0].reset();
                swal({ title: 'Berhasil', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function ModalUploadKelompokQiroaty() {
    $('#ModalUploadKelompokQiroaty').modal('show');
    $('#FileUploadKelompokQiroaty').val("");
}

function SaveUploadKelompokQiroaty() {
    var formData = new FormData($('#FormUploadKelompokQiroaty')[0]);
    $.ajax({
        url: UrlService.UploadKelompokQiroaty,
        type: "POST",
        data: formData,
        dataType: "json",
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (responsebefore) {
            ProgressBar("wait");
        },
        success: function (responseupload) {
            ProgressBar("success");
            if (responseupload.IsSuccess == true) {
                TabelKelasQuran.ajax.reload();
                $("#ModalUploadKelompokQiroaty").modal("hide");
                swal({
                    title: 'Berhasil Upload Kelompok Qiroaty',
                    text: responseupload.ReturnMessage,
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
            } else if (responseupload.IsSuccess == false) {
                swal({
                    title: 'Gagal Upload Kelompok Qiroaty',
                    text: responseupload.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            ProgressBar("success");
            swal({
                title: 'Gagal Upload Kelompok Qiroaty',
                text: jqXHR + " " + textStatus + " " + errorThrown,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}