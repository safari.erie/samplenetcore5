﻿CKEDITOR.replace( document.querySelector( '.ckeditor-catatan' ), {
    toolbar: [
        { name: 'document', items: [ 'NewPage', 'Preview' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
        { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
    ]
});
CKEDITOR.replace( document.querySelector( '.ckeditor-upload' ), {
    toolbar: [
        { name: 'document', items: [ 'NewPage', 'Preview' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
        { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
    ]
});
$(document).ready(function () {
  DataMapelGuru(GetCurrentDate("TanggalOnly"));
  $("input[name='CheckAllIdSiswa']").change(function () {
    //"select all" change
    var status = this.checked; // "select all" checked status
    $("input[name='IdSiswa']").each(function () {
      //iterate all listed checkbox items
      this.checked = status; //change ".checkbox" checked status
    });
  });

  $("input[name='TipeNilai']").change(function (e) {
    if ($(this).val() == "NilaiAngka") {
      $("#DivNilaiAngka").fadeIn();
      $("#DivNilaiHuruf").hide();
      $('#NilaiHuruf').val('');
    } else {
      $("#DivNilaiAngka").hide();
      $("#DivNilaiHuruf").fadeIn();
      $('#NilaiAngka').val('');
    }
  });


});

const Url = {
  GetKbmByPegawais: base_url + "/api/elearnings/GetKbmByPegawais",
  GetKbmByPegawai: base_url + "/api/elearnings/GetKbmByPegawai",
  GetKbmTugasSiswas: base_url + "/api/elearnings/GetKbmTugasSiswas",
  UpdateNilai: base_url + "/api/elearnings/UpdateNilai",
  UpdateAdbsensi: base_url + "/api/elearnings/UpdateAdbsensi",
  UploadJawabanSusulan: base_url + "/api/elearnings/UploadJawabanSusulan",
};

function PencarianMapel() {
  var Tanggal = $("#TanggalPencarian").val();
  DataMapelGuru(Tanggal);
}
function remove_quotes(values1) {
	if(values1 == null) return null;		
	values1 = values1.replace(/"/g, "\\'");
	var values = values1.toString();
	var str = "";
	var blockList = ['"','\'','\\']; // This is the list of key words to be escaped
	var flag = 0;
	for(var i = 0;i<values.length;i++)
	{
		for(var j=0;j<blockList.length;j++)
		{
			if(values[i] == blockList[j])
			{
				flag = 1;
				break;
			}
		}
		if(flag == 0)
		str += values[i];
		else
		{
			str += '\\';
			str += values[i];
			flag = 0;
		}
	}
	return str;
}

function DataMapelGuru(Param) {
  $("#TabelData tfoot th").each(function () {
    var title = $(this).text();
    $(this).html(
      '<input type="text" class="form-control" placeholder="CARI ' +
      title +
      '" />'
    );
  });
  var TabelData = $("#TabelData").DataTable({
    paging: true,
    searching: true,
    ordering: true,
    info: true,
    pageLength: 10,
    lengthChange: true,
    processing: true,
    ajax: {
      url: Url.GetKbmByPegawais + "?Tanggal=" + Param,
      method: "GET",
      beforeSend: function (xhr) {},
      dataSrc: function (json) {
        if (json.Data == null) {
          iziToast.error({
              title: 'Gagal',
              message: json.ReturnMessage,
              position: 'topRight'
          });
          $("div#empty-data").fadeIn();
          $("div#loading-data").hide();
          $("span#htmlEmptyData").html(json.ReturnMessage);
          $("#ModalPencarian").modal("hide");
          return json;
        } else {
          $("div#no-data").hide();
          $("div#data").fadeIn();
          $("#ModalPencarian").modal("hide");
          return json.Data;
        }
      },
    },
    columns: [{
        render: function (data, type, full, meta) {
          var Data = '';
          var str = full.NamaMapel;
          var cekLibur = str.slice(0, 5);
          if (cekLibur == "LIBUR") {
            Data += '<button type="button" class="btn btn-primary btn-sm btn-block"><i class="fa fa-bullhorn"></i> Libur</button>';
          } else {
            var ParamMeeting = "";
            var ParamJawaban = "";
            ParamMeeting = "ModalMeeting('" + full.IdKbmMateri + "')";
             ParamJawaban =
             "JawabanSiswa('" +
             full.IdKbmMateri +
             "','" +
             full.NamaMapel +
             "','" +
             remove_quotes(full.JudulMateri) +
             "','" +
             full.NamaKelasParalel +
             "')";
			//ParamJawaban = `JawabanSiswa(${full.IdKbmMateri},'${full.NamaMapel}', '${full.JudulMateri.replace(/"/g, "\\'")}', '${full.NamaKelasParalel}')`;
			//console.log('cekQuotes -> ',remove_quotes(full.JudulMateri))
			//var abc = full.JudulMateri.replace(/"/g, "\\'");
			//console.log('aaaa -> ',abc)
            Data +=
              '<button type="button" class="btn btn-primary btn-sm" onClick="' +
              ParamMeeting +
              '"><i class="fa fa-youtube-play"></i> Meeting</button>&nbsp;&nbsp;';
            Data +=
              '<button type="button" class="btn btn-success btn-sm" onClick="' +
              ParamJawaban +
              '"><i class="fa fa-users"></i> Jawaban Siswa</button>&nbsp;&nbsp;';
          }

          return Data;
        },
      },
      {
        "render": function (data, type, full, meta) {
          var str = full.NamaMapel;
          var cekLibur = str.slice(0, 5);
          var data = '';
          if (cekLibur == "LIBUR") {
            data = 'Tidak ada';
          } else {
            data = full.JudulMateri;
          }
          return data;
        }
      },
      {
        data: "NamaKelasParalel",
      },
      {
        data: "NamaMapel",
      },
      {
        data: "Tanggal",
      },
      {
        data: "Hari",
      },
      {
        render: function (data, type, full, meta) {
          var data = "";
          data += full.JamMulai + " s/d " + full.JamSelesai;
          return data;
        },
      },
    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      //console.log(aData.JenisData);
      $(nRow).find('td:eq(0)').css('font-weight', '300');
      $(nRow).find('td:eq(1)').css('font-weight', '300');
      $(nRow).find('td:eq(2)').css('font-weight', '300');
      $(nRow).find('td:eq(3)').css('font-weight', '300');
      $(nRow).find('td:eq(4)').css('font-weight', '300');
      $(nRow).find('td:eq(5)').css('font-weight', '300');
      $(nRow).find('td:eq(6)').css('font-weight', '300');
      var str = aData.NamaMapel;
      var cekLibur = str.slice(0, 5);
      if (cekLibur == "LIBUR") {
        //cell background color
        $(nRow).find('td:eq(0)').css('background-color', '#ffceced4');
        $(nRow).find('td:eq(0)').css('color', '#5e5f61');
        $(nRow).find('td:eq(1)').css('background-color', '#ffceced4');
        $(nRow).find('td:eq(1)').css('color', '#5e5f61');
        $(nRow).find('td:eq(2)').css('background-color', '#ffceced4');
        $(nRow).find('td:eq(2)').css('color', '#5e5f61');
        $(nRow).find('td:eq(3)').css('background-color', '#ffceced4');
        $(nRow).find('td:eq(3)').css('color', '#5e5f61');
        $(nRow).find('td:eq(3)').css('background-color', '#ffceced4');
        $(nRow).find('td:eq(3)').css('color', '#5e5f61');
        $(nRow).find('td:eq(4)').css('background-color', '#ffceced4');
        $(nRow).find('td:eq(4)').css('color', '#5e5f61');
        $(nRow).find('td:eq(5)').css('background-color', '#ffceced4');
        $(nRow).find('td:eq(5)').css('color', '#5e5f61');
        $(nRow).find('td:eq(6)').css('background-color', '#ffceced4');
        $(nRow).find('td:eq(6)').css('color', '#5e5f61');
      }
    },
    bDestroy: true,
  });
  TabelData.columns().every(function () {
    var that = this;

    $("input", this.footer()).on("keyup change clear", function () {
      if (that.search() !== this.value) {
        that.search(this.value).draw();
      }
    });
  });
  $(".dataTables_filter").css("display", "none");
  $(".dataTables_length").css("display", "none");
}

function ModalMeeting(Param) {
  $("#ModalMeeting").modal("show");

  $.ajax({
    url: Url.GetKbmByPegawai + "?IdKbmMateri=" + Param,
    type: "GET",
    dataType: "JSON",
    beforeSend: function () {
      ProgressBar("wait");
    },
    success: function (res) {
      ProgressBar("success");
      var data = res.Data;
      console.log(data);
      if (res.IsSuccess) {
        $("#IdKbmMateri", "#FormMeeting").val(data.IdKbmMateri);
        $("#NamaMapel", "#FormMeeting").val(data.NamaMapel);
        $("#KelasParalel", "#FormMeeting").val(data.NamaKelasParalel);
      } else if (!res.IsSuccess) {
        swal({
          title: "Gagal",
          text: res.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Terjadi Kesalahan",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });

  $.ajax({
    url: Url.GetKbmTugasSiswas + "?IdKbmMateri=" + Param,
    type: "GET",
    dataType: "JSON",
    success: function (res) {
      if (res.IsSuccess) {
        var htmlIdSiswa = "";
        $.each(res.Data, (i, v) => {
          htmlIdSiswa +=
            '<option value="' + v.IdSiswa + '">' + v.NamaSiswa + "</option>";
        });
        $("#Absensi").html(htmlIdSiswa);
      } else if (!res.IsSuccess) {
        swal({
          title: "Gagal",
          text: res.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Terjadi Kesalahan",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });

  var TabelData = $("#TabelAbsensi").DataTable({
    paging: true,
    searching: true,
    ordering: true,
    info: true,
    pageLength: 10,
    lengthChange: true,
    processing: true,
    ajax: {
      url: Url.GetKbmTugasSiswas + "?IdKbmMateri=" + Param,
      method: "GET",
      beforeSend: function (xhr) {},
      dataSrc: function (json) {
        if (json.Data == null) {
          return json;
        } else {
          return json.Data;
        }
      },
    },
    columns: [{
        render: function (data, type, full, meta) {
          var Data = "";
          var ParamSiswa = "";
          // var ParamMeeting = '';
          var ParamPreview = "";
          ParamSiswa = "ModalNilai('" + full.IdSiswa + "')";
          Data += '<input type="checkbox" name="IdSiswa" value="" />';
          return Data;
        },
      },
      {
        data: "NamaSiswa",
      },
      {
        data: "NamaKelasParalel",
      },
    ],
    bDestroy: true,
  });
}

function DownloadTemplateNilai(IdKbmMateri) {
  var UrlDownloadTemplateNilai = base_url + "/api/Download/DownloadNilaiSiswa?IdKbmMateri=" + IdKbmMateri;
  window.open(UrlDownloadTemplateNilai, '_blank');
}

function UploadMassalNilai(IdKbmMateri, NamaMapel, JudulMateri, NamaKelasParalel) {
  $("#ModalUploadMassalNilai").modal("show");
  $('#FormUploadMassalNilai')[0].reset();
  $("input[name='NamaMapel']", "#FormUploadMassalNilai").val(NamaMapel);
  $("input[name='JudulMateri']", "#FormUploadMassalNilai").val(JudulMateri);
  $("input[name='KelasParalel']", "#FormUploadMassalNilai").val(NamaKelasParalel);
  $("#BtnSaveUploadMassalNilai").attr("onClick", "SaveUploadMassalNilai('" + IdKbmMateri + "','" + NamaMapel + "','" + JudulMateri + "','" + NamaKelasParalel + "');");
  $(".modal-title").html("Form Upload Nilai Massal");
}

function SaveUploadMassalNilai(IdKbmMateri, NamaMapel, JudulMateri, NamaKelasParalel) {
  var formData = new FormData($('#FormUploadMassalNilai')[0]);
  $.ajax({
    url: base_url + "/api/Elearnings/UploadNilaiSiswa",
    type: "POST",
    data: formData,
    dataType: "json",
    contentType: false,
    cache: true,
    processData: false,
    beforeSend: function (responsebefore) {
      ProgressBar("wait");
    },
    success: function (responseupload) {
      ProgressBar("success");
      if (responseupload.IsSuccess == true) {
        JawabanSiswa(IdKbmMateri, NamaMapel, JudulMateri, NamaKelasParalel);
        swal({
          title: 'Berhasil Upload Nilai Massal',
          text: responseupload.ReturnMessage,
          confirmButtonClass: 'btn-success text-white',
          confirmButtonText: 'Oke, Mengerti',
          type: 'success'
        });
      } else if (responseupload.IsSuccess == false) {
        swal({
          title: 'Gagal Upload Nilai Massal',
          text: responseupload.ReturnMessage,
          confirmButtonClass: 'btn-danger text-white',
          confirmButtonText: 'Oke, Mengerti',
          type: 'error'
        });
      }
    },
    error: function (jqXHR, textStatus, errorThrown) {
      ProgressBar("success");
      swal({
        title: 'Gagal Upload Nilai Massal',
        text: jqXHR + " " + textStatus + " " + errorThrown,
        confirmButtonClass: 'btn-danger text-white',
        confirmButtonText: 'Oke, Mengerti',
        type: 'error'
      });
    }
  });
}

function JawabanSiswa(IdKbmMateri, NamaMapel, JudulMateri, NamaKelasParalel) {
  $("#htmlMapel").html(
    JudulMateri != "null" ? JudulMateri : "Terjadi Kesalahan"
  );
  $("#htmlTanggal").html(NamaMapel + " - " + NamaKelasParalel);
  console.log('GetKbmTugasSiswas-> ', Url.GetKbmTugasSiswas + "?IdKbmMateri=" + IdKbmMateri);
  $("#BtnDownloadTemplateNilai").attr("onClick", "DownloadTemplateNilai('" + IdKbmMateri + "');");
  $("#BtnUploadMassalNilai").attr("onClick", "UploadMassalNilai('" + IdKbmMateri + "','" + NamaMapel + "','" + JudulMateri + "','" + NamaKelasParalel + "');");

  $("#TabelJawaban tfoot th").each(function () {
    var title = $(this).text();
    $(this).html(
      '<input type="text" class="form-control" placeholder="CARI ' +
      title +
      '" />'
    );
  });
  var TabelData = $("#TabelJawaban").DataTable({
    paging: true,
    searching: true,
    ordering: true,
    info: true,
    pageLength: 10,
    lengthChange: true,
    processing: true,
    ajax: {
      url: Url.GetKbmTugasSiswas + "?IdKbmMateri=" + IdKbmMateri,
      method: "GET",
      beforeSend: function (xhr) {
        ProgressBar("wait");
      },
      dataSrc: function (json) {
        ProgressBar("success");
        if (json.Data == null) {
          return json;
        } else {
          $("div#JawabanSiswa").fadeIn();
          $("div#data").hide();
          $("div#empty-data").hide();
          $("div#loading-data").hide();
          return json.Data;
        }
      },
    },
    columns: [{
        render: function (data, type, full, meta) {
          var Data = "";
          var Catatan = "";
          if (full.JawabanSusulan != null) {
            if (full.JawabanSusulan.Catatan != null) {
              Catatan = full.JawabanSusulan.Catatan;
            } else {
              Catatan = "";
            }
          } else {
            Catatan = "";
          }
          var ParamSiswa = "";
          var ParamUpload = "";
          ParamSiswa =
            "ModalNilai('" + full.IdKbmMateri + "','" + full.IdSiswa + "')";
          ParamUpload =
            "ModalUploadJawaban('" +
            full.IdKbmMateri +
            "','" +
            full.IdSiswa +
            "','" +
            Catatan +
            "')";
          Data +=
            '<button type="button" class="btn btn-primary btn-sm" onClick="' +
            ParamSiswa +
            '"><i class="fa fa-user"></i>&nbsp; Nilai</button>&nbsp;&nbsp;';
          Data +=
            '<button type="button" class="btn btn-warning btn-sm" onClick="' +
            ParamUpload +
            '"><i class="fa fa-upload"></i>&nbsp; Upload Jawaban</button>&nbsp;&nbsp;';
          return Data;
        },
      },
      {
        data: "NamaSiswa",
      },
      {
        data: "NamaKelasParalel",
      },
      {
        render: function (data, type, full, meta) {
          var Data = "";
          if (full.IsRemedial == 1) {
            Data = '<span class="badge badge-danger">Remedial</span>';
          } else if (full.PathFileUrl == null) {
            if (full.JawabanSusulan != null) {
              Data = '<span class="badge badge-success">Sudah Mengerjakan</span>';
            } else {
              Data = '<span class="badge badge-warning">Belum Mengerjakan</span>';
            }
          } else {
            Data = '<span class="badge badge-success">Sudah Mengerjakan</span>';
          }
          return Data;
        },
      },
      {
        render: function (data, type, full, meta) {
          var Data = "";
          if (full.PathFileUrl != null) {
            Data = full.PathFileUrl;
          } else {
            if (full.JawabanSusulan != null) {
              Data = full.JawabanSusulan.NamaFile;
            }
          }
          return Data;
        },
      },
      {
        render: function (data, type, full, meta) {
          var Data = "";
          if (full.NilaiHuruf == null) {
            Data = full.NilaiAngka;
          } else {
            Data = full.NilaiHuruf;
          }
          return Data;
        },
      },
      {
        data: "StatusMeeting",
      },
    ],
    bDestroy: true,
  });
  TabelData.columns().every(function () {
    var that = this;

    $("input", this.footer()).on("keyup change clear", function () {
      if (that.search() !== this.value) {
        that.search(this.value).draw();
      }
    });
  });
  $(".dataTables_filter").css("display", "none");
  $(".dataTables_length").css("display", "none");
}

var _docExts = ["pdf", "doc", "docx", "odt", "txt"];
var _imgExts = ["jpg", "jpeg", "png", "gif", "ico"];

function isExtension(ext, extnArray) {
  var result = false;
  var i;
  if (ext) {
    ext = ext.toLowerCase();
    for (i = 0; i < extnArray.length; i++) {
      if (extnArray[i].toLowerCase() === ext) {
        result = true;
        break;
      }
    }
  }
  return result;
}

function ModalGambarJawaban(File) {
  $('#ModalGambarJawaban').modal('show');
  $('#GambarJawaban').html('<center><img src="' + base_url + '/Asset/Files/Elearning/' + File + '" class="img-fluid" /></center>');
  var Unduh =
    '<a href="' + base_url + '/Asset/Files/Elearning/' + File + '" target="_blank" class="btn btn-primary" download><i class="fa fa-download"></i> Unduh' +
    "</a>";
  $('#BtnUnduh').html(Unduh);
}

function ModalNilai(IdKbmMateri, IdSiswa) {
  $("#ModalNilai").modal("show");
  $("#ModalHeaderFormApproveJawabanSiswa").text(
    "Form Input Hasil Jawaban Siswa"
  );

  $('#FormApproveJawabanSiswa')[0].reset();
  $("#IdKbmMateri", "#FormApproveJawabanSiswa").val("");
  $("#IdSiswa", "#FormApproveJawabanSiswa").val("");
  $('#NilaiHuruf').val('');
  $('#NilaiAngka').val('');
  CKEDITOR.instances.Catatan.setData('');

  $.ajax({
    url: Url.GetKbmTugasSiswas + "?IdKbmMateri=" + IdKbmMateri,
    type: "GET",
    dataType: "JSON",
    beforeSend: function (xhr) {
      ProgressBar("wait");
    },
    success: function (res) {
      ProgressBar("success");
      if (res.IsSuccess) {
        $.each(res.Data, (i, v) => {
          if (v.IdSiswa == IdSiswa) {
            $("#IdKbmMateri", "#FormApproveJawabanSiswa").val(v.IdKbmMateri);
            $("#IdSiswa", "#FormApproveJawabanSiswa").val(v.IdSiswa);
            $("#Nilai", "#FormApproveJawabanSiswa").val(v.Nilai);
            CKEDITOR.instances.Catatan.setData(v.Catatan);

            var htmlJawaban = "";
            if (v.IdJenisPathFile == 1) {

              var NamaFile = v.PathFileUrl;
              var Pecah = NamaFile.split(';');
              $.each(Pecah, (i, v) => {
                var extn = v.split('.').pop();
                var isDoc = isExtension(extn, _docExts);
                var isImg = isExtension(extn, _imgExts);
                if (isImg) {
                  var ParamFile = "ModalGambarJawaban('" + cekQuotes(v) + "')";
                  htmlJawaban +=
                    '<div class="col-md-6"><a href="javascript:void(0);" onclick="' + ParamFile + '" style="color:inherit;">' +
                    '<span class="badge badge-info mt-2"><i class="fa fa-download"></i> ' + v + '</span>&nbsp; ' +
                    "</a></div>";
                } else {
                  htmlJawaban +=
                    '<div class="col-md-6"><a href="' + base_url + '/Asset/Files/Elearning/' + v + '" target="_blank" download>' +
                    '<span class="badge badge-info mt-2"><i class="fa fa-download"></i> ' + v + '</span>' +
                    "</a></div>";
                }
              })


            } else if (v.IdJenisPathFile == 2) {
              if (v.PathFileUrl == "Ya") {
                htmlJawaban = '<div class="col-md-6"><span class="badge badge-success"><i class="fa fa-check"></i> Sudah Mengisi Rubrik</span></div>';
              } else {
                htmlJawaban =
                  '<div class="col-md-12"><iframe width="100%" height="300px" class="img-fluid" src="' +
                  v.PathFileUrl +
                  '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>';
              }
            } else {
              if (v.JawabanSusulan != null) {
                htmlJawaban =
                  '<div class="col-md-6"><a href="' + base_url + '/Asset/Files/Elearning/' + v.JawabanSusulan.NamaFile + '" target="_blank" download>' +
                  '<span class="badge badge-info"><i class="fa fa-download"></i> ' + v.JawabanSusulan.NamaFile + '</span>' +
                  "</a></div>";
              } else {
                htmlJawaban =
                  '<span class="badge badge-danger">Belum Ada Jawaban</span>';
              }
            }
            $("#htmlJawaban").html(htmlJawaban);

            if (v.NilaiHuruf != null) {
              $('#TipeNilai1').attr('checked', false);
              $('#TipeNilai2').attr('checked', true);
              $('#NilaiHuruf').val(v.NilaiHuruf);
              $("#DivNilaiAngka").css("display", "none");
              $("#DivNilaiHuruf").css("display", "block");
            } else {
              $('#TipeNilai1').attr('checked', true);
              $('#TipeNilai2').attr('checked', false);
              $('#NilaiAngka').val(v.NilaiAngka);
              $("#DivNilaiAngka").css("display", "block");
              $("#DivNilaiHuruf").css("display", "none");
            }

            if (v.IsRemedial == 1) {
              $('#Remedial').attr('checked', true);
              $('#Tuntas').attr('checked', false);
            } else {
              $('#Remedial').attr('checked', false);
              $('#Tuntas').attr('checked', true);
            }
          }
        });
      } else if (!res.IsSuccess) {
        swal({
          title: "Gagal",
          text: res.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Terjadi Kesalahan",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function ModalUploadJawaban(IdKbmMateri, IdSiswa, Catatan) {
  $("#ModalUploadJawaban").modal("show");
  $("#IdKbmMateri", "#FormUploadJawaban").val(IdKbmMateri);
  $("#IdSiswa", "#FormUploadJawaban").val(IdSiswa);
  CKEDITOR.instances.CatatanUpload.setData(Catatan);
  $('#FileJawaban').val('');
}

function Kembali() {
  $("#TabelData").DataTable().ajax.reload();
  $(".dataTables_scrollHeadInner, .table-bordered").css("width", "inherit");

  $("div#JawabanSiswa").hide();
  $("div#data").fadeIn();
  $("div#empty-data").hide();
  $("div#loading-data").hide();
}

function UpdateAdbsensi() {
  var fd = new FormData($("#FormMeeting")[0]);
  var formObj = $("#FormMeeting").serializeObject();
  $("select[name='Absensi[]'] option:selected").each(function (i, item) {
    fd.append("IdSiswas[" + i + "]", $(item).val());
  });

  $.ajax({
    url: Url.UpdateAdbsensi +
      "?IdKbmMateri=" +
      formObj.IdKbmMateri +
      "&NamaUrl=" +
      formObj.NamaUrl,
    type: "POST",
    data: fd,
    processData: false,
    contentType: false,
    dataType: "JSON",
    success: function (res) {
      if (res.IsSuccess) {
        swal({
          title: "Sukses",
          text: "Anda berhasil simpan siswa yang tidak hadir",
          confirmButtonClass: "btn-success text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "success",
        });
        $("#ModalMeeting").modal("hide");
      } else if (!res.IsSuccess) {
        swal({
          title: "Gagal",
          text: res.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Terjadi Kesalahan",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function UpdateNilai() {
  var formObj = $("#FormApproveJawabanSiswa").serializeObject();
  var Catatan = $("textarea[name='Catatan']", "#FormApproveJawabanSiswa").val();
  var NilaiHurufx = formObj.NilaiHuruf == 'undefined' ? null : formObj.NilaiHuruf;
  var fd = new FormData();
  fd.append('IdKbmMateri', formObj.IdKbmMateri)
  fd.append('IdSiswa', formObj.IdSiswa)
  fd.append('NilaiAngka', formObj.NilaiAngka)
  fd.append('NilaiHuruf', NilaiHurufx)
  fd.append('IsRemedial', formObj.IsRemedial)
  fd.append('Catatan', formObj.Catatan)
  $.ajax({
    url: Url.UpdateNilai,
    method: 'POST',
    processData: false,
    contentType: false,
    data: fd,
    dataType: 'JSON',
    beforeSend: function (xhr) {
      ProgressBar('wait');
    },
    success: function (res) {
      ProgressBar('success');
      if (res.IsSuccess) {
        swal({
          title: "Sukses",
          text: "Anda berhasil simpan nilai siswa",
          confirmButtonClass: "btn-success text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "success",
        });
        $("#TabelJawaban").DataTable().ajax.reload();
        $("#ModalNilai").modal("hide");
      } else if (!res.IsSuccess) {
        ProgressBar('success');
        swal({
          title: "Gagal",
          text: res.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Terjadi Kesalahan",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function UploadJawabanSusulan() {
  var fd = new FormData($("#FormUploadJawaban")[0]);
  fd.append(
    "FileJawaban",
    $('input[type="file"][name="FileJawaban"]')[0].files[0]
  );
  var formObj = $("#FormUploadJawaban").serializeObject();

  $.ajax({
    url: Url.UploadJawabanSusulan +
      "?IdSiswa=" +
      formObj.IdSiswa +
      "?IdKbmMateri=" +
      formObj.IdKbmMateri +
      "&Catatan=" +
      formObj.CatatanUpload,
    type: "POST",
    data: fd,
    processData: false,
    contentType: false,
    dataType: "JSON",
    success: function (res) {
      if (res.IsSuccess) {
        $("#ModalUploadJawaban").modal("hide");
        $("#TabelJawaban").DataTable().ajax.reload();

        swal({
          title: "Sukses",
          text: "Berhasil upload jawaban siswa",
          confirmButtonClass: "btn-success text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "success",
        });
      } else if (!res.IsSuccess) {
        swal({
          title: "Gagal",
          text: res.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Terjadi Kesalahan",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}