﻿/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 12082020
================================================================================================================= */
const UrlService = {
    GetUnits: base_url + '/Units/GetUnits'
};

//////// START SECTION TABEL HariLibur ///////////
$('#TabelHariLibur tfoot th').each(function () {
    var title = $(this).text();
    $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
});
var TabelHariLibur = $('#TabelHariLibur').DataTable({
    "paging": false,
    "searching": true,
    "ordering": true,
    "info": true,
    "pageLength": 5,
    "lengthChange": true,
    "scrollX": true,
    "scrollY": "230px",
    "scrollCollapse": true,
    "processing": true,
    "ajax": {
        "url": base_url + "/api/Elearnings/GetHariLiburs",
        "method": 'GET',
        "beforeSend": function (xhr) {
        },
        "dataSrc": function (json) {
            if (json.Data == null) {
                iziToast.error({
                    title: 'Gagal Menampilkan Data Tabel Hari Libur',
                    message: json.ReturnMessage,
                    position: 'topRight'
                });
                return json;
            } else {
                return json.Data;
            }
        }
    },
    "columns": [
        {
            "render": function (data, type, full, meta) {
                var Data = "";
                var ParamEdit = "EditData('" + full.Tanggal + "')";
                var ParamHapus = "HapusData('" + full.IdHariLibur + "','" + cekQuotes(full.Nama) + "')";
                Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
                Data += '&nbsp;&nbsp;&nbsp;';
                Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                return Data;
            }
        },
        { "data": "Tanggal" },
        { "data": "Nama" },
    ],
    "bDestroy": true
});
TabelHariLibur.columns().every(function () {
    var that = this;

    $('input', this.footer()).on('keyup change clear', function () {
        if (that.search() !== this.value) {
            that
                .search(this.value)
                .draw();
        }
    });
});
$(".dataTables_filter").css("display", "none");
$(".dataTables_length").css("display", "none");
//////// START SECTION TABEL HARI LIBUR ///////////

function TambahData() {
    $('#FormHariLibur')[0].reset();
    $("#BtnSaveHariLibur").attr("onClick", "SaveData();");
    $(".modal-title").html("Tambah Hari Libur");
    $("input[name='Tanggal']", "#FormHariLibur").removeAttr("value");
    $("input[name='IdHariLibur']", "#FormHariLibur").val("0");
    $('#DivTanggalAkhir').fadeIn();
    SetDatepicker();
}

function EditData(Tanggal) {
    $('#FormHariLibur')[0].reset();
    $("#BtnSaveHariLibur").attr("onClick", "SaveData();");
    $(".modal-title").html("Edit Hari Libur");
    $('#DivTanggalAkhir').fadeOut();
    $("input[name='TanggalEnd']", "#FormHariLibur").val("");
    var Url = base_url + "/api/Elearnings/GetHariLibur?Tanggal=" + Tanggal;
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var ResponseData = responsesuccess.Data;
                DestroyDatepicker();
                $("input[name='IdHariLibur']", "#FormHariLibur").val(ResponseData.IdHariLibur).attr("readonly", true);
                $("input[name='Tanggal']", "#FormHariLibur").val(ResponseData.Tanggal).attr("readonly", true);
                $("input[name='Nama']", "#FormHariLibur").val(ResponseData.Nama);
            } else {
                swal({ title: 'Gagal :(', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function SaveData() {
    var form = new FormData($('#FormHariLibur')[0]);
    var Url = base_url + "/api/Elearnings/HariLiburAddEdit";

    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        data: form,
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $('#FormHariLibur')[0].reset();
                TabelHariLibur.ajax.reload();
                TambahData();
                iziToast.success({
                    title: 'Berhasil Menyimpan Hari Libur',
                    message: responsesave.ReturnMessage,
                    position: 'topRight'
                });
            } else if (responsesave.IsSuccess == false) {
                iziToast.error({
                    title: 'Gagal Menyimpan Hari Libur',
                    message: responsesave.ReturnMessage,
                    position: 'topRight'
                });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function HapusData(IdHariLibur, Nama) {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus data Hari Libur " + Nama,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: true
    },
        function () {
            var Url = base_url + "/api/Elearnings/HariLiburDelete?IdHariLibur=" + IdHariLibur;
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesave) {
                    ProgressBar("success");
                    if (responsesave.IsSuccess == true) {
                        TabelHariLibur.ajax.reload();
                        iziToast.success({
                            title: 'Berhasil Menghapus Hari Libur',
                            message: responsesave.ReturnMessage,
                            position: 'topRight'
                        });
                    } else if (responsesave.IsSuccess == false) {
                        iziToast.error({
                            title: 'Gagal Menghapus Hari Libur',
                            message: responsesave.ReturnMessage,
                            position: 'topRight'
                        });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}

GetTabelUnit();
function GetTabelUnit() {
    $('#TabelUnit tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelUnit = $('#TabelUnit').DataTable({
        "paging": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "pageLength": 5,
        "lengthChange": false,
        "processing": true,
        "order": [
            [1, "asc"]
        ],
        "columnDefs": [{
            "targets": 1,
            "type": "date-id",
            "searchable": true
        }],
        "ajax": {
            "url": UrlService.GetUnits,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    swal({ title: 'Gagal Menampilkan Data Tabel Unit', text: json.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [
            {
                render: function (data, type, full, meta) {
                    var Data = "";
                    var ParamEdit = "";
                    var ParamHapus = "";
                    ParamEdit = "SetHariLiburUnit('" + full.IdUnit + "','" + cekQuotes(full.Nama) + "')";
                    Data += '<button type="button" class="btn btn-primary btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Set Hari Libur</button>&nbsp;&nbsp;';
                    return Data;
                },
            },
            {
                "render": function (data, type, full, meta) {
                    return "<strong>" + full.IdUnit + "</strong>";
                }
            },
            {
                "render": function (data, type, full, meta) {
                    return "<strong>" + full.Nama + "</strong>";
                }
            },
        ],
        "bDestroy": true
    });
    TabelUnit.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function SetHariLiburUnit(IdUnit, NamaUnit) {
    $("#ImgSearch").css("display", "none");
    $("#FormSetHariLibur").css("display", "block");

    $("#LabelNoAssign").html("No Assign Hari Libur");
    $("#LabelAssign").html("Assign Hari Libur");

    $("#HeaderBody").html("Set Hari Libur Unit - " + NamaUnit);
    $('.ComboSumber').attr("name", "NoAssignLiburs[]");
    $('.ComboTerpilih').attr("name", "AssignLiburs[]");
    $("#BtnSave").attr("onClick", "SaveHariLiburUnit()");

    $("input[name='IdUnit']").val(IdUnit);
    ComboGetHariLibursByUnit(function (HtmlComboNoAssignLiburs, HtmlComboAssignLiburs) {
        $("select[name='NoAssignLiburs[]']").html(HtmlComboNoAssignLiburs);
        $("select[name='AssignLiburs[]']").html(HtmlComboAssignLiburs);
    }, IdUnit);
}

function SaveHariLiburUnit() {
    var formObj = $('#FormSetHariLibur').serializeObject();
    var IdHariLiburs = "";
    $('.ComboTerpilih').find('option').each(function (index, item) {
        IdHariLiburs += "&IdHariLiburs=" + parseInt($(item).val());
    });
    var Url = base_url + "/api/Elearnings/AddHariLiburUnit?IdUnit=" + parseInt(formObj.IdUnit) + IdHariLiburs;
    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $('#FormSetHariLibur')[0].reset();
                $("#TabelUnit").DataTable().ajax.reload();
                swal({ title: 'Berhasil', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}
