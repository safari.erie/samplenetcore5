﻿const UrlService = {
    JenisEvaluasiHarianAddEdit: base_url + '/api/Elearnings/JenisEvaluasiHarianAddEditPegawai',
    GetJenisEvaluasiHarians: base_url + '/api/Elearnings/GetJenisEvaluasiHarianPegawais',
    GetUnits: base_url + '/Units/GetUnits',
    DeleteJenisEvaluasiHarian: base_url + '/api/Elearnings/DeleteJenisEvaluasiHarianPegawai',
    AddEvalHarianUnit: base_url + "/api/Elearnings/AddEvalHarianUnitPegawai"
};

GetTabelMutabaah();
function GetTabelMutabaah() {
    $('#TabelMutabaah tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelMutabaah = $('#TabelMutabaah').DataTable({
        "paging": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "pageLength": 5,
        "lengthChange": false,
        "processing": true,
        "order": [
            [1, "asc"]
        ],
        "columnDefs": [{
            "targets": 1,
            "type": "date-id",
            "searchable": true
        }],
        "ajax": {
            "url": UrlService.GetJenisEvaluasiHarians,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    // iziToast.error({
                    //     title: 'Gagal Menampilkan Data Tabel Jenis Evaluasi Harian Pegawai',
                    //     message: json.ReturnMessage,
                    //     position: 'topRight'
                    // });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [
            {
                render: function (data, type, full, meta) {
                    var Data = "";
                    var ParamEdit = "";
                    var ParamHapus = "";
                    ParamEdit = "EditData('" + full.IdJenisEvaluasiHarianPegawai + "','" + cekQuotes(full.Nama) + "','" + cekQuotes(full.NamaSingkat) + "','" + full.IdUnit + "')";
                    ParamHapus = "HapusData('" + full.IdJenisEvaluasiHarianPegawai + "','" + cekQuotes(full.Nama) + "')";
                    Data += '<button type="button" class="btn btn-primary btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Edit</button>&nbsp;&nbsp;';
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash-o"></i> Hapus</button>';
                    return Data;
                },
            },
            {
                "render": function (data, type, full, meta) {
                    return "<strong>" + full.Nama + "</strong>";
                }
            },
            {
                "render": function (data, type, full, meta) {
                    return "<strong>" + full.NamaSingkat + "</strong>";
                }
            },
        ],
        "bDestroy": true
    });
    TabelMutabaah.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function GoSetEvaluasiHarianUnit() {
    $('html, body').animate({
        scrollTop: $("#TabelUnit").offset().top
    }, 50);
}

GetTabelUnit();
function GetTabelUnit() {
    $('#TabelUnit tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelUnit = $('#TabelUnit').DataTable({
        "paging": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "pageLength": 5,
        "lengthChange": false,
        "processing": true,
        "order": [
            [1, "asc"]
        ],
        "columnDefs": [{
            "targets": 1,
            "type": "date-id",
            "searchable": true
        }],
        "ajax": {
            "url": UrlService.GetUnits,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    swal({ title: 'Gagal Menampilkan Data Tabel Unit', text: json.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [
            {
                render: function (data, type, full, meta) {
                    var Data = "";
                    var ParamEdit = "";
                    var ParamHapus = "";
                    ParamEdit = "SetEvaluasiHarianUnit('" + full.IdUnit + "','" + cekQuotes(full.Nama) + "')";
                    Data += '<button type="button" class="btn btn-primary btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Set Evaluasi Harian</button>&nbsp;&nbsp;';
                    return Data;
                },
            },
            {
                "render": function (data, type, full, meta) {
                    return "<strong>" + full.IdUnit + "</strong>";
                }
            },
            {
                "render": function (data, type, full, meta) {
                    return "<strong>" + full.Nama + "</strong>";
                }
            },
        ],
        "bDestroy": true
    });
    TabelUnit.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function SetEvaluasiHarianUnit(IdUnit, NamaUnit) {
    $("#ImgSearch").css("display", "none");
    $("#FormSetEvaluasiHarian").css("display", "block");

    $("#LabelNoAssign").html("No Assign Evaluasi Harian");
    $("#LabelAssign").html("Assign Evaluasi Harian");

    $("#HeaderBody").html("Set Evaluasi Harian Unit - " + NamaUnit);
    $('.ComboSumber').attr("name", "NoAssignEvals[]");
    $('.ComboTerpilih').attr("name", "AssignEvals[]");
    $("#BtnSave").attr("onClick", "SaveEvaluasiHarianUnit()");

    $("input[name='IdUnit']").val(IdUnit);
    ComboGetJenisEvaluasiHariansByUnitPegawai(function (HtmlComboNoAssignEvals, HtmlComboAssignEvals) {
        $("select[name='NoAssignEvals[]']").html(HtmlComboNoAssignEvals);
        $("select[name='AssignEvals[]']").html(HtmlComboAssignEvals);
    }, IdUnit);
}

function SaveEvaluasiHarianUnit() {
    var formObj = $('#FormSetEvaluasiHarian').serializeObject();
    var IdJenisEvaluasiHarianPegawais = "";
    $('.ComboTerpilih').find('option').each(function (index, item) {
        IdJenisEvaluasiHarianPegawais += "&IdJenisEvaluasiHarianPegawais=" + parseInt($(item).val());
    });
    var Url = UrlService.AddEvalHarianUnit + "?IdUnit=" + parseInt(formObj.IdUnit) + IdJenisEvaluasiHarianPegawais;
    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $('#FormSetEvaluasiHarian')[0].reset();
                $("#TabelUnit").DataTable().ajax.reload();
                swal({ title: 'Berhasil', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function CancelMutabaah() {
    $('#FormJenisMutabaah')[0].reset();
}
function EditData(IdJenisEvaluasiHarianPegawai, Nama, NamaSingkat, IdUnit) {
    $("input[name='IdJenisEvaluasiHarianPegawai']").val(IdJenisEvaluasiHarianPegawai);
    $("input[name='Nama']").val(Nama);
    $("input[name='NamaSingkat']").val(NamaSingkat);
}


function HapusData(IdJenisEvaluasiHarianPegawai, Nama) {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus data Mutabaah Pegawai " + Nama,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            var Url = UrlService.DeleteJenisEvaluasiHarian + "?IdJenisEvaluasiHarianPegawai=" + IdJenisEvaluasiHarianPegawai;
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesave) {
                    ProgressBar("success");
                    if (responsesave.IsSuccess == true) {
                        GetTabelMutabaah();
                        swal({ title: 'Berhasil Menghapus Mutabaah Pegawai', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                    } else if (responsesave.IsSuccess == false) {
                        swal({ title: 'Gagal Menghapus Mutabaah Pegawai', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}

function SimpanMutabaah() {
    var formData = new FormData($('#FormJenisMutabaah')[0]);
    $.ajax({
        url: UrlService.JenisEvaluasiHarianAddEdit,
        method: 'POST',
        processData: false,
        contentType: false,
        data: formData,
        dataType: 'JSON',
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $('#FormJenisMutabaah')[0].reset();
                GetTabelMutabaah("");
                swal({ title: 'Berhasil Menyimpan Jenis Mutabaah Pegawai', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal Menyimpan Jenis Mutabaah Pegawai', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}