﻿CKEDITOR.replace( document.querySelector( '.ckeditor-materi' ), {
    toolbar: [
        { name: 'document', items: [ 'NewPage', 'Preview' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
        { name: 'basicstyles', items: ['Bold', 'Italic'] },
        { name: 'links' }
    ]
});
CKEDITOR.replace( document.querySelector( '.ckeditor-soal' ), {
    toolbar: [
        { name: 'document', items: [ 'NewPage', 'Preview' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
        { name: 'basicstyles', items: [ 'Bold', 'Italic' ] },
        { name: 'links' }
    ]
});
$(document).ready(function () {
    $.fn.modal.Constructor.prototype._enforceFocus = function () {};
    DataMapelGuru("");
    // TambahUrlVideo();
    // TambahUrlVideo();
    // TambahUploadGambar();
    // TambahUploadGambar();

    $("input[name='TipeSoal']").change(function () {
        if (this.value == 2) {
            $("#TipeSoalUpload").css("display", "none");
            $("#TipeSoalEmbed").css("display", "block");
            $('#FileSoal').val('');
            $('#UrlSoal').val('');
        } else if (this.value == 1) {
            $("#TipeSoalUpload").css("display", "block");
            $("#TipeSoalEmbed").css("display", "none");
            $('#FileSoal').val('');
            $('#UrlSoal').val('');
        } else {
            $("#TipeSoalUpload").css("display", "none");
            $("#TipeSoalEmbed").css("display", "none");
            $('#FileSoal').val('');
            $('#UrlSoal').val('');
        }
    });

    $("input[name='TipeSoalRemedial']").change(function () {
        if (this.value == 2) {
            $("#TipeSoalRemedialUpload").css("display", "none");
            $("#TipeSoalRemedialEmbed").css("display", "block");
            $('#FileSoalRemedial').val('');
            $('#UrlSoalRemedial').val('');
        } else if (this.value == 1) {
            $("#TipeSoalRemedialUpload").css("display", "block");
            $("#TipeSoalRemedialEmbed").css("display", "none");
            $('#FileSoalRemedial').val('');
            $('#UrlSoalRemedial').val('');
        } else {
            $("#TipeSoalRemedialUpload").css("display", "none");
            $("#TipeSoalRemedialEmbed").css("display", "none");
            $('#FileSoalRemedial').val('');
            $('#UrlSoalRemedial').val('');
        }
    });
});

const Url = {
    GetKbmByPegawais: base_url + '/api/elearnings/GetKbmByPegawais',
    GetKbmByPegawai: base_url + '/api/elearnings/GetKbmByPegawai',
    UpdateMateris: base_url + '/api/elearnings/UpdateMateris',
    DeleteFileMateri: base_url + '/api/elearnings/deletefilemateri',
    DeleteFileSoal: base_url + '/api/elearnings/deletefilesoal',
    DeleteFileMedia: base_url + '/api/elearnings/deletefilemedia',
    ResetMateriSoal: base_url + '/api/elearnings/ResetMateriSoal',
}

var xInputUrlVideo = 1;
var xInputUploadGambar = 1;

function DataMapelGuru(Param) {
    $('#TabelData tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="CARI ' + title + '" />');
    });
    var TabelData = $('#TabelData').DataTable({
        "paging": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "processing": true,
        "scrollX": true,
        "scrollY": false,
        "responsive": {
            details: {
                renderer: $.fn.dataTable.Responsive.renderer.tableAll()
            }
        },
        "ajax": {
            "url": Url.GetKbmByPegawais + "?Tanggal=" + Param,
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    $('div#empty-data').fadeIn();
                    $('div#loading-data').hide();
                    $('span#htmlEmptyData').html(json.ReturnMessage);
                    return json;
                } else {
                    $('div#no-data').hide();
                    $('div#data').fadeIn();
                    return json.Data;
                }
            }
        },
        "columns": [{
                "data": "IdKbmMateri",
                "render": function (data, type, full, meta) {
                    var Data = '';
                    var str = full.NamaMapel;
                    var cekLibur = str.slice(0, 5);
                    if (cekLibur == "LIBUR") {
                        Data += '<button type="button" class="btn btn-primary btn-sm btn-block"><i class="fa fa-bullhorn"></i> Libur</button>';
                    } else {
                        var JudulMateri = full.JudulMateri != null ? "" : "disabled";
                        var ParamCreate = '';
                        var ParamPreview = '';
                        var ParamResetMateri = '';
                        ParamCreate = "BuatSoal('" + full.IdKbmMateri + "')";
                        ParamPreview = "Preview('" + full.IdKbmMateri + "')";
                        ParamResetMateri = "ResetMateriSoal('" + full.IdKbmMateri + "')";
                        Data += '<button type="button" class="btn btn-success btn-sm" onClick="' + ParamCreate + '"><i class="fa fa-plus"></i> Buat</button>&nbsp;&nbsp;';
                        Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamPreview + '" ' + JudulMateri + '><i class="fa fa-eye"></i> Lihat</button>&nbsp;&nbsp;';
                        Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamResetMateri + '" ' + JudulMateri + '><i class="fa fa-times"></i> Reset</button>';
                    }

                    return Data;
                }
            },
            {
                "data": "JudulMateri",
                "render": function (data, type, full, meta) {
                    var str = full.NamaMapel;
                    var cekLibur = str.slice(0, 5);
                    var data = '';
                    if (cekLibur == "LIBUR") {
                        data = 'Tidak ada';
                    } else {
                        data = full.JudulMateri;
                    }
                    return data;
                }
            },
            {
                "data": "NamaKelasParalel"
            },
            {
                "className": 'desktop mobile tablet',
                "data": "NamaMapel"
            },
            {
                "className": 'desktop mobile tablet',
                "data": "Tanggal"
            },
            {
                "className": 'desktop mobile tablet',
                "data": "Hari"
            },
            {
                "data": "JamMulai",
                "render": function (data, type, full, meta) {
                    var data = '';
                    data += full.JamMulai + ' s/d ' + full.JamSelesai;
                    return data;
                }
            },

        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            //console.log(aData.JenisData);
            $(nRow).find('td:eq(0)').css('font-weight', '300');
            $(nRow).find('td:eq(1)').css('font-weight', '300');
            $(nRow).find('td:eq(2)').css('font-weight', '300');
            $(nRow).find('td:eq(3)').css('font-weight', '300');
            $(nRow).find('td:eq(4)').css('font-weight', '300');
            $(nRow).find('td:eq(5)').css('font-weight', '300');
            $(nRow).find('td:eq(6)').css('font-weight', '300');
            var str = aData.NamaMapel;
            var cekLibur = str.slice(0, 5);
            if (cekLibur == "LIBUR") {
                //cell background color
                $(nRow).find('td:eq(0)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(0)').css('color', '#5e5f61');
                $(nRow).find('td:eq(1)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(1)').css('color', '#5e5f61');
                $(nRow).find('td:eq(2)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(2)').css('color', '#5e5f61');
                $(nRow).find('td:eq(3)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(3)').css('color', '#5e5f61');
                $(nRow).find('td:eq(3)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(3)').css('color', '#5e5f61');
                $(nRow).find('td:eq(4)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(4)').css('color', '#5e5f61');
                $(nRow).find('td:eq(5)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(5)').css('color', '#5e5f61');
                $(nRow).find('td:eq(6)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(6)').css('color', '#5e5f61');
            }
        },
        "bDestroy": true
    });
    TabelData.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function GetKelasMapel() {
    $.ajax({
        url: Url.GetKbmByPegawais,
        type: 'GET',
        dataType: 'JSON',
        success: function (res) {
            var data = res.Data;
            if (res.IsSuccess) {
                var htmlKelas = '';
                $.each(data, (i, v) => {
                    var str = v.NamaMapel;
                    var cekLibur = str.slice(0, 5);
                    if (cekLibur != "LIBUR")
                        htmlKelas += '<option value="' + v.IdKbmMateri + '">' + v.NamaKelasParalel + ' - ' + v.NamaMapel + '</option>';
                })
                $('#KelasParalelMassal').html(htmlKelas);
            } else {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

function BuatSoal(Param) {
    var browser = get_browser();
    if (browser.name !== 'Chrome') {
        $('#AlertChromeErr').fadeIn();
    } else {
        $('#AlertChromeErr').fadeOut();
    }
    $("#ModalFormBuatSoal").modal("show");
    $('#myTab a[href="#materix"]').tab('show');
    $('#FormBuatSoal')[0].reset();
    $("#ModalHeaderFormBuatSoal").text("Buat Materi & Soal");
    $("button[name='BtnAddUrlVideo']").attr("onClick", "TambahUrlVideo();");
    $("button[name='BtnAddUploadGambar']").attr("onClick", "TambahUploadGambar();");

    /// reset form ///
    $('#UrlSoalReguler').attr('checked', false);
    $('#FileSoalReguler').attr('checked', false);
    $('#UrlSoal').val('');
    $("#TipeSoalUpload").css("display", "none");
    $("#TipeSoalEmbed").css("display", "none");
    $('#htmlFileSoal').html('');

    $('#UrlTipeSoalRemedial').attr('checked', false);
    $('#FileTipeSoalRemedial').attr('checked', false);
    $('#UrlSoalRemedial').val('');
    $("#TipeSoalRemedialUpload").css("display", "none");
    $("#TipeSoalRemedialEmbed").css("display", "none");
    $('#htmlFileSoalRemedial').html('');

    $('#TipeJawabanSoalFile').attr('checked', false);
    $('#TipeJawabanSoalUrl').attr('checked', false);
    $('#FileTipeJawabanRemedial').attr('checked', false);
    $('#UrlTipeJawabanRemedial').attr('checked', false);
    /// reset form ///

    $('#FileMateri').val('');
    $('#UrlVideo').val('');
    $('#UploadGambar').val('');
    $('#FileSoal').val('');
    $('#FileSoalRemedial').val('');
    CKEDITOR.instances.Deskripsi.setData('');
    CKEDITOR.instances.DeskripsiTugas.setData('');


    if (Param != 0) {
        $('#KelasMassal').hide();
        $('#KelasNonMassal').fadeIn();
        $.ajax({
            url: Url.GetKbmByPegawai + "?IdKbmMateri=" + Param,
            type: 'GET',
            dataType: 'JSON',
            beforeSend: function () {
                ProgressBar("wait")
            },
            success: function (res) {
                ProgressBar("success");
                var data = res.Data;
                if (res.IsSuccess) {


                    //if (data.JudulMateri != null) {
                    //    $('#BtnResetData').show();
                    //    $('#BtnResetData').attr('onclick', 'ResetMateriSoal(' + data.IdKbmMateri + ')');

                    //} else {
                    //    $('#BtnResetData').hide();
                    //}

                    $('#IdKbmMateris', '#FormBuatSoal').val(data.IdKbmMateri);
                    $('#KelasParalel', '#FormBuatSoal').val(data.NamaKelasParalel);
                    $('#Mapel', '#FormBuatSoal').val(data.NamaMapel);
                    $('#Judul', '#FormBuatSoal').val(data.JudulMateri);
                    CKEDITOR.instances.Deskripsi.setData(data.Deskripsi);

                    CKEDITOR.instances.DeskripsiTugas.setData(data.DeskripsiTugas);

                    $('#DataMedia a[href="#form-media"]').tab('show');
                    if ((data.FileKbmMedias).length > 0) {
                        $('#DataMedia').fadeIn();
                        console.log(data.FileKbmMedias)
                        $('#TabelDaftarMedia').DataTable({
                            "paging": true,
                            "searching": true,
                            "ordering": false,
                            "info": false,
                            "pageLength": 10,
                            "lengthChange": true,
                            "processing": true,
                            "data": data.FileKbmMedias,
                            "columns": [{
                                    "render": function (data, type, full, meta) {
                                        var Data = '';
                                        var ParamCreate = "DeleteFileMedia('" + full.IdKbmMedia + "','" + Param + "')";
                                        Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamCreate + '"><i class="fa fa-times"></i></button>&nbsp;&nbsp;';
                                        return Data;
                                    }
                                },
                                {
                                    "render": function (data, type, full, meta) {
                                        var data = '';
                                        if (full.IdJenisPathFile == 1) {
                                            data += '<img src="' + base_url + '/Asset/Files/Elearning/' + full.PathFileUrl + '" class="img-fluid" height="300px" style="cursor:pointer;" />'
                                        } else {
                                            data += '<iframe width="100%" height="300px" class="img-fluid" src="' + full.PathFileUrl + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
                                        }

                                        return data;
                                    }
                                },
                            ],
                            "bDestroy": true
                        });

                    } else {
                        $('#DataMedia').hide();
                    }


                    if (data.SoalReguler != null) {
                        $('#TipeJawabanSoal').val(data.SoalReguler.IdJenisJawaban);

                        if (data.SoalReguler.NamaUrl != null) {
                            $('#UrlSoalReguler').attr('checked', true);
                            $('#FileSoalReguler').attr('checked', false);
                            $('#UrlSoal').val(data.SoalReguler.NamaUrl);
                            $("#TipeSoalUpload").css("display", "none");
                            $("#TipeSoalEmbed").css("display", "block");
                        } else {
                            $('#FileSoalReguler').attr('checked', true);
                            $('#UrlSoalReguler').attr('checked', false);

                            if (data.SoalReguler.NamaFile != null) {
                                $('#htmlFileSoal').html(data.SoalReguler.NamaFile + '&nbsp; <a href="javascript:void(0);" class="text-danger" onclick="DeleteFileSoal(' + data.IdKbmMateri + ',' + data.SoalReguler.IdJenisSoal + ');"><i class="fa fa-times"></i></a> ');
                            }

                            $("#TipeSoalUpload").css("display", "block");
                            $("#TipeSoalEmbed").css("display", "none");
                        }
                    }

                    if (data.SoalRemedial != null) {
                        $('#TipeJawabanRemedial').val(data.SoalRemedial.IdJenisJawaban);

                        if (data.SoalRemedial.NamaUrl != null) {
                            $('#UrlTipeSoalRemedial').attr('checked', true);
                            $('#FileTipeSoalRemedial').attr('checked', false);
                            $('#UrlSoalRemedial').val(data.SoalRemedial.NamaUrl);
                            $("#TipeSoalRemedialUpload").css("display", "none");
                            $("#TipeSoalRemedialEmbed").css("display", "block");
                        } else {
                            $('#FileTipeSoalRemedial').attr('checked', true);
                            $('#UrlTipeSoalRemedial').attr('checked', false);

                            if (data.SoalRemedial.NamaFile != null) {
                                $('#htmlFileSoalRemedial').html(data.SoalRemedial.NamaFile + '&nbsp; <a href="javascript:void(0);" class="text-danger" onclick="DeleteFileSoal(' + data.IdKbmMateri + ',' + data.SoalRemedial.IdJenisSoal + ');"><i class="fa fa-times"></i></a> ');
                            }

                            $("#TipeSoalRemedialUpload").css("display", "block");
                            $("#TipeSoalRemedialEmbed").css("display", "none");
                        }
                    }

                    ///////batasssssssssssssssss
                    if (data.NamaFileMateri != null) {
                        $('#htmlFileMateri').html(data.NamaFileMateri + '&nbsp; <a href="javascript:void(0);" class="text-danger" onclick="DeleteFileMateri(' + data.IdKbmMateri + ');"><i class="fa fa-times"></i></a> ');
                    } else {
                        $('#htmlFileMateri').html('');
                    }

                } else if (!res.IsSuccess) {
                    swal({
                        title: 'Gagal',
                        text: res.ReturnMessage,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            },
            error: function (responserror, a, e) {
                ProgressBar("success");
                swal({
                    title: 'Terjadi Kesalahan',
                    text: JSON.stringify(responserror) + " : " + e,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        })
    } else {
        $('#KelasMassal').fadeIn();
        $('#KelasNonMassal').hide();
        $('#DataMedia a[href="#form-media"]').tab('show');
        $('#DataMedia').hide();
        $('#htmlFileMateri').html("");
        $('#htmlFileSoal').html("");
        $('#htmlFileSoalRemedial').html("");

        GetKelasMapel()
    }

}

function TambahUrlVideo() {
    var HtmlData = "";
    HtmlData += '<div class="form-group form-row" id="InputUrlVideo_' + xInputUrlVideo + '">' +
        '<label class="col-md-12 col-form-label font-weight-bold">Embed Video</label>' +
        '<input type="text" class="col-md-9 form-control" name="UrlVideo[]" placeholder="Ketikkan URL Video Youtube" data-input="wajib" required/>' +
        '<button class="btn btn-danger col-md-3" type="button" onClick="HapusUrlVideo(' + xInputUrlVideo + ');"><i class="fa fa-trash"></i> Hapus</button>' +
        '<div class="invalid-feedback"> Kolom Wajib diisi.</div>' +
        '</div>';
    $(".url-video-wrap").append(HtmlData);
    xInputUrlVideo++;

}

function TambahUploadGambar() {
    var HtmlData = "";
    HtmlData += '<div class="form-group form-row" id="InputUploadGambar_' + xInputUploadGambar + '">' +
        '<label class="col-md-12 col-form-label font-weight-bold">Upload Gambar</label>' +
        '<input type="file" class="col-md-9 form-control" name="UploadGambar[]" placeholder="Pilih Gambar..." data-input="wajib" required />' +
        '<button class="btn btn-danger col-md-3" type="button" onClick="HapusUploadGambar(' + xInputUploadGambar + ');"><i class="fa fa-trash"></i> Hapus</button>' +
        '<div class="invalid-feedback"> Kolom Wajib diisi.</div>' +
        '</div>';
    $(".upload-gambar-wrap").append(HtmlData);
    xInputUploadGambar++;
}

function HapusUrlVideo(IdBtn) {
    $("#InputUrlVideo_" + IdBtn).remove();
    xInputUrlVideo--;
}

function HapusUploadGambar(IdBtn) {
    $("#InputUploadGambar_" + IdBtn).remove();
    xInputUrlVideo--;
}

function ModalMeeting(Param) {
    $('#ModalMeeting').modal('show');

    $.ajax({
        url: Url.GetKbmByPegawai + "?IdKbmMateri=" + Param,
        type: 'GET',
        dataType: 'JSON',
        beforeSend: function () {
            ProgressBar("wait")
        },
        success: function (res) {
            ProgressBar("success");
            var data = res.Data;
            if (res.IsSuccess) {

                $('#IdKbmMateri').val(data.IdKbmMateri);
                $('#NamaMapel', '#FormMeeting').val(data.NamaMapel);
                $('#KelasParalel', '#FormMeeting').val(data.NamaKelasParalel);

            } else if (!res.IsSuccess) {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

function Preview(Param) {
    $.ajax({
        url: Url.GetKbmByPegawai + "?IdKbmMateri=" + Param,
        type: 'GET',
        dataType: 'JSON',
        beforeSend: function () {
            ProgressBar("wait")
        },
        success: function (res) {
            ProgressBar("success");
            $('#myTab a[href="#materi"]').tab('show')
            $('div#preview').fadeIn();
            $('div#data').hide();
            $('div#empty-data').hide();
            $('div#loading-data').hide();

            var data = res.Data;
            console.log('preview -> ', data)

            if (res.IsSuccess) {
                $('#materi_guru').html(data.Guru);
                $('#materi_mapel').html(data.NamaMapel);
                $('#materi_judul').html(data.JudulMateri);
                $('#materi_deskripsi').html(data.Deskripsi);
                var htmlDownloadFileMateri = '';
                if (data.NamaFileMateri != null) {
                    $('#materi_downloadfile').html('<br /><a href="' + base_url + '/Asset/Files/Elearning/' + data.NamaFileMateri + '" class="btn btn-primary">Download File Materi</a><br /><br />');
                } else {
                    $('#materi_downloadfile').html('');
                }
                let htmlGambar = '';
                let htmlVideo = '';
                $.each(data.FileKbmMedias, (i, v) => {
                    if (v.IdJenisPathFile == 1) {
                        var Path = '';
                        Path = "PreviewGambar('" + v.PathFileUrl + "')";

                        htmlGambar += '<div class="col-md-3">';
                        htmlGambar += '<img src="' + base_url + '/Asset/Files/Elearning/' + v.PathFileUrl + '" class="img-fluid" onclick="' + Path + '" style="cursor:pointer;" />'
                        htmlGambar += '</div>';
                    } else if (v.IdJenisPathFile == 2) {
                        var UrlYt = v.PathFileUrl;
                        var myId = getEmbedYt(UrlYt);
                        htmlVideo += '<div class="col-md-3">';
                        htmlVideo += '<iframe width="100%" height="300px" class="img-fluid" src="' + myId + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
                        htmlVideo += '</div>';
                    }
                });
                $('#materi_gambar').html(htmlGambar);
                $('#materi_video').html(htmlVideo);

                $('#soal_deskripsi').html(data.DeskripsiTugas);
                var htmlDownloadSoal = '';
                if (data.SoalReguler != null) {
                    $('#IdJenisSoal').val(data.SoalReguler.IdJenisSoal);
                    if (data.SoalReguler.NamaFile != null) {
                        htmlDownloadSoal += '<a class="btn btn-primary" href="' + base_url + '/Asset/Files/Elearning/' + data.SoalReguler.NamaFile + '" download><i class="fa fa-download"></i> Download Soal</a>&nbsp;&nbsp;  ';
                    } else {
                        var UrlYt = data.SoalReguler.NamaUrl;
                        var myId = getEmbedYt(UrlYt);
                        htmlDownloadSoal += '<iframe width="500px" height="500px" class="img-fluid" src="' + myId + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>&nbsp;&nbsp;  ';
                    }
                } else {
                    htmlDownloadSoal = '<button class="btn btn-danger" type="button"><i class="fa fa-times"></i> Tidak Ada Soal</button>';
                    $('#DivUploadJawabanClose').fadeIn();
                    $('#DivUploadJawabanOpen').hide();
                }

                if (data.SoalRemedial != null) {
                    $('#IdJenisSoal').val(data.SoalRemedial.IdJenisSoal);
                    if (data.SoalRemedial.NamaFile != null) {
                        htmlDownloadSoal += '<a class="btn btn-primary" href="' + base_url + '/Asset/Files/Elearning/' + data.SoalRemedial.NamaFile + '" download><i class="fa fa-download"></i> Download Soal Remedial</a>';
                    } else {
                        var UrlYt = data.SoalRemedial.NamaUrl;
                        var myId = getEmbedYt(UrlYt);
                        htmlDownloadSoal += '<iframe width="500px" height="500px" class="img-fluid" src="' + myId + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                    }
                } else {
                    htmlDownloadSoal = '<button class="btn btn-danger" type="button"><i class="fa fa-times"></i> Tidak Ada Soal Remedial</button>';

                }
                $('#htmlDownloadSoal').html(htmlDownloadSoal);

                //start jawaban in here
                if (data.SoalReguler.IdJenisJawaban == 1) {
                    $('#jawaban_upload').show();
                    $('#jawaban_youtube').hide();
                    $('#jawaban_rubrik').hide();
                    $('#DivUploadJawabanClose').hide();
                    $('#DivUploadJawabanOpen').fadeIn();
                } else if (data.SoalReguler.IdJenisJawaban == 2) {
                    $('#jawaban_upload').hide();
                    $('#jawaban_youtube').show();
                    $('#jawaban_rubrik').hide();
                    $('#DivUploadJawabanClose').hide();
                    $('#DivUploadJawabanOpen').fadeIn();
                } else if (data.SoalReguler.IdJenisJawaban == 3) {
                    $('#jawaban_upload').hide();
                    $('#jawaban_youtube').hide();
                    $('#jawaban_rubrik').show();
                    $('#DivUploadJawabanClose').hide();
                    $('#DivUploadJawabanOpen').fadeIn();
                } else if (data.SoalReguler.IdJenisJawaban == 0) {
                    $('#jawaban_upload').hide();
                    $('#jawaban_youtube').show();
                    $('#jawaban_rubrik').hide();
                    $('#DivUploadJawabanClose').fadeIn();
                    $('#DivUploadJawabanOpen').hide();
                }

                if (data.SoalRemedial.IdJenisJawaban == 1) {
                    $('#jawaban_upload1').show();
                    $('#jawaban_youtube1').hide();
                    $('#jawaban_rubrik1').hide();
                    $('#DivUploadJawabanClose1').hide();
                    $('#DivUploadJawabanOpen1').fadeIn();
                } else if (data.SoalRemedial.IdJenisJawaban == 2) {
                    $('#jawaban_upload1').hide();
                    $('#jawaban_youtube1').show();
                    $('#jawaban_rubrik1').hide();
                    $('#DivUploadJawabanClose1').hide();
                    $('#DivUploadJawabanOpen1').fadeIn();
                } else if (data.SoalRemedial.IdJenisJawaban == 3) {
                    $('#jawaban_upload1').hide();
                    $('#jawaban_youtube1').hide();
                    $('#jawaban_rubrik1').show();
                    $('#DivUploadJawabanClose').hide();
                    $('#DivUploadJawabanOpen').fadeIn();
                } else if (data.SoalRemedial.IdJenisJawaban == 0) {
                    $('#jawaban_upload1').hide();
                    $('#jawaban_youtube1').show();
                    $('#jawaban_rubrik1').hide();
                    $('#DivUploadJawabanClose1').fadeIn();
                    $('#DivUploadJawabanOpen1').hide();
                }

                if (data.MeetingUrl != null) {
                    $('#meeting_ada').show();
                    $('#meeting_empty').hide();
                    $('#meeting_url').html('<iframe width="500px" height="500px" class="img-fluid" src="' + data.MeetingUrl + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
                } else {
                    $('#meeting_empty').show();
                    $('#meeting_ada').hide();
                }

                $('#TabelSiswaProgress').DataTable({
                    "paging": true,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "pageLength": 5,
                    "lengthChange": false,
                    "scrollX": true,
                    "data": data.TugasSiswas,
                    "columns": [{
                            "render": function (data, type, full, meta) {
                                data = meta.row + 1;
                                return data;
                            }
                        },
                        {
                            "data": "NamaSiswa"
                        },
                        {
                            "render": function (data, type, full, meta) {
                                if (full.TugasStatus != "Belum Mengerjakan") {
                                    data = "<span class='badge badge-success'><i class='fa fa-check'></i> Sudah Mengerjakan</span>";
                                } else {
                                    data = "<span class='badge badge-warning'><i class='fa fa-times'></i> Belum Mengerjakan</span>";
                                }
                                return data;
                            }
                        },
                    ],
                    "bDestroy": true
                });

                $('.dataTables_scrollHeadInner, .table-bordered').css('width', 'inherit');

            } else if (!res.IsSuccess) {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

function PreviewGambar(Param) {
    $('#ModalMateriGambar').modal('show');
    $('#htmlMateriGambar').html('<center><img src="' + base_url + '/Asset/Files/Elearning/' + Param + '" class="img-fluid" /></center>');
}

function UpdateMateris() {
    var TypeInput = true;
    $('[data-input="wajib"]', '#FormBuatSoal').each(function (i, v) {
        if ($(this).val() == "") {
            $('#FormBuatSoal').addClass('was-validated');
            TypeInput = false;
        }

    })

    if (TypeInput) {
        var fd = new FormData($('#FormBuatSoal')[0]);
        var formObj = $('#FormBuatSoal').serializeObject();
        $('input[name="UploadGambar[]"][type="file"]').each(function (i, v) {
            fd.append('MediaFiles', $('input[type="file"][name="UploadGambar[]"]')[i].files[0]);
        });

        $('input[name="UrlVideo[]"][type="text"]').each(function (i, v) {
            var url = getEmbedYt(v.value);
            fd.append('MediaUrls[' + i + ']', url);
        });

        fd.append('FileMateri', $('input[type="file"][name="FileMateri"]')[0].files[0]);

        fd.append('FileSoals[0].IdJenisSoal', 1);
        fd.append('FileSoals[0].IdJenisPathFile', formObj.TipeJawabanSoal);
        var UrlSoal = getEmbedYt(formObj.UrlSoal);
        fd.append('FileSoals[0].NamaUrl', UrlSoal);
        fd.append('FileSoals[0].FileSoal', $('input[type="file"][name="FileSoal"]')[0].files[0]);

        fd.append('FileSoals[1].IdJenisSoal', 2);
        fd.append('FileSoals[1].IdJenisPathFile', formObj.TipeJawabanRemedial);
        var UrlSoalRemedial = getEmbedYt(formObj.UrlSoalRemedial);
        fd.append('FileSoals[1].NamaUrl', UrlSoalRemedial);
        fd.append('FileSoals[1].FileSoal', $('input[type="file"][name="FileSoalRemedial"]')[0].files[0]);

        $.ajax({
            url: Url.UpdateMateris,
            method: 'POST',
            processData: false,
            contentType: false,
            data: fd,
            dataType: 'JSON',
            beforeSend: function () {
                ProgressBar("wait");
            },
            success: function (res) {
                ProgressBar("success");
                var data = res.Data;
                if (res.IsSuccess) {
                    swal({
                        title: 'Berhasil',
                        text: 'Anda berhasil memperbarui soal & materi',
                        confirmButtonClass: 'btn-success text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'success'
                    });
                    $('#TabelData').DataTable().ajax.reload();
                    $('#ModalFormBuatSoal').modal('hide');
                } else if (!res.IsSuccess) {
                    swal({
                        title: 'Gagal',
                        text: res.ReturnMessage,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            },
            error: function (responserror, a, e) {
                ProgressBar("success");
                swal({
                    title: 'Terjadi Kesalahan',
                    text: JSON.stringify(responserror) + " : " + e,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        })
    }

}

function Kembali() {
    //history.back() 
    $('#TabelData').DataTable().ajax.reload();
    $('.dataTables_scrollHeadInner, .table-bordered').css('width', 'inherit');

    $('#preview').hide();
    $('#data').fadeIn();
}

function DeleteFileMateri(IdKbmMateri) {
    swal({
            title: "Apakah Anda Yakin ?",
            text: "Anda akan hapus file materi",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                url: Url.DeleteFileMateri + "?IdKbmMateri=" + IdKbmMateri,
                method: "GET",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (res) {
                    ProgressBar("success");
                    if (res.IsSuccess == true) {
                        $('#TabelData').DataTable().ajax.reload();
                        swal({
                            title: 'Sukses',
                            text: "Berhasil Hapus File Materi",
                            confirmButtonClass: 'btn-success text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'success'
                        });
                        $('#htmlFileMateri').html('');
                    } else if (res.IsSuccess == false) {
                        swal({
                            title: 'Gagal',
                            text: res.ReturnMessage,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                },
                error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({
                        title: 'Error :(',
                        text: JSON.stringify(responserror) + " : " + e,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            });
        });
}

function DeleteFileSoal(IdKbmMateri, IdJenisSoal) {
    var tipeSoal = IdJenisSoal == 1 ? "reguler" : "remedial";
    swal({
            title: "Apakah Anda Yakin ?",
            text: "Anda akan hapus file soal " + tipeSoal,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                url: Url.DeleteFileSoal + "?IdKbmMateri=" + IdKbmMateri + "&IdJenisSoal=" + IdJenisSoal,
                method: "GET",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (res) {
                    ProgressBar("success");
                    if (res.IsSuccess == true) {
                        $('#TabelData').DataTable().ajax.reload();
                        swal({
                            title: 'Sukses',
                            text: "Berhasil hapus file soal " + tipeSoal,
                            confirmButtonClass: 'btn-success text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'success'
                        });
                        IdJenisSoal == 1 ? $('#htmlFileSoal').html('') : $('#htmlFileSoalRemedial').html('');
                    } else if (res.IsSuccess == false) {
                        swal({
                            title: 'Gagal',
                            text: res.ReturnMessage,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                },
                error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({
                        title: 'Error :(',
                        text: JSON.stringify(responserror) + " : " + e,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            });
        });
}

function DeleteFileMedia(IdKbmMedia, IdKbmMateri) {
    swal({
            title: "Apakah Anda Yakin ?",
            text: "Anda akan hapus file media ini",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                url: Url.DeleteFileMedia + "?IdKbmMedia=" + IdKbmMedia,
                method: "GET",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (res) {
                    ProgressBar("success");
                    if (res.IsSuccess == true) {
                        $('#TabelData').DataTable().ajax.reload();
                        $("#ModalFormBuatSoal").modal("hide");
                        swal({
                            title: 'Sukses',
                            text: "Berhasil Hapus File Media",
                            confirmButtonClass: 'btn-success text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'success'
                        });
                        setTimeout(() => {
                            BuatSoal(IdKbmMateri);
                        }, 1000);
                    } else if (res.IsSuccess == false) {
                        swal({
                            title: 'Gagal',
                            text: res.ReturnMessage,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                },
                error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({
                        title: 'Error :(',
                        text: JSON.stringify(responserror) + " : " + e,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            });
        });
}

function ResetMateriSoal(IdKbmMateri) {
    swal({
            title: "Apakah Anda Yakin ?",
            text: "Anda akan reset materi ini",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                url: Url.ResetMateriSoal + "?IdKbmMateri=" + IdKbmMateri,
                method: "GET",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (res) {
                    ProgressBar("success");
                    if (res.IsSuccess == true) {
                        $('#ModalFormBuatSoal').modal('hide');
                        $('#TabelData').DataTable().ajax.reload();
                        swal({
                            title: 'Sukses',
                            text: "Berhasil Reset Materi",
                            confirmButtonClass: 'btn-success text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'success'
                        });
                    } else if (res.IsSuccess == false) {
                        swal({
                            title: 'Gagal',
                            text: res.ReturnMessage,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                },
                error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({
                        title: 'Error :(',
                        text: JSON.stringify(responserror) + " : " + e,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            });
        });
}

$("input[type='file']").on("change", function () {
    if (this.files[0].size > 5000000) {
        swal({
            title: 'Gagal',
            text: 'Maksimal ukuran 5MB, silahkan upload ke googledrive, lalu cantumkan url dipenjelasan materi/soal !',
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
        $(this).val('');
    }
});