$(document).ready(function () {

    $.getJSON(base_url + '/api/pegawais/getpegawai', ((res) => {
        console.log(res.Data.IdUnit)
        if (res.Data.IdUnit < 4) {
            ComboGetUnitSklhJs('NonAdmin', res.Data.IdUnit);
        } else {
            // ComboGetUnitSklh('Admin', res.Data.IdUnit);
            ComboGetUnitSklh(function (obj) {
                $("select[name='IdUnit']").html(obj);
            });
        }
    }));



    $("select[name='IdUnit']","#FormFilter").change(function (e) {
        ComboGetKelas(function (obj) {
            $("select[name='IdKelas']").html(obj);
        }, this.value);
        // TabelKelas('Kelas', this.value);
        // $('#htmlTabelKelas').html('Kelas');
        // $('#btnKelas').fadeIn();
        // $('#card-kelas-empty').hide();
        // $('#card-kelas').fadeIn();
    })

    $("select[name='IdKelas']", "#FormFilter").change(function (e) {
        ComboGetKelasParalel(function (obj) {
            $("select[name='IdKelasParalel']").html(obj);
        }, this.value);
        TabelKelas('KelasParalel', this.value);
        $('#htmlTabelKelas').html('Kelas Paralel');
        $('#btnKelas').fadeIn();
        $('#btnKelas').fadeIn();
        $('#card-kelas-empty').hide();
        $('#card-kelas').fadeIn();
    })

    $("input[name='CheckAllIdKelas']").change(function () {
        var status = this.checked;
        $("input[name='ProsesIdKelas']").each(function () {
            this.checked = status;
        });
    });

    $("input[name='ProsesIdKelas']").change(function () {
        if (this.checked == false) {
            $("input[name='CheckAllIdKelas']")[0].checked = false;
        }
        if ($("input[name='ProsesIdKelas']:checked").length == $("input[name='IdKelas']").length) {
            $("input[name='CheckAllIdKelas']")[0].checked = true;
        }
    });
});

function ComboGetUnitSklhJs(Type, IdUnit) {
    var Url = base_url + "/Units/GetUnits";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            if (res.IsSuccess == true) {
                var html_combo = '';
                $.each(res.Data, (i, v) => {
                    if (Type == "NonAdmin") {
                        if (v.IdUnit == IdUnit)
                            html_combo += '<option value="' + v.IdUnit + '">' + v.Nama + '</option>';
                    } else if (Type == "Admin") {
                        html_combo += '<option value="' + v.IdUnit + '">' + v.Nama + '</option>';
                    }


                });
                $("select[name='IdUnit']").append(html_combo);
            } else if (res.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

const UrlService = {
    GetKelass: base_url + '/kelass/GetKelass',
    GetKelasParalels: base_url + '/kelass/GetKelasParalels',
    GetKbmsByKelas: base_url + '/api/elearnings/GetKbmsByKelas',
    GetKbmsByKelasParalelKelompok: base_url + '/api/elearnings/GetKbmsByKelasParalelKelompok',
    DownloadJadwalKbm: base_url + '/api/download/DownloadJadwalKbm',
    SetJadwalKelasParalel: base_url + '/api/elearnings/SetJadwalKelasParalel',
    UploadJadwalKbm: base_url + '/api/elearnings/UploadJadwalKbm',
};

function DownloadJadwalKbm() {
    var IdKelompok = $('#IdKelompok option:selected').val();
    if (IdKelompok != "")
        window.open(UrlService.DownloadJadwalKbm + '?IdKelompok=' + IdKelompok, '_blank');
}

function TabelKelas(Type, Val) {
    if (Type == 'Kelas') var url = UrlService.GetKelass + "?IdUnit=" + Val;
    if (Type == 'KelasParalel') var url = UrlService.GetKelasParalels + "?IdKelas=" + Val;
    $("#TabelKelas tfoot th").each(function () {
        var title = $(this).text();
        $(this).html(
            '<input type="text" class="form-control" placeholder="CARI ' +
            title +
            '" />'
        );
    });
    var TabelKelas = $("#TabelKelas").DataTable({
        paging: false,
        searching: true,
        ordering: true,
        info: true,
        pageLength: 100,
        lengthChange: true,
        processing: true,
        ajax: {
            url: url,
            method: "GET",
            beforeSend: function (xhr) { },
            dataSrc: function (json) {
                if (json.Data == null) {
                    return json;
                } else {
                    return json.Data;
                }
            },
        },
        columns: [{
            "render": function (data, type, full, meta) {
                if (Type == 'Kelas') var Value = full.IdKelas;
                if (Type == 'KelasParalel') var Value = full.IdKelasParalel;
                var BtnLoop = "";
                var Param = "'" + Value + "'";
                var JumlahProses = (Value).length;
                BtnLoop += '<input type="checkbox" name="ProsesIdKelas" value="' + Value + '">&nbsp;&nbsp;';
                for (var ijp = 0; ijp < JumlahProses; ijp++) {
                    var NamaProses = Value[ijp];
                    var paramtobereplaced = {
                        "&": "",
                        " ": ""
                    };
                    var NamaProsesJoin = NamaProses.replace(/&| /gi, function (matched) {
                        return paramtobereplaced[matched];
                    });
                    var NamaProsesFunction = "Btn" + NamaProsesJoin;
                    BtnLoop += '<button type="button" onClick="' + NamaProsesFunction + '(' + Param + ')" class="btn btn-info btn-sm"> ' + NamaProses + '</button>&nbsp;&nbsp;';
                }
                data = BtnLoop;
                return data;
            }
        },
        {
            data: "Nama",
        },
        {
            render: function (data, type, full, meta) {
                if (Type == 'Kelas') var Value = full.IdKelas;
                if (Type == 'KelasParalel') var Value = full.IdKelasParalel;
                var Data = "";
                var ParamEdit = "TabelJadwal('" + Type + "','" + Value + "','" + full.Nama + "')";
                Data += '<button type="button" class="btn btn-primary btn-sm" onClick="' + ParamEdit + '">Lihat <i class="fa fa-arrow-right"></i></button>';
                return Data;
            },
        },
        ],
        bDestroy: true,
    });
    TabelKelas.columns().every(function () {
        var that = this;

        $("input", this.footer()).on("keyup change clear", function () {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function TabelJadwal(Type, Val, Nama) {
    var IdKelompok = $('#IdKelompok option:selected').val();
    if (IdKelompok == "") {
        swal({
            title: 'Gagal',
            text: 'Silahkan pilih salah satu kategori !',
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
        return;
    }
    $('#alertInfoJadwal').fadeIn();
    if (Type == 'Kelas') {
        var url = UrlService.GetKbmsByKelas + "?IdKelompok=" + IdKelompok + "&IdKelas=" + Val;
        $('#htmlInfoJadwal').html('Kelas : ' + Nama + ' &nbsp;&nbsp; Kategori : ' + IdKelompok);
    } else if (Type == 'KelasParalel') {
        var url = UrlService.GetKbmsByKelasParalelKelompok + "?IdKelasParalel=" + Val + "&IdKelompok=" + IdKelompok;
        $('#htmlInfoJadwal').html('Kelas Paralel : ' + Nama + '&nbsp;&nbsp; Kategori : ' + IdKelompok);
    }
    console.log(url)
    $("#TabelJadwal tfoot th").each(function () {
        var title = $(this).text();
        $(this).html(
            '<input type="text" class="form-control" placeholder="CARI ' +
            title +
            '" />'
        );
    });
    var TabelJadwal = $("#TabelJadwal").DataTable({
        paging: false,
        searching: true,
        ordering: false,
        info: true,
        pageLength: 100,
        lengthChange: true,
        processing: true,
        "scrollX": true,
        "scrollY": "230px",
        "scrollCollapse": true,
        ajax: {
            url: url,
            method: "GET",
            beforeSend: function (xhr) { },
            dataSrc: function (json) {
                if (json.Data == null) {
                    $('#card-jadwal-empty').fadeIn();
                    $('#card-jadwal').hide();
                    iziToast.error({
                        title: 'Data Tidak Ditemukan',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    $('#card-jadwal-empty').hide();
                    $('#card-jadwal').fadeIn();
                    return json.Data;
                }
            },
        },
        columns: [{
            data: "KelasParalel",
        },
        {
            data: "NamaMapel",
        },
        {
            data: "NamaGuru",
        },
        {
            data: "Hari",
        },
        {
            data: "JamMulai",
        },
        {
            data: "JamSelesai",
        },
        ],
        bDestroy: true,
    });
    TabelJadwal.columns().every(function () {
        var that = this;

        $("input", this.footer()).on("keyup change clear", function () {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function SetJadwalKelasParalel() {
    var IdKelompok = $('#IdKelompok option:selected').val();
    if (IdKelompok == "") {
        swal({
            title: 'Gagal',
            text: 'Silahkan pilih salah satu kategori !',
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
        return;
    }
    var IdKelasParalels = '';
    var CekValue = '';
    $("input[name='ProsesIdKelas']:checked").each(function (i, item) {
        IdKelasParalels += "&IdKelasParalels=" + parseInt($(item).val());
        CekValue = parseInt($(item).val());
    });
    if (CekValue == "") {
        swal({
            title: 'Gagal',
            text: 'Silahkan pilih salah satu kelas !',
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
        return;
    }
    console.log(UrlService.SetJadwalKelasParalel + '?IdKelompok=' + IdKelompok + '&IdKelasParalels=' + IdKelasParalels);
    $.ajax({
        url: UrlService.SetJadwalKelasParalel + '?IdKelompok=' + IdKelompok + '&IdKelasParalels=' + IdKelasParalels,
        type: 'GET',
        beforeSend: function (xhr) {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            if (res.IsSuccess) {
                swal({
                    title: "Sukses",
                    text: "Anda berhasil setjadwal",
                    confirmButtonClass: "btn-success text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "success",
                });
            } else {
                swal({
                    title: "Gagal",
                    text: res.ReturnMessage,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: "Terjadi Kesalahan",
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: "btn-danger text-white",
                confirmButtonText: "Oke, Mengerti",
                type: "error",
            });
        }
    })
}

function ModalUpload() {
    $('#ModalUploadJadwal').modal('show');
}

function UploadJadwalKbm() {
    var fd = new FormData($("#FormJadwalUpload")[0]);
    fd.append(
        "FileUpload",
        $('input[type="file"][name="FileUpload"]')[0].files[0]
    );

    $.ajax({
        url: UrlService.UploadJadwalKbm,
        type: "POST",
        data: fd,
        processData: false,
        contentType: false,
        dataType: "JSON",
        beforeSend: function (xhr) {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            if (res.IsSuccess) {
                $("#ModalUploadJadwal").modal("hide");
                // $("#TabelJadwal").DataTable().ajax.reload();
                swal({
                    title: "Sukses",
                    text: "Anda berhasil upload jadwal",
                    confirmButtonClass: "btn-success text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "success",
                });
            } else if (!res.IsSuccess) {
                swal({
                    title: "Gagal",
                    text: res.ReturnMessage,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: "Terjadi Kesalahan",
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: "btn-danger text-white",
                confirmButtonText: "Oke, Mengerti",
                type: "error",
            });
        },
    });
}

FilterSettingJadwal();
function FilterSettingJadwal() {
    setTimeout(function () {
        ComboGetUnitSklh(function (obj) {
            console.log(obj);
            $("select[name='IdUnit']", "#FormFilterSettingJadwal").html(obj);
        }, 1);
        StartFilterSettingJadwal();
    }, 1000);
    setTimeout(function () {
        StartFilterSettingJadwal();
    }, 1500);
    
}

function StartFilterSettingJadwal() {
    var formObj = $('#FormFilterSettingJadwal').serializeObject();
    //////// START SECTION TABEL HARI LIBUR ///////////
    $('#TabelSettingJadwal tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelSettingJadwal = $('#TabelSettingJadwal').DataTable({
        "paging": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "pageLength": 100,
        "lengthChange": true,
        "scrollX": true,
        "scrollY": "300px",
        "scrollCollapse": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/api/Elearnings/GetJadwalKbmsUnit?IdUnit=" + parseInt(formObj.IdUnit) + "&Tanggal=" +formObj.Tanggal,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Atur Jadwal',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    $('#alertSettingJadwal').fadeIn();
                    var Periode = (json.Data.Periode).split(' ');
                    var TanggalAwal = indoDate(Periode[1]);
                    var TanggalAkhir = indoDate(Periode[3]);
                    $('#htmlSettingJadwal').html('Periode tanggal ' + TanggalAwal + ' sampai tanggal ' + TanggalAkhir);
                    return json.Data.Jadwals;
                    
                }
            }
        },
        "columns": [
            { "data": "KelasParalel" },
            { "data": "Kelompok" },
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    if (full.Setting == "T") {
                        Data += "<span class='badge badge-danger'><i class='fa fa-close'></i> Belum</span>";
                    } else if (full.Setting == "Y") {
                        Data += "<span class='badge badge-success'><i class='fa fa-check'></i> Sudah</span>";
                    }
                    return Data;
                }
            },
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            //console.log(aData.JenisData);
            $(nRow).find('td:eq(0)').css('font-weight', 'bold');
            $(nRow).find('td:eq(1)').css('font-weight', 'bold');
            $(nRow).find('td:eq(2)').css('font-weight', 'bold');
            $(nRow).find('td:eq(3)').css('font-weight', 'bold');
            $(nRow).find('td:eq(4)').css('font-weight', 'bold');
            if (aData.Setting <= "T") {
                //cell background color
                $(nRow).find('td:eq(0)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(0)').css('color', '#5e5f61');
                $(nRow).find('td:eq(1)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(1)').css('color', '#5e5f61');
                $(nRow).find('td:eq(2)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(2)').css('color', '#5e5f61');
            } else if (aData.Setting == "Y") {
                //cell background color
                $(nRow).find('td:eq(0)').css('background-color', 'white');
                $(nRow).find('td:eq(0)').css('color', 'black');
                $(nRow).find('td:eq(1)').css('background-color', 'white');
                $(nRow).find('td:eq(1)').css('color', 'black');
                $(nRow).find('td:eq(2)').css('background-color', 'white');
                $(nRow).find('td:eq(2)').css('color', 'black');
            }
        },
        "bDestroy": true
    });
    TabelSettingJadwal.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
    //////// START SECTION TABEL HARI LIBUR ///////////

    
}