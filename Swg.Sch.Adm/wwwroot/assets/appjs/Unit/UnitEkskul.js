const UrlSvc = {
    GetUnitEkskuls: `${base_url}/units/GetUnitEkskuls`,
    GetUnitEkskul: `${base_url}/units/GetUnitEkskul`,
    UnitEkskulAdd: `${base_url}/units/UnitEkskulAdd`,
    UnitEkskulEdit: `${base_url}/units/UnitEkskulEdit`,
    UnitEkskulReject: `${base_url}/units/UnitEkskulReject`,
    UnitEkskulDelete: `${base_url}/units/UnitEkskulDelete`,
}

$(document).ready((e) => {
    DataEskuls()
})

const DataEskuls = () => {
    var res = initAjax(`${UrlSvc.GetUnitEkskuls}`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        $('#TabelUnitEkskul tfoot th').each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
        });
        var TabelData = $('#TabelUnitEkskul').DataTable({
            "paging": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "pageLength": 10,
            "lengthChange": true,
            "scrollX": true,
            "processing": true,
            "data": res.Data,
            "columns": [{
                    "render": function (data, type, full, meta) {
                        var Data = "";
                        var ParamEdit = "ShowModal('Edit','" + full.IdUnitEkskul + "')";
                        var ParamPreview = "PreviewData('" + full.IdUnitEkskul + "')";
                        var ParamHapus = "HapusEkskul('" + full.IdUnitEkskul + "','" + cekQuotes(full.Judul) + "')";
                        var ParamReject = "RejectEkskul('" + full.IdUnitEkskul + "','" + cekQuotes(full.Judul) + "')";
                        if(full.ActionButton[0].ButtonEdit){
                            Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
                            Data += '&nbsp;&nbsp;&nbsp;';
                        }
                        Data += '<button type="button" class="btn btn-primary btn-sm" onClick="' + ParamPreview + '"><i class="fa fa-eye"></i> Preview</button>';
                        Data += '&nbsp;&nbsp;&nbsp;';
                        if(full.ActionButton[0].ButtonDelete){
                            Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                            Data += '&nbsp;&nbsp;&nbsp;';
                        }
                        if(full.ActionButton[0].ButtonReject){
                            Data += '<button type="button" class="btn btn-warning btn-sm" onClick="' + ParamReject + '"><i class="fa fa-close"></i> Reject</button>';
                        }
                        return Data;
                    }
                },
                {
                    "data": "IdUnitEkskul"
                },
                {
                    "data": "Unit"
                },
                {
                    "render": function (data, type, full, meta) {
                        var OnError = "this.style.display='none'";
                        var Data = '<img onerror="' + OnError + '" src="' + base_url + '/Asset/Files/Ekskul/Unit/' + full.FileImage + '" style="height:50px;object-fit:cover;object-position:center;">';

                        return Data;
                    }
                },
                {
                    "render": function (data, type, full, meta) {
                        var Data = '';
                        Data = '<code>' + full.Judul + '</code>';
                        return Data;
                    }
                },
                {
                    "render": function (data, type, full, meta) {
                        var Data = '';
                        if (full.Status === 'Baru Dibuat') {
                            Data = '<span class="badge badge-warning">' + full.Status + '</span>';
                        } else if (full.Status === 'Disetujui') {
                            Data = '<span class="badge badge-info">' + full.Status + '</span>';
                        } else if (full.Status === 'Publish') {
                            Data = '<span class="badge badge-success">' + full.Status + '</span>';
                        }
                        return Data;
                    }
                },
            ],
            "bDestroy": true
        });
        TabelData.columns().every(function () {
            var that = this;
    
            $('input', this.footer()).on('keyup change clear', function () {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });
        $(".dataTables_filter").css("display", "none");
        $(".dataTables_length").css("display", "none");
    } else {
    }
}

const DataEskul = (id) => {
    var res = initAjax(`${UrlSvc.GetUnitEkskul}?IdUnitEkskul=${id}`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        $("input[name='IdUnitEkskul']", "#FormUnitEkskul").val(res.Data.IdUnitEkskul).attr("readonly", true);
        $("input[name='Judul']", "#FormUnitEkskul").val(res.Data.Judul);
        $("textarea[name='Deskripsi']", "#FormUnitEkskul").val(res.Data.Deskripsi);
        CKEDITOR.instances.Deskripsi.setData(res.Data.Deskripsi);
        $("#ThumbnailImageEkskul").removeAttr("src").attr("src", base_url + "/Asset/Files/Ekskul/Unit/" + res.Data.FileImage);
        $('#ModalUnitEkskul').modal('show')
    } else {
        iziToast.error({
            title: 'Gagal',
            message: res.ReturnMessage,
            position: 'topRight'
        });
    }
}

const ShowModal = (isAction, id) => {
    if (isAction === 'Add') {
        $('#BtnSaveUnitEkskul').attr('onClick', 'SaveEkskul("Add")')
        $('#ModalUnitEkskul').modal('show')
    } else {
        DataEskul(id)
        $('#BtnSaveUnitEkskul').attr('onClick', 'SaveEkskul("Edit")')
    }
}

const SaveEkskul = (Tipe) => {
    var form = new FormData($('#FormUnitEkskul')[0]);
    if (Tipe == "Add") {
        var Url = UrlSvc.UnitEkskulAdd;
    } else if (Tipe == "Edit") {
        var Url = UrlSvc.UnitEkskulEdit;
    }

    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        data: form,
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            if (res.IsSuccess) {
                $('#FormUnitEkskul')[0].reset();
                $("#ModalUnitEkskul").modal("hide");
                DataEskuls()
                iziToast.success({
                    title: 'Berhasil Menyimpan Ekskul Unit',
                    message: res.ReturnMessage,
                    position: 'topRight'
                });
            } else {
                iziToast.error({
                    title: 'Gagal Menyimpan Ekskul Unit',
                    message: res.ReturnMessage,
                    position: 'topRight'
                });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

const RejectEkskul = (id) => {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan me-reject data Ekskul Unit ini",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: true
    },
        function () {
            var res = initAjax(`${UrlSvc.UnitEkskulReject}?IdUnitEkskul=${id}`);
            if (res) ProgressBar("success");
            if (res.IsSuccess) {
                DataEskuls()
                iziToast.success({
                    title: 'Berhasil Me-Reject Ekskul Unit',
                    message: res.ReturnMessage,
                    position: 'topRight'
                });
            } else {
                iziToast.error({
                    title: 'Gagal Me-Reject Ekskul Unit',
                    message: res.ReturnMessage,
                    position: 'topRight'
                });

            }
        });
}

const HapusEkskul = (id) => {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan hapus data Ekskul Unit ini",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: true
    },
        function () {
            var res = initAjax(`${UrlSvc.UnitEkskulDelete}?IdUnitEkskul=${id}`);
            if (res) ProgressBar("success");
            if (res.IsSuccess) {
                DataEskuls()
                iziToast.success({
                    title: 'Berhasil Hapus Ekskul Unit',
                    message: res.ReturnMessage,
                    position: 'topRight'
                });
            } else {
                iziToast.error({
                    title: 'Gagal Hapus Ekskul Unit',
                    message: res.ReturnMessage,
                    position: 'topRight'
                });

            }
        });
}

const PreviewData = (id) => {
    var res = initAjax(`${UrlSvc.GetUnitEkskul}?IdUnitEkskul=${id}`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        $(".HeadingPreview").html(res.Data.Judul);
        $(".BodyPreview").html(res.Data.Keterangan);
        $(".ImagePreview").removeAttr("src").attr("src", base_url + "/Asset/Files/Ekskul/Unit/" + res.Data.FileImage);
        $("#ModalPreviewUnitEkskul").modal("show");
    } else {
        iziToast.error({
            title: 'Gagal',
            message: res.ReturnMessage,
            position: 'topRight'
        });
    }
}