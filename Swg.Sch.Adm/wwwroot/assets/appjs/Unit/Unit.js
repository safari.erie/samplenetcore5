﻿/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 15052020
================================================================================================================= */
// CKEDITOR.replace( document.querySelector( '.ckeditor-kurikulum' ), {
//     toolbar: [
//         { name: 'document', items: [ 'NewPage', 'Preview' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
//         { name: 'basicstyles', items: [ 'Bold', 'Italic' ] },
//         { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] } 
//     ]
// });
// CKEDITOR.replace( document.querySelector( '.ckeditor-alamat' ), {
//     toolbar: [
//         { name: 'document', items: [ 'NewPage', 'Preview' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
//         { name: 'basicstyles', items: [ 'Bold', 'Italic' ] },
//         { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] } 
//     ]
// });
// CKEDITOR.replace( document.querySelector( '.ckeditor-profile' ), {
//     toolbar: [
//         { name: 'document', items: [ 'NewPage', 'Preview' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
//         { name: 'basicstyles', items: [ 'Bold', 'Italic' ] },
//         { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] } 
//     ]
// });
// CKEDITOR.replace( document.querySelector( '.ckeditor-visimisi' ), {
//     toolbar: [
//         { name: 'document', items: [ 'NewPage', 'Preview' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
//         { name: 'basicstyles', items: [ 'Bold', 'Italic' ] },
//         { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] } 
//     ]
// });
//////// START SECTION Tabel Unit ///////////
$('#TabelUnit tfoot th').each(function () {
    var title = $(this).text();
    $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
});
var TabelUnit = $('#TabelUnit').DataTable({
    "paging": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "pageLength": 10,
    "lengthChange": true,
    "scrollX": true,
    "processing": true,
    "ajax": {
        "url": base_url + "/Units/GetUnits",
        "method": 'GET',
        "beforeSend": function (xhr) {
        },
        "dataSrc": function (json) {
            if (json.Data == null) {
                iziToast.error({
                    title: 'Gagal Menampilkan Data Tabel Unit',
                    message: json.ReturnMessage,
                    position: 'topRight'
                });
                return json;
            } else {
                // iziToast.success({
                //     title: 'Berhasil Menampilkan Data Tabel Unit',
                //     message: json.ReturnMessage,
                //     position: 'topRight'
                // });
                return json.Data;
            }
        }
    },
    "columns": [
        {
            "render": function (data, type, full, meta) {
                var Data = "";
                var ParamEdit = "EditData('" + full.IdUnit + "')";
                Data += '<button type="button" class="btn btn-primary btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
                return Data;
            }
        },
        { "data": "Nama" },
        {
            "render": function (data, type, full, meta) {
                var Data = "";
                Data += `<img src="/Asset/Files/Appl/Image/${full.Logo}" style="width:30px;height:30px;object-fit:cover;" onerror="imgError(this);" class="rounded-circle mr-1" />`;
                return Data;
            }
        },
        { "data": "Nspn" },
        { "data": "Alamat" },
        { "data": "NoTelepon" },
    ],
    "bDestroy": true
});
TabelUnit.columns().every(function () {
    var that = this;

    $('input', this.footer()).on('keyup change clear', function () {
        if (that.search() !== this.value) {
            that
                .search(this.value)
                .draw();
        }
    });
});
$(".dataTables_filter").css("display", "none");
$(".dataTables_length").css("display", "none");
//////// START SECTION Tabel Unit ///////////

function EditData(IdUnit) {
    $("#ModalUnit").modal("show");
    $('#FormUnit')[0].reset();
    $("#BtnSaveUnit").attr("onClick", "SaveData('EditData');");
    var Url = base_url + "/Units/GetUnit?IdUnit=" + IdUnit;
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var ResponseData = responsesuccess.Data;
                $(".modal-title").html("Edit Unit (" + ResponseData.Nama + ")");

                $("input[name='IdUnit']", "#FormUnit").val(ResponseData.IdUnit);
                $("input[name='Nama']", "#FormUnit").val(ResponseData.Nama);
                $("input[name='NamaSingkat']", "#FormUnit").val(ResponseData.NamaSingkat);
                $("input[name='KodePos']", "#FormUnit").val(ResponseData.KodePos);
                $("input[name='Desa']", "#FormUnit").val(ResponseData.Desa);
                $("input[name='Kecamatan']", "#FormUnit").val(ResponseData.Kecamatan);
                $("input[name='Kabupaten']", "#FormUnit").val(ResponseData.Kabupaten);
                $("input[name='Provinsi']", "#FormUnit").val(ResponseData.Provinsi);
                $("input[name='NoTelepon']", "#FormUnit").val(ResponseData.NoTelepon);
                $("input[name='NoFaximili']", "#FormUnit").val(ResponseData.NoFaximili);
                $("input[name='Email']", "#FormUnit").val(ResponseData.Email);
                $("input[name='Web']", "#FormUnit").val(ResponseData.Web);
                $("input[name='Moto']", "#FormUnit").val(ResponseData.Moto);

                $("input[name='Nspn']", "#FormUnit").val(ResponseData.Nspn);
                $("input[name='WaktuPenyelenggaraan']", "#FormUnit").val(ResponseData.WaktuPenyelenggaraan);
                $("input[name='Naungan']", "#FormUnit").val(ResponseData.Naungan);
                $("input[name='NoSkPendirian']", "#FormUnit").val(ResponseData.NoSkPendirian);
                $("input[name='TanggalSkPendirian']", "#FormUnit").val(ResponseData.TanggalSkPendirian);
                $("input[name='NoSkOperasional']", "#FormUnit").val(ResponseData.NoSkOperasional);
                $("input[name='TanggalSkOperasional']", "#FormUnit").val(ResponseData.TanggalSkOperasional);
                $("input[name='JenisAkreditasi']", "#FormUnit").val(ResponseData.JenisAkreditasi);
                $("input[name='NoSkAkreditasi']", "#FormUnit").val(ResponseData.NoSkAkreditasi);
                $("input[name='TanggalSkAkreditasi']", "#FormUnit").val(ResponseData.TanggalSkAkreditasi);
                $("input[name='NoSertifikatIso']", "#FormUnit").val(ResponseData.NoSertifikatIso);
                $("input[name='NamaYayasan']", "#FormUnit").val(ResponseData.NamaYayasan);
                $("input[name='LuasTanah']", "#FormUnit").val(ResponseData.LuasTanah);
                $("input[name='LuasBangunan']", "#FormUnit").val(ResponseData.LuasBangunan);
                $("input[name='ProviderInternet']", "#FormUnit").val(ResponseData.ProviderInternet);
                $("input[name='BandwidthInternet']", "#FormUnit").val(ResponseData.BandwidthInternet);
                $("input[name='SumberListrik']", "#FormUnit").val(ResponseData.SumberListrik);
                $("input[name='DayaListrik']", "#FormUnit").val(ResponseData.DayaListrik);
                $("input[name='SliderJudul']", "#FormUnit").val(ResponseData.SliderJudul);
                $("input[name='SliderKeterangan']", "#FormUnit").val(ResponseData.SliderKeterangan);
                $("textarea[name='Alamat']", "#FormUnit").val(ResponseData.Alamat);
                // CKEDITOR.instances.Kurikulum.setData(ResponseData.Kurikulum);
                // CKEDITOR.instances.Profile.setData(ResponseData.Profile);
                $("textarea[name='Kurikulum']", "#FormUnit").summernote('code',ResponseData.Kurikulum);
                $("textarea[name='Profile']", "#FormUnit").summernote('code',ResponseData.Profile);
                $("textarea[name='VisiMisi']", "#FormUnit").summernote('code',ResponseData.VisiMisi);


                $("#ThumbnailImageLogo").removeAttr("src").attr("src", base_url + "/Asset/Files/Appl/Image/" + ResponseData.Logo);
                $("#ThumbnailSliderBanner").removeAttr("src").attr("src", base_url + "/Asset/Files/Appl/Image/" + ResponseData.SliderBanner);
                $("#ThumbnailSliderImage").removeAttr("src").attr("src", base_url + "/Asset/Files/Appl/Image/" + ResponseData.SliderImage);
                $("#ThumbnailThumbnail").removeAttr("src").attr("src", base_url + "/Asset/Files/Appl/Image/" + ResponseData.Thumbnail);
                $("#ThumbnailThumbnailBesar").removeAttr("src").attr("src", base_url + "/Asset/Files/Appl/Image/" + ResponseData.ThumbnailBesar);
                $("#ThumbnailStrukturOrganisasi").removeAttr("src").attr("src", base_url + "/Asset/Files/Appl/Image/" + ResponseData.StrukturOrganisasi);

                ComboGetSekolah(function (obj) {
                    $("select[name='IdSekolah']").html(obj);
                }, ResponseData.IdSekolah);

                ComboGetStatusData(function (obj) {
                    $("select[name='StatusUnit']").html(obj);
                }, ResponseData.StatusUnit);
            } else {
                swal({ title: 'Gagal :(', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function SaveData(Tipe) {
    var formObj = $('#FormUnit').serializeObject();
    var form = new FormData($('#FormUnit')[0]);
    if (Tipe == "EditData") {
        var Url = base_url + "/Units/UnitEdit";
        var TextAlert = "Berhasil Edit Data Unit";
    }
    // form.append('Kurikulum', CKEDITOR.instances.Kurikulum.getData())

    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        data: form,
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $('#FormUnit')[0].reset();
                $("#ModalUnit").modal("hide");
                TabelUnit.ajax.reload();
                swal({ title: 'Berhasil Menyimpan Unit', text: TextAlert, confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal Menyimpan Unit', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

