﻿/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 15052020
================================================================================================================= */
$("select[name='IdJenisGaleri']").change(function () {
  if (this.value == 1) {
    $("#JenisImage").removeAttr("style");
    $("#JenisVideo").css("display", "none");
    $("#htmlNamaUpload").html("File Gambar");
  } else if (this.value == 2) {
    $("#JenisVideo").removeAttr("style");
    $("#JenisImage").removeAttr("style");
    //$("#JenisImage").css("display", "none");
    $("#htmlNamaUpload").html("File Thumbnail");
  }
});

//////// START SECTION Tabel Galeri ///////////
$("#TabelUnitGaleri tfoot th").each(function () {
  var title = $(this).text();
  $(this).html(
    '<input type="text" class="form-control" placeholder="Cari ' +
      title +
      '" />'
  );
});
var TabelUnitGaleri = $("#TabelUnitGaleri").DataTable({
  paging: true,
  searching: true,
  ordering: true,
  info: true,
  pageLength: 10,
  lengthChange: true,
  scrollX: true,
  processing: true,
  ajax: {
    url: base_url + "/Units/GetUnitGaleris",
    method: "GET",
    beforeSend: function (xhr) {},
    dataSrc: function (json) {
      if (json.Data == null) {
        if (json.ReturnMessage != "data tidak ada") {
          iziToast.error({
            title: "Gagal Menampilkan Data Tabel Galeri Unit",
            message: json.ReturnMessage,
            position: "topRight",
          });
        }
        return json;
      } else {
        return json.Data;
      }
    },
  },
  columns: [
    {
      render: function (data, type, full, meta) {
        var Data = "";
        var ParamEdit = "EditData('" + full.IdUnitGaleri + "')";
        var ParamHapus =
          "HapusData('" + full.IdUnitGaleri + "','" + full.Judul + "')";
        Data +=
          '<button type="button" class="btn btn-info btn-sm" onClick="' +
          ParamEdit +
          '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
        Data += "&nbsp;&nbsp;&nbsp;";
        Data +=
          '<button type="button" class="btn btn-danger btn-sm" onClick="' +
          ParamHapus +
          '"><i class="fa fa-trash"></i> Hapus</button>';
        return Data;
      },
    },
    { data: "Unit" },
    { data: "Judul" },
    {
      render: function (data, type, full, meta) {
        if (full.JenisGaleri == "Video") {
          return `<span class="badge badge-primary">Video</span>`;
        } else {
          return `<span class="badge badge-info">Gambar</span>`;
        }
      },
    },
    // { data: "Keterangan" },
    {
      render: function (data, type, full, meta) {
        var OnError = "this.style.display='none'";
        var Data =
          '<img onerror="' +
          OnError +
          '" src="' +
          base_url +
          "/Asset/Files/Galeri/Unit/" +
          full.FileUrl +
          '" style="height:100px;object-fit:cover;object-position:center;">';

        return Data;
      },
    },
    {
      render: function (data, type, full, meta) {
        if (full.JenisGaleri == "Video") {
          return full.UrlVideo;
        } else {
          return null;
        }
      },
    },
  ],
  bDestroy: true,
});
TabelUnitGaleri.columns().every(function () {
  var that = this;

  $("input", this.footer()).on("keyup change clear", function () {
    if (that.search() !== this.value) {
      that.search(this.value).draw();
    }
  });
});
$(".dataTables_filter").css("display", "none");
$(".dataTables_length").css("display", "none");
//////// START SECTION Tabel Galeri ///////////

function TambahData() {
  $("#ModalUnitGaleri").modal("show");
  $("#FormUnitGaleri")[0].reset();
  $("#BtnSaveUnitGaleri").attr("onClick", "SaveData('TambahData');");
  $(".modal-title").html("Form Tambah Galeri Unit");
  ComboGetJenisGaleri(function (obj) {
    $("select[name='IdJenisGaleri']").html(obj);
  });
  $("#ThumbnailImage").hide();
}

function EditData(IdUnitGaleri) {
  $("#ModalUnitGaleri").modal("show");
  $("#htmlNamaUpload").html("File Upload");
  $("#ThumbnailImage").fadeIn();
  $("#FormUnitGaleri")[0].reset();
  $("#BtnSaveUnitGaleri").attr("onClick", "SaveData('EditData');");
  $(".modal-title").html("Form Edit Galeri Unit");
  var Url = base_url + "/Units/GetUnitGaleri?IdUnitGaleri=" + IdUnitGaleri;
  $.ajax({
    url: Url,
    type: "GET",
    dataType: "json",
    beforeSend: function (beforesend) {
      ProgressBar("wait");
    },
    success: function (responsesuccess) {
      ProgressBar("success");
      if (responsesuccess.IsSuccess == true) {
        var ResponseData = responsesuccess.Data;
        $("input[name='IdUnitGaleri']", "#FormUnitGaleri")
          .val(ResponseData.IdUnitGaleri)
          .attr("readonly", true);
        $("input[name='Judul']", "#FormUnitGaleri").val(ResponseData.Judul);
        $("textarea[name='Keterangan']", "#FormUnitGaleri").val(
          ResponseData.Keterangan
        );
        CKEDITOR.instances.Keterangan.setData(ResponseData.Keterangan);

        if (ResponseData.IdJenisGaleri == 1) {
          $("#JenisImage").removeAttr("style");
          $("#JenisVideo").css("display", "none");
          $("#ThumbnailImageUG")
            .removeAttr("src")
            .attr(
              "src",
              base_url + "/Asset/Files/Galeri/Unit/" + ResponseData.FileUrl
            );
        } else if (ResponseData.IdJenisGaleri == 2) {
          $("input[name='UrlVideo']", "#FormUnitGaleri").val(
            ResponseData.UrlVideo
          );
          $("#JenisVideo").removeAttr("style");
          $("#JenisImage").css("display", "none");
        }

        ComboGetJenisGaleri(function (obj) {
          $("select[name='IdJenisGaleri']").html(obj);
        }, ResponseData.IdJenisGaleri);
      } else {
        swal({
          title: "Gagal :(",
          text: responsesuccess.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function SaveData(Tipe) {
  var form = new FormData($("#FormUnitGaleri")[0]);
  if (Tipe == "TambahData") {
    var Url = base_url + "/Units/UnitGaleriAdd";
    var TextAlert = "Berhasil Tambah Data Galeri";
  } else if (Tipe == "EditData") {
    var Url = base_url + "/Units/UnitGaleriEdit";
    var TextAlert = "Berhasil Edit Data Galeri";
  }

  $.ajax({
    url: Url,
    method: "POST",
    dataType: "json",
    data: form,
    contentType: false,
    cache: true,
    processData: false,
    beforeSend: function (before) {
      ProgressBar("wait");
    },
    success: function (responsesave) {
      ProgressBar("success");
      if (responsesave.IsSuccess == true) {
        $("#FormUnitGaleri")[0].reset();
        $("#ModalUnitGaleri").modal("hide");
        TabelUnitGaleri.ajax.reload();
        swal({
          title: "Berhasil Menyimpan Galeri",
          text: responsesave.ReturnMessage,
          confirmButtonClass: "btn-success text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "success",
        });
      } else if (responsesave.IsSuccess == false) {
        swal({
          title: "Gagal Menyimpan Galeri",
          text: responsesave.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function HapusData(IdUnitGaleri, Judul) {
  swal(
    {
      title: "Apakah Anda Yakin ?",
      text: "Anda akan menghapus data Galeri " + Judul,
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-success",
      confirmButtonText: "Ya, Saya Yakin!",
      cancelButtonClass: "btn-danger",
      cancelButtonText: "Tidak, Batalkan!",
      closeOnConfirm: true,
    },
    function () {
      var Url =
        base_url + "/Units/UnitGaleriDelete?IdUnitGaleri=" + IdUnitGaleri;
      $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        headers: {
          "Content-Type": "application/json",
        },
        beforeSend: function (before) {
          ProgressBar("wait");
        },
        success: function (responsesave) {
          ProgressBar("success");
          if (responsesave.IsSuccess == true) {
            TabelUnitGaleri.ajax.reload();
            iziToast.success({
              title: "Berhasil Menghapus Galeri Unit",
              message: responsesave.ReturnMessage,
              position: "topRight",
            });
          } else if (responsesave.IsSuccess == false) {
            iziToast.error({
              title: "Gagal Menghapus Galeri Unit",
              message: responsesave.ReturnMessage,
              position: "topRight",
            });
          }
        },
        error: function (responserror, a, e) {
          ProgressBar("success");
          swal({
            title: "Error :(",
            text: JSON.stringify(responserror) + " : " + e,
            confirmButtonClass: "btn-danger text-white",
            confirmButtonText: "Oke, Mengerti",
            type: "error",
          });
        },
      });
    }
  );
}
