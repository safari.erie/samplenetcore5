﻿/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 15052020
================================================================================================================= */
CKFinder.setupCKEditor();
CKEDITOR.replace('ckeditor', {
    filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
    filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
    filebrowserUploadUrl: '/ckfinder/connector?command=QuickUpload&type=Files&currentFolder=/archive/',
    filebrowserImageUploadUrl: '/ckfinder/connector?command=QuickUpload&type=Images&currentFolder=/cars/'
});
//////// START SECTION Tabel Kegiatan Unit ///////////
$('#TabelUnitKegiatan tfoot th').each(function () {
    var title = $(this).text();
    $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
});
var TabelUnitKegiatan = $('#TabelUnitKegiatan').DataTable({
    "paging": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "pageLength": 10,
    "lengthChange": true,
    "scrollX": true,
    "processing": true,
    "ajax": {
        "url": base_url + "/Units/GetUnitKegiatans",
        "method": 'GET',
        "beforeSend": function (xhr) {
        },
        "dataSrc": function (json) {
            if (json.Data == null) {
                if (json.ReturnMessage != "data tidak ada") {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Kegiatan Unit',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                }
                return json;
            } else {
                return json.Data;
            }
        }
    },
    columnDefs: [
        { targets: [0], width: "auto", visible: true },
        { targets: [1], width: "auto", visible: true },
        { targets: [2], width: "auto", visible: true },
        { targets: [3], width: "auto", visible: true },
        { targets: [4], width: "auto", visible: true },
        { targets: [5], width: "auto", visible: true },
        { targets: [6], width: "auto", visible: true },
    ],
    "columns": [
        {
            "data":"ActionButton",
            "render": function (data, type, full, meta) {
                var Data = "";
                var ParamEdit = "EditData('" + full.IdUnitKegiatan + "')";
                var ParamPreview = "PreviewData('" + full.IdUnitKegiatan + "')";
                var ParamHapus = "HapusData('" + full.IdUnitKegiatan + "','" + cekQuotes(full.Judul) + "')";
                var ParamReject = "RejectData('" + full.IdUnitKegiatan + "','" + cekQuotes(full.Judul) + "')";
                if(full.ActionButton[0].ButtonEdit){
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
                    Data += '&nbsp;&nbsp;&nbsp;';
                }
                Data += '<button type="button" class="btn btn-primary btn-sm" onClick="' + ParamPreview + '"><i class="fa fa-eye"></i> Preview</button>';
                Data += '&nbsp;&nbsp;&nbsp;';
                if(full.ActionButton[0].ButtonDelete){
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                    Data += '&nbsp;&nbsp;&nbsp;';
                }
                if(full.ActionButton[0].ButtonReject){
                    Data += '<button type="button" class="btn btn-warning btn-sm" onClick="' + ParamReject + '"><i class="fa fa-close"></i> Reject</button>';
                }
                return Data;
            }
        },
        { "data": "Unit" },
        { "data": "Judul" },
        {
            "render": function (data, type, full, meta) {
                var Data = '<span class="badge badge-info">' + full.NamaJenis + '</span>';
                return Data;
            }
        },
        {
            "render": function (data, type, full, meta) {
                var Data = '';
                if (full.Status == 99) {
                    Data = '<span class="badge badge-danger">' + full.NamaStatus + '</span>';
                } else {
                    Data = '<span class="badge badge-success">' + full.NamaStatus + '</span>';
                }
                return Data;
            }
        },
        {
            "render": function (data, type, full, meta) {
                var OnError = "this.style.display='none'";
                var Data = '<img onerror="' + OnError + '" src="' + base_url + '/Asset/Files/Kegiatan/Unit/' + full.FileImage + '" style="height:100px;object-fit:cover;object-position:center;">';

                return Data;
            }
        },
        { "data": "Url" },
    ],
    "bDestroy": true
});
TabelUnitKegiatan.columns().every(function () {
    var that = this;

    $('input', this.footer()).on('keyup change clear', function () {
        if (that.search() !== this.value) {
            that
                .search(this.value)
                .draw();
        }
    });
});
$(".dataTables_filter").css("display", "none");
$(".dataTables_length").css("display", "none");
//////// START SECTION Tabel Kegiatan Unit ///////////

$("input[name='Judul']","#FormUnitKegiatan").keyup(function () {
    $("input[name='Url']", "#FormUnitKegiatan").val(regexUrl($("input[name='Judul']", "#FormUnitKegiatan").val()));
});

function TambahData() {
    $("#ModalUnitKegiatan").modal("show");
    $('#FormUnitKegiatan')[0].reset();
    $("#BtnSaveUnitKegiatan").attr("onClick", "SaveData('TambahData');");
    $(".modal-title").html("Tambah Kegiatan Unit");
    CKEDITOR.instances.Keterangan.setData('');
    $("#ThumbnailImageUK").removeAttr("src");
    ComboGetJenisDataKontenUnit(function (obj) {
        $("select[name='Jenis']").html(obj);
    },0);
}

function PreviewData(IdUnitKegiatan) {
    $("#ModalPreviewUnitKegiatan").modal("show");
    $(".modal-title").html("Preview Kegiatan Unit");
    var Url = base_url + "/Units/GetUnitKegiatan?IdUnitKegiatan=" + IdUnitKegiatan;
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var ResponseData = responsesuccess.Data;
                $(".HeadingPreview").html(ResponseData.Judul);
                $(".BodyPreview").html(ResponseData.Keterangan);
                $(".ImagePreview").removeAttr("src").attr("src", base_url + "/Asset/Files/Kegiatan/Unit/" + ResponseData.FileImage);
            } else {
                swal({ title: 'Gagal :(', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function EditData(IdUnitKegiatan) {
    $("#ModalUnitKegiatan").modal("show");
    $('#FormUnitKegiatan')[0].reset();
    $("#BtnSaveUnitKegiatan").attr("onClick", "SaveData('EditData');");
    $(".modal-title").html("Edit Kegiatan Unit");
    var Url = base_url + "/Units/GetUnitKegiatan?IdUnitKegiatan=" + IdUnitKegiatan;
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var ResponseData = responsesuccess.Data;
                ComboGetJenisDataKontenUnit(function (obj) {
                    $("select[name='Jenis']").html(obj);
                }, ResponseData.Jenis);
                $("input[name='IdUnitKegiatan']", "#FormUnitKegiatan").val(ResponseData.IdUnitKegiatan).attr("readonly", true);
                $("input[name='Judul']", "#FormUnitKegiatan").val(ResponseData.Judul);
                $("input[name='Url']", "#FormUnitKegiatan").val(ResponseData.Url);
                $("textarea[name='Keterangan']", "#FormUnitKegiatan").val(ResponseData.Keterangan);
                CKEDITOR.instances.Keterangan.setData(ResponseData.Keterangan);
                $("#ThumbnailImageUK").removeAttr("src").attr("src", base_url + "/Asset/Files/Kegiatan/Unit/" + ResponseData.FileImage);
            } else {
                swal({ title: 'Gagal :(', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function SaveData(Tipe) {
    var form = new FormData($('#FormUnitKegiatan')[0]);
    if (Tipe == "TambahData") {
        var Url = base_url + "/Units/UnitKegiatanAdd";
    } else if (Tipe == "EditData") {
        var Url = base_url + "/Units/UnitKegiatanEdit";
    }

    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        data: form,
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $('#FormUnitKegiatan')[0].reset();
                $("#ModalUnitKegiatan").modal("hide");
                TabelUnitKegiatan.ajax.reload();
                iziToast.success({
                    title: 'Berhasil Menyimpan Kegiatan Unit',
                    message: responsesave.ReturnMessage,
                    position: 'topRight'
                });
            } else if (responsesave.IsSuccess == false) {
                iziToast.error({
                    title: 'Gagal Menyimpan Kegiatan Unit',
                    message: responsesave.ReturnMessage,
                    position: 'topRight'
                });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function HapusData(IdUnitKegiatan, Judul) {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus data Kegiatan Unit " + Judul,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: true
    },
        function () {
            var Url = base_url + "/Units/UnitKegiatanDelete?IdUnitKegiatan=" + IdUnitKegiatan;
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesave) {
                    ProgressBar("success");
                    if (responsesave.IsSuccess == true) {
                        TabelUnitKegiatan.ajax.reload();
                        iziToast.success({
                            title: 'Berhasil Menghapus Kegiatan Unit',
                            message: responsesave.ReturnMessage,
                            position: 'topRight'
                        });
                    } else if (responsesave.IsSuccess == false) {
                        iziToast.error({
                            title: 'Gagal Menghapus Kegiatan Unit',
                            message: responsesave.ReturnMessage,
                            position: 'topRight'
                        });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}

function RejectData(IdUnitKegiatan, Judul) {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan me-reject data Kegiatan Unit " + Judul,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: true
    },
        function () {
            var Url = base_url + "/Units/UnitKegiatanReject?IdUnitKegiatan=" + IdUnitKegiatan;
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesave) {
                    ProgressBar("success");
                    if (responsesave.IsSuccess == true) {
                        TabelUnitKegiatan.ajax.reload();
                        iziToast.success({
                            title: 'Berhasil Me-Reject Kegiatan Unit',
                            message: responsesave.ReturnMessage,
                            position: 'topRight'
                        });
                    } else if (responsesave.IsSuccess == false) {
                        iziToast.error({
                            title: 'Gagal Me-Reject Kegiatan Unit',
                            message: responsesave.ReturnMessage,
                            position: 'topRight'
                        });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}

