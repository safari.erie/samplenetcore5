﻿/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 15052020
================================================================================================================= */
$.fn.modal.Constructor.prototype._enforceFocus = function () {};

ComboGetUnit(function (obj) {
    $("select[name='IdUnit']").html(obj);
});
$("select[name='IdUnit']").change(function () {
    TabelUnitPegawai(this.value);
    $("#BtnTambah").attr("onClick", 'TambahData("' + this.value + '")');
});
TabelUnitPegawai(1);

function TabelUnitPegawai(IdUnit) {
    //////// START SECTION Tabel Unit Pegawai ///////////
    $('#TabelUnitPegawai tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelUnitPegawai = $('#TabelUnitPegawai').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/Units/GetUnitPegawais?IdUnit=" + IdUnit,
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    if (json.ReturnMessage != "data tidak ada") {
                        iziToast.error({
                            title: 'Gagal Menampilkan Data Tabel Pegawai Unit',
                            message: json.ReturnMessage,
                            position: 'topRight'
                        });
                    }
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        columnDefs: [{
                targets: [0],
                width: "auto",
                visible: true
            },
            {
                targets: [1],
                width: "auto",
                visible: true
            },
            {
                targets: [2],
                width: "auto",
                visible: true
            },
            {
                targets: [3],
                width: "auto",
                visible: true
            },
        ],
        "columns": [{
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamHapus = "HapusData('" + IdUnit + "','" + full.IdPegawai + "','" + cekQuotes(full.Nama) + "')";
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                    return Data;
                }
            },
            {
                "data": "Nama"
            },
            {
                "data": "Nik"
            },
            //{
            //    "data": "JenisPegawai"
            //},
            {
                "data": "Jabatan"
            },
        ],
        "bDestroy": true
    });
    TabelUnitPegawai.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
    //////// START SECTION Tabel Unit Pegawai ///////////
}

function TambahData(IdUnit) {
    $("#ModalUnitPegawai").modal("show");
    $('#FormUnitPegawai')[0].reset();
    $("#BtnSaveUnitPegawai").attr("onClick", "SaveData('TambahData');");
    $(".modal-title").html("Form Tambah Pegawai Unit");
    ComboGetUnit(function (obj) {
        $("select[name='IdUnit']").html(obj);
    }, IdUnit);
    ComboGetPegawai(function (obj) {
        $("select[name='IdPegawai']").html(obj);
    });
    ComboGetJenisPegawai(function (obj) {
        $("select[name='IdJenisPegawai']").html(obj);
    });
}

function SaveData() {
    var formObj = $('#FormUnitPegawai').serializeObject();

    var Pegawais = [];
    var Data = {};
    Data.IdPegawai = parseInt(formObj.IdPegawai);
    //Data.IdJenisPegawai = parseInt(formObj.IdJenisPegawai);
    Pegawais.push(Data);

    var Url = base_url + "/Units/UnitPegawaiAdd?IdUnit=" + parseInt(formObj.IdUnit);
    var JsonData = JSON.stringify(Pegawais);
    $.ajax({
        url: Url,
        method: "POST",
        data: JsonData,
        dataType: "json",
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $('#FormUnitPegawai')[0].reset();
                $("#ModalUnitPegawai").modal("hide");
                TabelUnitPegawai(formObj.IdUnit);
                swal({
                    title: 'Berhasil Menyimpan Pegawai Unit',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
            } else if (responsesave.IsSuccess == false) {
                swal({
                    title: 'Gagal Menyimpan Pegawai Unit',
                    text: responsesave.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function HapusData(IdUnit, IdPegawai, Nama) {
    swal({
            title: "Apakah Anda Yakin ?",
            text: "Anda akan menghapus data pegawai unit atas nama "+Nama,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: true
        },
        function () {
            var Pegawais = [];
            var Data = {};
            Data.IdPegawai = parseInt(IdPegawai);
            Pegawais.push(Data);

            var Url = base_url + "/Units/UnitPegawaiDelete?IdUnit=" + parseInt(IdUnit);
            var JsonData = JSON.stringify(Pegawais);
            $.ajax({
                url: Url,
                method: "POST",
                data: JsonData,
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesave) {
                    ProgressBar("success");
                    if (responsesave.IsSuccess == true) {
                        TabelUnitPegawai(IdUnit);
                        iziToast.success({
                            title: 'Berhasil Menghapus Pegawai Unit',
                            message: responsesave.ReturnMessage,
                            position: 'topRight'
                        });
                    } else if (responsesave.IsSuccess == false) {
                        iziToast.error({
                            title: 'Gagal Menghapus Pegawai Unit',
                            message: responsesave.ReturnMessage,
                            position: 'topRight'
                        });
                    }
                },
                error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({
                        title: 'Error :(',
                        text: JSON.stringify(responserror) + " : " + e,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            });
        });
}