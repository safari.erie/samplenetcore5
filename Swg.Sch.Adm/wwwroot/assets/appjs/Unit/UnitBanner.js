const UrlSvc = {
  GetUnitBanners: `${base_url}/units/GetUnitBanners`,
  GetUnitBanner: `${base_url}/units/GetUnitBanner`,
  UnitBannerAdd: `${base_url}/units/UnitBannerAdd`,
  UnitBannerEdit: `${base_url}/units/UnitBannerEdit`,
  UnitBannerDelete: `${base_url}/units/UnitBannerDelete`,
};

$(document).ready((e) => {
  DataBanners();
  ComboGetUnitSklh(function (obj) {
    $("select[name='IdUnit']", "#FormUnitBanner").html(obj);
  });
});

const DataBanners = () => {
  var res = initAjax(`${UrlSvc.GetUnitBanners}`);
  if (res) ProgressBar("success");
  if (res.IsSuccess) {
    $("#TabelUnitBanner tfoot th").each(function () {
      var title = $(this).text();
      $(this).html(
        '<input type="text" class="form-control" placeholder="Cari ' +
          title +
          '" />'
      );
    });
    var TabelData = $("#TabelUnitBanner").DataTable({
      paging: true,
      searching: true,
      ordering: true,
      info: true,
      pageLength: 10,
      lengthChange: true,
      scrollX: true,
      processing: true,
      data: res.Data,
      columns: [
        {
          render: function (data, type, full, meta) {
            var Data = "";
            Data = `<button class="btn btn-info btn-sm mr-1" onClick="ShowModal('Edit',${full.IdUnitBanner})"><i class="fa fa-pencil-square-o"></i> Edit </button>`;
            Data += `<button class="btn btn-danger btn-sm" onClick="DeleteData(${full.IdUnitBanner})"><i class="fa fa-trash"></i> Hapus </button>`;
            return Data;
          },
        },
        {
          data: "Unit",
        },
        {
          render: function (data, type, full, meta) {
            var OnError = "this.style.display='none'";
            var Data =
              '<img onerror="' +
              OnError +
              '" src="' +
              base_url +
              "/Asset/Files/Galeri/Unit/" +
              full.FileImage +
              '" style="height:50px;object-fit:cover;object-position:center;">';

            return Data;
          },
        },
      ],
      bDestroy: true,
    });
    TabelData.columns().every(function () {
      var that = this;

      $("input", this.footer()).on("keyup change clear", function () {
        if (that.search() !== this.value) {
          that.search(this.value).draw();
        }
      });
    });
  } else {
  }
};

const DataBanner = (id) => {
  var res = initAjax(`${UrlSvc.GetUnitBanner}?IdUnitBanner=${id}`);
  if (res) ProgressBar("success");
  if (res.IsSuccess) {
    $("#ModalUnitBanner").modal("show");
    $("#IdUnitBanner", "#FormUnitBanner").val(res.Data.IdUnitBanner);
    ComboGetUnitSklh(function (obj) {
      $("select[name='IdUnit']", "#FormUnitBanner").html(obj);
    }, res.Data.IdUnit);
    $("#ThumbnailImageBanner")
      .removeAttr("src")
      .attr("src", base_url + "/Asset/Files/Galeri/Unit/" + res.Data.FileImage);
  } else {
    iziToast.error({
      title: "Gagal",
      message: res.ReturnMessage,
      position: "topRight",
    });
  }
};

const ShowModal = (isAction, id) => {
  $("select#IdUnit", "#FormUnitBanner").val("").trigger("change");
  $("#FileImage", "#FormUnitBanner").val("");
  if (isAction === "Add") {
    $("#BtnSaveUnitBanner").attr("onClick", 'SaveBanner("Add")');
    $("#ModalUnitBanner").modal("show");
  } else {
    DataBanner(id);
    $("#BtnSaveUnitBanner").attr("onClick", 'SaveBanner("Edit")');
  }
};

const SaveBanner = (Tipe) => {
  var form = new FormData($("#FormUnitBanner")[0]);
  if (Tipe == "Add") {
    var Url = UrlSvc.UnitBannerAdd;
  } else if (Tipe == "Edit") {
    var Url = UrlSvc.UnitBannerEdit;
  }

  $.ajax({
    url: Url,
    method: "POST",
    dataType: "json",
    data: form,
    contentType: false,
    cache: true,
    processData: false,
    beforeSend: function (before) {
      ProgressBar("wait");
    },
    success: function (res) {
      ProgressBar("success");
      if (res.IsSuccess) {
        $("#FormUnitBanner")[0].reset();
        $("#ModalUnitBanner").modal("hide");
        DataBanners();
        iziToast.success({
          title: "Berhasil Menyimpan Banner Unit",
          message: res.ReturnMessage,
          position: "topRight",
        });
      } else {
        iziToast.error({
          title: "Gagal Menyimpan Banner Unit",
          message: res.ReturnMessage,
          position: "topRight",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
};

const DeleteData = (id) => {
  swal(
    {
      title: "Apakah Anda Yakin ?",
      text: "Anda akan hapus data Banner Unit ini",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-success",
      confirmButtonText: "Ya, Saya Yakin!",
      cancelButtonClass: "btn-danger",
      cancelButtonText: "Tidak, Batalkan!",
      closeOnConfirm: true,
    },
    function () {
      var res = initAjax(`${UrlSvc.UnitBannerDelete}?IdUnitBanner=${id}`);
      if (res) ProgressBar("success");
      if (res.IsSuccess) {
        DataBanners();
        iziToast.success({
          title: "Berhasil Hapus Banner Unit",
          message: res.ReturnMessage,
          position: "topRight",
        });
      } else {
        iziToast.error({
          title: "Gagal Hapus Banner Unit",
          message: res.ReturnMessage,
          position: "topRight",
        });
      }
    }
  );
};
