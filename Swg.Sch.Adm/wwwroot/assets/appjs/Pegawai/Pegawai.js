﻿$(document).ready(function () {
    DataList();
    ComboGetJenisPegawai(function (obj) {
        $('select#IdJenisPegawai').html(obj);
    });
    ComboGetListJabatan(function (obj) {
        $('select#IdJabatan').html(obj);
    });
    ComboGetListJenjangPendidikan(function (obj) {
        $('select#IdJenjangPendidikan').html(obj);
    });
    ComboGetListUser(function (obj) {
        $('select#IdPegawai').html(obj);
    });
    ComboGetAgama(function (obj) {
        $('select#IdAgama').html(obj);
    });
    ComboGetJenisKelamin(function (obj) {
        $('select#KdJenisKelamin').html(obj);
    });
    ComboGetBulan(function (obj) {
        $("select[name='BulanDownload']").html(obj);
    });
    ComboGetTahun(function (obj) {
        $("select[name='Tahun']").html(obj);
    });
    ComboGetJenisGolonganPegawai(function (obj) {
        $('select#IdGolonganPegawai').html(obj);
    });
    ComboGetUnit(function (obj) {
        $("select[name='IdUnit']").html(obj);
    });
    ComboGetJenisStatusNikah(function (obj) {
        $("select[name='StatusKawin']").html(obj);
    });

    GetKelasParalelPegawaix();
    GetKompetensiPegawaix();
})



function DataEditor(id = null) {
   
    if (id != null) {
        GetById(id);
        $('#IdPegawai').attr('disabled', true);
        $('#DivNoHp').fadeIn();
        $('#DivEmail').fadeIn();
        $('#DivAlamat').fadeIn();
        $("textarea[name='Sambutan']", "#FormPegawai").summernote('code','');
    } else {
         $('div#EditorForm').fadeIn();
        $('div#ListData').hide();
        $('#DivNoHp').hide();
        $('#DivEmail').hide();
        $('#DivAlamat').hide();
        $('#IdPegawai').attr('disabled', false);
        $('input#TipeForm').val('');
        $("#FormPegawai")[0].reset();
        $('select#IdPegawai').val("").trigger('change');
        $('select#IdJenisPegawai').val("").trigger('change');
        $('select#IdJabatan').val("").trigger('change');
        $('select#IdAgama').val("").trigger('change');
        $('select#KdJenisKelamin').val("").trigger('change');
        $('select#IdJenjangPendidikan').val("").trigger('change');
        $('select#IdGolonganPegawai').val("").trigger('change');
        $('select#JumlahAnak').val("").trigger('change');
    }
}

function DataList() {
    $('#TabelPegawai tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelPegawai = $('#TabelPegawai').DataTable({
        "paging": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "processing": true,
        "scrollX": true,
        "scrollY": false,
        // "responsive": {
        //     details: {
        //         renderer: $.fn.dataTable.Responsive.renderer.tableAll()
        //     }
        // },
        "ajax": {
            "url": base_url + "/api/pegawais/getlistpegawai",
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    if (json.ReturnMessage != "data tidak ada") {
                        iziToast.error({
                            title: 'Gagal Menampilkan Data Tabel',
                            message: json.ReturnMessage,
                            position: 'topRight'
                        });
                    }
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [
            {
                "data": "IdPegawai",
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamEdit = "DataEditor('" + full.IdPegawai + "')";
                    var ParamStatus = "DataStatus('" + full.IdPegawai + "','" + full.Nama + "','" + full.Status + "')";
                    Data += '<button type="button" class="btn btn-success btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
                    Data += '&nbsp;&nbsp;&nbsp;';
                    if (full.Status == "Aktif") {
                        Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamStatus + '"><i class="fa fa-times"></i> Deaktivasi</button>';
                    } else {
                        Data += '<button type="button" class="btn btn-success btn-sm" onClick="' + ParamStatus + '"><i class="fa fa-check"></i> Aktivasi</button>';
                    }
                    return Data;
                }
            },
            {
                "className": 'desktop mobile tablet',
                "data": "Nip"
            },
            {
                "className": 'desktop mobile tablet',
                "data": "Nama"
            },
            {
                "className": '',
                "data": "JenisKelamin"
            },
            {
                "data": "NoHandphone"
            },
            {
                "className": 'desktop',
                "data": "TanggalLahir",
                "render": function (data, type, full, meta) {
                    var Data = indoDate(full.TanggalLahir);
                    return Data;
                }
            },
            {
                "data": "Unit"
            },
            {
                "data": "Jabatan"
            },
            {
                "className": 'desktop',
                "data": "Status",
                "render": function (data, type, full, meta) {
                    var Data = "";
                    if (full.Status == "Aktif") {
                        Data += '<span style="color:green;font-weight:bold;cursor:pointer;"><i class="fa fa-check"></i> ' + full.Status + '</span>';
                    } else {
                        Data += '<span style="color:red;font-weight:bold;cursor:pointer;"><i class="fa fa-times"></i> ' + full.Status + '</span>';
                    }
                    return Data;
                }
            },
           
        ],
        "bDestroy": true
    });
    TabelPegawai.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function DataStatus(Id, Nama, Status) {
    if (Status == "Aktif") {
        swal({
                title: "Apakah Anda Yakin ?",
                text: "Anda akan deaktivasi pegawai " + Nama,
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ya, Saya Yakin!",
                cancelButtonClass: "btn-danger",
                cancelButtonText: "Tidak, Batalkan!",
                closeOnConfirm: false
            },
            function () {
                var Url = base_url + "/api/pegawais/SetPegawaiInActive?idpegawai=" + Id;
                $.ajax({
                    url: Url,
                    method: "GET",
                    dataType: "json",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    beforeSend: function (before) {
                        ProgressBar("wait");
                    },
                    success: function (res) {
                        ProgressBar("success");
                        if (res.IsSuccess == true) {
                            $('#TabelPegawai').DataTable().ajax.reload();
                            swal({
                                title: 'Berhasil',
                                text: "Berhasil DeAktivasi Pegawai",
                                confirmButtonClass: 'btn-success text-white',
                                confirmButtonText: 'Oke, Mengerti',
                                type: 'success'
                            });
                        } else if (res.IsSuccess == false) {
                            swal({
                                title: 'Gagal',
                                text: res.ReturnMessage,
                                confirmButtonClass: 'btn-danger text-white',
                                confirmButtonText: 'Oke, Mengerti',
                                type: 'error'
                            });
                        }
                    },
                    error: function (responserror, a, e) {
                        ProgressBar("success");
                        swal({
                            title: 'Error :(',
                            text: JSON.stringify(responserror) + " : " + e,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                });
            });
    } else {
        swal({
                title: "Apakah Anda Yakin ?",
                text: "Anda akan aktivasi pegawai " + Nama,
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ya, Saya Yakin!",
                cancelButtonClass: "btn-danger",
                cancelButtonText: "Tidak, Batalkan!",
                closeOnConfirm: false
            },
            function () {
                var Url = base_url + "/api/pegawais/SetPegawaiActive?idpegawai=" + Id;
                $.ajax({
                    url: Url,
                    method: "GET",
                    dataType: "json",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    beforeSend: function (before) {
                        ProgressBar("wait");
                    },
                    success: function (res) {
                        ProgressBar("success");
                        if (res.IsSuccess == true) {
                            $('#TabelPegawai').DataTable().ajax.reload();
                            swal({
                                title: 'Berhasil',
                                text: "Berhasil Aktivasi Pegawai",
                                confirmButtonClass: 'btn-success text-white',
                                confirmButtonText: 'Oke, Mengerti',
                                type: 'success'
                            });
                        } else if (res.IsSuccess == false) {
                            swal({
                                title: 'Gagal',
                                text: res.ReturnMessage,
                                confirmButtonClass: 'btn-danger text-white',
                                confirmButtonText: 'Oke, Mengerti',
                                type: 'error'
                            });
                        }
                    },
                    error: function (responserror, a, e) {
                        ProgressBar("success");
                        swal({
                            title: 'Error :(',
                            text: JSON.stringify(responserror) + " : " + e,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                });
            });
    }

}

function DataSave() {
    var TipeForm = $('input#TipeForm').val();
    var formObj = $('#FormPegawai').serializeObject();
    var form = new FormData($('#FormPegawai')[0]);
    var IdPegawai = $('#IdPegawai option:selected').val();
    if (TipeForm == "Edit") {
        var url = base_url + '/api/pegawais/Pegawaiedit';
    } else {
        var url = base_url + '/api/pegawais/Pegawaiadd';
    }
    form.append("IdPegawai", IdPegawai);

    $('select[name^="IdKelasParalel[]"] option:selected', '#FormPegawai').each(function (i, v) {
        form.append('KelasDiampu[' + i + '].IdKelasParalel', v.value);
    });
    $('select[name^="IdKompetensi[]"] option:selected', '#FormPegawai').each(function (i, v) {
        form.append('Kompetensi[' + i + '].IdKompetensi', v.value);
    });

    $.ajax({
        "url": url,
        "method": "POST",
        "timeout": 0,
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        "dataType": "json",
        "beforeSend": function (xhr) {
            ProgressBar("wait");
        },
        "success": function (res) {
            ProgressBar("success");
            if (res.IsSuccess == true) {
                $('#TabelPegawai').DataTable().ajax.reload();
                $('div#EditorForm').hide();
                $('div#ListData').fadeIn();
                swal({
                    title: 'Sukses',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
            } else if (res.IsSuccess == false) {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        "error": function (err, a, e) {
            ProgressBar("success");
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(err),
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

function Kembali() {
    $('div#EditorForm').hide();
    $('div#ListData').fadeIn();
}

function GetById(id = null) {
    var formObj = $('#FormUnitPegawai').serializeObject();
    $.ajax({
        url: base_url + '/api/pegawais/getlistpegawai',
        type: 'GET',
        beforeSend: function (xhr) {
            ProgressBar("wait");
        },
        success: function (res) {
            var i = 0;
            var Jum = (res.Data).length;
            for (i; i < Jum; i++) {
                var CekIdPegawai = res.Data[i].IdPegawai;
                if (CekIdPegawai == id) {
                    ProgressBar("success");
                    $('div#EditorForm').fadeIn();
                    $('div#ListData').hide();
                    ComboGetListUser(function (obj) {
                        $('select#IdPegawai').html(obj);
                    }, res.Data[i].IdPegawai);
                    $('input[name="TipeForm"]').val('Edit');
                    // $('input[name="IdPegawai"]').val(res.Data[i].IdPegawai);
                    $('input[name="Nama"]', '#FormPegawai').val(res.Data[i].Nama);
                    $('input[name="Gelar"]', '#FormPegawai').val(res.Data[i].Gelar);
                    $('textarea[name="Alamat"]', '#FormPegawai').val(res.Data[i].Alamat);
                    $('input[name="NoHandphone"]', '#FormPegawai').val(res.Data[i].NoHandphone);
                    $('input[name="Email"]', '#FormPegawai').val(res.Data[i].Email);
                    $('input[name="Nip"]', '#FormPegawai').val(res.Data[i].Nip);
                    $('input[name="Nik"]', '#FormPegawai').val(res.Data[i].Nik);
                    $('input[name="TempatLahir"]', '#FormPegawai').val(res.Data[i].TempatLahir);
                    $('input[name="TanggalLahir"]', '#FormPegawai').val(res.Data[i].TanggalLahir);
                    ComboGetJenisPegawai(function (obj) {
                        $('select#IdJenisPegawai').html(obj);
                    }, res.Data[i].IdJenisPegawai);
                    ComboGetListJabatan(function (obj) {
                        $('select#IdJabatan').html(obj);
                    }, res.Data[i].IdJabatan);
                    ComboGetListJenjangPendidikan(function (obj) {
                        $('select#IdJenjangPendidikan').html(obj);
                    }, res.Data[i].IdJenjangPendidikan);
                    ComboGetAgama(function (obj) {
                        $('select#IdAgama').html(obj);
                    }, res.Data[i].IdAgama);
                    ComboGetJenisKelamin(function (obj) {
                        $('select#KdJenisKelamin').html(obj);
                    }, res.Data[i].KdJenisKelamin);
                    ComboGetJenisGolonganPegawai(function (obj) {
                        $('select#IdGolonganPegawai').html(obj);
                    }, res.Data[i].IdGolonganPegawai);
                    $('input[name="NamaInstitusiPendidikan"]', '#FormPegawai').val(res.Data[i].NamaInstitusiPendidikan);
                    $('input[name="NamaPasangan"]', '#FormPegawai').val(res.Data[i].NamaPasangan);
                    $('input[name="TanggalMasuk"]', '#FormPegawai').val(res.Data[i].TanggalMasuk);
                    $('input[name="NoDarurat"]', '#FormPegawai').val(res.Data[i].NoDarurat);
                    ComboGetUnit(function (obj) {
                        $("select[name='IdUnit']").html(obj);
                    }, res.Data[i].IdUnit);

                    const KelasDiampu = [];
                    $.each(res.Data[i].KelasDiampu, (i, v) => {
                        KelasDiampu.push(
                            `${v.IdKelasParalel}`
                        );
                    })

                    const Kompetensi = [];
                    $.each(res.Data[i].Kompetensi, (i, v) => {
                        Kompetensi.push(
                            `${v.IdKompetensi}`
                        );
                    })

                    $('select[name="IdKelasParalel[]"]', '#FormPegawai').val(KelasDiampu).trigger('change');
                    
                    $('select[name="IdKompetensi[]"]', '#FormPegawai').val(Kompetensi).trigger('change');
                    $('input[name="NoKk"]', '#FormPegawai').val(res.Data[i].NoKk);
                    $('input[name="JurusanPendidikan"]', '#FormPegawai').val(res.Data[i].JurusanPendidikan);
                    $('textarea[name="AktifitasDakwah"]', '#FormPegawai').val(res.Data[i].AktifitasDakwah);
                    $('textarea[name="OrganisasiMasyarakat"]', '#FormPegawai').val(res.Data[i].OrganisasiMasyarakat);
                    $('textarea[name="PengalamanKerja"]', '#FormPegawai').val(res.Data[i].PengalamanKerja);
                    $('input[name="Facebook"]', '#FormPegawai').val(res.Data[i].Facebook);
                    $('input[name="Twitter"]', '#FormPegawai').val(res.Data[i].Twitter);
                    $('input[name="Instagram"]', '#FormPegawai').val(res.Data[i].Instagram);
                    $('input[name="Youtube"]', '#FormPegawai').val(res.Data[i].Youtube);
                    $("textarea[name='Sambutan']", "#FormPegawai").summernote('code', res.Data[i].Sambutan);
                    
                    ComboGetJenisStatusNikah(function (obj) {
                        $("select[name='StatusKawin']", '#FormPegawai').html(obj);
                    }, res.Data[i].StatusKawin);
                    $('select[name="JumlahAnak"]', '#FormPegawai').val(res.Data[i].JumlahAnak).trigger('change');
                }
            }
        },
        error: function (err, a, e) {
            swal({
                title: 'Error :(',
                text: JSON.stringify(err) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

function DownloadGajiPegawai() {
    var Tahun = $('#Tahun').val();
    var Bulan = $('#BulanDownload').val();
    window.open(base_url + '/api/download/DownloadGajiPegawai' + '?Tahun=' + Tahun + '&Bulan=' + Bulan, '_blank');
}

function ModalUploadSlipGaji() {
    $('#ModalUploadSlipGaji').modal('show');
}

function UploadGaji() {
    var form = new FormData($('#FormUploadSlipGaji')[0]);
    $.ajax({
        url: base_url + '/api/pegawais/UploadGaji',
        method: "POST",
        processData: false,
        mimeType: "multipart/form-data",
        contentType: false,
        data: form,
        dataType: "JSON",
        beforeSend: function (xhr) {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            if (res.IsSuccess) {
                swal({
                    title: 'Sukses',
                    text: 'Berhasil upload slip gaji pegawai',
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
                $("#TabelData").DataTable().ajax.reload();
                $('#ModalUploadSlipGaji').modal('hide');

            } else {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

function GetKelasParalelPegawaix() {
    $.ajax({
        url: base_url + '/api/pegawais/GetKelasParalelPegawai',
        type: 'GET',
        success: function (res) {
            if (res.IsSuccess) {
                var html = "";
                $.each(res.Data, (i, v) => {
                    html += `<option value='${v.Id}'>${v.Nama}</option>`;
                });
                $("select[name='IdKelasParalel[]']", '#FormPegawai').html(html);
            } else {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (err, a, e) {
            swal({
                title: 'Error :(',
                text: JSON.stringify(err) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function GetKompetensiPegawaix() {
    $.ajax({
        url: base_url + '/api/pegawais/GetKompetensiPegawai',
        type: 'GET',
        success: function (res) {
            if (res.IsSuccess) {
                var html = "";
                $.each(res.Data, (i, v) => {
                    html += `<option value='${v.Id}'>${v.Nama}</option>`;
                });
                $("select[name='IdKompetensi[]']",'#FormPegawai').html(html);
            } else {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (err, a, e) {
            swal({
                title: 'Error :(',
                text: JSON.stringify(err) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}