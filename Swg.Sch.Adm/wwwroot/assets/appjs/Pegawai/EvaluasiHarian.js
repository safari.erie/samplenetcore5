﻿$(document).ready(function () {
    GetTabelMutabaah("");
})

const UrlService = {
    GetJenisEvaluasiHarian: base_url + '/api/pegawais/GetJenisEvaluasiHarianPegawai',
    GetKbmEvalHarians: base_url + '/api/pegawais/GetKbmEvalHarianPegawais',
    GetEvaluasiHarian: base_url + '/api/pegawais/GetEvaluasiHarianPegawai',
    EvaluasiHarianAdd: base_url + '/api/pegawais/EvaluasiHarianAddPegawai',
};

function ModalCariByTanggal() {
    $('#ModalTanggal').modal('show');
}

function PencarianByTanggal() {
    var Tanggal = $('#TanggalPencarian').val();
    GetTabelMutabaah(Tanggal);
    $('#ModalTanggal').modal('hide');
}

function GetTabelMutabaah(Tanggal) {
    var TabelMutabaah = $('#TabelMutabaah').DataTable({
        "paging": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "pageLength": 5,
        "lengthChange": false,
        "scrollX": true,
        "scrollY": "500px",
        "scrollCollapse": true,
        "processing": true,
        "columnDefs": [{
            "targets": 1,
            "type": "date-id",
            "searchable": true
        }],
        "ajax": {
            "url": UrlService.GetKbmEvalHarians + "?Tanggal=" + Tanggal,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    swal({ title: 'Gagal Menampilkan Data Tabel Mutabaah Pegawai', text: json.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    return "<strong>" + full.JenisEvaluasiPegawai + "</strong>";
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var HtmlData = "";
                    if (full.HariKe1 == "Y") {
                        HtmlData += "<input type='checkbox' name='HariKe1[]' value='" + full.HariKe1Tanggal + "|" + full.IdJenisEvaluasiPegawai + "' style='width: 1.50rem;height: 1.50rem;' checked='true'>";
                        //HtmlData += "<i class='fa fa-check fa-2x' style='color:green'></i>";
                    } else {
                        HtmlData += "<input type='checkbox' name='HariKe1[]' value='" + full.HariKe1Tanggal + "|" + full.IdJenisEvaluasiPegawai + "' style='width: 1.50rem;height: 1.50rem;' >";
                        //HtmlData += "<i class='fa fa-close fa-2x' style='color:red'></i>";
                    }
                    return HtmlData;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var HtmlData = "";
                    if (full.HariKe2 == "Y") {
                        HtmlData += "<input type='checkbox' name='HariKe2[]' value='" + full.HariKe2Tanggal + "|" + full.IdJenisEvaluasiPegawai + "' style='width: 1.50rem;height: 1.50rem;' checked='true'>";
                        //HtmlData += "<i class='fa fa-check fa-2x' style='color:green'></i>";
                    } else {
                        HtmlData += "<input type='checkbox' name='HariKe2[]' value='" + full.HariKe2Tanggal + "|" + full.IdJenisEvaluasiPegawai + "' style='width: 1.50rem;height: 1.50rem;' >";
                        //HtmlData += "<i class='fa fa-close fa-2x' style='color:red'></i>";
                    }
                    return HtmlData;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var HtmlData = "";
                    if (full.HariKe3 == "Y") {
                        HtmlData += "<input type='checkbox' name='HariKe3[]' value='" + full.HariKe3Tanggal + "|" + full.IdJenisEvaluasiPegawai + "' style='width: 1.50rem;height: 1.50rem;' checked='true'>";
                        //HtmlData += "<i class='fa fa-check fa-2x' style='color:green'></i>";
                    } else {
                        HtmlData += "<input type='checkbox' name='HariKe3[]' value='" + full.HariKe3Tanggal + "|" + full.IdJenisEvaluasiPegawai + "' style='width: 1.50rem;height: 1.50rem;' >";
                        //HtmlData += "<i class='fa fa-close fa-2x' style='color:red'></i>";
                    }
                    return HtmlData;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var HtmlData = "";
                    if (full.HariKe4 == "Y") {
                        HtmlData += "<input type='checkbox' name='HariKe4[]' value='" + full.HariKe4Tanggal + "|" + full.IdJenisEvaluasiPegawai + "' style='width: 1.50rem;height: 1.50rem;' checked='true'>";
                        //HtmlData += "<i class='fa fa-check fa-2x' style='color:green'></i>";
                    } else {
                        HtmlData += "<input type='checkbox' name='HariKe4[]' value='" + full.HariKe4Tanggal + "|" + full.IdJenisEvaluasiPegawai + "' style='width: 1.50rem;height: 1.50rem;' >";
                        //HtmlData += "<i class='fa fa-close fa-2x' style='color:red'></i>";
                    }
                    return HtmlData;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var HtmlData = "";
                    if (full.HariKe5 == "Y") {
                        HtmlData += "<input type='checkbox' name='HariKe5[]' value='" + full.HariKe5Tanggal + "|" + full.IdJenisEvaluasiPegawai + "' style='width: 1.50rem;height: 1.50rem;' checked='true'>";
                        //HtmlData += "<i class='fa fa-check fa-2x' style='color:green'></i>";
                    } else {
                        HtmlData += "<input type='checkbox' name='HariKe5[]' value='" + full.HariKe5Tanggal + "|" + full.IdJenisEvaluasiPegawai + "' style='width: 1.50rem;height: 1.50rem;' >";
                        //HtmlData += "<i class='fa fa-close fa-2x' style='color:red'></i>";
                    }
                    return HtmlData;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var HtmlData = "";
                    if (full.HariKe6 == "Y") {
                        HtmlData += "<input type='checkbox' name='HariKe6[]' value='" + full.HariKe6Tanggal + "|" + full.IdJenisEvaluasiPegawai + "' style='width: 1.50rem;height: 1.50rem;' checked='true'>";
                        //HtmlData += "<i class='fa fa-check fa-2x' style='color:green'></i>";
                    } else {
                        HtmlData += "<input type='checkbox' name='HariKe6[]' value='" + full.HariKe6Tanggal + "|" + full.IdJenisEvaluasiPegawai + "' style='width: 1.50rem;height: 1.50rem;' >";
                        //HtmlData += "<i class='fa fa-close fa-2x' style='color:red'></i>";
                    }
                    return HtmlData;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var HtmlData = "";
                    if (full.HariKe7 == "Y") {
                        HtmlData += "<input type='checkbox' name='HariKe7[]' value='" + full.HariKe7Tanggal + "|" + full.IdJenisEvaluasiPegawai + "' style='width: 1.50rem;height: 1.50rem;' checked='true'>";
                        //HtmlData += "<i class='fa fa-check fa-2x' style='color:green'></i>";
                    } else {
                        HtmlData += "<input type='checkbox' name='HariKe7[]' value='" + full.HariKe7Tanggal + "|" + full.IdJenisEvaluasiPegawai + "' style='width: 1.50rem;height: 1.50rem;' >";
                        //HtmlData += "<i class='fa fa-close fa-2x' style='color:red'></i>";
                    }
                    return HtmlData;
                }
            },
        ],
        "bDestroy": true
    });
}

//TambahMutabaah();
//function TambahMutabaah() {
//    ComboGetJenisEvaluasiHarian(function (obj) {
//        $("select[name='IdJenisEvaluasiHarianPegawai']").html(obj);
//    });
//}

// $("input[name='HariKe1CheckAll']").click(function () {
//     $("input[name='HariKe1[]']").not(this).prop('checked', this.checked);
// });
// $("input[name='HariKe2CheckAll']").click(function () {
//     $("input[name='HariKe2[]']").not(this).prop('checked', this.checked);
// });
// $("input[name='HariKe3CheckAll']").click(function () {
//     $("input[name='HariKe3[]']").not(this).prop('checked', this.checked);
// });
// $("input[name='HariKe4CheckAll']").click(function () {
//     $("input[name='HariKe4[]']").not(this).prop('checked', this.checked);
// });
// $("input[name='HariKe5CheckAll']").click(function () {
//     $("input[name='HariKe5[]']").not(this).prop('checked', this.checked);
// });
// $("input[name='HariKe6CheckAll']").click(function () {
//     $("input[name='HariKe6[]']").not(this).prop('checked', this.checked);
// });
// $("input[name='HariKe7CheckAll']").click(function () {
//     $("input[name='HariKe7[]']").not(this).prop('checked', this.checked);
// });

function ProsesMutabaah() {
    //var form = new FormData($('#FormTabelMutabaahSiswa')[0]);
    //var formObj = $('#FormTabelMutabaahSiswa').serializeObject();

    var EvalHarianDatas = [];
    $("input[name='HariKe1[]']", "#FormTabelMutabaahSiswa").each(function (i) {
        var EvalHarianData = {};
        var HariKe1Val = $(this).val().split("|");
        if (this.checked) {
            EvalHarianData.Tanggal = HariKe1Val[0];
            EvalHarianData.IdJenisEvaluasiHarianPegawai = parseInt(HariKe1Val[1]);
            EvalHarianData.IsDone = "Y";
            EvalHarianData.Catatan = "-";
        } else {
            EvalHarianData.Tanggal = HariKe1Val[0];
            EvalHarianData.IdJenisEvaluasiHarianPegawai = parseInt(HariKe1Val[1]);
            EvalHarianData.IsDone = "T";
            EvalHarianData.Catatan = "-";
        }
        EvalHarianDatas.push(EvalHarianData);
    });

    $("input[name='HariKe2[]']", "#FormTabelMutabaahSiswa").each(function (i) {
        var EvalHarianData = {};
        var HariKe2Val = $(this).val().split("|");
        if (this.checked) {
            EvalHarianData.Tanggal = HariKe2Val[0];
            EvalHarianData.IdJenisEvaluasiHarianPegawai = parseInt(HariKe2Val[1]);
            EvalHarianData.IsDone = "Y";
            EvalHarianData.Catatan = "-";
        } else {
            EvalHarianData.Tanggal = HariKe2Val[0];
            EvalHarianData.IdJenisEvaluasiHarianPegawai = parseInt(HariKe2Val[1]);
            EvalHarianData.IsDone = "T";
            EvalHarianData.Catatan = "-";
        }
        EvalHarianDatas.push(EvalHarianData);
    });

    $("input[name='HariKe3[]']", "#FormTabelMutabaahSiswa").each(function (i) {
        var EvalHarianData = {};
        var HariKe3Val = $(this).val().split("|");
        if (this.checked) {
            EvalHarianData.Tanggal = HariKe3Val[0];
            EvalHarianData.IdJenisEvaluasiHarianPegawai = parseInt(HariKe3Val[1]);
            EvalHarianData.IsDone = "Y";
            EvalHarianData.Catatan = "-";
        } else {
            EvalHarianData.Tanggal = HariKe3Val[0];
            EvalHarianData.IdJenisEvaluasiHarianPegawai = parseInt(HariKe3Val[1]);
            EvalHarianData.IsDone = "T";
            EvalHarianData.Catatan = "-";
        }
        EvalHarianDatas.push(EvalHarianData);
    });

    $("input[name='HariKe4[]']", "#FormTabelMutabaahSiswa").each(function (i) {
        var EvalHarianData = {};
        var HariKe4Val = $(this).val().split("|");
        if (this.checked) {
            EvalHarianData.Tanggal = HariKe4Val[0];
            EvalHarianData.IdJenisEvaluasiHarianPegawai = parseInt(HariKe4Val[1]);
            EvalHarianData.IsDone = "Y";
            EvalHarianData.Catatan = "-";
        } else {
            EvalHarianData.Tanggal = HariKe4Val[0];
            EvalHarianData.IdJenisEvaluasiHarianPegawai = parseInt(HariKe4Val[1]);
            EvalHarianData.IsDone = "T";
            EvalHarianData.Catatan = "-";
        }
        EvalHarianDatas.push(EvalHarianData);
    });

    $("input[name='HariKe5[]']", "#FormTabelMutabaahSiswa").each(function (i) {
        var EvalHarianData = {};
        var HariKe5Val = $(this).val().split("|");
        if (this.checked) {
            EvalHarianData.Tanggal = HariKe5Val[0];
            EvalHarianData.IdJenisEvaluasiHarianPegawai = parseInt(HariKe5Val[1]);
            EvalHarianData.IsDone = "Y";
            EvalHarianData.Catatan = "-";
        } else {
            EvalHarianData.Tanggal = HariKe5Val[0];
            EvalHarianData.IdJenisEvaluasiHarianPegawai = parseInt(HariKe5Val[1]);
            EvalHarianData.IsDone = "T";
            EvalHarianData.Catatan = "-";
        }
        EvalHarianDatas.push(EvalHarianData);
    });

    $("input[name='HariKe6[]']", "#FormTabelMutabaahSiswa").each(function (i) {
        var EvalHarianData = {};
        var HariKe6Val = $(this).val().split("|");
        if (this.checked) {
            EvalHarianData.Tanggal = HariKe6Val[0];
            EvalHarianData.IdJenisEvaluasiHarianPegawai = parseInt(HariKe6Val[1]);
            EvalHarianData.IsDone = "Y";
            EvalHarianData.Catatan = "-";
        } else {
            EvalHarianData.Tanggal = HariKe6Val[0];
            EvalHarianData.IdJenisEvaluasiHarianPegawai = parseInt(HariKe6Val[1]);
            EvalHarianData.IsDone = "T";
            EvalHarianData.Catatan = "-";
        }
        EvalHarianDatas.push(EvalHarianData);
    });

    $("input[name='HariKe7[]']", "#FormTabelMutabaahSiswa").each(function (i) {
        var EvalHarianData = {};
        var HariKe7Val = $(this).val().split("|");
        if (this.checked) {
            EvalHarianData.Tanggal = HariKe7Val[0];
            EvalHarianData.IdJenisEvaluasiHarianPegawai = parseInt(HariKe7Val[1]);
            EvalHarianData.IsDone = "Y";
            //EvalHarianData.Catatan = "-";
        } else {
            EvalHarianData.Tanggal = HariKe7Val[0];
            EvalHarianData.IdJenisEvaluasiHarianPegawai = parseInt(HariKe7Val[1]);
            EvalHarianData.IsDone = "T";
            //EvalHarianData.Catatan = "-";
        }
        EvalHarianDatas.push(EvalHarianData);
    });
    console.log("DATA NYA : " + EvalHarianDatas);
    SimpanMutabaah(EvalHarianDatas);
}

function SimpanMutabaah(EvalHarianDatas) {
    //var form = new FormData($('#FormInputMutabaah')[0]);
    //var formObj = $('#FormInputMutabaah').serializeObject();
    //var Url = UrlService.EvaluasiHarianAdd + "?Tanggal=" + Tanggal + "&IdJenisEvaluasiHarianPegawai=" + IdJenisEvaluasiHarianPegawai + "&IsDone=" + IsDone + "&Catatan=" + Catatan;

    var Url = UrlService.EvaluasiHarianAdd;
    var JsonData = JSON.stringify(EvalHarianDatas);
    console.log(JsonData);
    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        data: JsonData,
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                //$("#ModalFormInputMutabaah").modal("hide");
                //$("select[name='IdJenisEvaluasiHarianPegawai'] option", "#FormInputMutabaah").trigger('change');
                //$("input[name='Tanggal']", "#FormInputMutabaah").removeAttr("value");
                GetTabelMutabaah("");
                swal({ title: 'Berhasil Menyimpan Mutabaah Pegawai', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal Menyimpan Mutabaah Pegawai', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}