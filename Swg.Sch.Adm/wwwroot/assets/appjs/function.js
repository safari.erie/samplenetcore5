﻿/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 14042020
================================================================================================================= */
var base_url = location.protocol + "//" + document.location.host;
$(document).ready(function () {
  let session = sessionStorage.getItem("DataUser");
  if (session != null) {
    SetDatepicker();
    $(".uang").mask("000.000.000.000", {
      reverse: true,
    });

    CKEDITOR.editorConfig = function (config) {
      config.entities_latin = false;
      config.entities_greek = false;
      config.entities = false;
      config.basicEntities = false;
    };
    CKEDITOR.on("instanceReady", function () {
      $.each(CKEDITOR.instances, function (instance) {
        CKEDITOR.instances[instance].on("change", function (e) {
          for (instance in CKEDITOR.instances)
            CKEDITOR.instances[instance].updateElement();
        });
      });
    });
  }
});

function GlobalAjax(Url) {
  var ResultArray = $.ajax({
    url: Url,
    type: "GET",
    dataType: "json",
    async: false,
  });

  var result = ResultArray.responseJSON; //JSON.stringify
  if (result.IsSuccess == false) {
    iziToast.error({
      title: "Gagal Menampilkan Data",
      message: result.ReturnMessage,
      position: "topRight",
    });
  } else {
    return result.Data;
  }
}
jQuery.fn.serializeObject = function () {
  var arrayData, objectData;
  arrayData = this.serializeArray();
  objectData = {};

  $.each(arrayData, function () {
    var value;

    if (this.value != null) {
      value = this.value;
    } else {
      value = "";
    }

    if (objectData[this.name] != null) {
      if (!objectData[this.name].push) {
        objectData[this.name] = [objectData[this.name]];
      }

      objectData[this.name].push(value);
    } else {
      objectData[this.name] = value;
    }
  });

  return objectData;
};

function DestroyDatepicker() {
  $('[data-toggle="datepicker"]').datepicker("destroy");
}

function SetDatepicker() {
  var Datex = new Date();
  var NextDay = Datex.setDate(Datex.getDate() + 1);

  $('[data-toggle="datepicker"]').datepicker({
    format: "dd-mm-yyyy",
    pick: function (e) {
      var date = e.date.getDate();
      jQuery(this).attr("value", date);
    },
  });
  $('[data-toggle="datepicker-min-today"]').datepicker({
    format: "dd-mm-yyyy",
    startDate: GetCurrentDate("TanggalOnly"),
  });
  $('[data-toggle="datepicker-min-nextday"]').datepicker({
    format: "dd-mm-yyyy",
    startDate: NextDay,
  });
  $('[data-toggle="datepicker-maxtoday"]').datepicker({
    format: "dd-mm-yyyy",
    startDate: "01-01-0001",
    endDate: GetCurrentDate("TanggalOnly"),
  });
  $('[data-toggle="datepicker-maxtoday-7"]').datepicker({
    format: "dd-mm-yyyy",
    startDate: "01-01-0001",
    endDate: GetCurrentDate("TanggalOnly") + 7,
  });
  $(".clockpickers").clockpicker({
    donetext: "Done",
  });
}

function Logout() {
  sessionStorage.clear();
  localStorage.clear();
  window.location = base_url + "/Home/Index";
}

function ucwords(str, force) {
  str = force ? str.toLowerCase() : str;
  return str.replace(/(\b)([a-zA-Z])/g, function (firstLetter) {
    return firstLetter.toUpperCase();
  });
}

function GetCurrentDate(Tipe) {
  if (Tipe == "TanggalOnly") {
    var ResponseCurrDate = GlobalAjax(base_url + "/Commons/GetCurrentDate");
    return ResponseCurrDate;
  }
}

function GetCurrentDateLocal(Tipe) {
  var d = new Date($.now());
  if (Tipe == "TanggalOnly") {
    var date_now = d.getDate();
    var month_now = d.getMonth() + 1;
    if (date_now < 10) {
      date_now = "0" + date_now;
    }
    if (month_now < 10) {
      month_now = "0" + month_now;
    }

    return date_now + "-" + month_now + "-" + d.getFullYear();
  }
}

// modifikasi fungsi seperti dual list box
$(".ComboSumber").on("change", function () {
  var ValueSelected = this.value;
  var TextSelected = $("option:selected", this).text();

  var HtmlSelected =
    "<option value='" +
    ValueSelected +
    "' selected>" +
    TextSelected +
    "</option>";
  $(".ComboTerpilih").append(HtmlSelected);
  $("option:selected", this).remove();
});

$(".ComboTerpilih").on("change", function () {
  var ValueSelected = this.value;
  var TextSelected = $("option:selected", this).text();

  var HtmlSelected =
    "<option value='" + ValueSelected + "'>" + TextSelected + "</option>";
  $(".ComboSumber").append(HtmlSelected);
  $("option:selected", this).remove();
});

function PilihSemua() {
  $(".ComboSumber")
    .find("option")
    .each(function (index, item) {
      var ValueSelected = $(item).val();
      var TextSelected = $(item).text();

      var HtmlSelected =
        "<option value='" +
        ValueSelected +
        "' selected>" +
        TextSelected +
        "</option>";
      $(".ComboTerpilih").append(HtmlSelected);

      $(this).remove();
    });
}

function BatalPilihSemua() {
  $(".ComboTerpilih")
    .find("option")
    .each(function (index, item) {
      var ValueSelected = $(item).val();
      var TextSelected = $(item).text();

      var HtmlSelected =
        "<option value='" + ValueSelected + "'>" + TextSelected + "</option>";
      $(".ComboSumber").append(HtmlSelected);

      $(this).remove();
    });
}

function formatRupiahtoAngka(Rupiah) {
  return parseInt(Rupiah.replace(/,.*|[^0-9]/g, ""), 10);
}

function formatRupiah(angka) {
  var TipeData = jQuery.type(angka);
  if (TipeData == "number") {
    var bilangan = angka;
    var reverse = bilangan.toString().split("").reverse().join(""),
      ribuan = reverse.match(/\d{1,3}/g);
    ribuan = ribuan.join(".").split("").reverse().join("");
    return ribuan;
  }
  return "Tidak Ada";
}

function indoDate(Date) {
  var SplitTanggal = Date.split("-");
  var Hari = SplitTanggal[0];
  var Bulan = SplitTanggal[1];
  var Tahun = SplitTanggal[2];

  var ArrayBulan = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember",
  ];
  if (Bulan < 10) {
    Bulan = Bulan.replace("0", "");
  }

  return Hari + " " + ArrayBulan[Bulan - 1] + " " + Tahun;
}

function setFocusOnDivWithId(elementId) {
  const scrollIntoViewOptions = {
    behavior: "smooth",
    block: "center",
  };
  document.getElementById(elementId).scrollIntoView(scrollIntoViewOptions);
}

//add by eri
// add date 20200316
// validate email and number only
function validateEmail(email) {
  var re =
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function numOnly(event) {
  var key = event.keyCode;
  return (key > 47 && key < 58) || key == 8 || key == 9 || key == 46;
}

function preventNumberInput(e) {
  var keyCode = e.keyCode ? e.keyCode : e.which;
  if ((keyCode > 47 && keyCode < 58) || (keyCode > 95 && keyCode < 107)) {
    e.preventDefault();
  }
}

/// TANGGAL & JAM REALTIME ///
setInterval(function () {
  waktu_realtime();
}, 1);

function waktu_realtime() {
  var tanggal = new Date();
  if (tanggal.getHours() < 10) {
    var jam_saat_ini = "0" + tanggal.getHours();
  } else {
    var jam_saat_ini = tanggal.getHours();
  }
  if (tanggal.getMinutes() < 10) {
    var menit_saat_ini = "0" + tanggal.getMinutes();
  } else {
    var menit_saat_ini = tanggal.getMinutes();
  }
  if (tanggal.getSeconds() < 10) {
    var detik_saat_ini = "0" + tanggal.getSeconds();
  } else {
    var detik_saat_ini = tanggal.getSeconds();
  }
  $("#time_now").html(
    jam_saat_ini + ":" + menit_saat_ini + ":" + detik_saat_ini
  );

  var tanggallengkap = new String();
  var namahari = "Minggu Senin Selasa Rabu Kamis Jumat Sabtu";
  namahari = namahari.split(" ");
  var namabulan =
    "Januari Februari Maret April Mei Juni Juli Agustus September Oktober November Desember";
  namabulan = namabulan.split(" ");
  var tgl = new Date();
  var hari = tgl.getDay();
  var tanggal = tgl.getDate();
  var bulan = tgl.getMonth();
  var tahun = tgl.getFullYear();

  $("#date_now").html(
    namahari[hari] +
      ", " +
      tanggal +
      " " +
      namabulan[bulan] +
      " " +
      tahun +
      " - "
  );
}
/// TANGGAL & JAM REAL TIME ///

function SetFullName(FirstName, MiddleName, LastName) {
  var satu = FirstName == null ? "" : FirstName;
  var dua = MiddleName == null ? "" : MiddleName;
  var tiga = LastName == null ? "" : LastName;
  if (FirstName != null) satu += " ";
  if (MiddleName != null) dua += " ";
  if (LastName != null) tiga += " ";
  return satu + dua + tiga;
}

function DownloadFile(FileName) {
  var url = base_url + "/Commons/DownloadFile?FileName=" + FileName;
  window.open(url, "_blank");
}

function scrollTop() {
  $("html,body").animate(
    {
      scrollTop: 0,
    },
    "slow"
  );
}

// $(window).on('keydown', function (e) {
//     if (e.which == 13) {
//         CariBerkas()
//         return false;
//     }
// });

function DownloadBerkasPpdb(Param) {
  const BerkasPpdb = {
    SertifikatPrestasi: 0,
    FotoSiswa: 1,
    KartuPeserta: 2,
    PerjanjianCicil: 3,
  };
  var formObj = $("#FormPencarianBerkas").serializeObject();
  var BerkasSearchValue = formObj.BerkasSearchValue;

  var IdPpdbDaftar = BerkasSearchValue;
  var url = "";
  if (Param == BerkasPpdb.SertifikatPrestasi) {
    url =
      base_url + "/api/ppdbs/PrintBerkasPrestasi?idppdbdaftar=" + IdPpdbDaftar;
  }
  if (Param == BerkasPpdb.FotoSiswa) {
    url = base_url + "/api/ppdbs/PrintFotoSiswa?idppdbdaftar=" + IdPpdbDaftar;
  }
  if (Param == BerkasPpdb.KartuPeserta) {
    url =
      base_url + "/api/ppdbs/PrintKartuPeserta?idppdbdaftar=" + IdPpdbDaftar;
  }
  if (Param == BerkasPpdb.PerjanjianCicil) {
    url =
      base_url + "/api/ppdbs/PrintPerjanjianCicil?idppdbdaftar=" + IdPpdbDaftar;
  }
  window.open(url, "_blank");
}

$('select[name="BerkasSearchField"]', "#FormPencarianBerkas").on(
  "change",
  function () {
    if (this.value == 1) {
      $('input[name="BerkasSearchValue"]').attr(
        "placeholder",
        "Ketikkan Nomor PPDB"
      );
    } else if (this.value == 2) {
      $('input[name="BerkasSearchValue"]').attr(
        "placeholder",
        "Ketikkan Nomor Induk Siswa"
      );
    } else {
      $('input[name="BerkasSearchValue"]').attr(
        "placeholder",
        "Ketikkan Sesuatu Disini..."
      );
    }
  }
);

function CariBerkas() {
  var formObj = $("#FormPencarianBerkas").serializeObject();
  var BerkasSearchField = formObj.BerkasSearchField;
  var BerkasSearchValue = formObj.BerkasSearchValue;

  if (BerkasSearchField == "") {
    iziToast.error({
      title: "Gagal Mencari Data",
      message: "Silahkan Pilih Combo Pencarian Terlebih Dahulu",
      position: "topRight",
    });
  } else if (BerkasSearchValue == "") {
    iziToast.error({
      title: "Gagal Mencari Data",
      message: "Kolom Pencarian Tidak Boleh Kosong",
      position: "topRight",
    });
  } else {
    if (BerkasSearchField == 1) {
      var Url =
        base_url + "/api/ppdbs/GetPpdbSiswa?IdPpdbDaftar=" + BerkasSearchValue;
    } else if (BerkasSearchField == 2) {
      var Url = base_url + "/Siswas/GetSiswaByNis?Nis=" + BerkasSearchValue;
    }

    $.ajax({
      url: Url,
      type: "GET",
      dataType: "json",
      beforeSend: function (beforesend) {
        ProgressBar("wait");
      },
      success: function (responsesuccess) {
        ProgressBar("success");
        if (responsesuccess.IsSuccess == true) {
          if (BerkasSearchField == 1) {
            CariBerkasPpdb();
          } else if (BerkasSearchField == 2) {
            CariBerkasSiswa();
          }
        } else {
          iziToast.error({
            title: "Data Tidak Ditemukan",
            message: responsesuccess.ReturnMessage,
            position: "topRight",
          });
        }
      },
      error: function (responserror, a, e) {
        ProgressBar("success");
        iziToast.error({
          title: "Error :(",
          message: JSON.stringify(responserror) + " : " + e,
          position: "topRight",
        });
      },
    });
  }
}

function CariBerkasSiswa() {
  var formObj = $("#FormPencarianBerkas").serializeObject();
  $("#ModalSiswa").modal("show");
  $("#FormSiswa")[0].reset();
  $("#BtnAksi").css("display", "none");
  $(".modal-title").html("Form Data Siswa");
  var Url = base_url + "/Siswas/GetSiswaByNis?Nis=" + formObj.BerkasSearchValue;
  $.ajax({
    url: Url,
    type: "GET",
    dataType: "json",
    beforeSend: function (beforesend) {
      ProgressBar("wait");
    },
    success: function (responsesuccess) {
      ProgressBar("success");
      if (responsesuccess.IsSuccess == true) {
        $(".select2-container", "#FormSiswa").attr(
          "style",
          "pointer-events: none;"
        );
        $("#FieldsetFormSiswa").attr("disabled", true);
        var ResponseData = responsesuccess.Data;
        $("input[name='IdSiswa']", "#FormSiswa")
          .val(ResponseData.IdSiswa)
          .attr("readonly", true);
        $("input[name='Nisn']", "#FormSiswa")
          .val(ResponseData.Nisn)
          .attr("readonly", true);
        $("input[name='NisLama']", "#FormSiswa")
          .val(ResponseData.NisLama)
          .attr("readonly", true);
        $("input[name='Nis']", "#FormSiswa").val(ResponseData.Nis);
        $("input[name='Nik']", "#FormSiswa").val(ResponseData.Nik);
        $("input[name='Nama']", "#FormSiswa").val(ResponseData.Nama);
        $("input[name='NamaPanggilan']", "#FormSiswa").val(
          ResponseData.NamaPanggilan
        );
        $("input[name='TempatLahir']", "#FormSiswa").val(
          ResponseData.TempatLahir
        );
        $("input[name='TanggalLahir']", "#FormSiswa").val(
          ResponseData.TanggalLahir
        );
        $("input[name='NoHandphone']", "#FormSiswa").val(
          ResponseData.NoHandphone
        );
        $("input[name='NoDarurat']", "#FormSiswa").val(ResponseData.NoDarurat);
        $("input[name='NoDarurat']", "#FormSiswa").val(ResponseData.NoDarurat);
        $("input[name='Email']", "#FormSiswa").val(ResponseData.Email);
        $("textarea[name='AlamatTinggal']", "#FormSiswa").val(
          ResponseData.AlamatTinggal
        );
        $("textarea[name='AlamatOrtu']", "#FormSiswa").val(
          ResponseData.AlamatOrtu
        );
        $("input[name='NikIbu']", "#FormSiswa").val(ResponseData.NikIbu);
        $("input[name='NamaIbu']", "#FormSiswa").val(ResponseData.NamaIbu);
        $("input[name='NamaInstansiIbu']", "#FormSiswa").val(
          ResponseData.NamaInstansiIbu
        );
        $("input[name='NikAyah']", "#FormSiswa").val(ResponseData.NikAyah);
        $("input[name='NamaAyah']", "#FormSiswa").val(ResponseData.NamaAyah);
        $("input[name='NamaInstansiAyah']", "#FormSiswa").val(
          ResponseData.NamaInstansiAyah
        );
        $("input[name='EmailOrtu']", "#FormSiswa").val(ResponseData.EmailOrtu);

        ComboGetAgama(function (obj) {
          $("select[name='IdAgama']").html(obj);
        }, ResponseData.IdAgama);

        ComboGetJenisPekerjaan(function (obj) {
          $("select[name='IdJenisPekerjaanAyah']").html(obj);
        }, ResponseData.IdJenisPekerjaanAyah);

        ComboGetJenisPekerjaan(function (obj) {
          $("select[name='IdJenisPekerjaanIbu']").html(obj);
        }, ResponseData.IdJenisPekerjaanIbu);

        ComboGetJenisKelamin(function (obj) {
          $("select[name='KdJenisKelamin']").html(obj);
        }, ResponseData.KdJenisKelamin);

        ComboGetJenisPrestasiSiswa(function (obj) {
          $("select[name='IdJenisPrestasi']").html(obj);
        }, ResponseData.IdJenisPrestasi);

        ComboGetUnit(function (obj) {
          $("select[name='IdUnit']").html(obj);
        }, ResponseData.IdUnit);

        $("#ComboFormKelas").removeAttr("style");
        ComboGetKelas(
          function (obj) {
            $("select[name='IdKelas']").html(obj);
          },
          ResponseData.IdUnit,
          ResponseData.IdKelas
        );

        $("#ComboFormKelasParalel").removeAttr("style");
        ComboGetKelasParalel(
          function (obj) {
            $("select[name='IdKelasParalel']").html(obj);
          },
          ResponseData.IdKelas,
          ResponseData.IdKelasParalel
        );
      } else {
        swal({
          title: "Gagal :(",
          text: responsesuccess.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

// function CariBerkasPpdb() {
//     $('#nav-tab a[href="#nav-status"]').tab('show');
//     var formObj = $('#FormPencarianBerkas').serializeObject();
//     var IdPpdbDaftar = formObj.BerkasSearchValue;
//     var url = base_url + '/api/ppdbs/GetPpdbDaftar?idppdbdaftar=' + IdPpdbDaftar;
//     $.ajax({
//         url: url,
//         type: 'GET',
//         dataType: 'JSON',
//         beforeSend: function () {
//             ProgressBar('wait');
//         },
//         success: function (res) {
//             ProgressBar('success');
//             if (res.IsSuccess) {
//                 $('#ModalCariBerkasPpdb').modal('show');
//                 const {
//                     IdPpdbDaftar,
//                     IdJenisKategoriPendaftaran,
//                     IdJenisPendaftaran,
//                     IdJalurPendaftaran,
//                     Agama,
//                     AsalSekolah,
//                     AlamatOrtu,
//                     AlamatTinggal,
//                     Email,
//                     EmailOrtu,
//                     IdAgama,
//                     IdJenisPekerjaanAyah,
//                     IdJenisPekerjaanIbu,
//                     IdJenisPrestasi,
//                     IdKelas,
//                     IdKelasParalel,
//                     IdSiswa,
//                     IdUnit,
//                     JenisKelamin,
//                     JenisPekerjaanAyah,
//                     JenisPekerjaanIbu,
//                     JalurPendaftaran,
//                     JenisKategoriPendaftaran,
//                     JenisPendaftaran,
//                     KdJenisKelamin,
//                     Kelas,
//                     KelasParalel,
//                     Nama,
//                     NamaAyah,
//                     NamaIbu,
//                     NamaInstansiAyah,
//                     NamaInstansiIbu,
//                     NamaPanggilan,
//                     NikSiswa,
//                     NikAyah,
//                     NikIbu,
//                     Nisn,
//                     NoDarurat,
//                     NoHandphoneOrtu,
//                     Status,
//                     StatusBerkas,
//                     StatusBayarPendaftaran,
//                     StatusBayarPendidikan,
//                     StatusObservasi,
//                     StatusProsesPpdb,
//                     TanggalLahir,
//                     TempatLahir,
//                     Unit,
//                     Prestasis,
//                     Foto,
//                     FileKtpIbu,
//                     FileKtpAyah,
//                     FileKartuKeluarga,
//                     FileSuratKesehatan,
//                     FileBebasNarkoba,
//                     FileRaport,
//                     FileKartuPeserta,
//                     FilePerjanjianCicil,
//                     IdPpdbSeragam,
//                 } = res.Data;
//                 if (IdJalurPendaftaran == 2)
//                     $('#sertifikatPrestasi').hide();
//                 if (IdJalurPendaftaran == 1)
//                     $('#sertifikatPrestasi').show();
//                 GetKategoriDaftars(IdJenisKategoriPendaftaran)
//                 GetJenisPendaftarans(IdJenisPendaftaran)
//                 GetJalurPendaftarans(IdJalurPendaftaran)
//                 ComboGetUnit(function (obj) {
//                     $("select[name='IdUnit']").html(obj);
//                 }, IdUnit);

//                 ComboGetKelas(function (obj) {
//                     $("select[name='IdKelas']").html(obj);
//                 }, IdUnit, IdKelas);

//                 ComboGetJenisKelamin(function (obj) {
//                     $("select[name='KdJenisKelamin']").html(obj);
//                 }, KdJenisKelamin);

//                 ComboGetJenisPekerjaan(function (obj) {
//                     $("select[name='IdJenisPekerjaanIbu']").html(obj);
//                 }, IdJenisPekerjaanIbu);

//                 ComboGetJenisPekerjaan(function (obj) {
//                     $("select[name='IdJenisPekerjaanAyah']").html(obj);
//                 }, IdJenisPekerjaanAyah);

//                 ComboGetSeragam(function (obj) {
//                     $("select[name='Seragam']").html(obj);
//                 }, IdPpdbSeragam);

//                 $('span#id_ppdb_daftar').html('#' + IdPpdbDaftar)
//                 $('input#IdPpdbDaftar').val(IdPpdbDaftar)
//                 $('input#NikSiswa').val(NikSiswa)
//                 $('input#Nama').val(Nama)
//                 $('input#NamaPanggilan').val(NamaPanggilan)
//                 $('input#KdJenisKelamin').val(KdJenisKelamin)
//                 $('textarea#AlamatTinggal').val(AlamatTinggal)
//                 $('select#IdAgama').val(IdAgama)
//                 $('input#TempatLahir').val(TempatLahir)
//                 $('input#TanggalLahir').val(TanggalLahir)
//                 $('input#AsalSekolah').val(AsalSekolah)
//                 $('input#NikIbu').val(NikIbu)
//                 $('input#NamaIbu').val(NamaIbu)
//                 $('select#IdJenisPekerjaanIbu').val(IdJenisPekerjaanIbu)
//                 $('input#NamaInstansiIbu').val(NamaInstansiIbu)
//                 $('input#NikAyah').val(NikAyah)
//                 $('input#NamaAyah').val(NamaAyah)
//                 $('select#IdJenisPekerjaanAyah').val(IdJenisPekerjaanAyah)
//                 $('input#NamaInstansiAyah').val(NamaInstansiAyah)
//                 $('input#AlamatOrtu').val(AlamatOrtu)
//                 $('input#NoHandphoneOrtu').val(NoHandphoneOrtu)
//                 $('input#EmailOrtu').val(EmailOrtu)

//                 //$('span#htmlImageSiswa').html('<img alt="image" src="' + base_url + '/FileSiswa/' + IdPpdbDaftar + '.jpeg" class="rounded-circle author-box-picture">')
//                 $('span#htmlNamaSiswa').html(Nama)
//                 $('span#cb_kategori').html(JenisKategoriPendaftaran)
//                 $('span#cb_jenjang_pendidikan').html(Unit)
//                 $('span#cb_kelas').html(Kelas)
//                 $('span#cb_jenis_pendaftaran').html(JenisPendaftaran)

//                 $('span#cb_status_proses').html(Status != null ? Status : 'Dalam Proses')
//                 $('span#cb_status_berkas').html(StatusBerkas != null ? StatusBerkas : 'Dalam Proses')
//                 $('span#cb_status_bayar_daftar').html(StatusBayarPendaftaran != null ? StatusBayarPendaftaran : 'Dalam Proses')
//                 $('span#cb_status_observasi').html(StatusObservasi != null ? StatusObservasi : 'Dalam Proses')
//                 $('span#cb_status_bayar_pendidikan').html(StatusBayarPendidikan != null ? StatusBayarPendidikan : 'Dalam Proses')
//                 $('span#cb_status_siswa').html(StatusProsesPpdb)

//                 if (IdJalurPendaftaran == 1)
//                     $('small#infoPrestasi').show();
//                 else
//                     $('small#infoPrestasi').hide();

//                 $.each(Prestasis, (i, v) => {
//                     i = i + 1;
//                     CreateFormPrestasiss();
//                     setTimeout((e) => {
//                         GetJenisPrestasis(i, v.IdJenisPrestasi);
//                         GetTingkatPrestasiSiswas(i, v.IdJenisTingkatPrestasi);
//                         GetTahuns(i, v.Tahun);
//                         $('#IdPpdbDaftarPrestasi' + i).val(v.IdPpdbDaftarPrestasi);
//                         $('#NamaPenyelenggara' + i).val(v.NamaPenyelenggara);
//                         $('#Keterangan' + i).val(v.Keterangan);
//                     }, 1000)
//                 });

//                 if (Foto != null) {
//                     $('#fotoSiswa').fadeIn();
//                     $('#linkFotoSiswa').attr('href', base_url + '/Asset/Files/Ppdb/Berkas/' + Foto);
//                 } else {
//                     $('#fotoSiswa').hide();
//                 }

//                 if (FileKtpIbu != null) {
//                     $('#ktpIbu').fadeIn();
//                     $('#linkKtpIbu').attr('href', base_url + '/Asset/Files/Ppdb/Berkas/' + FileKtpIbu);
//                 } else {
//                     $('#ktpIbu').hide();
//                 }

//                 if (FileKtpAyah != null) {
//                     $('#ktpAyah').fadeIn();
//                     $('#linkKtpAyah').attr('href', base_url + '/Asset/Files/Ppdb/Berkas/' + FileKtpAyah);
//                 } else {
//                     $('#ktpAyah').hide();
//                 }

//                 if (FileKartuKeluarga != null) {
//                     $('#kartuKeluarga').fadeIn();
//                     $('#linkKartuKeluarga').attr('href', base_url + '/Asset/Files/Ppdb/Berkas/' + FileKartuKeluarga);
//                 } else {
//                     $('#kartuKeluarga').hide();
//                 }

//                 if (FileSuratKesehatan != null) {
//                     $('#suratKesehatan').fadeIn();
//                     $('#linkAkte').attr('href', base_url + '/Asset/Files/Ppdb/Berkas/' + FileAkte);
//                     $('#linkKartuKeluarga').attr('href', base_url + '/Asset/Files/Ppdb/Berkas/' + FileSuratKesehatan);
//                     $('#linkBebasNarkoba').attr('href', base_url + '/Asset/Files/Ppdb/Berkas/' + FileBebasNarkoba);
//                 } else {
//                     $('#suratKesehatan').hide();
//                 }

//                 if (FileBebasNarkoba != null) {
//                     $('#bebasNarkoba').fadeIn();
//                     $('#linkBebasNarkoba').attr('href', base_url + '/Asset/Files/Ppdb/Berkas/' + FileBebasNarkoba);
//                 } else {
//                     $('#bebasNarkoba').hide();
//                 }

//                 if (FileKartuPeserta != null) {
//                     $('#kartuPeserta').fadeIn();
//                     $('#linkKartuPeserta').attr('href', base_url + '/Asset/Files/Ppdb/Berkas/' + FileKartuPeserta);
//                 } else {
//                     $('#kartuPeserta').hide();
//                 }

//                 if (FilePerjanjianCicil != null) {
//                     $('#perjanjianCicil').fadeIn();
//                     $('#linkPerjanjianCicil').attr('href', base_url + '/Asset/Files/Ppdb/Berkas/' + FilePerjanjianCicil);
//                 } else {
//                     $('#perjanjianCicil').hide();
//                 }

//                 if (FileRaport != null) {
//                     $('#raport').fadeIn();
//                     $('#linkRaport').attr('href', base_url + '/Asset/Files/Ppdb/Berkas/' + FileRaport);
//                 } else {
//                     $('#raport').hide();
//                 }

//                 if (Prestasis != null) {
//                     $.each(Prestasis, (i, v) => {
//                         $('#sertifikatPrestasi').fadeIn();
//                         $('#linkSertifikatPrestasi').attr('href', base_url + "/FilePrestasi/" + v.FilePrestasi);
//                         return i < 0;
//                     });
//                 } else {
//                     $('#sertifikatPrestasi').hide();
//                 }

//             } else if (!res.IsSuccess) {
//                 swal({
//                     title: 'Gagal',
//                     text: res.ReturnMessage,
//                     confirmButtonClass: 'btn-danger text-white',
//                     confirmButtonText: 'Oke, Mengerti',
//                     type: 'error'
//                 });
//             }

//         },
//         error: function (xhr, error, status) {
//             swal({
//                 title: 'Terjadi Kesalahan',
//                 text: JSON.stringify(xhr) + " : " + e,
//                 confirmButtonClass: 'btn-danger text-white',
//                 confirmButtonText: 'Oke, Mengerti',
//                 type: 'error'
//             });
//         }
//     })
// }

const CariBerkasPpdb = () => {
  $('#nav-tab a[href="#nav-status"]').tab("show");
  var formObj = $("#FormPencarianBerkas").serializeObject();
  var IdPpdbDaftar = formObj.BerkasSearchValue;
  var url = base_url + "/api/ppdbs/GetPpdbSiswa?IdPpdbDaftar=" + IdPpdbDaftar;
  var res = initAjax(url);
  if (res) ProgressBar("success");
  if (res.IsSuccess) {
    var data = res.Data;
    $("#ModalCariBerkasPpdb").modal("show");
    $("#id_ppdb_daftar").html(IdPpdbDaftar);
    $("#CBInfoStatus").html(data.Status);
    $("#CBInfoStatusProses").html(
      data.StatusProses == null ? "Menunggu diproses" : data.StatusProses
    );
    $("#CBInfoStatusBiaya").html(data.StatusBiaya);

    $("#DivInfoSodara").hide();
    if (data.NamaSodara != null) {
      $("#DivInfoSodara").fadeIn();
      $("#InfoNamaSodara").html(data.NamaSodara);
    }
    $("#DivInfoAnakPegawai").hide();
    if (data.NamaPegawai != null) {
      $("#DivInfoAnakPegawai").fadeIn();
      $("#InfoNamaPegawai").html(data.NamaPegawai);
    }
    $("#DivInfoCatatanProses").hide();
    if (data.CatatanProses != null) {
      $("#DivInfoCatatanProses").fadeIn();
      $("#InfoCatatanProses").html(data.CatatanProses);
    }
    $("#CBIdPpdbDaftar").val(data.IdPpdbDaftar);
    $("#CBJenisKategoriPendaftaran").val(data.JenisKategoriPendaftaran);
    $("#CBJenisPendaftaran").val(data.JenisPendaftaran);
    $("#CBJalurPendaftaran").val(data.JalurPendaftaran);
    $("#CBUnit").val(data.Unit);
    $("#CBKelas").val(data.Kelas);
    if (data.Unit == "SMAIT AT Taufiq")
      $("#CBKelas").val(data.Kelas + " - " + data.Peminatan);
    $("#CBTanggalLahir").val(data.TanggalLahir);

    $("#CBIdPpdbDaftar").val(data.IdPpdbDaftar);
    ComboGetSeragam(function (obj) {
      $("select[name='CBIdPpdbSeragam']").html(obj);
    }, data.IdPpdbSeragam);
    ComboGetAgama(function (obj) {
      $("select[name='CBIdAgama']").html(obj);
    }, data.IdAgama);
    ComboGetJenisWargaNegara(function (obj) {
      $("select[name='CBIdKewarganegaraan']").html(obj);
    }, data.IdKewarganegaraan);
    ComboGetTinggalSiswa(function (obj) {
      $("select[name='CBIdTinggal']").html(obj);
    }, data.IdTinggal);
    ComboGetJenisTransportasi(function (obj) {
      $("select[name='CBIdTransportasi']").html(obj);
    }, data.IdTransportasi);
    ComboGetJenisBahasa(function (obj) {
      $("select[name='CBIdBahasa']").html(obj);
    }, data.IdBahasa);
    $("#CBNik").val(data.Nik);
    $("#CBNisn").val(data.Nisn);
    $("#CBNama").val(data.Nama);
    $("#CBNamaPanggilan").val(data.NamaPanggilan);
    $("#CBTempatLahir").val(data.TempatLahir);
    $("#CBKdJenisKelamin").val(data.KdJenisKelamin);
    $("#CBKdGolonganDarah").val(data.KdGolonganDarah);
    $("#CBAlamatTinggal").val(data.AlamatTinggal);
    $("#CBEmail").val(data.Email);
    $("#CBNoHandphone").val(data.NoHandphone);
    $("#CBNoDarurat").val(data.NoDarurat);
    $("#CBTinggiBadan").val(data.TinggiBadan);
    $("#CBBeratBadan").val(data.BeratBadan);
    $("#CBAnakKe").val(data.AnakKe);
    $("#CBJumlahSodaraKandung").val(data.JumlahSodaraKandung);
    $("#CBJumlahSodaraTiri").val(data.JumlahSodaraTiri);
    $("#CBJumlahSodaraAngkat").val(data.JumlahSodaraAngkat);
    $("#CBJarakKeSekolah").val(data.JarakKeSekolah);
    $("#CBWaktuTempuh").val(data.WaktuTempuh);
    $("#CBPenyakitDiderita").val(data.PenyakitDiderita);
    $("#CBKelainanJasmani").val(data.KelainanJasmani);
    $("#CBSekolahAsal").val(data.SekolahAsal);
    $("#CBAlamatSekolahAsal").val(data.AlamatSekolahAsal);
    $("#CBNspnSekolahAsal").val(data.NspnSekolahAsal);

    //ortu
    $("#CBDivSudahTiadaIbu").hide();
    if (data.DataOrtu[0].IdStatusHidup == 0) {
      $("#CBDivSudahTiadaIbu").fadeIn();
    }
    $("#CBIdTipeIbu").val(data.DataOrtu[0].IdTipe);
    $("#CBNamaIbu").val(data.DataOrtu[0].Nama);
    $("#CBAlamatIbu").val(data.DataOrtu[0].Alamat);
    $("#CBNikIbu").val(data.DataOrtu[0].Nik);
    $("#CBNamaInstansiIbu").val(data.DataOrtu[0].NamaInstansi);
    $("#CBJabatanIbu").val(data.DataOrtu[0].Jabatan);
    $("#CBEmailIbu").val(data.DataOrtu[0].Email);
    $("#CBNoHandphoneIbu").val(data.DataOrtu[0].NoHandphone);
    $("#CBNoTelpRumahIbu").val(data.DataOrtu[0].NoTelpRumah);
    ComboGetJenisPekerjaan(function (obj) {
      $("#CBIdJenisPekerjaanIbu").html(obj);
    }, data.DataOrtu[0].IdJenisPekerjaan);
    ComboGetAgama(function (obj) {
      $("#CBIdAgamaIbu").html(obj);
    }, data.DataOrtu[0].IdAgama);
    ComboGetJenisWargaNegara(function (obj) {
      $("#CBIdKewarganegaraanIbu").html(obj);
    }, data.DataOrtu[0].IdKewarganegaraan);
    ComboGetListJenjangPendidikan(function (obj) {
      $("#CBIdJenjangPendidikanIbu").html(obj);
    }, data.DataOrtu[0].IdJenjangPendidikan);
    $("#CBIdStatusPenikahanIbu").val(data.DataOrtu[0].IdStatusPenikahan);
    $("#CBTempatLahirIbu").val(data.DataOrtu[0].TempatLahir);
    $("#CBTanggalLahirIbu").val(data.DataOrtu[0].TanggalLahir);
    $("#CBIdStatusHidupIbu").val(data.DataOrtu[0].IdStatusHidup);
    $("#CBTanggalMeninggalIbu").val(data.DataOrtu[0].TanggalMeninggal);

    $("#CBDivSudahTiadaAyah").hide();
    if (data.DataOrtu[1].IdStatusHidup == 0) {
      $("#CBDivSudahTiadaAyah").fadeIn();
    }
    $("#CBIdTipeAyah").val(data.DataOrtu[1].IdTipe);
    $("#CBNamaAyah").val(data.DataOrtu[1].Nama);
    $("#CBAlamatAyah").val(data.DataOrtu[1].Alamat);
    $("#CBNikAyah").val(data.DataOrtu[1].Nik);
    $("#CBNamaInstansiAyah").val(data.DataOrtu[1].NamaInstansi);
    $("#CBJabatanAyah").val(data.DataOrtu[1].Jabatan);
    $("#CBEmailAyah").val(data.DataOrtu[1].Email);
    $("#CBNoHandphoneAyah").val(data.DataOrtu[1].NoHandphone);
    $("#CBNoTelpRumahAyah").val(data.DataOrtu[1].NoTelpRumah);
    ComboGetJenisPekerjaan(function (obj) {
      $("#CBIdJenisPekerjaanAyah").html(obj);
    }, data.DataOrtu[1].IdJenisPekerjaan);
    ComboGetAgama(function (obj) {
      $("#CBIdAgamaAyah").html(obj);
    }, data.DataOrtu[1].IdAgama);
    ComboGetJenisWargaNegara(function (obj) {
      $("#CBIdKewarganegaraanAyah").html(obj);
    }, data.DataOrtu[1].IdKewarganegaraan);
    ComboGetListJenjangPendidikan(function (obj) {
      $("#CBIdJenjangPendidikanAyah").html(obj);
    }, data.DataOrtu[0].IdJenjangPendidikan);
    $("#CBIdStatusPenikahanAyah").val(data.DataOrtu[1].IdStatusPenikahan);
    $("#CBTempatLahirAyah").val(data.DataOrtu[1].TempatLahir);
    $("#CBTanggalLahirAyah").val(data.DataOrtu[1].TanggalLahir);
    $("#CBIdStatusHidupAyah").val(data.DataOrtu[1].IdStatusHidup);
    $("#CBTanggalMeninggalAyah").val(data.DataOrtu[1].TanggalMeninggal);

    //berkas
    $("#CBDivSuratKesehatan").hide();
    $("#CBDivBebasNarkoba").hide();
    if (data.Unit == "SMAIT AT Taufiq") {
      $("#CBDivSuratKesehatan").fadeIn();
      $("#CBDivBebasNarkoba").fadeIn();
    }
    $("#CBImgFileFoto").attr(
      "src",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.Foto
    );
    $("#CBImgFileKtpIbu").attr(
      "src",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileKtpIbu
    );
    $("#CBImgFileKtpAyah").attr(
      "src",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileKtpAyah
    );
    $("#CBImgFileKk").attr(
      "src",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileKartuKeluarga
    );
    $("#CBImgFileAkte").attr(
      "src",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileAkte
    );
    $("#CBImgFileSuratKesehatan").attr(
      "src",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileSuratKesehatan
    );
    $("#CBImgFileBebasNarkoba").attr(
      "src",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileBebasNarkoba
    );

    $("#linkFotoSiswa").attr(
      "href",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.Foto
    );
    $("#linkAkte").attr(
      "href",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileAkte
    );
    $("#linkSuratKesehatan").attr(
      "href",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileSuratKesehatan
    );
    $("#linkBebasNarkoba").attr(
      "href",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileBebasNarkoba
    );
    $("#linkKtpIbu").attr(
      "href",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileKtpIbu
    );
    $("#linkKtpAyah").attr(
      "href",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileKtpAyah
    );
    $("#linkKartuKeluarga").attr(
      "href",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileKartuKeluarga
    );
    $("#linkKartuPeserta").attr(
      "href",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileKartuPeserta
    );

    //biaya
    $("#DivInfoCicil").hide();
    $("#DivTagCil").hide();
    if (data.TagCicil.length > 0) {
      $("#DivInfoCicil").fadeIn();
      $("#DivTagCil").fadeIn();
    }

    var htmlTagCicil = ``;
    $.each(data.TagCicil, (i, v) => {
      htmlTagCicil += `
            <div class="col-md-6">
                <div class="form-group form-row">
                    <label class="col-form-label col-sm-3" style="text-align:left;">Cicil Ke:</label>
                    <div class="col-sm-9">
                        <input type="text" id="CilKe[]" name="CilKe[]" class="form-control" value="${v.CilKe}" readonly />
                    </div>
                </div>`;
      htmlTagCicil += `
                    <div class="form-group form-row">
                        <label class="col-form-label col-sm-3" style="text-align:left;">Jumlah Cicil:</label>
                        <div class="col-sm-9">
                            <input type="text" id="RpCilKe[]" name="RpCilKe[]" class="form-control" value="${v.RpCilKe.toLocaleString(
                              "id-ID"
                            )}" readonly />
                        </div>
                    </div>`;

      htmlTagCicil += `
                <div class="form-group form-row">
                    <label class="col-form-label col-sm-3" style="text-align:left;">Tanggal Cicil:</label>
                    <div class="col-sm-9">
                        <input type="text" id="TanggalCilKe[]" name="TanggalCilKe[]" class="form-control" value="${indoDate(
                          v.TanggalCilKe
                        )}" readonly />
                    </div>
                </div>
                <div class="form-group form-row">
                    <label class="col-form-label col-sm-3" style="text-align:left;">Status:</label>
                    <div class="col-sm-9">
                        <input type="text" id="Status[]" name="Status[]" class="form-control" value="${
                          v.Status
                        }" readonly />
                    </div>
                </div>
                <div class="form-group form-row">
                    <label class="col-form-label col-sm-3" style="text-align:left;">Tanggal Lunas:</label>
                    <div class="col-sm-9">
                        <input type="text" id="TanggalLunas[]" name="TanggalLunas[]" class="form-control" value="${
                          v.TanggalLunas && indoDate(v.TanggalLunas)
                        }" readonly />
                    </div>
                </div>
                <hr />
            </div>
            `;
    });
    $("#DivTagCil").html(htmlTagCicil);

    var htmlTagPpdb = ``;
    $.each(data.TagPpdb, (i, v) => {
      htmlTagPpdb += `
            <div class="col-md-6">
            <div class="form-group form-row">
                <label class="col-form-label col-sm-3" style="text-align:left;">Jenis Biaya:</label>
                <div class="col-sm-9">
                    <input type="hidden" id="IdJenisBiayaPpdb[]" name="IdJenisBiayaPpdb[]" class="form-control" value="${v.IdJenisBiayaPpdb}" />
                    <input type="text" id="JenisBiayaPpdb[]" name="JenisBiayaPpdb[]" class="form-control" value="${v.JenisBiayaPpdb}" readonly />
                </div>
            </div>`;
      htmlTagPpdb += `
            <div class="form-group form-row">
                <label class="col-form-label col-sm-3" style="text-align:left;">Jumlah:</label>
                <div class="col-sm-9">
                    <input type="text" id="Rp[]" name="Rp[]" class="form-control" value="${formatRupiah(
                      v.Rp
                    )}" readonly />
                </div>
            </div>`;
      htmlTagPpdb += `
            <div class="form-group form-row">
                <label class="col-form-label col-sm-3" style="text-align:left;">Catatan:</label>
                <div class="col-sm-9">
                    <textarea type="text" id="CatatanTag[]" name="CatatanTag[]" class="form-control" style="height:100px" placeholder="Ketikan Catatan (opsional)" readonly>${
                      v.Catatan == null ? "" : v.Catatan
                    }</textarea>
                </div>
            </div>
            <hr />

            </div>
            `;
    });
    $("#DivTagPpdb").html(htmlTagPpdb);
  } else {
    swal({
      title: "Gagal",
      text: res.ReturnMessage,
      confirmButtonClass: "btn-danger text-white",
      confirmButtonText: "Oke, Mengerti",
      type: "error",
    });
  }
};

function GetJenisPrestasis(i, IdJenisPrestasi = 0) {
  $.getJSON(base_url + "/api/ppdbs/GetJenisPrestasis", (res) => {
    let html = '<option value="">Select One</option>';
    if (res.IsSuccess) {
      $.each(res.Data, (i, v) => {
        if (IdJenisPrestasi == v.Id) {
          html +=
            '<option value="' + v.Id + '" selected>' + v.Nama + "</option>";
        } else {
          html += '<option value="' + v.Id + '">' + v.Nama + "</option>";
        }
      });
    } else console.log(res);
    $("#IdJenisPrestasi" + i).html(html);
  });
}

function GetTingkatPrestasiSiswas(i, IdJenisTingkatPrestasi = 0) {
  $.getJSON(base_url + "/api/ppdbs/GetTingkatPrestasiSiswas", (res) => {
    let html = '<option value="">Select One</option>';
    if (res.IsSuccess) {
      $.each(res.Data, (i, v) => {
        if (IdJenisTingkatPrestasi == v.Id) {
          html +=
            '<option value="' + v.Id + '" selected>' + v.Nama + "</option>";
        } else {
          html += '<option value="' + v.Id + '">' + v.Nama + "</option>";
        }
      });
    } else console.log(res);
    $("#IdJenisTingkatPrestasi" + i).html(html);
    // $('select[name="IdJenisTingkatPrestasi[]"]').html(html);
  });
}

function GetTahuns(i, Tahun = 0) {
  var StartDate = new Date().getFullYear() - 4;
  var EndDate = new Date().getFullYear();
  var HtmlCombo = "";
  HtmlCombo += "<option value=''>Select One</option>";
  for (var ix = StartDate; ix <= EndDate; ix++) {
    if (Tahun == ix) {
      HtmlCombo += "<option value='" + ix + "' selected>" + ix + "</option>"; //jadikan selected
    } else {
      HtmlCombo += "<option value='" + ix + "'>" + ix + "</option>"; //jadikan selected
    }
  }
  $("#Tahun" + i).html(HtmlCombo);
  // $('select[name="Tahun[]"]').html(HtmlCombo)
}

function ModalPrestasis(bool) {
  // $('#FormPrestasis').removeClass('was-validated');
  if (bool) {
    // $('small#infoPrestasi').fadeIn();
    $("#modalPrestasis").modal("show");
  } else if (!bool) {
    // $('small#infoPrestasi').fadeOut();
    $("#modalPrestasis").modal("hide");
  }
}

function ModalBatalkanPrestasis() {
  var formObj = $("#FormPencarianBerkas").serializeObject();
  var IdPpdbDaftar = formObj.BerkasSearchValue;
  $.ajax({
    url: base_url + "/api/ppdbs/GetPpdbDaftar?IdPpdbDaftar=" + IdPpdbDaftar,
    dataType: "JSON",
    type: "GET",
    success: (res) => {
      $.each(res.Data.Prestasis, (i, v) => {
        i = i + 1;
        DeleteFormPrestasis(i);
      });
    },
  });
  ModalPrestasis(false);
  // $('select#IdJalurPendaftaran').val('');
  // $('#FormPrestasis')[0].reset();
}

var xInputPrestasis = 1;

function CreateFormPrestasiss() {
  var HtmlData = "";
  HtmlData +=
    '<div class="form-row FormPrestasis" id="InputPrestasis_' +
    xInputPrestasis +
    '">' +
    '    <input type="hidden" class="form-control" name="IdPpdbDaftarPrestasi[]" id="IdPpdbDaftarPrestasi' +
    xInputPrestasis +
    '"/>' +
    '    <div class="form-group col-md-4">' +
    '        <label for=""> Prestasi</label>' +
    '        <select disabled class="form-control" name="IdJenisPrestasi[]" id="IdJenisPrestasi' +
    xInputPrestasis +
    '" data-input="wajib-prestasis" required>' +
    "        </select>" +
    '        <div class="invalid-feedback">' +
    "            Kolom Wajib diisi." +
    "        </div>" +
    "    </div>" +
    '    <div class="form-group col-md-4">' +
    '        <label for="">Tingkat Prestasi</label>' +
    '        <select disabled class="form-control" name="IdJenisTingkatPrestasi[]" id="IdJenisTingkatPrestasi' +
    xInputPrestasis +
    '" data-input="wajib-prestasis" required>' +
    '            <option value="">Select One</option>' +
    "        </select>" +
    '        <div class="invalid-feedback">' +
    "            Kolom Wajib diisi." +
    "        </div>" +
    "    </div>" +
    '    <div class="form-group col-md-4">' +
    '        <label for="">Tahun</label>' +
    '        <select disabled class="form-control" name="Tahun[]" id="Tahun' +
    xInputPrestasis +
    '" data-input="wajib-prestasis" required>' +
    '            <option value="">Select One</option>' +
    "        </select>" +
    '        <div class="invalid-feedback">' +
    "            Kolom Wajib diisi." +
    "        </div>" +
    "    </div>" +
    '    <div class="form-group col-md-6">' +
    '        <label for="">Nama Penyelenggara</label>' +
    '        <input disabled type="text" class="form-control" name="NamaPenyelenggara[]" id="NamaPenyelenggara' +
    xInputPrestasis +
    '" placeholder="Ketik Nama Penyelenggara..." data-input="wajib-prestasis" required />' +
    '        <div class="invalid-feedback">' +
    "            Kolom Wajib diisi." +
    "        </div>" +
    "    </div>" +
    '    <div class="form-group col-md-6">' +
    '        <label for="">Keterangan</label> (optional)' +
    '        <input disabled type="text" class="form-control" name="Keterangan[]" id="Keterangan' +
    xInputPrestasis +
    '" placeholder="Ketik Keterangan..." />' +
    "    </div>" +
    '    <div class="form-group col-md-12"><hr /></div>' +
    "</div>";
  $("#formPrestasis").append(HtmlData);
  GetJenisPrestasis(xInputPrestasis);
  GetTingkatPrestasiSiswas(xInputPrestasis);
  GetTahuns(xInputPrestasis);
  xInputPrestasis++;
  SetDatepicker();
}

function DeleteFormPrestasis(IdBtn) {
  $("#InputPrestasis_" + IdBtn).remove();
  xInputPrestasis--;
}

function DownloadBerkasPrestasi() {
  var IdPpdbDaftar = $("#IdPpdbDaftar").val();
  var url =
    base_url + "/api/ppdbs/PrintBerkasPrestasi?idppdbdaftar=" + IdPpdbDaftar;
  window.open(url, "_blank");
}

// function GetKategoriDaftars(Id) {
//     $.getJSON(base_url + "/api/ppdbs/GetKategoriDaftars", (res) => {
//         let html = '<option value="">- Pilih Salah Satu -</option>';
//         if (res.IsSuccess) {
//             $.each(res.Data, (i, v) => {
//                 if (v.Id == Id)
//                     html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
//                 else
//                     html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
//             });
//         } else console.log(res)
//         $('select[name="IdJenisKategoriPendaftaran"]').html(html);
//     });
// }

// function GetJenisPendaftarans(Id) {
//     $.getJSON(base_url + "/api/ppdbs/GetJenisPendaftarans", (res) => {
//         let html = '<option value="">- Pilih Salah Satu -</option>';
//         if (res.IsSuccess) {
//             $.each(res.Data, (i, v) => {
//                 if (v.Id == Id)
//                     html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
//                 else
//                     html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
//             });
//         } else console.log(res)
//         $('select[name="IdJenisPendaftaran"]').html(html);
//     });
// }

// function GetJalurPendaftarans(Id) {
//     $.getJSON(base_url + "/api/ppdbs/GetJalurPendaftarans", (res) => {
//         let html = '<option value="">- Pilih Salah Satu -</option>';
//         if (res.IsSuccess) {
//             $.each(res.Data, (i, v) => {
//                 if (v.Id == Id)
//                     html += '<option value="' + v.Id + '" selected>' + v.Nama + '</option>';
//                 else
//                     html += '<option value="' + v.Id + '">' + v.Nama + '</option>';
//             });
//         } else console.log(res)
//         $('select[name="IdJalurPendaftaran"]').html(html);
//     });
// }

createYoutubeEmbed = (key) => {
  return "https://www.youtube.com/embed/" + key;
};
getEmbedYt = (text) => {
  if (!text) return text;
  const self = this;

  const linkreg = /(?:)<a([^>]+)>(.+?)<\/a>/g;
  const fullreg =
    /(https?:\/\/)?(www\.)?(youtube\.com\/watch\?v=|youtu\.be\/)([^& \n<]+)(?:[^ \n<]+)?/g;
  const regex =
    /(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/watch\?v=|youtu\.be\/)([^& \n<]+)(?:[^ \n<]+)?/g;

  let resultHtml = text;

  // get all the matches for youtube links using the first regex
  const match = text.match(fullreg);
  if (match && match.length > 0) {
    // get all links and put in placeholders
    const matchlinks = text.match(linkreg);
    if (matchlinks && matchlinks.length > 0) {
      for (var i = 0; i < matchlinks.length; i++) {
        resultHtml = resultHtml.replace(
          matchlinks[i],
          "#placeholder" + i + "#"
        );
      }
    }

    // now go through the matches one by one
    for (var i = 0; i < match.length; i++) {
      // get the key out of the match using the second regex
      let matchParts = match[i].split(regex);
      // replace the full match with the embedded youtube code
      resultHtml = resultHtml.replace(
        match[i],
        self.createYoutubeEmbed(matchParts[1])
      );
    }

    // ok now put our links back where the placeholders were.
    if (matchlinks && matchlinks.length > 0) {
      for (var i = 0; i < matchlinks.length; i++) {
        resultHtml = resultHtml.replace(
          "#placeholder" + i + "#",
          matchlinks[i]
        );
      }
    }
  }
  return resultHtml;
};

cekQuotes = (param1) => {
  let regex = /[^\w\s]/gi;
  let retChar;
  if (regex.test(param1) == true) {
    retChar = param1.replace("'", "\\'");
  } else {
    retChar = param1;
  }
  return retChar;
};

function regexUrl(Text) {
  var a = Text;
  var b = /[^0-9a-zA-Z]+/g;
  var c = a.replace(b, "-");

  var Hasil = $.trim(c).toLowerCase();
  return Hasil;
}

const initAjax = (service) => {
  var dataArray = $.ajax({
    type: "GET",
    async: false,
    beforeSend: function () {
      ProgressBar("wait");
    },
    url: service,
    error: function (err, a, e) {
      ProgressBar("success");
      swal({
        title: "Terjadi Kesalahan",
        text: JSON.stringify(err),
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });

  var result = dataArray.responseJSON; //JSON.stringify

  return result;
};

function remove_quotes(values1) {
  if (values1 == null) return null;
  values1 = values1.replace(/"/g, "\\'");
  var values = values1.toString();
  var str = "";
  var blockList = ['"', "'", "\\"]; // This is the list of key words to be escaped
  var flag = 0;
  for (var i = 0; i < values.length; i++) {
    for (var j = 0; j < blockList.length; j++) {
      if (values[i] == blockList[j]) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) str += values[i];
    else {
      str += "\\";
      str += values[i];
      flag = 0;
    }
  }
  return str;
}

function get_browser() {
  var ua = navigator.userAgent,
    tem,
    M =
      ua.match(
        /(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i
      ) || [];
  if (/trident/i.test(M[1])) {
    tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
    return { name: "IE", version: tem[1] || "" };
  }
  if (M[1] === "Chrome") {
    tem = ua.match(/\bOPR|Edge\/(\d+)/);
    if (tem != null) {
      return { name: "Opera", version: tem[1] };
    }
  }
  M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, "-?"];
  if ((tem = ua.match(/version\/(\d+)/i)) != null) {
    M.splice(1, 1, tem[1]);
  }
  return {
    name: M[0],
    version: M[1],
  };
}

function imgError(image) {
  image.onerror = "";
  image.src = "/assets/img/avatar/avatar-1.png";
  return true;
}
