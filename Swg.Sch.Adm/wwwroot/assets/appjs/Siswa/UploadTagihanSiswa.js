/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 15052020
================================================================================================================= */
function GetNamaSiswa(IdSiswa) {
    var Url = base_url + "/Siswas/GetSiswa?IdSiswa=" + IdSiswa;
    var ResponseData = GlobalAjax(Url);
    var Data = "";
    Data = ResponseData.Nama;
    return Data;
}
function GetNisSiswa(IdSiswa) {
    var Url = base_url + "/Siswas/GetSiswa?IdSiswa=" + IdSiswa;
    var ResponseData = GlobalAjax(Url);
    var Data = "";
    Data = ResponseData.Nis;
    return Data;
}
ComboGetSiswa(function (obj) {
    $("select[name='IdSiswa']").html(obj);
});
ComboGetBulan(function (obj) {
    $("select[name='IdBulan']").html(obj);
});
ComboGetTahun(function (obj) {
    $("select[name='IdTahun']").html(obj);
});

$("select[name='IdSiswa']", "#FormTabelTagihanSiswa").change(function () {
    var formObj = $('#FormTabelTagihanSiswa').serializeObject();
    if (formObj.IdSiswa != "" && formObj.IdBulan != "" && formObj.IdTahun != "") {
        $("#DivTabelTagihanSiswa").removeAttr("style");
        $("#ImgSearch").css("display", "none");

        var formObj = $('#FormTabelTagihanSiswa').serializeObject();
        TabelTagihanSiswa(GetNisSiswa(formObj.IdSiswa), formObj.IdBulan, formObj.IdTahun);
    }
});
$("select[name='IdBulan']", "#FormBerdasarkan").change(function () {
    var formObj = $('#FormBerdasarkan').serializeObject();
    if (formObj.IdSiswa != "" && formObj.IdBulan != "" && formObj.IdTahun != "") {
        $("#DivTabelTagihanSiswa").removeAttr("style");
        $("#ImgSearch").css("display", "none");

        var formObj = $('#FormBerdasarkan').serializeObject();
        // TabelTagihanSiswa(formObj.IdSiswa, formObj.IdBulan, formObj.IdTahun);
    }
});
$("select[name='IdTahun']", "#FormBerdasarkan").change(function () {
    var formObj = $('#FormBerdasarkan').serializeObject();
    if (formObj.IdSiswa != "" && formObj.IdBulan != "" && formObj.IdTahun != "") {
        $("#DivTabelTagihanSiswa").removeAttr("style");
        $("#ImgSearch").css("display", "none");

        var formObj = $('#FormBerdasarkan').serializeObject();
        // TabelTagihanSiswa(formObj.IdSiswa, formObj.IdBulan, formObj.IdTahun);
    }
});

function TabelTagihanSiswa(IdSiswa, IdBulan, IdTahun) {
    var UrlTabel = base_url + "/Siswas/GetSiswaTagihan?Nis=" + IdSiswa; // + "&Bulan=" + IdBulan + "&Tahun=" + IdTahun;
    //////// START SECTION Tabel Tagihan Siswa ///////////
    
    var TabelTagihanSebelumnya = $('#TabelTagihanSebelumnya').DataTable({
        "paging": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": UrlTabel,
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.IsSuccess) {
                    var data = json.Data;
                    $('#ImgEmpty').hide();
                    $('#DivRincianTagihan').fadeIn();
                    $('#IdSiswa').val(data.IdSiswa);
                    $('#InfoNamaSiswa').html(GetNamaSiswa(data.IdSiswa));
                    $('#InfoPeriode').html(data.Periode);
                    $('#InfoTotalTagihan').html(formatRupiah(data.RpTotal));
                    $('#InfoTotalTagihanSebelumnya').html(formatRupiah(data.RpSebelumnya));
                    $('#InfoTotalTagihanBerjalan').html(formatRupiah(data.RpBerjalan));
                    var Status = '';
                    if (data.Status == 'BELUM LUNAS') {
                        Status = '<span class="badge badge-danger">BELUM LUNAS</span>';
                    } else {
                        Status = '<span class="badge badge-success">LUNAS</span>';
                    }
                    $('#InfoStatus').html(Status);
                    $('#InfoTanggalLunas').html(data.TanggalLunas);
                    $('#InfoNomorRef').html(data.RefNo);
                } else {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Tagihan Siswa',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                }
                if (json.Data.TagihanSebelumnya == null) {
                    if (json.ReturnMessage != "data tidak ada") {
                        iziToast.error({
                            title: 'Gagal Menampilkan Data Tabel Tagihan Siswa',
                            message: json.ReturnMessage,
                            position: 'topRight'
                        });
                    }
                    return json;
                } else {
                    return json.Data.TagihanSebelumnya;
                }
            }
        },
        "columns": [
            {
                "data": "JenisTagihan"
            },
            {
                "render": function (data, type, full, meta) {
                    return formatRupiah(parseInt(full.Rp));
                }
            },
            {
                "render": function (data, type, full, meta) {
                    return `<button type="button" class="btn btn-primary btn-sm" onclick="EditTagihan(${full.IdJenisTagihanSiswa}, '${full.JenisTagihan}', ${full.Rp}, 'Tagihan Sebelumnya', '${full.Periode}')"><i class="fa fa-pencil-square-o"></i>  EDIT</button> `;
                }
            },
        ],
        "bDestroy": true
    });
    

    //// perbedaan tagihan ////
    
    var TabelTagihanBerjalan = $('#TabelTagihanBerjalan').DataTable({
        "paging": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": UrlTabel,
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data.TagihanBerjalan == null) {
                    if (json.ReturnMessage != "data tidak ada") {
                        iziToast.error({
                            title: 'Gagal Menampilkan Data Tabel Tagihan Siswa',
                            message: json.ReturnMessage,
                            position: 'topRight'
                        });
                    }
                    return json;
                } else {
                    console.log(json.Data.TagihanBerjalan)
                    return json.Data.TagihanBerjalan;
                }
            }
        },
        "columns": [
            {
                "data": "JenisTagihan"
            },
            {
                "render": function (data, type, full, meta) {
                    return formatRupiah(parseInt(full.Rp));
                }
            },
            {
                "render": function (data, type, full, meta) {
                    return `<button type="button" class="btn btn-primary btn-sm" onclick="EditTagihan(${full.IdJenisTagihanSiswa}, '${full.JenisTagihan}', ${full.Rp}, 'Tagihan Berjalan', '${full.Periode}')"><i class="fa fa-pencil-square-o"></i>  EDIT</button> `;
                }
            },
        ],
        "bDestroy": true
    });
    
    
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
    //////// START SECTION Tabel Tagihan Siswa ///////////
}

function DownloadTemplateTagihan() {
    var formObj = $('#FormBerdasarkan').serializeObject();
    if ((formObj.IdBulan != "") && (formObj.IdTahun != "")) {
        window.open(base_url + "/Siswas/DowloadTemplateTagihan?Tahun=" + parseInt(formObj.IdTahun) + "&Bulan=" + parseInt(formObj.IdBulan), '_blank');
    } else {
        iziToast.error({
            title: 'Gagal Download Template Tagihan',
            message: "Kolom Isian Bulan & Tahun Wajib Di Isi",
            position: 'topRight'
        });
    }
}

function TambahData() {
    $("#ModalUploadTagihanSiswa").modal("show");
    $(".modal-title").html("Upload Tagihan Massal");
    $('#FieldFileUpload').val('');
}
function TambahDataSusulan() {
    $("#ModalUploadTagihanSiswaSusulan").modal("show");
    $(".modal-title").html("Upload Tagihan Susulan");
    $('#FieldFileUploadSusulan').val('');
}

function SaveUploadTagihanSiswa() {
    var formObj = $('#FormUploadTagihanSiswa').serializeObject();
    var formData = new FormData($('#FormUploadTagihanSiswa')[0]);
    $.ajax({
        url: base_url + "/Siswas/UploadTagihanSiswa",
        type: "POST",
        data: formData,
        dataType: "json",
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (responsebefore) {
            ProgressBar("wait");
        },
        success: function (responseupload) {
            ProgressBar("success");
            if (responseupload.IsSuccess == true) {
                $("#DivTabelTagihanSiswa").removeAttr("style");
                $("#ImgSearch").css("display", "none");
                $("#ModalUploadTagihanSiswa").modal("hide");
                swal({
                    title: 'Berhasil Upload Tagihan Massal',
                    text: responseupload.ReturnMessage,
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
            } else if (responseupload.IsSuccess == false) {
                swal({
                    title: 'Gagal Upload Tagihan Massal',
                    text: responseupload.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            ProgressBar("success");
            swal({
                title: 'Gagal Upload Tagihan Siswa',
                text: jqXHR + " " + textStatus + " " + errorThrown,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}
function SaveUploadTagihanSiswaSusulan() {
    var formObj = $('#FormUploadTagihanSiswaSusulan').serializeObject();
    var formData = new FormData($('#FormUploadTagihanSiswaSusulan')[0]);
    $.ajax({
        url: base_url + "/Siswas/UploadTagihanSiswaSusulan",
        type: "POST",
        data: formData,
        dataType: "json",
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (responsebefore) {
            ProgressBar("wait");
        },
        success: function (responseupload) {
            ProgressBar("success");
            if (responseupload.IsSuccess == true) {
                $("#DivTabelTagihanSiswa").removeAttr("style");
                $("#ImgSearch").css("display", "none");
                $("#ModalUploadTagihanSiswaSusulan").modal("hide");
                swal({
                    title: 'Berhasil Upload Tagihan Susulan',
                    text: responseupload.ReturnMessage,
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
            } else if (responseupload.IsSuccess == false) {
                swal({
                    title: 'Gagal Upload Tagihan Susulan',
                    text: responseupload.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            ProgressBar("success");
            swal({
                title: 'Gagal Upload Tagihan Susulan',
                text: jqXHR + " " + textStatus + " " + errorThrown,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function DeleteTagihan(IdSiswa, IdJenisTagihan, Periode, JenisTagihan, Jumlah) {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan melakukan set lunas untuk jenis tagihan ini",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: base_url + '/siswas/DeleteTagihanSiswa?IdSiswa=' + IdSiswa + '&IdJenisTagihanSiswa=' + IdJenisTagihan + '&Periode=' + Periode,
                type: 'POST',
                dataType: 'JSON',
                beforeSend: function () {
                    ProgressBar('wait')
                },
                success: function (res) {
                    ProgressBar('success')
                    // $("#TabelTagihanSiswa").DataTable().ajax.reload();
                    if (res.IsSuccess) {
                        swal({
                            title: 'Sukses',
                            text: 'Berhasil Menghapus Tagihan Siswa',
                            confirmButtonClass: 'btn-success text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'success'
                        });
                        TabelTagihanSiswa(GetNisSiswa(IdSiswa));
                    } else if (!res.IsSuccess) {
                        swal({
                            title: 'Gagal',
                            text: res.ReturnMessage,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                },
                error: function (err, a, e) {
                    ProgressBar("success");
                    swal({
                        title: 'Error :(',
                        text: JSON.stringify(responserror) + " : " + e,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            })
        });
}


function DeleteTagihanMassal() {
    var IdSiswa = $('select[name="IdSiswa"] option:selected').val();
    var NamaSiswa = $('select[name="IdSiswa"] option:selected').text();
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus semua tagihan siswa " + NamaSiswa,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: base_url + '/siswas/DeleteTagihanMassal?IdSiswa=' + IdSiswa,
                type: 'POST',
                dataType: 'JSON',
                beforeSend: function () {
                    ProgressBar('wait')
                },
                success: function (res) {
                    ProgressBar('success')
                    $("#TabelTagihanSiswa").DataTable().ajax.reload();
                    if (res.IsSuccess) {
                        swal({
                            title: 'Sukses',
                            text: 'Berhasil Menghapus Tagihan Siswa',
                            confirmButtonClass: 'btn-success text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'success'
                        });
                    } else if (!res.IsSuccess) {
                        swal({
                            title: 'Gagal',
                            text: res.ReturnMessage,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                },
                error: function (err, a, e) {
                    ProgressBar("success");
                    swal({
                        title: 'Error :(',
                        text: JSON.stringify(responserror) + " : " + e,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            })
        });
}

function EditTagihan(IdJenisTagihanSiswa, JenisTagihan, Rp, Ingfo, Periode) {
    $('#ModalEditTagihanSiswa').modal('show');
    $('input[name="IdJenisTagihanSiswa"]').val(IdJenisTagihanSiswa);
    $('input[name="JenisTagihan"]').val(JenisTagihan);
    $('input[name="Nominal"]').val(Rp);
    $('#AlertTagihan').html(Ingfo);
    $('input[name="Periode"]').val(Periode);
}

function SaveEditTagihan() {
    var formObj = $('#FormEditTagihanSiswa').serializeObject();
    var IdSiswa = $('select[name="IdSiswa"] option:selected').val();
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan melakukan pembaruan nominal untuk jenis tagihan ini",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: base_url + '/siswas/UpdateTagihanSiswa?IdSiswa=' + IdSiswa + '&IdJenisTagihanSiswa=' + formObj.IdJenisTagihanSiswa + '&Periode=' + formObj.Periode + '&Rp=' + formObj.Nominal,
                type: 'POST',
                dataType: 'JSON',
                beforeSend: function () {
                    ProgressBar('wait')
                },
                success: function (res) {
                    ProgressBar('success')
                    if (res.IsSuccess) {
                        swal({
                            title: 'Sukses',
                            text: 'Berhasil Perbarui Tagihan Siswa',
                            confirmButtonClass: 'btn-success text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'success'
                        });
                        TabelTagihanSiswa(GetNisSiswa(IdSiswa));
                        $('#ModalEditTagihanSiswa').modal('hide');
                    } else if (!res.IsSuccess) {
                        swal({
                            title: 'Gagal',
                            text: res.ReturnMessage,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                },
                error: function (err, a, e) {
                    ProgressBar("success");
                    swal({
                        title: 'Error :(',
                        text: JSON.stringify(responserror) + " : " + e,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            })
        });
}