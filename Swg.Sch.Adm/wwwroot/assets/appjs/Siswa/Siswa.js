﻿/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 15052020
================================================================================================================= */
$.fn.modal.Constructor.prototype._enforceFocus = function () {};

///// LOAD AWAL //////
ComboBerdasarkan("0");
//////////////////////

$("select[name='IdTipe']").change(function () {
  ComboTipe(this.value);
});

function ComboTipe(Value) {
  if (Value != "") {
    if (Value == 1) {
      // pencarian
      $("#ComboTahunAjaran").css("display", "none");
      $("#DivBtnArsip").css("display", "none");
      $("#ComboBerdasarkan").removeAttr("style");
      $("#LabelBerdasarkan").text("Berdasarkan");
      $("#DivBtnHapus").css("display", "none");
      $("#DivBtnDownloadSiswa").css("display", "none");
    } else if (Value == 2) {
      // hapus data
      // $("#ComboTahunAjaran").css("display", "none");
      // $("#DivBtnArsip").css("display", "none");
      // $("#ComboBerdasarkan").removeAttr("style");
      // $("#LabelBerdasarkan").text("Berdasarkan");
      // $("#DivBtnHapus").removeAttr("style");
      // $("#DivBtnDownloadSiswa").css("display", "none");
    } else if (Value == 3) {
      // arsip data
      $("#ComboTahunAjaran").removeAttr("style");
      $("#DivBtnArsip").removeAttr("style");

      $("#ComboBerdasarkan").css("display", "none");
      $("#DivBtnHapus").css("display", "none");
      $("#DivBtnDownloadSiswa").css("display", "none");
      $("#ComboUnit").css("display", "none");
      $("#ComboKelas").css("display", "none");
      $("#ComboKelasParalel").css("display", "none");
      ComboGetTahunAjaran(function (obj) {
        $("select[name='IdTahunAjaran']").html(obj);
      });
    } else if (Value == 4) {
      // download data siswa
      $("#ComboTahunAjaran").css("display", "none");
      $("#DivBtnArsip").css("display", "none");
      $("#ComboBerdasarkan").css("display", "none");
      $("#LabelBerdasarkan").text("Berdasarkan");
      $("#DivBtnHapus").css("display", "none");
      $("#DivBtnDownloadSiswa").removeAttr("style");

      $("#ComboUnit").removeAttr("style");
      ComboGetUnitSklh(function (obj) {
        $("select[name='IdUnit']").html(obj);
      });
    }
  } else {
    $("#ComboUnit").css("display", "none");
    $("#ComboKelas").css("display", "none");
    $("#ComboKelasParalel").css("display", "none");
  }
}

$("select[name='IdBerdasarkan']").change(function () {
  ComboBerdasarkan(this.value);
});

function ComboBerdasarkan(Value) {
  if (Value != "") {
    if (Value == 0) {
      $("#TabelSiswa").removeAttr("style");
      $("#ImgSearch").css("display", "none");
      TabelSiswa(0, "");
    } else if (Value == 1) {
      // unit
      $("#ComboKelas").css("display", "none");
      $("#ComboKelasParalel").css("display", "none");

      $("#ComboUnit").removeAttr("style");
      ComboGetUnitSklh(function (obj) {
        $("select[name='IdUnit']").html(obj);
      });
    } else if (Value == 2) {
      // kelas
      $("#ComboKelas").css("display", "none");
      $("#ComboKelasParalel").css("display", "none");

      $("#ComboUnit").removeAttr("style");
      ComboGetUnitSklh(function (obj) {
        $("select[name='IdUnit']").html(obj);
      });
    } else if (Value == 3) {
      // kelas paralel
      $("#ComboUnit").removeAttr("style");
      ComboGetUnitSklh(function (obj) {
        $("select[name='IdUnit']").html(obj);
      });
    }
  } else {
    $("#ComboUnit").css("display", "none");
    $("#ComboKelas").css("display", "none");
    $("#ComboKelasParalel").css("display", "none");
  }
}

$("select[name='IdUnit']").change(function () {
  var formObj = $("#FormTabelSiswa").serializeObject();
  $("#DivBtnDownloadDapodik").fadeIn();
  if (formObj.IdBerdasarkan != "") {
    if (formObj.IdBerdasarkan == 1) {
      $("#TabelSiswa").removeAttr("style");
      $("#ImgSearch").css("display", "none");
      TabelSiswa(1, this.value);
    } else if (formObj.IdBerdasarkan == 2) {
      $("#ComboKelas").css("display", "none");
      $("#ComboKelasParalel").css("display", "none");

      $("#ComboKelas").removeAttr("style");
      ComboGetKelas(function (obj) {
        $("select[name='IdKelas']").html(obj);
      }, this.value);
    } else if (formObj.IdBerdasarkan == 3) {
      $("#ComboKelas").removeAttr("style");
      ComboGetKelas(function (obj) {
        $("select[name='IdKelas']").html(obj);
      }, this.value);
    }
  } else if (formObj.IdTipe != "") {
    if (formObj.IdTipe == 4) {
      $("#TabelSiswa").removeAttr("style");
      $("#ImgSearch").css("display", "none");
      TabelSiswa(1, this.value);
    }
  } else {
    $("#ComboKelas").css("display", "none");
    $("#ComboKelasParalel").css("display", "none");
  }
});

$("select[name='IdKelas']").change(function () {
  var formObj = $("#FormTabelSiswa").serializeObject();
  if (formObj.IdBerdasarkan != "") {
    if (formObj.IdBerdasarkan == 1) {
    } else if (formObj.IdBerdasarkan == 2) {
      $("#TabelSiswa").removeAttr("style");
      $("#ImgSearch").css("display", "none");
      TabelSiswa(2, this.value);
    } else if (formObj.IdBerdasarkan == 3) {
      $("#ComboKelasParalel").removeAttr("style");
      ComboGetKelasParalel(function (obj) {
        $("select[name='IdKelasParalel']").html(obj);
      }, this.value);
    }
  } else {
    $("#ComboKelasParalel").css("display", "none");
  }
});

$("select[name='IdKelasParalel']").change(function () {
  var formObj = $("#FormTabelSiswa").serializeObject();
  if (formObj.IdBerdasarkan != "") {
    if (formObj.IdBerdasarkan == 1) {
    } else if (formObj.IdBerdasarkan == 2) {
    } else if (formObj.IdBerdasarkan == 3) {
      $("#TabelSiswa").removeAttr("style");
      $("#ImgSearch").css("display", "none");
      TabelSiswa(3, this.value);
    }
  } else {
  }
});

function TabelSiswa(SearchField, SearchValue) {
  if (SearchField == 1) {
    var UrlTabel = base_url + "/Siswas/GetSiswasByUnit?IdUnit=" + SearchValue;
  } else if (SearchField == 2) {
    var UrlTabel = base_url + "/Siswas/GetSiswasByKelas?IdKelas=" + SearchValue;
  } else if (SearchField == 3) {
    var UrlTabel =
      base_url +
      "/Siswas/GetSiswasByKelasParalel?IdKelasParalel=" +
      SearchValue;
  } else {
    var UrlTabel = base_url + "/Siswas/GetSiswas";
  }
  //////// START SECTION Tabel Siswa ///////////
  $("#TabelSiswa tfoot th").each(function () {
    var title = $(this).text();
    $(this).html(
      '<input type="text" class="form-control" placeholder="Cari ' +
        title +
        '" />'
    );
  });
  var TabelSiswa = $("#TabelSiswa").DataTable({
    paging: true,
    searching: true,
    ordering: false,
    info: true,
    pageLength: 10,
    lengthChange: true,
    processing: true,
    scrollX: true,
    scrollY: false,
    // "responsive": {
    //     details: {
    //         renderer: $.fn.dataTable.Responsive.renderer.tableAll()
    //     }
    // },
    ajax: {
      url: UrlTabel,
      method: "GET",
      beforeSend: function (xhr) {},
      dataSrc: function (json) {
        if (json.Data == null) {
          iziToast.error({
            title: "Gagal Menampilkan Data Tabel Siswa",
            message: json.ReturnMessage,
            position: "topRight",
          });
          return json;
        } else {
          return json.Data;
        }
      },
      error: function (responserror, a, e) {
        iziToast.error({
          title: "Error : TabelSiswa",
          message: JSON.stringify(responserror) + " : " + e.toString(),
          position: "topRight",
          timeout: 3000,
        });

        return [];
      },
    },

    columns: [
      {
        className: "desktop",
        data: "IdSiswa",
        render: function (data, type, full, meta) {
          var Data = "";
          var ParamEdit = "EditData('" + full.IdSiswa + "')";
          var ParamNonAktif =
            "NonAktif('" + full.IdSiswa + "','" + cekQuotes(full.Nama) + "')";
          Data +=
            '<button type="button" class="btn btn-info btn-sm" onClick="' +
            ParamEdit +
            '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
          if (full.Status == 1) {
            Data += "&nbsp;&nbsp;";
            Data +=
              '<button type="button" class="btn btn-danger btn-sm" onClick="' +
              ParamNonAktif +
              '"><i class="fa fa-ban"></i> Non-Aktifkan</button>';
          }
          return Data;
        },
      },
      {
        className: "desktop mobile tablet",
        data: "Nama",
      },
      {
        className: "desktop mobile tablet",
        data: "Nis",
      },
      {
        render: function (data, type, full, meta) {
          var data = `<code>${full.Password}</code>`;
          return data;
        },
      },
      {
        className: "desktop",
        data: "Pin",
      },
      {
        className: "desktop",
        data: "Unit",
      },
      {
        className: "desktop",
        data: "TanggalLahir",
        render: function (data, type, full, meta) {
          var Data = "";
          Data = indoDate(full.TanggalLahir);
          return Data;
        },
      },
      {
        className: "desktop",
        data: "Nik",
      },
      {
        className: "desktop",
        data: "StrStatus",
        render: function (data, type, full, meta) {
          var Data = "";
          if (full.Status == 1) {
            Data +=
              '<span class="badge badge-success"><i class="fa fa-check"></i> ' +
              full.StrStatus +
              "</span>";
          } else {
            Data +=
              '<span class="badge badge-danger"><i class="fa fa-close"></i> ' +
              full.StrStatus +
              "</span>";
          }
          return Data;
        },
      },
      {
        className: "desktop",
        data: "NamaIbu",
      },
      {
        className: "desktop",
        data: "NamaAyah",
      },
    ],
    bDestroy: true,
  });
  TabelSiswa.columns().every(function () {
    var that = this;

    $("input", this.footer()).on("keyup change clear", function () {
      if (that.search() !== this.value) {
        that.search(this.value).draw();
      }
    });
  });
  //////// START SECTION TABEL SISWA ///////////
  $(".dataTables_filter").css("display", "none");
  $(".dataTables_length").css("display", "none");
}

$("input[name='AlamatOrtuSamaDenganAlamatSiswa']").change(function () {
  var formObj = $("#FormSiswa").serializeObject();
  if ($(this).prop("checked") == true) {
    $("textarea[name='AlamatOrtu']")
      .val(formObj.AlamatTinggal)
      .attr("readonly", true);
  } else if ($(this).prop("checked") == false) {
    $("textarea[name='AlamatOrtu']").val("").removeAttr("readonly");
  }
});

function TambahManual() {
  $("#ModalKonfirmasiTambahData").modal("hide");
  $("#ModalSiswa").modal("show");
  $(".modal").css("overflow-y", "auto");
  $("#FormSiswa")[0].reset();
  $("#BtnSaveManualSiswa").attr("onClick", "SaveData('TambahData');");
  $(".modal-title").html("Form Tambah Data Siswa");

  $(".select2-container", "#FormSiswa").removeAttr("style");
  $("#FieldsetFormSiswa").removeAttr("disabled");
  $("#BtnAksi").removeAttr("style");

  $("input[name='Nisn']", "#FormSiswa").removeAttr("readonly");
  $("input[name='Nis']", "#FormSiswa").removeAttr("readonly");
  $("input[name='Nis']", "#FormSiswa").keyup(function () {
    $("input[name='NisLama']", "#FormSiswa").val(
      $("input[name='Nis']", "#FormSiswa").val()
    );
  });
  ComboGetAgama(function (obj) {
    $("select[name='IdAgama']").html(obj);
  });

  ComboGetJenisPekerjaan(function (obj) {
    $("select[name='IdJenisPekerjaanIbu']").html(obj);
    $("select[name='IdJenisPekerjaanAyah']").html(obj);
  });

  ComboGetJenisKelamin(function (obj) {
    $("select[name='KdJenisKelamin']").html(obj);
  });

  ComboGetJenisPrestasiSiswa(function (obj) {
    $("select[name='IdJenisPrestasi']").html(obj);
  });

  ComboGetUnitSklh(function (obj) {
    $("select[name='IdUnit']").html(obj);
  });

  $("select[name='IdUnit']", "#FormSiswa").change(function () {
    $("#ComboFormKelas").removeAttr("style");
    ComboGetKelas(function (obj) {
      $("select[name='IdKelas']").html(obj);
    }, this.value);
  });

  $("select[name='IdKelas']", "#FormSiswa").change(function () {
    $("#ComboFormKelasParalel").removeAttr("style");
    ComboGetKelasParalel(function (obj) {
      $("select[name='IdKelasParalel']").html(obj);
    }, this.value);
  });
}

function TambahUpload() {
  $("#ModalKonfirmasiTambahData").modal("hide");
  $("#ModalUploadSiswa").modal("show");
  $("#FormSiswa")[0].reset();
  $("#BtnSaveSiswa").attr("onClick", "SaveData('UploadData');");
  $(".modal-title").html("Form Upload Data Siswa");

  $(".select2-container", "#FormSiswa").removeAttr("style");
  $("#FieldsetFormSiswa").removeAttr("disabled");
  $("#BtnAksi").removeAttr("style");
}

function TambahData() {
  $("#ModalKonfirmasiTambahData").modal("show");
}

function EditData(IdSiswa) {
  $("#ModalSiswa").modal("show");
  $("#FormSiswa")[0].reset();
  $("#BtnSaveManualSiswa").attr("onClick", "SaveData('EditData');");
  $(".modal-title").html("Form Edit Data Siswa");

  $(".select2-container", "#FormSiswa").removeAttr("style");
  $("#FieldsetFormSiswa").removeAttr("disabled");
  $("#BtnAksi").removeAttr("style");

  var Url = base_url + "/Siswas/GetSiswa?IdSiswa=" + IdSiswa;
  $.ajax({
    url: Url,
    type: "GET",
    dataType: "json",
    beforeSend: function (beforesend) {
      ProgressBar("wait");
    },
    success: function (responsesuccess) {
      ProgressBar("success");
      if (responsesuccess.IsSuccess == true) {
        var ResponseData = responsesuccess.Data;
        $("input[name='IdSiswa']", "#FormSiswa")
          .val(ResponseData.IdSiswa)
          .attr("readonly", true);
        $("input[name='Nisn']", "#FormSiswa").val(ResponseData.Nisn);
        $("input[name='NisLama']", "#FormSiswa").val(ResponseData.NisLama);
        $("input[name='Nis']", "#FormSiswa").val(ResponseData.Nis);
        $("input[name='Nik']", "#FormSiswa").val(ResponseData.Nik);
        $("input[name='Nama']", "#FormSiswa").val(ResponseData.Nama);
        $("input[name='NamaPanggilan']", "#FormSiswa").val(
          ResponseData.NamaPanggilan
        );
        $("input[name='TempatLahir']", "#FormSiswa").val(
          ResponseData.TempatLahir
        );
        $("input[name='TanggalLahir']", "#FormSiswa").val(
          ResponseData.TanggalLahir
        );
        $("input[name='NoHandphone']", "#FormSiswa").val(
          ResponseData.NoHandphone
        );
        $("input[name='NoDarurat']", "#FormSiswa").val(ResponseData.NoDarurat);
        $("input[name='NoDarurat']", "#FormSiswa").val(ResponseData.NoDarurat);
        $("input[name='Email']", "#FormSiswa").val(ResponseData.Email);
        $("textarea[name='AlamatTinggal']", "#FormSiswa").val(
          ResponseData.AlamatTinggal
        );
        $("textarea[name='AlamatOrtu']", "#FormSiswa").val(
          ResponseData.AlamatOrtu
        );
        $("input[name='NikIbu']", "#FormSiswa").val(ResponseData.NikIbu);
        $("input[name='NamaIbu']", "#FormSiswa").val(ResponseData.NamaIbu);
        $("input[name='NamaInstansiIbu']", "#FormSiswa").val(
          ResponseData.NamaInstansiIbu
        );
        $("input[name='NikAyah']", "#FormSiswa").val(ResponseData.NikAyah);
        $("input[name='NamaAyah']", "#FormSiswa").val(ResponseData.NamaAyah);
        $("input[name='NamaInstansiAyah']", "#FormSiswa").val(
          ResponseData.NamaInstansiAyah
        );
        $("input[name='EmailOrtu']", "#FormSiswa").val(ResponseData.EmailOrtu);

        ComboGetAgama(function (obj) {
          $("select[name='IdAgama']").html(obj);
        }, ResponseData.IdAgama);

        ComboGetJenisPekerjaan(function (obj) {
          $("select[name='IdJenisPekerjaanAyah']").html(obj);
        }, ResponseData.IdJenisPekerjaanAyah);

        ComboGetJenisPekerjaan(function (obj) {
          $("select[name='IdJenisPekerjaanIbu']").html(obj);
        }, ResponseData.IdJenisPekerjaanIbu);

        ComboGetJenisKelamin(function (obj) {
          $("select[name='KdJenisKelamin']").html(obj);
        }, ResponseData.KdJenisKelamin);

        ComboGetJenisPrestasiSiswa(function (obj) {
          $("select[name='IdJenisPrestasi']").html(obj);
        }, ResponseData.IdJenisPrestasi);

        ComboGetUnitSklh(function (obj) {
          $("select[name='IdUnit']").html(obj);
        }, ResponseData.IdUnit);

        $("#ComboFormKelas").removeAttr("style");
        ComboGetKelas(
          function (obj) {
            $("select[name='IdKelas']").html(obj);
          },
          ResponseData.IdUnit,
          ResponseData.IdKelas
        );

        $("#ComboFormKelasParalel").removeAttr("style");
        ComboGetKelasParalel(
          function (obj) {
            $("select[name='IdKelasParalel']").html(obj);
          },
          ResponseData.IdKelas,
          ResponseData.IdKelasParalel
        );

        $("select[name='IdUnit']", "#FormSiswa").change(function () {
          $("#ComboFormKelas").removeAttr("style");
          ComboGetKelas(function (obj) {
            $("select[name='IdKelas']").html(obj);
          }, this.value);
        });

        $("select[name='IdKelas']", "#FormSiswa").change(function () {
          $("#ComboFormKelasParalel").removeAttr("style");
          ComboGetKelasParalel(function (obj) {
            $("select[name='IdKelasParalel']").html(obj);
          }, this.value);
        });
      } else {
        swal({
          title: "Gagal",
          text: responsesuccess.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function SaveData(Tipe) {
  var form = new FormData($("#FormSiswa")[0]);
  if (Tipe == "TambahData") {
    var Url = base_url + "/Siswas/SiswaAdd";
    var TextAlert = "Berhasil Tambah Data Siswa";
  } else if (Tipe == "EditData") {
    var Url = base_url + "/Siswas/SiswaEdit";
    var TextAlert = "Berhasil Edit Data Siswa";
  }

  if (Tipe == "UploadData") {
    var formData = new FormData($("#FormUploadSiswa")[0]);
    $.ajax({
      url: base_url + "/Siswas/UploadSiswa",
      type: "POST",
      data: formData,
      dataType: "json",
      contentType: false,
      cache: true,
      processData: false,
      beforeSend: function (responsebefore) {
        ProgressBar("wait");
      },
      success: function (responseupload) {
        ProgressBar("success");
        if (responseupload.IsSuccess == true) {
          $("#TabelSiswa").removeAttr("style");
          $("#ImgSearch").css("display", "none");

          $("#FormUploadSiswa")[0].reset();
          $("#ModalUploadSiswa").modal("hide");
          TabelSiswa(0, "");
          iziToast.success({
            title: "Berhasil Upload Data Siswa",
            message: responseupload.ReturnMessage,
            position: "topRight",
          });
        } else if (responseupload.IsSuccess == false) {
          iziToast.error({
            title: "Gagal Upload Data Siswa",
            message: responseupload.ReturnMessage,
            position: "topRight",
          });
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        ProgressBar("success");
        swal({
          title: "Gagal Upload Data Siswa",
          text: jqXHR + " " + textStatus + " " + errorThrown,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      },
    });
  } else {
    $.ajax({
      url: Url,
      method: "POST",
      dataType: "json",
      data: form,
      contentType: false,
      cache: true,
      processData: false,
      beforeSend: function (before) {
        ProgressBar("wait");
      },
      success: function (responsesave) {
        ProgressBar("success");
        if (responsesave.IsSuccess == true) {
          $("#TabelSiswa").removeAttr("style");
          $("#ImgSearch").css("display", "none");

          $("#FormSiswa")[0].reset();
          $("#ModalSiswa").modal("hide");
          TabelSiswa(0, "");
          iziToast.success({
            title: "Berhasil Menyimpan Data Siswa",
            message: responsesave.ReturnMessage,
            position: "topRight",
          });
        } else if (responsesave.IsSuccess == false) {
          iziToast.error({
            title: "Gagal Menyimpan Data Siswa",
            message: responsesave.ReturnMessage,
            position: "topRight",
          });
        }
      },
      error: function (responserror, a, e) {
        ProgressBar("success");
        swal({
          title: "Error",
          text: JSON.stringify(responserror) + " : " + e,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      },
    });
  }
}

function NonAktif(IdSiswa, NamaSiswa) {
  swal(
    {
      title: "Apakah Anda Yakin ?",
      text: "Anda akan menon-aktifkan siswa atas nama " + NamaSiswa,
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-success",
      confirmButtonText: "Ya, Saya Yakin!",
      cancelButtonClass: "btn-danger",
      cancelButtonText: "Tidak, Batalkan!",
      closeOnConfirm: false,
    },
    function () {
      var Url =
        base_url + "/Siswas/SetSiswaInActive?IdSiswa=" + parseInt(IdSiswa);
      $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        headers: {
          "Content-Type": "application/json",
        },
        beforeSend: function (before) {
          ProgressBar("wait");
        },
        success: function (responsesave) {
          ProgressBar("success");
          if (responsesave.IsSuccess == true) {
            $("#TabelSiswa").removeAttr("style");
            $("#ImgSearch").css("display", "none");

            TabelSiswa(0, "");
            swal({
              title: "Berhasil Menon-Aktifkan Siswa",
              text: responsesave.ReturnMessage,
              confirmButtonClass: "btn-success text-white",
              confirmButtonText: "Oke, Mengerti",
              type: "success",
            });
          } else if (responsesave.IsSuccess == false) {
            swal({
              title: "Gagal Menon-Aktifkan Siswa",
              text: responsesave.ReturnMessage,
              confirmButtonClass: "btn-danger text-white",
              confirmButtonText: "Oke, Mengerti",
              type: "error",
            });
          }
        },
        error: function (responserror, a, e) {
          ProgressBar("success");
          swal({
            title: "Error",
            text: JSON.stringify(responserror) + " : " + e,
            confirmButtonClass: "btn-danger text-white",
            confirmButtonText: "Oke, Mengerti",
            type: "error",
          });
        },
      });
    }
  );
}

function DownloadData() {
  var formObj = $("#FormTabelSiswa").serializeObject();
  window.open(
    base_url + "/Siswas/DowloadSiswasByUnit?IdUnit=" + parseInt(formObj.IdUnit),
    "_blank"
  );
}
function DownloadDapodik() {
  var formObj = $("#FormTabelSiswa").serializeObject();
  window.open(
    base_url + "/Siswas/DownloadDapodik?IdUnit=" + parseInt(formObj.IdUnit),
    "_blank"
  );
}

function HapusData() {
  var formObj = $("#FormTabelSiswa").serializeObject();
  var NamaBerdasarkan = $(
    "select[name='IdBerdasarkan'] option:selected",
    "#FormTabelSiswa"
  ).text();
  var NamaUnit = $(
    "select[name='IdUnit'] option:selected",
    "#FormTabelSiswa"
  ).text();
  var NamaKelas = $(
    "select[name='IdKelas'] option:selected",
    "#FormTabelSiswa"
  ).text();
  var NamaKelasParalel = $(
    "select[name='IdKelasParalel'] option:selected",
    "#FormTabelSiswa"
  ).text();
  if (formObj.IdTipe == 2) {
    if (formObj.IdBerdasarkan == 0) {
      var Url = base_url + "/Siswas/SiswaDeleteAll";
      var TextConfirm = NamaBerdasarkan;
    } else if (formObj.IdBerdasarkan == 1) {
      var Url =
        base_url +
        "/Siswas/SiswaDeleteByUnit?IdUnit=" +
        parseInt(formObj.IdUnit);
      var TextConfirm = NamaBerdasarkan + " pada unit " + NamaUnit;
    } else if (formObj.IdBerdasarkan == 2) {
      var Url =
        base_url +
        "/Siswas/SiswaDeleteByKelas?IdKelas=" +
        parseInt(formObj.IdKelas);
      var TextConfirm = NamaBerdasarkan + " pada kelas " + NamaKelas;
    } else if (formObj.IdBerdasarkan == 3) {
      var Url =
        base_url +
        "/Siswas/SiswaDeleteByKelasParalel?IdKelasParalel=" +
        parseInt(formObj.IdKelasParalel);
      var TextConfirm =
        NamaBerdasarkan + " pada kelas paralel " + NamaKelasParalel;
    }
  }

  swal(
    {
      title: "Apakah Anda Yakin ?",
      text: "Anda akan menghapus data siswa berdasarkan " + TextConfirm,
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-success",
      confirmButtonText: "Ya, Saya Yakin!",
      cancelButtonClass: "btn-danger",
      cancelButtonText: "Tidak, Batalkan!",
      closeOnConfirm: false,
    },
    function () {
      $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        headers: {
          "Content-Type": "application/json",
        },
        beforeSend: function (before) {
          ProgressBar("wait");
        },
        success: function (responsesave) {
          ProgressBar("success");
          if (responsesave.IsSuccess == true) {
            $("#TabelSiswa").removeAttr("style");
            $("#ImgSearch").css("display", "none");

            if (formObj.IdBerdasarkan == 0) {
              TabelSiswa(formObj.IdBerdasarkan, "");
            } else if (formObj.IdBerdasarkan == 1) {
              TabelSiswa(formObj.IdBerdasarkan, parseInt(formObj.IdUnit));
            } else if (formObj.IdBerdasarkan == 2) {
              TabelSiswa(formObj.IdBerdasarkan, parseInt(formObj.IdKelas));
            } else if (formObj.IdBerdasarkan == 3) {
              TabelSiswa(
                formObj.IdBerdasarkan,
                parseInt(formObj.IdKelasParalel)
              );
            }

            swal({
              title: "Berhasil Menghapus Siswa",
              text: "",
              confirmButtonClass: "btn-success text-white",
              confirmButtonText: "Oke, Mengerti",
              type: "success",
            });
          } else if (responsesave.IsSuccess == false) {
            console.log(responsesave.ReturnMessage);
            swal({
              title: "Gagal Menghapus Siswa",
              text: responsesave.ReturnMessage,
              confirmButtonClass: "btn-danger text-white",
              confirmButtonText: "Oke, Mengerti",
              type: "error",
            });
          }
        },
        error: function (responserror, a, e) {
          ProgressBar("success");
          swal({
            title: "Error",
            text: JSON.stringify(responserror) + " : " + e,
            confirmButtonClass: "btn-danger text-white",
            confirmButtonText: "Oke, Mengerti",
            type: "error",
          });
        },
      });
    }
  );
}

function ArsipData(Tipe) {
  var formObj = $("#FormTabelSiswa").serializeObject();
  var NamaTahunAjaran = $(
    "select[name='IdTahunAjaran'] option:selected",
    "#FormTabelSiswa"
  ).text();
  if (formObj.IdTipe == 3) {
    if (Tipe == null) {
      var Url =
        base_url +
        "/Siswas/SetSiswaArchive?IdTahunAjaran=" +
        parseInt(formObj.IdTahunAjaran);
      var TextConfirm = NamaTahunAjaran;
    } else if (Tipe == "ReArchive") {
      var Url =
        base_url +
        "/Siswas/SetSiswaReArchive?IdTahunAjaran=" +
        parseInt(formObj.IdTahunAjaran);
      var TextConfirm = NamaTahunAjaran;
    }
  }
  swal(
    {
      title: "Apakah Anda Yakin ?",
      text: "Anda akan meng-arsip data siswa tahun ajaran " + TextConfirm,
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-success",
      confirmButtonText: "Ya, Saya Yakin!",
      cancelButtonClass: "btn-danger",
      cancelButtonText: "Tidak, Batalkan!",
      closeOnConfirm: false,
    },
    function () {
      $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        headers: {
          "Content-Type": "application/json",
        },
        beforeSend: function (before) {
          ProgressBar("wait");
        },
        success: function (responsesave) {
          ProgressBar("success");
          if (responsesave.IsSuccess == true) {
            $("#TabelSiswa").removeAttr("style");
            $("#ImgSearch").css("display", "none");

            TabelSiswa(formObj.IdBerdasarkan, "");
            swal({
              title: "Berhasil Meng-arsip Siswa",
              text: "",
              confirmButtonClass: "btn-success text-white",
              confirmButtonText: "Oke, Mengerti",
              type: "success",
            });
          } else if (responsesave.IsSuccess == false) {
            swal({
              title: "Gagal Meng-arsip Siswa",
              text: responsesave.ReturnMessage,
              confirmButtonClass: "btn-danger text-white",
              confirmButtonText: "Oke, Mengerti",
              type: "error",
            });
          }
        },
        error: function (responserror, a, e) {
          ProgressBar("success");
          swal({
            title: "Error",
            text: JSON.stringify(responserror) + " : " + e,
            confirmButtonClass: "btn-danger text-white",
            confirmButtonText: "Oke, Mengerti",
            type: "error",
          });
        },
      });
    }
  );
}

function EditDataMassal() {
  $("#ModalEditDataMassal").modal("show");
}

function UploadEditDataMassal() {
  var formData = new FormData($("#FormEditDataMassal")[0]);
  $.ajax({
    url: base_url + "/Siswas/UploadEditSiswa",
    type: "POST",
    data: formData,
    dataType: "json",
    contentType: false,
    cache: true,
    processData: false,
    beforeSend: function (responsebefore) {
      ProgressBar("wait");
    },
    success: function (responseupload) {
      ProgressBar("success");
      if (responseupload.IsSuccess == true) {
        $("#TabelSiswa").removeAttr("style");
        $("#ImgSearch").css("display", "none");

        $("#FormEditDataMassal")[0].reset();
        $("#ModalEditDataMassal").modal("hide");
        TabelSiswa(0, "");
        iziToast.success({
          title: "Berhasil Upload",
          message: "Anda berhasil melakukan pembaruan data secara massal",
          position: "topRight",
        });
      } else if (responseupload.IsSuccess == false) {
        iziToast.error({
          title: "Gagal Upload",
          message: responseupload.ReturnMessage,
          position: "topRight",
        });
      }
    },
    error: function (jqXHR, textStatus, errorThrown) {
      ProgressBar("success");
      swal({
        title: "Gagal Upload Data Siswa",
        text: JSON.stringify(jqXHR) + " " + textStatus + " " + errorThrown,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}
