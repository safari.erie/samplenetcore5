﻿/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 15052020
================================================================================================================= */
    //////// START SECTION TABEL VISI MISI ///////////
    $('#TabelSekolahVisiMisi tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelSekolahVisiMisi = $('#TabelSekolahVisiMisi').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": base_url + "/Sekolahs/GetSekolahVisiMisis",
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Visi Misi',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    iziToast.success({
                        title: 'Berhasil Menampilkan Data Tabel Visi Misi',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json.Data;
                }
            }
        },
        columnDefs: [
            { targets: [0], width: "auto", visible: true },
            { targets: [1], width: "auto", visible: true },
            { targets: [2], width: "auto", visible: true },
            { targets: [3], width: "auto", visible: true },
            { targets: [4], width: "auto", visible: true },
        ],
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamEdit = "EditData('" + full.IdSekolahVisiMisi + "')";
                    var ParamHapus = "HapusData('" + full.IdSekolahVisiMisi + "','" + full.Judul + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
                    Data += '&nbsp;&nbsp;&nbsp;';
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                    return Data;
                }
            },
            { "data": "Judul" },
            { "data": "JenisVisiMisi" },
            { "data": "Konten" },
            { "data": "FileIkon" },
        ],
        "bDestroy": true
    });
    TabelSekolahVisiMisi.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    //////// START SECTION TABEL VISI MISI ///////////

    function TambahData() {
        $("#ModalSekolahVisiMisi").modal("show");
        $('#FormSekolahVisiMisi')[0].reset();
        $("#BtnSaveSekolahVisiMisi").attr("onClick", "SaveData('TambahData');");
        $(".modal-title").html("Tambah Visi & Misi");
        ComboGetJenisVisiMisi(function (obj) {
            $("select[name='IdJenisVisiMisi']").html(obj);
        });
    }

    function EditData(IdSekolahVisiMisi) {
        $("#ModalSekolahVisiMisi").modal("show");
        $('#FormSekolahVisiMisi')[0].reset();
        $("#BtnSaveSekolahVisiMisi").attr("onClick", "SaveData('EditData');");
        $(".modal-title").html("Edit Visi & Misi");
        var Url = base_url + "/Sekolahs/GetSekolahVisiMisi?IdSekolahVisiMisi=" + IdSekolahVisiMisi;
        $.ajax({
            url: Url,
            type: "GET",
            dataType: "json",
            beforeSend: function (beforesend) {
                ProgressBar("wait");
            },
            success: function (responsesuccess) {
                ProgressBar("success");
                if (responsesuccess.IsSuccess == true) {
                    var ResponseData = responsesuccess.Data;
                    $("input[name='IdSekolahVisiMisi']", "#FormSekolahVisiMisi").val(ResponseData.IdSekolahVisiMisi).attr("readonly", true);
                    $("input[name='Judul']", "#FormSekolahVisiMisi").val(ResponseData.Judul);
                    $("textarea[name='Konten']", "#FormSekolahVisiMisi").summernote('code', ResponseData.Konten);
                    $("input[name='FileIkon']", "#FormSekolahVisiMisi").val(ResponseData.FileIkon);

                    ComboGetJenisVisiMisi(function (obj) {
                        $("select[name='IdJenisVisiMisi']").html(obj);
                    }, ResponseData.IdJenisVisiMisi);

                } else {
                    swal({ title: 'Gagal :(', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            }, error: function (responserror, a, e) {
                ProgressBar("success");
                swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        });
    }

    function SaveData(Tipe) {
        var formObj = $('#FormSekolahVisiMisi').serializeObject();
        var fd = new FormData($('#FormSekolahVisiMisi')[0]);
        if (Tipe == "TambahData") {
            var Url = base_url + "/Sekolahs/SekolahVisiMisiAdd";
        } else if (Tipe == "EditData") {
            var Url = base_url + "/Sekolahs/SekolahVisiMisiEdit";
            fd.append('IdSekolahVisiMisi', formObj.IdSekolahVisiMisi)
        }

        fd.append('IdJenisVisiMisi', formObj.IdJenisVisiMisi)
        fd.append('Judul', formObj.Judul)
        fd.append('Konten', formObj.Konten)
        fd.append('FileIkon', formObj.FileIkon)
        fd.append('IdSekolah', 0)

        $.ajax({
            url: Url,
            method: "POST",
            dataType: "json",
            data: fd,
            contentType: false,
            cache: true,
            processData: false,
            beforeSend: function (before) {
                ProgressBar("wait");
            },
            success: function (responsesave) {
                ProgressBar("success");
                if (responsesave.IsSuccess == true) {
                    $('#FormSekolahVisiMisi')[0].reset();
                    $("#ModalSekolahVisiMisi").modal("hide");
                    TabelSekolahVisiMisi.ajax.reload();
                    swal({ title: 'Berhasil Menyimpan Visi Misi', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                } else if (responsesave.IsSuccess == false) {
                    swal({ title: 'Gagal Menyimpan Visi Misi', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            }, error: function (responserror, a, e) {
                ProgressBar("success");
                swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        });
    }

    function HapusData(IdSekolahVisiMisi, Judul) {
        swal({
            title: "Apakah Anda Yakin ?",
            text: "Anda akan menghapus data Visi Misi " + Judul,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: false
        },
            function () {
                var Url = base_url + "/Sekolahs/SekolahVisiMisiDelete?IdSekolahVisiMisi=" + IdSekolahVisiMisi;
                $.ajax({
                    url: Url,
                    method: "POST",
                    dataType: "json",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    beforeSend: function (before) {
                        ProgressBar("wait");
                    },
                    success: function (responsesave) {
                        ProgressBar("success");
                        if (responsesave.IsSuccess == true) {
                            TabelSekolahVisiMisi.ajax.reload();
                            swal({ title: 'Berhasil Menghapus Visi Misi', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                        } else if (responsesave.IsSuccess == false) {
                            swal({ title: 'Gagal Menghapus Visi Misi', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                        }
                    }, error: function (responserror, a, e) {
                        ProgressBar("success");
                        swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    }
                });
            });
    }

