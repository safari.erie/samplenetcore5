const UrlSvc = {
    GetKalenderPendidikans: `${base_url}/Sekolahs/GetKalenderPendidikans`,
    GetKalenderPendidikan: `${base_url}/Sekolahs/GetKalenderPendidikan`,
    KalenderPendidikanAdd: `${base_url}/Sekolahs/KalenderPendidikanAdd`,
    KalenderPendidikanEdit: `${base_url}/Sekolahs/KalenderPendidikanEdit`,
    KalenderPendidikanDelete: `${base_url}/Sekolahs/KalenderPendidikanDelete`,
}

$(document).ready((e) => {
    DataKalenders()
})

const DataKalenders = () => {
    $('#TabelKalenderPendidikan tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelData = $('#TabelKalenderPendidikan').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": UrlSvc.GetKalenderPendidikans,
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [{
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamEdit = `ModalKalenderPendidikan('edit',${full.IdKalenderPendidikan})`;
                    var ParamHapus = "HapusData('" + full.IdKalenderPendidikan + "')";
                    Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
                    Data += '<button type="button" class="btn btn-danger btn-sm ml-2" onClick="' + ParamHapus + '"><i class="fa fa-trash-o"></i> Hapus</button>';
                    return Data;
                }
            },
             {
                "data": "IdKalenderPendidikan"
            },
            {
                "data": "Semester"
            },
            {
                "data": "Keterangan"
            },
            {
                "render": function (data, type, full, meta) {
                    var OnError = "this.style.display='none'";
                    var Data = '<img onerror="' + OnError + '" src="' + base_url + '/Asset/Files/Appl/Image/' + full.FileImage + '" style="height:50px;object-fit:cover;object-position:center;">';

                    return Data;
                }
            },
        ],
        "bDestroy": true
    });
    TabelData.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");

}

const DataKalender = (id) => {
    var res = initAjax(`${UrlSvc.GetKalenderPendidikan}?IdKalenderPendidikan=${id}`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        $("input[name='IdKalenderPendidikan']", "#FormKalenderPendidikan").val(res.Data.IdKalenderPendidikan).attr("readonly", true);
        $("select[name='Semester']", "#FormKalenderPendidikan").val(res.Data.Semester);
        $("textarea[name='Keterangan']", "#FormKalenderPendidikan").val(res.Data.Keterangan);
        $("#FileImagePreviewKalenderPendidikan", "#FormKalenderPendidikan").removeAttr("src").attr("src", base_url + "/Asset/Files/Appl/Image/" + res.Data.FileImage);
        $('#ModalKalenderPendidikan').modal('show')
    } else {
        iziToast.error({
            title: 'Gagal',
            message: res.ReturnMessage,
            position: 'topRight'
        });
    }
}

const ModalKalenderPendidikan = (isAction, id) => {
    if (isAction === "add") {
        $('#ModalTitle').html('Tambah Baru');
        $('#BtnModalAddEdit').attr('onClick', 'SaveData()')
        $('#FileImagePreviewKalenderPendidikan').fadeOut()
        $('#ModalKalenderPendidikan').modal('show');
        $("textarea[name='Keterangan']", "#FormKalenderPendidikan").val('');
        $("select[name='Semester']", "#FormKalenderPendidikan").val('');
        $("input[name='Semester']", "#IdKalenderPendidikan").val('');
    } else if (isAction === "edit") {
        $('#ModalTitle').html('Perbarui Data');
        $('#BtnModalAddEdit').attr('onClick', 'UpdateData()')
        $('#FileImagePreviewKalenderPendidikan').fadeIn()
        DataKalender(id)
    }
    $('input[type="file"][name="FileImage"]', '#FormKalenderPendidikan').val('')
}

function SaveData() {
    var fd = new FormData();
    var formObj = $('#FormKalenderPendidikan').serializeObject();
    fd.append('Semester', formObj.Semester)
    fd.append('Keterangan', formObj.Keterangan)
    fd.append('FileImage', $('input[type="file"][name="FileImage"]', '#FormKalenderPendidikan')[0].files[0]);
    $.ajax({
        "url": UrlSvc.KalenderPendidikanAdd,
        "method": "POST",
        "timeout": 0,
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": fd,
        "dataType": "JSON",
        "beforeSend": function (x) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                swal({ title: 'Berhasil', text: 'Berhasil simpan data!', confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                $('#ModalKalenderPendidikan').modal('hide');
                $('#TabelKalenderPendidikan').DataTable().ajax.reload();
            } else {
                swal({ title: 'Gagal :(', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function UpdateData() {
    var fd = new FormData();
    var formObj = $('#FormKalenderPendidikan').serializeObject();
    fd.append('IdKalenderPendidikan', formObj.IdKalenderPendidikan)
    fd.append('Semester', formObj.Semester)
    fd.append('Keterangan', formObj.Keterangan)
    fd.append('FileImage', $('input[type="file"][name="FileImage"]', '#FormKalenderPendidikan')[0].files[0]);
    $.ajax({
        "url": UrlSvc.KalenderPendidikanEdit,
        "method": "POST",
        "timeout": 0,
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": fd,
        "dataType": "JSON",
        "beforeSend": function (x) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                swal({ title: 'Berhasil', text: 'Berhasil perbarui data!', confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
                $('#ModalKalenderPendidikan').modal('hide');
                $('#TabelKalenderPendidikan').DataTable().ajax.reload();
            } else {
                swal({ title: 'Gagal :(', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function HapusData(id) {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus data ini",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: true
    },
        function () {
            $.ajax({
                url: `${UrlSvc.KalenderPendidikanDelete}?IdKalenderPendidikan=${id}`,
                method: "GET",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesave) {
                    ProgressBar("success");
                    if (responsesave.IsSuccess == true) {
                        $('#TabelKalenderPendidikan').DataTable().ajax.reload();
                        iziToast.success({
                            title: 'Berhasil Menghapus Kalender Pendidikan',
                            message: responsesave.ReturnMessage,
                            position: 'topRight'
                        });
                    } else if (responsesave.IsSuccess == false) {
                        iziToast.error({
                            title: 'Gagal Menghapus Kalender Pendidikan',
                            message: responsesave.ReturnMessage,
                            position: 'topRight'
                        });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}