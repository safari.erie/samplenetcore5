const UrlSvc = {
  GetTestimonis: `${base_url}/Sekolahs/GetTestimonis`,
  GetTestimoni: `${base_url}/Sekolahs/GetTestimoni`,
  UpdateTestimoni: `${base_url}/Sekolahs/UpdateTestimoni`,
  DeleteTestimoni: `${base_url}/Sekolahs/DeleteTestimoni`,
};

$(document).ready((e) => {
  DataTestimonis();
});

const DataTestimonis = () => {
  $("#TabelTestimoni tfoot th").each(function () {
    var title = $(this).text();
    $(this).html(
      '<input type="text" class="form-control" placeholder="Cari ' +
        title +
        '" />'
    );
  });
  var TabelData = $("#TabelTestimoni").DataTable({
    paging: true,
    searching: true,
    ordering: true,
    info: true,
    pageLength: 10,
    lengthChange: true,
    scrollX: true,
    processing: true,
    ajax: {
      url: UrlSvc.GetTestimonis,
      method: "GET",
      beforeSend: function (xhr) {},
      dataSrc: function (json) {
        if (json.Data == null) {
          return json;
        } else {
          return json.Data;
        }
      },
    },
    columns: [
      {
        render: function (data, type, full, meta) {
          var Data = "";
          var ParamEdit = `ModalTestimoni('edit',${full.IdTestimoni})`;
          var ParamHapus = "HapusData('" + full.IdTestimoni + "')";
          Data +=
            '<button type="button" class="btn btn-info btn-sm" onClick="' +
            ParamEdit +
            '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
          Data +=
            '<button type="button" class="btn btn-danger btn-sm ml-2" onClick="' +
            ParamHapus +
            '"><i class="fa fa-trash-o"></i> Hapus</button>';
          return Data;
        },
      },
      {
        data: "Nama",
      },
      {
        data: "Sebagai",
      },
      {
        render: function (data, type, full, meta) {
          var Data = "";
          if (full.Status === 1) {
            Data = `<span class="badge badge-info">Publish</span>`;
          } else {
            Data = `<span class="badge badge-success">Baru</span>`;
          }
          return Data;
        },
      },
      {
        data: "CreatedDate",
      },
    ],
    bDestroy: true,
  });
  TabelData.columns().every(function () {
    var that = this;

    $("input", this.footer()).on("keyup change clear", function () {
      if (that.search() !== this.value) {
        that.search(this.value).draw();
      }
    });
  });
  $(".dataTables_filter").css("display", "none");
  $(".dataTables_length").css("display", "none");
};

const DataTestimoni = (id) => {
  var res = initAjax(`${UrlSvc.GetTestimoni}?IdTestimoni=${id}`);
  if (res) ProgressBar("success");
  if (res.IsSuccess) {
    $("input[name='IdTestimoni']", "#FormTestimoni")
      .val(res.Data.IdTestimoni)
      .attr("readonly", false);
    $("input[name='Nama']", "#FormTestimoni")
      .val(res.Data.Nama)
      .attr("readonly", false);
    // $("select[name='Sebagai']", "#FormTestimoni").val(res.Data.Sebagai).attr("readonly", false);
    $("input[name='Sebagai']", "#FormTestimoni")
      .val(res.Data.Sebagai)
      .attr("readonly", false);
    $("textarea[name='Pesan']", "#FormTestimoni")
      .val(res.Data.Pesan)
      .attr("readonly", false);
    $("input[name='Url']", "#FormTestimoni").val(res.Data.Url);
    if (res.Data.FotoUser !== null) {
      $("#FotoUserPreviewTestimoni", "#FormTestimoni")
        .removeAttr("src")
        .attr("src", base_url + "/Asset/Files/Testimoni/" + res.Data.FotoUser);
      $("#DivFotoUser").fadeIn();
    } else {
      $("#DivFotoUser").fadeOut();
    }
    $("#ModalTestimoni").modal("show");
  } else {
    iziToast.error({
      title: "Gagal",
      message: res.ReturnMessage,
      position: "topRight",
    });
  }
};

const ModalTestimoni = (isAction, id) => {
  if (isAction === "add") {
    $("#ModalTitle").html("Tambah Baru");
    $("#BtnModalAddEdit").attr("onClick", "SaveData()");
    $("#FotoUserPreviewTestimoni").fadeOut();
    $("#ModalTestimoni").modal("show");
    $("textarea[name='Keterangan']", "#FormTestimoni").val("");
    $("select[name='Semester']", "#FormTestimoni").val("");
    $("input[name='Semester']", "#IdTestimoni").val("");
  } else if (isAction === "edit") {
    $("#ModalTitle").html("Perbarui Data");
    $("#BtnModalAddEdit").attr("onClick", "UpdateData()");
    $("#FotoUserPreviewTestimoni").fadeIn();
    DataTestimoni(id);
  }
  $('input[type="file"][name="FotoUser"]', "#FormTestimoni").val("");
};

function UpdateData() {
  var fd = new FormData();
  var formObj = $("#FormTestimoni").serializeObject();
  fd.append("IdTestimoni", formObj.IdTestimoni);
  fd.append("Nama", formObj.Nama);
  fd.append("Sebagai", formObj.Sebagai);
  fd.append("Pesan", formObj.Pesan);
  fd.append("Url", formObj.Url);
  $.ajax({
    url: UrlSvc.UpdateTestimoni,
    method: "POST",
    timeout: 0,
    processData: false,
    mimeType: "multipart/form-data",
    contentType: false,
    data: fd,
    dataType: "JSON",
    beforeSend: function (x) {
      ProgressBar("wait");
    },
    success: function (responsesuccess) {
      ProgressBar("success");
      if (responsesuccess.IsSuccess == true) {
        swal({
          title: "Berhasil",
          text: "Berhasil perbarui data!",
          confirmButtonClass: "btn-success text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "success",
        });
        $("#ModalTestimoni").modal("hide");
        $("#TabelTestimoni").DataTable().ajax.reload();
      } else {
        swal({
          title: "Gagal :(",
          text: responsesuccess.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function HapusData(id) {
  swal(
    {
      title: "Apakah Anda Yakin ?",
      text: "Anda akan menghapus data ini",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-success",
      confirmButtonText: "Ya, Saya Yakin!",
      cancelButtonClass: "btn-danger",
      cancelButtonText: "Tidak, Batalkan!",
      closeOnConfirm: true,
    },
    function () {
      $.ajax({
        url: `${UrlSvc.DeleteTestimoni}?IdTestimoni=${id}`,
        method: "GET",
        dataType: "json",
        headers: {
          "Content-Type": "application/json",
        },
        beforeSend: function (before) {
          ProgressBar("wait");
        },
        success: function (responsesave) {
          ProgressBar("success");
          if (responsesave.IsSuccess == true) {
            $("#TabelTestimoni").DataTable().ajax.reload();
            iziToast.success({
              title: "Berhasil Menghapus Testimoni",
              message: responsesave.ReturnMessage,
              position: "topRight",
            });
          } else if (responsesave.IsSuccess == false) {
            iziToast.error({
              title: "Gagal Menghapus Testimoni",
              message: responsesave.ReturnMessage,
              position: "topRight",
            });
          }
        },
        error: function (responserror, a, e) {
          ProgressBar("success");
          swal({
            title: "Error :(",
            text: JSON.stringify(responserror) + " : " + e,
            confirmButtonClass: "btn-danger text-white",
            confirmButtonText: "Oke, Mengerti",
            type: "error",
          });
        },
      });
    }
  );
}
