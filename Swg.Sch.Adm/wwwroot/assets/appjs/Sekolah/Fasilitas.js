/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 15052020
================================================================================================================= */

$("select[name='IdJenisFasilitas']").change(function () {
    if (this.value == 1) {
        $("#JenisImage").removeAttr("style");
        $("#JenisVideo").css("display", "none");
    } else if (this.value == 2) {
        $("#JenisVideo").removeAttr("style");
        $("#JenisImage").css("display", "none");
    } 
});

//////// START SECTION Tabel Fasilitas ///////////
$('#TabelSekolahFasilitas tfoot th').each(function () {
    var title = $(this).text();
    $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
});
var TabelSekolahFasilitas = $('#TabelSekolahFasilitas').DataTable({
    "paging": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "pageLength": 10,
    "lengthChange": true,
    "scrollX": true,
    "processing": true,
    "ajax": {
        "url": base_url + "/Sekolahs/GetSekolahFasilitass",
        "method": 'GET',
        "beforeSend": function (xhr) {
        },
        "dataSrc": function (json) {
            if (json.Data == null) {
                if (json.ReturnMessage != "data tidak ada") {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Fasilitas Sekolah',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                }
                return json;
            } else {
                return json.Data;
            }
        }
    },
    "columns": [
        {
            "render": function (data, type, full, meta) {
                var Data = "";
                var ParamEdit = "EditData('" + full.IdSekolahFasilitas + "')";
                var ParamHapus = "HapusData('" + full.IdSekolahFasilitas + "','" + cekQuotes(full.Judul) + "')";
                Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
                Data += '&nbsp;&nbsp;&nbsp;';
                Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                return Data;
            }
        },
        { "data": "Judul" },
        { "data": "Deskripsi" },
        {
            "render": function (data, type, full, meta) {
                var OnError = "this.style.display='none'";
                var Data = '<img onerror="' + OnError + '" src="' + base_url + '/Asset/Files/Fasilitas/Sekolah/' + full.FileImage + '" style="height:100px;object-fit:cover;object-position:center;">';
                return Data;
            }
        }
    ],
    "bDestroy": true
});
TabelSekolahFasilitas.columns().every(function () {
    var that = this;

    $('input', this.footer()).on('keyup change clear', function () {
        if (that.search() !== this.value) {
            that
                .search(this.value)
                .draw();
        }
    });
});
$(".dataTables_filter").css("display", "none");
$(".dataTables_length").css("display", "none");
//////// START SECTION Tabel Fasilitas ///////////

function TambahData() {
    $("#ModalSekolahFasilitas").modal("show");
    $('#FormSekolahFasilitas')[0].reset();
    $("#BtnSaveSekolahFasilitas").attr("onClick", "SaveData('TambahData');");
    $(".modal-title").html("Form Tambah Fasilitas Sekolah");
    $("#ThumbnailImageSF").removeAttr("src");
}

function EditData(IdSekolahFasilitas) {
    $("#ModalSekolahFasilitas").modal("show");
    $('#FormSekolahFasilitas')[0].reset();
    $("#BtnSaveSekolahFasilitas").attr("onClick", "SaveData('EditData');");
    $(".modal-title").html("Form Edit Fasilitas Sekolah");
    var Url = base_url + "/Sekolahs/GetSekolahFasilitas?IdSekolahFasilitas=" + IdSekolahFasilitas;
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var ResponseData = responsesuccess.Data;
                $("input[name='IdSekolahFasilitas']", "#FormSekolahFasilitas").val(ResponseData.IdSekolahFasilitas).attr("readonly", true);
                $("input[name='Judul']", "#FormSekolahFasilitas").val(ResponseData.Judul);
                $("textarea[name='Deskripsi']", "#FormSekolahFasilitas").val(ResponseData.Deskripsi);
                CKEDITOR.instances.Deskripsi.setData(ResponseData.Deskripsi);

                $("#JenisImage").removeAttr("style");
                $("#ThumbnailImageSF").removeAttr("src").attr("src", base_url + "/Asset/Files/Fasilitas/Sekolah/" + ResponseData.FileImage);

            } else {
                swal({ title: 'Gagal :(', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function SaveData(Tipe) {
    var form = new FormData($('#FormSekolahFasilitas')[0]);
    if (Tipe == "TambahData") {
        var Url = base_url + "/Sekolahs/SekolahFasilitasAdd";
        var TextAlert = "Berhasil Tambah Data Fasilitas";
    } else if (Tipe == "EditData") {
        var Url = base_url + "/Sekolahs/SekolahFasilitasEdit";
        var TextAlert = "Berhasil Edit Data Fasilitas";
    }

    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        data: form,
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $('#FormSekolahFasilitas')[0].reset();
                $("#ModalSekolahFasilitas").modal("hide");
                TabelSekolahFasilitas.ajax.reload();
                iziToast.success({
                    title: 'Berhasil Menyimpan Fasilitas Sekolah',
                    message: TextAlert,
                    position: 'topRight'
                });
            } else if (responsesave.IsSuccess == false) {
                iziToast.success({
                    title: 'Berhasil Menyimpan Fasilitas Sekolah',
                    message: responsesave.ReturnMessage,
                    position: 'topRight'
                });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function HapusData(IdSekolahFasilitas, Judul) {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus data Fasilitas " + Judul,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: true
    },
        function () {
            var Url = base_url + "/Sekolahs/SekolahFasilitasDelete?IdSekolahFasilitas=" + IdSekolahFasilitas;
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesave) {
                    ProgressBar("success");
                    if (responsesave.IsSuccess == true) {
                        TabelSekolahFasilitas.ajax.reload();
                        iziToast.success({
                            title: 'Berhasil Menghapus Fasilitas Sekolah',
                            message: responsesave.ReturnMessage,
                            position: 'topRight'
                        });
                    } else if (responsesave.IsSuccess == false) {
                        iziToast.error({
                            title: 'Gagal Menghapus Fasilitas Sekolah',
                            message: responsesave.ReturnMessage,
                            position: 'topRight'
                        });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}

