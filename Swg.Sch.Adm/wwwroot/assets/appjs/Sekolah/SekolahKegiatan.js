﻿/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 15052020
================================================================================================================= */

//////// START SECTION Tabel Kegiatan Sekolah ///////////
$('#TabelSekolahKegiatan tfoot th').each(function () {
    var title = $(this).text();
    $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
});
var TabelSekolahKegiatan = $('#TabelSekolahKegiatan').DataTable({
    "paging": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "pageLength": 10,
    "lengthChange": true,
    "scrollX": true,
    "processing": true,
    "ajax": {
        "url": base_url + "/Sekolahs/GetSekolahKegiatans",
        "method": 'GET',
        "beforeSend": function (xhr) {},
        "dataSrc": function (json) {
            if (json.Data == null) {
                if (json.ReturnMessage != "data tidak ada") {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Kegiatan Sekolah',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                }
                return json;
            } else {
                return json.Data;
            }
        }
    },
    "columns": [{
            "render": function (data, type, full, meta) {
                var Data = "";
                var ParamEdit = "EditData('" + full.IdSekolahKegiatan + "')";
                var ParamHapus = "HapusData('" + full.IdSekolahKegiatan + "','" + cekQuotes(full.Judul) + "')";
                Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
                Data += '&nbsp;&nbsp;&nbsp;';
                Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                return Data;
            }
        },
        {
            "data": "Judul"
        },
        {
            "data": "Keterangan"
        },
        {
            "render": function (data, type, full, meta) {
                var OnError = "this.style.display='none'";
                var Data = '<img onerror="' + OnError + '" src="' + base_url + '/Asset/Files/Kegiatan/Sekolah/kegiatan/' + full.FileImage + '" style="height:100px;object-fit:cover;object-position:center;">';
                return Data;
            }
        },
        {
            "data": "Url"
        },
    ],
    "bDestroy": true
});
TabelSekolahKegiatan.columns().every(function () {
    var that = this;

    $('input', this.footer()).on('keyup change clear', function () {
        if (that.search() !== this.value) {
            that
                .search(this.value)
                .draw();
        }
    });
});
$(".dataTables_filter").css("display", "none");
$(".dataTables_length").css("display", "none");
//////// START SECTION Tabel Kegiatan Sekolah ///////////

$("input[name='Judul']", "#FormSekolahKegiatan").keyup(function () {
    $("input[name='Url']", "#FormSekolahKegiatan").val(regexUrl($("input[name='Judul']", "#FormSekolahKegiatan").val()));
});

function TambahData() {
    $("#ModalSekolahKegiatan").modal("show");
    $('#FormSekolahKegiatan')[0].reset();
    $("#BtnSaveSekolahKegiatan").attr("onClick", "SaveData('TambahData');");
    $(".modal-title").html("Tambah Kegiatan Sekolah");
    $('#Keterangan').summernote('code', '');
    // $('button.note-video-btn').click(function () {
    //     $('#ModalSekolahKegiatan').modal().on('shown', function () {
    //         $('body').css('overflow', 'hidden');
    //     }).on('hidden', function () {
    //         $('body').css('overflow', 'auto');
    //     })
    // })
}

function EditData(IdSekolahKegiatan) {
    $("#ModalSekolahKegiatan").modal("show");
    $('#FormSekolahKegiatan')[0].reset();
    $("#BtnSaveSekolahKegiatan").attr("onClick", "SaveData('EditData');");
    $(".modal-title").html("Edit Kegiatan Sekolah");
    var Url = base_url + "/Sekolahs/GetSekolahKegiatan?IdSekolahKegiatan=" + IdSekolahKegiatan;
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var ResponseData = responsesuccess.Data;
                $("input[name='IdSekolahKegiatan']", "#FormSekolahKegiatan").val(ResponseData.IdSekolahKegiatan).attr("readonly", true);
                $("input[name='Judul']", "#FormSekolahKegiatan").val(ResponseData.Judul);
                $("input[name='Url']", "#FormSekolahKegiatan").val(ResponseData.Url);
                $("textarea[name='Keterangan']", "#FormSekolahKegiatan").val(ResponseData.Keterangan);
                $('#Keterangan').summernote('code', ResponseData.Keterangan);
                $("#ThumbnailImageSK").removeAttr("src").attr("src", base_url + "/Asset/Files/Kegiatan/Sekolah/kegiatan/" + ResponseData.FileImage);

            } else {
                swal({
                    title: 'Gagal :(',
                    text: responsesuccess.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function SaveData(Tipe) {
    var form = new FormData($('#FormSekolahKegiatan')[0]);
    if (Tipe == "TambahData") {
        var Url = base_url + "/Sekolahs/SekolahKegiatanAdd";
    } else if (Tipe == "EditData") {
        var Url = base_url + "/Sekolahs/SekolahKegiatanEdit";
    }

    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        data: form,
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $('#FormSekolahKegiatan')[0].reset();
                $("#ModalSekolahKegiatan").modal("hide");
                TabelSekolahKegiatan.ajax.reload();
                iziToast.success({
                    title: 'Berhasil Menyimpan Kegiatan Sekolah',
                    message: responsesave.ReturnMessage,
                    position: 'topRight'
                });
            } else if (responsesave.IsSuccess == false) {
                iziToast.error({
                    title: 'Gagal Menyimpan Kegiatan Sekolah',
                    message: responsesave.ReturnMessage,
                    position: 'topRight'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}

function HapusData(IdSekolahKegiatan, Judul) {
    swal({
            title: "Apakah Anda Yakin ?",
            text: "Anda akan menghapus data Kegiatan Sekolah " + Judul,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: true
        },
        function () {
            var Url = base_url + "/Sekolahs/SekolahKegiatanDelete?IdSekolahKegiatan=" + IdSekolahKegiatan;
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesave) {
                    ProgressBar("success");
                    if (responsesave.IsSuccess == true) {
                        TabelSekolahKegiatan.ajax.reload();
                        iziToast.success({
                            title: 'Berhasil Menghapus Kegiatan Sekolah',
                            message: responsesave.ReturnMessage,
                            position: 'topRight'
                        });
                    } else if (responsesave.IsSuccess == false) {
                        iziToast.error({
                            title: 'Gagal Menghapus Kegiatan Sekolah',
                            message: responsesave.ReturnMessage,
                            position: 'topRight'
                        });
                    }
                },
                error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({
                        title: 'Error :(',
                        text: JSON.stringify(responserror) + " : " + e,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            });
        });
}