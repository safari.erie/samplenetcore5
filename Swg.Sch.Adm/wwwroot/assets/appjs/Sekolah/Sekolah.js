﻿/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 15052020
================================================================================================================= */

$.ajax({
    url: base_url + "/Sekolahs/GetSekolah",
    type: "GET",
    dataType: "json",
    success: function (responseedit) {
        $("input[name='IdSekolah']", "#FormSekolah").val(responseedit.Data.IdSekolah);
        $("input[name='Nama']", "#FormSekolah").val(responseedit.Data.Nama);
        $("input[name='NamaSingkat']", "#FormSekolah").val(responseedit.Data.NamaSingkat);
        $("textarea[name='Alamat']", "#FormSekolah").summernote('code',responseedit.Data.Alamat);
        $("input[name='KodePos']", "#FormSekolah").val(responseedit.Data.KodePos);
        $("input[name='Desa']", "#FormSekolah").val(responseedit.Data.Desa);
        $("input[name='Kecamatan']", "#FormSekolah").val(responseedit.Data.Kecamatan);
        $("input[name='Kabupaten']", "#FormSekolah").val(responseedit.Data.Kabupaten);
        $("input[name='Provinsi']", "#FormSekolah").val(responseedit.Data.Provinsi);
        $("input[name='NoTelepon']", "#FormSekolah").val(responseedit.Data.NoTelepon);
        $("input[name='NoFaximili']", "#FormSekolah").val(responseedit.Data.NoFaximili);
        $("input[name='Email']", "#FormSekolah").val(responseedit.Data.Email);
        $("input[name='Web']", "#FormSekolah").val(responseedit.Data.Web);
        $("input[name='Moto']", "#FormSekolah").val(responseedit.Data.Moto);
        $("input[name='Facebook']", "#FormSekolah").val(responseedit.Data.Facebook);
        $("input[name='Twitter']", "#FormSekolah").val(responseedit.Data.Twitter);
        $("input[name='Instagram']", "#FormSekolah").val(responseedit.Data.Instagram);
        $("input[name='Youtube']", "#FormSekolah").val(responseedit.Data.Youtube);
        $("textarea[name='Profile']", "#FormSekolah").summernote('code',responseedit.Data.Profile);
        $("input[name='Video']", "#FormSekolah").val(responseedit.Data.Video);

        $("#ThumbnailImageFotoWeb").removeAttr("src").attr("src", base_url + "/Asset/Files/Kegiatan/Sekolah/" + responseedit.Data.FotoWeb);
        $("#ThumbnailImageLogo").removeAttr("src").attr("src", base_url + "/Asset/Files/Kegiatan/Sekolah/" + responseedit.Data.Logo);
        $("#ThumbnailImageIcon").removeAttr("src").attr("src", base_url + "/Asset/Files/Kegiatan/Sekolah/" + responseedit.Data.Icon);
    }
});

function SaveSekolah() {
    var formData = new FormData($('#FormSekolah')[0]);
    $.ajax({
        url: base_url + "/Sekolahs/SekolahEdit",
        type: "POST",
        data: formData,
        dataType: "json",
        contentType: false,
        cache: true,
        processData: false,
        success: function (responsesave) {
            if (responsesave.IsSuccess == true) {
                swal({ title: 'Berhasil Edit Sekolah', text: "", confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal Edit Sekolah', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (errorresponse) {
            swal({ title: 'Gagal Edit Sekolah', text: errorresponse, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}
