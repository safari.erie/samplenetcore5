const UrlService = {
  GetBanners: `${base_url}/Sekolahs/GetBanners`,
  AddBanner: `${base_url}/Sekolahs/AddBanner`,
  UpdateBanner: `${base_url}/Sekolahs/UpdateBanner`,
  DeleteBanner: `${base_url}/Sekolahs/DeleteBanner`,
};

//////// START SECTION Tabel Galeri ///////////
$("#TabelBanner tfoot th").each(function () {
  var title = $(this).text();
  $(this).html(
    '<input type="text" class="form-control" placeholder="Cari ' +
      title +
      '" />'
  );
});
var TabelBanner = $("#TabelBanner").DataTable({
  paging: true,
  searching: true,
  ordering: true,
  info: true,
  pageLength: 10,
  lengthChange: true,
  scrollX: true,
  processing: true,
  ajax: {
    url: UrlService.GetBanners,
    method: "GET",
    beforeSend: function (xhr) {},
    dataSrc: function (json) {
      if (json.Data == null) {
        if (json.ReturnMessage != "data tidak ada") {
          iziToast.error({
            title: "Gagal Menampilkan Data Tabel Galeri Sekolah",
            message: json.ReturnMessage,
            position: "topRight",
          });
        }
        return json;
      } else {
        return json.Data;
      }
    },
  },
  columns: [
    {
      render: function (data, type, full, meta) {
        var Data = "";
        var img = `${base_url}/Asset/Files/Appl/File/${full.FileImage}`;
        var ParamEdit = `ModalEditData(${full.IdBanner}, '${full.Nama}', '${full.Tipe}', '${img}')`;
        var ParamHapus = "HapusData('" + full.IdBanner + "')";
        Data +=
          '<button type="button" class="btn btn-info btn-sm mr-2" onClick="' +
          ParamEdit +
          '"><i class="fa fa-pencil"></i> Edit</button>';
        Data +=
          '<button type="button" class="btn btn-danger btn-sm" onClick="' +
          ParamHapus +
          '"><i class="fa fa-trash"></i> Hapus</button>';
        return Data;
      },
    },
    { data: "Nama" },
    {
      render: function (data, type, full, meta) {
        if (full.Tipe == "web") {
          return `<span class="badge badge-success">WEB</span>`;
        } else if (full.Tipe == "siswaweb") {
          return `<span class="badge badge-primary">SISWA WEB <i class="fa fa-globe"></i></span>`;
        } else if (full.Tipe == "siswamobile") {
          return `<span class="badge badge-primary">SISWA MOBILE <i class="fa fa-mobile"></i></span>`;
        }
      },
    },
    {
      render: function (data, type, full, meta) {
        var OnError = "this.style.display='none'";
        var Data =
          '<img onerror="' +
          OnError +
          '" src="' +
          base_url +
          "/Asset/Files/Appl/File/" +
          full.FileImage +
          '" style="height:100px;object-fit:cover;object-position:center;">';
        return Data;
      },
    },
    {
      render: function (data, type, full, meta) {
        return indoDate(full.CreatedDate);
      },
    },
  ],
  bDestroy: true,
});
TabelBanner.columns().every(function () {
  var that = this;

  $("input", this.footer()).on("keyup change clear", function () {
    if (that.search() !== this.value) {
      that.search(this.value).draw();
    }
  });
});
$(".dataTables_filter").css("display", "none");
$(".dataTables_length").css("display", "none");
//////// START SECTION Tabel Galeri ///////////

function ModalTambahData() {
  $("#ModalTitle").html("Tambah Banner");
  $("#ModalBanner").modal("show");
  $("#NamaBanner").val("");
  $("#TipeBanner").val("");
  $("#FileImagePreview").fadeOut();
  $("#BtnModalAddEdit").attr("onClick", "SaveData()");
  $('input[type="file"][name="FileImage"]').val("");
}
function ModalEditData(id, nama, tipe, img) {
  $("#ModalTitle").html("Perbarui Banner");
  $("#ModalBanner").modal("show");
  $("#IdBanner").val(id);
  $("#NamaBanner").val(nama);
  $("#TipeBanner").val(tipe);
  $("#FileImagePreview").fadeIn();
  $("#FileImagePreview").attr("src", img);
  $("#BtnModalAddEdit").attr("onClick", "EditData()");
  $('input[type="file"][name="FileImage"]').val("");
}

function SaveData() {
  var fd = new FormData();
  fd.append("Data.Nama", $("#NamaBanner").val());
  fd.append("Data.Tipe", $("#TipeBanner").val());
  fd.append(
    "Data.FileImage",
    $('input[type="file"][name="FileImage"]')[0].files[0]
  );
  $.ajax({
    url: UrlService.AddBanner,
    method: "POST",
    timeout: 0,
    processData: false,
    mimeType: "multipart/form-data",
    contentType: false,
    data: fd,
    dataType: "JSON",
    beforeSend: function (x) {
      ProgressBar("wait");
    },
    success: function (responsesuccess) {
      ProgressBar("success");
      if (responsesuccess.IsSuccess == true) {
        swal({
          title: "Berhasil",
          text: "Berhasil simpan data!",
          confirmButtonClass: "btn-success text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "success",
        });
        $("#ModalBanner").modal("hide");
        $("#TabelBanner").DataTable().ajax.reload();
      } else {
        swal({
          title: "Gagal :(",
          text: responsesuccess.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function EditData() {
  var fd = new FormData();
  fd.append("Data.IdBanner", $("#IdBanner").val());
  fd.append("Data.Nama", $("#NamaBanner").val());
  fd.append("Data.Tipe", $("#TipeBanner").val());
  fd.append(
    "Data.FileImage",
    $('input[type="file"][name="FileImage"]')[0].files[0]
  );
  $.ajax({
    url: UrlService.UpdateBanner,
    method: "POST",
    timeout: 0,
    processData: false,
    mimeType: "multipart/form-data",
    contentType: false,
    data: fd,
    dataType: "JSON",
    beforeSend: function (x) {
      ProgressBar("wait");
    },
    success: function (responsesuccess) {
      ProgressBar("success");
      if (responsesuccess.IsSuccess == true) {
        swal({
          title: "Berhasil",
          text: "Berhasil perbarui data!",
          confirmButtonClass: "btn-success text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "success",
        });
        $("#ModalBanner").modal("hide");
        $("#TabelBanner").DataTable().ajax.reload();
      } else {
        swal({
          title: "Gagal :(",
          text: responsesuccess.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function HapusData(id) {
  swal(
    {
      title: "Apakah Anda Yakin ?",
      text: "Anda akan menghapus data ini",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-success",
      confirmButtonText: "Ya, Saya Yakin!",
      cancelButtonClass: "btn-danger",
      cancelButtonText: "Tidak, Batalkan!",
      closeOnConfirm: true,
    },
    function () {
      $.ajax({
        url: UrlService.DeleteBanner + "?IdBanner=" + id,
        method: "GET",
        dataType: "json",
        headers: {
          "Content-Type": "application/json",
        },
        beforeSend: function (before) {
          ProgressBar("wait");
        },
        success: function (responsesave) {
          ProgressBar("success");
          if (responsesave.IsSuccess == true) {
            $("#TabelBanner").DataTable().ajax.reload();
            iziToast.success({
              title: "Berhasil Menghapus Banner",
              message: responsesave.ReturnMessage,
              position: "topRight",
            });
          } else if (responsesave.IsSuccess == false) {
            iziToast.error({
              title: "Gagal Menghapus Banner",
              message: responsesave.ReturnMessage,
              position: "topRight",
            });
          }
        },
        error: function (responserror, a, e) {
          ProgressBar("success");
          swal({
            title: "Error :(",
            text: JSON.stringify(responserror) + " : " + e,
            confirmButtonClass: "btn-danger text-white",
            confirmButtonText: "Oke, Mengerti",
            type: "error",
          });
        },
      });
    }
  );
}
