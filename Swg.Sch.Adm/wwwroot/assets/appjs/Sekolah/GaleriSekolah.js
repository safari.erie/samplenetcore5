﻿/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 15052020
================================================================================================================= */

$("select[name='IdJenisGaleri']").change(function () {
    if (this.value == 1) {
        $("#JenisImage").removeAttr("style");
        $("#JenisVideo").css("display", "none");
    } else if (this.value == 2) {
        $("#JenisVideo").removeAttr("style");
        $("#JenisImage").css("display", "none");
    } 
});

//////// START SECTION Tabel Galeri ///////////
$('#TabelSekolahGaleri tfoot th').each(function () {
    var title = $(this).text();
    $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
});
var TabelSekolahGaleri = $('#TabelSekolahGaleri').DataTable({
    "paging": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "pageLength": 10,
    "lengthChange": true,
    "scrollX": true,
    "processing": true,
    "ajax": {
        "url": base_url + "/Sekolahs/GetSekolahGaleris",
        "method": 'GET',
        "beforeSend": function (xhr) {
        },
        "dataSrc": function (json) {
            if (json.Data == null) {
                if (json.ReturnMessage != "data tidak ada") {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Galeri Sekolah',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                }
                return json;
            } else {
                return json.Data;
            }
        }
    },
    "columns": [
        {
            "render": function (data, type, full, meta) {
                var Data = "";
                var ParamEdit = "EditData('" + full.IdSekolahGaleri + "')";
                var ParamHapus = "HapusData('" + full.IdSekolahGaleri + "','" + cekQuotes(full.Judul) + "')";
                Data += '<button type="button" class="btn btn-info btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
                Data += '&nbsp;&nbsp;&nbsp;';
                Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                return Data;
            }
        },
        { "data": "Judul" },
        { "data": "JenisGaleri" },
        { "data": "Keterangan" },
        {
            "render": function (data, type, full, meta) {
                var OnError = "this.style.display='none'";
                var Data = '<img onerror="' + OnError + '" src="' + base_url + '/Asset/Files/Galeri/Sekolah/' + full.FileUrl + '" style="height:100px;object-fit:cover;object-position:center;">';
                return Data;
            }
        },
        {
            "render": function (data, type, full, meta) {
                if (full.JenisGaleri == "Video") {
                    return full.UrlVideo;
                } else {
                    return null;
                }
            }
        }
    ],
    "bDestroy": true
});
TabelSekolahGaleri.columns().every(function () {
    var that = this;

    $('input', this.footer()).on('keyup change clear', function () {
        if (that.search() !== this.value) {
            that
                .search(this.value)
                .draw();
        }
    });
});
$(".dataTables_filter").css("display", "none");
$(".dataTables_length").css("display", "none");
//////// START SECTION Tabel Galeri ///////////

function TambahData() {
    $("#ModalSekolahGaleri").modal("show");
    $('#FormSekolahGaleri')[0].reset();
    $("#BtnSaveSekolahGaleri").attr("onClick", "SaveData('TambahData');");
    $(".modal-title").html("Form Tambah Galeri Sekolah");
    ComboGetJenisGaleri(function (obj) {
        $("select[name='IdJenisGaleri']").html(obj);
    });
    $("#ThumbnailImageSG").removeAttr("src");
}

function EditData(IdSekolahGaleri) {
    $("#ModalSekolahGaleri").modal("show");
    $('#FormSekolahGaleri')[0].reset();
    $("#BtnSaveSekolahGaleri").attr("onClick", "SaveData('EditData');");
    $(".modal-title").html("Form Edit Galeri Sekolah");
    var Url = base_url + "/Sekolahs/GetSekolahGaleri?IdSekolahGaleri=" + IdSekolahGaleri;
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        beforeSend: function (beforesend) {
            ProgressBar("wait");
        },
        success: function (responsesuccess) {
            ProgressBar("success");
            if (responsesuccess.IsSuccess == true) {
                var ResponseData = responsesuccess.Data;
                $("input[name='IdSekolahGaleri']", "#FormSekolahGaleri").val(ResponseData.IdSekolahGaleri).attr("readonly", true);
                $("input[name='Judul']", "#FormSekolahGaleri").val(ResponseData.Judul);
                $("textarea[name='Keterangan']", "#FormSekolahGaleri").val(ResponseData.Keterangan);
                CKEDITOR.instances.Keterangan.setData(ResponseData.Keterangan);

                if (ResponseData.IdJenisGaleri == 1) {
                    $("#JenisImage").removeAttr("style");
                    $("#JenisVideo").css("display", "none");
                    $("#ThumbnailImageSG").removeAttr("src").attr("src", base_url + "/Asset/Files/Galeri/Sekolah/" + ResponseData.FileUrl);
                } else if (ResponseData.IdJenisGaleri == 2) {
                    $("input[name='UrlVideo']", "#FormSekolahGaleri").val(ResponseData.UrlVideo);
                    $("#JenisVideo").removeAttr("style");
                    $("#JenisImage").css("display", "none");
                } 

                ComboGetJenisGaleri(function (obj) {
                    $("select[name='IdJenisGaleri']").html(obj);
                }, ResponseData.IdJenisGaleri);

            } else {
                swal({ title: 'Gagal :(', text: responsesuccess.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function SaveData(Tipe) {
    var form = new FormData($('#FormSekolahGaleri')[0]);
    if (Tipe == "TambahData") {
        var Url = base_url + "/Sekolahs/SekolahGaleriAdd";
        var TextAlert = "Berhasil Tambah Data Galeri";
    } else if (Tipe == "EditData") {
        var Url = base_url + "/Sekolahs/SekolahGaleriEdit";
        var TextAlert = "Berhasil Edit Data Galeri";
    }

    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        data: form,
        contentType: false,
        cache: true,
        processData: false,
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                $('#FormSekolahGaleri')[0].reset();
                $("#ModalSekolahGaleri").modal("hide");
                TabelSekolahGaleri.ajax.reload();
                iziToast.success({
                    title: 'Berhasil Menyimpan Galeri Sekolah',
                    message: TextAlert,
                    position: 'topRight'
                });
            } else if (responsesave.IsSuccess == false) {
                iziToast.success({
                    title: 'Berhasil Menyimpan Galeri Sekolah',
                    message: responsesave.ReturnMessage,
                    position: 'topRight'
                });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

function HapusData(IdSekolahGaleri, Judul) {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus data Galeri " + Judul,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: true
    },
        function () {
            var Url = base_url + "/Sekolahs/SekolahGaleriDelete?IdSekolahGaleri=" + IdSekolahGaleri;
            $.ajax({
                url: Url,
                method: "POST",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesave) {
                    ProgressBar("success");
                    if (responsesave.IsSuccess == true) {
                        TabelSekolahGaleri.ajax.reload();
                        iziToast.success({
                            title: 'Berhasil Menghapus Galeri Sekolah',
                            message: responsesave.ReturnMessage,
                            position: 'topRight'
                        });
                    } else if (responsesave.IsSuccess == false) {
                        iziToast.error({
                            title: 'Gagal Menghapus Galeri Sekolah',
                            message: responsesave.ReturnMessage,
                            position: 'topRight'
                        });
                    }
                }, error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                }
            });
        });
}

