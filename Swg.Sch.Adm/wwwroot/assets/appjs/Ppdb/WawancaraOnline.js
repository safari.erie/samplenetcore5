$(document).ready((e) => {
    GetPpdbDaftarList();
})

const UrlService = {
    GetPpdbDaftarList: `${base_url}/api/tests/GetPpdbDaftarListTemp?proses=Wawancara Online`,
};

const GetPpdbDaftarList = () => {
    var res = initAjax(UrlService.GetPpdbDaftarList);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        $('#empty-data').hide();
        $('#data').fadeIn();
        $('#TabelData tfoot th').each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
        });
        var TabelData = $('#TabelData').DataTable({
            "paging": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "pageLength": 10,
            "lengthChange": true,
            "scrollX": true,
            "processing": true,
            "data": res.Data,
            "columns": [{
                    // "render": function (data, type, full, meta) {
                    //     var Data = `<button type="button" class="btn btn-primary btn-sm" onClick="Preview(${full.IdPpdbDaftar})"><i class="fa fa-rocket"></i> Proses</button>`;
                    //     return Data;
                    // }
                    "render": function (data, type, full, meta) {
                        var Data = ``;
                        if (full.StatusPpdb == 'Input Form Wawancara') {
                            Data += `<button type="button" class="btn btn-danger btn-sm" onClick="Preview(${full.IdPpdbDaftar})"><i class="fa fa-times"></i> Tidak Hadir</button>
                                    <button type="button" class="btn btn-primary btn-sm" onClick="Preview(${full.IdPpdbDaftar})"><i class="fa fa-rocket"></i> Hadir</button>`;
                        }
                        if (full.StatusPpdb == 'Verifikasi Berkas') {
                            Data += `<button type="button" class="btn btn-info btn-sm" onClick="Preview(${full.IdPpdbDaftar})"><i class="fa fa-key"></i> Aktivasi VA</button>`;
                        }
                        if (full.StatusPpdb == 'Verifikasi Lunas Daftar') {
                            Data += `<button type="button" class="btn btn-warning btn-sm" onClick="Preview(${full.IdPpdbDaftar})"><i class="fa fa-money"></i> Menunggu Pembayaran</button>`;
                        }
                        return Data;
                    }
                },
                {
                    "data": "IdPpdbDaftar"
                },
                {
                    "data": "Nama"
                },
                {
                    "data": "StatusBayarPendaftaran"
                },
                {
                    "data": "StatusObservasi"
                },
                {
                    "data": "StatusBayarPendidikan"
                },
                {
                    "data": "StatusProsesPpdb"
                },
                {
                    "data": "StatusPpdb"
                    // "render": function (data, type, full, meta) {
                    //     var Data = ``;
                    //     if (full.StatusPpdb == 'Registrasi') {
                    //         Data += `<button type="button" class="btn btn-danger btn-sm" onClick="Preview(${full.IdPpdbDaftar})"><i class="fa fa-times"></i> Tidak Lengkap</button>
                    //                 &nbsp;
                    //                 <button type="button" class="btn btn-primary btn-sm" onClick="Preview(${full.IdPpdbDaftar})"><i class="fa fa-rocket"></i> Proses</button>`;
                    //     } 
                    //     if (full.StatusPpdb == 'Verifikasi Berkas') {
                    //         Data += `<button type="button" class="btn btn-info btn-sm" onClick="Preview(${full.IdPpdbDaftar})"><i class="fa fa-key"></i> Aktivasi VA</button>`;
                    //     } 
                    //     if (full.StatusPpdb == 'Registrasi') {
                    //         Data += `<button type="button" class="btn btn-warning btn-sm" onClick="Preview(${full.IdPpdbDaftar})"><i class="fa fa-money"></i> Menunggu Pembayaran</button>`;
                    //     } 
                    //     return Data;
                    // }
                },
            ],
            "bDestroy": true
        });
        TabelData.columns().every(function () {
            var that = this;
    
            $('input', this.footer()).on('keyup change clear', function () {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });
        $(".dataTables_filter").css("display", "none");
        $(".dataTables_length").css("display", "none");
    } else {
        swal({
            title: 'Gagal',
            text: res.ReturnMessage,
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
    }
}

const Preview = () => {
    $('#ModalPreviewBerkas').modal('show');
}