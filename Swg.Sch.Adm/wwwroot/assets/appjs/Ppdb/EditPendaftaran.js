$(document).ready((e) => {
    GetPpdbDaftars();

    $("#IdStatusHidupIbu").change((e) => {
        if (e.target.value == "0") {
            $('#DivSudahTiadaIbu').fadeIn();
        } else {
            $('#TanggalMeninggalIbu').val('');
            $('#DivSudahTiadaIbu').fadeOut();
        }
    });
    $("#IdStatusHidupAyah").change((e) => {
        if (e.target.value == "0") {
            $('#DivSudahTiadaAyah').fadeIn();
        } else {
            $('#TanggalMeninggalAyah').val('');
            $('#DivSudahTiadaAyah').fadeOut();
        }
    });
})

const UrlService = {
    GetPpdbDaftars: `${base_url}/api/ppdbs/GetPpdbDaftars`,
    GetPpdbDaftar: `${base_url}/api/ppdbs/GetPpdbDaftar`,
    GetPpdbSiswa: `${base_url}/api/ppdbs/GetPpdbSiswa`,
    EditPpdbDaftar: `${base_url}/api/ppdbs/EditPpdbDaftar`,
    EditPpdbSiswa: `${base_url}/api/ppdbs/EditPpdbSiswa`,
};

const GetPpdbDaftars = () => {
    var res = initAjax(`${UrlService.GetPpdbDaftars}?status=0`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        $('#empty-data').hide();
        $('#data').fadeIn();
        $('#TabelData tfoot th').each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
        });
        var TabelData = $('#TabelData').DataTable({
            "paging": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "pageLength": 10,
            "lengthChange": true,
            "scrollX": true,
            "processing": true,
            "data": res.Data,
            "columns": [{
                    "render": function (data, type, full, meta) {
                        var Data = `<button type="button" class="btn btn-primary btn-sm" onClick="Preview(${full.IdPpdbDaftar})"><i class="fa fa-pencil-square-o"></i> Perbarui</button>`;
                        return Data;
                    }
                },
                {
                    "data": "IdPpdbDaftar"
                },
                {
                    "data": "Nama"
                },
                {
                    "render": function (data, type, full, meta) {
                        var Data = full.NoHandphone;
                        var Nmr = (full.NoHandphone).length;
                        Data += ` <a href="http://wa.me/62${Data.slice(1, Nmr)}" target="_blank" class="text-success"><i class="fa fa-whatsapp"></i></a>`;
                        return Data;
                    }
                },
                {
                    "render": function (data, type, full, meta) {
                        var Data = full.Email;
                        Data += ` <a href="mailto:${Data}" target="_blank" style=" color: red;"><b><i class="fa fa-envelope-o"></i></b></a>`
                        return Data;
                    }
                },
                {
                    "data": "JenisKategoriPendaftaran"
                },
                {
                    "data": "JenisPendaftaran"
                },
                {
                    "data": "JalurPendaftaran"
                },
                {
                    "render": function (data, type, full, meta) {
                        var Data = '';
                        Data = `<span class="badge badge-info"><i class="fa fa-clock-o"></i> ${full.Status}</span>`;
                        if (full.Pin != null)
                            Data = '<span class="badge badge-success"><i class="fa fa-check"></i> Lunas</span>';
                        if (full.Status == "Daftar Tunggu")
                            Data = '<span class="badge badge-warning"><i class="fa fa-warning"></i>  Daftar Tunggu</span>';
                        
                        return Data;
                    }
                },
                {
                    "data": "Unit"
                },
                {
                    "render": function (data, type, full, meta) {
                        var Data = full.Kelas + ' ' + full.Peminatan;
                        return Data;
                    }
                },
            ],
            "bDestroy": true
        });
        TabelData.columns().every(function () {
            var that = this;
    
            $('input', this.footer()).on('keyup change clear', function () {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });
        $(".dataTables_filter").css("display", "none");
        $(".dataTables_length").css("display", "none");
    } else {
        $('#InfoEmptyData').html(res.ReturnMessage);
        $('#empty-data').show();
        $('#data').hide();
    }
}


const Preview = (IdPpdbDaftar) => {
    $('#nav-tab a[href="#nav-siswa-edit"]').tab('show');

    var res = initAjax(`${UrlService.GetPpdbDaftar}?IdPpdbDaftar=${IdPpdbDaftar}`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        var data = res.Data;

        var res = initAjax(`${UrlService.GetPpdbSiswa}?IdPpdbDaftar=${IdPpdbDaftar}`);
        if (res) ProgressBar("success");
        if (res.IsSuccess) {
            var data = res.Data;
            $('#ModalPreviewSiswa').modal('show');
            $('#PasFoto').attr('src', base_url + "/Asset/Files/Ppdb/Berkas/" + data.Foto);
            $('#linkHeaderEditPasFoto').attr('href', base_url + "/Asset/Files/Ppdb/Berkas/" + data.Foto);
            $('span#htmlNamaSiswa').html(data.Nama);
            $('span#htmlKategori').html(data.JenisKategoriPendaftaran);
            $('span#htmlKelas').html(data.Kelas + ' ' + data.Peminatan);
            $('span#htmlJenisDaftar').html(data.JenisPendaftaran);
            
            //siswa
            $('#IdPpdbDaftar').val(data.IdPpdbDaftar);
            $('#JenisKategoriPendaftaran').val(data.JenisKategoriPendaftaran);
            $('#JenisPendaftaran').val(data.JenisPendaftaran);
            $('#JalurPendaftaran').val(data.JalurPendaftaran);
            $('#Unit').val(data.Unit);
            $('#Kelas').val(data.Kelas);
            if (data.Unit == "SMAIT AT Taufiq")
                $('#Kelas').val(data.Kelas + ' - ' + data.Peminatan);
            $('#TanggalLahir').val(data.TanggalLahir);

            $('#IdPpdbDaftar').val(data.IdPpdbDaftar);
            GetKategoriDaftars(function (obj) {
                $("select[name='IdJenisKategoriPendaftaran']").html(obj);
            }, data.IdJenisKategoriPendaftaran);
            ComboGetJenisPendaftarans(function (obj) {
                $("select[name='IdJenisPendaftaran']").html(obj);
            }, data.IdJenisPendaftaran);
            ComboGetJalurPendaftaran(function (obj) {
                $("select[name='IdJalurPendaftaran']").html(obj);
            }, data.IdJalurPendaftaran);
            GetKelasPpdbs(function (obj) {
                $("select[name='IdPpdbKelas']").html(obj);
            }, data.IdPpdbKelas);
            GetPpdbSeragams(function (obj) {
                $("select[name='IdPpdbSeragam']").html(obj);
            }, data.IdPpdbSeragam);
            ComboGetAgama(function (obj) {
                $("select[name='IdAgama']").html(obj);
            }, data.IdAgama);
            ComboGetJenisWargaNegara(function (obj) {
                $("select[name='IdKewarganegaraan']").html(obj);
            }, data.IdKewarganegaraan);
            ComboGetTinggalSiswa(function (obj) {
                $("select[name='IdTinggal']").html(obj);
            }, data.IdTinggal);
            ComboGetJenisTransportasi(function (obj) {
                $("select[name='IdTransportasi']").html(obj);
            }, data.IdTransportasi);
            ComboGetJenisBahasa(function (obj) {
                $("select[name='IdBahasa']").html(obj);
            }, data.IdBahasa);
            $('#Nik').val(data.Nik);
            $('#Nisn').val(data.Nisn);
            $('#Nama').val(data.Nama);
            $('#NamaPanggilan').val(data.NamaPanggilan);
            $('#TempatLahir').val(data.TempatLahir);
            $('#KdJenisKelamin').val(data.KdJenisKelamin);
            $('#KdGolonganDarah').val(data.KdGolonganDarah);
            $('#AlamatTinggal').val(data.AlamatTinggal);
            $('#Email').val(data.Email);
            $('#NoHandphone').val(data.NoHandphone);
            $('#NoDarurat').val(data.NoDarurat);
            $('#TinggiBadan').val(data.TinggiBadan);
            $('#BeratBadan').val(data.BeratBadan);
            $('#AnakKe').val(data.AnakKe);
            $('#JumlahSodaraKandung').val(data.JumlahSodaraKandung);
            $('#JumlahSodaraTiri').val(data.JumlahSodaraTiri);
            $('#JumlahSodaraAngkat').val(data.JumlahSodaraAngkat);
            $('#JarakKeSekolah').val(data.JarakKeSekolah);
            $('#WaktuTempuh').val(data.WaktuTempuh);
            $('#PenyakitDiderita').val(data.PenyakitDiderita);
            $('#KelainanJasmani').val(data.KelainanJasmani);
            $('#SekolahAsal').val(data.SekolahAsal);
            $('#AlamatSekolahAsal').val(data.AlamatSekolahAsal);
            $('#NspnSekolahAsal').val(data.NspnSekolahAsal);

            //ortu
            $('#DivSudahTiadaIbu').hide();
            if (data.DataOrtu[0].IdStatusHidup == 0) {
                $('#DivSudahTiadaIbu').fadeIn();
            }
            $('#IdTipeIbu').val(data.DataOrtu[0].IdTipe);
            $('#NamaIbu').val(data.DataOrtu[0].Nama);
            $('#AlamatIbu').val(data.DataOrtu[0].Alamat);
            $('#NikIbu').val(data.DataOrtu[0].Nik);
            $('#NamaInstansiIbu').val(data.DataOrtu[0].NamaInstansi);
            $('#JabatanIbu').val(data.DataOrtu[0].Jabatan);
            $('#EmailIbu').val(data.DataOrtu[0].Email);
            $('#NoHandphoneIbu').val(data.DataOrtu[0].NoHandphone);
            $('#NoTelpRumahIbu').val(data.DataOrtu[0].NoTelpRumah);
            ComboGetJenisPekerjaan(function (obj) {
                $('#IdJenisPekerjaanIbu').html(obj);
            }, data.DataOrtu[0].IdJenisPekerjaan);
            ComboGetAgama(function (obj) {
                $('#IdAgamaIbu').html(obj);
            }, data.DataOrtu[0].IdAgama);
            ComboGetJenisWargaNegara(function (obj) {
                $('#IdKewarganegaraanIbu').html(obj);
            }, data.DataOrtu[0].IdKewarganegaraan);
            ComboGetListJenjangPendidikan(function (obj) {
                $('#IdJenjangPendidikanIbu').html(obj);
            }, data.DataOrtu[0].IdJenjangPendidikan);
            $('#IdStatusPenikahanIbu').val(data.DataOrtu[0].IdStatusPenikahan);
            $('#TempatLahirIbu').val(data.DataOrtu[0].TempatLahir);
            $('#TanggalLahirIbu').val(data.DataOrtu[0].TanggalLahir);
            $('#IdStatusHidupIbu').val(data.DataOrtu[0].IdStatusHidup);
            $('#TanggalMeninggalIbu').val(data.DataOrtu[0].TanggalMeninggal);

            $('#DivSudahTiadaAyah').hide();
            if (data.DataOrtu[1].IdStatusHidup == 0) {
                $('#DivSudahTiadaAyah').fadeIn();
            }
            $('#IdTipeAyah').val(data.DataOrtu[1].IdTipe);
            $('#NamaAyah').val(data.DataOrtu[1].Nama);
            $('#AlamatAyah').val(data.DataOrtu[1].Alamat);
            $('#NikAyah').val(data.DataOrtu[1].Nik);
            $('#NamaInstansiAyah').val(data.DataOrtu[1].NamaInstansi);
            $('#JabatanAyah').val(data.DataOrtu[1].Jabatan);
            $('#EmailAyah').val(data.DataOrtu[1].Email);
            $('#NoHandphoneAyah').val(data.DataOrtu[1].NoHandphone);
            $('#NoTelpRumahAyah').val(data.DataOrtu[1].NoTelpRumah);
            ComboGetJenisPekerjaan(function (obj) {
                $('#IdJenisPekerjaanAyah').html(obj);
            }, data.DataOrtu[1].IdJenisPekerjaan);
            ComboGetAgama(function (obj) {
                $('#IdAgamaAyah').html(obj);
            }, data.DataOrtu[1].IdAgama);
            ComboGetJenisWargaNegara(function (obj) {
                $('#IdKewarganegaraanAyah').html(obj);
            }, data.DataOrtu[1].IdKewarganegaraan);
            ComboGetListJenjangPendidikan(function (obj) {
                $('#IdJenjangPendidikanAyah').html(obj);
            }, data.DataOrtu[0].IdJenjangPendidikan);
            $('#IdStatusPenikahanAyah').val(data.DataOrtu[1].IdStatusPenikahan);
            $('#TempatLahirAyah').val(data.DataOrtu[1].TempatLahir);
            $('#TanggalLahirAyah').val(data.DataOrtu[1].TanggalLahir);
            $('#IdStatusHidupAyah').val(data.DataOrtu[1].IdStatusHidup);
            $('#TanggalMeninggalAyah').val(data.DataOrtu[1].TanggalMeninggal);
            
            //berkas
            $('#DivSuratKesehatan').hide();
            $('#DivBebasNarkoba').hide();
            if (data.Unit == "SMAIT AT Taufiq") {
                $('#DivSuratKesehatan').fadeIn();
                $('#DivBebasNarkoba').fadeIn();
            }
            $('#ImgFileFoto').attr('src', base_url + "/Asset/Files/Ppdb/Berkas/" + data.Foto);
            $('#ImgFileKtpIbu').attr('src', base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileKtpIbu);
            $('#ImgFileKtpAyah').attr('src', base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileKtpAyah);
            $('#ImgFileKk').attr('src', base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileKartuKeluarga);
            $('#ImgFileAkte').attr('src', base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileAkte);
            $('#ImgFileSuratKesehatan').attr('src', base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileSuratKesehatan);
            $('#ImgFileBebasNarkoba').attr('src', base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileBebasNarkoba);

            $('#FileFoto').val('');
            $('#FileKtpIbu').val('');
            $('#FileKtpAyah').val('');
            $('#FileKk').val('');
            $('#FileAkte').val('');
            $('#FileSuratKesehatan').val('');
            $('#FileBebasNarkoba').val('');
        } else {
            $('#XNikOrtu').val(data.NikOrtu);
            $('#XSekolah').val(data.Sekolah);
            $('#ModalPreview').modal('show');
            $('#DivInfoSodara').hide();
            if (data.NisSodara != null) {
                $('#InfoNisSodara').html(data.NisSodara);
                $('#DivInfoSodara').fadeIn();
            }
            $('#XIdPpdbDaftar').val(data.IdPpdbDaftar);
            $('#XJenisKategoriPendaftaran').val(data.JenisKategoriPendaftaran);
            $('#XJenisPendaftaran').val(data.JenisPendaftaran);
            $('#XJalurPendaftaran').val(data.JalurPendaftaran);
            $('#XUnit').val(data.Unit);
            $('#XKelas').val(data.Kelas);
            if (data.Unit == "SMAIT AT Taufiq")
                $('#XKelas').val(data.Kelas + ' - ' + data.Peminatan);
            $('#XNama').val(data.Nama);
            $('#XKdJenisKelamin').val(data.KdJenisKelamin);
            $('#XTempatLahir').val(data.TempatLahir);
            $('#XTanggalLahir').val(data.TanggalLahir);
            $('#XEmail').val(data.Email);
            $('#XNoHandphone').val(data.NoHandphone);
        }
        
        

    } else {
        swal({
            title: 'Gagal',
            text: res.ReturnMessage,
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
    }
}

const EditPpdbDaftar = () => {
    var IdPpdbDaftar, Nama;
    Nama = $('#Nama').val();


    var formObj = $('#FormEditPendaftaran').serializeObject();
    var fd = new FormData($('#FormEditPendaftaran')[0]);
    fd.append("Nama", formObj.Nama);
    fd.append("KdJenisKelamin", formObj.KdJenisKelamin);
    fd.append("TempatLahir", formObj.TempatLahir);
    fd.append("TanggalLahir", formObj.TanggalLahir);
    fd.append("Email", formObj.Email);
    fd.append("NoHandphone", formObj.NoHandphone);
    fd.append("NikOrtu", formObj.NikOrtu);

    $.ajax({
        url: `${UrlService.EditPpdbDaftar}?IdPpdbDaftar=${formObj.IdPpdbDaftar}`,
        method: "POST",
        timeout: 0,
        processData: false,
        mimeType: "multipart/form-data",
        contentType: false,
        data: fd,
        dataType: 'JSON',
        beforeSend: (e) => {
            ProgressBar('wait');
        }, 
        success: (res) => {
            ProgressBar('success');
            if (res.IsSuccess) {
                $('#ModalPreview').modal('hide');
                GetPpdbDaftars();
                swal({
                    title: 'Sukses',
                    text: 'Anda berhasil memperbarui data peserta : '+ Nama,
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                }); 
            } else {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (err, a, e) {
            ProgressBar("success");
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(err),
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

const EditPpdbSiswa = () => {
    var formObj = $('#FormEditPendaftaranSiswa').serializeObject();
    var fd = new FormData($('#FormEditPendaftaranSiswa')[0]);

    fd.append("Siswa.IdPpdbDaftar", formObj.IdPpdbDaftar);
    fd.append("Siswa.IdPpdbKelas", formObj.IdPpdbKelas);
    fd.append("Siswa.IdJenisKategoriPendaftaran", formObj.IdJenisKategoriPendaftaran);
    fd.append("Siswa.IdJenisPendaftaran", formObj.IdJenisPendaftaran);
    fd.append("Siswa.IdJalurPendaftaran", formObj.IdJalurPendaftaran);
    fd.append("Siswa.Nik", formObj.Nik);
    fd.append("Siswa.Nisn", formObj.Nisn);
    fd.append("Siswa.Nama", formObj.Nama);
    fd.append("Siswa.TempatLahir", formObj.TempatLahir);
    fd.append("Siswa.TanggalLahir", formObj.TanggalLahir);
    fd.append("Siswa.IdAgama", formObj.IdAgama);
    fd.append("Siswa.IdKewarganegaraan", formObj.IdKewarganegaraan);
    fd.append("Siswa.KdJenisKelamin", formObj.KdJenisKelamin);
    fd.append("Siswa.KdGolonganDarah", formObj.KdGolonganDarah);
    fd.append("Siswa.AlamatTinggal", formObj.AlamatTinggal);
    fd.append("Siswa.IdTinggal", formObj.IdTinggal);
    fd.append("Siswa.Email", formObj.Email);
    fd.append("Siswa.NoHandphone", formObj.NoHandphone);
    fd.append("Siswa.NoDarurat", formObj.NoDarurat);
    fd.append("Siswa.IdBahasa", formObj.IdBahasa);
    fd.append("Siswa.TinggiBadan", formObj.TinggiBadan);
    fd.append("Siswa.BeratBadan", formObj.BeratBadan);
    fd.append("Siswa.AnakKe", formObj.AnakKe);
    fd.append("Siswa.JumlahSodaraKandung", formObj.JumlahSodaraKandung);
    fd.append("Siswa.JumlahSodaraTiri", formObj.JumlahSodaraTiri);
    fd.append("Siswa.JumlahSodaraAngkat", formObj.JumlahSodaraAngkat);
    fd.append("Siswa.IdTransportasi", formObj.IdTransportasi);
    fd.append("Siswa.JarakKeSekolah", formObj.JarakKeSekolah);
    fd.append("Siswa.WaktuTempuh", formObj.WaktuTempuh);
    fd.append("Siswa.PenyakitDiderita", formObj.PenyakitDiderita);
    fd.append("Siswa.KelainanJasmani", formObj.KelainanJasmani);
    fd.append("Siswa.SekolahAsal", formObj.SekolahAsal);
    fd.append("Siswa.AlamatSekolahAsal", formObj.AlamatSekolahAsal);
    fd.append("Siswa.NspnSekolahAsal", formObj.NspnSekolahAsal);
    fd.append("Siswa.IdPpdbSeragam", formObj.IdPpdbSeragam);

    if ($('select[name="IdTipe[0]"]').val() != "") {
        
       
        $('input[name^="IdTipe[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'IdTipe', v.value);
        });
        $('input[name^="Nama[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'Nama[]', v.value);
        });
        $('input[name^="Alamat[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'Alamat', v.value);
            console.log(i)
        });
        $('input[name^="Nik[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'Nik', v.value);
        });
        $('select[name^="IdJenisPekerjaan[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'IdJenisPekerjaan', v.value);
        });
        $('input[name^="NamaInstansi[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'NamaInstansi', v.value);
        });
        $('input[name^="Jabatan[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'Jabatan', v.value);
        });
        $('input[name^="Email[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'Email', v.value);
        });
        $('input[name^="NoHandphone[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'NoHandphone', v.value);
        });
        $('input[name^="NoTelpRumah[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'NoTelpRumah', v.value);
        });
        $('select[name^="IdAgama[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'IdAgama', v.value);
        });
        $('select[name^="IdKewarganegaraan[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'IdKewarganegaraan', v.value);
        });
        $('select[name^="IdJenjangPendidikan[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'IdJenjangPendidikan', v.value);
        });
        $('select[name^="IdStatusPenikahan[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'IdStatusPenikahan', v.value);
        });
        $('input[name^="TanggalLahir[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'TanggalLahir', v.value);
        });
        $('input[name^="TempatLahir[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'TempatLahir', v.value);
        });
        $('select[name^="IdStatusHidup[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'IdStatusHidup', v.value);
        });
        $('input[name^="TanggalMeninggal[]"]').each(function (i, v) {
            fd.append('Ortus[' + i + '].' + 'TanggalMeninggal', v.value);
        });

    } else {
        swal({
            title: 'Gagal',
            text: 'data ortu harus diisi',
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
    }

    fd.append('FileFoto', $('input[type="file"][name="FileFoto"]')[0].files[0]);
    fd.append('FileKtpIbu', $('input[type="file"][name="FileKtpIbu"]')[0].files[0]);
    fd.append('FileKtpAyah', $('input[type="file"][name="FileKtpAyah"]')[0].files[0]);
    fd.append('FileKK', $('input[type="file"][name="FileKk"]')[0].files[0]);
    fd.append('FileAkte', $('input[type="file"][name="FileAkte"]')[0].files[0]);
    fd.append('FileSuratKesehatan', $('input[type="file"][name="FileSuratKesehatan"]')[0].files[0]);
    fd.append('FileBebasNarkoba', $('input[type="file"][name="FileBebasNarkoba"]')[0].files[0]);
    
    
    $.ajax({
        url: `${UrlService.EditPpdbSiswa}`,
        method: "POST",
        timeout: 0,
        processData: false,
        mimeType: "multipart/form-data",
        contentType: false,
        data: fd,
        dataType: 'JSON',
        beforeSend: (e) => {
            ProgressBar('wait');
        }, 
        success: (res) => {
            ProgressBar('success');
            if (res.IsSuccess) {
                $('#ModalPreviewSiswa').modal('hide');
                GetPpdbDaftars();
                swal({
                    title: 'Sukses',
                    text: 'Anda berhasil memperbarui data peserta : '+ formObj.Nama,
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                }); 
            } else {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }

        },
        error: function (err, a, e) {
            ProgressBar("success");
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(err),
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}