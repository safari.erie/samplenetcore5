﻿$(document).ready(function () {
  DataList();
});

function DataEditor(id = null) {
  $("div#data").fadeIn();
  $("div#no-data").hide();

  $("div#EditorForm").fadeIn();
  $("div#ListData").hide();
  if (id != null) {
    GetById(id);
  } else {
    $("#FormPpdbKonten")[0].reset();
    $("input#IdPpdbKonten").val("");
    $('textarea[name="Deskripsi"]').summernote("code", "");
  }
}

function DataList() {
  $("#TabelPpdbKonten tfoot th").each(function () {
    var title = $(this).text();
    $(this).html(
      '<input type="text" class="form-control" placeholder="Cari ' +
        title +
        '" />'
    );
  });
  var TabelPpdbKonten = $("#TabelPpdbKonten").DataTable({
    paging: true,
    searching: true,
    ordering: true,
    info: true,
    pageLength: 10,
    lengthChange: true,
    scrollX: true,
    processing: true,
    ajax: {
      url: base_url + "/api/ppdbs/GetPpdbKontens",
      method: "GET",
      beforeSend: function (xhr) {},
      dataSrc: function (json) {
        if (json.Data == null) {
          $("div#empty-data").fadeIn();
          $("div#loading-data").hide();
          $("#InfoEmptyData").html(json.ReturnMessage);
          return json;
        } else {
          $("div#no-data").hide();
          $("div#data").fadeIn();
          return json.Data;
        }
      },
    },
    columns: [
      {
        render: function (data, type, full, meta) {
          var Data = `<button type="button" class="btn btn-info btn-sm" onClick="DataEditor(${full.IdPpdbKonten})"><i class="fa fa-pencil"></i> Perbarui</button>`;
          return Data;
        },
      },
      {
        data: "JenisPpdbKonten",
      },
    ],
    bDestroy: true,
  });
  TabelPpdbKonten.columns().every(function () {
    var that = this;

    $("input", this.footer()).on("keyup change clear", function () {
      if (that.search() !== this.value) {
        that.search(this.value).draw();
      }
    });
  });
  $(".dataTables_filter").css("display", "none");
  $(".dataTables_length").css("display", "none");
}

function DataHapus(Id, Judul) {
  swal(
    {
      title: "Apakah Anda Yakin ?",
      text: "Anda akan menghapus data " + Judul,
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-success",
      confirmButtonText: "Ya, Saya Yakin!",
      cancelButtonClass: "btn-danger",
      cancelButtonText: "Tidak, Batalkan!",
      closeOnConfirm: false,
    },
    function () {
      var Url = base_url + "/api/ppdbs/ppdbinfodelete?idppdbKonten=" + Id;
      $.ajax({
        url: Url,
        method: "get",
        dataType: "json",
        headers: {
          "Content-Type": "application/json",
        },
        beforeSend: function (before) {
          ProgressBar("wait");
        },
        success: function (responsesave) {
          ProgressBar("success");
          if (responsesave.IsSuccess == true) {
            $("#TabelPpdbKonten").DataTable().ajax.reload();
            swal({
              title: "Berhasil Menghapus",
              text: "",
              confirmButtonClass: "btn-success text-white",
              confirmButtonText: "Oke, Mengerti",
              type: "success",
            });
          } else if (responsesave.IsSuccess == false) {
            swal({
              title: "Gagal Menghapus",
              text: responsesave.ReturnMessage,
              confirmButtonClass: "btn-danger text-white",
              confirmButtonText: "Oke, Mengerti",
              type: "error",
            });
          }
        },
        error: function (responserror, a, e) {
          ProgressBar("success");
          swal({
            title: "Error :(",
            text: JSON.stringify(responserror) + " : " + e,
            confirmButtonClass: "btn-danger text-white",
            confirmButtonText: "Oke, Mengerti",
            type: "error",
          });
        },
      });
    }
  );
}

function DataSave() {
  var CekIdPpdbKonten = $("input#IdPpdbKonten").val();

  var fd = new FormData($("#FormPpdbKonten")[0]);
  if (CekIdPpdbKonten != "") {
    fd.append("IdPpdbKonten", CekIdPpdbKonten);
    var url = base_url + "/api/ppdbs/EditPpdbKonten";
  } else {
    var url = base_url + "/api/ppdbs/AddPpdbKonten";
  }

  var IdJenisPpdbKonten = $("select#IdJenisPpdbKonten").val();
  if (IdJenisPpdbKonten == 0) {
    iziToast.error({
      title: "Gagal",
      message: "Silahkan input jenis ppdb terlebih dahulu !",
      position: "topRight",
    });
    return;
  }
  fd.append("IdJenisPpdbKonten", $("select#IdJenisPpdbKonten").val());
  fd.append("Deskripsi", $("textarea#Deskripsi").val());
  $.ajax({
    url: url,
    method: "POST",
    timeout: 0,
    processData: false,
    mimeType: "multipart/form-data",
    contentType: false,
    data: fd,
    dataType: "JSON",
    beforeSend: function (x) {
      ProgressBar("wait");
    },
    success: function (res) {
      ProgressBar("success");
      if (res.IsSuccess) {
        $("#TabelPpdbKonten").DataTable().ajax.reload();
        $("div#EditorForm").hide();
        $("div#ListData").fadeIn();

        iziToast.success({
          title: "Berhasil",
          message: "Data berhasil disimpan",
          position: "topRight",
        });
      } else if (!res.IsSuccess) {
        iziToast.error({
          title: "Gagal",
          message: res.ReturnMessage,
          position: "topRight",
        });
      }
    },
    error: function (err, a, e) {
      ProgressBar("success");
      iziToast.error({
        title: "Terjadi Kesalahan",
        message: JSON.stringify(err),
        position: "topRight",
      });
    },
  });
}

function Kembali() {
  $("div#EditorForm").hide();
  $("div#ListData").fadeIn();
}

function GetById(id = null) {
  $.ajax({
    url: base_url + "/api/ppdbs/GetPpdbKonten?IdPpdbKonten=" + id,
    type: "GET",
    success: function (res) {
      if (res.IsSuccess) {
        $('input[name="IdPpdbKonten"]').val(res.Data.IdPpdbKonten);
        $("#IdJenisPpdbKonten").val(res.Data.IdJenisPpdbKonten);
        $('textarea[name="Deskripsi"]').summernote("code", res.Data.Deskripsi);
      } else if (!res.IsSuccess) {
        iziToast.error({
          title: "Gagal",
          message: res.ReturnMessage,
          position: "topRight",
        });
      }
    },
    error: function (err, a, e) {
      iziToast.error({
        title: "Terjadi Kesalahan",
        message: JSON.stringify(err),
        position: "topRight",
      });
    },
  });
}
