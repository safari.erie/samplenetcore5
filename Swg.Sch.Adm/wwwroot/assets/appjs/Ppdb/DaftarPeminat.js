$(document).ready((e) => {
  GetDaftarPeminatPpdbs();
});

const UrlService = {
  GetDaftarPeminatPpdbs: `${base_url}/api/ppdbs/GetDaftarPeminatPpdbs`,
  GetDaftarPeminatPpdb: `${base_url}/api/ppdbs/GetDaftarPeminatPpdb`,
};

const GetDaftarPeminatPpdbs = () => {
  var res = initAjax(`${UrlService.GetDaftarPeminatPpdbs}`);
  if (res) ProgressBar("success");
  if (res.IsSuccess) {
    $("#empty-data").hide();
    $("#data").fadeIn();
    $("#TabelData tfoot th").each(function () {
      var title = $(this).text();
      $(this).html(
        '<input type="text" class="form-control" placeholder="Cari ' +
          title +
          '" />'
      );
    });
    var TabelData = $("#TabelData").DataTable({
      paging: true,
      searching: true,
      ordering: true,
      info: true,
      pageLength: 10,
      lengthChange: true,
      scrollX: true,
      processing: true,
      data: res.Data,
      columns: [
        {
          data: "NamaLengkap",
        },
        {
          render: function (data, type, full, meta) {
            var Data = full.NomorHp;
            var Nmr = full.NomorHp.length;
            Data += ` <a href="http://wa.me/62${Data.slice(
              1,
              Nmr
            )}" target="_blank" style=" color: green;"><b><i class="fa fa-whatsapp"></i></b></a>`;
            return Data;
          },
        },
        {
          render: function (data, type, full, meta) {
            var Data = full.Email;
            Data += ` <a href="mailto:${Data}" target="_blank" style=" color: red;"><b><i class="fa fa-envelope-o"></i></b></a>`;
            return Data;
          },
        },
        {
          data: "Kelas",
        },
        {
          data: "NamaAyah",
        },
        {
          data: "NamaIbu",
        },
        {
          data: "CreatedDate",
        },
      ],
      bDestroy: true,
    });
    TabelData.columns().every(function () {
      var that = this;
      $("input", this.footer()).on("keyup change clear", function () {
        if (that.search() !== this.value) {
          that.search(this.value).draw();
        }
      });
    });
  } else {
    $("#InfoEmptyData").html(res.ReturnMessage);
    $("#empty-data").show();
    $("#data").hide();
  }
};
