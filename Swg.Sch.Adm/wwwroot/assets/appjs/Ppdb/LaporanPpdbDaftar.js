$(document).ready((e) => {
    GetPpdbDaftars();
})

const UrlService = {
    GetPpdbDaftars: `${base_url}/api/ppdbs/GetPpdbDaftars`,
    GetPpdbDaftar: `${base_url}/api/ppdbs/GetPpdbDaftar`,
};

const GetPpdbDaftars = () => {
    var res = initAjax(`${UrlService.GetPpdbDaftars}?status=0`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        $('#empty-data').hide();
        $('#data').fadeIn();
        $('#TabelData tfoot th').each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
        });
        var TabelData = $('#TabelData').DataTable({
            "paging": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "pageLength": 10,
            "lengthChange": true,
            "scrollX": true,
            "processing": true,
            "data": res.Data,
            "columns": [{
                    "render": function (data, type, full, meta) {
                        var Data = `<button type="button" class="btn btn-primary btn-sm" onClick="Preview(${full.IdPpdbDaftar})"><i class="fa fa-user"></i> Lihat</button>`;
                        return Data;
                    }
                },
                {
                    "data": "IdPpdbDaftar"
                },
                {
                    "data": "Nama"
                },
                {
                    "render": function (data, type, full, meta) {
                        var Data = full.NoHandphone;
                        var Nmr = (full.NoHandphone).length;
                        Data += ` <a href="http://wa.me/62${Data.slice(1, Nmr)}" target="_blank" style=" color: green;"><b><i class="fa fa-whatsapp"></i></b></a>`
                        return Data;
                    }
                },
                {
                    "render": function (data, type, full, meta) {
                        var Data = full.Email;
                        Data += ` <a href="mailto:${Data}" target="_blank" style=" color: red;"><b><i class="fa fa-envelope-o"></i></b></a>`
                        return Data;
                    }
                },
                {
                    "render": function (data, type, full, meta) {
                        var Data = '';
                        Data = `<span class="badge badge-info"><i class="fa fa-clock-o"></i> ${full.Status}</span>`;
                        if (full.Pin != null)
                            Data = '<span class="badge badge-success"><i class="fa fa-check"></i> Lunas</span>';
                        if (full.Status == "Daftar Tunggu")
                            Data = '<span class="badge badge-warning"><i class="fa fa-warning"></i>  Daftar Tunggu</span>';
                        
                        return Data;
                    }
                },
                {
                    "data": "JenisKategoriPendaftaran"
                },
                {
                    "data": "JenisPendaftaran"
                },
                {
                    "data": "JalurPendaftaran"
                },
                {
                    "data": "Unit"
                },
                {
                    "render": function (data, type, full, meta) {
                        var Data = full.Kelas + ' ' + full.Peminatan;
                        return Data;
                    }
                },
            ],
            "bDestroy": true
        });
        TabelData.columns().every(function () {
            var that = this;
    
            $('input', this.footer()).on('keyup change clear', function () {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });
        // $(".dataTables_filter").css("display", "none");
        // $(".dataTables_length").css("display", "none");
    } else {
        $('#InfoEmptyData').html(res.ReturnMessage);
        $('#empty-data').show();
        $('#data').hide();
    }
}


const Preview = (IdPpdbDaftar) => {
    var res = initAjax(`${UrlService.GetPpdbDaftar}?IdPpdbDaftar=${IdPpdbDaftar}`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        var data = res.Data;
        $('#NikOrtu').val(data.NikOrtu);
        $('#Sekolah').val(data.Sekolah);
        $('#ModalPreview').modal('show');
        $('#DivInfoSodara').hide();
        if (data.NisSodara != null) {
            $('#InfoNisSodara').html(data.NisSodara);
            $('#DivInfoSodara').fadeIn();
        }
        $('#IdPpdbDaftar').val(data.IdPpdbDaftar);
        $('#JenisKategoriPendaftaran').val(data.JenisKategoriPendaftaran);
        $('#JenisPendaftaran').val(data.JenisPendaftaran);
        $('#JalurPendaftaran').val(data.JalurPendaftaran);
        $('#Unit').val(data.Unit);
        $('#Kelas').val(data.Kelas);
        if (data.Unit == "SMAIT AT Taufiq")
            $('#Kelas').val(data.Kelas + ' - ' + data.Peminatan);
        $('#Nama').val(data.Nama);
        $('#KdJenisKelamin').val(data.KdJenisKelamin);
        $('#TempatLahir').val(data.TempatLahir);
        $('#TanggalLahir').val(data.TanggalLahir);
        $('#Email').val(data.Email);
        $('#NoHandphone').val(data.NoHandphone);

    } else {
        swal({
            title: 'Gagal',
            text: res.ReturnMessage,
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
    }
}
