﻿const UrlService = {
    GetPpdbDaftars: `${base_url}/api/ppdbs/GetPpdbDaftars`,
    GetPpdbDaftar: `${base_url}/api/ppdbs/GetPpdbDaftar`,
    GetJenisRedaksiEmails: `${base_url}/api/ppdbs/GetJenisRedaksiEmails`,
    ResendEmail: `${base_url}/api/ppdbs/ResendEmail`,
};

$('#TabelResendEmail tfoot th').each(function () {
    var title = $(this).text();
    $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
});
var TabelResendEmail = $('#TabelResendEmail').DataTable({
    "paging": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "pageLength": 10,
    "lengthChange": true,
    "scrollX": true,
    "processing": true,
    "ajax": {
        "url": UrlService.GetPpdbDaftars,
        "method": 'GET',
        "beforeSend": function (xhr) {
        },
        "dataSrc": function (json) {
            if (json.Data == null) {
                iziToast.error({
                    title: 'Gagal Menampilkan Data Tabel Resend Email',
                    message: json.ReturnMessage,
                    position: 'topRight'
                });
                return json;
            } else {
                iziToast.success({
                    title: 'Berhasil Menampilkan Data Tabel Resend Email',
                    message: json.ReturnMessage,
                    position: 'topRight'
                });
                return json.Data;
            }
        }
    },
    "columns": [{
            "render": function (data, type, full, meta) {
                var Data = `<button type="button" class="btn btn-primary btn-sm" onClick="Preview(${full.IdPpdbDaftar})"><i class="fa fa-rocket"></i> Proses</button>`;
                return Data;
            }
        },
        {
            "data": "IdPpdbDaftar"
        },
        {
            "data": "Nama"
        },
        {
            "render": function (data, type, full, meta) {
                var Data = full.NoHandphone;
                var Nmr = (full.NoHandphone).length;
                Data += ` <a href="http://wa.me/62${Data.slice(1, Nmr)}" target="_blank" class="text-success"><i class="fa fa-whatsapp"></i></a>`;
                return Data;
            }
        },
        {
            "data": "JenisKategoriPendaftaran"
        },
        {
            "data": "JenisPendaftaran"
        },
        {
            "data": "JalurPendaftaran"
        },
        {
            "render": function (data, type, full, meta) {
                var Data = '';
                Data = `<span class="badge badge-info"><i class="fa fa-clock-o"></i> ${full.Status}</span>`;
                if (full.Pin != null)
                    Data = '<span class="badge badge-success"><i class="fa fa-check"></i> Lunas</span>';
                if (full.Status == "Daftar Tunggu")
                    Data = '<span class="badge badge-warning"><i class="fa fa-warning"></i>  Daftar Tunggu</span>';
                return Data;
            }
        },
        {
            "data": "Unit"
        },
        {
            "data": "Kelas"
        },
    ],
    "bDestroy": true
});
TabelResendEmail.columns().every(function () {
    var that = this;

    $('input', this.footer()).on('keyup change clear', function () {
        if (that.search() !== this.value) {
            that
                .search(this.value)
                .draw();
        }
    });
});
$(".dataTables_filter").css("display", "none");
$(".dataTables_length").css("display", "none");

const Preview = (IdPpdbDaftar) => {
    var res = initAjax(`${UrlService.GetPpdbDaftar}?IdPpdbDaftar=${IdPpdbDaftar}`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        var data = res.Data;
        $('#ModalResendEmail').modal('show');
        $('#IdPpdbDaftar').val(data.IdPpdbDaftar);
        $('#Nama').val(data.Nama);
        $('#Email').val(data.Email);
        GetJenisRedaksiEmails()

    } else {
        swal({
            title: 'Gagal',
            text: res.ReturnMessage,
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
    }
}

function KirimEmail() {
    var formObj = $('#FormKirimEmail').serializeObject();
    $.ajax({
        url: `${UrlService.ResendEmail}?IdPpdbDaftar=${formObj.IdPpdbDaftar}&IdProses=${formObj.Proses}`,
        type: "GET",
        dataType: "json",
        success: function (responsesendmail) {
            if (responsesendmail.IsSuccess == true) {
                $("#FormKirimEmail")[0].reset();
                $("#ModalResendEmail").modal("hide");
                swal({ title: 'Berhasil Mengirim Email', text: "Berhasil mengirim email ke " + formObj.Email, confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesendmail.IsSuccess == false) {
                swal({ title: 'Gagal Mengirim Email', text: responsesendmail.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            swal({ title: 'Gagal Mengirim Email', text: jqXHR + " " + textStatus + " " + errorThrown, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}

const GetJenisRedaksiEmails = () => {
    var res = initAjax(`${UrlService.GetJenisRedaksiEmails}`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        var data = res.Data;
        var html = ``;
        $.each(data, (i, v) => {
            html += `<option value="${v.Id}">${v.Nama}</option>`;
        })
        $('#Proses').html(html);
    } else {
        swal({
            title: 'Gagal',
            text: res.ReturnMessage,
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
    }
}