$(document).ready((e) => {
    GetPpdbDaftars();
})

const UrlService = {
    GetPpdbDaftars: `${base_url}/api/ppdbs/GetPpdbDaftars`,
    GetPpdbDaftar: `${base_url}/api/ppdbs/GetPpdbDaftar`,
    SetLunasBiayaDaftar: `${base_url}/api/ppdbs/SetLunasBiayaDaftar`,
};

const GetPpdbDaftars = () => {
    var res = initAjax(`${UrlService.GetPpdbDaftars}?status=1`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        $('#empty-data').hide();
        $('#data').fadeIn();
        $('#TabelData tfoot th').each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
        });
        var TabelData = $('#TabelData').DataTable({
            "paging": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "pageLength": 10,
            "lengthChange": true,
            "scrollX": true,
            "processing": true,
            "data": res.Data,
            "columns": [{
                    "render": function (data, type, full, meta) {
                        var Data = `<button type="button" class="btn btn-primary btn-sm" onClick="Preview(${full.IdPpdbDaftar})"><i class="fa fa-rocket"></i> Proses</button>`;
                        return Data;
                    }
                },
                {
                    "data": "IdPpdbDaftar"
                },
                {
                    "data": "Nama"
                },
                {
                    "render": function (data, type, full, meta) {
                        var Data = full.NoHandphone;
                        var Nmr = (full.NoHandphone).length;
                        Data += ` <a href="http://wa.me/62${Data.slice(1, Nmr)}" target="_blank" style=" color: green;"><b><i class="fa fa-whatsapp"></i></b></a>`
                        return Data;
                    }
                },
                {
                    "render": function (data, type, full, meta) {
                        var Data = full.Email;
                        Data += ` <a href="mailto:${Data}" target="_blank" style=" color: red;"><b><i class="fa fa-envelope-o"></i></b></a>`
                        return Data;
                    }
                },
                {
                    "data": "JenisKategoriPendaftaran"
                },
                {
                    "data": "JenisPendaftaran"
                },
                {
                    "data": "JalurPendaftaran"
                },
                {
                    "render": function (data, type, full, meta) {
                        var Data = '';
                        Data = `<span class="badge badge-info"><i class="fa fa-clock-o"></i> ${full.Status}</span>`;
                        if (full.Pin != null)
                            Data = '<span class="badge badge-success"><i class="fa fa-check"></i> Lunas</span>';
                        return Data;
                    }
                },
                {
                    "data": "Unit"
                },
                {
                    "render": function (data, type, full, meta) {
                        var Data = full.Kelas + ' ' + full.Peminatan;
                        return Data;
                    }
                },
            ],
            "bDestroy": true
        });
        TabelData.columns().every(function () {
            var that = this;
    
            $('input', this.footer()).on('keyup change clear', function () {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });
        // $(".dataTables_filter").css("display", "none");
        // $(".dataTables_length").css("display", "none");
    } else {
        $('#InfoEmptyData').html(res.ReturnMessage);
        $('#empty-data').show();
        $('#data').hide();
    }
}

const Preview = (IdPpdbDaftar) => {
    var res = initAjax(`${UrlService.GetPpdbDaftar}?IdPpdbDaftar=${IdPpdbDaftar}`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        var data = res.Data;
        $('#NikOrtu').val(data.NikOrtu);
        $('#Sekolah').val(data.Sekolah);
        $('#collapseDataSiswa').collapse('hide');
        $('#ModalPreview').modal('show');
        $('#DivInfoSodara').hide();
        if (data.NisSodara != null) {
            $('#InfoNisSodara').html(data.NisSodara);
            $('#DivInfoSodara').fadeIn();
        }
        $('#IdPpdbDaftar').val(data.IdPpdbDaftar);
        $('#JenisKategoriPendaftaran').val(data.JenisKategoriPendaftaran);
        $('#JenisPendaftaran').val(data.JenisPendaftaran);
        $('#JalurPendaftaran').val(data.JalurPendaftaran);
        $('#Unit').val(data.Unit);
        $('#Kelas').val(data.Kelas);
        if (data.Unit == "SMAIT AT Taufiq")
            $('#Kelas').val(data.Kelas + ' - ' + data.Peminatan);
        $('#Nama').val(data.Nama);
        $('#KdJenisKelamin').val(data.KdJenisKelamin);
        $('#TempatLahir').val(data.TempatLahir);
        $('#TanggalLahir').val(indoDate(data.TanggalLahir));
        $('#Email').val(data.Email);
        $('#NoHandphone').val(data.NoHandphone);

        if (data.Pin != null) {
            $('#DivInfoLunas').show();
            $('#DivInputPelunasan').hide();
            $('#BtnPelunasan').hide();
        } else {
            $('#DivInfoLunas').hide();
            $('#DivInputPelunasan').show();
            $('#BtnPelunasan').show();
        }

        $('#NamaPeserta').val(data.Nama);
        $('#Tanggal').val('');
        $('#Catatan').val('');

        $('#InfoNamaPeserta').html(data.Nama);
        $('#InfoPin').html(data.Pin);

    } else {
        swal({
            title: 'Gagal',
            text: res.ReturnMessage,
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
    }
}

const SetLunasBiayaDaftar = () => {
    var formObj = $('#FormPelunasan').serializeObject();
    var IdPpdbDaftar = formObj.IdPpdbDaftar;
    var NamaPeserta = formObj.NamaPeserta;
    var Tanggal = formObj.Tanggal;
    var Catatan = formObj.Catatan;
    if (Tanggal == "") {
        swal({
            title: 'Gagal',
            text: 'Tanggal Lunas harus diisi !',
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
        return;
    }
    $.ajax({
        url: `${UrlService.SetLunasBiayaDaftar}?IdPpdbDaftar=${IdPpdbDaftar}&TanggalLunas=${Tanggal}&Catatan=${Catatan}`,
        type: 'GET',
        dataType: 'JSON',
        beforeSend: () => {
            ProgressBar('wait');
        },
        success: (res) => {
            ProgressBar('success');
            if (res.IsSuccess) {
                swal({
                    title: 'Berhasil',
                    text: 'Anda berhasil melakukan pelunasan pada nama peserta : ' + NamaPeserta,
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
                $('#ModalPreview').modal('hide');
                GetPpdbDaftars();
            } else {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: (e, a, x) => {
            ProgressBar('success');
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(e),
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}