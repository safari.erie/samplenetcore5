const GenerateLaporan = () => {
    var formObj = $('#FormLaporanPpdb').serializeObject();

    var fd = new FormData();
    fd.append('TanggalAwal', formObj.TanggalAwal);
    fd.append('TanggalAkhir', formObj.TanggalAkhir);
    fd.append('Data[KuotaPpdbTkA][TotL]', formObj.KuotaPpdbTkATotL);
    fd.append('Data[KuotaPpdbTkA][TotP]', formObj.KuotaPpdbTkATotP);
    fd.append('Data[KuotaPpdbTkB][TotL]', formObj.KuotaPpdbTkBTotL);
    fd.append('Data[KuotaPpdbTkB][TotP]', formObj.KuotaPpdbTkBTotP);
    fd.append('Data[KuotaPpdbSd][TotL]', formObj.KuotaPpdbSdTotL);
    fd.append('Data[KuotaPpdbSd][TotP]', formObj.KuotaPpdbSdTotP);
    fd.append('Data[KuotaPpdbSmp][TotL]', formObj.KuotaPpdbSmpTotL);
    fd.append('Data[KuotaPpdbSmp][TotP]', formObj.KuotaPpdbSmpTotP);
    fd.append('Data[KuotaPpdbSmaIpa][TotL]', formObj.KuotaPpdbSmaIpaTotL);
    fd.append('Data[KuotaPpdbSmaIpa][TotP]', formObj.KuotaPpdbSmaIpaTotP);
    fd.append('Data[KuotaPpdbSmaIps][TotL]', formObj.KuotaPpdbSmaIpsTotL);
    fd.append('Data[KuotaPpdbSmaIps][TotP]', formObj.KuotaPpdbSmaIpsTotP);

    var url = base_url + '/api/download/DownloadLaporanGelombangPpdb';

    $.ajax({
        url: url,
        method: 'POST',
        processData: false,
        contentType: false,
        data: fd,
        xhrFields: {
            responseType: 'blob'
        },
        beforeSend: function(xhr) {
            ProgressBar('wait');
        },
        success: function(message, textStatus, response) {
            ProgressBar('success');
            var header = response.getResponseHeader('Content-Disposition');
            var fileName = header.split("=")[1];
            var blob = new Blob([message]);
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = fileName;
            link.download = link.download.split(';')[0];
            console.log('link -> ', link.download)
            link.click();
        },
        error: function (err, a, e) {
            ProgressBar('success');
            iziToast.error({
                title: 'Terjadi Kesalahan',
                message: JSON.stringify(err),
                position: 'topRight'
            });
        }
    })

}

