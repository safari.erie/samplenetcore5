$(document).ready((e) => {
    ComboGetUnitSklh(function (obj) {
        $("select[name='IdUnit']").html(obj);
    });

    $("select[name='IdUnit']").change((e) => {
        let Id = e.target.value;
        ComboGetKelas(function (obj) {
            $("select[name='IdKelas']").html(obj);
        }, Id);
    });
    
    GetPpdbKelass();
})

const UrlService = {
    GetPpdbKelass: `${base_url}/api/ppdbs/GetPpdbKelass`,
    PpdbKelasAdd: `${base_url}/api/ppdbs/PpdbKelasAdd`,
    PpdbKelasEdit: `${base_url}/api/ppdbs/PpdbKelasEdit`,
    PpdbKelasDelete: `${base_url}/api/ppdbs/PpdbKelasDelete`,
};

const GetPpdbKelass = () => {
    var res = initAjax(UrlService.GetPpdbKelass);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        $('#empty-data').hide();
        $('#data').fadeIn();
        $('#TabelData tfoot th').each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
        });
        var TabelData = $('#TabelData').DataTable({
            "paging": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "pageLength": 10,
            "lengthChange": true,
            "scrollX": true,
            "processing": true,
            "data": res.Data,
            "columns": [{
                    "render": function (data, type, full, meta) {
                    var Data = `<button type="button" class="btn btn-primary btn-sm" onClick="GetPpdbKelasBy(${full.IdPpdbKelas})"><i class="fa fa-rocket"></i> Edit</button>
                    <button type="button" class="btn btn-danger btn-sm" onClick="PpdbKelasDelete(${full.IdPpdbKelas})"><i class="fa fa-trash"></i> Hapus</button>
                    `;
                        return Data;
                    }
                },
                {"data":"Kelas"},
                {
                    "render": function (data, type, full, meta) {
                        var Data = `tidak ada`;
                        if(full.IdJenisPeminatan != 0)
                            if(full.IdJenisPeminatan == 1)
                                Data = 'IPA';
                                if(full.IdJenisPeminatan == 2)
                                    Data = 'IPS';
                        return Data;
                    }
                },
                {"data":"KuotaDaftarP"},
                {"data":"KuotaDaftarL"},
                {"data":"KuotaDaftarPrestasi"},
                {"data":"KuotaTerimaP"},
                {"data":"KuotaTerimaL"},
                {
                    "render": function (data, type, full, meta) {
                        var Data = indoDate(full.MaxTanggalLahir);
                        return Data;
                    }
                },
            ],
            "bDestroy": true
        });
        TabelData.columns().every(function () {
            var that = this;
    
            $('input', this.footer()).on('keyup change clear', function () {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });
        $(".dataTables_filter").css("display", "none");
        $(".dataTables_length").css("display", "none");
    } else {
        swal({
            title: 'Gagal',
            text: res.ReturnMessage,
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
    }
}

const GetPpdbKelasBy = (IdPpdbKelas = null) => {
    console.log(UrlService.GetPpdbKelass)
    if (IdPpdbKelas != null) {
        var res = initAjax(UrlService.GetPpdbKelass);
        if (res) ProgressBar("success");
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.IdPpdbKelas == IdPpdbKelas) {
                    $('#IdUnit').attr('disabled', true);
                    $('#IdKelas').attr('disabled', true);
                    $('#IdJenisPeminatan').attr('disabled', true);
                    
                    $('#IdPpdbKelas').val(v.IdPpdbKelas);

                    ComboGetUnitSklh(function (obj) {
                        $("select[name='IdUnit']").html(obj);
                    }, v.IdUnit);
    
                    ComboGetKelas(function (obj) {
                        $("select[name='IdKelas']").html(obj);
                    }, v.IdUnit, v.IdKelas);

                    $('#IdJenisPeminatan').val(v.IdJenisPeminatan == 0 ? '':v.IdJenisPeminatan);
                    $('#KuotaDaftarP').val(v.KuotaDaftarP);
                    $('#KuotaDaftarL').val(v.KuotaDaftarL);
                    $('#KuotaTerimaP').val(v.KuotaTerimaP);
                    $('#KuotaTerimaL').val(v.KuotaTerimaL);
                    $('#KuotaDaftarPrestasi').val(v.KuotaDaftarPrestasi);
                    $('#MaxTanggalLahir').val(v.MaxTanggalLahir);

                    $('#TitleAddEdit').html('Perbarui Data');
                    $('#ModalAddEdit').modal('show');
                }
            })
        } else {
            swal({
                title: 'Gagal',
                text: res.ReturnMessage,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    } else {
        $('#IdUnit').removeAttr('disabled');
        $('#IdKelas').removeAttr('disabled');
        $('#IdJenisPeminatan').removeAttr('disabled');
        $('#FormEditor')[0].reset();
        $('#TitleAddEdit').html('Tambah Data');
        $('#ModalAddEdit').modal('show');
    }
    
}

const PpdbKelasAddEdit = () => {
    var fd = new FormData($('#FormEditor')[0]);
    var formObj = $('#FormEditor').serializeObject();
    fd.append('IdKelas', $('#IdKelas option:selected').val());
    fd.append('IdJenisPeminatan', $('#IdJenisPeminatan option:selected').val());

    $.ajax({
        "url": formObj.IdPpdbKelas == "" ? UrlService.PpdbKelasAdd : UrlService.PpdbKelasEdit,
        "method": "POST",
        "timeout": 0,
        "processData": false,
        "contentType": false,
        "data": fd,
        "dataType": "JSON",
        "beforeSend": () => {
            ProgressBar("wait");
        },
        "success": (res) => {
            ProgressBar("success");
            if (res.IsSuccess) {
                swal({
                    title: 'Berhasil',
                    text: 'Anda sukses simpan data',
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
                $('#ModalAddEdit').modal('hide');
                GetPpdbKelass();
            } else if (!res.IsSuccess) {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        "error": (err, a, e) => {
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(err),
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}


const PpdbKelasDelete = (IdPpdbKelas) => {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus data ini",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
    function () {
        $.ajax({
            url: UrlService.PpdbKelasDelete + '?IdPpdbKelas='+IdPpdbKelas,
            method: "get",
            dataType: "json",
            headers: {
                "Content-Type": "application/json"
            },
            beforeSend: function (before) {
                ProgressBar("wait");
            },
            success: function (res) {
                ProgressBar("success");
                if (res.IsSuccess == true) {
                    GetPpdbKelass();
                    swal({
                        title: 'Sukses',
                        text: "Data berhasil dihapus",
                        confirmButtonClass: 'btn-success text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'success'
                    });
                } else if (res.IsSuccess == false) {
                    swal({
                        title: 'Gagal',
                        text: res.ReturnMessage,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            },
            error: function (err, a, e) {
                ProgressBar("success");
                swal({
                    title: 'Terjadi Kesalahan',
                    text: JSON.stringify(err) + " : " + e,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        });
    });
}