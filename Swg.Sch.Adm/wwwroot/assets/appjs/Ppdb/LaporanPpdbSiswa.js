$(document).ready((e) => {
  GetPpdbSiswasByAdmin();
});

const UrlService = {
  GetPpdbSiswasByAdmin: `${base_url}/api/ppdbs/GetPpdbSiswasByAdmin`,
  GetPpdbSiswa: `${base_url}/api/ppdbs/GetPpdbSiswa`,
};

const GetPpdbSiswasByAdmin = () => {
  var res = initAjax(UrlService.GetPpdbSiswasByAdmin);
  if (res) ProgressBar("success");
  if (res.IsSuccess) {
    $("#empty-data").hide();
    $("#data").fadeIn();
    $("#TabelData tfoot th").each(function () {
      var title = $(this).text();
      $(this).html(
        '<input type="text" class="form-control" placeholder="Cari ' +
          title +
          '" />'
      );
    });
    var TabelData = $("#TabelData").DataTable({
      paging: true,
      searching: true,
      ordering: true,
      info: true,
      pageLength: 10,
      lengthChange: true,
      scrollX: true,
      processing: true,
      data: res.Data,
      columns: [
        {
          render: function (data, type, full, meta) {
            var Data = `<button type="button" class="btn btn-primary btn-sm" onClick="Preview(${full.IdPpdbDaftar})"><i class="fa fa-user"></i> Lihat</button>`;
            Data += `<button type="button" class="btn btn-info btn-sm ml-2" onClick="GenerateKartuPeserta(${full.IdPpdbDaftar})"><i class="fa fa-file"></i> Generate Kartu Peserta</button>`;
            return Data;
          },
        },
        {
          data: "IdPpdbDaftar",
        },
        {
          data: "Nama",
        },
        {
          render: function (data, type, full, meta) {
            var Data = `<code>${full.Status}</code>`;
            return Data;
          },
        },
        {
          render: function (data, type, full, meta) {
            var Data = "";
            Data = `<span class="badge badge-warning"><i class="fa fa-clock-o"></i> Menunggu Diproses</span>`;
            if (full.StatusProses != "")
              Data = `<span class="badge badge-info"><i class="fa fa-truck"></i>  ${full.StatusProses}</span>`;
            return Data;
          },
        },
        {
          data: "JenisKategoriPendaftaran",
        },
        {
          data: "JenisPendaftaran",
        },
        {
          data: "JalurPendaftaran",
        },
        {
          data: "Unit",
        },
      ],
      bDestroy: true,
    });
    TabelData.columns().every(function () {
      var that = this;

      $("input", this.footer()).on("keyup change clear", function () {
        if (that.search() !== this.value) {
          that.search(this.value).draw();
        }
      });
    });
  } else {
    $("#InfoEmptyData").html(res.ReturnMessage);
    $("#empty-data").show();
    $("#data").hide();
  }
};

const Preview = (IdPpdbDaftar) => {
  var url = base_url + "/api/ppdbs/GetPpdbSiswa?IdPpdbDaftar=" + IdPpdbDaftar;
  var res = initAjax(url);
  if (res) ProgressBar("success");
  if (res.IsSuccess) {
    var data = res.Data;
    $("#ModalCariBerkasPpdb").modal("show");
    $("#id_ppdb_daftar").html(IdPpdbDaftar);
    $("#SXInfoStatus").html(data.Status);
    $("#SXInfoStatusProses").html(
      data.StatusProses == null ? "Menunggu diproses" : data.StatusProses
    );
    $("#SXInfoStatusBiaya").html(data.StatusBiaya);

    $("#DivInfoSodara").hide();
    if (data.NamaSodara != null) {
      $("#DivInfoSodara").fadeIn();
      $("#InfoNamaSodara").html(data.NamaSodara);
    }
    $("#DivInfoAnakPegawai").hide();
    if (data.NamaPegawai != null) {
      $("#DivInfoAnakPegawai").fadeIn();
      $("#InfoNamaPegawai").html(data.NamaPegawai);
    }
    $("#DivInfoCatatanProses").hide();
    if (data.CatatanProses != null) {
      $("#DivInfoCatatanProses").fadeIn();
      $("#InfoCatatanProses").html(data.CatatanProses);
    }
    $("#SXIdPpdbDaftar").val(data.IdPpdbDaftar);
    $("#SXJenisKategoriPendaftaran").val(data.JenisKategoriPendaftaran);
    $("#SXJenisPendaftaran").val(data.JenisPendaftaran);
    $("#SXJalurPendaftaran").val(data.JalurPendaftaran);
    $("#SXUnit").val(data.Unit);
    $("#SXKelas").val(data.Kelas);
    if (data.Unit == "SMAIT AT Taufiq")
      $("#SXKelas").val(data.Kelas + " - " + data.Peminatan);
    $("#SXTanggalLahir").val(data.TanggalLahir);

    $("#SXIdPpdbDaftar").val(data.IdPpdbDaftar);
    ComboGetSeragam(function (obj) {
      $("select[name='SXIdPpdbSeragam']").html(obj);
    }, data.IdPpdbSeragam);
    ComboGetAgama(function (obj) {
      $("select[name='SXIdAgama']").html(obj);
    }, data.IdAgama);
    ComboGetJenisWargaNegara(function (obj) {
      $("select[name='SXIdKewarganegaraan']").html(obj);
    }, data.IdKewarganegaraan);
    ComboGetTinggalSiswa(function (obj) {
      $("select[name='SXIdTinggal']").html(obj);
    }, data.IdTinggal);
    ComboGetJenisTransportasi(function (obj) {
      $("select[name='SXIdTransportasi']").html(obj);
    }, data.IdTransportasi);
    ComboGetJenisBahasa(function (obj) {
      $("select[name='SXIdBahasa']").html(obj);
    }, data.IdBahasa);
    $("#SXNik").val(data.Nik);
    $("#SXNisn").val(data.Nisn);
    $("#SXNama").val(data.Nama);
    $("#SXNamaPanggilan").val(data.NamaPanggilan);
    $("#SXTempatLahir").val(data.TempatLahir);
    $("#SXKdJenisKelamin").val(data.KdJenisKelamin);
    $("#SXKdGolonganDarah").val(data.KdGolonganDarah);
    $("#SXAlamatTinggal").val(data.AlamatTinggal);
    $("#SXEmail").val(data.Email);
    $("#SXNoHandphone").val(data.NoHandphone);
    $("#SXNoDarurat").val(data.NoDarurat);
    $("#SXTinggiBadan").val(data.TinggiBadan);
    $("#SXBeratBadan").val(data.BeratBadan);
    $("#SXAnakKe").val(data.AnakKe);
    $("#SXJumlahSodaraKandung").val(data.JumlahSodaraKandung);
    $("#SXJumlahSodaraTiri").val(data.JumlahSodaraTiri);
    $("#SXJumlahSodaraAngkat").val(data.JumlahSodaraAngkat);
    $("#SXJarakKeSekolah").val(data.JarakKeSekolah);
    $("#SXWaktuTempuh").val(data.WaktuTempuh);
    $("#SXPenyakitDiderita").val(data.PenyakitDiderita);
    $("#SXKelainanJasmani").val(data.KelainanJasmani);
    $("#SXSekolahAsal").val(data.SekolahAsal);
    $("#SXAlamatSekolahAsal").val(data.AlamatSekolahAsal);
    $("#SXNspnSekolahAsal").val(data.NspnSekolahAsal);

    //ortu
    $("#SXDivSudahTiadaIbu").hide();
    if (data.DataOrtu[0].IdStatusHidup == 0) {
      $("#SXDivSudahTiadaIbu").fadeIn();
    }
    $("#SXIdTipeIbu").val(data.DataOrtu[0].IdTipe);
    $("#SXNamaIbu").val(data.DataOrtu[0].Nama);
    $("#SXAlamatIbu").val(data.DataOrtu[0].Alamat);
    $("#SXNikIbu").val(data.DataOrtu[0].Nik);
    $("#SXNamaInstansiIbu").val(data.DataOrtu[0].NamaInstansi);
    $("#SXJabatanIbu").val(data.DataOrtu[0].Jabatan);
    $("#SXEmailIbu").val(data.DataOrtu[0].Email);
    $("#SXNoHandphoneIbu").val(data.DataOrtu[0].NoHandphone);
    $("#SXNoTelpRumahIbu").val(data.DataOrtu[0].NoTelpRumah);
    ComboGetJenisPekerjaan(function (obj) {
      $("#SXIdJenisPekerjaanIbu").html(obj);
    }, data.DataOrtu[0].IdJenisPekerjaan);
    ComboGetAgama(function (obj) {
      $("#SXIdAgamaIbu").html(obj);
    }, data.DataOrtu[0].IdAgama);
    ComboGetJenisWargaNegara(function (obj) {
      $("#SXIdKewarganegaraanIbu").html(obj);
    }, data.DataOrtu[0].IdKewarganegaraan);
    ComboGetListJenjangPendidikan(function (obj) {
      $("#SXIdJenjangPendidikanIbu").html(obj);
    }, data.DataOrtu[0].IdJenjangPendidikan);
    $("#SXIdStatusPenikahanIbu").val(data.DataOrtu[0].IdStatusPenikahan);
    $("#SXTempatLahirIbu").val(data.DataOrtu[0].TempatLahir);
    $("#SXTanggalLahirIbu").val(data.DataOrtu[0].TanggalLahir);
    $("#SXIdStatusHidupIbu").val(data.DataOrtu[0].IdStatusHidup);
    $("#SXTanggalMeninggalIbu").val(data.DataOrtu[0].TanggalMeninggal);

    $("#SXDivSudahTiadaAyah").hide();
    if (data.DataOrtu[1].IdStatusHidup == 0) {
      $("#SXDivSudahTiadaAyah").fadeIn();
    }
    $("#SXIdTipeAyah").val(data.DataOrtu[1].IdTipe);
    $("#SXNamaAyah").val(data.DataOrtu[1].Nama);
    $("#SXAlamatAyah").val(data.DataOrtu[1].Alamat);
    $("#SXNikAyah").val(data.DataOrtu[1].Nik);
    $("#SXNamaInstansiAyah").val(data.DataOrtu[1].NamaInstansi);
    $("#SXJabatanAyah").val(data.DataOrtu[1].Jabatan);
    $("#SXEmailAyah").val(data.DataOrtu[1].Email);
    $("#SXNoHandphoneAyah").val(data.DataOrtu[1].NoHandphone);
    $("#SXNoTelpRumahAyah").val(data.DataOrtu[1].NoTelpRumah);
    ComboGetJenisPekerjaan(function (obj) {
      $("#SXIdJenisPekerjaanAyah").html(obj);
    }, data.DataOrtu[1].IdJenisPekerjaan);
    ComboGetAgama(function (obj) {
      $("#SXIdAgamaAyah").html(obj);
    }, data.DataOrtu[1].IdAgama);
    ComboGetJenisWargaNegara(function (obj) {
      $("#SXIdKewarganegaraanAyah").html(obj);
    }, data.DataOrtu[1].IdKewarganegaraan);
    ComboGetListJenjangPendidikan(function (obj) {
      $("#SXIdJenjangPendidikanAyah").html(obj);
    }, data.DataOrtu[0].IdJenjangPendidikan);
    $("#SXIdStatusPenikahanAyah").val(data.DataOrtu[1].IdStatusPenikahan);
    $("#SXTempatLahirAyah").val(data.DataOrtu[1].TempatLahir);
    $("#SXTanggalLahirAyah").val(data.DataOrtu[1].TanggalLahir);
    $("#SXIdStatusHidupAyah").val(data.DataOrtu[1].IdStatusHidup);
    $("#SXTanggalMeninggalAyah").val(data.DataOrtu[1].TanggalMeninggal);

    //berkas
    $("#SXDivSuratKesehatan").hide();
    $("#SXDivBebasNarkoba").hide();
    if (data.Unit == "SMAIT AT Taufiq") {
      $("#SXDivSuratKesehatan").fadeIn();
      $("#SXDivBebasNarkoba").fadeIn();
    }
    $("#SXImgFileFoto").attr(
      "src",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.Foto
    );
    $("#SXImgFileKtpIbu").attr(
      "src",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileKtpIbu
    );
    $("#SXImgFileKtpAyah").attr(
      "src",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileKtpAyah
    );
    $("#SXImgFileKk").attr(
      "src",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileKartuKeluarga
    );
    $("#SXImgFileAkte").attr(
      "src",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileAkte
    );
    $("#SXImgFileSuratKesehatan").attr(
      "src",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileSuratKesehatan
    );
    $("#SXImgFileBebasNarkoba").attr(
      "src",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileBebasNarkoba
    );

    $("#linkFotoSiswa").attr(
      "href",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.Foto
    );
    $("#linkAkte").attr(
      "href",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileAkte
    );
    $("#linkSuratKesehatan").attr(
      "href",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileSuratKesehatan
    );
    $("#linkBebasNarkoba").attr(
      "href",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileBebasNarkoba
    );
    $("#linkKtpIbu").attr(
      "href",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileKtpIbu
    );
    $("#linkKtpAyah").attr(
      "href",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileKtpAyah
    );
    $("#linkKartuKeluarga").attr(
      "href",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileKartuKeluarga
    );
    $("#linkKartuPeserta").attr(
      "href",
      base_url + "/Asset/Files/Ppdb/Berkas/" + data.FileKartuPeserta
    );

    if (data.FileKartuKeluarga == null) {
      $("#SXImgFileKk").attr("src", base_url + "/assets/img/no-image.png");
      $("#linkKartuKeluarga").attr("href", "#");
    }
    if (data.FileAkte == null) {
      $("#SXImgFileAkte").attr("src", base_url + "/assets/img/no-image.png");
      $("#linkAkte").attr("href", "#");
    }

    //biaya
    $("#DivInfoCicil").hide();
    $("#DivTagCil").hide();
    if (data.TagCicil.length > 0) {
      $("#DivInfoCicil").fadeIn();
      $("#DivTagCil").fadeIn();
    }

    var htmlTagCicil = ``;
    $.each(data.TagCicil, (i, v) => {
      htmlTagCicil += `
            <div class="col-md-6">
                <div class="form-group form-row">
                    <label class="col-form-label col-sm-3" style="text-align:left;">Cicil Ke:</label>
                    <div class="col-sm-9">
                        <input type="text" id="CilKe[]" name="CilKe[]" class="form-control" value="${v.CilKe}" readonly />
                    </div>
                </div>`;
      htmlTagCicil += `
                    <div class="form-group form-row">
                        <label class="col-form-label col-sm-3" style="text-align:left;">Jumlah Cicil:</label>
                        <div class="col-sm-9">
                            <input type="number" id="RpCilKe[]" name="RpCilKe[]" class="form-control" value="${v.RpCilKe}" readonly />
                        </div>
                    </div>`;

      htmlTagCicil += `
                <div class="form-group form-row">
                    <label class="col-form-label col-sm-3" style="text-align:left;">Tanggal Cicil:</label>
                    <div class="col-sm-9">
                        <input type="text" id="TanggalCilKe[]" name="TanggalCilKe[]" class="form-control" value="${indoDate(
                          v.TanggalCilKe
                        )}" readonly />
                    </div>
                </div>
                <div class="form-group form-row">
                    <label class="col-form-label col-sm-3" style="text-align:left;">Status:</label>
                    <div class="col-sm-9">
                        <input type="text" id="Status[]" name="Status[]" class="form-control" value="${
                          v.Status
                        }" readonly />
                    </div>
                </div>
                <div class="form-group form-row">
                    <label class="col-form-label col-sm-3" style="text-align:left;">Tanggal Lunas:</label>
                    <div class="col-sm-9">
                        <input type="text" id="TanggalLunas[]" name="TanggalLunas[]" class="form-control" value="${
                          v.TanggalLunas
                        }" readonly />
                    </div>
                </div>
                <hr />
            </div>
            `;
    });
    $("#DivTagCil").html(htmlTagCicil);

    var htmlTagPpdb = ``;
    $.each(data.TagPpdb, (i, v) => {
      htmlTagPpdb += `
            <div class="col-md-6">
            <div class="form-group form-row">
                <label class="col-form-label col-sm-3" style="text-align:left;">Jenis Biaya:</label>
                <div class="col-sm-9">
                    <input type="hidden" id="IdJenisBiayaPpdb[]" name="IdJenisBiayaPpdb[]" class="form-control" value="${v.IdJenisBiayaPpdb}" />
                    <input type="text" id="JenisBiayaPpdb[]" name="JenisBiayaPpdb[]" class="form-control" value="${v.JenisBiayaPpdb}" readonly />
                </div>
            </div>`;
      htmlTagPpdb += `
            <div class="form-group form-row">
                <label class="col-form-label col-sm-3" style="text-align:left;">Jumlah:</label>
                <div class="col-sm-9">
                    <input type="text" id="Rp[]" name="Rp[]" class="form-control" value="${formatRupiah(
                      v.Rp
                    )}" readonly />
                </div>
            </div>`;
      htmlTagPpdb += `
            <div class="form-group form-row">
                <label class="col-form-label col-sm-3" style="text-align:left;">Catatan:</label>
                <div class="col-sm-9">
                    <textarea type="text" id="CatatanTag[]" name="CatatanTag[]" class="form-control" style="height:100px" placeholder="Ketikan Catatan (opsional)" readonly>${
                      v.Catatan == null ? "" : v.Catatan
                    }</textarea>
                </div>
            </div>
            <hr />

            </div>
            `;
    });
    $("#DivTagPpdb").html(htmlTagPpdb);

    // $('.dataTables_scrollHeadInner, .table-bordered').css('width', 'inherit');
    //prestasi
    $("#DivPrestasi").hide();
    if (data.Prestasi.length > 1) $("#DivPrestasi").fadeIn();
    $("#TabelData tfoot th").each(function () {
      var title = $(this).text();
      $(this).html(
        '<input type="text" class="form-control" placeholder="Cari ' +
          title +
          '" />'
      );
    });
    var TabelData = $("#TabelPrestasi").DataTable({
      paging: true,
      searching: true,
      ordering: true,
      info: true,
      pageLength: 5,
      lengthChange: true,
      scrollX: true,
      processing: true,
      data: data.Prestasi,
      columns: [
        {
          data: "JenisPrestasi",
        },
        {
          data: "JenisTingkatPrestasi",
        },
        {
          data: "NamaPenyelenggara",
        },
        {
          data: "Tahun",
        },
        {
          data: "Keterangan",
        },
        {
          render: function (data, type, full, meta) {
            var Data = `<a href="${base_url}/Asset/Files/Ppdb/Berkas/${full.FileSertifikat}" download>Download</a>`;
            return Data;
          },
        },
      ],
      bDestroy: true,
    });
    TabelData.columns().every(function () {
      var that = this;

      $("input", this.footer()).on("keyup change clear", function () {
        if (that.search() !== this.value) {
          that.search(this.value).draw();
        }
      });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");

    //rapor
    $("#DivRapor").hide();
    if (data.Rapor.length > 1) $("#DivRapor").fadeIn();
    $("#TabelData tfoot th").each(function () {
      var title = $(this).text();
      $(this).html(
        '<input type="text" class="form-control" placeholder="Cari ' +
          title +
          '" />'
      );
    });
    var TabelData = $("#TabelRapor").DataTable({
      paging: true,
      searching: true,
      ordering: true,
      info: true,
      pageLength: 5,
      lengthChange: true,
      scrollX: true,
      processing: true,
      data: data.Rapor,
      columns: [
        {
          render: function (data, type, full, meta) {
            var Data = `Semester - ${full.Semester}`;
            return Data;
          },
        },
        {
          data: "Mapel",
        },
        {
          data: "Nilai",
        },
        {
          render: function (data, type, full, meta) {
            var Data = `<a href="${base_url}/Asset/Files/Ppdb/Berkas/${full.FileRapor}" download>Download</a>`;
            return Data;
          },
        },
      ],
      bDestroy: true,
    });
    TabelData.columns().every(function () {
      var that = this;

      $("input", this.footer()).on("keyup change clear", function () {
        if (that.search() !== this.value) {
          that.search(this.value).draw();
        }
      });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
  } else {
    swal({
      title: "Gagal",
      text: res.ReturnMessage,
      confirmButtonClass: "btn-danger text-white",
      confirmButtonText: "Oke, Mengerti",
      type: "error",
    });
  }
};

const GenerateKartuPeserta = (idPpdbDaftar) => {
  swal(
    {
      title: "Apakah Anda Yakin ?",
      text: "Anda akan mengirim kartu peserta ke email siswa ini",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-success",
      confirmButtonText: "Ya, Saya Yakin!",
      cancelButtonClass: "btn-danger",
      cancelButtonText: "Tidak, Batalkan!",
      closeOnConfirm: false,
    },
    function () {
      var Url =
        base_url +
        "/api/ppdbs/GenerateBaruNameTag?idppdbdaftar=" +
        idPpdbDaftar;
      $.ajax({
        url: Url,
        method: "get",
        dataType: "json",
        headers: {
          "Content-Type": "application/json",
        },
        beforeSend: function (before) {
          ProgressBar("wait");
        },
        success: function (responsesave) {
          ProgressBar("success");
          if (responsesave.IsSuccess == true) {
            $("#TabelPpdbKonten").DataTable().ajax.reload();
            swal({
              title: "Berhasil",
              text: "Anda berhasil mengirim kartu peserta",
              confirmButtonClass: "btn-success text-white",
              confirmButtonText: "Oke, Mengerti",
              type: "success",
            });
          } else if (responsesave.IsSuccess == false) {
            swal({
              title: "Gagal Proses",
              text: responsesave.ReturnMessage,
              confirmButtonClass: "btn-danger text-white",
              confirmButtonText: "Oke, Mengerti",
              type: "error",
            });
          }
        },
        error: function (responserror, a, e) {
          ProgressBar("success");
          swal({
            title: "Error :(",
            text: JSON.stringify(responserror) + " : " + e,
            confirmButtonClass: "btn-danger text-white",
            confirmButtonText: "Oke, Mengerti",
            type: "error",
          });
        },
      });
    }
  );
};
