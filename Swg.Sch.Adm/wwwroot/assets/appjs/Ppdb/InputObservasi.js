$(document).ready((e) => {
  GetPpdbSiswas();
  CreateFormCicils();
  CreateFormCicils();
});

const UrlService = {
  GetPpdbSiswas: `${base_url}/api/ppdbs/GetPpdbSiswas`,
  GetPpdbSiswa: `${base_url}/api/ppdbs/GetPpdbSiswa`,
  InputObservasi: `${base_url}/api/ppdbs/InputObservasi`,
  UploadInputObservasi: `${base_url}/api/ppdbs/UploadInputObservasi`,
};

const GetPpdbSiswas = () => {
  var res = initAjax(`${UrlService.GetPpdbSiswas}?status=3`);
  if (res) ProgressBar("success");
  if (res.IsSuccess) {
    $("#empty-data").hide();
    $("#data").fadeIn();
    $("#TabelData tfoot th").each(function () {
      var title = $(this).text();
      $(this).html(
        '<input type="text" class="form-control" placeholder="Cari ' +
          title +
          '" />'
      );
    });
    var TabelData = $("#TabelData").DataTable({
      paging: true,
      searching: true,
      ordering: true,
      info: true,
      pageLength: 10,
      lengthChange: true,
      scrollX: true,
      processing: true,
      data: res.Data,
      columns: [
        {
          render: function (data, type, full, meta) {
            var Data = `<button type="button" class="btn btn-primary btn-sm" onClick="Preview(${full.IdPpdbDaftar})"><i class="fa fa-rocket"></i> Proses</button>`;
            return Data;
          },
        },

        {
          data: "IdPpdbDaftar",
        },
        {
          data: "Nama",
        },
        {
          data: "JenisKategoriPendaftaran",
        },
        {
          data: "JenisPendaftaran",
        },
        {
          render: function (data, type, full, meta) {
            var Data = "";
            Data = `<span class="badge badge-info"><i class="fa fa-user-plus"></i> ${full.Status}</span>`;
            return Data;
          },
        },
        {
          render: function (data, type, full, meta) {
            var Data = "";
            Data = `<span class="badge badge-warning"><i class="fa fa-times"></i> Tidak</span>`;
            if (full.NamaPegawai != "")
              Data = `<span class="badge badge-success"><i class="fa fa-check"></i> Ya</span>`;
            return Data;
          },
        },

        {
          data: "JalurPendaftaran",
        },
        {
          data: "Unit",
        },
        {
          data: "Kelas",
        },
      ],
      bDestroy: true,
    });
    TabelData.columns().every(function () {
      var that = this;

      $("input", this.footer()).on("keyup change clear", function () {
        if (that.search() !== this.value) {
          that.search(this.value).draw();
        }
      });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
  } else {
    $("#InfoEmptyData").html(res.ReturnMessage);
    $("#empty-data").show();
    $("#data").hide();

    // swal({
    //     title: 'Gagal',
    //     text: res.ReturnMessage,
    //     confirmButtonClass: 'btn-danger text-white',
    //     confirmButtonText: 'Oke, Mengerti',
    //     type: 'error'
    // });
  }
};

const Preview = (IdPpdbDaftar) => {
  var res = initAjax(`${UrlService.GetPpdbSiswa}?IdPpdbDaftar=${IdPpdbDaftar}`);
  if (res) ProgressBar("success");
  if (res.IsSuccess) {
    var data = res.Data;

    if (data.FileLembarKomitmen === null) {
      $("#BtnObservasi").attr("disabled", true);
      $("#DivInfoLembarKomitmen").fadeIn();
    } else {
      $("#DivInfoLembarKomitmen").fadeOut();
      $("#BtnObservasi").attr("disabled", false);
    }

    $("#DivInfoSodara").hide();
    if (data.NamaSodara != null) {
      $("#DivInfoSodara").fadeIn();
      $("#InfoNamaSodara").html(data.NamaSodara);
    }
    $("#DivInfoAnakPegawai").hide();
    if (data.NamaPegawai != null) {
      $("#DivInfoAnakPegawai").fadeIn();
      $("#InfoNamaPegawai").html(data.NamaPegawai);
    }
    $("#DivInfoCatatanProses").hide();
    if (data.CatatanProses != null) {
      $("#DivInfoCatatanProses").fadeIn();
      $("#InfoCatatanProses").html(data.CatatanProses);
    }

    $("#collapseDataSiswa").collapse("hide");
    $("#ModalPreview").modal("show");
    // $('#IdPpdbDaftar').val(data.IdPpdbDaftar);
    $("#JenisKategoriPendaftaran").val(data.JenisKategoriPendaftaran);
    $("#JenisPendaftaran").val(data.JenisPendaftaran);
    $("#JalurPendaftaran").val(data.JalurPendaftaran);
    $("#Unit").val(data.Unit);
    $("#Kelas").val(data.Kelas);
    if (data.Unit == "SMAIT AT Taufiq")
      $("#Kelas").val(data.Kelas + " - " + data.Peminatan);
    $("#TanggalLahir").val(data.TanggalLahir);

    $("#IdPpdbDaftar").val(data.IdPpdbDaftar);
    ComboGetSeragam(function (obj) {
      $("select[name='IdPpdbSeragam']").html(obj);
    }, data.IdPpdbSeragam);
    ComboGetAgama(function (obj) {
      $("select[name='IdAgama']").html(obj);
    }, data.IdAgama);
    ComboGetJenisWargaNegara(function (obj) {
      $("select[name='IdKewarganegaraan']").html(obj);
    }, data.IdKewarganegaraan);
    ComboGetTinggalSiswa(function (obj) {
      $("select[name='IdTinggal']").html(obj);
    }, data.IdTinggal);
    ComboGetJenisTransportasi(function (obj) {
      $("select[name='IdTransportasi']").html(obj);
    }, data.IdTransportasi);
    ComboGetJenisBahasa(function (obj) {
      $("select[name='IdBahasa']").html(obj);
    }, data.IdBahasa);
    $("#Nik").val(data.Nik);
    $("#Nisn").val(data.Nisn);
    $("#Nama").val(data.Nama);
    $("#NamaPanggilan").val(data.NamaPanggilan);
    $("#TempatLahir").val(data.TempatLahir);
    $("#KdJenisKelamin").val(data.KdJenisKelamin);
    $("#KdGolonganDarah").val(data.KdGolonganDarah);
    $("#AlamatTinggal").val(data.AlamatTinggal);
    $("#Email").val(data.Email);
    $("#NoHandphone").val(data.NoHandphone);
    $("#NoDarurat").val(data.NoDarurat);
    $("#TinggiBadan").val(data.TinggiBadan);
    $("#BeratBadan").val(data.BeratBadan);
    $("#AnakKe").val(data.AnakKe);
    $("#JumlahSodaraKandung").val(data.JumlahSodaraKandung);
    $("#JumlahSodaraTiri").val(data.JumlahSodaraTiri);
    $("#JumlahSodaraAngkat").val(data.JumlahSodaraAngkat);
    $("#JarakKeSekolah").val(data.JarakKeSekolah);
    $("#WaktuTempuh").val(data.WaktuTempuh);
    $("#PenyakitDiderita").val(data.PenyakitDiderita);
    $("#KelainanJasmani").val(data.KelainanJasmani);
    $("#SekolahAsal").val(data.SekolahAsal);
    $("#AlamatSekolahAsal").val(data.AlamatSekolahAsal);
    $("#NspnSekolahAsal").val(data.NspnSekolahAsal);

    $("#NamaPeserta").val(data.Nama);
    $("#Tanggal").val("");
    $("#Catatan").val("");

    $("#InfoNamaPeserta").html(data.Nama);

    ComboGetStatusHasilObservasi(function (obj) {
      $("select[name='StatusHasilObservasi']").html(obj);
    });

    var htmlTagPpdb = ``;
    $.each(data.TagPpdb, (i, v) => {
      htmlTagPpdb += `
            <div class="col-md-6">
            <div class="form-group form-row">
                <label class="col-form-label col-sm-3" style="text-align:left;">Jenis Biaya:</label>
                <div class="col-sm-9">
                    <input type="hidden" id="IdJenisBiayaPpdb[]" name="IdJenisBiayaPpdb[]" class="form-control" value="${v.IdJenisBiayaPpdb}" />
                    <input type="text" id="JenisBiayaPpdb[]" name="JenisBiayaPpdb[]" class="form-control" value="${v.JenisBiayaPpdb}" readonly />
                </div>
            </div>`;
      if (v.JenisBiayaPpdb == "WAKAF" || v.JenisBiayaPpdb == "INFAQ") {
        htmlTagPpdb += `
                <div class="form-group form-row">
                    <label class="col-form-label col-sm-3" style="text-align:left;">Jumlah:</label>
                    <div class="col-sm-9">
                        <input type="number" id="Rp[]" name="Rp[]" class="form-control" value="${v.Rp}" />
                    </div>
                </div>`;
      } else {
        htmlTagPpdb += `
                <div class="form-group form-row">
                    <label class="col-form-label col-sm-3" style="text-align:left;">Jumlah:</label>
                    <div class="col-sm-9">
                        <input type="number" id="Rp[]" name="Rp[]" class="form-control" value="${v.Rp}"  />
                    </div>
                </div>`;
      }

      htmlTagPpdb += `
            <div class="form-group form-row">
                <label class="col-form-label col-sm-3" style="text-align:left;">Catatan:</label>
                <div class="col-sm-9">
                    <textarea type="text" id="CatatanTag[]" name="CatatanTag[]" class="form-control" style="height:100px" placeholder="Ketikan Catatan (opsional)"></textarea>
                </div>
            </div>
            </div>
            `;
    });
    // htmlTagPpdb += `</div>`;
    $("#DivTagPpdb").html(htmlTagPpdb);
  } else {
    swal({
      title: "Gagal",
      text: res.ReturnMessage,
      confirmButtonClass: "btn-danger text-white",
      confirmButtonText: "Oke, Mengerti",
      type: "error",
    });
  }
};

const InputObservasi = () => {
  var fd = new FormData($("#FormAbsensi")[0]);

  var formObj = $("#FormAbsensi").serializeObject();
  var IdPpdbDaftar = formObj.IdPpdbDaftar;
  var StatusHasilObservasi = formObj.StatusHasilObservasi;
  var NamaPeserta = formObj.NamaPeserta;
  var Catatan = formObj.Catatan;

  if (StatusHasilObservasi == "") {
    swal({
      title: "Gagal",
      text: "Silahkan pilih salah sau status hasil observasi!",
      confirmButtonClass: "btn-danger text-white",
      confirmButtonText: "Oke, Mengerti",
      type: "error",
    });
    return;
  }

  if ($('input[name="IdJenisBiayaPpdb[0]"]').val() != "") {
    $('input[name^="IdJenisBiayaPpdb[]"]').each(function (i, v) {
      fd.append("Biayas[" + i + "]." + "IdJenisBiayaPpdb", v.value);
    });

    $('input[name^="Rp[]"]').each(function (i, v) {
      fd.append("Biayas[" + i + "]." + "Rp", v.value);
    });

    $('textarea[name^="CatatanTag[]"]').each(function (i, v) {
      fd.append("Biayas[" + i + "]." + "Catatan", v.value);
    });
  }

  if ($('input[name="RpCilKe[]"]').val() != 0) {
    $('input[name^="CilKe[]"]').each(function (i, v) {
      fd.append("Cicils[" + i + "]." + "CilKe", v.value);
    });

    $('input[name^="RpCilKe[]"]').each(function (i, v) {
      fd.append("Cicils[" + i + "]." + "RpCilKe", v.value);
    });

    $('input[name^="TanggalCilKe[]"]').each(function (i, v) {
      fd.append("Cicils[" + i + "]." + "TanggalCilKe", v.value);
    });
  }

  $.ajax({
    url: `${UrlService.InputObservasi}?IdPpdbDaftar=${IdPpdbDaftar}&StatusHasilObservasi=${StatusHasilObservasi}&Catatan=${Catatan}`,
    method: "POST",
    timeout: 0,
    processData: false,
    contentType: false,
    data: fd,
    dataType: "JSON",
    beforeSend: () => {
      ProgressBar("wait");
    },
    success: (res) => {
      ProgressBar("success");
      if (res.IsSuccess) {
        swal({
          title: "Berhasil",
          text:
            "Anda berhasil melakukan input observasi pada nama peserta : " +
            NamaPeserta,
          confirmButtonClass: "btn-success text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "success",
        });
        $("#ModalPreview").modal("hide");
        GetPpdbSiswas();
      } else {
        swal({
          title: "Gagal",
          text: res.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: (e, a, x) => {
      ProgressBar("success");
      swal({
        title: "Terjadi Kesalahan",
        text: JSON.stringify(e),
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
};

var xInputCicils = 1;

const CreateFormCicils = () => {
  var HtmlData = "";
  HtmlData += `
    <div class="col-md-6" id="InputCicilForm_${xInputCicils}">
        <div class="form-group form-row">
            <label class="col-form-label col-sm-3" style="text-align:left;color:white">
            <a href="javascript:void(0);" class="text-white" onClick="DeleteFormCicils(${xInputCicils});"><i class="fa fa-times"></i></a> 
            Cilcil Ke:
            </label>
            <div class="col-sm-9">
                <input type="text" id="CilKe[]" name="CilKe[]" class="form-control" value="${xInputCicils}" readonly />
            </div>
        </div>
        <div class="form-group form-row">
            <label class="col-form-label col-sm-3" style="text-align:left;color:white">Jumlah:</label>
            <div class="col-sm-9">
                <input type="number" id="RpCilKe[]" name="RpCilKe[]" class="form-control" placeholder="Masukan Jumlah" />
            </div>
        </div>
        <div class="form-group form-row">
            <label class="col-form-label col-sm-3" style="text-align:left;color:white">Tanggal:</label>
            <div class="col-sm-9">
                <input type="text" id="TanggalCilKe[]" name="TanggalCilKe[]" class="form-control" data-toggle="datepicker" placeholder="Pilih Tanggal" readonly/>
            </div>
        </div>
        <hr />
    </div>
    `;
  $("#DivFormCicils").append(HtmlData);
  $('[data-toggle="datepicker"]').datepicker({
    format: "dd-mm-yyyy",
    pick: function (e) {
      var date = e.date.getDate();
      jQuery(this).attr("value", date);
    },
  });
  xInputCicils++;
};

const DeleteFormCicils = (IdBtn) => {
  $("#InputCicilForm_" + IdBtn).remove();
  xInputCicils--;
};

const OpenModalUpload = () => {
  $("#ModalUploadMassal").modal("show");
};

const SaveUploadMassal = () => {
  var formObj = $("#FormUploadInputObservasi").serializeObject();
  var formData = new FormData($("#FormUploadInputObservasi")[0]);
  $.ajax({
    url: UrlService.UploadInputObservasi,
    type: "POST",
    data: formData,
    dataType: "json",
    contentType: false,
    cache: true,
    processData: false,
    beforeSend: function (responsebefore) {
      ProgressBar("wait");
    },
    success: function (responseupload) {
      ProgressBar("success");
      if (responseupload.IsSuccess == true) {
        GetPpdbSiswas();
        $("#ModalUploadMassal").modal("hide");
        swal({
          title: "Sukses",
          text: "Anda berhasil melakukan set hadir observasi secara massal",
          confirmButtonClass: "btn-success text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "success",
        });
      } else if (responseupload.IsSuccess == false) {
        swal({
          title: "Gagal",
          text: responseupload.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (jqXHR, textStatus, errorThrown) {
      ProgressBar("success");
      swal({
        title: "Gagal Upload",
        text: JSON.stringify(jqXHR) + " " + textStatus + " " + errorThrown,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
};
