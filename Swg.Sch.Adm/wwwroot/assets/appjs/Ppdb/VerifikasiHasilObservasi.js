$(document).ready((e) => {
    GetPpdbSiswas();
})

const UrlService = {
    GetPpdbSiswas: `${base_url}/api/ppdbs/GetPpdbSiswas`,
    GetPpdbSiswa: `${base_url}/api/ppdbs/GetPpdbSiswa`,
    VerifikasiObservasi: `${base_url}/api/ppdbs/VerifikasiObservasi`,
};

const GetPpdbSiswas = () => {
    var res = initAjax(`${UrlService.GetPpdbSiswas}?status=4`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        $('#empty-data').hide();
        $('#data').fadeIn();     
        $('#TabelData tfoot th').each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
        });
        var TabelData = $('#TabelData').DataTable({
            "paging": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "pageLength": 10,
            "lengthChange": true,
            "scrollX": true,
            "processing": true,
            "data": res.Data,
            "columns": [{
                    "render": function (data, type, full, meta) {
                        var Data = `<button type="button" class="btn btn-primary btn-sm" onClick="Preview(${full.IdPpdbDaftar})"><i class="fa fa-rocket"></i> Proses</button>`;
                        return Data;
                    }
                },
                
                {
                    "data": "IdPpdbDaftar"
                },
                {
                    "data": "Nama"
                },
                {
                    "data": "JenisKategoriPendaftaran"
                },
                {
                    "data": "JenisPendaftaran"
                },
                {
                    "render": function (data, type, full, meta) {
                        var Data = '';
                        Data = `<span class="badge badge-info"><i class="fa fa-user-plus"></i> ${full.Status}</span>`;
                        return Data;
                    }
                },
                {
                    "render": function (data, type, full, meta) {
                        var Data = '';
                        Data = `<span class="badge badge-warning"><i class="fa fa-times"></i> Tidak</span>`;
                        if (full.NamaPegawai != "")
                            Data = `<span class="badge badge-success"><i class="fa fa-check"></i> Ya</span>`;
                        return Data;
                    }
                },
                
                {
                    "data": "JalurPendaftaran"
                },
                {
                    "data": "Unit"
                },
                {
                    "data": "Kelas"
                },
            ],
            "bDestroy": true
        });
        TabelData.columns().every(function () {
            var that = this;
    
            $('input', this.footer()).on('keyup change clear', function () {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });
        $(".dataTables_filter").css("display", "none");
        $(".dataTables_length").css("display", "none");
    } else {
        $('#InfoEmptyData').html(res.ReturnMessage);
        $('#empty-data').show();
        $('#data').hide();

        // swal({
        //     title: 'Gagal',
        //     text: res.ReturnMessage,
        //     confirmButtonClass: 'btn-danger text-white',
        //     confirmButtonText: 'Oke, Mengerti',
        //     type: 'error'
        // });
    }
}

const Preview = (IdPpdbDaftar) => {
    var res = initAjax(`${UrlService.GetPpdbSiswa}?IdPpdbDaftar=${IdPpdbDaftar}`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        var data = res.Data;

        $('#DivInfoSodara').hide();
        if (data.NamaSodara != null) {
            $('#DivInfoSodara').fadeIn();
            $('#InfoNamaSodara').html(data.NamaSodara);
        }
        $('#DivInfoAnakPegawai').hide();
        if (data.NamaPegawai != null) {
            $('#DivInfoAnakPegawai').fadeIn();
            $('#InfoNamaPegawai').html(data.NamaPegawai);
        }
        $('#DivInfoCatatanProses').hide();
        if (data.CatatanProses != "") {
            $('#DivInfoCatatanProses').fadeIn();
            $('#InfoCatatanProses').html(data.CatatanProses);
        }

        $('#collapseDataSiswa').collapse('hide');
        $('#collapseTagPpdb').collapse('hide');
        $('#ModalPreview').modal('show');
        // $('#IdPpdbDaftar').val(data.IdPpdbDaftar);
        $('#JenisKategoriPendaftaran').val(data.JenisKategoriPendaftaran);
        $('#JenisPendaftaran').val(data.JenisPendaftaran);
        $('#JalurPendaftaran').val(data.JalurPendaftaran);
        $('#Unit').val(data.Unit);
        $('#Kelas').val(data.Kelas);
        if (data.Unit == "SMAIT AT Taufiq")
            $('#Kelas').val(data.Kelas + ' - ' + data.Peminatan);
        $('#TanggalLahir').val(data.TanggalLahir);

        $('#IdPpdbDaftar').val(data.IdPpdbDaftar);
        ComboGetSeragam(function (obj) {
            $("select[name='IdPpdbSeragam']").html(obj);
        }, data.IdPpdbSeragam);
        ComboGetAgama(function (obj) {
            $("select[name='IdAgama']").html(obj);
        }, data.IdAgama);
        ComboGetJenisWargaNegara(function (obj) {
            $("select[name='IdKewarganegaraan']").html(obj);
        }, data.IdKewarganegaraan);
        ComboGetTinggalSiswa(function (obj) {
            $("select[name='IdTinggal']").html(obj);
        }, data.IdTinggal);
        ComboGetJenisTransportasi(function (obj) {
            $("select[name='IdTransportasi']").html(obj);
        }, data.IdTransportasi);
        ComboGetJenisBahasa(function (obj) {
            $("select[name='IdBahasa']").html(obj);
        }, data.IdBahasa);
        $('#Nik').val(data.Nik);
        $('#Nisn').val(data.Nisn);
        $('#Nama').val(data.Nama);
        $('#NamaPanggilan').val(data.NamaPanggilan);
        $('#TempatLahir').val(data.TempatLahir);
        $('#KdJenisKelamin').val(data.KdJenisKelamin);
        $('#KdGolonganDarah').val(data.KdGolonganDarah);
        $('#AlamatTinggal').val(data.AlamatTinggal);
        $('#Email').val(data.Email);
        $('#NoHandphone').val(data.NoHandphone);
        $('#NoDarurat').val(data.NoDarurat);
        $('#TinggiBadan').val(data.TinggiBadan);
        $('#BeratBadan').val(data.BeratBadan);
        $('#AnakKe').val(data.AnakKe);
        $('#JumlahSodaraKandung').val(data.JumlahSodaraKandung);
        $('#JumlahSodaraTiri').val(data.JumlahSodaraTiri);
        $('#JumlahSodaraAngkat').val(data.JumlahSodaraAngkat);
        $('#JarakKeSekolah').val(data.JarakKeSekolah);
        $('#WaktuTempuh').val(data.WaktuTempuh);
        $('#PenyakitDiderita').val(data.PenyakitDiderita);
        $('#KelainanJasmani').val(data.KelainanJasmani);
        $('#SekolahAsal').val(data.SekolahAsal);
        $('#AlamatSekolahAsal').val(data.AlamatSekolahAsal);
        $('#NspnSekolahAsal').val(data.NspnSekolahAsal);

        $('#NamaPeserta').val(data.Nama);
        $('#Tanggal').val('');
        $('#Catatan').val('');

        $('#InfoNamaPeserta').html(data.Nama);

        ComboGetStatusVerifikasiObservasi(function (obj) {
            $("select[name='StatusVerifikasiObservasi']").html(obj);
        });


        var htmlTagCicil = ``;
        $.each(data.TagCicil, (i, v) => {
            htmlTagCicil += `
            <div class="col-md-6">
                <div class="form-group form-row">
                    <label class="col-form-label col-sm-3" style="text-align:left;">Cicil Ke:</label>
                    <div class="col-sm-9">
                        <input type="text" id="CilKe[]" name="CilKe[]" class="form-control" value="${v.CilKe}" readonly />
                    </div>
                </div>`;
                htmlTagCicil += `
                    <div class="form-group form-row">
                        <label class="col-form-label col-sm-3" style="text-align:left;">Jumlah Cicil:</label>
                        <div class="col-sm-9">
                            <input type="number" id="RpCilKe[]" name="RpCilKe[]" class="form-control" value="${v.RpCilKe}" readonly />
                        </div>
                    </div>`;
                
                htmlTagCicil += `
                <div class="form-group form-row">
                    <label class="col-form-label col-sm-3" style="text-align:left;">Tanggal Cicil:</label>
                    <div class="col-sm-9">
                        <input type="text" id="TanggalCilKe[]" name="TanggalCilKe[]" class="form-control" value="${indoDate(v.TanggalCilKe)}" readonly />
                    </div>
                </div>
                <div class="form-group form-row">
                    <label class="col-form-label col-sm-3" style="text-align:left;">Status:</label>
                    <div class="col-sm-9">
                        <input type="text" id="Status[]" name="Status[]" class="form-control" value="${v.Status}" readonly />
                    </div>
                </div>
                <div class="form-group form-row">
                    <label class="col-form-label col-sm-3" style="text-align:left;">Tanggal Lunas:</label>
                    <div class="col-sm-9">
                        <input type="text" id="TanggalLunas[]" name="TanggalLunas[]" class="form-control" value="${v.TanggalLunas}" readonly />
                    </div>
                </div>
                <hr />
            </div>
            `;
        });
        $('#DivTagCil').html(htmlTagCicil);
        
        var htmlTagPpdb = ``;
        $.each(data.TagPpdb, (i, v) => {
            htmlTagPpdb += `
            <div class="col-md-6">
                <div class="form-group form-row">
                    <label class="col-form-label col-sm-3" style="text-align:left;">Jenis Biaya:</label>
                    <div class="col-sm-9">
                        <input type="hidden" id="IdJenisBiayaPpdb[]" name="IdJenisBiayaPpdb[]" class="form-control" value="${v.IdJenisBiayaPpdb}" />
                        <input type="text" id="JenisBiayaPpdb[]" name="JenisBiayaPpdb[]" class="form-control" value="${v.JenisBiayaPpdb}" readonly />
                    </div>
                </div>`;
                htmlTagPpdb += `
                    <div class="form-group form-row">
                        <label class="col-form-label col-sm-3" style="text-align:left;">Jumlah:</label>
                        <div class="col-sm-9">
                            <input type="number" id="Rp[]" name="Rp[]" class="form-control" value="${v.Rp}" readonly />
                        </div>
                    </div>`;
                
                htmlTagPpdb += `
                <div class="form-group form-row">
                    <label class="col-form-label col-sm-3" style="text-align:left;">Catatan:</label>
                    <div class="col-sm-9">
                        <textarea type="text" id="CatatanTag[]" name="CatatanTag[]" class="form-control" style="height:100px" placeholder="Ketikan Catatan (opsional)" readonly>${v.Catatan == null ? '' : v.Catatan}</textarea>
                    </div>
                </div>
                <hr />
            </div>
            `;
        });
        $('#DivTagPpdb').html(htmlTagPpdb);
        

        $('#DivStatusProses').removeAttr('class');
        if (data.StatusProses == 'Direkomendasikan') {
            $('#DivStatusProses').addClass('alert alert-success');
            $('#InfoStatusProses').html('<i class="fa fa-check"></i> Peserta Direkomendasikan !');
        } else {
            $('#DivStatusProses').addClass('alert alert-danger');
            $('#InfoStatusProses').html('<i class="fa fa-times"></i>  Maaf, Peserta Tidak Direkomendasikan !');
        }

        $('#DivInfoCicil').hide();
        $('#DivTagCil').hide();
        if ((data.TagCicil).length > 0) {
            $('#DivInfoCicil').fadeIn();
            $('#DivTagCil').fadeIn();
        }
    } else {
        swal({
            title: 'Gagal',
            text: res.ReturnMessage,
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
    }
}

const VerifikasiObservasi = () => {
    var formObj = $('#FormObservasi').serializeObject();
    var IdPpdbDaftar = formObj.IdPpdbDaftar;
    var NamaPeserta = formObj.NamaPeserta;
    var StatusVerfikasiObservasi = formObj.StatusVerifikasiObservasi;
    var Catatan = formObj.Catatan;
    if (StatusVerfikasiObservasi == "") {
        swal({
            title: 'Gagal',
            text: 'Silahkan pilih salah sau status verifikasi hasil observasi!',
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
        return;
    }
    $.ajax({
        url: `${UrlService.VerifikasiObservasi}?IdPpdbDaftar=${IdPpdbDaftar}&StatusVerfikasiObservasi=${StatusVerfikasiObservasi}&Catatan=${Catatan}`,
        type: 'GET',
        dataType: 'JSON',
        beforeSend: () => {
            ProgressBar('wait');
        },
        success: (res) => {
            ProgressBar('success');
            if (res.IsSuccess) {
                swal({
                    title: 'Berhasil',
                    text: 'Anda berhasil melakukan verifikasi observasi pada nama peserta : ' + NamaPeserta,
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
                $('#ModalPreview').modal('hide');
                GetPpdbSiswas();
            } else {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: (e, a, x) => {
            ProgressBar('success');
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(e),
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}