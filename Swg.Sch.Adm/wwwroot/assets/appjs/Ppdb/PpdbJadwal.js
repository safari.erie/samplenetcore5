$(document).ready((e) => {
    GetPpdbJadwals();
})

const UrlService = {
    GetPpdbJadwals: `${base_url}/api/ppdbs/GetPpdbJadwals`,
    PpdbJadwalAdd: `${base_url}/api/ppdbs/PpdbJadwalAdd`,
    PpdbJadwalEdit: `${base_url}/api/ppdbs/PpdbJadwalEdit`,
    PpdbJadwalDelete: `${base_url}/api/ppdbs/PpdbJadwalDelete`,
};


const GetPpdbJadwals = () => {
    var res = initAjax(UrlService.GetPpdbJadwals);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        $('#empty-data').hide();
        $('#data').fadeIn();
        $('#TabelData tfoot th').each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
        });
        var TabelData = $('#TabelData').DataTable({
            "paging": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "pageLength": 10,
            "lengthChange": true,
            "scrollX": true,
            "processing": true,
            "data": res.Data,
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                    var Data = `<button type="button" class="btn btn-primary btn-sm" onClick="GetPpdbJadwalBy(${full.IdUnit},${full.IdJenisGelombangPpdb},${full.IdJenisPendaftaran},${full.IdJalurPendaftaran})"><i class="fa fa-rocket"></i> Edit</button>
                    <button type="button" class="btn btn-danger btn-sm" onClick="PpdbJadwalDelete(${full.IdUnit},${full.IdJenisGelombangPpdb},${full.IdJenisPendaftaran},${full.IdJalurPendaftaran})"><i class="fa fa-trash"></i> Hapus</button>
                    `;
                        return Data;
                    }
                },
                {"data":"Unit"},
                {"data":"JenisGelombangPpdb"},
                {"data":"JenisPendaftaran"},
                {"data":"JalurPendaftaran"},
                {"data":"IdUnit"},
                {"data":"IdJenisGelombangPpdb"},
                {"data":"IdJenisPendaftaran"},
                {"data":"IdJalurPendaftaran"},
                {"data":"TanggalAwalDaftar"},
                {"data":"TanggalAkhirDaftar"},
                {"data":"TanggalAwalBayarDaftar"},
                {"data":"TanggalAkhirBayarDaftar"},
                {"data":"TanggalAwalObservasi"},
                {"data":"TanggalAkhirObservasi"},
                {"data":"TanggalAwalBayarPendidikan"},
                {"data":"TanggalAkhirBayarPendidikan"},
                {"data":"TanggalKonfirmasiCadangan"},
            ],
            "bDestroy": true
        });
        TabelData.columns().every(function () {
            var that = this;
    
            $('input', this.footer()).on('keyup change clear', function () {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });
        $(".dataTables_filter").css("display", "none");
        $(".dataTables_length").css("display", "none");
    } else {
        swal({
            title: 'Gagal',
            text: res.ReturnMessage,
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
    }
}

const GetPpdbJadwalBy = (IdUnit, IdJenisGelombangPpdb, IdJenisPendaftaran, IdJalurPendaftaran) => {
    if (IdUnit != null) {
        var res = initAjax(UrlService.GetPpdbJadwals);
        if (res) ProgressBar("success");
        if (res.IsSuccess) {
            $.each(res.Data, (i, v) => {
                if (v.IdUnit == IdUnit
                    && v.IdUnit == IdUnit
                    && v.IdJenisGelombangPpdb == IdJenisGelombangPpdb
                    && v.IdJenisPendaftaran == IdJenisPendaftaran
                    && v.IdJalurPendaftaran == IdJalurPendaftaran) {
                    $('#IdUnit').attr('readonly', true);
                    $('#IdJenisPendaftaran').attr('readonly', true);
                    $('#IdJalurPendaftaran').attr('readonly', true);
                    $('#IdJenisGelombangPpdb').attr('readonly', true);
                    
                    ComboGetUnitSklh(function (obj) {
                        $("select[name='IdUnit']", "#FormEditor").html(obj);
                    }, v.IdUnit);
                    ComboGetJenisPendaftarans(function (obj) {
                        $("select[name='IdJenisPendaftaran']", "#FormEditor").html(obj);
                    }, v.IdJenisPendaftaran);
                    ComboGetJalurPendaftaran(function (obj) {
                        $("select[name='IdJalurPendaftaran']", "#FormEditor").html(obj);
                    }, v.IdJalurPendaftaran);
                    ComboGetGelombangPpdbs(function (obj) {
                        $("select[name='IdJenisGelombangPpdb']", "#FormEditor").html(obj);
                    }, v.IdJenisGelombangPpdb);
                    $('input[name="TanggalAwalDaftar"]').val(v.TanggalAwalDaftar);
                    $('input[name="TanggalAkhirDaftar"]').val(v.TanggalAkhirDaftar);
                    $('input[name="TanggalAwalBayarDaftar"]').val(v.TanggalAwalBayarDaftar);
                    $('input[name="TanggalAkhirBayarDaftar"]').val(v.TanggalAkhirBayarDaftar);
                    $('input[name="TanggalAwalObservasi"]').val(v.TanggalAwalObservasi);
                    $('input[name="TanggalAkhirObservasi"]').val(v.TanggalAkhirObservasi);
                    $('input[name="TanggalAwalBayarPendidikan"]').val(v.TanggalAwalBayarPendidikan);
                    $('input[name="TanggalAkhirBayarPendidikan"]').val(v.TanggalAkhirBayarPendidikan);
                    $('input[name="TanggalKonfirmasiCadangan"]').val(v.TanggalKonfirmasiCadangan);

                    $('#BtnJadwalAddEdit').removeAttr('onclick');
                    $('#BtnJadwalAddEdit').attr('onclick', 'PpdbJadwalAddEdit("Edit")');

                    $('#TitleAddEdit').html('Perbarui Data');
                    $('#ModalAddEdit').modal('show');
                }
            })
        } else {
            swal({
                title: 'Gagal',
                text: res.ReturnMessage,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    } else {
        ComboGetUnitSklh(function (obj) {
            $("select[name='IdUnit']", "#FormEditor").html(obj);
        });
        ComboGetJenisPendaftarans(function (obj) {
            $("select[name='IdJenisPendaftaran']", "#FormEditor").html(obj);
        });
        ComboGetJalurPendaftaran(function (obj) {
            $("select[name='IdJalurPendaftaran']", "#FormEditor").html(obj);
        });
        ComboGetGelombangPpdbs(function (obj) {
            $("select[name='IdJenisGelombangPpdb']", "#FormEditor").html(obj);
        });

        $('#IdUnit').removeAttr('readonly');
        $('#IdJenisPendaftaran').removeAttr('readonly');
        $('#IdJalurPendaftaran').removeAttr('readonly');
        $('#IdJenisGelombangPpdb').removeAttr('readonly');
        $('#FormEditor')[0].reset();

        $('input[name="TanggalAwalDaftar"]').val('');
        $('input[name="TanggalAkhirDaftar"]').val('');
        $('input[name="TanggalAwalBayarDaftar"]').val('');
        $('input[name="TanggalAkhirBayarDaftar"]').val('');
        $('input[name="TanggalAwalObservasi"]').val('');
        $('input[name="TanggalAkhirObservasi"]').val('');
        $('input[name="TanggalAwalBayarPendidikan"]').val('');
        $('input[name="TanggalAkhirBayarPendidikan"]').val('');
        $('input[name="TanggalKonfirmasiCadangan"]').val('');

        $('#BtnJadwalAddEdit').removeAttr('onclick');
        $('#BtnJadwalAddEdit').attr('onclick', 'PpdbJadwalAddEdit("Add")');
        $('#TitleAddEdit').html('Tambah Data');
        $('#ModalAddEdit').modal('show');
    }
    
}

const PpdbJadwalAddEdit = (Type) => {
    var fd = new FormData($('#FormEditor')[0]);
    var formObj = $('#FormEditor').serializeObject();

    fd.append("IdUnit", formObj.IdUnit);
    fd.append("IdJenisPendaftaran", formObj.IdJenisPendaftaran);
    fd.append("IdJalurPendaftaran", formObj.IdJalurPendaftaran);
    fd.append("IdJenisGelombangPpdb", formObj.IdJenisGelombangPpdb);
    $.ajax({
        "url": Type == "Add" ? UrlService.PpdbJadwalAdd : UrlService.PpdbJadwalEdit,
        "method": "POST",
        "timeout": 0,
        "processData": false,
        "contentType": false,
        "data": fd,
        "dataType": "JSON",
        "beforeSend": () => {
            ProgressBar("wait");
        },
        "success": (res) => {
            ProgressBar("success");
            if (res.IsSuccess) {
                swal({
                    title: 'Berhasil',
                    text: 'Anda sukses simpan data',
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
                GetPpdbJadwals();
                $('#ModalAddEdit').modal('hide');
            } else if (!res.IsSuccess) {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        "error": (err, a, e) => {
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(err),
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

const PpdbJadwalDelete = (IdUnit, IdJenisGelombangPpdb, IdJenisPendaftaran, IdJalurPendaftaran) => {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda akan menghapus data ini",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Ya, Saya Yakin!",
        cancelButtonClass: "btn-danger",
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false
    },
    function () {
        $.ajax({
            url: `${UrlService.PpdbJadwalDelete}?IdUnit=${IdUnit}&IdJenisGelombangPpdb=${IdJenisGelombangPpdb}&IdJenisPendaftaran=${IdJenisPendaftaran}&IdJalurPendaftaran=${IdJalurPendaftaran}`,
            method: "get",
            dataType: "json",
            headers: {
                "Content-Type": "application/json"
            },
            beforeSend: function (before) {
                ProgressBar("wait");
            },
            success: function (res) {
                ProgressBar("success");
                if (res.IsSuccess == true) {
                    GetPpdbJadwals();
                    swal({
                        title: 'Sukses',
                        text: "Data berhasil dihapus",
                        confirmButtonClass: 'btn-success text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'success'
                    });
                } else if (res.IsSuccess == false) {
                    swal({
                        title: 'Gagal',
                        text: res.ReturnMessage,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            },
            error: function (err, a, e) {
                ProgressBar("success");
                swal({
                    title: 'Terjadi Kesalahan',
                    text: JSON.stringify(err) + " : " + e,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        });
    });
}