$(document).ready(function () {
    DataList();
})

const UrlService = {
    GetListVa: base_url + '/api/ppdbs/GetListVa',
    UploadVA: base_url + '/api/ppdbs/UploadVA',
    DownloadPpdbVa: base_url + '/api/download/DownloadPpdbVa',
}

function DataList() {
    $('#TabelData tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelData = $('#TabelData').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": UrlService.GetListVa,
            "method": 'GET',
            "beforeSend": function (xhr) { },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    $('div#empty-data').fadeIn();
                    $('div#loading-data').hide();
                    return json;
                } else {
                    $('div#no-data').hide();
                    $('div#data').fadeIn();
                    return json.Data;
                }
            }
        },
        "columns": [
            {
                "data": "IdPpdbVa"
            },
            {
                "data": "Unit"
            },
            {
                "data": "Status"
            }
        ],
        "bDestroy": true
    });
    TabelData.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function ModalUpload() {
    $('#ModalUpload').modal('show');
}

function UploadVA() {
    var form = new FormData($('#FormUpload')[0]);
    $.ajax({
        url: UrlService.UploadVA,
        method: "POST",
        processData: false,
        mimeType: "multipart/form-data",
        contentType: false,
        data: form,
        dataType: "JSON",
        beforeSend: function (xhr) {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            $("#TabelData").DataTable().ajax.reload();
            $('#ModalUpload').modal('hide');
            if (res.IsSuccess) {
                swal({
                    title: 'Sukses',
                    text: 'Anda Berhasil Upload VA',
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });


            } else {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Error :(',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

function DownloadVa() {
    window.open(UrlService.DownloadPpdbVa, '_blank');
}