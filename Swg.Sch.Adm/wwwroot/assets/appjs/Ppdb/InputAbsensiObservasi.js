$(document).ready((e) => {
    GetPpdbSiswas();
})

const UrlService = {
    GetPpdbSiswas: `${base_url}/api/ppdbs/GetPpdbSiswas`,
    GetPpdbSiswa: `${base_url}/api/ppdbs/GetPpdbSiswa`,
    SetHadirTestObservasi: `${base_url}/api/ppdbs/SetHadirTestObservasi`,
};

const GetPpdbSiswas = () => {
    var res = initAjax(`${UrlService.GetPpdbSiswas}?status=2`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        $('#empty-data').hide();
        $('#data').fadeIn();
        $('#TabelData tfoot th').each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
        });
        var TabelData = $('#TabelData').DataTable({
            "paging": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "pageLength": 10,
            "lengthChange": true,
            "scrollX": true,
            "processing": true,
            "data": res.Data,
            "columns": [{
                    "render": function (data, type, full, meta) {
                        var Data = `<button type="button" class="btn btn-primary btn-sm" onClick="Preview(${full.IdPpdbDaftar})"><i class="fa fa-rocket"></i> Proses</button>`;
                        return Data;
                    }
                },
                
                {
                    "data": "IdPpdbDaftar"
                },
                {
                    "data": "Nama"
                },
                {
                    "data": "JenisKategoriPendaftaran"
                },
                {
                    "data": "JenisPendaftaran"
                },
                {
                    "render": function (data, type, full, meta) {
                        var Data = '';
                        Data = `<span class="badge badge-info"><i class="fa fa-user-plus"></i> ${full.Status}</span>`;
                        return Data;
                    }
                },
                {
                    "render": function (data, type, full, meta) {
                        var Data = '';
                        Data = `<span class="badge badge-warning"><i class="fa fa-times"></i> Tidak</span>`;
                        if (full.NamaPegawai != "")
                            Data = `<span class="badge badge-success"><i class="fa fa-check"></i> Ya</span>`;
                        return Data;
                    }
                },
                
                {
                    "data": "JalurPendaftaran"
                },
                {
                    "data": "Unit"
                },
                {
                    "data": "Kelas"
                },
            ],
            "bDestroy": true
        });
        TabelData.columns().every(function () {
            var that = this;
    
            $('input', this.footer()).on('keyup change clear', function () {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });
        $(".dataTables_filter").css("display", "none");
        $(".dataTables_length").css("display", "none");
    } else {
        $('#InfoEmptyData').html(res.ReturnMessage);
        $('#empty-data').show();
        $('#data').hide();

        // swal({
        //     title: 'Gagal',
        //     text: res.ReturnMessage,
        //     confirmButtonClass: 'btn-danger text-white',
        //     confirmButtonText: 'Oke, Mengerti',
        //     type: 'error'
        // });
    }
}

const Preview = (IdPpdbDaftar) => {
    var res = initAjax(`${UrlService.GetPpdbSiswa}?IdPpdbDaftar=${IdPpdbDaftar}`);
    if (res) ProgressBar("success");
    if (res.IsSuccess) {
        var data = res.Data;
        
        $('#DivInfoSodara').hide();
        if (data.NamaSodara != null) {
            $('#DivInfoSodara').fadeIn();
            $('#InfoNamaSodara').html(data.NamaSodara);
        }
        $('#DivInfoAnakPegawai').hide();
        if (data.NamaPegawai != null) {
            $('#DivInfoAnakPegawai').fadeIn();
            $('#InfoNamaPegawai').html(data.NamaPegawai);
        }

        $('#collapseDataSiswa').collapse('hide');
        $('#ModalPreview').modal('show');
        // $('#IdPpdbDaftar').val(data.IdPpdbDaftar);
        $('#JenisKategoriPendaftaran').val(data.JenisKategoriPendaftaran);
        $('#JenisPendaftaran').val(data.JenisPendaftaran);
        $('#JalurPendaftaran').val(data.JalurPendaftaran);
        $('#Unit').val(data.Unit);
        $('#Kelas').val(data.Kelas);
        if (data.Unit == "SMAIT AT Taufiq")
            $('#Kelas').val(data.Kelas + ' - ' + data.Peminatan);
        $('#Nama').val(data.Nama);
        $('#KdJenisKelamin').val(data.KdJenisKelamin);
        $('#TempatLahir').val(data.TempatLahir);
        $('#TanggalLahir').val(data.TanggalLahir);
        $('#Email').val(data.Email);
        $('#NoHandphone').val(data.NoHandphone);

        // if (data.Pin != null) {
        //     $('#DivInfoLunas').show();
        //     $('#DivInputAbsensi').hide();
        //     $('#BtnPelunasan').hide();
        // } else {
        //     $('#DivInfoLunas').hide();
        //     $('#DivInputAbsensi').show();
        //     $('#BtnPelunasan').show();
        // }

        $('#IdPpdbDaftar').val(data.IdPpdbDaftar);
        ComboGetSeragam(function (obj) {
            $("select[name='IdPpdbSeragam']").html(obj);
        }, data.IdPpdbSeragam);
        ComboGetAgama(function (obj) {
            $("select[name='IdAgama']").html(obj);
        }, data.IdAgama);
        ComboGetJenisWargaNegara(function (obj) {
            $("select[name='IdKewarganegaraan']").html(obj);
        }, data.IdKewarganegaraan);
        ComboGetTinggalSiswa(function (obj) {
            $("select[name='IdTinggal']").html(obj);
        }, data.IdTinggal);
        ComboGetJenisTransportasi(function (obj) {
            $("select[name='IdTransportasi']").html(obj);
        }, data.IdTransportasi);
        ComboGetJenisBahasa(function (obj) {
            $("select[name='IdBahasa']").html(obj);
        }, data.IdBahasa);
        $('#Nik').val(data.Nik);
        $('#Nisn').val(data.Nisn);
        $('#Nama').val(data.Nama);
        $('#NamaPanggilan').val(data.NamaPanggilan);
        $('#TempatLahir').val(data.TempatLahir);
        $('#KdJenisKelamin').val(data.KdJenisKelamin);
        $('#KdGolonganDarah').val(data.KdGolonganDarah);
        $('#AlamatTinggal').val(data.AlamatTinggal);
        $('#Email').val(data.Email);
        $('#NoHandphone').val(data.NoHandphone);
        $('#NoDarurat').val(data.NoDarurat);
        $('#TinggiBadan').val(data.TinggiBadan);
        $('#BeratBadan').val(data.BeratBadan);
        $('#AnakKe').val(data.AnakKe);
        $('#JumlahSodaraKandung').val(data.JumlahSodaraKandung);
        $('#JumlahSodaraTiri').val(data.JumlahSodaraTiri);
        $('#JumlahSodaraAngkat').val(data.JumlahSodaraAngkat);
        $('#JarakKeSekolah').val(data.JarakKeSekolah);
        $('#WaktuTempuh').val(data.WaktuTempuh);
        $('#PenyakitDiderita').val(data.PenyakitDiderita);
        $('#KelainanJasmani').val(data.KelainanJasmani);
        $('#SekolahAsal').val(data.SekolahAsal);
        $('#AlamatSekolahAsal').val(data.AlamatSekolahAsal);
        $('#NspnSekolahAsal').val(data.NspnSekolahAsal);

        $('#NamaPeserta').val(data.Nama);
        $('#Tanggal').val('');
        $('#Catatan').val('');

        $('#InfoNamaPeserta').html(data.Nama);

    } else {
        swal({
            title: 'Gagal',
            text: res.ReturnMessage,
            confirmButtonClass: 'btn-danger text-white',
            confirmButtonText: 'Oke, Mengerti',
            type: 'error'
        });
    }
}

const SetHadirTestObservasi = () => {
    var formObj = $('#FormAbsensi').serializeObject();
    var IdPpdbDaftar = formObj.IdPpdbDaftar;
    var NamaPeserta = formObj.NamaPeserta;
    var Catatan = formObj.Catatan;
    $.ajax({
        url: `${UrlService.SetHadirTestObservasi}?IdPpdbDaftar=${IdPpdbDaftar}&Catatan=${Catatan}`,
        type: 'GET',
        dataType: 'JSON',
        beforeSend: () => {
            ProgressBar('wait');
        },
        success: (res) => {
            ProgressBar('success');
            if (res.IsSuccess) {
                swal({
                    title: 'Berhasil',
                    text: 'Anda berhasil melakukan absensi pada nama peserta : ' + NamaPeserta,
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
                $('#ModalPreview').modal('hide');
                GetPpdbSiswas();
            } else {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: (e, a, x) => {
            ProgressBar('success');
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(e),
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}