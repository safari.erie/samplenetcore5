$(document).ready(function () {
    DataList();
    ComboGetUnitSklh(function (obj) {
        $("select[name='IdUnit']").html(obj);
    });
})

const UrlService = {
    GetPpdbSeragams: base_url + '/api/ppdbs/GetPpdbSeragams',
    GetPpdbSeragam: base_url + '/api/ppdbs/GetPpdbSeragam',
    PpdbSeragamAdd: base_url + '/api/ppdbs/PpdbSeragamAdd',
    PpdbSeragamEdit: base_url + '/api/ppdbs/PpdbSeragamEdit',
    PpdbSeragamDelete: base_url + '/api/ppdbs/PpdbSeragamDelete',
}

function DataList() {
    $('#TabelData tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    });
    var TabelData = $('#TabelData').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "pageLength": 10,
        "lengthChange": true,
        "scrollX": true,
        "processing": true,
        "ajax": {
            "url": UrlService.GetPpdbSeragams,
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    iziToast.error({
                        title: 'Gagal Menampilkan Data Tabel Seragam',
                        message: json.ReturnMessage,
                        position: 'topRight'
                    });
                    return json;
                } else {
                    return json.Data;
                }
            }
        },
        "columns": [{
                "render": function (data, type, full, meta) {
                    var Data = "";
                    var ParamEdit = `DataEditor(${full.IdPpdbSeragam})`;
                    var ParamHapus = "DataHapus('" + full.IdPpdbSeragam + "')";
                    Data += '<button type="button" class="btn btn-primary btn-sm" onClick="' + ParamEdit + '"><i class="fa fa-pencil-square-o"></i> Edit</button>';
                    Data += '&nbsp;&nbsp;&nbsp;';
                    Data += '<button type="button" class="btn btn-danger btn-sm" onClick="' + ParamHapus + '"><i class="fa fa-trash"></i> Hapus</button>';
                    return Data;
                }
            },
            {
                "data": "Unit"
            },
            {
                "data": "NamaSingkat"
            },
            {
                "data": "Nama"
            },
        ],
        "bDestroy": true
    });
    TabelData.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");
}

function DataEditor(Id) {
    $.getJSON(UrlService.GetPpdbSeragam + '?IdPpdbSeragam=' + Id, (res) => {
        if (res.IsSuccess) {
            ComboGetUnitSklh(function (obj) {
                $("select[name='IdUnit']").html(obj);
            }, res.Data.IdUnit);
            $('#IdPpdbSeragam').val(res.Data.IdPpdbSeragam);
            $('#Nama').val(res.Data.Nama);
            $('#NamaSingkat').val(res.Data.NamaSingkat);
        } else {
            swal({
                title: 'Gagal',
                text: res.ReturnMessage,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

function DataSave() {
    var formObj = $('#FormEditor').serializeObject();
    var fd = new FormData($('#FormEditor')[0]);

    var Url = '';
    if (formObj.IdPpdbSeragam == "") {
        Url = UrlService.PpdbSeragamAdd;
    } else {
        Url = UrlService.PpdbSeragamEdit;
    }
    $.ajax({
        url: Url,
        data: fd,
        timeout: 0,
        processData: false,
        contentType: false,
        type: 'POST',
        dataType: 'JSON',
        beforeSend: (xhr) => {
            ProgressBar("wait");
        },
        success: (res) => {
            ProgressBar("success");
            if (res.IsSuccess) {
                $('#TabelData').DataTable().ajax.reload();
                swal({
                    title: 'Berhasil',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
            } else {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

function DataHapus(Id) {
    swal({
            title: "Apakah Anda Yakin ?",
            text: "Anda akan menghapus data ini",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                url: UrlService.PpdbSeragamDelete + '?IdPpdbSeragam=' + Id,
                method: "get",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (before) {
                    ProgressBar("wait");
                },
                success: function (responsesave) {
                    ProgressBar("success");
                    if (responsesave.IsSuccess == true) {
                        $('#TabelData').DataTable().ajax.reload();
                        swal({
                            title: 'Sukses',
                            text: "Data berhasil dihapus",
                            confirmButtonClass: 'btn-success text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'success'
                        });
                    } else if (responsesave.IsSuccess == false) {
                        swal({
                            title: 'Gagal',
                            text: responsesave.ReturnMessage,
                            confirmButtonClass: 'btn-danger text-white',
                            confirmButtonText: 'Oke, Mengerti',
                            type: 'error'
                        });
                    }
                },
                error: function (responserror, a, e) {
                    ProgressBar("success");
                    swal({
                        title: 'Terjadi Kesalahan',
                        text: JSON.stringify(responserror) + " : " + e,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                }
            });
        });
}

function ResetForm() {
    $('#IdPpdbSeragam').val("");
    $('#Nama').val("");
    $('#NamaSingkat').val("");
    $('#IdUnit').val("");
}