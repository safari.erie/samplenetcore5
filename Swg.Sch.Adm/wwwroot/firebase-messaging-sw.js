﻿importScripts('https://www.gstatic.com/firebasejs/7.14.6/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.14.6/firebase-messaging.js');

const firebaseConfig = {
  apiKey: "AIzaSyCIEAav_s8lUwbPXgzAHupPXScfMdF_MzA",
  authDomain: "swg-attaufiq.firebaseapp.com",
  projectId: "swg-attaufiq",
  storageBucket: "swg-attaufiq.appspot.com",
  messagingSenderId: "1006170673539",
  appId: "1:1006170673539:web:9cdc0ba00a4f6ffc2ba6b6"
};

firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
    console.log(payload);
    const notification=JSON.parse(payload);
    const notificationOption={
        body:notification.body,
        icon:notification.icon
    };
    return self.registration.showNotification(payload.notification.title,notificationOption);
});