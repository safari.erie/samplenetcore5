﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Adm.Models;

namespace Swg.Sch.Adm.Controllers
{
    public class ELearningController : BaseController
    {
        public IActionResult UpdateSoalMateri()
        {
            var jss = ModuleJs("UpdateSoalMateri");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult DaftarJawabanSiswa()
        {
            var jss = ModuleJs("DaftarJawabanSiswa");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult JadwalKbm()
        {
            var jss = ModuleJs("JadwalKBM");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult ProgressQiroaty()
        {
            var jss = ModuleJs("ProgressQiroaty");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult KelompokQiroaty()
        {
            var jss = ModuleJs("KelompokQiroaty");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult JenisEvaluasiHarianSiswa()
        {
            var jss = ModuleJs("JenisEvaluasiHarianSiswa");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult JenisEvaluasiHarianPegawai()
        {
            var jss = ModuleJs("JenisEvaluasiHarianPegawai");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult Mapel()
        {
            var jss = ModuleJs("Mapel");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult HariLibur()
        {
            var jss = ModuleJs("HariLibur");
            ViewBag.ModuleJs = jss;
            return View();
        }

        public IActionResult UploadRapor()
        {
            var jss = ModuleJs("UploadRapor");
            ViewBag.ModuleJs = jss;
            return View();
        }

        public IActionResult ApprovalRapor()
        {
            var jss = ModuleJs("ApprovalRapor");
            ViewBag.ModuleJs = jss;
            return View();
        }

        public List<LibraryJsModel> ModuleJs(string Module)
        {
            List<LibraryJsModel> js = new List<LibraryJsModel>();
            LibraryJsModel updatesoalmateri_js = new LibraryJsModel
            {
                NameJs = "UpdateSoalMateri.js",
                TypeJs = "module",
                Path = "Elearning"
            };
            LibraryJsModel daftarjawabansiswa_js = new LibraryJsModel
            {
                NameJs = "DaftarJawabanSiswa.js",
                TypeJs = "module",
                Path = "Elearning"
            };
            LibraryJsModel jadwalkbm_js = new LibraryJsModel
            {
                NameJs = "JadwalKBM.js",
                TypeJs = "module",
                Path = "Elearning"
            };
            LibraryJsModel progress_qiroaty_js = new LibraryJsModel
            {
                NameJs = "ProgressQiroaty.js",
                TypeJs = "module",
                Path = "Elearning"
            };
            LibraryJsModel kelompok_qiroaty_js = new LibraryJsModel
            {
                NameJs = "KelompokQiroaty.js",
                TypeJs = "module",
                Path = "Elearning"
            };
            LibraryJsModel jenis_evaluasi_harian_siswa_js = new LibraryJsModel
            {
                NameJs = "JenisEvaluasiHarianSiswa.js",
                TypeJs = "module",
                Path = "Elearning"
            };
            LibraryJsModel jenis_evaluasi_harian_pegawai_js = new LibraryJsModel
            {
                NameJs = "JenisEvaluasiHarianPegawai.js",
                TypeJs = "module",
                Path = "Elearning"
            };
            LibraryJsModel mapel_js = new LibraryJsModel
            {
                NameJs = "Mapel.js",
                TypeJs = "module",
                Path = "Elearning"
            };
            LibraryJsModel hari_libur_js = new LibraryJsModel
            {
                NameJs = "HariLibur.js",
                TypeJs = "module",
                Path = "Elearning"
            };
            LibraryJsModel UploadRapor = new LibraryJsModel
            {
                NameJs = "UploadRapor.js",
                TypeJs = "module",
                Path = "Elearning"
            };
            LibraryJsModel ApprovalRapor = new LibraryJsModel
            {
                NameJs = "ApprovalRapor.js",
                TypeJs = "module",
                Path = "Elearning"
            };


            if (Module == "UpdateSoalMateri")
            {
                js.Add(updatesoalmateri_js);
            }
            if (Module == "DaftarJawabanSiswa")
            {
                js.Add(daftarjawabansiswa_js);
            }
            if (Module == "JadwalKBM")
            {
                js.Add(jadwalkbm_js);
            }
            if (Module == "ProgressQiroaty")
            {
                js.Add(progress_qiroaty_js);
            }
            if (Module == "KelompokQiroaty")
            {
                js.Add(kelompok_qiroaty_js);
            }
            if (Module == "JenisEvaluasiHarianSiswa")
            {
                js.Add(jenis_evaluasi_harian_siswa_js);
            }
            if (Module == "JenisEvaluasiHarianPegawai")
            {
                js.Add(jenis_evaluasi_harian_pegawai_js);
            }
            if (Module == "Mapel")
            {
                js.Add(mapel_js);
            }
            if (Module == "HariLibur")
            {
                js.Add(hari_libur_js);
            }
            if (Module == "UploadRapor")
            {
                js.Add(UploadRapor);
            }
            if (Module == "ApprovalRapor")
            {
                js.Add(ApprovalRapor);
            }
            return js;
        }
    }
}
