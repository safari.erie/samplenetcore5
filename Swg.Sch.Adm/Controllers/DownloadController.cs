﻿using System;
using System.Collections.Generic;
using System.IO;
using Ionic.Zip;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Models;
using Swg.Sch.Adm.Tools;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Swg.Sch.Adm.Controllers
{
    [Route("api/[controller]")]
    public class DownloadController : Controller
    {
        IELearningService eLearningService;
        ISchService schService;
        IPegawaiService pegawaiService;
        IPpdbService ppdbService;
        IELearningLaporanService eLearningLaporanService;

        private readonly int IdUser;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        public DownloadController(IHttpContextAccessor httpContextAccessor,
            IELearningService ELearningService,
            ISchService SchService,
            IPegawaiService PegawaiService,
            IELearningLaporanService ELearningLaporanService,
            IPpdbService PpdbService)
        {
            eLearningService = ELearningService;
            schService = SchService;
            pegawaiService = PegawaiService;
            ppdbService = PpdbService;
            eLearningLaporanService = ELearningLaporanService;
            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }
        }
        [HttpGet("DownloadJenisJilidQuran", Name = "DownloadJenisJilidQuran")]
        public ActionResult DownloadJenisJilidQuran()
        {
            try
            {
                var tahunPelajaran = schService.GetTahunAjaran(out string oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    tahunPelajaran = new TahunAjaranModel { Nama = "TP" };
                var data = schService.GetJenisJilidQurans(out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateJenisJilidQuran(IdUser, tahunPelajaran.Nama, data, out oMessage);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("jilid_quran_{0}.xlsx", tahunPelajaran.Nama);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }
        [HttpGet("DownloadPegawai", Name = "DownloadPegawai")]
        public ActionResult DownloadPegawai()
        {
            try
            {
                var tahunPelajaran = schService.GetTahunAjaran(out string oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    tahunPelajaran = new TahunAjaranModel { Nama = "TP" };
                var data = pegawaiService.GetListPegawai(out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GeneratePegawai(IdUser, tahunPelajaran.Nama, data, out oMessage);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("daftar_pegawai_{0}.xlsx", tahunPelajaran.Nama);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }
        [HttpGet("DownloadKelas", Name = "DownloadKelas")]
        public ActionResult DownloadKelas()
        {
            try
            {
                var tahunPelajaran = schService.GetTahunAjaran(out string oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    tahunPelajaran = new TahunAjaranModel { Nama = "TP" };
                var data = schService.GetKelass(out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateDaftarKelas(IdUser, tahunPelajaran.Nama, data, out oMessage);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("daftar_kelas_{0}.xlsx", tahunPelajaran.Nama);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }
        [HttpGet("DownloadMapel", Name = "DownloadMapel")]
        public ActionResult DownloadMapel()
        {
            try
            {
                var tahunPelajaran = schService.GetTahunAjaran(out string oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    tahunPelajaran = new TahunAjaranModel { Nama = "TP" };
                var data = schService.GetMapels(out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateDaftarMapel(IdUser, tahunPelajaran.Nama, data, out oMessage);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("daftar_mapel_{0}.xlsx", tahunPelajaran.Nama);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }

        [HttpGet("DownloadKelasQuran", Name = "DownloadKelasQuran")]
        public ActionResult DownloadKelasQuran()
        {
            try
            {
                var tahunPelajaran = schService.GetTahunAjaran(out string oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    tahunPelajaran = new TahunAjaranModel { Nama = "TP" };
                var data = schService.GetKelQurans(out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateKelQuran(IdUser, tahunPelajaran.Nama, data, out oMessage);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("kelas_quran_{0}.xlsx", tahunPelajaran.Nama);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }
        [HttpGet("DownloadJadwalQuran", Name = "DownloadJadwalQuran")]
        public ActionResult DownloadJadwalQuran()
        {
            try
            {
                var tahunPelajaran = schService.GetTahunAjaran(out string oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    tahunPelajaran = new TahunAjaranModel { Nama = "TP" };
                var data = schService.GetJadwalQurans(out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateJadwalQuran(IdUser, tahunPelajaran.Nama, data, out oMessage);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("jadwal_quran_{0}.xlsx", tahunPelajaran.Nama);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }
        [HttpGet("DownloadKelasQuranSiswa", Name = "DownloadKelasQuranSiswa")]
        public ActionResult DownloadKelasQuranSiswa()
        {
            try
            {
                var tahunPelajaran = schService.GetTahunAjaran(out string oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    tahunPelajaran = new TahunAjaranModel { Nama = "TP" };
                var data = schService.GetKelQuranSiswas(out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateKelQuranSiswa(IdUser, tahunPelajaran.Nama, data, out oMessage);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("kelas_quran_siswa_{0}.xlsx", tahunPelajaran.Nama);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }

        [HttpGet("DownloadJenisEvaluasiHarian", Name = "DownloadJenisEvaluasiHarian")]
        public ActionResult DownloadJenisEvaluasiHarian()
        {
            try
            {
                var tahunPelajaran = schService.GetTahunAjaran(out string oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    tahunPelajaran = new TahunAjaranModel { Nama = "TP" };
                var data = schService.GetJenisEvaluasiHarians(out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateJenisEvaluasiHarian(IdUser, tahunPelajaran.Nama, data, out oMessage);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("daftar_evaluasi_harian_{0}.xlsx", tahunPelajaran.Nama);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }

        [HttpGet("DownloadGajiPegawai", Name = "DownloadGajiPegawai")]
        public ActionResult DownloadGajiPegawai(int IdUser, int Tahun, int Bulan)
        {
            try
            {
                string BulanTahun = Bulan.ToString().PadLeft(2, '0') + "-" + Tahun.ToString();
                var data = pegawaiService.GetListPegawai(out string oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateTemplateGajiPegawai(IdUser, BulanTahun, data, out oMessage);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("gaji_pegawai_{0}.xlsx", BulanTahun);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }

        [HttpGet("DownloadNilaiSiswa", Name = "DownloadNilaiSiswa")]
        public ActionResult DownloadNilaiSiswa(int IdKbmMateri)
        {
            try
            {
                var data = eLearningService.GetKbmTugasSiswas(IdKbmMateri, out string oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateTemplateNilaiSiswa(IdUser, data, out oMessage);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("nilai_siswa_{0}.xlsx", IdKbmMateri);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }

        [HttpGet("DownloadJadwalKbm", Name = "DownloadJadwalKbm")]
        public ActionResult DownloadJadwalKbm(int IdKelompok)
        {
            try
            {
                List<SheetJadwalKbmModel> ret = eLearningService.GetKbms(IdUser, IdKelompok, out string oMessage);
                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateTemplateJadwal(IdUser, ret, out string oMessagex);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = "jadwal_kbm.xlsx";
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                Console.WriteLine("DownloadJadwalKbm -> " + ex);
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }

        [HttpGet("DownloadPpdbVa", Name = "DownloadPpdbVa")]
        public ActionResult DownloadPpdbVa()
        {
            try
            {
                var tahunPelajaran = schService.GetTahunAjaran(out string oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    tahunPelajaran = new TahunAjaranModel { Nama = "TP" };
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GeneratePpdbVa(IdUser, tahunPelajaran.Nama, out oMessage);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("daftar_ppdb_va_{0}.xlsx", tahunPelajaran.Nama);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }


        [HttpGet("DownloadPpdbCalonSiswa", Name = "DownloadPpdbCalonSiswa")]
        public ActionResult DownloadPpdbCalonSiswa()
        {
            try
            {
                var ret = ppdbService.GetPpdbSiswas(IdUser, out string oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                ExcelPpdb excel = new ExcelPpdb();
                var filename = excel.GenerateFileLaporanPPdbSiswa(IdUser, ret, out oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("{0}_{1}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmss"), "daftarcalonsiswa");
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }

        [HttpGet("DownloadMutabaahSiswa", Name = "DownloadMutabaahSiswa")]
        public ActionResult DownloadMutabaahSiswa(int IdKelasParalel, string Tanggal)
        {
            try
            {
                var ret = eLearningLaporanService.GetElEvalHarianList(IdUser, IdKelasParalel, Tanggal, out string oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateLaporanMutabaahSiswa(IdUser, ret, out oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("{0}_{1}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmss"), "lap_mutabaah_siswa");
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }

        // [HttpPost("DownloadLaporanGelombangPpdb", Name = "DownloadLaporanGelombangPpdb")]
        // public ActionResult DownloadLaporanGelombangPpdb(string TanggalAwal, string TanggalAkhir, SheetLaporanStatikGelombangPpdbModel Data)
        // {
        //     try
        //     {
        //         var ret = ppdbService.GetLaporanGelombangPpdb(IdUser, TanggalAwal, TanggalAkhir, Data, out string oMessage);
        //         if (!string.IsNullOrEmpty(oMessage))
        //             return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

        //         Tools.ExcelTool excel = new Tools.ExcelTool();
        //         var filename = excel.GenerateLaporanGelombangPpdb(TanggalAwal, TanggalAkhir, ret, IdUser, out oMessage);
        //         if (!string.IsNullOrEmpty(oMessage))
        //             return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

        //         var stream = new FileStream(filename, FileMode.Open);
        //         string fileDownload = string.Format("{0}_{1}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmss"), "laporan_gelombang_ppdb");
        //         return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

        //     }
        //     catch (Exception ex)
        //     {
        //         return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
        //     }
        // }

        [HttpPost("DownloadLaporanGelombangPpdb", Name = "DownloadLaporanGelombangPpdb")]
        public ActionResult DownloadLaporanGelombangPpdb(string TanggalAwal, string TanggalAkhir, SheetLaporanStatikGelombangPpdbModel Data)
        {
            try
            {
                var ret = ppdbService.GetLaporanGelombangPpdbTest(IdUser, TanggalAwal, TanggalAkhir, out string oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateLaporanGelombangPpdbTest(TanggalAwal, TanggalAkhir, ret, IdUser, Data, out oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("{0}_{1}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmss"), "laporan_gelombang_ppdb");
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }
        [HttpGet("DownloadLaporanGelombangPpdbTest", Name = "DownloadLaporanGelombangPpdbTest")]
        public ActionResult DownloadLaporanGelombangPpdbTest(string TanggalAwal, string TanggalAkhir)
        {
            try
            {
                SheetLaporanStatikGelombangPpdbModel Data = new SheetLaporanStatikGelombangPpdbModel
                {
                    KuotaBiayaPendidikanSd = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaBiayaPendidikanSmaIpa = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaBiayaPendidikanSmaIps = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaBiayaPendidikanSmp = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaBiayaPendidikanTkA = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaBiayaPendidikanTkB = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaCadanganObservasiSd = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaCadanganObservasiSmaIpa = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaCadanganObservasiSmaIps = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaCadanganObservasiSmp = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaCadanganObservasiTkA = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaCadanganObservasiTkB = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaDaftarSd = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaDaftarSmaIpa = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaDaftarSmaIps = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaDaftarSmp = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaDaftarTkA = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaDaftarTkB = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaDaftarTungguSd = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaDaftarTungguSmaIpa = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaDaftarTungguSmaIps = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaDaftarTungguSmp = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaDaftarTungguTkA = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaDaftarTungguTkB = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaLulusObservasiSd = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaLulusObservasiSmaIpa = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaLulusObservasiSmaIps = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaLulusObservasiSmp = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaLulusObservasiTkA = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaLulusObservasiTkB = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaPpdbSd = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaPpdbSmaIpa = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaPpdbSmaIps = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaPpdbSmp = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaPpdbTkA = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaPpdbTkB = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaTidakLulusObservasiSd = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaTidakLulusObservasiSmaIpa = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaTidakLulusObservasiSmaIps = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaTidakLulusObservasiSmp = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaTidakLulusObservasiTkA = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },
                    KuotaTidakLulusObservasiTkB = new SheetLaporanJkPpdbModel { TotL = 100, TotP = 100 },

                };
                var ret = ppdbService.GetLaporanGelombangPpdbTest(IdUser, TanggalAwal, TanggalAkhir, out string oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateLaporanGelombangPpdbTest(TanggalAwal, TanggalAkhir, ret, IdUser, Data, out oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("{0}_{1}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmss"), "laporan_gelombang_ppdb");
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }

        [HttpPost("DownloadLaporanElearningProgressPegawai", Name = "DownloadLaporanElearningProgressPegawai")]
        public ActionResult DownloadLaporanElearningProgressPegawai(string TanggalAwal, string TanggalAkhir, int IdUnit)
        {
            try
            {
                var ret = eLearningLaporanService.GetLaporanElearningProgressPegawai(IdUser, TanggalAwal, TanggalAkhir, IdUnit, out string oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateLaporanElearningProgressPegawai(TanggalAwal, TanggalAkhir, ret, out oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("{0}_{1}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmss"), "laporan_gelombang_ppdb");
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }
        [HttpGet("DownloadKelompokQiroaty", Name = "DownloadKelompokQiroaty")]
        public ActionResult DownloadKelompokQiroaty()
        {
            try
            {
                var file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/TemplateKelompokQiroaty.xlsx");
                var stream = new FileStream(file, FileMode.Open);
                string fileDownload = string.Format("{0}.xlsx", "TemplateKelompokQiroaty");
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }

        [HttpGet("DownloadBerkasPpdb", Name = "DownloadBerkasPpdb")]
        public ActionResult DownloadBerkasPpdb()
        {
            var ret = ppdbService.DownloadBerkas(out string oMessage);
            var path = AppDomain.CurrentDomain.BaseDirectory + "file.zip";
            using (ZipFile zip = new ZipFile())
            {
                var pathBackup = Path.Combine(AppSetting.PathApplFile, "BackupPpdb");
                zip.AddDirectory(pathBackup, "files");
                zip.Save(path);

                var bytes = System.IO.File.ReadAllBytes(path);
                System.IO.File.Delete(path);
                string fileDownload = string.Format("backup-{0}.zip", DateTime.Now.ToString("yyyyMMddHHmmss"));
                return File(bytes, "application/zip", fileDownload);
            }

        }

    }
}
