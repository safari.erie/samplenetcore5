using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Swg.Models;
using System.Linq;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using System;

namespace Swg.Sch.Adm.Controllers
{
    public class KeuangansController : BaseController
    {
        private readonly ICoaService _coaService;
        private readonly IKeuRenPenService renPenService;
        private readonly IKeuProgKegService progKegService;
        private readonly IKeuPengajuanService pengajuanService;
        private readonly IKeuPtgjwbService ptgjwbService;
        private readonly int IdUser;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        public KeuangansController(IHttpContextAccessor httpContextAccessor,
         ICoaService coaService,
        IKeuRenPenService RenPenService,
        IKeuProgKegService ProgKegService,
        IKeuPengajuanService PengajuanService,
        IKeuPtgjwbService PtgjwbService)
        {
            _coaService = coaService;
            renPenService = RenPenService;
            progKegService = ProgKegService;
            pengajuanService = PengajuanService;
            ptgjwbService = PtgjwbService;
            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }
        }
        
        #region COA
        public ResponseModel<List<ComboModel>> GetJenisAkunCoa()
        {
            var ret = _coaService.GetJenisAkun();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = ret != null ? "" : "data tidak ada",
                Data = ret
            };
        }

        public ResponseModel<List<ComboModel>> GetJenisTransaksiCoa()
        {
            var ret = _coaService.GetJenisTransaksi();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = ret != null ? "" : "data tidak ada",
                Data = ret
            };
        }

        public ResponseModel<string> AddCoa([FromBody] CoaAddModel Data)
        {
            string ret = _coaService.AddCoa(Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }

        public ResponseModel<string> EditCoa([FromBody] CoaAddModel Data)
        {
            string ret = _coaService.EditCoa(Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }

        public ResponseModel<List<CoaModel>> GetCoas()
        {
            var ret = _coaService.GetCoas(out string oMessage);
            return new ResponseModel<List<CoaModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        public ResponseModel<List<CoaModel>> GetCoasByJenisAkun(int IdJenisAkun)
        {
            var ret = _coaService.GetCoas(IdJenisAkun, out string oMessage);
            return new ResponseModel<List<CoaModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<CoaModel>> GetCoasByParent(int IdCoa)
        {
            var ret = _coaService.GetCoasByParent(IdCoa, out string oMessage);
            return new ResponseModel<List<CoaModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        public ResponseModel<CoaModel> GetCoa(string Kode)
        {
            var ret = _coaService.GetCoa(Kode, out string oMessage);
            return new ResponseModel<CoaModel>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = ret != null ? "" : "data tidak ada",
                Data = ret
            };
        }
        public ResponseModel<CoaModel> DeleteCoa(int IdCoa)
        {
            var ret = _coaService.DeleteCoa(IdCoa);
            return new ResponseModel<CoaModel>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }

        public ResponseModel<CoaModel> AddJenisSatuan(string Nama, string NamaSingkat)
        {
            var ret = _coaService.AddJenisSatuan(Nama, NamaSingkat);
            return new ResponseModel<CoaModel>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }

        public ResponseModel<CoaModel> AddJenisVolume(string Nama, string NamaSingkat)
        {
            var ret = _coaService.AddJenisVolume(Nama, NamaSingkat);
            return new ResponseModel<CoaModel>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }
        public ResponseModel<List<ComboModel>> GetJenisSatuans()
        {
            List<ComboModel> ret = _coaService.GetJenisSatuans(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        public ResponseModel<List<ComboModel>> GetJenisVolumes()
        {
            List<ComboModel> ret = _coaService.GetJenisVolumes(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<CoaModel> EditJenisSatuan(int IdJenisSatuan, string Nama, string NamaSingkat)
        {
            var ret = _coaService.EditJenisSatuan(IdJenisSatuan, Nama, NamaSingkat);
            return new ResponseModel<CoaModel>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }
        public ResponseModel<CoaModel> EditJenisVolume(int IdJenisVolume, string Nama, string NamaSingkat)
        {
            var ret = _coaService.EditJenisVolume(IdJenisVolume, Nama, NamaSingkat);
            return new ResponseModel<CoaModel>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }
        public ResponseModel<CoaModel> DeleteJenisSatuan(int IdJenisSatuan)
        {
            var ret = _coaService.DeleteJenisSatuan(IdJenisSatuan);
            return new ResponseModel<CoaModel>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }
        public ResponseModel<CoaModel> DeleteJenisVolume(int IdJenisVolume)
        {
            var ret = _coaService.DeleteJjenisVolume(IdJenisVolume);
            return new ResponseModel<CoaModel>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }
        #endregion

        #region Rencana Pendapatan
        public ResponseModel<string> InitRenPen(int IdTahunAjaran)
        {

            var ret = renPenService.InitRenPen(IdUser, IdTahunAjaran);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = string.Empty
            };
        }
        public ResponseModel<List<ComboModel>> GetCoaRenPens()
        {
            var ret = renPenService.GetCoas(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<KeuUnitRenPenListModel>> GetRenPens(int IdTahunAjaran)
        {
            var ret = renPenService.GetRenPens(IdUser, IdTahunAjaran, out string oMessage);
            return new ResponseModel<List<KeuUnitRenPenListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<KeuUnitRenPenCoaListModel>> GetRenPenCoas(int IdUnitRenPen)
        {
            var ret = renPenService.GetRenPenCoas(IdUser, IdUnitRenPen, out string oMessage);
            return new ResponseModel<List<KeuUnitRenPenCoaListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<KeuUnitRenPenCoaModel> GetRenPenCoa(int IdUnitRenPen, int IdCoa)
        {
            var ret = renPenService.GetRenPenCoa(IdUnitRenPen, IdCoa, out string oMessage);
            return new ResponseModel<KeuUnitRenPenCoaModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> RenPenAdd(int IdUnitRenPen, int IdCoa, double Rp)
        {
            var ret = renPenService.AddData(IdUser, IdUnitRenPen, IdCoa, Rp);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> RenPenEdit(int IdUnitRenPen, int IdCoa, double Rp)
        {
            var ret = renPenService.EditData(IdUser, IdUnitRenPen, IdCoa, Rp);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> RenPenDelete(int IdUnitRenPen, int IdCoa)
        {
            var ret = renPenService.DeleteData(IdUser, IdUnitRenPen, IdCoa);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> RenPenApproveDataByManKeu(int IdUnitRenPen, int StatusProses, string Catatan)
        {
            var ret = renPenService.ApproveDataByManKeu(IdUser, IdUnitRenPen, StatusProses, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> RenPenApproveDataByDirektur(int IdUnitRenPen, int StatusProses, string Catatan)
        {
            var ret = renPenService.ApproveDataByDirektur(IdUser, IdUnitRenPen, StatusProses, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> RenPenSetSah(int IdUnitRenPen)
        {
            var ret = renPenService.SetSah(IdUser, IdUnitRenPen);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ActionResult DownloadRenPen(int IdUnitRenPen)
        {
            try
            {

                var ret = renPenService.GetRenPenDownload(IdUser, IdUnitRenPen, out string oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelKeuTool excel = new Tools.ExcelKeuTool();

                var filename = excel.GenerateFileRenPen(ret, out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("renpen_{0}_{1}.xlsx", ret.NamaUnit, ret.TahunAjaran);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }
        #endregion Rencana Pendapatan
        
        #region Program Kegiatan Sekolah
        public ResponseModel<List<ComboModel>> GetCoaProgKegs()
        {
            var ret = progKegService.GetCoas(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<KeuUnitListModel>> GetKeuProKegUnits(int IdTahunAjaran)
        {
            var ret = progKegService.GetUnits(IdUser, IdTahunAjaran, out string oMessage);
            return new ResponseModel<List<KeuUnitListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<KeuUnitProListModel>> GetKeuProKegUnitPrograms(int IdUnitProKeg)
        {
            var ret = progKegService.GetPrograms(IdUnitProKeg, out string oMessage);
            return new ResponseModel<List<KeuUnitProListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<KeuUnitProKegListModel>> GetKeuProKegUnitKegiatans(int IdUnitProKeg, int IdProgram)
        {
            var ret = progKegService.GetKegiatans(IdUser, IdUnitProKeg, IdProgram, out string oMessage);
            return new ResponseModel<List<KeuUnitProKegListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }


        // BARU
        public ResponseModel<List<KeuUnitProKegWajibCoaListModel>> GetKeuKegiatanWajibCoas(int IdUnitProKegWajib)
        {
            var ret = progKegService.GetKegiatanWajibCoas(IdUser, IdUnitProKegWajib, out string oMessage);
            return new ResponseModel<List<KeuUnitProKegWajibCoaListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<KeuUnitProKegTambahanCoaListModel>> GetKeuKegiatanTambahanCoas(int IdUnitProKegTambahan)
        {
            var ret = progKegService.GetKegiatanTambahanCoas(IdUser, IdUnitProKegTambahan, out string oMessage);
            return new ResponseModel<List<KeuUnitProKegTambahanCoaListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<KeuUnitProKegWajibCoaListModel> GetKeuKegiatanWajibCoa(int IdUnitProkegWajibCoa)
        {
            var ret = progKegService.GetKegiatanWajibCoa(IdUnitProkegWajibCoa, out string oMessage);
            return new ResponseModel<KeuUnitProKegWajibCoaListModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<KeuUnitProKegTambahanCoaListModel> GetKeuKegiatanTambahanCoa(int IdUnitProkegTambahanCoa)
        {
            var ret = progKegService.GetKegiatanTambahanCoa(IdUnitProkegTambahanCoa, out string oMessage);
            return new ResponseModel<KeuUnitProKegTambahanCoaListModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<int> KegiatanTambahanAdd(int IdUnitProKeg, int IdProgram, string NamaKegiatan, string Tanggal)
        {
            var oMessage = progKegService.KegiatanTambahanAdd(IdUser, IdUnitProKeg, IdProgram, NamaKegiatan, Tanggal, out int IdUnitProkegTambahan);
            return new ResponseModel<int>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = IdUnitProkegTambahan
            };
        }
        public ResponseModel<string> KegiatanTambahanEdit(int IdUnitProkegTambahan, string NamaKegiatan, string Tanggal)
        {
            var oMessage = progKegService.KegiatanTambahanEdit(IdUser, IdUnitProkegTambahan, NamaKegiatan, Tanggal);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> KegiatanTambahanDelete(int IdUnitProkegTambahan)
        {
            var oMessage = progKegService.KegiatanTambahanDelete(IdUser, IdUnitProkegTambahan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> KegiatanWajibCoaAdd(KeuUnitProKegCoaWajibAddModel Data)
        {
            var oMessage = progKegService.KegiatanWajibCoaAdd(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> KegiatanWajibCoaEdit(KeuUnitProKegCoaWajibEditModel Data)
        {
            var oMessage = progKegService.KegiatanWajibCoaEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> KegiatanWajibCoaDelete(int IdUnitProkegWajibCoa)
        {
            var oMessage = progKegService.KegiatanWajibCoaDelete(IdUser, IdUnitProkegWajibCoa);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> KegiatanTambahanCoaAdd(KeuUnitProKegCoaTambahanAddModel Data)
        {
            var oMessage = progKegService.KegiatanTambahanCoaAdd(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> KegiatanTambahanCoaEdit(KeuUnitProKegCoaTambahanEditModel Data)
        {
            var oMessage = progKegService.KegiatanTambahanCoaEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> KegiatanTambahanCoaDelete(int IdUnitProkegTambahanCoa)
        {
            var oMessage = progKegService.KegiatanTambahanCoaDelete(IdUser, IdUnitProkegTambahanCoa);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ActionResult DownloadProKeg(int IdUnitProkeg)
        {
            try
            {

                var ret = progKegService.GetUnitDownload(IdUser, IdUnitProkeg, out string oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelKeuTool excel = new Tools.ExcelKeuTool();

                var filename = excel.GenerateFileProKeg(ret, out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("prokeg_{0}_{1}.xlsx", ret.NamaUnit, ret.TahunAjaran);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }
        public ResponseModel<string> ProKegApproveByKaUnit(int IdUnitProkeg, int StatusProses, string Catatan)
        {
            var ret = progKegService.ApproveByKaUnit(IdUser, IdUnitProkeg, StatusProses, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> ProKegApproveByManUnit(int IdUnitProkeg, int StatusProses, string Catatan)
        {
            var ret = progKegService.ApproveByManUnit(IdUser, IdUnitProkeg, StatusProses, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> ProKegApproveByManKeu(int IdUnitProkeg, int StatusProses, string Catatan)
        {
            var ret = progKegService.ApproveByManKeu(IdUser, IdUnitProkeg, StatusProses, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> ProKegApproveByDirektur(int IdUnitProkeg, int StatusProses, string Catatan)
        {
            var ret = progKegService.ApproveByDirektur(IdUser, IdUnitProkeg, StatusProses, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> ProKegSetSah(int IdUnitProkeg)
        {
            var ret = progKegService.SetSah(IdUser, IdUnitProkeg);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        #endregion Program Kegiatan Sekolah
        #region COA PAGU
        public ResponseModel<List<KeuUnitCoaPaguModel>> GetUnitCoaPagus(int IdUnit, int IdTahunAjaran)
        {
            var ret = renPenService.GetUnitCoaPagus(IdUser, IdUnit, IdTahunAjaran, out string oMessage);
            return new ResponseModel<List<KeuUnitCoaPaguModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<string>> GetUnitCoaPaguProKegs(int IdUnitCoaPagu)
        {
            var ret = renPenService.GetUnitCoaPaguProKegs(IdUser, IdUnitCoaPagu, out string oMessage);
            return new ResponseModel<List<string>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> EditCoaPagu(int IdUnitCoaPagu, double RpPagu)
        {
            var oMessage = renPenService.EditCoaPagu(IdUser, IdUnitCoaPagu, RpPagu);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        #endregion COA Pagu

        #region Pengajuan
        public ResponseModel<List<ComboModel>> GetCoaPengajuans()
        {
            var ret = pengajuanService.GetCoas(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<KeuPengajuanListModel>> GetPengajuans(int IdTahunAjaran)
        {
            var ret = pengajuanService.GetPengajuans(IdUser, IdTahunAjaran, out string oMessage);
            return new ResponseModel<List<KeuPengajuanListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<ComboModel>> GetPengajuanPrograms()
        {
            var ret = pengajuanService.GetPrograms(IdUser, out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<ComboModel>> GetProgramKegiatanWajibs(int IdTahunAjaran, int IdProgram)
        {
            var ret = pengajuanService.GetKegiatanWajibs(IdUser, IdTahunAjaran, IdProgram, out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<ComboModel>> GetProgramKegiatanTambahans(int IdTahunAjaran, int IdProgram)
        {
            var ret = pengajuanService.GetKegiatanTambahans(IdUser, IdTahunAjaran, IdProgram, out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> SetCoaPengajuanWajib(int IdUnitProKegWajib, string Nomor, string Tanggal)
        {
            var oMessage = pengajuanService.SetCoaPengajuanWajib(IdUser, IdUnitProKegWajib, Nomor, Tanggal);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> SetCoaPengajuanTambahan(int IdUnitProKegTambahan, string Nomor, string Tanggal)
        {
            var oMessage = pengajuanService.SetCoaPengajuanTambahan(IdUser, IdUnitProKegTambahan, Nomor, Tanggal);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<List<KeuPengajuanCoaListModel>> GetPengajuanCoas(int IdPengajuan)
        {
            var ret = pengajuanService.GetPengajuanCoas(IdUser, IdPengajuan, out string oMessage);
            return new ResponseModel<List<KeuPengajuanCoaListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> PengajuanDelete(int IdPengajuan)
        {
            var oMessage = pengajuanService.PengajuanDelete(IdUser, IdPengajuan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> PengajuanEdit(int IdPengajuan, string Nomor, string Tanggal)
        {
            var oMessage = pengajuanService.PengajuanEdit(IdUser, IdPengajuan, Nomor, Tanggal);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> PengajuanCoaAdd(KeuPengajuanCoaAddModel Data)
        {
            var oMessage = pengajuanService.PengajuanCoaAdd(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> PengajuanCoaEdit(KeuPengajuanCoaEditModel Data)
        {
            var oMessage = pengajuanService.PengajuanCoaEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> PengajuanCoaDelete(int IdPengajuanCoa)
        {
            var oMessage = pengajuanService.PengajuanCoaDelete(IdUser, IdPengajuanCoa);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> PengajuanApproveByKaUnit(int IdPengajuan, int StatusProses, double RpRealisasi, string Catatan)
        {
            var oMessage = pengajuanService.ApproveByKaUnit(IdUser, IdPengajuan, StatusProses, RpRealisasi, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> PengajuanApproveByManUnit(int IdPengajuan, int StatusProses, double RpRealisasi, string Catatan)
        {
            var oMessage = pengajuanService.ApproveByManUnit(IdUser, IdPengajuan, StatusProses, RpRealisasi, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> PengajuanApproveByManKeu(int IdPengajuan, int StatusProses, double RpRealisasi, string Catatan)
        {
            var oMessage = pengajuanService.ApproveByManKeu(IdUser, IdPengajuan, StatusProses, RpRealisasi, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> PengajuanApproveByDirektur(int IdPengajuan, int StatusProses, double RpRealisasi, string Catatan)
        {
            var oMessage = pengajuanService.ApproveByDirektur(IdUser, IdPengajuan, StatusProses, RpRealisasi, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> PengajuanSetRealisasi(int IdPengajuan)
        {
            var oMessage = pengajuanService.SetRealisasi(IdUser, IdPengajuan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ActionResult DownloadPengajuan(int IdPengajuan)
        {
            try
            {

                var ret = pengajuanService.GetPengajuanDownload(IdUser, IdPengajuan, out string oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelKeuTool excel = new Tools.ExcelKeuTool();

                var filename = excel.GenerateFilePengajuan(ret, out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("pengajuan_{0}_{1}.xlsx", ret.NamaUnit, ret.TahunAjaran);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }
        #endregion Pengajuan

        #region Pertanggungjawaban
        public ResponseModel<List<KeuPengajuanListModel>> PtgjwbGetPengajuans(int IdTahunAjaran)
        {
            var ret = ptgjwbService.GetPengajuans(IdUser, IdTahunAjaran, out string oMessage);
            return new ResponseModel<List<KeuPengajuanListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<KeuPengajuanCoaListModel>> PtgjwbGetPengajuanCoas(int IdPengajuan)
        {
            var ret = ptgjwbService.GetPengajuanCoas(IdUser, IdPengajuan, out string oMessage);
            return new ResponseModel<List<KeuPengajuanCoaListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> SetPilihPengajuan(int IdPengajuan)
        {
            var oMessage = ptgjwbService.SetPilihPengajuan(IdUser, IdPengajuan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<List<KeuPtgjwbListModel>> GetPertanggungjawabans(int IdTahunAjaran)
        {
            var ret = ptgjwbService.GetPertanggungjawabans(IdUser, IdTahunAjaran, out string oMessage);
            return new ResponseModel<List<KeuPtgjwbListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<KeuPtgjwbCoaListModel>> GetPertanggungjawabanCoas(int IdPengajuan)
        {
            var ret = ptgjwbService.GetPertanggungjawabanCoas(IdUser, IdPengajuan, out string oMessage);
            return new ResponseModel<List<KeuPtgjwbCoaListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> PertanggungjawabanDelete(int IdPengajuan)
        {
            var oMessage = ptgjwbService.PertanggungjawabanDelete(IdUser, IdPengajuan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> PertanggungjawabanCoaAdd(KeuPtgjwbCoaAddModel Data)
        {
            var oMessage = ptgjwbService.PertanggungjawabanCoaAdd(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> PertanggungjawabanCoaEdit(KeuPtgjwbCoaEditModel Data)
        {
            var oMessage = ptgjwbService.PertanggungjawabanCoaEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> PertanggungjawabanCoaDelete(int IdPtgjwbCoa)
        {
            var oMessage = ptgjwbService.PertanggungjawabanCoaDelete(IdUser, IdPtgjwbCoa);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> SetDilaporkan(int IdPengajuan)
        {
            var oMessage = ptgjwbService.SetDilaporkan(IdUser, IdPengajuan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ResponseModel<string> SetDibukukan(int IdPengajuan, int StatusProses, string Catatan)
        {
            var oMessage = ptgjwbService.SetDibukukan(IdUser, IdPengajuan, StatusProses, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        public ActionResult DownloadPtgjwb(int IdPengajuan)
        {
            try
            {

                var ret = ptgjwbService.GetPertanggungjawabanDownload(IdUser, IdPengajuan, out string oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelKeuTool excel = new Tools.ExcelKeuTool();

                var filename = excel.GenerateFilePtgjwb(ret, out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("ptgjwb_{0}_{1}.xlsx", ret.NamaUnit, ret.TahunAjaran);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }
        #endregion Pertanggungjawaban

    }
}