﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Models;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;

namespace Swg.Sch.Adm.Controllers
{
    [Route("api/[controller]")]
    public class PegawaisController : BaseController
    {
        private readonly IPegawaiService pegawaiService;
        private readonly IELearningService elearningService;
        private readonly IUserAppService userService;
        private readonly ISchService schService;
        private readonly int IdUser;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        public PegawaisController(IHttpContextAccessor httpContextAccessor, IPegawaiService PegawaiService, IELearningService ElearningService, IUserAppService UserService, ISchService SchService)
        {
            pegawaiService = PegawaiService;
            elearningService = ElearningService;
            schService = SchService;
            userService = UserService;
            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }
        }

        [HttpGet("GetKelasParalelPegawai", Name = "GetKelasParalelPegawai")]
        public ResponseModel<List<ComboModel>> GetKelasParalelPegawai()
        {
            var ret = pegawaiService.GetKelasParalelPegawai(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetKompetensiPegawai", Name = "GetKompetensiPegawai")]
        public ResponseModel<List<ComboModel>> GetKompetensiPegawai()
        {
            var ret = pegawaiService.GetKompetensiPegawai();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetJenisStatusNikah", Name = "GetJenisStatusNikah")]
        public ResponseModel<List<ComboModel>> GetJenisStatusNikah()
        {
            var ret = pegawaiService.GetJenisStatusNikah();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetListPegawai", Name = "GetListPegawai")]
        public ResponseModel<List<PegawaiModel>> GetListPegawai()
        {
            var ret = pegawaiService.GetListPegawai(out string oMessage);
            return new ResponseModel<List<PegawaiModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("PegawaiAdd", Name = "PegawaiAdd")]
        public ResponseModel<string> PegawaiAdd(PegawaiAddModel Data)
        {

            var ret = pegawaiService.PegawaiAdd(IdUser, Data, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("PegawaiEdit", Name = "PegawaiEdit")]
        public ResponseModel<string> PegawaiEdit(PegawaiEditModel Data)
        {

            var ret = pegawaiService.PegawaiEdit(IdUser, Data, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("SetPegawaiActive", Name = "SetPegawaiActive")]
        public ResponseModel<string> SetPegawaiActive(int IdPegawai)
        {

            var ret = pegawaiService.SetPegawaiActive(IdUser, IdPegawai, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("SetPegawaiInActive", Name = "SetPegawaiInActive")]
        public ResponseModel<string> SetPegawaiInActive(int IdPegawai)
        {

            var ret = pegawaiService.SetPegawaiInActive(IdUser, IdPegawai, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetListUser", Name = "GetListUser")]
        public ResponseModel<List<UserModel>> GetListUser()
        {

            var ret = userService.GetUsers(out string oMessage);
            return new ResponseModel<List<UserModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetJenisGolonganPegawai", Name = "GetJenisGolonganPegawai")]
        public ResponseModel<List<ComboModel>> GetJenisGolonganPegawai()
        {

            var ret = schService.GetJenisPegawaiGolongans();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetListJenjangPendidikan", Name = "GetListJenjangPendidikan")]
        public ResponseModel<List<ComboModel>> GetListJenjangPendidikan()
        {

            var ret = pegawaiService.GetListJenjangPendidikan(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetListJabatan", Name = "GetListJabatan")]
        public ResponseModel<List<ComboModel>> GetListJabatan()
        {

            var ret = pegawaiService.GetListJabatan(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetPegawaiGajis", Name = "GetPegawaiGajis")]
        public ResponseModel<List<PegawaiGajiListModel>> GetPegawaiGajis(int Month, int Year)
        {

            var ret = pegawaiService.GetPegawaiGajis(IdUser, Month, Year, out string oMessage);
            return new ResponseModel<List<PegawaiGajiListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetSlipGaji", Name = "GetSlipGaji")]
        public ResponseModel<SlipGajiModel> GetSlipGaji(string BulanTahun)
        {

            var ret = pegawaiService.GetSlipGaji(IdUser, BulanTahun, out string oMessage);
            return new ResponseModel<SlipGajiModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("UploadGaji", Name = "UploadGaji")]
        public ResponseModel<string> UploadGaji(IFormFile FileExcel)
        {
            if (FileExcel == null || FileExcel.Length == 0)
                return new ResponseModel<string> { IsSuccess = false, Data = null, ReturnMessage = "file tidak ada" };

            Tools.ExcelTool excel = new Tools.ExcelTool();
            List<SheetGajiPegawaiModel> DataExcelPegawai = excel.ReadExcelGajiPegawai(FileExcel, out string oMessage);
            if (!string.IsNullOrEmpty(oMessage))
                return new ResponseModel<string> { IsSuccess = false, ReturnMessage = oMessage };

            var ret = pegawaiService.UploadGaji(IdUser, DataExcelPegawai);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        [HttpPost("UploadGajiPrevious", Name = "UploadGajiPrevious")]
        public ResponseModel<string> UploadGajiPrevious(IFormFile FileExcel)
        {
            if (FileExcel == null || FileExcel.Length == 0)
                return new ResponseModel<string> { IsSuccess = false, Data = null, ReturnMessage = "file tidak ada" };

            Tools.ExcelTool excel = new Tools.ExcelTool();
            List<SheetGajiPegawaiPreviousModel> DataExcelPegawaiGajiPrevious = excel.ReadExcelGajiPegawaiPrevious(FileExcel, out string oMessage);
            if (!string.IsNullOrEmpty(oMessage))
                return new ResponseModel<string> { IsSuccess = false, ReturnMessage = oMessage };

            var ret = pegawaiService.UploadGajiPrevious(IdUser, DataExcelPegawaiGajiPrevious);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        [HttpGet("GetPegawai", Name = "GetPegawai")]
        public ResponseModel<PegawaiModel> GetPegawai()
        {
            var ret = schService.GetPegawai(IdUser, out string oMessage);
            return new ResponseModel<PegawaiModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }



        #region Evaluasi Harian Pegawai
        [HttpGet("GetJenisEvaluasiHarianPegawai", Name = "GetJenisEvaluasiHarianPegawai")]
        public ResponseModel<List<ComboModel>> GetJenisEvaluasiHarianPegawai()
        {
            var ret = elearningService.GetJenisEvaluasiHarianPegawai(IdUser, out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKbmEvalHarianPegawais", Name = "GetKbmEvalHarianPegawais")]
        public ResponseModel<List<KbmEvalHarianListPegawaiModel>> GetKbmEvalHarianPegawais(string Tanggal)
        {
            var ret = elearningService.GetKbmEvalHarianPegawais(IdUser, Tanggal, out string oMessage);
            return new ResponseModel<List<KbmEvalHarianListPegawaiModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKbmEvalHarianPegawai", Name = "GetKbmEvalHarianPegawai")]
        public ResponseModel<KbmEvalHarianPegawaiModel> GetKbmEvalHarianPegawai(int IdPegawai, string Tanggal, int IdJenisEvaluasiHarianPegawai)
        {
            var ret = elearningService.GetKbmEvalHarianPegawai(IdPegawai, Tanggal, IdJenisEvaluasiHarianPegawai, out string oMessage);
            return new ResponseModel<KbmEvalHarianPegawaiModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("EvaluasiHarianAddPegawai", Name = "EvaluasiHarianAddPegawai")]
        public ResponseModel<string> EvaluasiHarianAddPegawai([FromBody] List<KbmEvalHarianAddPegawaiModel> Data)
        {
            var ret = elearningService.EvaluasiHarianAddPegawai(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpGet("UpdateTokenFcmPegawai", Name = "UpdateTokenFcmPegawai")]
        public ResponseModel<string> UpdateTokenFcmPegawai(string Token)
        {

            var ret = pegawaiService.UpdateTokenFcmPegawai(IdUser, Token, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        #endregion Evaluasi Harian Pegawai

    }
}
