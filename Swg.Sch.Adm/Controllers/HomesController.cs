﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Models;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Adm.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomesController : BaseController
    {

        private readonly int IdUser;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        private readonly IELearningLaporanService eLearningLaporanService;
        public HomesController(IHttpContextAccessor httpContextAccessor, IELearningLaporanService ELearningLaporanService)
        {

            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }
            eLearningLaporanService = ELearningLaporanService;
        }
        [HttpGet("GetDashboards", Name = "GetDashboards")]
        public ResponseModel<List<ElDashboardModel>> GetDashboards()
        {
            var ret = eLearningLaporanService.GetElDashboard(out string oMessage);
            return new ResponseModel<List<ElDashboardModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
    }
}
