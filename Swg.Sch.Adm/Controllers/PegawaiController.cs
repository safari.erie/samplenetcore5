using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Adm.Models;

namespace Swg.Sch.Adm.Controllers
{
    public class PegawaiController : BaseController
    {

        public IActionResult Index()
        {
            var jss = ModuleJs("Pegawai");
            ViewBag.ModuleJs = jss;
            return View();
        }

        public IActionResult SlipGaji()
        {
            var jss = ModuleJs("SlipGaji");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult EvaluasiHarian()
        {
            var jss = ModuleJs("EvaluasiHarian");
            ViewBag.ModuleJs = jss;
            return View();
        }

        public List<LibraryJsModel> ModuleJs(string Module)
        {
            List<LibraryJsModel> js = new List<LibraryJsModel>();

            LibraryJsModel pegawai_js = new LibraryJsModel
            {
                NameJs = "Pegawai.js",
                TypeJs = "module_adm",
                Path = "Pegawai"
            };

            LibraryJsModel slip_gaji_js = new LibraryJsModel
            {
                NameJs = "SlipGaji.js",
                TypeJs = "module_adm",
                Path = "Pegawai"
            };

            LibraryJsModel evaluasi_harian_js = new LibraryJsModel
            {
                NameJs = "EvaluasiHarian.js",
                TypeJs = "module_adm",
                Path = "Pegawai"
            };

            if (Module == "Pegawai")
            {
                js.Add(pegawai_js);
            }

            else if (Module == "SlipGaji")
            {
                js.Add(slip_gaji_js);
            }

            else if (Module == "EvaluasiHarian")
            {
                js.Add(evaluasi_harian_js);
            }

            return js;
        }

    }
}