﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Adm.Models;

namespace Swg.Sch.Adm.Controllers
{
    public class PpdbController : BaseController
    {
        public IActionResult Konfigurasi()
        {
            ViewBag.ModuleJs = ModuleJs("Konfigurasi"); return View();
        }
        public IActionResult Konten()
        {
            ViewBag.ModuleJs = ModuleJs("Konten"); return View();
        }
        public IActionResult Biaya()
        {
            ViewBag.ModuleJs = ModuleJs("Biaya"); return View();
        }
        public IActionResult RedaksiEmail()
        {
            ViewBag.ModuleJs = ModuleJs("RedaksiEmail"); return View();
        }
        public IActionResult Kelas()
        {
            ViewBag.ModuleJs = ModuleJs("Kelas"); return View();
        }
        public IActionResult ResendEmail()
        {
            ViewBag.ModuleJs = ModuleJs("ResendEmail"); return View();
        }
        public IActionResult JenisBiaya()
        {
            ViewBag.ModuleJs = ModuleJs("JenisBiaya"); return View();
        }
        public IActionResult KodeBayar()
        {
            ViewBag.ModuleJs = ModuleJs("KodeBayar"); return View();
        }



        #region List PPDB
        public IActionResult VerifikasiBerkas()
        {
            ViewBag.ModuleJs = ModuleJs("VerifikasiBerkas"); return View();
        }
        public IActionResult WawancaraOnline()
        {
            ViewBag.ModuleJs = ModuleJs("WawancaraOnline"); return View();
        }
        public IActionResult InputObservasi()
        {
            ViewBag.ModuleJs = ModuleJs("InputObservasi"); return View();
        }
        public IActionResult Pengesahan()
        {
            ViewBag.ModuleJs = ModuleJs("Pengesahan"); return View();
        }
        public IActionResult EditPendaftaran()
        {
            ViewBag.ModuleJs = ModuleJs("EditPendaftaran"); return View();
        }
        public IActionResult VerifikasiBiayaPendaftaran()
        {
            ViewBag.ModuleJs = ModuleJs("VerifikasiBiayaPendaftaran"); return View();
        }
        public IActionResult VerifikasiBiayaPendidikan()
        {
            ViewBag.ModuleJs = ModuleJs("VerifikasiBiayaPendidikan"); return View();
        }
        public IActionResult VerifikasiHasilObservasi()
        {
            ViewBag.ModuleJs = ModuleJs("VerifikasiHasilObservasi"); return View();
        }
        public IActionResult DaftarTunggu()
        {
            ViewBag.ModuleJs = ModuleJs("DaftarTunggu"); return View();
        }
        public IActionResult DaftarCicil()
        {
            ViewBag.ModuleJs = ModuleJs("DaftarCicil"); return View();
        }
        public IActionResult TidakLolosObservasi()
        {
            ViewBag.ModuleJs = ModuleJs("TidakLolosObservasi"); return View();
        }
        public IActionResult PertanyaanKuisioner()
        {
            ViewBag.ModuleJs = ModuleJs("PertanyaanKuisioner"); return View();
        }
        public IActionResult Seragam()
        {
            ViewBag.ModuleJs = ModuleJs("Seragam"); return View();
        }
        #endregion

        #region baru
        public IActionResult PpdbKelas()
        {
            ViewBag.ModuleJs = ModuleJs("PpdbKelas"); return View();
        }
        public IActionResult PpdbJadwal()
        {
            ViewBag.ModuleJs = ModuleJs("PpdbJadwal"); return View();
        }
        public IActionResult AbsensiTestObservasi()
        {
            ViewBag.ModuleJs = ModuleJs("AbsensiTestObservasi"); return View();
        }
        public IActionResult AbsensiWawancara()
        {
            ViewBag.ModuleJs = ModuleJs("AbsensiWawancara"); return View();
        }
        public IActionResult DaftarTungguSiswa()
        {
            ViewBag.ModuleJs = ModuleJs("DaftarTungguSiswa"); return View();
        }
        public IActionResult LaporanPpdbDaftar()
        {
            ViewBag.ModuleJs = ModuleJs("LaporanPpdbDaftar"); return View();
        }
        public IActionResult LaporanPpdbSiswa()
        {
            ViewBag.ModuleJs = ModuleJs("LaporanPpdbSiswa"); return View();
        }
        public IActionResult LaporanObservasiWawancara()
        {
            ViewBag.ModuleJs = ModuleJs("LaporanObservasiWawancara"); return View();
        }
        public IActionResult LaporanPpdb()
        {
            ViewBag.ModuleJs = ModuleJs("LaporanPpdb"); return View();
        }
        public IActionResult DaftarPeminat()
        {
            ViewBag.ModuleJs = ModuleJs("DaftarPeminat"); return View();
        }
        #endregion


        #region ModuleJS
        public List<LibraryJsModel> ModuleJs(string Module)
        {
            List<LibraryJsModel> js = new List<LibraryJsModel>();
            LibraryJsModel Konten = new LibraryJsModel
            {
                NameJs = "Konten.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel Konfigurasi = new LibraryJsModel
            {
                NameJs = "Konfigurasi.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel Biaya = new LibraryJsModel
            {
                NameJs = "Biaya.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel RedaksiEmail = new LibraryJsModel
            {
                NameJs = "RedaksiEmail.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel Kelas = new LibraryJsModel
            {
                NameJs = "Kelas.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel ResendEmail = new LibraryJsModel
            {
                NameJs = "ResendEmail.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel JenisBiaya = new LibraryJsModel
            {
                NameJs = "JenisBiaya.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel KodeBayar = new LibraryJsModel
            {
                NameJs = "KodeBayar.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };

            #region JS List PPDB
            LibraryJsModel VerifikasiBerkas = new LibraryJsModel
            {
                NameJs = "VerifikasiBerkas.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel WawancaraOnline = new LibraryJsModel
            {
                NameJs = "WawancaraOnline.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel InputObservasi = new LibraryJsModel
            {
                NameJs = "InputObservasi.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel Pengesahan = new LibraryJsModel
            {
                NameJs = "Pengesahan.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel EditPendaftaran = new LibraryJsModel
            {
                NameJs = "EditPendaftaran.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel VerifikasiBiayaPendaftaran = new LibraryJsModel
            {
                NameJs = "VerifikasiBiayaPendaftaran.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel VerifikasiBiayaPendidikan = new LibraryJsModel
            {
                NameJs = "VerifikasiBiayaPendidikan.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel VerifikasiHasilObservasi = new LibraryJsModel
            {
                NameJs = "VerifikasiHasilObservasi.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel DaftarTunggu = new LibraryJsModel
            {
                NameJs = "DaftarTunggu.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel DaftarCicil = new LibraryJsModel
            {
                NameJs = "DaftarCicil.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel TidakLolosObservasi = new LibraryJsModel
            {
                NameJs = "TidakLolosObservasi.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel PertanyaanKuisioner = new LibraryJsModel
            {
                NameJs = "PertanyaanKuisioner.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel Seragam = new LibraryJsModel
            {
                NameJs = "Seragam.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            #endregion

            #region js baru
            LibraryJsModel PpdbKelas = new LibraryJsModel
            {
                NameJs = "PpdbKelas.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel PpdbJadwal = new LibraryJsModel
            {
                NameJs = "PpdbJadwal.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel AbsensiTestObservasi = new LibraryJsModel
            {
                NameJs = "AbsensiTestObservasi.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel AbsensiWawancara = new LibraryJsModel
            {
                NameJs = "AbsensiWawancara.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel DaftarTungguSiswa = new LibraryJsModel
            {
                NameJs = "DaftarTungguSiswa.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel LaporanPpdbDaftar = new LibraryJsModel
            {
                NameJs = "LaporanPpdbDaftar.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel LaporanPpdbSiswa = new LibraryJsModel
            {
                NameJs = "LaporanPpdbSiswa.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel LaporanObservasiWawancara = new LibraryJsModel
            {
                NameJs = "LaporanObservasiWawancara.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel LaporanPpdb = new LibraryJsModel
            {
                NameJs = "LaporanPpdb.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            LibraryJsModel DaftarPeminat = new LibraryJsModel
            {
                NameJs = "DaftarPeminat.js",
                TypeJs = "module_adm",
                Path = "Ppdb"
            };
            #endregion

            if (Module == "Konfigurasi")
            {
                js.Add(Konfigurasi);
            }
            else if (Module == "Konten")
            {
                js.Add(Konten);
            }
            else if (Module == "Biaya")
            {
                js.Add(Biaya);
            }
            else if (Module == "RedaksiEmail")
            {
                js.Add(RedaksiEmail);
            }
            else if (Module == "Kelas")
            {
                js.Add(Kelas);
            }
            else if (Module == "ResendEmail")
            {
                js.Add(ResendEmail);
            }
            else if (Module == "JenisBiaya")
            {
                js.Add(JenisBiaya);
            }
            else if (Module == "KodeBayar")
            {
                js.Add(KodeBayar);
            }

            #region JS List PPDB
            else if (Module == "VerifikasiBerkas")
            {
                js.Add(VerifikasiBerkas);
            }
            else if (Module == "WawancaraOnline")
            {
                js.Add(WawancaraOnline);
            }
            else if (Module == "InputObservasi")
            {
                js.Add(InputObservasi);
            }
            else if (Module == "Pengesahan")
            {
                js.Add(Pengesahan);
            }
            else if (Module == "EditPendaftaran")
            {
                js.Add(EditPendaftaran);
            }
            else if (Module == "VerifikasiHasilObservasi")
            {
                js.Add(VerifikasiHasilObservasi);
            }
            else if (Module == "VerifikasiBiayaPendaftaran")
            {
                js.Add(VerifikasiBiayaPendaftaran);
            }
            else if (Module == "VerifikasiBiayaPendidikan")
            {
                js.Add(VerifikasiBiayaPendidikan);
            }
            else if (Module == "Pengesahan")
            {
                js.Add(Pengesahan);
            }
            else if (Module == "DaftarTunggu")
            {
                js.Add(DaftarTunggu);
            }
            else if (Module == "DaftarCicil")
            {
                js.Add(DaftarCicil);
            }
            else if (Module == "TidakLolosObservasi")
            {
                js.Add(TidakLolosObservasi);
            }
            else if (Module == "PertanyaanKuisioner")
            {
                js.Add(PertanyaanKuisioner);
            }
            else if (Module == "Seragam")
            {
                js.Add(Seragam);
            }
            #endregion


            #region logik baru
            else if (Module == "PpdbKelas")
            {
                js.Add(PpdbKelas);
            }
            else if (Module == "PpdbJadwal")
            {
                js.Add(PpdbJadwal);
            }
            else if (Module == "AbsensiTestObservasi")
            {
                js.Add(AbsensiTestObservasi);
            }
            else if (Module == "AbsensiWawancara")
            {
                js.Add(AbsensiWawancara);
            }
            else if (Module == "DaftarTungguSiswa")
            {
                js.Add(DaftarTungguSiswa);
            }
            else if (Module == "LaporanPpdbDaftar")
            {
                js.Add(LaporanPpdbDaftar);
            }
            else if (Module == "LaporanPpdbSiswa")
            {
                js.Add(LaporanPpdbSiswa);
            }
            else if (Module == "LaporanObservasiWawancara")
            {
                js.Add(LaporanObservasiWawancara);
            }
            else if (Module == "LaporanPpdb")
            {
                js.Add(LaporanPpdb);
            }
            else if (Module == "DaftarPeminat")
            {
                js.Add(DaftarPeminat);
            }
            #endregion
            return js;
        }
        #endregion



    }
}