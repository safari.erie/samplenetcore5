﻿using System.Collections.Generic;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Models;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;

namespace Swg.Sch.Adm.Controllers
{
    [Route("api/[controller]")]
    //[ApiController]
    public class UploadRaporsController : BaseController
    {

        private readonly IUserAppService _applSettingService;
        private readonly ISchService _schService;
        private readonly ISiswaService _siswaService;
        private readonly IELearningService _eLearningService;

        private readonly int IdUser;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        public UploadRaporsController(
            IHttpContextAccessor httpContextAccessor,
            IUserAppService applSettingService,
            IELearningService ELearningService,
            ISiswaService SiswaService,
            ISchService SchService)
        {
            _httpContextAccessor = httpContextAccessor;
            _applSettingService = applSettingService;
            _eLearningService = ELearningService;
            _siswaService = SiswaService;
            _schService = SchService;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }

        }

        [RequestFormLimits(MultipartBodyLengthLimit = 509715200)]
        [RequestSizeLimit(509715200)]
        [HttpPost("UploadRapor", Name = "UploadRapor")]
        public ResponseModel<string> UploadRapor(IFormFile FileRapor)
        {
            // var ApplSettingByCode = _applSettingService.GetApplSetting("pathuploadfile", out string oMessage);
            // if (!string.IsNullOrEmpty(oMessage))
            // {
            //     return new ResponseModel<string>
            //     {
            //         IsSuccess = string.IsNullOrEmpty(oMessage),
            //         ReturnMessage = oMessage,
            //         Data = string.Empty
            //     };
            // }
            // string PathUpload = ApplSettingByCode.ValueString;
            string PathUpload = AppSetting.PathSiswaRapor;
            TahunAjaranModel Ta = new TahunAjaranModel();
            Ta = _schService.GetTahunAjaran(out string oMessage);
            if (!string.IsNullOrEmpty(oMessage))
            {
                return new ResponseModel<string>
                {
                    IsSuccess = string.IsNullOrEmpty(oMessage),
                    ReturnMessage = oMessage,
                    Data = string.Empty
                };
            }
            int retTa = Ta.IdTahunAjaran;
            var ret = _siswaService.UploadRapor(IdUser, FileRapor, PathUpload, retTa, out oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = oMessage
            };
        }

        [HttpGet("GetJenisBantuanTests", Name = "GetJenisBantuanTests")]
        public ResponseModel<List<ComboModel>> GetJenisBantuanTest()
        {
            var ret = _eLearningService.GetJenisBantuan();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = "",
                Data = ret
            };
        }

        [HttpPost("PublishRapors", Name = "PublishRapors")]
        public ResponseModel<string> PublishRapors(int IdJenisRapor, List<int> IdSiswa)
        {
            var ret = _siswaService.PublishRapors(IdUser, IdJenisRapor, IdSiswa, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetApprovSiswaRapors", Name = "GetApprovSiswaRapors")]
        public ResponseModel<List<SiswaRaporModel>> GetApprovSiswaRapors(int IdKelas, int IdJenisRapor)
        {
            var ret = _siswaService.GetApprovSiswaRapors(IdUser, IdKelas, IdJenisRapor, out string oMessage);
            return new ResponseModel<List<SiswaRaporModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetSiswaRapors", Name = "GetSiswaRapors")]
        public ResponseModel<List<SiswaRaporModel>> GetSiswaRapors()
        {
            var ret = _siswaService.GetApprovSiswaRapors(IdUser, out string oMessage);
            return new ResponseModel<List<SiswaRaporModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetJenisRapors", Name = "GetJenisRapors")]
        public ResponseModel<List<JenisRaporModel>> GetJenisRapors()
        {
            var ret = _siswaService.GetJenisRapors(out string oMessage);
            return new ResponseModel<List<JenisRaporModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
    }
}