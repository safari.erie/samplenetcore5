﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Web;


namespace Swg.Sch.Adm.Controllers
{
    //Created  : Eri Safari
    //CreatedDt : 20200306
    public class BaseController : Controller
    {
        //public bool needToDirection = true;
        public override void OnActionExecuting(Microsoft.AspNetCore.Mvc.Filters.ActionExecutingContext filterContext)
        {
            //check Session here

            var sessionUsernameOrEmail = HttpContext.Session.GetInt32("UserIdOrUsernamOrEmail");
            if (sessionUsernameOrEmail == null)
            {
                filterContext.Result =

                RedirectToAction("Login", "User");

                return;

            }
            base.OnActionExecuting(filterContext);
        }
    }
}