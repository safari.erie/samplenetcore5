﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Swg.Models;
using Swg.Services;

namespace Swg.Sch.Adm.Controllers
{
    public class ApplSettingsController : BaseController
    {
        private readonly IUserAppService _applSettingService;
        private readonly int IdUser;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        public ApplSettingsController(IHttpContextAccessor httpContextAccessor, IUserAppService applSettingService)
        {
            _applSettingService = applSettingService;
            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }
        }

        public ResponseModel<List<ApplSettingModel>> GetApplSettings()
        {

            var ret = _applSettingService.GetApplSettings(out string oMessage);
            return new ResponseModel<List<ApplSettingModel>>
            {
                IsSuccess = ret != null,
                ReturnMessage = ret != null ? "" : "data tidak ada",
                Data = ret
            };
        }

        public ResponseModel<ApplSettingModel> GetApplSetting(string Code)
        {

            var ret = _applSettingService.GetApplSetting(Code, out string oMessage);
            return new ResponseModel<ApplSettingModel>
            {
                IsSuccess = ret != null,
                ReturnMessage = ret != null ? "" : "data tidak ada",
                Data = ret
            };
        }

        public ResponseModel<string> AddApplSetting(ApplSettingModel Data)
        {

            var ret = _applSettingService.AddApplSetting(Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }

        public ResponseModel<string> EditApplSetting(ApplSettingModel Data)
        {

            var ret = _applSettingService.EditApplSetting(Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }

        public ResponseModel<string> DeleteApplSetting(string Code)
        {
            var ret = _applSettingService.DeleteApplSetting(Code);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }
    }
}