﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;
using Swg.Models;

namespace Swg.Sch.Adm.Controllers
{
    public class AuthController : Controller
    {
        private readonly IUserAppService _userService;
        private readonly IPegawaiService pegawaiService;
        private readonly int IdUser;
        private readonly string Token = string.Empty;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        public AuthController(IHttpContextAccessor httpContextAccessor, IUserAppService userService, IPegawaiService PegawaiService)
        {
            pegawaiService = PegawaiService;
            _userService = userService;
            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }

        }

        public ResponseModel<UserModel> Login(string UsernameOrEmail, string Password)
        {

            UserModel ret = _userService.Login(UsernameOrEmail, Password, 1, out string oMessage);
            var IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false;
            if (IsSuccess == true)
            {
                HttpContext.Session.SetInt32("UserIdOrUsernamOrEmail", ret.IdUser);
                HttpContext.Session.SetInt32("IdUser", ret.IdUser);
                HttpContext.Session.SetInt32("RoleId", ret.Roles[0].IdRole);
                HttpContext.Session.SetString("Username", ret.Username);
                HttpContext.Session.SetString("NamaPengguna", ret.FirstName);
                return new ResponseModel<UserModel>
                {
                    IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                    ReturnMessage = oMessage,
                    Data = ret
                };

            }
            else
            {
                return new ResponseModel<UserModel>
                {
                    IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                    ReturnMessage = oMessage,
                    Data = ret
                };
            }
        }

        public ResponseModel<string> ResetPassword(string UsernameOrEmail)
        {
            string ret = _userService.ResetPassword(UsernameOrEmail, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        public ResponseModel<List<PegawaiModel>> GetListPegawaiBy(int IdRole)
        {
            var ret = pegawaiService.GetListPegawaiBy(IdRole, out string oMessage);
            return new ResponseModel<List<PegawaiModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> KirimNotifikasi(string to, string title, string body, string click_action)
        {
            var ret = pegawaiService.KirimNotifikasi(to, title, body, click_action, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
    }
}