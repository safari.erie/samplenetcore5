﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Swg.Models;
using Swg.Services;

namespace Swg.Sch.Adm.Controllers
{
    public class ApplTasksController : BaseController
    {
        private readonly IUserAppService _applTaskService;
        private readonly int IdUser;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        public ApplTasksController(IHttpContextAccessor httpContextAccessor, IUserAppService applTaskService)
        {
            _applTaskService = applTaskService;
            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }
        }

        public ResponseModel<ApplTaskRoleModel> GetApplTaskByRole(int IdRole)
        {

            var ret = _applTaskService.GetApplTaskByRole(IdRole, out string oMessage);
            return new ResponseModel<ApplTaskRoleModel>
            {
                IsSuccess = ret != null,
                ReturnMessage = ret != null ? "" : "data tidak ada",
                Data = ret
            };
        }

        public ResponseModel<string> AddTasksRole(int IdRole, List<int> IdApplTasks)
        {
            var ret = _applTaskService.AddTasksRole(IdRole, IdApplTasks);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }

        public ResponseModel<List<ApplTaskModel>> GetMenus(int IdAppl)
        {

            var ret = _applTaskService.GetMenus(IdAppl,IdUser,out string oMessage);
            return new ResponseModel<List<ApplTaskModel>>
            {
                IsSuccess = ret != null,
                ReturnMessage = ret != null ? "" : "data tidak ada",
                Data = ret
            };
        }

        public ResponseModel<List<ApplModel>> GetAppls()
        {

            var ret = _applTaskService.GetAppls(out string oMessage);
            return new ResponseModel<List<ApplModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        public ResponseModel<List<ApplTaskModel>> GetApplTasks(int IdAppl)
        {

            var ret = _applTaskService.GetApplTasks(IdAppl, out string oMessage);
            return new ResponseModel<List<ApplTaskModel>>
            {
                IsSuccess = ret != null,
                ReturnMessage = ret != null ? "" : "data tidak ada",
                Data = ret
            };
        }

        public ResponseModel<string> EditApplTask(ApplTaskEditModel Data)
        {

            var ret = _applTaskService.EditApplTask(Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }

        public ResponseModel<string> AddApplTask(ApplTaskAddModel Data)
        {

            var ret = _applTaskService.AddApplTask(Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }
        public ResponseModel<string> DeleteApplTask(int IdApplTask)
        {

            var ret = _applTaskService.DeleteApplTask(IdApplTask);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }
    }
}