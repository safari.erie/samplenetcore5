﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Models;

namespace Swg.Sch.Adm.Controllers
{
    public class UnitsController : BaseController
    {
        private readonly IUnitService _unitService;
        private readonly int IdUser;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        public UnitsController(IHttpContextAccessor httpContextAccessor, IUnitService unitService)
        {
            _unitService = unitService;
            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }
        }
        public ResponseModel<List<ComboModel>> GetStatusData()
        {
            var ret = _unitService.GetStatusData();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }
        public ResponseModel<UnitKegiatanReadModel> GetUnitKegiatan(int IdUnitKegiatan)
        {
            var ret = _unitService.GetUnitKegiatan(IdUnitKegiatan, out string oMessage);
            return new ResponseModel<UnitKegiatanReadModel>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<UnitKegiatanReadModel>> GetUnitKegiatans()
        {
            var ret = _unitService.GetUnitKegiatans(IdUser, out string oMessage);
            return new ResponseModel<List<UnitKegiatanReadModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> UnitKegiatanAdd(UnitKegiatanAddEditModel Data)
        {
            var ret = _unitService.UnitKegiatanAdd(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<string> UnitKegiatanEdit(UnitKegiatanAddEditModel Data)
        {
            var ret = _unitService.UnitKegiatanEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<string> UnitKegiatanDelete(int IdUnitKegiatan)
        {
            var ret = _unitService.UnitKegiatanDelete(IdUser, IdUnitKegiatan);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<string> UnitKegiatanReject(int IdUnitKegiatan)
        {
            var ret = _unitService.UnitKegiatanReject(IdUser, IdUnitKegiatan);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<List<PegawaiModel>> GetUnitPegawais(int IdUnit)
        {
            var ret = _unitService.GetUnitPegawais(IdUnit, out string oMessage);
            return new ResponseModel<List<PegawaiModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        public ResponseModel<string> UnitPegawaiAdd(int IdUnit, [FromBody] List<PegawaiJenisModel> Pegawais)
        {
            var ret = _unitService.UnitPegawaiAdd(IdUser, IdUnit, Pegawais);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<string> UnitPegawaiDelete(int IdUnit, [FromBody] List<PegawaiJenisModel> Pegawais)
        {
            var ret = _unitService.UnitPegawaiDelete(IdUser, IdUnit, Pegawais);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<List<UnitReadModel>> GetUnits()
        {
            var ret = _unitService.GetUnits(out string oMessage);
            return new ResponseModel<List<UnitReadModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        public ResponseModel<UnitReadModel> GetUnit(int IdUnit)
        {
            var ret = _unitService.GetUnit(IdUnit, out string oMessage);
            return new ResponseModel<UnitReadModel>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> UnitEdit(UnitAddEditModel Data)
        {
            var ret = _unitService.UnitEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<List<UnitGaleriReadModel>> GetUnitGaleris()
        {
            var ret = _unitService.GetUnitGaleris(IdUser, out string oMessage);
            return new ResponseModel<List<UnitGaleriReadModel>>
            {
                IsSuccess = ret != null,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<UnitGaleriReadModel> GetUnitGaleri(int IdUnitGaleri)
        {
            var ret = _unitService.GetUnitGaleri(IdUnitGaleri, out string oMessage);
            return new ResponseModel<UnitGaleriReadModel>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> UnitGaleriAdd(UnitGaleriAddEditModel Data)
        {
            var ret = _unitService.UnitGaleriAdd(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> UnitGaleriEdit(UnitGaleriAddEditModel Data)
        {
            var ret = _unitService.UnitGaleriEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> UnitGaleriDelete(int IdUnitGaleri)
        {
            var ret = _unitService.UnitGaleriDelete(IdUser, IdUnitGaleri);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<List<UnitEkskulReadModel>> GetUnitEkskuls()
        {
            var ret = _unitService.GetUnitEkskuls(IdUser, out string oMessage);
            return new ResponseModel<List<UnitEkskulReadModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<UnitEkskulReadModel> GetUnitEkskul(int IdUnitEkskul)
        {
            var ret = _unitService.GetUnitEkskul(IdUser, IdUnitEkskul, out string oMessage);
            return new ResponseModel<UnitEkskulReadModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> UnitEkskulAdd(UnitEkskulAddEditModel Data)
        {
            var ret = _unitService.UnitEkskulAdd(IdUser, Data, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> UnitEkskulEdit(UnitEkskulAddEditModel Data)
        {
            var ret = _unitService.UnitEkskulEdit(IdUser, Data, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> UnitEkskulReject(int IdUnitEkskul)
        {
            var ret = _unitService.UnitEkskulReject(IdUser, IdUnitEkskul, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> UnitEkskulDelete(int IdUnitEkskul)
        {
            var ret = _unitService.UnitEkskulDelete(IdUser, IdUnitEkskul, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        #region UNIT BANNER
        public ResponseModel<List<UnitBannerReadModel>> GetUnitBanners()
        {
            var ret = _unitService.GetUnitBanners(out string oMessage);
            return new ResponseModel<List<UnitBannerReadModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<UnitBannerReadModel> GetUnitBanner(int IdUnitBanner)
        {
            var ret = _unitService.GetUnitBanner(IdUnitBanner, out string oMessage);
            return new ResponseModel<UnitBannerReadModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> UnitBannerAdd(UnitBannerAddEditModel Data)
        {
            var ret = _unitService.UnitBannerAdd(Data, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> UnitBannerEdit(UnitBannerAddEditModel Data)
        {
            var ret = _unitService.UnitBannerEdit(Data, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> UnitBannerDelete(int IdUnitBanner)
        {
            var ret = _unitService.UnitBannerDelete(IdUnitBanner, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        #endregion
    }
}