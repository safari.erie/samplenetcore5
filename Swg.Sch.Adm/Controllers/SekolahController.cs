﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Adm.Models;

namespace Swg.Sch.Adm.Controllers
{
    public class SekolahController : BaseController
    {

        public IActionResult Index()
        {
            var jss = ModuleJs("Sekolah");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult VisiMisi()
        {
            var jss = ModuleJs("VisiMisi");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult Galeri()
        {
            var jss = ModuleJs("GaleriSekolah");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult Kegiatan()
        {
            var jss = ModuleJs("SekolahKegiatan");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult Banner()
        {
            var jss = ModuleJs("Banner");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult KalenderPendidikan()
        {
            var jss = ModuleJs("KalenderPendidikan");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult Fasilitas()
        {
            var jss = ModuleJs("FasilitasSekolah");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult Prestasi()
        {
            var jss = ModuleJs("PrestasiSekolah");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult Testimoni()
        {
            var jss = ModuleJs("Testimoni");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public List<LibraryJsModel> ModuleJs(string Module)
        {
            List<LibraryJsModel> js = new List<LibraryJsModel>();
            LibraryJsModel galerisekolah_js = new LibraryJsModel
            {
                NameJs = "GaleriSekolah.js",
                TypeJs = "module_adm",
                Path = "Sekolah"
            };
            LibraryJsModel visimisi_js = new LibraryJsModel
            {
                NameJs = "VisiMisi.js",
                TypeJs = "module_adm",
                Path = "Sekolah"
            };
            LibraryJsModel sekolah_js = new LibraryJsModel
            {
                NameJs = "Sekolah.js",
                TypeJs = "module_adm",
                Path = "Sekolah"
            };
            LibraryJsModel sekolahkegiatan_js = new LibraryJsModel
            {
                NameJs = "SekolahKegiatan.js",
                TypeJs = "module_adm",
                Path = "Sekolah"
            };
            LibraryJsModel banner_js = new LibraryJsModel
            {
                NameJs = "Banner.js",
                TypeJs = "module_adm",
                Path = "Sekolah"
            };
            LibraryJsModel kalenderpendidikan_js = new LibraryJsModel
            {
                NameJs = "KalenderPendidikan.js",
                TypeJs = "module_adm",
                Path = "Sekolah"
            };
            LibraryJsModel fasilitassekolah_js = new LibraryJsModel
            {
                NameJs = "Fasilitas.js",
                TypeJs = "module_adm",
                Path = "Sekolah"
            };
            LibraryJsModel prestasisekolah_js = new LibraryJsModel
            {
                NameJs = "Prestasi.js",
                TypeJs = "module_adm",
                Path = "Sekolah"
            };
            LibraryJsModel testimoni_js = new LibraryJsModel
            {
                NameJs = "Testimoni.js",
                TypeJs = "module_adm",
                Path = "Sekolah"
            };

            if (Module == "GaleriSekolah")
            {
                js.Add(galerisekolah_js);
            }
            else if (Module == "VisiMisi")
            {
                js.Add(visimisi_js);
            }
            else if (Module == "Sekolah")
            {
                js.Add(sekolah_js);
            }
            else if (Module == "SekolahKegiatan")
            {
                js.Add(sekolahkegiatan_js);
            }
            else if (Module == "Banner")
            {
                js.Add(banner_js);
            }
            else if (Module == "KalenderPendidikan")
            {
                js.Add(kalenderpendidikan_js);
            }
            else if (Module == "FasilitasSekolah")
            {
                js.Add(fasilitassekolah_js);
            }
            else if (Module == "PrestasiSekolah")
            {
                js.Add(prestasisekolah_js);
            }
            else if (Module == "Testimoni")
            {
                js.Add(testimoni_js);
            }

            return js;
        }
    }
}