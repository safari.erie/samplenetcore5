﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Adm.Models;

namespace Swg.Sch.Adm.Controllers
{
    public class SiswaController : BaseController
    {

        public IActionResult Index()
        {
            var jss = ModuleJs("Siswa");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult UploadTagihanSiswa()
        {
            var jss = ModuleJs("UploadTagihanSiswa");
            ViewBag.ModuleJs = jss;
            return View();
        }

        public List<LibraryJsModel> ModuleJs(string Module)
        {
            List<LibraryJsModel> js = new List<LibraryJsModel>();
            LibraryJsModel siswa_js = new LibraryJsModel
            {
                NameJs = "Siswa.js",
                TypeJs = "module_adm",
                Path = "Siswa"
            };
            LibraryJsModel uploadtagihansiswa_js = new LibraryJsModel
            {
                NameJs = "UploadTagihanSiswa.js",
                TypeJs = "module_adm",
                Path = "Siswa"
            };
            LibraryJsModel laporanpembayaran_js = new LibraryJsModel
            {
                NameJs = "LaporanPembayaran.js",
                TypeJs = "module_adm",
                Path = "Siswa"
            };

            if (Module == "Siswa")
            {
                js.Add(siswa_js);
            }
            else if (Module == "UploadTagihanSiswa")
            {
                js.Add(uploadtagihansiswa_js);
            }


            return js;
        }
    }
}