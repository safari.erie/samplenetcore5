﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Models;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Services;

namespace Swg.Sch.Adm.Controllers
{
    [Route("api/[controller]")]
    public class PpdbsController : BaseController
    // public class PpdbsController : Controller
    {
        private readonly IPpdbService ppdbService;
        private readonly ISchService schService;
        private readonly IPdfService pdfService;
        private readonly int IdUser;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        public PpdbsController(
            IHttpContextAccessor httpContextAccessor,
            IPpdbService PpdbService,
            ISchService SchService,
            IPdfService PdfService)
        {
            ppdbService = PpdbService;
            pdfService = PdfService;
            schService = SchService;
            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }
        }

        #region Reff


        [HttpGet("GetJalurPendaftarans", Name = "GetJalurPendaftarans")]
        public ResponseModel<List<ComboModel>> GetJalurPendaftarans()
        {

            var ret = schService.GetJalurPendaftarans();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetJenisPekerjaans", Name = "GetJenisPekerjaans")]
        public ResponseModel<List<ComboModel>> GetJenisPekerjaans()
        {

            var ret = schService.GetJenisPekerjaans(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetJenisPendaftarans", Name = "GetJenisPendaftarans")]
        public ResponseModel<List<ComboModel>> GetJenisPendaftarans()
        {

            var ret = schService.GetJenisPendaftarans();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetJenisPrestasis", Name = "GetJenisPrestasis")]
        public ResponseModel<List<ComboModel>> GetJenisPrestasis()
        {

            var ret = schService.GetJenisPrestasis();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetKategoriDaftars", Name = "GetKategoriDaftars")]
        public ResponseModel<List<ComboModel>> GetKategoriDaftars()
        {

            var ret = schService.GetKategoriDaftars();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetJenisRedaksiEmails", Name = "GetJenisRedaksiEmails")]
        public ResponseModel<List<ComboModel>> GetJenisRedaksiEmails()
        {

            var ret = schService.GetJenisRedaksiEmails();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetJenisBiayaPpdbs", Name = "GetJenisBiayaPpdbs")]
        public ResponseModel<List<ComboModel>> GetJenisBiayaPpdbs()
        {

            var ret = schService.GetJenisBiayaPpdbs(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetGelombangPpdbs", Name = "GetGelombangPpdbs")]
        public ResponseModel<List<ComboModel>> GetGelombangPpdbs()
        {

            var ret = schService.GetGelombangPpdbs();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetTingkatPrestasiSiswas", Name = "GetTingkatPrestasiSiswas")]
        public ResponseModel<List<ComboModel>> GetTingkatPrestasiSiswas()
        {
            var ret = schService.GetTingkatPrestasiSiswas();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetStatusPpdbs", Name = "GetStatusPpdbs")]
        public ResponseModel<List<ComboModel>> GetStatusPpdbs()
        {
            var ret = schService.GetStatusPpdbs();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetAgamas", Name = "GetAgamas")]
        public ResponseModel<List<ComboModel>> GetAgamas()
        {
            var ret = schService.GetAgamas();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetJenisWargaNegara", Name = "GetJenisWargaNegara")]
        public ResponseModel<List<ComboModel>> GetJenisWargaNegara()
        {
            var ret = schService.GetJenisWargaNegara();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetTinggalSiswa", Name = "GetTinggalSiswa")]
        public ResponseModel<List<ComboModel>> GetTinggalSiswa()
        {
            var ret = schService.GetTinggalSiswa();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetJenisTransportasi", Name = "GetJenisTransportasi")]
        public ResponseModel<List<ComboModel>> GetJenisTransportasi()
        {
            var ret = schService.GetJenisTransportasi();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetJenisBahasa", Name = "GetJenisBahasa")]
        public ResponseModel<List<ComboModel>> GetJenisBahasa()
        {
            var ret = schService.GetJenisBahasa();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetStatusHasilObservasi", Name = "GetStatusHasilObservasi")]
        public ResponseModel<List<ComboModel>> GetStatusHasilObservasi()
        {
            var ret = schService.GetStatusHasilObservasi();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetStatusVerifikasiObservasi", Name = "GetStatusVerifikasiObservasi")]
        public ResponseModel<List<ComboModel>> GetStatusVerifikasiObservasi()
        {
            var ret = schService.GetStatusVerifikasiObservasi();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        #endregion

        #region PPDB 
        [HttpGet("GetPpdb", Name = "GetPpdb")]
        public ResponseModel<PpdbModelRead> GetPpdb()
        {

            var ret = schService.GetPpdb(out string oMessage);
            return new ResponseModel<PpdbModelRead>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("PpdbAdd", Name = "PpdbAdd")]
        public ResponseModel<string> PpdbAdd(PpdbModelAddEdit Data)
        {

            var ret = ppdbService.PpdbAdd(Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpPost("PpdbEdit", Name = "PpdbEdit")]
        public ResponseModel<string> PpdbEdit(PpdbModelAddEdit Data)
        {

            var ret = ppdbService.PpdbEdit(Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpGet("PpdbDelete", Name = "PpdbDelete")]
        public ResponseModel<string> PpdbDelete(int IdPpdb)
        {

            var ret = ppdbService.PpdbDelete(IdPpdb, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        #endregion

        #region Manajemen WEB PPDB 
        [HttpGet("GetPpdbInfos", Name = "GetPpdbInfos")]
        public ResponseModel<List<PpdbInfoModelRead>> GetPpdbInfos()
        {

            var ret = ppdbService.GetPpdbInfos(out string oMessage);
            return new ResponseModel<List<PpdbInfoModelRead>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetPpdbInfo", Name = "GetPpdbInfo")]
        public ResponseModel<PpdbInfoModelRead> GetPpdbInfo(int IdPpdbInfo)
        {

            var ret = ppdbService.GetPpdbInfo(IdPpdbInfo, out string oMessage);
            return new ResponseModel<PpdbInfoModelRead>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("PpdbInfoAdd", Name = "PpdbInfoAdd")]
        public ResponseModel<string> PpdbInfoAdd(PpdbInfoModelAddEdit Data)
        {

            var ret = ppdbService.PpdbInfoAdd(Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpPost("PpdbInfoEdit", Name = "PpdbInfoEdit")]
        public ResponseModel<string> PpdbInfoEdit(PpdbInfoModelAddEdit Data)
        {

            var ret = ppdbService.PpdbInfoEdit(Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpGet("PpdbInfoDelete", Name = "PpdbInfoDelete")]
        public ResponseModel<string> PpdbInfoDelete(int IdPpdbInfo)
        {

            var ret = ppdbService.PpdbInfoDelete(IdPpdbInfo, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        #endregion

        #region PPDB Redaksi Email
        [HttpGet("GetPpdbRedaksiEmails", Name = "GetPpdbRedaksiEmails")]
        public ResponseModel<List<PpdbRedaksiEmailModel>> GetPpdbRedaksiEmails()
        {

            var ret = ppdbService.GetPpdbRedaksiEmails(out string oMessage);
            return new ResponseModel<List<PpdbRedaksiEmailModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetPpdbRedaksiEmail", Name = "GetPpdbRedaksiEmail")]
        public ResponseModel<PpdbRedaksiEmailModel> GetPpdbRedaksiEmail(int IdPpdbRedaksiEmail)
        {

            var ret = ppdbService.GetPpdbRedaksiEmail(IdPpdbRedaksiEmail, out string oMessage);
            return new ResponseModel<PpdbRedaksiEmailModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("PpdbRedaksiEmailAdd", Name = "PpdbRedaksiEmailAdd")]
        public ResponseModel<string> PpdbRedaksiEmailAdd(PpdbRedaksiEmailModel Data)
        {

            var ret = ppdbService.PpdbRedaksiEmailAdd(Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpPost("PpdbRedaksiEmailEdit", Name = "PpdbRedaksiEmailEdit")]
        public ResponseModel<string> PpdbRedaksiEmailEdit(PpdbRedaksiEmailModel Data)
        {

            var ret = ppdbService.PpdbRedaksiEmailEdit(Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpGet("PpdbRedaksiEmailDelete", Name = "PpdbRedaksiEmailDelete")]
        public ResponseModel<string> PpdbRedaksiEmailDelete(int IdPpdbRedaksiEmail)
        {

            var ret = ppdbService.PpdbRedaksiEmailDelete(IdPpdbRedaksiEmail, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        #endregion

        #region PPDB Biaya
        [HttpGet("GetKelasPpdb", Name = "GetKelasPpdb")]
        public ResponseModel<List<ComboModel>> GetKelasPpdb(int IdUnit)
        {

            var ret = schService.GetKelasPpdb(IdUnit, out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKelasPpdbs", Name = "GetKelasPpdbs")]
        public ResponseModel<List<ComboModel>> GetKelasPpdbs()
        {

            var ret = schService.GetKelasPpdbs(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetPpdbBiayas", Name = "GetPpdbBiayas")]
        public ResponseModel<List<PpdbBiayaModel>> GetPpdbBiayas()
        {

            var ret = ppdbService.GetPpdbBiayas(out string oMessage);
            return new ResponseModel<List<PpdbBiayaModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetPpdbBiaya", Name = "GetPpdbBiaya")]
        public ResponseModel<PpdbBiayaModel> GetPpdbBiaya(int IdPpdbBiaya)
        {

            var ret = ppdbService.GetPpdbBiaya(IdPpdbBiaya, out string oMessage);
            return new ResponseModel<PpdbBiayaModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("PpdbBiayaAdd", Name = "PpdbBiayaAdd")]
        public ResponseModel<string> PpdbBiayaAdd(PpdbBiayaAddEditModel Data)
        {

            var ret = ppdbService.PpdbBiayaAdd(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }
        [HttpPost("PpdbBiayaEdit", Name = "PpdbBiayaEdit")]
        public ResponseModel<string> PpdbBiayaEdit(PpdbBiayaAddEditModel Data)
        {

            var ret = ppdbService.PpdbBiayaEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }
        [HttpGet("PpdbBiayaDelete", Name = "PpdbBiayaDelete")]
        public ResponseModel<string> PpdbBiayaDelete(int IdPpdbBiaya)
        {

            var ret = ppdbService.PpdbBiayaDelete(IdUser, IdPpdbBiaya, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        #endregion



        [HttpGet("GetPpdbJenisBiaya", Name = "GetPpdbJenisBiaya")]
        public ResponseModel<List<ComboModel>> GetPpdbJenisBiaya(int IdJenisBiayaPpdb)
        {
            var ret = ppdbService.GetPpdbJenisBiaya(IdUser, IdJenisBiayaPpdb, out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = string.IsNullOrEmpty(oMessage) ? ret : null
            };
        }

        [HttpPost("PpdbJenisBiayaAdd", Name = "PpdbJenisBiayaAdd")]
        public ResponseModel<string> PpdbJenisBiayaAdd(ComboModel Data)
        {
            var ret = ppdbService.PpdbJenisBiayaAdd(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpPost("PpdbJenisBiayaEdit", Name = "PpdbJenisBiayaEdit")]
        public ResponseModel<string> PpdbJenisBiayaEdit(ComboModel Data)
        {
            var ret = ppdbService.PpdbJenisBiayaEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpGet("PpdbJenisBiayaDelete", Name = "PpdbJenisBiayaDelete")]
        public ResponseModel<string> PpdbJenisBiayaDelete(int IdJenisBiayaPpdb)
        {
            var ret = ppdbService.PpdbJenisBiayaDelete(IdUser, IdJenisBiayaPpdb, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = string.IsNullOrEmpty(oMessage) ? ret : null
            };
        }

        [HttpGet("GetListVa", Name = "GetListVa")]
        public ResponseModel<List<PpdbVaSheetModel>> GetListVa()
        {
            var ret = ppdbService.GetListVa(IdUser, out string oMessage);
            return new ResponseModel<List<PpdbVaSheetModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = string.IsNullOrEmpty(oMessage) ? ret : null
            };
        }

        [HttpPost("UploadVA", Name = "UploadVA")]
        public ResponseModel<string> UploadVA(IFormFile FileUpload)
        {
            if (FileUpload == null || FileUpload.Length == 0)
                return new ResponseModel<string> { IsSuccess = false, Data = null, ReturnMessage = "file tidak ada" };

            Tools.ExcelTool excel = new Tools.ExcelTool();
            List<PpdbVaSheetModel> ret = excel.ReadPpdbVa(FileUpload, out string oMessage);
            if (!string.IsNullOrEmpty(oMessage))
                return new ResponseModel<string> { IsSuccess = false, ReturnMessage = oMessage };
            oMessage = ppdbService.UploadVA(IdUser, ret);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = null,
            };
        }

        [HttpGet("DownloadObservasi", Name = "DownloadObservasi")]
        public ActionResult DownloadObservasi()
        {
            try
            {
                List<PpdbDaftarKartuModel> ret = new List<PpdbDaftarKartuModel>();// ppdbService.GetPpdbDaftarList(IdUser, StatusPpdbConstant.VerifikasiBerkas, out string oMessage);
                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateLaporanObservasi(ret, IdUser);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = DateTime.Now.ToString("yyyyMMddHHmmss") + "_laporan_observasi.xlsx";
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }

        [HttpPost("UploadObservasi", Name = "UploadObservasi")]
        public ResponseModel<string> UploadObservasi(IFormFile FileUpload)
        {
            if (FileUpload == null || FileUpload.Length == 0)
                return new ResponseModel<string> { IsSuccess = false, Data = null, ReturnMessage = "file tidak ada" };

            Tools.ExcelTool excel = new Tools.ExcelTool();
            List<PpdbObservasiSheetModel> ret = excel.ReadObservasi(FileUpload, out string oMessage);
            if (!string.IsNullOrEmpty(oMessage))
                return new ResponseModel<string> { IsSuccess = false, ReturnMessage = oMessage };
            oMessage = string.Empty;// ppdbService.InputObservasis(IdUser, ret);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = null,
            };
        }

        [HttpGet("GetPpdbSeragams", Name = "GetPpdbSeragams")]
        public ResponseModel<List<PpdbSeragamListModel>> GetPpdbSeragams()
        {

            var ret = ppdbService.GetPpdbSeragams(IdUser, out string oMessage);
            return new ResponseModel<List<PpdbSeragamListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetPpdbSeragam", Name = "GetPpdbSeragam")]
        public ResponseModel<PpdbSeragamModel> GetPpdbSeragam(int IdPpdbSeragam)
        {

            var ret = ppdbService.GetPpdbSeragam(IdPpdbSeragam, out string oMessage);
            return new ResponseModel<PpdbSeragamModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("PpdbSeragamAdd", Name = "PpdbSeragamAdd")]
        public ResponseModel<string> PpdbSeragamAdd(PpdbSeragamModel Data)
        {

            var ret = ppdbService.PpdbSeragamAdd(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpPost("PpdbSeragamEdit", Name = "PpdbSeragamEdit")]
        public ResponseModel<string> PpdbSeragamEdit(PpdbSeragamModel Data)
        {

            var ret = ppdbService.PpdbSeragamEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpGet("PpdbSeragamDelete", Name = "PpdbSeragamDelete")]
        public ResponseModel<string> PpdbSeragamDelete(int IdPpdbSeragam)
        {

            var ret = ppdbService.PpdbSeragamDelete(IdUser, IdPpdbSeragam, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = string.IsNullOrEmpty(oMessage) ? ret : null
            };
        }

        [HttpGet("GetPpdbJadwals", Name = "GetPpdbJadwals")]
        public ResponseModel<List<PpdbJadwalViewModel>> GetPpdbJadwals()
        {
            var ret = ppdbService.GetPpdbJadwals(out string oMessage);
            return new ResponseModel<List<PpdbJadwalViewModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("PpdbJadwalAdd", Name = "PpdbJadwalAdd")]
        public ResponseModel<string> PpdbJadwalAdd(PpdbJadwalModel Data)
        {
            var ret = ppdbService.PpdbJadwalAdd(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpPost("PpdbJadwalEdit", Name = "PpdbJadwalEdit")]
        public ResponseModel<string> PpdbJadwalEdit(PpdbJadwalEditModel Data)
        {
            var ret = ppdbService.PpdbJadwalEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpGet("PpdbJadwalDelete", Name = "PpdbJadwalDelete")]
        public ResponseModel<string> PpdbJadwalDelete(int IdUnit, int IdJenisGelombangPpdb, int IdJenisPendaftaran, int IdJalurPendaftaran)
        {

            var ret = ppdbService.PpdbJadwalDelete(IdUser, IdUnit, IdJenisGelombangPpdb, IdJenisPendaftaran, IdJalurPendaftaran);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        [HttpGet("GetPpdbKelass", Name = "GetPpdbKelass")]
        public ResponseModel<List<PpdbKelasViewModel>> GetPpdbKelass()
        {
            var ret = ppdbService.GetPpdbKelass(out string oMessage);
            return new ResponseModel<List<PpdbKelasViewModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("PpdbKelasAdd", Name = "PpdbKelasAdd")]
        public ResponseModel<string> PpdbKelasAdd(PpdbKelasModel Data)
        {
            var ret = ppdbService.PpdbKelasAdd(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpPost("PpdbKelasEdit", Name = "PpdbKelasEdit")]
        public ResponseModel<string> PpdbKelasEdit(PpdbKelasEditModel Data)
        {
            var ret = ppdbService.PpdbKelasEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpGet("PpdbKelasDelete", Name = "PpdbKelasDelete")]
        public ResponseModel<string> PpdbKelasDelete(int IdPpdbKelas)
        {

            var ret = ppdbService.PpdbKelasDelete(IdUser, IdPpdbKelas);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        [HttpGet("GetPpdbSiswas", Name = "GetPpdbSiswas")]
        public ResponseModel<List<PpdbSiswaListModel>> GetPpdbSiswas(int Status)
        {

            var ret = ppdbService.GetPpdbSiswas(IdUser, Status, out string oMessage);
            return new ResponseModel<List<PpdbSiswaListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetPpdbDaftars", Name = "GetPpdbDaftars")]
        public ResponseModel<List<PpdbDaftarModel>> GetPpdbDaftars(int Status)
        {

            var ret = ppdbService.GetPpdbDaftars(IdUser, Status, out string oMessage);
            return new ResponseModel<List<PpdbDaftarModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetPpdbDaftar", Name = "GetPpdbDaftar")]
        public ResponseModel<PpdbDaftarModel> GetPpdbDaftar(int IdPpdbDaftar)
        {

            var ret = ppdbService.GetPpdbDaftar(IdPpdbDaftar, out string oMessage);
            return new ResponseModel<PpdbDaftarModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetPpdbDaftarBiaya", Name = "GetPpdbDaftarBiaya")]
        public ResponseModel<PpdbBiayaDaftarModel> GetPpdbDaftarBiaya(int IdPpdbDaftar)
        {

            var ret = ppdbService.GetPpdbDaftarBiaya(IdPpdbDaftar, out string oMessage);
            return new ResponseModel<PpdbBiayaDaftarModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetPpdbSiswa", Name = "GetPpdbSiswa")]
        public ResponseModel<PpdbSiswaModel> GetPpdbSiswa(int IdPpdbDaftar)
        {

            var ret = ppdbService.GetPpdbSiswa(IdPpdbDaftar, out string oMessage);
            return new ResponseModel<PpdbSiswaModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("SetLunasBiayaDaftar", Name = "SetLunasBiayaDaftar")]
        public ResponseModel<string> SetLunasBiayaDaftar(int IdPpdbDaftar, string TanggalLunas, string Catatan)
        {
            var oMesage = ppdbService.SetLunasBiayaDaftar(IdUser, IdPpdbDaftar, TanggalLunas, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMesage) ? true : false,
                ReturnMessage = oMesage,
                Data = null
            };
        }

        [HttpGet("SetHadirTestObservasi", Name = "SetHadirTestObservasi")]
        public ResponseModel<string> SetHadirTestObservasi(int IdPpdbDaftar, string Catatan)
        {
            var oMesage = ppdbService.SetHadirTestObservasi(IdUser, IdPpdbDaftar, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMesage) ? true : false,
                ReturnMessage = oMesage,
                Data = null
            };
        }
        [HttpGet("SetHadirWawancara", Name = "SetHadirWawancara")]
        public ResponseModel<string> SetHadirWawancara(int IdPpdbDaftar, string Catatan)
        {
            var oMesage = ppdbService.SetHadirWawancara(IdUser, IdPpdbDaftar, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMesage) ? true : false,
                ReturnMessage = oMesage,
                Data = null
            };
        }

        [HttpPost("InputObservasi", Name = "InputObservasi")]
        public ResponseModel<string> InputObservasi(int IdPpdbDaftar, int StatusHasilObservasi, string Catatan, List<PpdbBiayaSiswaDetilAddModel> Biayas, List<PpdbBiayaSiswaCicilAddModel> Cicils)
        {
            var oMesage = ppdbService.InputObservasi(IdUser, IdPpdbDaftar, StatusHasilObservasi, Catatan, Biayas, Cicils);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMesage) ? true : false,
                ReturnMessage = oMesage,
                Data = null
            };
        }

        [HttpGet("VerifikasiObservasi", Name = "VerifikasiObservasi")]
        public ResponseModel<string> VerifikasiObservasi(int IdPpdbDaftar, int StatusVerfikasiObservasi, string Catatan)
        {
            var oMesage = ppdbService.VerifikasiObservasi(IdUser, IdPpdbDaftar, StatusVerfikasiObservasi, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMesage) ? true : false,
                ReturnMessage = oMesage,
                Data = null
            };
        }

        [HttpGet("SetLunasBiayaSiswa", Name = "SetLunasBiayaSiswa")]
        public ResponseModel<string> SetLunasBiayaSiswa(int IdPpdbDaftar, string TanggalLunas, string Catatan, int CilKe)
        {
            var oMesage = ppdbService.SetLunasBiayaSiswa(IdUser, IdPpdbDaftar, TanggalLunas, Catatan, CilKe);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMesage) ? true : false,
                ReturnMessage = oMesage,
                Data = null
            };
        }

        [HttpGet("BackToHadirObservasi", Name = "BackToHadirObservasi")]
        public ResponseModel<string> BackToHadirObservasi(int IdPpdbDaftar)
        {
            var oMesage = ppdbService.BackToHadirObservasi(IdUser, IdPpdbDaftar);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMesage) ? true : false,
                ReturnMessage = oMesage,
                Data = null
            };
        }

        [HttpGet("Pengesahan", Name = "Pengesahan")]
        public ResponseModel<string> Pengesahan(int IdPpdbDaftar)
        {
            var oMesage = ppdbService.Pengesahan(IdUser, IdPpdbDaftar);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMesage) ? true : false,
                ReturnMessage = oMesage,
                Data = null
            };
        }

        [HttpGet("SetTidakDaftarTunggu", Name = "SetTidakDaftarTunggu")]
        public ResponseModel<string> SetTidakDaftarTunggu(int IdPpdbDaftar)
        {
            var oMesage = ppdbService.SetTidakDaftarTunggu(IdUser, IdPpdbDaftar);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMesage) ? true : false,
                ReturnMessage = oMesage,
                Data = null
            };
        }

        [HttpGet("ResendEmail", Name = "ResendEmail")]
        public ResponseModel<string> ResendEmail(int IdPpdbDaftar, int IdProses)
        {

            var ret = ppdbService.ResendEmail(IdPpdbDaftar, IdProses);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        [HttpGet("DownloadLaporanPendaftaran", Name = "DownloadLaporanPendaftaran")]
        public ActionResult DownloadLaporanPendaftaran()
        {
            try
            {
                List<PpdbDaftarModel> ret = ppdbService.GetPpdbDaftars(IdUser, 1, out string oMessage);
                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateLaporanPpdbDaftars(ret, IdUser);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = DateTime.Now.ToString("yyyyMMddHHmmss") + "_laporan_pendaftaran.xlsx";
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }

        [HttpPost("EditPpdbDaftar", Name = "EditPpdbDaftar")]
        public ResponseModel<string> EditPpdbDaftar(int IdPpdbDaftar, PpdbDaftarAddModel Data)
        {
            var ret = ppdbService.EditPpdbDaftar(IdUser, IdPpdbDaftar, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        [HttpPost("EditPpdbSiswa", Name = "EditPpdbSiswa")]
        public ResponseModel<string> EditPpdbSiswa(
            PpdbSiswaModel Siswa,
            List<PpdbSiswaOrtuAddModel> Ortus,
            List<PpdbSiswaRaporAddModel> Rapors,
            List<PpdbSiswaPrestasiAddModel> Prestasis,
            IFormFile FileFoto,
            IFormFile FileKtpIbu,
            IFormFile FileKtpAyah,
            IFormFile FileKk,
            IFormFile FileAkte,
            IFormFile FileSuratKesehatan,
            IFormFile FileBebasNarkoba,
            List<IFormFile> FileRapors,
            List<IFormFile> FilePrestasis
        )
        {
            var ret = ppdbService.EditPpdbSiswa(
                IdUser,
                Siswa,
                Ortus,
                Rapors,
                Prestasis,
                FileFoto,
                FileKtpIbu,
                FileKtpAyah,
                FileKk,
                FileAkte,
                FileSuratKesehatan,
                FileBebasNarkoba,
                FileRapors,
                FilePrestasis);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        [HttpGet("GetPpdbSiswasByAdmin", Name = "GetPpdbSiswasByAdmin")]
        public ResponseModel<List<PpdbSiswaListModel>> GetPpdbSiswasByAdmin()
        {
            var ret = ppdbService.GetPpdbSiswasByAdmin(IdUser, out string oMessage);
            return new ResponseModel<List<PpdbSiswaListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetPpdbSiswasx", Name = "GetPpdbSiswasx")]
        public ResponseModel<List<SheetPpdbDaftarSiswaModel>> GetPpdbSiswasx()
        {
            var ret = ppdbService.GetPpdbSiswas(IdUser, out string oMessage);
            return new ResponseModel<List<SheetPpdbDaftarSiswaModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetPpdbSiswaObservasiWawancaras", Name = "GetPpdbSiswaObservasiWawancaras")]
        public ResponseModel<List<PpdbSiswaObservasiWawancara>> GetPpdbSiswaObservasiWawancaras()
        {
            var ret = ppdbService.GetPpdbSiswaObservasiWawancaras(IdUser, out string oMessage);
            return new ResponseModel<List<PpdbSiswaObservasiWawancara>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetPpdbSiswaObservasiWawancaraTipe", Name = "GetPpdbSiswaObservasiWawancaraTipe")]
        public ResponseModel<List<PpdbSiswaObservasiWawancara>> GetPpdbSiswaObservasiWawancaraTipe(int IdTipe)
        {
            var ret = ppdbService.GetPpdbSiswaObservasiWawancaras(IdUser, IdTipe, out string oMessage);
            return new ResponseModel<List<PpdbSiswaObservasiWawancara>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("DownloadLaporanObservasiWawancara", Name = "DownloadLaporanObservasiWawancara")]
        public ActionResult DownloadLaporanObservasiWawancara()
        {
            try
            {
                List<PpdbSiswaObservasiWawancara> ret = ppdbService.GetPpdbSiswaObservasiWawancaras(IdUser, out string oMessage);
                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateLaporanObservasiWawancara(ret, IdUser);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = DateTime.Now.ToString("yyyyMMddHHmmss") + "_laporan_observasi_wawancara.xlsx";
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }

        [HttpGet("DownloadLaporanObservasi", Name = "DownloadLaporanObservasi")]
        public ActionResult DownloadLaporanObservasi()
        {
            try
            {
                List<PpdbSiswaObservasiWawancara> ret = ppdbService.GetPpdbSiswaObservasiWawancaras(IdUser, 20, out string oMessage);
                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateLaporanObservasiWawancara(ret, IdUser);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = DateTime.Now.ToString("yyyyMMddHHmmss") + "_laporan_observasi.xlsx";
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }

        [HttpGet("DownloadLaporanWawancara", Name = "DownloadLaporanWawancara")]
        public ActionResult DownloadLaporanWawancara()
        {
            try
            {
                List<PpdbSiswaObservasiWawancara> ret = ppdbService.GetPpdbSiswaObservasiWawancaras(IdUser, 21, out string oMessage);
                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateLaporanObservasiWawancara(ret, IdUser);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = DateTime.Now.ToString("yyyyMMddHHmmss") + "_laporan_wawancara.xlsx";
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }

        [HttpPost("UploadAbsensiObservasi", Name = "UploadAbsensiObservasi")]
        public ResponseModel<string> UploadAbsensiObservasi(IFormFile FileUpload)
        {
            if (FileUpload == null || FileUpload.Length == 0)
                return new ResponseModel<string> { IsSuccess = false, Data = null, ReturnMessage = "file tidak ada" };

            Tools.ExcelTool excel = new Tools.ExcelTool();
            List<PpdbAbsensiSheetModel> ret = excel.ReadPpdbAbsensi(FileUpload, out string oMessage);
            if (!string.IsNullOrEmpty(oMessage))
                return new ResponseModel<string> { IsSuccess = false, ReturnMessage = oMessage };
            oMessage = ppdbService.UploadAbsensiObservasi(IdUser, ret);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = null,
            };
        }
        [HttpPost("UploadAbsensiWawancara", Name = "UploadAbsensiWawancara")]
        public ResponseModel<string> UploadAbsensiWawancara(IFormFile FileUpload)
        {
            if (FileUpload == null || FileUpload.Length == 0)
                return new ResponseModel<string> { IsSuccess = false, Data = null, ReturnMessage = "file tidak ada" };

            Tools.ExcelTool excel = new Tools.ExcelTool();
            List<PpdbAbsensiSheetModel> ret = excel.ReadPpdbAbsensi(FileUpload, out string oMessage);
            if (!string.IsNullOrEmpty(oMessage))
                return new ResponseModel<string> { IsSuccess = false, ReturnMessage = oMessage };
            oMessage = ppdbService.UploadAbsensiWawancara(IdUser, ret);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = null,
            };
        }

        [HttpGet("GetPpdbSiswaDatas", Name = "GetPpdbSiswaDatas")]
        public ResponseModel<List<PpdbSiswaModel>> GetPpdbSiswaDatas(int Status)
        {
            var ret = ppdbService.GetPpdbSiswaDatas(IdUser, Status, out string oMessage);
            return new ResponseModel<List<PpdbSiswaModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("DownloadInputObservasi", Name = "DownloadInputObservasi")]
        public ActionResult DownloadInputObservasi()
        {
            try
            {
                var ret = ppdbService.GetPpdbSiswaDatas(IdUser, 3, out string oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelPpdb excel = new Tools.ExcelPpdb();
                var filename = excel.GenerateTemplateInputObservasi(IdUser, ret, out oMessage);
                if (!string.IsNullOrEmpty(oMessage))
                    return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("{0}_{1}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmss"), "lap_input_observasi");
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }

        [HttpPost("UploadInputObservasi", Name = "UploadInputObservasi")]
        public ResponseModel<string> UploadInputObservasi(IFormFile FileUpload)
        {
            if (FileUpload == null || FileUpload.Length == 0)
                return new ResponseModel<string> { IsSuccess = false, Data = null, ReturnMessage = "file tidak ada" };

            Tools.ExcelPpdb excel = new Tools.ExcelPpdb();
            List<PpdbInputObservasiSheetModel> ret = excel.ReadFilePpdbInputObservasi(FileUpload, out string oMessage);
            if (!string.IsNullOrEmpty(oMessage))
                return new ResponseModel<string> { IsSuccess = false, ReturnMessage = oMessage };
            oMessage = ppdbService.UploadInputObservasiMassal(1, ret);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = null,
            };
        }

        #region PPDB DAFTAR PEMINAT
        [HttpGet("GetDaftarPeminatPpdbs", Name = "GetDaftarPeminatPpdbs")]
        public ResponseModel<List<PpdbDaftarPeminat>> GetDaftarPeminatPpdbs()
        {
            var ret = ppdbService.GetDaftarPeminatPpdbs(out string oMessage);
            return new ResponseModel<List<PpdbDaftarPeminat>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetDaftarPeminatPpdb", Name = "GetDaftarPeminatPpdb")]
        public ResponseModel<PpdbDaftarPeminat> GetDaftarPeminatPpdb(int IdPpdbDaftarPeminat)
        {
            var ret = ppdbService.GetDaftarPeminatPpdb(IdPpdbDaftarPeminat, out string oMessage);
            return new ResponseModel<PpdbDaftarPeminat>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        #endregion

        #region PPDB KONTEN
        [HttpGet("GetPpdbKontens", Name = "GetPpdbKontens")]
        public ResponseModel<List<PpdbKontenListModel>> GetPpdbKontens()
        {
            var ret = ppdbService.GetPpdbKontens(out string oMessage);
            return new ResponseModel<List<PpdbKontenListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetPpdbKonten", Name = "GetPpdbKonten")]
        public ResponseModel<PpdbKontenByModel> GetPpdbKonten(int IdPpdbKonten)
        {
            var ret = ppdbService.GetPpdbKonten(IdPpdbKonten, out string oMessage);
            return new ResponseModel<PpdbKontenByModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("AddPpdbKonten", Name = "AddPpdbKonten")]
        public ResponseModel<string> AddPpdbKonten(int IdJenisPpdbKonten, string Deskripsi)
        {
            var ret = ppdbService.AddPpdbKonten(IdJenisPpdbKonten, Deskripsi, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("EditPpdbKonten", Name = "EditPpdbKonten")]
        public ResponseModel<string> EditPpdbKonten(int IdPpdbKonten, string Deskripsi)
        {
            var ret = ppdbService.EditPpdbKonten(IdPpdbKonten, Deskripsi, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GenerateBaruNameTag", Name = "GenerateBaruNameTag")]
        public ResponseModel<string> GenerateBaruNameTag(int IdPpdbDaftar)
        {
            var ret = ppdbService.GenerateBaruNameTag(IdPpdbDaftar, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        #endregion
        [HttpGet("TestNametag", Name = "TestNametag")]
        public ResponseModel<string> TestNametag(int IdPpdbKonten, string Deskripsi)
        {
            PpdbDaftarKartuModel data = new();
            data.Nama = "123456789012345678901234567890";
            data.NamaPanggilan = "BANG SALIM";
            data.JalurPendaftaran = "Reguler";
            data.JenisKategoriPendaftaran = "Internal";
            data.IdPpdbDaftar = 202100201;
            data.Foto = "test.jpg";
            var ret = pdfService.GenerateNameTagSma(data, AppSetting.PathPpdbBerkas);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }

    }
}