﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Models;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Swg.Sch.Adm.Controllers
{
    [Route("api/[controller]")]
    public class TestsController : ControllerBase
    {
        private readonly ICoaService _coaService;
        private readonly IKeuRenPenService renPenService;
        private readonly IKeuProgKegService progKegService;
        private readonly IKeuPengajuanService pengajuanService;
        private readonly IKeuPtgjwbService ptgjwbService;
        private readonly int IdUser;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        public TestsController(
            IHttpContextAccessor httpContextAccessor,
            ICoaService coaService,
            IKeuRenPenService RenPenService,
            IKeuProgKegService ProgKegService,
            IKeuPengajuanService PengajuanService,
            IKeuPtgjwbService PtgjwbService
            )
        {
            _coaService = coaService;
            renPenService = RenPenService;
            progKegService = ProgKegService;
            pengajuanService = PengajuanService;
            ptgjwbService = PtgjwbService;
            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }
        }
        #region COA
        [HttpGet("GetJenisAkun", Name = "GetJenisAkun" + "Adm")]
        public ResponseModel<List<ComboModel>> GetJenisAkun()
        {
            var ret = _coaService.GetJenisAkun();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = ret != null,
                ReturnMessage = ret == null ? "Jenis Akun Belum Disetup" : string.Empty,
                Data = ret
            };
        }
        [HttpGet("GetJenisTransaksi", Name = "GetJenisTransaksi" + "Adm")]
        public ResponseModel<List<ComboModel>> GetJenisTransaksi()
        {
            var ret = _coaService.GetJenisTransaksi();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = ret != null,
                ReturnMessage = ret == null ? "Jenis Transaksi Belum Disetup" : string.Empty,
                Data = ret
            };
        }
        [HttpGet("GetAllCoas", Name = "GetAllCoas" + "Adm")]
        public ResponseModel<List<CoaModel>> GetAllCoas()
        {
            var ret = _coaService.GetCoas(out string oMessage);
            return new ResponseModel<List<CoaModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetCoasByJenisAkun", Name = "GetCoasByJenisAkun" + "Adm")]
        public ResponseModel<List<CoaModel>> GetCoasByJenisAkun(int IdJenisAkun)
        {
            var ret = _coaService.GetCoas(IdJenisAkun, out string oMessage);
            return new ResponseModel<List<CoaModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetCoaByKode", Name = "GetCoaByKode" + "Adm")]
        public ResponseModel<CoaModel> GetCoaByKode(string Kode)
        {
            var ret = _coaService.GetCoa(Kode, out string oMessage);
            return new ResponseModel<CoaModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetCoaById", Name = "GetCoaById" + "Adm")]
        public ResponseModel<CoaModel> GetCoaById(int IdCoa)
        {
            var ret = _coaService.GetCoa(IdCoa, out string oMessage);
            return new ResponseModel<CoaModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        #endregion
        #region Rencana Pendapatan
        [HttpPost("InitRenPen", Name = "InitRenPen" + "Adm")]
        public ResponseModel<string> InitRenPen(int IdTahunAjaran)
        {

            var ret = renPenService.InitRenPen(IdUser, IdTahunAjaran);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = string.Empty
            };
        }
        [HttpGet("GetCoaRenPens", Name = "GetCoaRenPens" + "Adm")]
        public ResponseModel<List<ComboModel>> GetCoaRenPens()
        {
            var ret = renPenService.GetCoas(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetRenPens", Name = "GetRenPens" + "Adm")]
        public ResponseModel<List<KeuUnitRenPenListModel>> GetRenPens(int IdTahunAjaran)
        {
            var ret = renPenService.GetRenPens(IdUser, IdTahunAjaran, out string oMessage);
            return new ResponseModel<List<KeuUnitRenPenListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetRenPenCoas", Name = "GetRenPenCoas" + "Adm")]
        public ResponseModel<List<KeuUnitRenPenCoaListModel>> GetRenPenCoas(int IdUnitRenPen)
        {
            var ret = renPenService.GetRenPenCoas(IdUser, IdUnitRenPen, out string oMessage);
            return new ResponseModel<List<KeuUnitRenPenCoaListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetRenPenCoa", Name = "GetRenPenCoa" + "Adm")]
        public ResponseModel<KeuUnitRenPenCoaModel> GetRenPenCoa(int IdUnitRenPen, int IdCoa)
        {
            var ret = renPenService.GetRenPenCoa(IdUnitRenPen, IdCoa, out string oMessage);
            return new ResponseModel<KeuUnitRenPenCoaModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("RenPenAdd", Name = "RenPenAdd" + "Adm")]
        public ResponseModel<string> RenPenAdd(int IdUnitRenPen, int IdCoa, double Rp)
        {
            var ret = renPenService.AddData(IdUser, IdUnitRenPen, IdCoa, Rp);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        [HttpPost("RenPenEdit", Name = "RenPenEdit" + "Adm")]
        public ResponseModel<string> RenPenEdit(int IdUnitRenPen, int IdCoa, double Rp)
        {
            var ret = renPenService.EditData(IdUser, IdUnitRenPen, IdCoa, Rp);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        [HttpPost("RenPenDelete", Name = "RenPenDelete" + "Adm")]
        public ResponseModel<string> RenPenDelete(int IdUnitRenPen, int IdCoa)
        {
            var ret = renPenService.DeleteData(IdUser, IdUnitRenPen, IdCoa);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        [HttpPost("RenPenApproveDataByManKeu", Name = "RenPenApproveDataByManKeu" + "Adm")]
        public ResponseModel<string> RenPenApproveDataByManKeu(int IdUnitRenPen, int StatusProses, string Catatan)
        {
            var ret = renPenService.ApproveDataByManKeu(IdUser, IdUnitRenPen, StatusProses, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        [HttpPost("RenPenApproveDataByDirektur", Name = "RenPenApproveDataByDirektur" + "Adm")]
        public ResponseModel<string> RenPenApproveDataByDirektur(int IdUnitRenPen, int StatusProses, string Catatan)
        {
            var ret = renPenService.ApproveDataByDirektur(IdUser, IdUnitRenPen, StatusProses, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        [HttpPost("RenPenSetSah", Name = "RenPenSetSah" + "Adm")]
        public ResponseModel<string> RenPenSetSah(int IdUnitRenPen)
        {
            var ret = renPenService.SetSah(IdUser, IdUnitRenPen);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        
        [HttpGet("DownloadRenPen", Name = "DownloadRenPen" + "Adm")]
        public ActionResult DownloadRenPen(int IdUnitRenPen)
        {
            try
            {

                var ret = renPenService.GetRenPenDownload(IdUser, IdUnitRenPen, out string oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelKeuTool excel = new Tools.ExcelKeuTool();

                var filename = excel.GenerateFileRenPen(ret, out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("renpen_{0}_{1}.xlsx", ret.NamaUnit, ret.TahunAjaran);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }
        #endregion
        #region Program Kegiatan

        [HttpGet("GetCoaProgKegs", Name = "GetCoaProgKegs" + "Adm")]
        public ResponseModel<List<ComboModel>> GetCoaProgKegs()
        {
            var ret = progKegService.GetCoas(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetUnitPrograms", Name = "GetUnitPrograms" + "Adm")]
        public ResponseModel<List<ComboModel>> GetUnitPrograms(int IdUnitProKeg)
        {
            var ret = progKegService.GetUnitPrograms(IdUnitProKeg, out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKeuProKegUnits", Name = "GetKeuProKegUnits" + "Adm")]
        public ResponseModel<List<KeuUnitListModel>> GetKeuProKegUnits(int IdTahunAjaran)
        {
            var ret = progKegService.GetUnits(IdUser, IdTahunAjaran, out string oMessage);
            return new ResponseModel<List<KeuUnitListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKeuProKegUnitPrograms", Name = "GetKeuProKegUnitPrograms" + "Adm")]
        public ResponseModel<List<KeuUnitProListModel>> GetKeuProKegUnitPrograms(int IdUnitProKeg)
        {
            var ret = progKegService.GetPrograms(IdUnitProKeg, out string oMessage);
            return new ResponseModel<List<KeuUnitProListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKeuProKegUnitKegiatans", Name = "GetKeuProKegUnitKegiatans" + "Adm")]
        public ResponseModel<List<KeuUnitProKegListModel>> GetKeuProKegUnitKegiatans(int IdUnitProKeg, int IdProgram)
        {
            var ret = progKegService.GetKegiatans(IdUser, IdUnitProKeg, IdProgram, out string oMessage);
            return new ResponseModel<List<KeuUnitProKegListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetKeuKegiatanWajibCoas", Name = "GetKeuKegiatanWajibCoas" + "Adm")]
        public ResponseModel<List<KeuUnitProKegWajibCoaListModel>> GetKeuKegiatanWajibCoas(int IdUnitProKegWajib)
        {
            var ret = progKegService.GetKegiatanWajibCoas(IdUser, IdUnitProKegWajib, out string oMessage);
            return new ResponseModel<List<KeuUnitProKegWajibCoaListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKeuKegiatanTambahanCoas", Name = "GetKeuKegiatanTambahanCoas" + "Adm")]
        public ResponseModel<List<KeuUnitProKegTambahanCoaListModel>> GetKeuKegiatanTambahanCoas(int IdUnitProKegTambahan)
        {
            var ret = progKegService.GetKegiatanTambahanCoas(IdUser, IdUnitProKegTambahan, out string oMessage);
            return new ResponseModel<List<KeuUnitProKegTambahanCoaListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKeuKegiatanWajibCoa", Name = "GetKeuKegiatanWajibCoa" + "Adm")]
        public ResponseModel<KeuUnitProKegWajibCoaListModel> GetKeuKegiatanWajibCoa(int IdUnitProkegWajibCoa)
        {
            var ret = progKegService.GetKegiatanWajibCoa(IdUnitProkegWajibCoa, out string oMessage);
            return new ResponseModel<KeuUnitProKegWajibCoaListModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKeuKegiatanTambahanCoa", Name = "GetKeuKegiatanTambahanCoa" + "Adm")]
        public ResponseModel<KeuUnitProKegTambahanCoaListModel> GetKeuKegiatanTambahanCoa(int IdUnitProkegWajibCoa)
        {
            var ret = progKegService.GetKegiatanTambahanCoa(IdUnitProkegWajibCoa, out string oMessage);
            return new ResponseModel<KeuUnitProKegTambahanCoaListModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("KegiatanTambahanAdd", Name = "KegiatanTambahanAdd" + "Adm")]
        public ResponseModel<int> KegiatanTambahanAdd(int IdUnitProKeg, int IdProgram, string NamaKegiatan, string Tanggal)
        {
            var oMessage = progKegService.KegiatanTambahanAdd(IdUser, IdUnitProKeg, IdProgram, NamaKegiatan, Tanggal, out int IdUnitProkegTambahan);
            return new ResponseModel<int>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = IdUnitProkegTambahan
            };
        }
        [HttpPost("KegiatanTambahanEdit", Name = "KegiatanTambahanEdit" + "Adm")]
        public ResponseModel<string> KegiatanTambahanEdit(int IdUnitProkegTambahan, string NamaKegiatan, string Tanggal)
        {
            var oMessage = progKegService.KegiatanTambahanEdit(IdUser, IdUnitProkegTambahan, NamaKegiatan, Tanggal);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("KegiatanTambahanDelete", Name = "KegiatanTambahanDelete" + "Adm")]
        public ResponseModel<string> KegiatanTambahanDelete(int IdUnitProkegTambahan)
        {
            var oMessage = progKegService.KegiatanTambahanDelete(IdUser, IdUnitProkegTambahan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("KegiatanWajibCoaAdd", Name = "KegiatanWajibCoaAdd" + "Adm")]
        public ResponseModel<string> KegiatanWajibCoaAdd([FromBody] KeuUnitProKegCoaWajibAddModel Data)
        {
            var oMessage = progKegService.KegiatanWajibCoaAdd(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("KegiatanWajibCoaEdit", Name = "KegiatanWajibCoaEdit" + "Adm")]
        public ResponseModel<string> KegiatanWajibCoaEdit([FromBody] KeuUnitProKegCoaWajibEditModel Data)
        {
            var oMessage = progKegService.KegiatanWajibCoaEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("KegiatanWajibCoaDelete", Name = "KegiatanWajibCoaDelete" + "Adm")]
        public ResponseModel<string> KegiatanWajibCoaDelete(int IdUnitProkegWajibCoa)
        {
            var oMessage = progKegService.KegiatanWajibCoaDelete(IdUser, IdUnitProkegWajibCoa);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("KegiatanTambahanCoaAdd", Name = "KegiatanTambahanCoaAdd" + "Adm")]
        public ResponseModel<string> KegiatanTambahanCoaAdd([FromBody] KeuUnitProKegCoaTambahanAddModel Data)
        {
            var oMessage = progKegService.KegiatanTambahanCoaAdd(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("KegiatanTambahanCoaEdit", Name = "KegiatanTambahanCoaEdit" + "Adm")]
        public ResponseModel<string> KegiatanTambahanCoaEdit([FromBody] KeuUnitProKegCoaTambahanEditModel Data)
        {
            var oMessage = progKegService.KegiatanTambahanCoaEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("KegiatanTambahanCoaDelete", Name = "KegiatanTambahanCoaDelete" + "Adm")]
        public ResponseModel<string> KegiatanTambahanCoaDelete(int IdUnitProkegTambahanCoa)
        {
            var oMessage = progKegService.KegiatanTambahanCoaDelete(IdUser, IdUnitProkegTambahanCoa);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpGet("DownloadProKeg", Name = "DownloadProKeg" + "Adm")]
        public ActionResult DownloadProKeg(int IdUnitProkeg)
        {
            try
            {

                var ret = progKegService.GetUnitDownload(IdUser, IdUnitProkeg, out string oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelKeuTool excel = new Tools.ExcelKeuTool();

                var filename = excel.GenerateFileProKeg(ret, out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("prokeg_{0}_{1}.xlsx", ret.NamaUnit, ret.TahunAjaran);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }
        [HttpPost("ProKegApproveByKaUnit", Name = "ProKegApproveByKaUnit" + "Adm")]
        public ResponseModel<string> ProKegApproveByKaUnit(int IdUnitProkeg, int StatusProses, string Catatan)
        {
            var oMessage = progKegService.ApproveByKaUnit(IdUser, IdUnitProkeg, StatusProses, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("ProKegApproveByManUnit", Name = "ProKegApproveByManUnit" + "Adm")]
        public ResponseModel<string> ProKegApproveByManUnit(int IdUnitProkeg, int StatusProses, string Catatan)
        {
            var oMessage = progKegService.ApproveByManUnit(IdUser, IdUnitProkeg, StatusProses, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("ProKegApproveByManKeu", Name = "ProKegApproveByManKeu" + "Adm")]
        public ResponseModel<string> ProKegApproveByManKeu(int IdUnitProkeg, int StatusProses, string Catatan)
        {
            var oMessage = progKegService.ApproveByManKeu(IdUser, IdUnitProkeg, StatusProses, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("ProKegApproveByDirektur", Name = "ProKegApproveByDirektur" + "Adm")]
        public ResponseModel<string> ProKegApproveByDirektur(int IdUnitProkeg, int StatusProses, string Catatan)
        {
            var oMessage = progKegService.ApproveByDirektur(IdUser, IdUnitProkeg, StatusProses, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("ProKegSetSah", Name = "ProKegSetSah" + "Adm")]
        public ResponseModel<string> ProKegSetSah(int IdUnitProkeg)
        {
            var oMessage = progKegService.SetSah(IdUser, IdUnitProkeg);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        #endregion
        #region COA PAGU
        [HttpGet("GetUnitCoaPagus", Name = "GetUnitCoaPagus" + "Adm")]
        public ResponseModel<List<KeuUnitCoaPaguModel>> GetUnitCoaPagus(int IdUnit, int IdTahunAjaran)
        {
            var ret = renPenService.GetUnitCoaPagus(IdUser, IdUnit, IdTahunAjaran, out string oMessage);
            return new ResponseModel<List<KeuUnitCoaPaguModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetUnitCoaPaguProKegs", Name = "GetUnitCoaPaguProKegs" + "Adm")]
        public ResponseModel<List<string>> GetUnitCoaPaguProKegs(int IdUnitCoaPagu)
        {
            var ret = renPenService.GetUnitCoaPaguProKegs(IdUser, IdUnitCoaPagu, out string oMessage);
            return new ResponseModel<List<string>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("EditCoaPagu", Name = "EditCoaPagu" + "Adm")]
        public ResponseModel<string> EditCoaPagu(int IdUnitCoaPagu, double RpPagu)
        {
            var oMessage = renPenService.EditCoaPagu(IdUser, IdUnitCoaPagu, RpPagu);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        #endregion

        #region Pengajuan
        [HttpGet("GetCoaPengajuans", Name = "GetCoaPengajuans" + "Adm")]
        public ResponseModel<List<ComboModel>> GetCoaPengajuans()
        {
            var ret = pengajuanService.GetCoas(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetPengajuans", Name = "GetPengajuans" + "Adm")]
        public ResponseModel<List<KeuPengajuanListModel>> GetPengajuans(int IdTahunAjaran)
        {
            var ret = pengajuanService.GetPengajuans(IdUser, IdTahunAjaran, out string oMessage);
            return new ResponseModel<List<KeuPengajuanListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetPengajuanPrograms", Name = "GetPengajuanPrograms" + "Adm")]
        public ResponseModel<List<ComboModel>> GetPengajuanPrograms()
        {
            var ret = pengajuanService.GetPrograms(IdUser, out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetProgramKegiatanWajibs", Name = "GetProgramKegiatanWajibs" + "Adm")]
        public ResponseModel<List<ComboModel>> GetProgramKegiatanWajibs(int IdTahunAjaran, int IdProgram)
        {
            var ret = pengajuanService.GetKegiatanWajibs(IdUser, IdTahunAjaran, IdProgram, out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetProgramKegiatanTambahans", Name = "GetProgramKegiatanTambahans" + "Adm")]
        public ResponseModel<List<ComboModel>> GetProgramKegiatanTambahans(int IdTahunAjaran, int IdProgram)
        {
            var ret = pengajuanService.GetKegiatanTambahans(IdUser, IdTahunAjaran, IdProgram, out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("SetCoaPengajuanWajib", Name = "SetCoaPengajuanWajib" + "Adm")]
        public ResponseModel<string> SetCoaPengajuanWajib(int IdUnitProKegWajib, string Nomor, string Tanggal)
        {
            var oMessage = pengajuanService.SetCoaPengajuanWajib(IdUser, IdUnitProKegWajib, Nomor, Tanggal);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("SetCoaPengajuanTambahan", Name = "SetCoaPengajuanTambahan" + "Adm")]
        public ResponseModel<string> SetCoaPengajuanTambahan(int IdUnitProKegTambahan, string Nomor, string Tanggal)
        {
            var oMessage = pengajuanService.SetCoaPengajuanTambahan(IdUser, IdUnitProKegTambahan, Nomor, Tanggal);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpGet("GetPengajuanCoas", Name = "GetPengajuanCoas" + "Adm")]
        public ResponseModel<List<KeuPengajuanCoaListModel>> GetPengajuanCoas(int IdPengajuan)
        {
            var ret = pengajuanService.GetPengajuanCoas(IdUser, IdPengajuan, out string oMessage);
            return new ResponseModel<List<KeuPengajuanCoaListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("PengajuanDelete", Name = "PengajuanDelete" + "Adm")]
        public ResponseModel<string> PengajuanDelete(int IdPengajuan)
        {
            var oMessage = pengajuanService.PengajuanDelete(IdUser, IdPengajuan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("PengajuanEdit", Name = "PengajuanEdit" + "Adm")]
        public ResponseModel<string> PengajuanEdit(int IdPengajuan, string Nomor, string Tanggal)
        {
            var oMessage = pengajuanService.PengajuanEdit(IdUser, IdPengajuan, Nomor, Tanggal);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("PengajuanCoaAdd", Name = "PengajuanCoaAdd" + "Adm")]
        public ResponseModel<string> PengajuanCoaAdd([FromBody] KeuPengajuanCoaAddModel Data)
        {
            var oMessage = pengajuanService.PengajuanCoaAdd(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("PengajuanCoaEdit", Name = "PengajuanCoaEdit" + "Adm")]
        public ResponseModel<string> PengajuanCoaEdit([FromBody] KeuPengajuanCoaEditModel Data)
        {
            var oMessage = pengajuanService.PengajuanCoaEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("PengajuanCoaDelete", Name = "PengajuanCoaDelete" + "Adm")]
        public ResponseModel<string> PengajuanCoaDelete(int IdPengajuanCoa)
        {
            var oMessage = pengajuanService.PengajuanCoaDelete(IdUser, IdPengajuanCoa);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("PengajuanApproveByKaUnit", Name = "PengajuanApproveByKaUnit" + "Adm")]
        public ResponseModel<string> PengajuanApproveByKaUnit(int IdPengajuan, int StatusProses, double RpRealisasi, string Catatan)
        {
            var oMessage = pengajuanService.ApproveByKaUnit(IdUser, IdPengajuan, StatusProses, RpRealisasi, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("PengajuanApproveByManUnit", Name = "PengajuanApproveByManUnit" + "Adm")]
        public ResponseModel<string> PengajuanApproveByManUnit(int IdPengajuan, int StatusProses, double RpRealisasi, string Catatan)
        {
            var oMessage = pengajuanService.ApproveByManUnit(IdUser, IdPengajuan, StatusProses, RpRealisasi, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("PengajuanApproveByManKeu", Name = "PengajuanApproveByManKeu" + "Adm")]
        public ResponseModel<string> PengajuanApproveByManKeu(int IdPengajuan, int StatusProses, double RpRealisasi, string Catatan)
        {
            var oMessage = pengajuanService.ApproveByManKeu(IdUser, IdPengajuan, StatusProses, RpRealisasi, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("PengajuanApproveByDirektur", Name = "PengajuanApproveByDirektur" + "Adm")]
        public ResponseModel<string> PengajuanApproveByDirektur(int IdPengajuan, int StatusProses, double RpRealisasi, string Catatan)
        {
            var oMessage = pengajuanService.ApproveByDirektur(IdUser, IdPengajuan, StatusProses, RpRealisasi, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("PengajuanSetRealisasi", Name = "PengajuanSetRealisasi" + "Adm")]
        public ResponseModel<string> PengajuanSetRealisasi(int IdPengajuan)
        {
            var oMessage = pengajuanService.SetRealisasi(IdUser, IdPengajuan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpGet("DownloadPengajuan", Name = "DownloadPengajuan" + "Adm")]
        public ActionResult DownloadPengajuan(int IdPengajuan)
        {
            try
            {

                var ret = pengajuanService.GetPengajuanDownload(IdUser, IdPengajuan, out string oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelKeuTool excel = new Tools.ExcelKeuTool();

                var filename = excel.GenerateFilePengajuan(ret, out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("pengajuan_{0}_{1}.xlsx", ret.NamaUnit, ret.TahunAjaran);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }
        #endregion
        [HttpGet("DownloadPtgjwb", Name = "DownloadPtgjwb" + "Adm")]
        public ActionResult DownloadPtgjwb(int IdPengajuan)
        {
            try
            {

                var ret = ptgjwbService.GetPertanggungjawabanDownload(IdUser, IdPengajuan, out string oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });

                Tools.ExcelKeuTool excel = new Tools.ExcelKeuTool();

                var filename = excel.GenerateFilePtgjwb(ret, out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home", new { ErrorMessage = oMessage });
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("ptgjwb_{0}_{1}.xlsx", ret.NamaUnit, ret.TahunAjaran);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }
    }
}
