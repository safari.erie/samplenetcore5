using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Models;
using Swg.Models.Constants;

namespace Swg.Sch.Adm.Controllers
{
    public class SiswasController : BaseController
    {
        private readonly ISiswaService _siswaService;
        private readonly ISchService _schService;
        private readonly IUnitService _unitService;
        private readonly int IdUser;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        public SiswasController(IHttpContextAccessor httpContextAccessor, ISiswaService siswaService, ISchService schService, IUnitService unitService)
        {
            _siswaService = siswaService;
            _schService = schService;
            _unitService = unitService;
            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }
        }
        public ResponseModel<List<ComboModel>> GetJenisPrestasiSiswa()
        {
            var ret = _siswaService.GetJenisPrestasiSiswa();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }
        public ResponseModel<List<ComboModel>> GetJenisKelamin()
        {
            var ret = _siswaService.GetJenisKelamin();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }
        public ResponseModel<List<ComboModel>> GetJenisPekerjaans()
        {
            var ret = _schService.GetJenisPekerjaans(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<ComboModel>> GetAgamas()
        {
            var ret = _schService.GetAgamas();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }
        public ResponseModel<List<SiswaListModel>> GetSiswasByUnit(int IdUnit)
        {
            var ret = _siswaService.GetSiswasByUnit(IdUnit, out string oMessage);
            return new ResponseModel<List<SiswaListModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<SiswaListModel>> GetSiswasByKelas(int IdKelas)
        {
            var ret = _siswaService.GetSiswasByKelas(IdKelas, out string oMessage);
            return new ResponseModel<List<SiswaListModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<SiswaListModel>> GetSiswasByKelasParalel(int IdKelasParalel)
        {
            var ret = _siswaService.GetSiswasByKelasParalel(IdKelasParalel, out string oMessage);
            return new ResponseModel<List<SiswaListModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<SiswaListModel>> GetSiswas()
        {
            var ret = _schService.GetSiswas(StatusDataConstant.Aktif, out string oMessage);
            return new ResponseModel<List<SiswaListModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<SiswaModel> GetSiswa(int IdSiswa)
        {
            var ret = _schService.GetSiswa(IdSiswa, out string oMessage);
            return new ResponseModel<SiswaModel>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<SiswaModel> GetSiswaByNis(string Nis)
        {
            var ret = _siswaService.GetSiswaByNis(Nis, out string oMessage);
            return new ResponseModel<SiswaModel>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> SiswaAdd(SiswaAddModel Data)
        {
            var ret = _siswaService.SiswaAdd(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> SiswaEdit(SiswaEditModel Data)
        {
            var ret = _siswaService.SiswaEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> SetSiswaInActive(int IdSiswa)
        {
            var ret = _siswaService.SetSiswaInActive(IdUser, IdSiswa);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<List<TahunAjaranModel>> GetTahunAjarans()
        {
            var ret = _schService.GetTahunAjarans(out string oMessage);
            return new ResponseModel<List<TahunAjaranModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        public ResponseModel<string> SetSiswaArchive(int IdTahunAjaran)
        {
            var ret = _siswaService.SetSiswaArchive(IdUser, IdTahunAjaran);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> SetSiswaReArchive(int IdTahunAjaran)
        {
            var ret = _siswaService.SetSiswaReArchive(IdUser, IdTahunAjaran);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<string> UploadSiswa(IFormFile FileExcel)
        {
            if (FileExcel == null || FileExcel.Length == 0)
                return new ResponseModel<string> { IsSuccess = false, Data = null, ReturnMessage = "file tidak ada" };

            Tools.ExcelTool excel = new Tools.ExcelTool();
            List<SheetSiswaModel> DataExcelSiswa = excel.ReadExcelSiswa(FileExcel, out string oMessage);
            if (!string.IsNullOrEmpty(oMessage))
                return new ResponseModel<string> { IsSuccess = false, ReturnMessage = oMessage };

            var ret = _siswaService.SiswaUpload(IdUser, DataExcelSiswa);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> UploadEditSiswa(IFormFile FileExcel)
        {
            if (FileExcel == null || FileExcel.Length == 0)
                return new ResponseModel<string> { IsSuccess = false, Data = null, ReturnMessage = "file tidak ada" };

            Tools.ExcelTool excel = new Tools.ExcelTool();
            List<SheetSiswaModel> DataExcelSiswa = excel.ReadExcelEditSiswa(FileExcel, out string oMessage);
            if (!string.IsNullOrEmpty(oMessage))
                return new ResponseModel<string> { IsSuccess = false, ReturnMessage = oMessage };

            var ret = _siswaService.SiswaEditUpload(IdUser, DataExcelSiswa);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> UploadTagihanSiswa(IFormFile FileUpload)
        {
            if (FileUpload == null || FileUpload.Length == 0)
                return new ResponseModel<string> { IsSuccess = false, Data = null, ReturnMessage = "file tidak ada" };

            Tools.ExcelTool excel = new Tools.ExcelTool();
            List<SiswaTagihanAddModel> DataExcelTagihan = excel.ReadSiswaTagihanFile(FileUpload, out string oMessage);
            if (!string.IsNullOrEmpty(oMessage))
                return new ResponseModel<string> { IsSuccess = false, ReturnMessage = oMessage };

            var ret = _siswaService.SiswaTagihanUpload(IdUser, DataExcelTagihan);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> UploadTagihanSiswaSusulan(IFormFile FileUpload)
        {
            if (FileUpload == null || FileUpload.Length == 0)
                return new ResponseModel<string> { IsSuccess = false, Data = null, ReturnMessage = "file tidak ada" };

            Tools.ExcelTool excel = new Tools.ExcelTool();
            List<SiswaTagihanAddModel> DataExcelTagihan = excel.ReadSiswaTagihanFile(FileUpload, out string oMessage);
            if (!string.IsNullOrEmpty(oMessage))
                return new ResponseModel<string> { IsSuccess = false, ReturnMessage = oMessage };

            var ret = _siswaService.SiswaTagihanUploadSusulan(IdUser, DataExcelTagihan);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }



        public ActionResult DowloadSiswasByUnit(int IdUnit)
        {
            try
            {
                var tahunPelajaran = _schService.GetTahunAjaran(out string oMessage);
                if (string.IsNullOrEmpty(oMessage))
                    tahunPelajaran = new TahunAjaranModel { Nama = "TP" };
                List<SiswaListModel> ret = new List<SiswaListModel>();
                if (IdUnit == 0)
                {
                    for (int i = 1; i <= 4; i++)
                    {
                        var data = _schService.GetSiswasByUnit(i, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home");
                        foreach (var item in data)
                        {
                            ret.Add(item);
                        }
                    }
                }
                else
                {
                    ret = _schService.GetSiswasByUnit(IdUnit, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return RedirectToAction("Error", "Home");
                }
                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateDaftarSiswa(IdUser, tahunPelajaran.Nama, ret, out oMessage);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("daftar_siswa_{0}.xlsx", tahunPelajaran.Nama);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home");
            }
        }

        public ActionResult DowloadTemplateTagihan(int Tahun, int Bulan)
        {
            try
            {
                string err = string.Empty;
                string BulanTahun = Bulan.ToString().PadLeft(2, '0') + "-" + Tahun.ToString();
                List<SiswaListModel> ret = new List<SiswaListModel>();
                for (int i = 1; i <= 4; i++)
                {
                    var data = _schService.GetSiswasByUnit(i, out err);
                    if (!string.IsNullOrEmpty(err)) return RedirectToAction("Error", "Home");
                    foreach (var item in data)
                    {
                        ret.Add(item);
                    }
                }
                Tools.ExcelTool excel = new Tools.ExcelTool();
                var filename = excel.GenerateTemplateTagihan(IdUser, BulanTahun, ret, out err);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("tagihan_siswa_{0}.xlsx", BulanTahun);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home");
            }
        }

        public ResponseModel<SiswaTagihanModel> GetSiswaTagihan(string Nis)
        {
            var ret = _siswaService.GetSiswaTagihan(Nis, out string oMessage);
            return new ResponseModel<SiswaTagihanModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        public ResponseModel<string> DeleteTagihanSiswa(int IdSiswa, int IdJenisTagihanSiswa, string Periode)
        {
            var ret = _schService.DeleteTagihanSiswa(IdSiswa, IdJenisTagihanSiswa, Periode);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<string> UpdateTagihanSiswa(int IdSiswa, int IdJenisTagihanSiswa, string Periode, double Rp)
        {
            var ret = _schService.UpdateTagihanSiswa(IdSiswa, IdJenisTagihanSiswa, Periode, Rp);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<string> UpdateTokenFcmSiswa(string Token, string UserAgent)
        {
            var ret = _siswaService.UpdateTokenFcmSiswa(IdUser, 1, Token, UserAgent, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<List<LaporanRiwayatTransaksiBankModel>> GetLaporanRiwayatTransaksiBank(string TanggalAwal, string TanggalAkhir)
        {
            var ret = _siswaService.GetLaporanRiwayatTransaksiBank(TanggalAwal, TanggalAkhir, out string oMessage);
            return new ResponseModel<List<LaporanRiwayatTransaksiBankModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<ResumeListSiswaTagihanModel> GetResumeSiswaTagihan(string TanggalAwal, string TanggalAkhir)
        {
            ResumeListSiswaTagihanModel ret = _siswaService.GetResumeSiswaTagihan(TanggalAwal, TanggalAkhir, out string oMessage);
            return new ResponseModel<ResumeListSiswaTagihanModel>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ActionResult DownloadDapodik(int IdUnit)
        {
            try
            {
                SheetSiswaViewProfileModel ret = _siswaService.GetSheetSiswaViewProfile(IdUser, IdUnit, out string oMessage);
                Tools.ExcelDapodik excel = new Tools.ExcelDapodik();
                var filename = excel.GenerateFileDapodikSiswa(IdUser, ret);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = $"dapodik_{_schService.ToSlug(ret.Unit)}.xlsx";// DateTime.Now.ToString("yyyyMMddHHmmss") + "_laporan_observasi.xlsx";
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }
    }
}


