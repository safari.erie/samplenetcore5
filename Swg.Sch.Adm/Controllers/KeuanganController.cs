using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Adm.Models;

namespace Swg.Sch.Adm.Controllers
{
    public class KeuanganController : BaseController
    {
        public IActionResult DaftarSatuan()
        {
            var jss = ModuleJs("DaftarSatuan");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult DaftarVolume()
        {
            var jss = ModuleJs("DaftarVolume");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult Coa()
        {
            var jss = ModuleJs("Coa");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult InputKegiatanUnit()
        {
            var jss = ModuleJs("InputKegiatanUnit");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult ProgramKegiatanSekolah()
        {
            var jss = ModuleJs("ProgramKegiatanSekolah");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult InputRencanaPendapatan()
        {
            var jss = ModuleJs("InputRencanaPendapatan");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult PaguAnggaran()
        {
            var jss = ModuleJs("PaguAnggaran");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult Penagihan()
        {
            var jss = ModuleJs("Penagihan");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult Penerimaan()
        {
            var jss = ModuleJs("Penerimaan");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult PengajuanBiaya()
        {
            var jss = ModuleJs("PengajuanBiaya");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult Pertanggungjawaban()
        {
            var jss = ModuleJs("Pertanggungjawaban");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult Laporan()
        {
            var jss = ModuleJs("Laporan");
            ViewBag.ModuleJs = jss;
            return View();
        }

        public List<LibraryJsModel> ModuleJs(string Module)
        {
            List<LibraryJsModel> js = new List<LibraryJsModel>();
            LibraryJsModel daftar_satuan_js = new LibraryJsModel
            {
                NameJs = "DaftarSatuan.js?n=1",
                TypeJs = "module",
                Path = "Keuangan"
            };
            LibraryJsModel daftar_volume_js = new LibraryJsModel
            {
                NameJs = "DaftarVolume.js?n=1",
                TypeJs = "module",
                Path = "Keuangan"
            };
            LibraryJsModel coa_js = new LibraryJsModel
            {
                NameJs = "Coa.js?n=1",
                TypeJs = "module",
                Path = "Keuangan"
            };
            LibraryJsModel input_kegiatan_unit_js = new LibraryJsModel
            {
                NameJs = "InputKegiatanUnit.js?n=1",
                TypeJs = "module",
                Path = "Keuangan"
            };
            LibraryJsModel program_kegiatan_sekolah_js = new LibraryJsModel
            {
                NameJs = "ProgramKegiatanSekolah.js?n=1",
                TypeJs = "module",
                Path = "Keuangan"
            };
            LibraryJsModel input_rencana_pendapatan_js = new LibraryJsModel
            {
                NameJs = "InputRencanaPendapatan.js?n=1",
                TypeJs = "module",
                Path = "Keuangan"
            };
            LibraryJsModel pagu_anggaran_js = new LibraryJsModel
            {
                NameJs = "PaguAnggaran.js?n=1",
                TypeJs = "module",
                Path = "Keuangan"
            };
            LibraryJsModel penagihan_js = new LibraryJsModel
            {
                NameJs = "Penagihan.js?n=1",
                TypeJs = "module",
                Path = "Keuangan"
            };
            LibraryJsModel penerimaan_js = new LibraryJsModel
            {
                NameJs = "Penerimaan.js?n=1",
                TypeJs = "module",
                Path = "Keuangan"
            };
            LibraryJsModel pengajuan_biaya_js = new LibraryJsModel
            {
                NameJs = "PengajuanBiaya.js?n=1",
                TypeJs = "module",
                Path = "Keuangan"
            };
            LibraryJsModel pertanggungjawaban_js = new LibraryJsModel
            {
                NameJs = "Pertanggungjawaban.js?n=1",
                TypeJs = "module",
                Path = "Keuangan"
            };
            LibraryJsModel laporan_js = new LibraryJsModel
            {
                NameJs = "Laporan.js?n=1",
                TypeJs = "module",
                Path = "Keuangan"
            };


            if (Module == "DaftarSatuan")
            {
                js.Add(daftar_satuan_js);
            } else if (Module == "DaftarVolume")
            {
                js.Add(daftar_volume_js);
            } else if (Module == "Coa")
            {
                js.Add(coa_js);
            } else if (Module == "InputKegiatanUnit")
            {
                js.Add(input_kegiatan_unit_js);
            } else if (Module == "ProgramKegiatanSekolah")
            {
                js.Add(program_kegiatan_sekolah_js);
            } else if (Module == "InputRencanaPendapatan")
            {
                js.Add(input_rencana_pendapatan_js);
            } else if (Module == "PaguAnggaran")
            {
                js.Add(pagu_anggaran_js);
            } else if (Module == "Penagihan")
            {
                js.Add(penagihan_js);
            } else if (Module == "Penerimaan")
            {
                js.Add(penerimaan_js);
            } else if (Module == "PengajuanBiaya")
            {
                js.Add(pengajuan_biaya_js);
            } else if (Module == "Pertanggungjawaban")
            {
                js.Add(pertanggungjawaban_js);
            }
            else if (Module == "Laporan")
            {
                js.Add(laporan_js);
            }
            return js;
        }
    }
}
