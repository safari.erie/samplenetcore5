﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Adm.Controllers
{
    public class ApplController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult User_Management()
        {
            var moduleJs = ModuleJs("User_Management");
            ViewBag.ModuleJs = moduleJs;
            return View();
        }
        public IActionResult Appl_Setting()
        {
            var moduleJs = ModuleJs("Appl_Setting");
            ViewBag.ModuleJs = moduleJs;
            return View();
        }
        public IActionResult Menu_Setting()
        {
            var moduleJs = ModuleJs("Menu_Setting");
            ViewBag.ModuleJs = moduleJs;
            return View();
        }
        public IActionResult Role_Setting()
        {
            var moduleJs = ModuleJs("Role_Setting");
            ViewBag.ModuleJs = moduleJs;
            return View();
        }
        public IActionResult Access_Setting()
        {
            var moduleJs = ModuleJs("Access_Setting");
            ViewBag.ModuleJs = moduleJs;
            return View();
        }

        public IActionResult Roles_fitur()
        {
            var moduleJs = ModuleJs("roles_fitur");
            ViewBag.ModuleJs = moduleJs;
            return View();
        }

        public List<AppJsModel> ModuleJs(string Module)
        {
            List<AppJsModel> mdl = new List<AppJsModel>();
            AppJsModel user_management_js = new AppJsModel
            {
                NameJs = "user_management.js",
                TypeJs = "user_management",
                Path = "Appl"
            };
            AppJsModel appl_setting_js = new AppJsModel
            {
                NameJs = "appl_setting.js",
                TypeJs = "appl_setting",
                Path = "Appl"
            };
            AppJsModel menu_setting_js = new AppJsModel
            {
                NameJs = "menu_setting.js",
                TypeJs = "menu_setting",
                Path = "Appl"
            };
            AppJsModel role_setting_js = new AppJsModel
            {
                NameJs = "role_setting.js",
                TypeJs = "role_setting",
                Path = "Appl"
            };
            AppJsModel access_setting_js = new AppJsModel
            {
                NameJs = "access_setting.js",
                TypeJs = "access_setting",
                Path = "Appl"
            };

            AppJsModel roles_fitur_list = new AppJsModel
            {
                NameJs = "roles_fitur_list.js",
                TypeJs = "access_setting",
                Path = "Appl"
            };
            AppJsModel roles_fitur_action = new AppJsModel
            {
                NameJs = "roles_fitur_action.js",
                TypeJs = "access_setting",
                Path = "Appl"
            };

            AppJsModel common_appls = new AppJsModel
            {
                NameJs = "common_appls.js",
                TypeJs = "access_setting",
                Path = "Appl"
            };

            if (Module == "User_Management")
            {
                mdl.Add(user_management_js);
            }
            else
            if (Module == "Appl_Setting")
            {
                mdl.Add(appl_setting_js);
            }
            else
            if (Module == "Menu_Setting")
            {
                mdl.Add(menu_setting_js);
            }
            else
            if (Module == "Role_Setting")
            {
                mdl.Add(role_setting_js);
            }
            else if (Module == "roles_fitur") { mdl.Add(common_appls); mdl.Add(roles_fitur_list); mdl.Add(roles_fitur_action); }
            else
            if (Module == "Access_Setting")
            {
                mdl.Add(access_setting_js);
            }
            return mdl;
        }

    }
}