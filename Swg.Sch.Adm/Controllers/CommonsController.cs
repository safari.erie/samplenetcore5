﻿using System;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Models;
using Swg.Services;

namespace Swg.Sch.Adm.Controllers
{
    public class CommonsController : BaseController
    {
        private readonly int IdUser;
        private readonly string Token = string.Empty;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        private readonly IDateTimeService dateTimeService;
        private readonly IUserAppService userAppService;
        private readonly ICommonService commonService;
        public CommonsController(
            IHttpContextAccessor httpContextAccessor,
            IDateTimeService DateTimeService,
            IUserAppService UserAppService,
            ICommonService CommonService)
        {
            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }
            dateTimeService = DateTimeService;
            userAppService = UserAppService;
            commonService = CommonService;
        }

        public ActionResult DownloadFile(string FileName)
        {
            if (string.IsNullOrEmpty(FileName))
                return View("NotFound");
            try
            {
                string pathUpload = AppSetting.PathApplFile;

                var splitFileName = FileName.Split(".");
                string extFileName = splitFileName[3];
                if (extFileName == "pdf")
                {
                    extFileName = "application/pdf";
                }
                else if (extFileName == "zip")
                {
                    extFileName = "application/zip";
                }
                else if (extFileName == "doc")
                {
                    extFileName = "application/msword";
                }
                string _fileName = Path.Combine(pathUpload, FileName);
                return File(new FileStream(_fileName, FileMode.Open), extFileName);
            }
            catch (Exception)
            {
                return View("NotFound");
            }

        }
        public ResponseModel<string> GetCurrentDate()
        {
            return new ResponseModel<string>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = dateTimeService.GetCurrdate().ToString("dd-MM-yyyy")
            };

        }
        public ResponseModel<string> SetFullName(string FirstName, string MiddleName, string LastName)
        {
            return new ResponseModel<string>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = userAppService.SetFullName(FirstName, MiddleName, LastName)
            };

        }
        public ResponseModel<string> EncryptString(string text)
        {
            return new ResponseModel<string>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = commonService.EncryptString(text)
            };

        }
        public ResponseModel<string> DecryptString(string cipherText)
        {
            return new ResponseModel<string>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = commonService.DecryptString(cipherText)
            };

        }

        public ResponseModel<bool> ValidateUsername(string username)
        {
            return new ResponseModel<bool>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = commonService.ValidateUsername(username)
            };

        }

        public ResponseModel<bool> ValidatePassword(string password)
        {
            return new ResponseModel<bool>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = commonService.ValidatePassword(password, out string oMessage)
            };

        }

        public ResponseModel<string> ValidateEmail(string email)
        {
            var ret = commonService.ValidateEmail(email, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = ret,
                ReturnMessage = ret==true?string.Empty:oMessage,
                Data = string.Empty
            };

        }

        public ResponseModel<string> SetAlamat(string AlTunjuk, string AlAlamat, string AlBlokNo, string AlRw, string AlRt, string Desa, string Kec, string Kab, string Prov)
        {
            return new ResponseModel<string>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = commonService.SetAlamat(AlTunjuk, AlAlamat, AlBlokNo,  AlRw, AlRt, Desa, Kec, Kab, Prov)
            };

        }

        public ResponseModel<string> Terbilang(decimal Rp)
        {
            return new ResponseModel<string>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = commonService.Terbilang(Rp)
            };

        }

    }
}