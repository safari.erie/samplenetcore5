﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Models;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Adm.Controllers
{
    [Route("api/[controller]")]
    public class ELearningsController : BaseController
    {
        private readonly IELearningService _eLearningService;
        private readonly IEskulService _eskulService;
        private readonly ISchService _schService;
        private readonly IKelasService _kelasService;
        private readonly IUnitService _unitService;

        private readonly int IdUser;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        public ELearningsController(IHttpContextAccessor httpContextAccessor, ISchService schService, IELearningService eLearningService,
        IEskulService EskulService, IKelasService KelasService, IUnitService UnitService)
        {
            _eLearningService = eLearningService;
            _eskulService = EskulService;
            _schService = schService;
            _kelasService = KelasService;
            _unitService = UnitService;
            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }
        }

        [HttpGet("GetKbmByPegawais", Name = "GetKbmByPegawais")]
        public ResponseModel<List<KbmGuruListModel>> GetKbmByPegawais(string Tanggal)
        {
            var ret = _eLearningService.GetKbmByPegawai(IdUser, Tanggal, out string oMessage);
            return new ResponseModel<List<KbmGuruListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetKbmByPegawai", Name = "GetKbmByPegawai")]
        public ResponseModel<KbmGuruModel> GetKbmByPegawai(int IdKbmMateri)
        {
            var ret = _eLearningService.GetKbmByPegawai(IdKbmMateri, out string oMessage);
            return new ResponseModel<KbmGuruModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetKbmTugasSiswas", Name = "GetKbmTugasSiswas")]
        public ResponseModel<List<KbmSiswaListModel>> GetKbmTugasSiswas(int IdKbmMateri)
        {
            var ret = _eLearningService.GetKbmTugasSiswas(IdKbmMateri, out string oMessage);
            return new ResponseModel<List<KbmSiswaListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("UpdateMateris", Name = "UpdateMateris")]
        public ResponseModel<string> UpdateMateris(List<int> IdKbmMateris, KbmUpdateMateriModel Data, IFormFile FileMateri, List<KbmUpdateSoalModel> FileSoals, List<IFormFile> MediaFiles, List<string> MediaUrls)
        {
            List<KbmUpdateMateriMediaModel> FileMedias = new List<KbmUpdateMateriMediaModel>();
            if (MediaFiles != null)
            {
                for (int i = 0; i < MediaFiles.Count; i++)
                {
                    FileMedias.Add(new KbmUpdateMateriMediaModel
                    {
                        IdJenisPathFile = 1,
                        FileMedia = MediaFiles[i],
                        NamaUrl = null,
                    });
                }
            }
            if (MediaUrls != null)
            {
                foreach (var item in MediaUrls)
                {
                    if (item != null)
                    {
                        FileMedias.Add(new KbmUpdateMateriMediaModel
                        {
                            IdJenisPathFile = 2,
                            FileMedia = null,
                            NamaUrl = item,
                        });
                    }
                }
            }

            var ret = _eLearningService.UpdateMateri(IdUser, IdKbmMateris, Data, FileMateri, FileSoals, FileMedias);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpGet("GetUnitPegawai", Name = "GetUnitPegawai")]
        public ResponseModel<PegawaiUnitModel> GetUnitPegawai()
        {
            var ret = _schService.GetUnitPegawai(IdUser, out string oMessage);
            return new ResponseModel<PegawaiUnitModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("UpdateNilai", Name = "UpdateNilai")]
        public ResponseModel<string> UpdateNilai(int IdKbmMateri, int IdSiswa, double NilaiAngka, string NilaiHuruf, int IsRemedial, string Catatan)
        {
            var ret = _eLearningService.UpdateNilai(IdUser, IdKbmMateri, IdSiswa, NilaiAngka, NilaiHuruf, IsRemedial, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpPost("UploadNilaiSiswa", Name = "UploadNilaiSiswa")]
        public ResponseModel<string> UploadNilaiSiswa(IFormFile FileUpload)
        {
            if (FileUpload == null || FileUpload.Length == 0)
                return new ResponseModel<string> { IsSuccess = false, Data = null, ReturnMessage = "file tidak ada" };

            Tools.ExcelTool excel = new Tools.ExcelTool();
            List<SheetNilaiSiswa> Data = excel.ReadExcelNilaiSiswa(FileUpload, out string oMessage);
            if (!string.IsNullOrEmpty(oMessage))
                return new ResponseModel<string> { IsSuccess = false, ReturnMessage = oMessage };

            var ret = _eLearningService.UploadNilaiSiswa(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        [HttpPost("UpdateAdbsensi", Name = "UpdateAdbsensi")]
        public ResponseModel<string> UpdateAdbsensi(int IdKbmMateri, string NamaUrl, List<int> IdSiswas)
        {
            var ret = _eLearningService.UpdateAdbsensi(IdUser, IdKbmMateri, NamaUrl, IdSiswas);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpPost("UploadJawabanSusulan", Name = "UploadJawabanSusulan")]
        public ResponseModel<string> UploadJawabanSusulan(int IdSiswa, int IdKbmMateri, string Catatan, IFormFile FileJawaban)
        {
            var ret = _eLearningService.UploadJawabanSusulan(IdUser, IdSiswa, IdKbmMateri, Catatan, FileJawaban);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        // [HttpGet("GetUnits", Name = "GetUnits")]
        // public ResponseModel<List<UnitReadModel>> GetUnits()
        // {
        //     var ret = _unitService.GetUnits(out string oMessage);
        //     return new ResponseModel<List<UnitReadModel>>
        //     {
        //         IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
        //         ReturnMessage = oMessage,
        //         Data = ret
        //     };
        // }

        // [HttpGet("GetKelass", Name = "GetKelass")]
        // public ResponseModel<List<KelasModel>> GetKelass(int IdUnit)
        // {
        //     var ret = _kelasService.GetKelass(IdUnit, out string oMessage);
        //     return new ResponseModel<List<KelasModel>>
        //     {
        //         IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
        //         ReturnMessage = oMessage,
        //         Data = ret
        //     };
        // }

        // [HttpGet("GetKelasParalels", Name = "GetKelasParalels")]
        // public ResponseModel<List<KelasParalelModel>> GetKelasParalels(int IdKelas)
        // {
        //     var ret = _kelasService.GetKelasParalels(IdKelas, out string oMessage);
        //     return new ResponseModel<List<KelasParalelModel>>
        //     {
        //         IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
        //         ReturnMessage = oMessage,
        //         Data = ret
        //     };
        // }


        #region Eskul
        [HttpGet("GetJilidQuran", Name = "GetJilidQuran")]
        public ResponseModel<List<ComboModel>> GetJilidQuran()
        {

            var ret = _eskulService.GetJilidQuran();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }

        [HttpGet("GetKelasQuranSiswa", Name = "GetKelasQuranSiswa")]
        public ResponseModel<KelasQuranSiswaModel> GetKelasQuranSiswa(int IdSiswa, string Tanggal)
        {
            var ret = _eskulService.GetKelasQuranSiswa(IdSiswa, Tanggal, out string oMessage);
            return new ResponseModel<KelasQuranSiswaModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetKelasQurans", Name = "GetKelasQurans")]
        public ResponseModel<List<KelasQuranListModel>> GetKelasQurans(string Tanggal)
        {
            var ret = _eskulService.GetKelasQurans(IdUser, Tanggal, out string oMessage);
            return new ResponseModel<List<KelasQuranListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetKelasQuranSiswas", Name = "GetKelasQuranSiswas")]
        public ResponseModel<List<KelasQuranSiswaListModel>> GetKelasQuranSiswas(int IdKelasQuran, string Tanggal)
        {
            var ret = _eskulService.GetKelasQuranSiswas(IdKelasQuran, Tanggal, out string oMessage);
            return new ResponseModel<List<KelasQuranSiswaListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetKelasQuranSiswaProgres", Name = "GetKelasQuranSiswaProgres")]
        public ResponseModel<KelasQuranSiswaProgresModel> GetKelasQuranSiswaProgres(int IdSiswa, int IdKelasQuran, string Tanggal)
        {
            var ret = _eskulService.GetKelasQuranSiswaProgres(IdSiswa, IdKelasQuran, Tanggal, out string oMessage);
            return new ResponseModel<KelasQuranSiswaProgresModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetKelasQuranSiswaUjian", Name = "GetKelasQuranSiswaUjian")]
        public ResponseModel<KelasQuranSiswaUjianModel> GetKelasQuranSiswaUjian(int IdSiswa, int IdKelasQuran, string Tanggal)
        {
            var ret = _eskulService.GetKelasQuranSiswaUjian(IdSiswa, IdKelasQuran, Tanggal, out string oMessage);
            return new ResponseModel<KelasQuranSiswaUjianModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("KelasQuranSiswaProgresAdd", Name = "KelasQuranSiswaProgresAdd")]
        public ResponseModel<string> KelasQuranSiswaProgresAdd(int IdSiswa, int IdKelasQuran, string Tanggal, int IdJenisJilidQuran, int Halaman, string Catatan)
        {
            var ret = _eskulService.KelasQuranSiswaProgresAdd(IdUser, IdSiswa, IdKelasQuran, Tanggal, IdJenisJilidQuran, Halaman, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = ret
            };
        }

        [HttpPost("KelasQuranSiswaUjianAdd", Name = "KelasQuranSiswaUjianAdd")]
        public ResponseModel<string> KelasQuranSiswaUjianAdd(int IdSiswa, int IdKelasQuran, string Tanggal, int IdJenisJilidQuran, string Hasil, string Nilai, string Catatan)
        {
            var ret = _eskulService.KelasQuranSiswaUjianAdd(IdUser, IdSiswa, IdKelasQuran, Tanggal, IdJenisJilidQuran, Hasil, Nilai, Catatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = ret
            };
        }

        [HttpPost("UpdateKelompokQuran", Name = "UpdateKelompokQuran")]
        public ResponseModel<string> UpdateKelompokQuran(KelasQuranModel Data)
        {
            var ret = _eLearningService.UpdateKelompokQuran(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpPost("UpdateKelompokQuranSiswa", Name = "UpdateKelompokQuranSiswa")]
        public ResponseModel<string> UpdateKelompokQuranSiswa(int IdKelasQuran, List<int> IdSiswas)
        {
            var ret = _eLearningService.UpdateKelompokQuranSiswa(IdUser, IdKelasQuran, IdSiswas);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }
        [HttpGet("GetKelompokQuran", Name = "GetKelompokQuran")]
        public ResponseModel<List<KelasQuranModel>> GetKelompokQuran()
        {
            var ret = _eLearningService.GetKelompokQuran(IdUser, out string oMessage);
            return new ResponseModel<List<KelasQuranModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKelompokQuranSiswa", Name = "GetKelompokQuranSiswa")]
        public ResponseModel<List<KelompokQuranSiswaModel>> GetKelompokQuranSiswa(int IdKelasQuran)
        {
            var ret = _eLearningService.GetKelompokQuranSiswa(IdKelasQuran, out string oMessage);
            return new ResponseModel<List<KelompokQuranSiswaModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        #endregion

        [HttpGet("DeleteFileMateri", Name = "DeleteFileMateri")]
        public ResponseModel<string> DeleteFileMateri(int IdKbmMateri)
        {
            var ret = _eLearningService.DeleteFileMateri(IdUser, IdKbmMateri);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpGet("DeleteFileSoal", Name = "DeleteFileSoal")]
        public ResponseModel<string> DeleteFileSoal(int IdKbmMateri, int IdJenisSoal)
        {
            var ret = _eLearningService.DeleteFileSoal(IdUser, IdKbmMateri, IdJenisSoal);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpGet("DeleteFileMedia", Name = "DeleteFileMedia")]
        public ResponseModel<string> DeleteFileMedia(int IdKbmMedia)
        {
            var ret = _eLearningService.DeleteFileMedia(IdUser, IdKbmMedia);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }



        [HttpGet("GetJenisEvaluasiHariansByUnit", Name = "GetJenisEvaluasiHariansByUnit")]
        public ResponseModel<EvalHarianUnitModel> GetJenisEvaluasiHariansByUnit(int IdUnit)
        {
            var ret = _eLearningService.GetJenisEvaluasiHariansByUnit(IdUnit, out string oMessage);
            return new ResponseModel<EvalHarianUnitModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        #region Evaluasi Harian Siswa
        [HttpPost("JenisEvaluasiHarianAddEdit", Name = "JenisEvaluasiHarianAddEdit")]
        public ResponseModel<string> JenisEvaluasiHarianAddEdit(EvalHarianModel Data)
        {
            var ret = _eLearningService.JenisEvaluasiHarianAddEdit(Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = ret
            };
        }
        [HttpGet("GetJenisEvaluasiHarians", Name = "GetJenisEvaluasiHarians")]
        public ResponseModel<List<EvalHarianModel>> GetJenisEvaluasiHarians()
        {
            var ret = _eLearningService.GetJenisEvaluasiHarians(out string oMessage);
            return new ResponseModel<List<EvalHarianModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("DeleteJenisEvaluasiHarian", Name = "DeleteJenisEvaluasiHarian")]
        public ResponseModel<string> DeleteJenisEvaluasiHarian(int IdJenisEvaluasiHarian)
        {
            var ret = _eLearningService.DeleteJenisEvaluasiHarian(IdJenisEvaluasiHarian);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }
        [HttpPost("AddEvalHarianUnit", Name = "AddEvalHarianUnit")]
        public ResponseModel<string> AddEvalHarianUnit(int IdUnit, List<int> IdJenisEvaluasiHarians)
        {
            var ret = _eLearningService.AddEvalHarianUnit(IdUnit, IdJenisEvaluasiHarians);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }
        #endregion Evaluasi Harian Siswa


        #region Evaluasi Harian Pegawai
        [HttpGet("GetJenisEvaluasiHariansByUnitPegawai", Name = "GetJenisEvaluasiHariansByUnitPegawai")]
        public ResponseModel<EvalHarianUnitPegawaiModel> GetJenisEvaluasiHariansByUnitPegawai(int IdUnit)
        {
            var ret = _eLearningService.GetJenisEvaluasiHariansByUnitPegawai(IdUnit, out string oMessage);
            return new ResponseModel<EvalHarianUnitPegawaiModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("JenisEvaluasiHarianAddEditPegawai", Name = "JenisEvaluasiHarianAddEditPegawai")]
        public ResponseModel<string> JenisEvaluasiHarianAddEditPegawai(EvalHarianPegawaiModel Data)
        {
            var ret = _eLearningService.JenisEvaluasiHarianAddEditPegawai(Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = ret
            };
        }
        [HttpGet("GetJenisEvaluasiHarianPegawais", Name = "GetJenisEvaluasiHarianPegawais")]
        public ResponseModel<List<EvalHarianPegawaiModel>> GetJenisEvaluasiHarianPegawais()
        {
            var ret = _eLearningService.GetJenisEvaluasiHarianPegawais(out string oMessage);
            return new ResponseModel<List<EvalHarianPegawaiModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("DeleteJenisEvaluasiHarianPegawai", Name = "DeleteJenisEvaluasiHarianPegawai")]
        public ResponseModel<string> DeleteJenisEvaluasiHarianPegawai(int IdJenisEvaluasiHarianPegawai)
        {
            var ret = _eLearningService.DeleteJenisEvaluasiHarianPegawai(IdJenisEvaluasiHarianPegawai);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }
        [HttpPost("AddEvalHarianUnitPegawai", Name = "AddEvalHarianUnitPegawai")]
        public ResponseModel<string> AddEvalHarianUnitPegawai(int IdUnit, List<int> IdJenisEvaluasiHarianPegawais)
        {
            var ret = _eLearningService.AddEvalHarianUnitPegawai(IdUnit, IdJenisEvaluasiHarianPegawais);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }
        #endregion Evaluasi Harian Pegawai



        [HttpGet("ResetMateriSoal", Name = "ResetMateriSoal")]
        public ResponseModel<string> ResetMateriSoal(int IdKbmMateri)
        {
            var ret = _eLearningService.ResetMateriSoal(IdUser, IdKbmMateri);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpGet("GetKbmsByKelas", Name = "GetKbmsByKelas")]
        public ResponseModel<List<SheetJadwalKbmModel>> GetKbmsByKelas(int IdKelompok, int IdKelas)
        {
            var ret = _eLearningService.GetKbmsByKelas(IdUser, IdKelompok, IdKelas, out string oMessage);
            return new ResponseModel<List<SheetJadwalKbmModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetKbmsByKelasParalelKelompok", Name = "GetKbmsByKelasParalelKelompok")]
        public ResponseModel<List<SheetJadwalKbmModel>> GetKbmsByKelasParalelKelompok(int IdKelasParalel, int IdKelompok)
        {
            var ret = _eLearningService.GetKbmsByKelasParalelKelompok(IdKelasParalel, IdKelompok, out string oMessage);
            return new ResponseModel<List<SheetJadwalKbmModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("UploadJadwalKbm", Name = "UploadJadwalKbm")]
        public ResponseModel<string> UploadJadwalKbm(IFormFile FileUpload)
        {
            if (FileUpload == null || FileUpload.Length == 0)
                return new ResponseModel<string> { IsSuccess = false, Data = null, ReturnMessage = "file tidak ada" };

            Tools.ExcelTool excel = new Tools.ExcelTool();
            List<SheetJadwalKbmModel> Data = excel.ReadJadwalKbm(FileUpload, out string oMessage);
            if (!string.IsNullOrEmpty(oMessage))
                return new ResponseModel<string> { IsSuccess = false, ReturnMessage = oMessage };

            var ret = _eLearningService.UploadJadwalKbm(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        [HttpGet("SetJadwalKelasParalel", Name = "SetJadwalKelasParalel")]
        public ResponseModel<string> SetJadwalKelasParalel(int IdKelompok, List<int> IdKelasParalels)
        {
            var ret = _eLearningService.SetJadwalKelasParalel(IdUser, IdKelompok, IdKelasParalels);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpGet("GetMapels", Name = "GetMapels")]
        public ResponseModel<List<MapelModel>> GetMapels()
        {
            var ret = _eLearningService.GetMapels(out string oMessage);
            return new ResponseModel<List<MapelModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetMapel", Name = "GetMapel")]
        public ResponseModel<MapelModel> GetMapel(int IdMapel)
        {
            var ret = _eLearningService.GetMapel(IdMapel, out string oMessage);
            return new ResponseModel<MapelModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("MapelAddEdit", Name = "MapelAddEdit")]
        public ResponseModel<string> MapelAddEdit(MapelModel Data)
        {
            var ret = _eLearningService.MapelAddEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = ret
            };
        }

        [HttpPost("MapelDelete", Name = "MapelDelete")]
        public ResponseModel<string> MapelDelete(int IdMapel)
        {
            var ret = _eLearningService.MapelDelete(IdUser, IdMapel);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = ret
            };
        }

        [HttpGet("GetHariLiburs", Name = "GetHariLiburs")]
        public ResponseModel<List<HariLiburModel>> GetHariLiburs()
        {
            var ret = _schService.GetHariLiburs(out string oMessage);
            return new ResponseModel<List<HariLiburModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetHariLibur", Name = "GetHariLibur")]
        public ResponseModel<HariLiburModel> GetHariLibur(string Tanggal)
        {
            var ret = _schService.GetHariLibur(Tanggal, out string oMessage);
            return new ResponseModel<HariLiburModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("HariLiburAddEdit", Name = "HariLiburAddEdit")]
        public ResponseModel<string> HariLiburAddEdit(HariLiburModel Data)
        {
            var ret = _schService.HariLiburAddEdit(Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = ret
            };
        }

        [HttpPost("HariLiburDelete", Name = "HariLiburDelete")]
        public ResponseModel<string> HariLiburDelete(HariLiburModel Data)
        {
            var ret = _schService.HariLiburDelete(Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = ret
            };
        }
        [HttpPost("AddHariLiburUnit", Name = "AddHariLiburUnit")]
        public ResponseModel<string> AddHariLiburUnit(int IdUnit, List<int> IdHariLiburs)
        {
            var ret = _schService.AddHariLiburUnit(IdUnit, IdHariLiburs);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }
        [HttpGet("GetHariLibursByUnit", Name = "GetHariLibursByUnit")]
        public ResponseModel<HariLiburUnitModel> GetHariLibursByUnit(int IdUnit)
        {
            var ret = _schService.GetHariLibursByUnit(IdUnit, out string oMessage);
            return new ResponseModel<HariLiburUnitModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("UploadJadwalKbmNextWeek", Name = "UploadJadwalKbmNextWeek")]
        public ResponseModel<string> UploadJadwalKbmNextWeek(IFormFile FileUpload)
        {
            if (FileUpload == null || FileUpload.Length == 0)
                return new ResponseModel<string> { IsSuccess = false, Data = null, ReturnMessage = "file tidak ada" };

            Tools.ExcelTool excel = new Tools.ExcelTool();
            List<SheetJadwalKbmModel> Data = excel.ReadJadwalKbmNextWeek(FileUpload, out string oMessage);
            if (!string.IsNullOrEmpty(oMessage))
                return new ResponseModel<string> { IsSuccess = false, ReturnMessage = oMessage };

            var ret = _eLearningService.SetJadwalKelasParalelNextWeek(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = ret
            };
        }
        [HttpGet("GetJadwalKbmsUnit", Name = "GetJadwalKbmsUnit")]
        public ResponseModel<KbmJadwalUnitModel> GetJadwalKbmsUnit(int IdUnit, string Tanggal)
        {
            var ret = _eLearningService.GetJadwalKbmsUnit(IdUser, IdUnit, Tanggal, out string oMessage);
            return new ResponseModel<KbmJadwalUnitModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("SetJadwalKelasParalelNextWeek", Name = "SetJadwalKelasParalelNextWeek")]
        public ResponseModel<string> SetJadwalKelasParalelNextWeek(int IdUser, int Kelompok, List<int> IdKelasParalels)
        {
            var ret = _eLearningService.SetJadwalKelasParalelNextWeek(IdUser, Kelompok, IdKelasParalels);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret) ? true : false,
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }

        [HttpPost("UploadKelompokQiroaty", Name = "UploadKelompokQiroaty")]
        public ResponseModel<string> UploadKelompokQiroaty(IFormFile FileUpload)
        {
            if (FileUpload == null || FileUpload.Length == 0)
                return new ResponseModel<string> { IsSuccess = false, Data = null, ReturnMessage = "file tidak ada" };

            Tools.ExcelTool excel = new Tools.ExcelTool();
            KelasQuranAllExcelModel Data = excel.ReadExcelKelompokQiroaty(FileUpload, out string oMessage);
            if (!string.IsNullOrEmpty(oMessage))
                return new ResponseModel<string> { IsSuccess = false, ReturnMessage = oMessage };

            var ret = _eLearningService.AddKelompokQuranExcel(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        [HttpPost("UploadKelompokQiroatyJadwal", Name = "UploadKelompokQiroatyJadwal")]
        public ResponseModel<string> UploadKelompokQiroatyJadwal(IFormFile FileUpload)
        {
            if (FileUpload == null || FileUpload.Length == 0)
                return new ResponseModel<string> { IsSuccess = false, Data = null, ReturnMessage = "file tidak ada" };

            Tools.ExcelTool excel = new Tools.ExcelTool();
            List<KelasQuranJadwalExcelModel> Data = excel.ReadExcelJadwalKelompokQiroaty(FileUpload, out string oMessage);
            if (!string.IsNullOrEmpty(oMessage))
                return new ResponseModel<string> { IsSuccess = false, ReturnMessage = oMessage };

            var ret = _eLearningService.AddKelompokQuranJadwalExcel(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        [HttpPost("UploadKelompokQiroatySiswa", Name = "UploadKelompokQiroatySiswa")]
        public ResponseModel<string> UploadKelompokQiroatySiswa(IFormFile FileUpload)
        {
            if (FileUpload == null || FileUpload.Length == 0)
                return new ResponseModel<string> { IsSuccess = false, Data = null, ReturnMessage = "file tidak ada" };

            Tools.ExcelTool excel = new Tools.ExcelTool();
            List<KelasQuranSiswaExcelModel> Data = excel.ReadExcelSiswaKelompokQiroaty(FileUpload, out string oMessage);
            if (!string.IsNullOrEmpty(oMessage))
                return new ResponseModel<string> { IsSuccess = false, ReturnMessage = oMessage };

            var ret = _eLearningService.AddKelompokQuranSiswaExcel(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
    }
}
