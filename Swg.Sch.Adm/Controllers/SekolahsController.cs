﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Swg.Models;
using Swg.Models.Constants;

namespace Swg.Sch.Adm.Controllers
{
    public class SekolahsController : BaseController
    {
        private readonly ISekolahService _sekolahService;
        private readonly ISchService _schService;
        private readonly int IdUser;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        public SekolahsController(IHttpContextAccessor httpContextAccessor, ISekolahService sekolahService, ISchService schService)
        {
            _sekolahService = sekolahService;
            _schService = schService;
            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }
        }
        public ResponseModel<List<ComboModel>> GetJenisVisiMisi()
        {
            var ret = _sekolahService.GetJenisVisiMisi();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }
        public ResponseModel<List<ComboModel>> GetJenisGaleri()
        {
            var ret = _sekolahService.GetJenisGaleri();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }
        public ResponseModel<List<ComboModel>> GetJenisPegawai()
        {
            var ret = _sekolahService.GetJenisPegawai();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }
        public ResponseModel<List<PegawaiModel>> GetPegawais()
        {
            var ret = _schService.GetPegawais(StatusDataConstant.Aktif, out string oMessage);
            return new ResponseModel<List<PegawaiModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<SekolahReadModel> GetSekolah()
        {
            var ret = _sekolahService.GetSekolah(out string oMessage);
            return new ResponseModel<SekolahReadModel>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<SekolahVisiMisiModel> GetSekolahVisiMisi(int IdSekolahVisiMisi)
        {
            var ret = _sekolahService.GetSekolahVisiMisi(IdSekolahVisiMisi, out string oMessage);
            return new ResponseModel<SekolahVisiMisiModel>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<SekolahVisiMisiModel>> GetSekolahVisiMisis()
        {
            var ret = _sekolahService.GetSekolahVisiMisis(IdUser, out string oMessage);
            return new ResponseModel<List<SekolahVisiMisiModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> SekolahVisiMisiAdd(SekolahVisiMisiAddModel Data)
        {
            var ret = _sekolahService.SekolahVisiMisiAdd(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<string> SekolahVisiMisiEdit(SekolahVisiMisiEditModel Data)
        {
            var ret = _sekolahService.SekolahVisiMisiEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<string> SekolahVisiMisiDelete(int IdSekolahVisiMisi)
        {
            var ret = _sekolahService.SekolahVisiMisiDelete(IdUser, IdSekolahVisiMisi);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<List<SekolahGaleriReadModel>> GetSekolahGaleris()
        {
            var ret = _sekolahService.GetSekolahGaleris(out string oMessage);
            return new ResponseModel<List<SekolahGaleriReadModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<SekolahGaleriReadModel> GetSekolahGaleri(int IdSekolahGaleri)
        {
            var ret = _sekolahService.GetSekolahGaleri(IdSekolahGaleri, out string oMessage);
            return new ResponseModel<SekolahGaleriReadModel>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> SekolahGaleriAdd(SekolahGaleriAddEditModel Data)
        {
            var ret = _sekolahService.SekolahGaleriAdd(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> SekolahGaleriEdit(SekolahGaleriAddEditModel Data)
        {
            var ret = _sekolahService.SekolahGaleriEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        public ResponseModel<string> SekolahGaleriDelete(int IdSekolahGaleri)
        {
            var ret = _sekolahService.SekolahGaleriDelete(IdUser, IdSekolahGaleri);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<string> SekolahEdit(SekolahAddEditModel Data)
        {
            var ret = _sekolahService.SekolahEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<SekolahKegiatanReadModel> GetSekolahKegiatan(int IdSekolahKegiatan)
        {
            var ret = _sekolahService.GetSekolahKegiatan(IdSekolahKegiatan, out string oMessage);
            return new ResponseModel<SekolahKegiatanReadModel>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<SekolahKegiatanReadModel>> GetSekolahKegiatans()
        {
            var ret = _sekolahService.GetSekolahKegiatans(out string oMessage);
            return new ResponseModel<List<SekolahKegiatanReadModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> SekolahKegiatanAdd(SekolahKegiatanAddEditModel Data)
        {
            var ret = _sekolahService.SekolahKegiatanAdd(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<string> SekolahKegiatanEdit(SekolahKegiatanAddEditModel Data)
        {
            var ret = _sekolahService.SekolahKegiatanEdit(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<string> SekolahKegiatanDelete(int IdSekolahKegiatan)
        {
            var ret = _sekolahService.SekolahKegiatanDelete(IdUser, IdSekolahKegiatan);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        #region BANNER
        public ResponseModel<List<BannerModel>> GetBanners()
        {
            List<BannerModel> ret = _schService.GetBanners(null, out string oMessage);
            return new ResponseModel<List<BannerModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> AddBanner(BannerAddEditModel Data)
        {
            string ret = _schService.AddBanner(IdUser, Data, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> UpdateBanner(BannerAddEditModel Data)
        {
            string ret = _schService.UpdateBanner(IdUser, Data, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> DeleteBanner(int IdBanner)
        {
            string ret = _schService.DeleteBanner(IdBanner, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        #endregion
        #region KALENDER PENDIDIKAN

        public ResponseModel<List<KalenderPendidikanModel>> GetKalenderPendidikans()
        {
            List<KalenderPendidikanModel> ret = _sekolahService.GetKalenderPendidikans(out string oMessage);
            return new ResponseModel<List<KalenderPendidikanModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<KalenderPendidikanModel> GetKalenderPendidikan(int IdKalenderPendidikan)
        {
            KalenderPendidikanModel ret = _sekolahService.GetKalenderPendidikan(IdKalenderPendidikan, out string oMessage);
            return new ResponseModel<KalenderPendidikanModel>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> KalenderPendidikanAdd(KalenderPendidikanAddEditModel Data)
        {
            string ret = _sekolahService.KalenderPendidikanAdd(Data, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> KalenderPendidikanEdit(KalenderPendidikanAddEditModel Data)
        {
            string ret = _sekolahService.KalenderPendidikanEdit(Data, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> KalenderPendidikanDelete(int IdKalenderPendidikan)
        {
            string ret = _sekolahService.KalenderPendidikanDelete(IdKalenderPendidikan, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        #endregion
        #region SEKOLAH FASILITAS
        public ResponseModel<List<SekolahFasilitasModel>> GetSekolahFasilitass()
        {
            List<SekolahFasilitasModel> ret = _sekolahService.GetSekolahFasilitass(out string oMessage);
            return new ResponseModel<List<SekolahFasilitasModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<SekolahFasilitasModel> GetSekolahFasilitas(int IdSekolahFasilitas)
        {
            SekolahFasilitasModel ret = _sekolahService.GetSekolahFasilitas(IdSekolahFasilitas, out string oMessage);
            return new ResponseModel<SekolahFasilitasModel>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> SekolahFasilitasAdd(SekolahFasilitasAddEditModel Data)
        {
            string ret = _sekolahService.SekolahFasilitasAdd(Data, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> SekolahFasilitasEdit(SekolahFasilitasAddEditModel Data)
        {
            string ret = _sekolahService.SekolahFasilitasEdit(Data, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> SekolahFasilitasDelete(int IdSekolahFasilitas)
        {
            string ret = _sekolahService.SekolahFasilitasDelete(IdSekolahFasilitas, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        #endregion
        #region SEKOLAH PRESTASI
        public ResponseModel<List<SekolahPrestasiModel>> GetSekolahPrestasis()
        {
            List<SekolahPrestasiModel> ret = _sekolahService.GetSekolahPrestasis(out string oMessage);
            return new ResponseModel<List<SekolahPrestasiModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<SekolahPrestasiModel> GetSekolahPrestasi(int IdSekolahPrestasi)
        {
            SekolahPrestasiModel ret = _sekolahService.GetSekolahPrestasi(IdSekolahPrestasi, out string oMessage);
            return new ResponseModel<SekolahPrestasiModel>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> SekolahPrestasiAdd(SekolahPrestasiAddEditModel Data)
        {
            string ret = _sekolahService.SekolahPrestasiAdd(IdUser, Data, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> SekolahPrestasiEdit(SekolahPrestasiAddEditModel Data)
        {
            string ret = _sekolahService.SekolahPrestasiEdit(IdUser, Data, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> SekolahPrestasiDelete(int IdSekolahPrestasi)
        {
            string ret = _sekolahService.SekolahPrestasiDelete(IdSekolahPrestasi, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        #endregion
        #region TESTIMONI
        public ResponseModel<List<TestimoniModel>> GetTestimonis()
        {
            List<TestimoniModel> ret = _schService.GetTestimonis(out string oMessage);
            return new ResponseModel<List<TestimoniModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<TestimoniModel> GetTestimoni(int IdTestimoni)
        {
            TestimoniModel ret = _schService.GetTestimoni(IdTestimoni, out string oMessage);
            return new ResponseModel<TestimoniModel>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> UpdateTestimoni(int IdTestimoni, string Nama, string Sebagai, string Pesan, string Url)
        {
            string ret = _schService.UpdateTestimoni(IdUser, IdTestimoni, Nama, Sebagai, Pesan, Url, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> DeleteTestimoni(int IdTestimoni)
        {
            string ret = _schService.DeleteTestimoni(IdTestimoni, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        #endregion
    }
}