﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Adm.Models;

namespace Swg.Sch.Adm.Controllers
{
    public class KelasController : BaseController
    {
        
        public IActionResult Index()
        {
            var jss = ModuleJs("Kelas");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult KelasParalel()
        {
            var jss = ModuleJs("KelasParalel");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public List<LibraryJsModel> ModuleJs(string Module)
        {
            List<LibraryJsModel> js = new List<LibraryJsModel>();
            LibraryJsModel kelas_js = new LibraryJsModel
            {
                NameJs = "Kelas.js?n=1",
                TypeJs = "module_adm",
                Path = "Kelas"
            };
            LibraryJsModel kelasparalel_js = new LibraryJsModel
            {
                NameJs = "KelasParalel.js?n=1",
                TypeJs = "module_adm",
                Path = "Kelas"
            };

            if (Module == "Kelas")
            {
                js.Add(kelas_js);
            }
            else if (Module == "KelasParalel")
            {
                js.Add(kelasparalel_js);
            }

            return js;
        }
    }
}