﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Adm.Models;

namespace Swg.Sch.Adm.Controllers
{
    public class HomeController : BaseController
    {
        public IActionResult Index()
        {
            var jss = ModuleJs("dashboard");
            ViewBag.ModuleJs = jss;
            return View();
        }

        public IActionResult Error(string ErrorMessage)
        {
            ViewBag.ErrorMessage = ErrorMessage;
            return View();
        }
        public List<LibraryJsModel> ModuleJs(string Module)
        {
            List<LibraryJsModel> js = new List<LibraryJsModel>();
            LibraryJsModel dashboard_js = new LibraryJsModel
            {
                NameJs = "Dashboard.js?n=1",
                TypeJs = "home",
                Path = "Home"
            };

            if (Module == "dashboard")
            {
                js.Add(dashboard_js);
            }
            return js;
        }
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
        [Route("Home/Error/{statusCode}")]
        public IActionResult HttpStatusCodeHandler(int statusCode)
        {
            if (statusCode == 404)
            {
                ViewBag.StatusCode = 404;
                ViewBag.Message = "Halaman Tidak Ditemukan";
            }
            else if (statusCode == 405)
            {
                ViewBag.StatusCode = 405;
                ViewBag.Message = "Method Not Allowed";
            }
            else if (statusCode == 500)
            {
                ViewBag.StatusCode = 500;
                ViewBag.Message = "Internal Server Error";
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return View("NotFound");
        }
    }
}
