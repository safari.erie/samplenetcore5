﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Adm.Models;

namespace Swg.Sch.Adm.Controllers
{

    public class LaporanController : BaseController
    {
        public IActionResult Materi()
        {
            var jss = ModuleJs("materi_tugas");
            ViewBag.ModuleJs = jss;
            return View();
        }

        public IActionResult KbmSiswa()
        {
            var jss = ModuleJs("KbmSiswa");
            ViewBag.ModuleJs = jss;
            return View();
        }

        public IActionResult MutabaahSiswa()
        {
            var jss = ModuleJs("MutabaahSiswa");
            ViewBag.ModuleJs = jss;
            return View();
        }


        public IActionResult PresensiSiswa()
        {
            var jss = ModuleJs("KehadiranSiswa");
            ViewBag.ModuleJs = jss;
            return View();
        }

        public IActionResult Qiroaty()
        {
            var jss = ModuleJs("Qiroaty");
            ViewBag.ModuleJs = jss;
            return View();
        }

        public IActionResult SlipGaji()
        {
            var jss = ModuleJs("SlipGaji");
            ViewBag.ModuleJs = jss;
            return View();
        }

        public IActionResult ProgressPegawai()
        {
            var jss = ModuleJs("ProgressPegawai");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult RiwayatBank()
        {
            var jss = ModuleJs("RiwayatBank");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult ResumeTagihanSiswa()
        {
            var jss = ModuleJs("ResumeTagihanSiswa");
            ViewBag.ModuleJs = jss;
            return View();
        }

        public List<LibraryJsModel> ModuleJs(string Module)
        {
            List<LibraryJsModel> js = new List<LibraryJsModel>();

            LibraryJsModel MateriTugas = new LibraryJsModel
            {
                NameJs = "ReportMateriTugas.js",
                TypeJs = "module_adm",
                Path = "Laporan"
            };
            LibraryJsModel KbmSiswa = new LibraryJsModel
            {
                NameJs = "KbmSiswa.js",
                TypeJs = "module_adm",
                Path = "Laporan"
            };

            LibraryJsModel KehadiranSiswa = new LibraryJsModel
            {
                NameJs = "KehadiranSiswa.js",
                TypeJs = "module_adm",
                Path = "Laporan"
            };

            LibraryJsModel MutabaahSiswa = new LibraryJsModel
            {
                NameJs = "MutabaahSiswa.js",
                TypeJs = "module_adm",
                Path = "Laporan"
            };

            LibraryJsModel Qiroaty = new LibraryJsModel
            {
                NameJs = "Qiroaty.js",
                TypeJs = "module_adm",
                Path = "Laporan"
            };
            LibraryJsModel SlipGaji = new LibraryJsModel
            {
                NameJs = "SlipGaji.js",
                TypeJs = "module_adm",
                Path = "Laporan"
            };
            LibraryJsModel ProgressPegawai = new LibraryJsModel
            {
                NameJs = "ProgressPegawai.js",
                TypeJs = "module_adm",
                Path = "Laporan"
            };
            LibraryJsModel RiwayatBank = new LibraryJsModel
            {
                NameJs = "RiwayatBank.js",
                TypeJs = "module_adm",
                Path = "Laporan"
            };
            LibraryJsModel ResumeTagihanSiswa = new LibraryJsModel
            {
                NameJs = "ResumeTagihanSiswa.js",
                TypeJs = "module_adm",
                Path = "Laporan"
            };

            if (Module == "materi_tugas")
            {
                js.Add(MateriTugas);
            }
            else if (Module == "KbmSiswa")
            {
                js.Add(KbmSiswa);
            }
            else if (Module == "KehadiranSiswa")
            {
                js.Add(KehadiranSiswa);
            }
            else if (Module == "MutabaahSiswa")
            {
                js.Add(MutabaahSiswa);
            }
            else if (Module == "Qiroaty")
            {
                js.Add(Qiroaty);
            }
            else if (Module == "SlipGaji")
            {
                js.Add(SlipGaji);
            }
            else if (Module == "ProgressPegawai")
            {
                js.Add(ProgressPegawai);
            }
            else if (Module == "RiwayatBank")
            {
                js.Add(RiwayatBank);
            }
            else if (Module == "ResumeTagihanSiswa")
            {
                js.Add(ResumeTagihanSiswa);
            }
            return js;

        }
    }


}