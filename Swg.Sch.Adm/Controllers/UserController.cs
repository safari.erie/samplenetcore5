﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Adm.Controllers;
using Swg.Sch.Adm.Models;

namespace SwgAcct.Web.Controllers
{
    public class UserController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login()
        {
            return View();
        }
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return Redirect("Login");
        }
        public IActionResult ResetPassword()
        {
            return View();
        }
        public IActionResult GantiPassword()
        {
            return View();
        }

    }
}