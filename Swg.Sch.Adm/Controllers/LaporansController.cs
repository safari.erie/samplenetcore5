﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Models;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Adm.Controllers
{
    [Route("api/[controller]")]
    public class LaporansController : BaseController
    {

        private readonly int IdUser;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        private readonly IELearningLaporanService eLearningLaporanService;
        private readonly IPegawaiService pegawaiService;
        public LaporansController(IHttpContextAccessor httpContextAccessor,
            IELearningLaporanService ELearningLaporanService, IPegawaiService PegawaiService
            )
        {
            eLearningLaporanService = ELearningLaporanService;
            pegawaiService = PegawaiService;
            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }
        }

        [HttpGet("GetElMateriTugas", Name = "GetElMateriTugas")]
        public ResponseModel<ElMateriTugasModel> GetElMateriTugas(string Tanggal)
        {
            var ret = eLearningLaporanService.GetElMateriTugas(IdUser, Tanggal, out string oMessage);
            return new ResponseModel<ElMateriTugasModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetElMateriTugasKelas", Name = "GetElMateriTugasKelas")]
        public ResponseModel<List<ElMateriTugasKelasModel>> GetElMateriTugasKelas(int IdUnit, string Tanggal)
        {
            var ret = eLearningLaporanService.GetElMateriTugasKelas(IdUser, IdUnit, Tanggal, out string oMessage);
            return new ResponseModel<List<ElMateriTugasKelasModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetElMateriTugasKelasParalel", Name = "GetElMateriTugasKelasParalel")]
        public ResponseModel<List<ElMateriTugasKelasParalelModel>> GetElMateriTugasKelasParalel(int IdKelas, string Tanggal)
        {
            var ret = eLearningLaporanService.GetElMateriTugasKelasParalel(IdUser, IdKelas, Tanggal, out string oMessage);
            return new ResponseModel<List<ElMateriTugasKelasParalelModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetElMateriTugasList", Name = "GetElMateriTugasList")]
        public ResponseModel<ElMateriTugasListModel> GetElMateriTugasList(int IdKelasParalel, string Tanggal)
        {
            var ret = eLearningLaporanService.GetElMateriTugasList(IdUser, IdKelasParalel, Tanggal, out string oMessage);
            return new ResponseModel<ElMateriTugasListModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }


        [HttpGet("GetElKbmSiswa", Name = "GetElKbmSiswa")]
        public ResponseModel<ElKbmSiswaModel> GetElKbmSiswa(string Tanggal)
        {
            var ret = eLearningLaporanService.GetElKbmSiswa(IdUser, Tanggal, out string oMessage);
            return new ResponseModel<ElKbmSiswaModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetElKbmSiswaKelas", Name = "GetElKbmSiswaKelas")]
        public ResponseModel<List<ElKbmSiswaKelasModel>> GetElKbmSiswaKelas(int IdUnit, string Tanggal)
        {
            var ret = eLearningLaporanService.GetElKbmSiswaKelas(IdUser, IdUnit, Tanggal, out string oMessage);
            return new ResponseModel<List<ElKbmSiswaKelasModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetElKbmSiswaKelasParalel", Name = "GetElKbmSiswaKelasParalel")]
        public ResponseModel<List<ElKbmSiswaKelasParalelModel>> GetElKbmSiswaKelasParalel(int IdKelas, string Tanggal)
        {
            var ret = eLearningLaporanService.GetElKbmSiswaKelasParalel(IdUser, IdKelas, Tanggal, out string oMessage);
            return new ResponseModel<List<ElKbmSiswaKelasParalelModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetElKbmSiswaList", Name = "GetElKbmSiswaList")]
        public ResponseModel<ElKbmSiswaListModel> GetElKbmSiswaList(int IdKelasParalel, string Tanggal)
        {
            var ret = eLearningLaporanService.GetElKbmSiswaList(IdUser, IdKelasParalel, Tanggal, out string oMessage);
            return new ResponseModel<ElKbmSiswaListModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetElAbsenSiswa", Name = "GetElAbsenSiswa")]
        public ResponseModel<ElAbsenSiswaModel> GetElAbsenSiswa(string Tanggal)
        {
            var ret = eLearningLaporanService.GetElAbsenSiswa(IdUser, Tanggal, out string oMessage);
            return new ResponseModel<ElAbsenSiswaModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetElAbsenSiswaKelas", Name = "GetElAbsenSiswaKelas")]
        public ResponseModel<List<ElAbsenSiswaKelasModel>> GetElAbsenSiswaKelas(int IdUnit, string Tanggal)
        {
            var ret = eLearningLaporanService.GetElAbsenSiswaKelas(IdUser, IdUnit, Tanggal, out string oMessage);
            return new ResponseModel<List<ElAbsenSiswaKelasModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetElAbsenSiswaKelasParalel", Name = "GetElAbsenSiswaKelasParalel")]
        public ResponseModel<List<ElAbsenSiswaKelasParalelModel>> GetElAbsenSiswaKelasParalel(int IdKelas, string Tanggal)
        {
            var ret = eLearningLaporanService.GetElAbsenSiswaKelasParalel(IdUser, IdKelas, Tanggal, out string oMessage);
            return new ResponseModel<List<ElAbsenSiswaKelasParalelModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetElAbsenSiswaMapel", Name = "GetElAbsenSiswaMapel")]
        public ResponseModel<List<ElAbsenSiswaMapelModel>> GetElAbsenSiswaMapel(int IdKelasParalel, string Tanggal)
        {
            var ret = eLearningLaporanService.GetElAbsenSiswaMapel(IdUser, IdKelasParalel, Tanggal, out string oMessage);
            return new ResponseModel<List<ElAbsenSiswaMapelModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetElAbsenSiswaList", Name = "GetElAbsenSiswaList")]
        public ResponseModel<ElAbsenSiswaMapelListModel> GetElAbsenSiswaList(int IdKelasParalel, int IdMapel, string Tanggal)
        {
            var ret = eLearningLaporanService.GetElAbsenSiswaList(IdUser, IdKelasParalel, IdMapel, Tanggal, out string oMessage);
            return new ResponseModel<ElAbsenSiswaMapelListModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetElEvalHarianList", Name = "GetElEvalHarianList")]
        public ResponseModel<List<ElEvalHarianListModel>> GetElEvalHarianList(int IdKelasParalel, string Tanggal)
        {
            var ret = eLearningLaporanService.GetElEvalHarianList(IdUser, IdKelasParalel, Tanggal, out string oMessage);
            return new ResponseModel<List<ElEvalHarianListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetElEvalHarian", Name = "GetElEvalHarian")]
        public ResponseModel<ElEvalHarianModel> GetElEvalHarian(string Tanggal)
        {
            var ret = eLearningLaporanService.GetElEvalHarian(IdUser, Tanggal, out string oMessage);
            return new ResponseModel<ElEvalHarianModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetElEvalHarianKelas", Name = "GetElEvalHarianKelas")]
        public ResponseModel<List<ElEvalHarianKelasModel>> GetElEvalHarianKelas(int IdUnit, string Tanggal)
        {
            var ret = eLearningLaporanService.GetElEvalHarianKelas(IdUser, IdUnit, Tanggal, out string oMessage);
            return new ResponseModel<List<ElEvalHarianKelasModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetElEvalHarianKelasParalel", Name = "GetElEvalHarianKelasParalel")]
        public ResponseModel<List<ElEvalHarianKelasParalelModel>> GetElEvalHarianKelasParalel(int IdKelas, string Tanggal)
        {
            var ret = eLearningLaporanService.GetElEvalHarianKelasParalel(IdUser, IdKelas, Tanggal, out string oMessage);
            return new ResponseModel<List<ElEvalHarianKelasParalelModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetElEvalHarianSiswa", Name = "GetElEvalHarianSiswa")]
        public ResponseModel<ElEvalHarianSiswaModel> GetElEvalHarianSiswa(int IdSiswa, string Tanggal, int HariKe)
        {
            var ret = eLearningLaporanService.GetElEvalHarianSiswa(IdUser, IdSiswa, Tanggal, HariKe, out string oMessage);
            return new ResponseModel<ElEvalHarianSiswaModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetGajiBulans", Name = "GetGajiBulans")]
        public ResponseModel<List<GajiPegawaiBulanModel>> GetGajiBulans(int Month, int Year)
        {

            var ret = pegawaiService.GetGajiBulans(IdUser, Month, Year, out string oMessage);
            return new ResponseModel<List<GajiPegawaiBulanModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetGajiBulanUnits", Name = "GetGajiBulanUnits")]
        public ResponseModel<List<GajiPegawaiUnitModel>> GetGajiBulanUnits(string Periode)
        {

            var ret = pegawaiService.GetGajiBulanUnits(IdUser, Periode, out string oMessage);
            return new ResponseModel<List<GajiPegawaiUnitModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetGajiBulanUnitPegawais", Name = "GetGajiBulanUnitPegawais")]
        public ResponseModel<List<PegawaiGajiListModel>> GetGajiBulanUnitPegawais(string Periode, int IdUnit)
        {

            var ret = pegawaiService.GetGajiBulanUnitPegawais(IdUser, Periode, IdUnit, out string oMessage);
            return new ResponseModel<List<PegawaiGajiListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("LapGetSlipGaji", Name = "LapGetSlipGaji")]
        public ResponseModel<SlipGajiModel> LapGetSlipGaji(int IdPegawai, string Periode)
        {

            var ret = pegawaiService.GetSlipGaji(IdUser, IdPegawai, Periode, out string oMessage);
            return new ResponseModel<SlipGajiModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

    }
}