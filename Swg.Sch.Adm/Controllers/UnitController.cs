﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Adm.Models;

namespace Swg.Sch.Adm.Controllers
{
    public class UnitController : BaseController
    {

        public IActionResult Index()
        {
            var jss = ModuleJs("Unit");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult Galeri()
        {
            var jss = ModuleJs("GaleriUnit");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult Kegiatan()
        {
            var jss = ModuleJs("UnitKegiatan");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult Ekskul()
        {
            var jss = ModuleJs("UnitEkskul");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult Banner()
        {
            var jss = ModuleJs("UnitBanner");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult UnitPegawai()
        {
            var jss = ModuleJs("UnitPegawai");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public List<LibraryJsModel> ModuleJs(string Module)
        {
            List<LibraryJsModel> js = new List<LibraryJsModel>();
            LibraryJsModel galeriunit_js = new LibraryJsModel
            {
                NameJs = "GaleriUnit.js",
                TypeJs = "module_adm",
                Path = "Unit"
            };
            LibraryJsModel unit_js = new LibraryJsModel
            {
                NameJs = "Unit.js",
                TypeJs = "module_adm",
                Path = "Unit"
            };
            LibraryJsModel unitkegiatan_js = new LibraryJsModel
            {
                NameJs = "UnitKegiatan.js",
                TypeJs = "module_adm",
                Path = "Unit"
            };
            LibraryJsModel unitekskul_js = new LibraryJsModel
            {
                NameJs = "UnitEkskul.js",
                TypeJs = "module_adm",
                Path = "Unit"
            };
            LibraryJsModel unitbanner_js = new LibraryJsModel
            {
                NameJs = "UnitBanner.js",
                TypeJs = "module_adm",
                Path = "Unit"
            };
            LibraryJsModel unitpegawai_js = new LibraryJsModel
            {
                NameJs = "UnitPegawai.js",
                TypeJs = "module_adm",
                Path = "Unit"
            };

            if (Module == "GaleriUnit")
            {
                js.Add(galeriunit_js);
            }
            else if (Module == "Unit")
            {
                js.Add(unit_js);
            }
            else if (Module == "UnitKegiatan")
            {
                js.Add(unitkegiatan_js);
            }
            else if (Module == "UnitEkskul")
            {
                js.Add(unitekskul_js);
            }
            else if (Module == "UnitBanner")
            {
                js.Add(unitbanner_js);
            }
            else if (Module == "UnitPegawai")
            {
                js.Add(unitpegawai_js);
            }

            return js;
        }
    }
}