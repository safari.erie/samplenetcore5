﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Swg.Models;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Adm.Controllers
{
    public class KelassController : BaseController
    {
        private readonly IKelasService _kelasService;
        private readonly int IdUser;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        public KelassController(IHttpContextAccessor httpContextAccessor, IKelasService kelasService)
        {
            _kelasService = kelasService;
            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }
        }
        
        public ResponseModel<KelasModel> GetKelas(int IdKelas)
        {
            var ret = _kelasService.GetKelas(IdKelas, out string oMessage);
            return new ResponseModel<KelasModel>
            {
                IsSuccess = ret != null,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<KelasModel>> GetKelass(int IdUnit)
        {
            var ret = _kelasService.GetKelass(IdUnit, out string oMessage);
            return new ResponseModel<List<KelasModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> KelasAdd(int IdUnit, string Nama, string NamaSingkat)
        {
            var ret = _kelasService.KelasAdd(IdUser, IdUnit, Nama, NamaSingkat);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<string> KelasEdit(int IdKelas, string Nama, string NamaSingkat)
        {
            var ret = _kelasService.KelasEdit(IdUser, IdKelas, Nama, NamaSingkat);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<string> KelasDelete(int IdKelas)
        {
            var ret = _kelasService.KelasDelete(IdUser, IdKelas);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<KelasParalelModel> GetKelasParalel(int IdKelasParalel)
        {
            var ret = _kelasService.GetKelasParalel(IdKelasParalel, out string oMessage);
            return new ResponseModel<KelasParalelModel>
            {
                IsSuccess = ret != null,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<List<KelasParalelModel>> GetKelasParalels(int IdKelas)
        {
            var ret = _kelasService.GetKelasParalels(IdKelas, out string oMessage);
            return new ResponseModel<List<KelasParalelModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<string> KelasParalelAdd(int IdKelas, string Nama, string NamaSingkat)
        {
            var ret = _kelasService.KelasParalelAdd(IdUser, IdKelas, Nama, NamaSingkat);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<string> KelasParalelEdit(int IdKelasParalel, string Nama, string NamaSingkat)
        {
            var ret = _kelasService.KelasParalelEdit(IdUser, IdKelasParalel, Nama, NamaSingkat);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        public ResponseModel<string> KelasParalelDelete(int IdKelasParalel)
        {
            var ret = _kelasService.KelasParalelDelete(IdUser, IdKelasParalel);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
    }
}