using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Swg.Sch.Shared.ViewModels;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace Swg.Sch.Adm.Tools
{
    public class ExcelTool
    {
        private static readonly Dictionary<string, string> DictMapel = new Dictionary<string, string>()
        {
            {"qiroatinam","Qiroaty"},
            {"seni,fismor","Seni Budaya dan Fisika Motorik"},
            {"kognitifdanbahasa","Kognitif dan Basaha"},
            {"duha+tahfidzh","Duhda dan Tahfidzh"},
            {"qiro'aty","Qiroaty"},
            {"mentoring","Mentoring"},
            {"pjok","Pendidikan Jasmani dan Olahraga Kesehatan"},
            {"bahasaarab","Bahasa Arab"},
            {"pbsbk","PB SBK"},
            {"pbppkn","PB Pendidikan Pancasila dan Kewarganegaraan"},
            {"pbbahasaindonesia","PB Bahasa Indonesia"},
            {"english","Bahasa Inggris"},
            {"lifeskill","Life Skill"},
            {"pbmatematika","PB Matematika"},
            {"pai","Pendidikan Agama Islam"},
            {"pbsbdp","PB SBDP"},
            {"bahasaindonesia","Bahasa Indonesia"},
            {"matematika","Matematika"},
            {"pb","PB"},
            {"b.inggris/b.sunda","Bahasa Inggris/Basa Sunda"},
            {"ips","Ilmu Pengetahuan Sosial"},
            {"pkn","Pendidikan Kewarganegaraan"},
            {"ipa","Ilmu Pengetahuan Alam"},
            {"bahasainggris","Bahasa Inggris"},
            {"sb&prakarya","Seni Budaya dan Prakarya"},
            {"pendidikanagamaislam","Pendidikan Agama Islam"},
            {"pendidikankewarganegaraan","Pendidikan Kewarganegaraan"},
            {"sejarahindonesia","Sejarah Indonesia"},
            {"matematikadasar","Matematika Dasar"},
            {"senibudaya","Seni Budaya dan Prakarya"},
            {"pendidikanjasmaniolahragadankesehatan(pjok)","Pendidikan Jasmani dan Olahraga Kesehatan"},
            {"prakarya","Seni Budaya dan Prakarya"},
            {"matematikaminat","Matematika Minat"},
            {"fisika","Fisika"},
            {"kimia","Kimia"},
            {"biologi","Biologi"},
            {"dzikirpagi&riyadushshalihin","Dzikir Pagi dan Riradushshalihin"},
            {"bimbingankonseling","Bimbingan dan Konseling"},
            {"sosiologi","Sosiologi"},
            {"ekonomi","Ekonomi"},
            {"sejarahminat","Sejarah Minat"},
            {"geografi","Geografi"},
            {"senibudaya/prakarya","Seni Budaya dan Prakarya"},
        };
        private readonly string ServiceName = "Swg.Sch.Adm.Tools.ExcelTool.";
        public ExcelTool()
        {
        }
        #region private
        public string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }
        public DateTime StrToDateTime(string StrDate)
        {
            if (StrDate.Length == 10)
            {
                return DateTime.ParseExact(StrDate.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            else
            {
                return DateTime.ParseExact(StrDate.Replace("/", "-"), "dd-MM-yyyy HH24:mm:ss", CultureInfo.InvariantCulture);
            }
        }
        public string SetNoHandphone(string NoHp)
        {
            NoHp = NoHp.Replace("+62", "");
            NoHp = NoHp.Replace(" ", "");
            NoHp = NoHp.Replace("-", "");
            NoHp = NoHp.Trim();
            if (string.IsNullOrEmpty(NoHp)) return "-";
            if (NoHp.IndexOf('/') > 0)
                NoHp = NoHp.Substring(0, NoHp.IndexOf("/"));
            if (NoHp.Substring(0, 1) != "0") NoHp = "0" + NoHp;
            return NoHp;
        }
        public string GetColumnName(int index)
        {
            const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            var value = "";

            if (index >= letters.Length)
                value += letters[index / letters.Length - 1];

            value += letters[index % letters.Length];

            return value;
        }
        public string GetErrorMessage(string MethodeName, Exception ex)
        {
            string errMessage = ex.Message;
            if (ex.InnerException != null)
            {
                errMessage = ex.InnerException.Message;
                if (ex.InnerException.InnerException != null)
                {
                    errMessage = ex.InnerException.InnerException.Message;
                    if (ex.InnerException.InnerException.InnerException != null)
                    {
                        errMessage = ex.InnerException.InnerException.InnerException.Message;
                    }
                }
            }
            var lineNumber = 0;
            const string lineSearch = ":line ";
            var index = ex.StackTrace.LastIndexOf(lineSearch);
            if (index != -1)
            {
                var lineNumberText = ex.StackTrace.Substring(index + lineSearch.Length);
                if (int.TryParse(lineNumberText, out lineNumber))
                {
                }
            }
            return MethodeName + ", line: " + lineNumber + Environment.NewLine + "Error Message: " + errMessage;
        }

        public string GetCellErrorMessage(string SheetName, int row, int cell)
        {
            return string.Format("sheet {0} baris {1} kolom {2} salah", SheetName, row + 1, GetColumnName(cell));
        }
        public string GetStringValue(ISheet sheet, int rowNum, int cellNum, out string oMessage)
        {
            string err = GetCellErrorMessage(sheet.SheetName, rowNum, cellNum);
            try
            {
                oMessage = string.Empty;
                IRow row = sheet.GetRow(rowNum);
                if (row == null)
                {
                    oMessage = err + ": baris kosong";
                    return string.Empty;
                }
                ICell cell = row.GetCell(cellNum);
                if (cell == null)
                {
                    return string.Empty;
                }
                return cell.ToString();
            }
            catch (Exception ex)
            {
                oMessage = err + ": " + ex.Message;
                return null;
            }
        }
        public double? GetNumberValue(XSSFWorkbook xssfwb, ISheet sheet, int rowNum, int cellNum, out string oMessage)
        {
            string err = GetCellErrorMessage(sheet.SheetName, rowNum, cellNum);
            try
            {
                oMessage = string.Empty;

                IRow row = sheet.GetRow(rowNum);
                if (row == null)
                {
                    oMessage = err + ": baris kosong";
                    return null;
                }
                ICell cell = row.GetCell(cellNum);
                if (cell == null)
                {
                    return 0;
                }
                XSSFFormulaEvaluator formula = new XSSFFormulaEvaluator(xssfwb);
                if (cell.CellType == CellType.Formula)
                    formula.Evaluate(cell);
                return double.Parse(cell.NumericCellValue.ToString());
            }
            catch (Exception ex)
            {
                oMessage = err + ": " + ex.Message;
                return null;
            }
        }
        public string GetDateValue(XSSFWorkbook xssfwb, ISheet sheet, int rowNum, int cellNum, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;

                IRow row = sheet.GetRow(rowNum);
                if (row == null)
                {
                    oMessage = GetCellErrorMessage(sheet.SheetName, rowNum, cellNum);
                    return null;
                }
                ICell cell = row.GetCell(cellNum);
                if (cell == null)
                {
                    oMessage = GetCellErrorMessage(sheet.SheetName, rowNum, cellNum);
                    return null;
                }
                if (cell.CellType == CellType.String) return cell.ToString();

                if (DateUtil.IsCellDateFormatted(cell))
                {
                    DateTime date = cell.DateCellValue;
                    return date.ToString("dd-MM-yyyy");
                }
                else
                {
                    return cell.ToString();
                }
            }
            catch (Exception ex)
            {
                oMessage = GetCellErrorMessage(sheet.SheetName, rowNum, cellNum);
                oMessage += "->" + ex.Message;
                return null;
            }
        }
        public int? GetIntValue(XSSFWorkbook xssfwb, ISheet sheet, int rowNum, int cellNum, out string oMessage)
        {
            string err = GetCellErrorMessage(sheet.SheetName, rowNum, cellNum);
            try
            {
                oMessage = string.Empty;

                IRow row = sheet.GetRow(rowNum);
                if (row == null)
                {
                    oMessage = err + ": baris kosong";
                    return null;
                }
                ICell cell = row.GetCell(cellNum);
                if (cell == null)
                {
                    return 0;
                }
                XSSFFormulaEvaluator formula = new XSSFFormulaEvaluator(xssfwb);
                if (cell.CellType == CellType.Formula)
                    formula.Evaluate(cell);
                return int.Parse(cell.NumericCellValue.ToString());
            }
            catch (Exception ex)
            {
                oMessage = err + ": " + ex.Message;
                return null;
            }
        }

        public IFont SetFontStyle(XSSFWorkbook xssfwb)
        {
            XSSFFont font = (XSSFFont)xssfwb.CreateFont();
            font.FontHeightInPoints = 12;
            font.Boldweight = (short)FontBoldWeight.Bold;
            font.IsBold = true;

            IFont ifont = xssfwb.CreateFont();
            ifont.FontHeightInPoints = 12;
            ifont.IsItalic = false;
            return ifont;

        }
        public XSSFCellStyle SetTextStye(XSSFWorkbook xssfwb)
        {
            XSSFCellStyle TextStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
            TextStyle.BorderBottom = BorderStyle.Thin;
            TextStyle.BorderLeft = BorderStyle.Thin;
            TextStyle.BorderRight = BorderStyle.Thin;
            TextStyle.BorderTop = BorderStyle.Thin;
            TextStyle.DataFormat = (short)CellType.String;
            TextStyle.SetFont(SetFontStyle(xssfwb));
            return TextStyle;
        }
        public XSSFCellStyle SetTextStyleWithoutBorder(XSSFWorkbook xssfwb)
        {
            XSSFCellStyle TextStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
            TextStyle.BorderBottom = BorderStyle.None;
            TextStyle.BorderLeft = BorderStyle.None;
            TextStyle.BorderRight = BorderStyle.None;
            TextStyle.BorderTop = BorderStyle.None;
            TextStyle.DataFormat = (short)CellType.String;
            TextStyle.SetFont(SetFontStyle(xssfwb));
            return TextStyle;
        }

        public XSSFCellStyle SetRpStye(XSSFWorkbook xssfwb)
        {
            XSSFCellStyle RpStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
            RpStyle.DataFormat = xssfwb.CreateDataFormat().GetFormat("#,##0.00");
            RpStyle.BorderBottom = BorderStyle.Thin;
            RpStyle.BorderLeft = BorderStyle.Thin;
            RpStyle.BorderRight = BorderStyle.Thin;
            RpStyle.BorderTop = BorderStyle.Thin;
            RpStyle.Alignment = HorizontalAlignment.Right;
            RpStyle.SetFont(SetFontStyle(xssfwb));
            return RpStyle;
        }
        public XSSFCellStyle SetNumberStyeWithDecimal(XSSFWorkbook xssfwb, int numDec)
        {
            XSSFCellStyle NumberStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
            string format = "#,##0";
            for (int i = 0; i < numDec; i++)
            {
                if (i == 0)
                    format += ".";
                format += "0";
            }
            NumberStyle.DataFormat = xssfwb.CreateDataFormat().GetFormat(format);
            NumberStyle.BorderBottom = BorderStyle.Thin;
            NumberStyle.BorderLeft = BorderStyle.Thin;
            NumberStyle.BorderRight = BorderStyle.Thin;
            NumberStyle.BorderTop = BorderStyle.Thin;
            NumberStyle.Alignment = HorizontalAlignment.Right;
            NumberStyle.SetFont(SetFontStyle(xssfwb));
            return NumberStyle;
        }
        public XSSFCellStyle SetNumberStye(XSSFWorkbook xssfwb)
        {
            XSSFCellStyle NumberStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();

            NumberStyle.DataFormat = xssfwb.CreateDataFormat().GetFormat("#,##0");
            NumberStyle.BorderBottom = BorderStyle.Thin;
            NumberStyle.BorderLeft = BorderStyle.Thin;
            NumberStyle.BorderRight = BorderStyle.Thin;
            NumberStyle.BorderTop = BorderStyle.Thin;
            NumberStyle.Alignment = HorizontalAlignment.Right;
            NumberStyle.SetFont(SetFontStyle(xssfwb));
            return NumberStyle;
        }
        public XSSFCellStyle SetDoubleStyle(XSSFWorkbook xssfwb)
        {
            XSSFCellStyle RpStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
            RpStyle.DataFormat = xssfwb.CreateDataFormat().GetFormat("#,##0.00000000000000");
            RpStyle.BorderBottom = BorderStyle.Thin;
            RpStyle.BorderLeft = BorderStyle.Thin;
            RpStyle.BorderRight = BorderStyle.Thin;
            RpStyle.BorderTop = BorderStyle.Thin;
            RpStyle.Alignment = HorizontalAlignment.Right;
            RpStyle.SetFont(SetFontStyle(xssfwb));
            return RpStyle;
        }

        public string InitExcel(int IdUser, string FileName, string SheetName, out XSSFWorkbook xssfwb, out ISheet osheet, out string OutFileName)
        {
            xssfwb = null;
            osheet = null;
            OutFileName = string.Empty;
            try
            {
                string TemplateFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("FileTemplates/{0}.xlsx", FileName));
                OutFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, IdUser.ToString() + ".xlsx");
                if (File.Exists(OutFileName))
                    File.Delete(OutFileName);

                xssfwb = new XSSFWorkbook();
                Console.WriteLine(OutFileName);
                FileStream filex = new FileStream(TemplateFileName, FileMode.Open, FileAccess.Read);
                xssfwb = new XSSFWorkbook(filex);
                osheet = xssfwb.GetSheet(SheetName);
                if (osheet == null)
                {
                    return string.Format("sheet {0} tidak ada di file {1}", SheetName, TemplateFileName);
                }
                for (int i = xssfwb.NumberOfSheets - 1; i >= 0; i--)
                {
                    ISheet tmpSheet = xssfwb.GetSheetAt(i);
                    if (!tmpSheet.SheetName.Equals(SheetName))
                    {
                        Console.WriteLine(tmpSheet.SheetName);
                        xssfwb.RemoveSheetAt(i);
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string InitExcel(int IdUser, string SheetName, out XSSFWorkbook xssfwb, out ISheet osheet, out string OutFileName)
        {
            return InitExcel(IdUser, "template_laporan", SheetName, out xssfwb, out osheet, out OutFileName);
        }

        #endregion
        public List<SiswaTagihanAddModel> ReadSiswaTagihanFile(IFormFile file, out string oMessage)
        {
            string err = string.Empty;
            try
            {
                oMessage = string.Empty;
                if (file == null || file.Length == 0)
                {
                    oMessage = "file tidak sesuai";
                    return null;
                }
                string fileExtension = Path.GetExtension(file.FileName);
                if (fileExtension == ".xlsx")
                {
                    XSSFWorkbook xssfwb = new XSSFWorkbook();
                    Stream fileStream = file.OpenReadStream();
                    List<SiswaTagihanAddModel> ret = new List<SiswaTagihanAddModel>();
                    if (fileExtension == ".xlsx") { xssfwb = new XSSFWorkbook(fileStream); }
                    ISheet sheet = xssfwb.GetSheetAt(0);
                    string bulan = sheet.GetRow(1).GetCell(2).ToString();
                    for (int row = 4; row <= sheet.LastRowNum; row++)
                    {
                        IRow cells = sheet.GetRow(row);
                        if (cells != null)
                        {
                            string cekStringValue = GetStringValue(sheet, row, 1, out oMessage);
                            if (string.IsNullOrEmpty(cekStringValue))
                                continue;

                            SiswaTagihanAddModel m = new SiswaTagihanAddModel();
                            m.Bulan = bulan;
                            int cellNo = 1;
                            ICell cell = cells.GetCell(cellNo);
                            if (cell == null) { oMessage = GetCellErrorMessage(sheet.SheetName, row, cellNo); ; return null; }
                            m.KodeBayar = cell.ToString();

                            var strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Nis = strValue;

                            ++cellNo;
                            strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Nama = strValue;

                            ++cellNo;
                            ++cellNo;

                            var numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Spp1 = (double)numValue;

                            numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Boks1 = (double)numValue;

                            numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Ppdb1 = (double)numValue;

                            numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Jemputan1 = (double)numValue;

                            numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Katering1 = (double)numValue;

                            numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Komite1 = (double)numValue;

                            ++cellNo;

                            numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Spp2 = (double)numValue;

                            numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Boks2 = (double)numValue;

                            numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Ppdb2 = (double)numValue;

                            numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Jemputan2 = (double)numValue;

                            numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Katering2 = (double)numValue;

                            numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Komite2 = (double)numValue;
                            ret.Add(m);

                            ++cellNo;
                            ++cellNo;

                            strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Catatan = strValue;

                            strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Min = strValue;
                            strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Max = strValue;
                        }
                    }
                    return ret;
                }
                else
                {
                    oMessage = "format file salah";
                    return null;
                }
            }
            catch (Exception ex)
            {
                oMessage = ex.Message + Environment.NewLine + err;
                return null;
            }

        }
        private List<SheetSiswaModel> ReadSheetSiswa(ISheet sheet, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<SheetSiswaModel> ret = new List<SheetSiswaModel>();
                for (int row = 4; row <= sheet.LastRowNum; row++)
                {
                    SheetSiswaModel m = new SheetSiswaModel();
                    var strValue = GetStringValue(sheet, row, 0, out oMessage);
                    //if (!string.IsNullOrEmpty(oMessage)) return null;
                    //m.KelasParalel = strValue;

                    strValue = GetStringValue(sheet, row, 1, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.Nis = strValue;
                    if (m.Nis.Length > 10) m.Nis = m.Nis.Substring(0, 10);

                    strValue = GetStringValue(sheet, row, 2, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.Nisn = strValue;
                    if (m.Nisn.Length > 10) m.Nisn = m.Nisn.Substring(0, 10);

                    strValue = GetStringValue(sheet, row, 3, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.Nik = strValue;

                    strValue = GetStringValue(sheet, row, 4, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.Nama = strValue;

                    strValue = GetStringValue(sheet, row, 5, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.NamaPanggilan = strValue;

                    strValue = GetStringValue(sheet, row, 6, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.TempatLahir = strValue;

                    strValue = GetDateValue((XSSFWorkbook)sheet.Workbook, sheet, row, 7, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) strValue = "01-01-1900";
                    m.TanggalLahir = strValue;

                    strValue = GetStringValue(sheet, row, 8, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.Agama = strValue;

                    strValue = GetStringValue(sheet, row, 9, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.JenisKelamin = strValue;

                    strValue = GetStringValue(sheet, row, 10, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.NoHandphone = strValue;

                    strValue = GetStringValue(sheet, row, 11, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.NoDarurat = strValue;

                    strValue = GetStringValue(sheet, row, 12, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.Email = strValue;

                    strValue = GetStringValue(sheet, row, 13, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.AlamatTinggal = strValue;

                    strValue = GetStringValue(sheet, row, 14, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.AlamatOrtu = strValue;

                    strValue = GetStringValue(sheet, row, 15, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.NikIbu = strValue;

                    strValue = GetStringValue(sheet, row, 16, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.NamaIbu = strValue;

                    strValue = GetStringValue(sheet, row, 17, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.JenisPekerjaanIbu = strValue;
                    strValue = GetStringValue(sheet, row, 18, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.NamaInstansiIbu = strValue;

                    strValue = GetStringValue(sheet, row, 19, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.NikAyah = strValue;

                    strValue = GetStringValue(sheet, row, 20, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.NamaAyah = strValue;

                    strValue = GetStringValue(sheet, row, 21, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.JenisPekerjaanAyah = strValue;

                    strValue = GetStringValue(sheet, row, 22, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.NamaInstansiAyah = strValue;

                    strValue = GetStringValue(sheet, row, 23, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.EmailOrtu = strValue;

                    strValue = GetStringValue(sheet, row, 24, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.Unit = strValue;

                    strValue = GetStringValue(sheet, row, 25, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.Kelas = strValue;

                    strValue = GetStringValue(sheet, row, 26, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.KelasParalel = strValue;

                    strValue = GetStringValue(sheet, row, 27, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.KelasQuran = strValue;

                    ret.Add(m);
                }

                Console.WriteLine("siswa: " + ret.Count());
                return ret;

            }
            catch (Exception ex)
            {
                oMessage = ex.Message;
                return null;
            }
        }
        public List<SheetSiswaModel> ReadExcelSiswa(IFormFile file, out string oMessage)
        {
            Console.WriteLine("ReadExcelSiswa");
            try
            {
                List<SheetSiswaModel> ret = new List<SheetSiswaModel>();
                XSSFWorkbook xssfwb = new XSSFWorkbook();
                Stream fileStream = file.OpenReadStream();
                xssfwb = new XSSFWorkbook(fileStream);
                string sheetName = "Siswa";
                ISheet sheet = xssfwb.GetSheet(sheetName);
                if (sheet == null)
                {
                    oMessage = string.Format("sheet {0} tidak ada di file {1}", sheetName, file);
                    return null;
                }
                ret = ReadSheetSiswa(sheet, out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return null;
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage(ServiceName, ex);
                return null;
            }
        }
        private List<SheetSiswaModel> ReadSheetEditSiswa(ISheet sheet, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<SheetSiswaModel> ret = new List<SheetSiswaModel>();
                for (int row = 4; row <= sheet.LastRowNum; row++)
                {
                    SheetSiswaModel m = new SheetSiswaModel();

                    var strValue = GetStringValue(sheet, row, 1, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.Nis = strValue;
                    if (m.Nis.Length > 10) m.Nis = m.Nis.Substring(0, 10);

                    strValue = GetStringValue(sheet, row, 2, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.Nisn = strValue;
                    if (m.Nisn.Length > 10) m.Nisn = m.Nisn.Substring(0, 10);

                    strValue = GetStringValue(sheet, row, 3, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.Nik = strValue;

                    strValue = GetStringValue(sheet, row, 4, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.Nama = strValue;

                    strValue = GetStringValue(sheet, row, 5, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.NamaPanggilan = strValue;

                    strValue = GetStringValue(sheet, row, 6, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.TempatLahir = strValue;

                    strValue = GetDateValue((XSSFWorkbook)sheet.Workbook, sheet, row, 7, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) strValue = "01-01-1900";
                    m.TanggalLahir = strValue;

                    strValue = GetStringValue(sheet, row, 8, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.Agama = strValue;

                    strValue = GetStringValue(sheet, row, 9, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.JenisKelamin = strValue;

                    strValue = GetStringValue(sheet, row, 10, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.NoHandphone = strValue;

                    strValue = GetStringValue(sheet, row, 11, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.NoDarurat = strValue;

                    strValue = GetStringValue(sheet, row, 12, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.Email = strValue;

                    strValue = GetStringValue(sheet, row, 13, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.AlamatTinggal = strValue;

                    strValue = GetStringValue(sheet, row, 14, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.AlamatOrtu = strValue;

                    strValue = GetStringValue(sheet, row, 15, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.NikIbu = strValue;

                    strValue = GetStringValue(sheet, row, 16, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.NamaIbu = strValue;

                    strValue = GetStringValue(sheet, row, 17, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.JenisPekerjaanIbu = strValue;

                    strValue = GetStringValue(sheet, row, 18, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.NamaInstansiIbu = strValue;

                    strValue = GetStringValue(sheet, row, 19, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.NikAyah = strValue;

                    strValue = GetStringValue(sheet, row, 20, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.NamaAyah = strValue;

                    strValue = GetStringValue(sheet, row, 21, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.JenisPekerjaanAyah = strValue;

                    strValue = GetStringValue(sheet, row, 22, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.NamaInstansiAyah = strValue;

                    strValue = GetStringValue(sheet, row, 23, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.EmailOrtu = strValue;

                    strValue = GetStringValue(sheet, row, 24, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.Unit = strValue;

                    strValue = GetStringValue(sheet, row, 25, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.Kelas = strValue;

                    strValue = GetStringValue(sheet, row, 26, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.KelasParalel = strValue;

                    strValue = GetStringValue(sheet, row, 27, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.KelasQuran = strValue;

                    ret.Add(m);
                }

                Console.WriteLine("siswa: " + ret.Count());
                return ret;

            }
            catch (Exception ex)
            {
                oMessage = ex.Message;
                return null;
            }
        }

        public List<SheetSiswaModel> ReadExcelEditSiswa(IFormFile file, out string oMessage)
        {
            Console.WriteLine("ReadExcelEditSiswa");
            try
            {
                List<SheetSiswaModel> ret = new List<SheetSiswaModel>();
                XSSFWorkbook xssfwb = new XSSFWorkbook();
                Stream fileStream = file.OpenReadStream();
                xssfwb = new XSSFWorkbook(fileStream);
                ISheet sheet = xssfwb.GetSheetAt(0);
                if (sheet == null)
                {
                    oMessage = string.Format("sheet tidak ada");
                    return null;
                }
                ret = ReadSheetEditSiswa(sheet, out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return null;
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage(ServiceName, ex);
                return null;
            }
        }
        public List<SheetGajiPegawaiPreviousModel> ReadExcelGajiPegawaiPrevious(IFormFile file, out string oMessage)
        {
            string err = string.Empty;
            try
            {
                oMessage = string.Empty;
                if (file == null || file.Length == 0)
                {
                    oMessage = "file tidak sesuai";
                    return null;
                }
                string fileExtension = Path.GetExtension(file.FileName);
                if (fileExtension != ".xlsx") { oMessage = string.Format("format file {0} salah", fileExtension); return null; }

                XSSFWorkbook xssfwb = new XSSFWorkbook();
                Stream fileStream = file.OpenReadStream();
                List<SheetGajiPegawaiPreviousModel> ret = new List<SheetGajiPegawaiPreviousModel>();

                xssfwb = new XSSFWorkbook(fileStream);
                ISheet sheet = xssfwb.GetSheetAt(0);
                string bulan = sheet.GetRow(1).GetCell(2).ToString();
                for (int row = 4; row <= sheet.LastRowNum; row++)
                {
                    IRow cells = sheet.GetRow(row);
                    if (cells != null)
                    {
                        if (sheet.GetRow(row).GetCell(1) == null) continue;

                        SheetGajiPegawaiPreviousModel m = new SheetGajiPegawaiPreviousModel();
                        m.Bulan = bulan;
                        int cellNo = 1;
                        ICell cell = cells.GetCell(cellNo);
                        if (cell == null) { oMessage = GetCellErrorMessage(sheet.SheetName, row, cellNo); ; return null; }
                        m.Unit = cell.ToString();

                        var strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Nip = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Nama = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pendidikan = strValue;

                        strValue = GetDateValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.TanggalMasuk = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Golongan = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Status = strValue;

                        var numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Mkg = (int)numValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.JenisPegawai = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Jabatan = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.NamaInstitusiPendidikan = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Agama = strValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GT_Pokok = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GT_Operasional = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GT_Jabatan = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GT_Keluarga = (double)numValue;

                        ++cellNo;
                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GTT_Lembur = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GTT_Anomali = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GTT_Performa = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GTT_Bpjs = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GTT_Pensiun = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GTT_Thr = (double)numValue;

                        ++cellNo;
                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_Koreksi = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_Icat = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_DayCare = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_Koperasi = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_Absensi = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_Lembur = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_Bpjs = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_Peserta = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_Pensiun = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_Lainnya = (double)numValue;

                        ++cellNo;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_Koreksi = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_Lembur = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_InvalGuru = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_OverTime = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_Backup = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_LemburHari = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_LemburJam = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_Tunjangan = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_Bpap = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_Lainnya = (double)numValue;

                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = ex.Message + Environment.NewLine + err;
                return null;
            }

        }
        public List<SheetGajiPegawaiModel> ReadExcelGajiPegawai(IFormFile file, out string oMessage)
        {
            string err = string.Empty;
            try
            {
                oMessage = string.Empty;
                if (file == null || file.Length == 0)
                {
                    oMessage = "file tidak sesuai";
                    return null;
                }
                string fileExtension = Path.GetExtension(file.FileName);
                if (fileExtension != ".xlsx") { oMessage = string.Format("format file {0} salah", fileExtension); return null; }

                XSSFWorkbook xssfwb = new XSSFWorkbook();
                Stream fileStream = file.OpenReadStream();
                List<SheetGajiPegawaiModel> ret = new List<SheetGajiPegawaiModel>();

                xssfwb = new XSSFWorkbook(fileStream);
                ISheet sheet = xssfwb.GetSheetAt(0);
                string bulan = sheet.GetRow(1).GetCell(2).ToString();
                for (int row = 4; row <= sheet.LastRowNum; row++)
                {
                    IRow cells = sheet.GetRow(row);
                    if (cells != null)
                    {
                        if (sheet.GetRow(row).GetCell(1) == null) continue;

                        SheetGajiPegawaiModel m = new SheetGajiPegawaiModel();
                        m.Bulan = bulan;
                        int cellNo = 1;
                        ICell cell = cells.GetCell(cellNo);
                        if (cell == null) { oMessage = GetCellErrorMessage(sheet.SheetName, row, cellNo); ; return null; }
                        m.Unit = cell.ToString();

                        var strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Nip = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Nama = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pendidikan = strValue;

                        strValue = GetDateValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.TanggalMasuk = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Golongan = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Status = strValue;

                        var numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Mkg = (int)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GT_Pokok = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GT_Operasional = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GT_Jabatan = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GT_Keluarga = (double)numValue;

                        ++cellNo;
                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GTT_Lembur = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GTT_Anomali = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GTT_Performa = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GTT_Bpjs = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GTT_Pensiun = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.GTT_Thr = (double)numValue;

                        ++cellNo;
                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_Koreksi = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_Icat = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_DayCare = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_Koperasi = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_Absensi = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_Lembur = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_Bpjs = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_Peserta = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_Pensiun = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pot_Lainnya = (double)numValue;

                        ++cellNo;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_Koreksi = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_Lembur = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_InvalGuru = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_OverTime = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_Backup = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_LemburHari = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_LemburJam = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_Tunjangan = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_Bpap = (double)numValue;

                        numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Tam_Lainnya = (double)numValue;

                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = ex.Message + Environment.NewLine + err;
                return null;
            }

        }
        public List<SheetPegawaiModel> ReadExcelPegawai(IFormFile file, out string oMessage)
        {
            string err = string.Empty;
            try
            {
                oMessage = string.Empty;
                if (file == null || file.Length == 0)
                {
                    oMessage = "file tidak sesuai";
                    return null;
                }
                string fileExtension = Path.GetExtension(file.FileName);
                if (fileExtension != ".xlsx") { oMessage = string.Format("format file {0} salah", fileExtension); return null; }

                XSSFWorkbook xssfwb = new XSSFWorkbook();
                Stream fileStream = file.OpenReadStream();
                List<SheetPegawaiModel> ret = new List<SheetPegawaiModel>();

                xssfwb = new XSSFWorkbook(fileStream);
                ISheet sheet = xssfwb.GetSheetAt(0);
                for (int row = 4; row <= sheet.LastRowNum; row++)
                {
                    IRow cells = sheet.GetRow(row);
                    if (cells != null)
                    {
                        if (sheet.GetRow(row).GetCell(1) == null) continue;

                        SheetPegawaiModel m = new SheetPegawaiModel();
                        int cellNo = 0;
                        ICell cell = cells.GetCell(cellNo);
                        if (cell == null) { oMessage = GetCellErrorMessage(sheet.SheetName, row, cellNo); ; return null; }

                        var idPegawai = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.IdPegawai = (int)idPegawai;

                        var strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Unit = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Nip = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Nama = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Pendidikan = strValue;

                        strValue = GetDateValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.TanggalMasuk = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Golongan = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Status = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Nik = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.TempatLahir = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.TanggalLahir = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.JenisKelamin = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Agama = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.NamaInstitusiPendidikan = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.NamaPasangan = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.NoDarurat = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Jabatan = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.NoHandphone = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Email = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Alamat = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.NoTelpon = strValue;

                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = ex.Message + Environment.NewLine + err;
                return null;
            }

        }
        public List<SheetNilaiSiswa> ReadExcelNilaiSiswa(IFormFile file, out string oMessage)
        {
            string err = string.Empty;
            try
            {
                oMessage = string.Empty;
                if (file == null || file.Length == 0)
                {
                    oMessage = "file tidak sesuai";
                    return null;
                }
                string fileExtension = Path.GetExtension(file.FileName);
                if (fileExtension != ".xlsx") { oMessage = string.Format("format file {0} salah", fileExtension); return null; }

                XSSFWorkbook xssfwb = new XSSFWorkbook();
                Stream fileStream = file.OpenReadStream();
                List<SheetNilaiSiswa> ret = new List<SheetNilaiSiswa>();

                xssfwb = new XSSFWorkbook(fileStream);
                ISheet sheet = xssfwb.GetSheetAt(0);
                int IdKbmMateri = 0;
                string JenisNilai = sheet.GetRow(5).GetCell(1).ToString();
                string NamaUrlMeeting = sheet.GetRow(4).GetCell(4).ToString();
                try
                {
                    string KbmMateri = sheet.GetRow(4).GetCell(1).ToString();
                    KbmMateri = KbmMateri.Substring(0, KbmMateri.IndexOf("]"));
                    KbmMateri = KbmMateri.Replace("[", "").Replace("]", "");
                    IdKbmMateri = int.Parse(KbmMateri);
                }
                catch (Exception ex)
                {
                    oMessage = ex.Message;
                    return null;
                }
                for (int row = 7; row <= sheet.LastRowNum; row++)
                {
                    IRow cells = sheet.GetRow(row);
                    if (cells != null)
                    {
                        if (sheet.GetRow(row).GetCell(1) == null) continue;

                        SheetNilaiSiswa m = new SheetNilaiSiswa
                        {
                            IdKBmMateri = IdKbmMateri,
                            JenisNilai = JenisNilai,
                            NamaUrlMeeting = NamaUrlMeeting
                        };
                        int cellNo = 0;
                        ICell cell = cells.GetCell(cellNo);
                        if (cell == null) { oMessage = GetCellErrorMessage(sheet.SheetName, row, cellNo); ; return null; }


                        var strValue = GetStringValue(sheet, row, cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Nis = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.NamaSiswa = strValue;

                        ++cellNo;
                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Hadir = strValue;


                        var numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.NilaiAngka = (double)numValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.NilaiHuruf = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Hasil = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Catatan = strValue;


                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = ex.Message + Environment.NewLine + err;
                return null;
            }

        }
        public List<SheetJadwalKbmModel> ReadJadwalKbm(IFormFile file, out string oMessage)
        {
            string err = string.Empty;
            try
            {
                oMessage = string.Empty;
                if (file == null || file.Length == 0)
                {
                    oMessage = "file tidak sesuai";
                    return null;
                }
                string fileExtension = Path.GetExtension(file.FileName);
                if (fileExtension != ".xlsx") { oMessage = string.Format("format file {0} salah", fileExtension); return null; }

                XSSFWorkbook xssfwb = new XSSFWorkbook();
                Stream fileStream = file.OpenReadStream();
                List<SheetJadwalKbmModel> ret = new List<SheetJadwalKbmModel>();

                xssfwb = new XSSFWorkbook(fileStream);
                ISheet sheet = xssfwb.GetSheetAt(0);
                var idKategori = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, 2, 1, out oMessage);
                if (!string.IsNullOrEmpty(oMessage)) return null;

                for (int row = 4; row <= sheet.LastRowNum; row++)
                {
                    IRow cells = sheet.GetRow(row);
                    if (cells != null)
                    {
                        if (sheet.GetRow(row).GetCell(1) == null) continue;

                        SheetJadwalKbmModel m = new SheetJadwalKbmModel
                        {
                            Kelompok = (int)idKategori
                        };
                        int cellNo = 0;
                        ICell cell = cells.GetCell(cellNo);
                        if (cell == null) { oMessage = GetCellErrorMessage(sheet.SheetName, row, cellNo); ; return null; }


                        var strValue = GetStringValue(sheet, row, cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.KelasParalel = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Hari = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.JamMulai = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.JamSelesai = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.NamaMapel = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.NipGuru = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.NamaGuru = strValue;

                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = ex.Message + Environment.NewLine + err;
                return null;
            }

        }
        public List<SheetJadwalKbmModel> ReadJadwalKbmNextWeek(IFormFile file, out string oMessage)
        {
            string err = string.Empty;
            try
            {
                oMessage = string.Empty;
                if (file == null || file.Length == 0)
                {
                    oMessage = "file tidak sesuai";
                    return null;
                }
                string fileExtension = Path.GetExtension(file.FileName);
                if (fileExtension != ".xlsx") { oMessage = string.Format("format file {0} salah", fileExtension); return null; }

                XSSFWorkbook xssfwb = new XSSFWorkbook();
                Stream fileStream = file.OpenReadStream();
                List<SheetJadwalKbmModel> ret = new List<SheetJadwalKbmModel>();

                xssfwb = new XSSFWorkbook(fileStream);
                ISheet sheet = xssfwb.GetSheetAt(0);
                if (!string.IsNullOrEmpty(oMessage)) return null;

                for (int row = 4; row <= sheet.LastRowNum; row++)
                {
                    IRow cells = sheet.GetRow(row);
                    if (cells != null)
                    {
                        if (sheet.GetRow(row).GetCell(1) == null) continue;

                        SheetJadwalKbmModel m = new SheetJadwalKbmModel();
                        int cellNo = 3;

                        var strValue = GetStringValue(sheet, row, cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.KelasParalel = strValue;

                        var numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Kelompok = (int)numValue;

                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = ex.Message + Environment.NewLine + err;
                return null;
            }

        }

        #region PPDB
        public PpdbObservasiModel ReadObservasiFile(int IdUser, IFormFile file, out string oMessage)
        {
            oMessage = string.Empty;
            if (file == null || file.Length == 0)
            {
                oMessage = "file tidak sesuai";
                return null;
            }

            string fileExtension = Path.GetExtension(file.FileName);
            if (fileExtension == ".xls" || fileExtension == ".xlsx")
            {
                HSSFWorkbook hssfwb = new HSSFWorkbook();
                XSSFWorkbook xssfwb = new XSSFWorkbook();
                Stream fileStream = file.OpenReadStream();
                PpdbObservasiModel ret = new PpdbObservasiModel();
                if (fileExtension == ".xls") { hssfwb = new HSSFWorkbook(fileStream); }
                if (fileExtension == ".xlsx") { xssfwb = new XSSFWorkbook(fileStream); }
                ISheet sheet = (fileExtension == ".xls") ? hssfwb.GetSheetAt(0) : xssfwb.GetSheetAt(0);

                ret.Biayas = new List<PpdbObservasiBiayaModel>();
                for (int row = 3; row <= sheet.LastRowNum; row++)
                {
                    ret.Biayas.Add(new PpdbObservasiBiayaModel
                    {
                        IdUser = IdUser,
                        IdPpdbDaftar = int.Parse(sheet.GetRow(row).GetCell(0).ToString()),
                        CatatanObservasi = sheet.GetRow(row).GetCell(1).ToString(),
                        CatatanWawancara = sheet.GetRow(row).GetCell(2).ToString(),
                        StatusObservasi = sheet.GetRow(row).GetCell(3).ToString(),
                        JenisBiayaPpdb = sheet.GetRow(row).GetCell(4).ToString(),
                    });

                }
                return ret;
            }
            else
            {
                oMessage = "format file salah";
                return null;
            }
        }
        // public string GenerateLaporanPendaftaran(List<PpdbDaftarKartuModel> Data, int IdUser)
        // {
        //     try
        //     {
        //         string _sourceFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/Laporan_ppdb.xlsx");
        //         string _fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/test.xlsx");
        //         Console.WriteLine(_fileName);
        //         if (System.IO.File.Exists(_fileName))
        //             System.IO.File.Delete(_fileName);

        //         XSSFWorkbook xssfwb = new XSSFWorkbook();


        //         FileStream filex = new FileStream(_sourceFile, FileMode.Open, FileAccess.Read);
        //         xssfwb = new XSSFWorkbook(filex);
        //         string _namaUnit = (from a in Data
        //                             select a.Unit).FirstOrDefault();

        //         ISheet sheetx = xssfwb.GetSheet("Detil");
        //         sheetx.CopySheet(_namaUnit);
        //         ISheet sheet = xssfwb.GetSheet(_namaUnit);
        //         sheet.GetRow(1).GetCell(0).SetCellValue(_namaUnit);
        //         sheet.GetRow(2).GetCell(0).SetCellValue("Didownload oleh : " + IdUser + " pada tanggal " + DateTime.Now.ToString());

        //         XSSFCellStyle sSFCellStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
        //         sSFCellStyle.BorderBottom = BorderStyle.Thin;
        //         sSFCellStyle.BorderLeft = BorderStyle.Thin;
        //         sSFCellStyle.BorderRight = BorderStyle.Thin;
        //         sSFCellStyle.BorderTop = BorderStyle.Thin;


        //         int i = 5;
        //         foreach (var item in Data)
        //         {
        //             IRow row = sheet.CreateRow(i);
        //             ICell cell = row.CreateCell(0);
        //             cell.SetCellValue(i - 4);
        //             cell.CellStyle = sSFCellStyle;

        //             ICell cell1 = row.CreateCell(1);
        //             cell1.SetCellValue(item.IdPpdbDaftar);
        //             cell1.CellStyle = sSFCellStyle;

        //             ICell cell2 = row.CreateCell(2);
        //             cell2.SetCellValue(item.Nama);
        //             cell2.CellStyle = sSFCellStyle;

        //             ICell cell3 = row.CreateCell(3);
        //             cell3.SetCellValue(item.TempatLahir);
        //             cell3.CellStyle = sSFCellStyle;

        //             ICell cell4 = row.CreateCell(4);
        //             cell4.SetCellValue(item.TanggalLahir);
        //             cell4.CellStyle = sSFCellStyle;

        //             //ICell cell5 = row.CreateCell(5);
        //             //cell5.SetCellValue(item.NamaIbu);
        //             //cell5.CellStyle = sSFCellStyle;
        //             //
        //             //ICell cell6 = row.CreateCell(6);
        //             //cell6.SetCellValue(item.NamaAyah);
        //             //cell6.CellStyle = sSFCellStyle;
        //             //
        //             //ICell cell7 = row.CreateCell(7);
        //             //cell7.SetCellValue(item.TanggalDaftar);
        //             //cell7.CellStyle = sSFCellStyle;

        //             ICell cell8 = row.CreateCell(8);
        //             cell8.SetCellValue(item.JenisPendaftaran);
        //             cell8.CellStyle = sSFCellStyle;

        //             ICell cell9 = row.CreateCell(9);
        //             cell9.SetCellValue(item.JalurPendaftaran);
        //             cell9.CellStyle = sSFCellStyle;

        //             //ICell cell10 = row.CreateCell(10);
        //             //cell10.SetCellValue(item.SekolahAsal);
        //             //cell10.CellStyle = sSFCellStyle;

        //             ICell cell11 = row.CreateCell(11);
        //             cell11.SetCellValue(item.Unit);
        //             cell11.CellStyle = sSFCellStyle;

        //             ICell cell12 = row.CreateCell(12);
        //             cell12.SetCellValue(item.Kelas);
        //             cell12.CellStyle = sSFCellStyle;

        //             //ICell cell13 = row.CreateCell(13);
        //             //cell13.SetCellValue(item.KategoriPendaftaran);
        //             //cell13.CellStyle = sSFCellStyle;

        //             //string _anak_pegawai = "";
        //             //if (item.IsAnakPegawai == 0)
        //             //{
        //             //    _anak_pegawai = "Tidak";
        //             //}
        //             //else
        //             //{
        //             //    _anak_pegawai = "Ya";
        //             //}
        //             //ICell cell14 = row.CreateCell(14);
        //             //cell14.SetCellValue(_anak_pegawai);
        //             //cell14.CellStyle = sSFCellStyle;

        //             i++;
        //         }
        //         for (int a = 0; a < 5; a++)
        //         {
        //             xssfwb.RemoveAt(0);
        //         }


        //         xssfwb.Write(filex);
        //         FileStream file = new FileStream(_fileName, FileMode.Create);
        //         xssfwb.Write(file);
        //         file.Close();
        //         filex.Close();
        //         return _fileName;
        //     }
        //     catch (Exception ex)
        //     {
        //         Console.WriteLine(ex.Message);
        //         return string.Empty;
        //     }
        // }
        public string GenerateKuisioner(List<PpdbKuisionerExcelModel> Data, int IdUser)
        {
            try

            {
                string _sourceFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/Laporan_ppdb.xlsx");
                string _fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/test.xlsx");
                Console.WriteLine(_fileName);
                if (System.IO.File.Exists(_fileName))
                    System.IO.File.Delete(_fileName);

                XSSFWorkbook xssfwb = new XSSFWorkbook();


                FileStream filex = new FileStream(_sourceFile, FileMode.Open, FileAccess.Read);
                xssfwb = new XSSFWorkbook(filex);
                string _namaUnit = (from a in Data
                                    select a.NamaUnit).FirstOrDefault();


                ISheet sheetx = xssfwb.GetSheet("Kuisioner");
                sheetx.CopySheet(_namaUnit);
                ISheet sheet = xssfwb.GetSheet(_namaUnit);
                sheet.GetRow(1).GetCell(0).SetCellValue(_namaUnit);
                sheet.GetRow(2).GetCell(0).SetCellValue("Didownload oleh : " + IdUser + " pada tanggal " + DateTime.Now.ToString());

                XSSFCellStyle sSFCellStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
                sSFCellStyle.BorderBottom = BorderStyle.Thin;
                sSFCellStyle.BorderLeft = BorderStyle.Thin;
                sSFCellStyle.BorderRight = BorderStyle.Thin;
                sSFCellStyle.BorderTop = BorderStyle.Thin;


                for (int _row = 5; _row < Data.Count + 6; _row++)
                {
                    var row = sheet.CreateRow(_row);
                    for (int _cell = 0; _cell < 35; _cell++)
                    {
                        var cell = row.CreateCell(_cell);
                        cell.CellStyle = sSFCellStyle;
                    }
                }
                int i = 5;
                foreach (var item in Data)
                {

                    sheet.GetRow(i).GetCell(0).SetCellValue(i - 4);
                    sheet.GetRow(i).GetCell(1).SetCellValue(item.IdPpdbDaftar);
                    sheet.GetRow(i).GetCell(2).SetCellValue(item.Nama);
                    sheet.GetRow(i).GetCell(3).SetCellValue(item.NamaIbu);
                    sheet.GetRow(i).GetCell(4).SetCellValue(item.NamaAyah);
                    sheet.GetRow(i).GetCell(5).SetCellValue(item.AsalSekolah);
                    sheet.GetRow(i).GetCell(6).SetCellValue(item.NamaUnit);
                    sheet.GetRow(i).GetCell(7).SetCellValue(item.NamaKelas);
                    sheet.GetRow(i).GetCell(8).SetCellValue(item.JenisKategoriPendaftaran);
                    string _anak_pegawai = "";
                    if (item.IdPegawai == null)
                    {
                        _anak_pegawai = "Tidak";
                    }
                    else
                    {
                        _anak_pegawai = "Ya";
                    }
                    sheet.GetRow(i).GetCell(9).SetCellValue(_anak_pegawai);

                    if (item.Jawabans.Count > 0)
                    {
                        for (int jw = 0; jw < 25; jw++)
                        {
                            if (jw > 21)
                            {
                                if (jw == 22)
                                {
                                    sheet.GetRow(i).GetCell(jw + 10).SetCellValue(item.Jawabans[jw].OpsiJawaban + " " + item.Jawabans[jw].Jawaban);
                                }
                                else
                                {
                                    sheet.GetRow(i).GetCell(jw + 10).SetCellValue(item.Jawabans[jw].Jawaban);
                                }
                            }
                            else
                            {
                                sheet.GetRow(i).GetCell(jw + 10).SetCellValue(item.Jawabans[jw].OpsiJawaban);
                            }

                        }
                    }
                    i++;
                }
                for (int a = 0; a < 5; a++)
                {
                    xssfwb.RemoveAt(0);
                }


                xssfwb.Write(filex);
                FileStream file = new FileStream(_fileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                filex.Close();
                return _fileName;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return string.Empty;
            }
        }
        public List<PpdbVaSheetModel> ReadPpdbVa(IFormFile file, out string oMessage)
        {
            oMessage = string.Empty;
            string fileExtension = Path.GetExtension(file.FileName);
            if (fileExtension == ".xls" || fileExtension == ".xlsx")
            {
                HSSFWorkbook hssfwb = new HSSFWorkbook();
                XSSFWorkbook xssfwb = new XSSFWorkbook();
                Stream fileStream = file.OpenReadStream();
                List<PpdbVaSheetModel> ret = new List<PpdbVaSheetModel>();
                if (fileExtension == ".xls") { hssfwb = new HSSFWorkbook(fileStream); }
                if (fileExtension == ".xlsx") { xssfwb = new XSSFWorkbook(fileStream); }
                ISheet sheet = (fileExtension == ".xls") ? hssfwb.GetSheetAt(0) : xssfwb.GetSheetAt(0);

                for (int row = 4; row <= sheet.LastRowNum; row++)
                {
                    IRow cells = sheet.GetRow(row);
                    if (cells != null)
                    {
                        PpdbVaSheetModel m = new PpdbVaSheetModel();
                        int cellNo = 0;
                        ICell cell = cells.GetCell(cellNo);
                        if (sheet.GetRow(row).GetCell(0) == null) continue;

                        if (cell == null) { oMessage = GetCellErrorMessage(sheet.SheetName, row, cellNo); ; return null; }

                        var numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.IdPpdbVa = (int)numValue;

                        var strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Unit = strValue;

                        ret.Add(m);
                    }
                }
                return ret;
            }
            else
            {
                oMessage = "format file salah";
                return null;
            }
        }

        public string GenerateLaporanObservasi(List<PpdbDaftarKartuModel> Data, int IdUser)
        {
            try
            {
                string _sourceFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/Laporan_ppdb.xlsx");
                string _fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/test.xlsx");
                Console.WriteLine(_fileName);
                if (System.IO.File.Exists(_fileName))
                    System.IO.File.Delete(_fileName);

                XSSFWorkbook xssfwb = new XSSFWorkbook();


                FileStream filex = new FileStream(_sourceFile, FileMode.Open, FileAccess.Read);
                xssfwb = new XSSFWorkbook(filex);
                string _namaUnit = (from a in Data
                                    select a.Unit).FirstOrDefault();

                ISheet sheetx = xssfwb.GetSheet("Observasi");
                sheetx.CopySheet(_namaUnit);
                ISheet sheet = xssfwb.GetSheet(_namaUnit);
                sheet.GetRow(1).GetCell(0).SetCellValue(_namaUnit);
                sheet.GetRow(2).GetCell(0).SetCellValue("Didownload oleh : " + IdUser + " pada tanggal " + DateTime.Now.ToString());

                XSSFCellStyle sSFCellStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
                sSFCellStyle.BorderBottom = BorderStyle.Thin;
                sSFCellStyle.BorderLeft = BorderStyle.Thin;
                sSFCellStyle.BorderRight = BorderStyle.Thin;
                sSFCellStyle.BorderTop = BorderStyle.Thin;


                int i = 5;
                foreach (var item in Data)
                {
                    IRow row = sheet.CreateRow(i);
                    ICell cell = row.CreateCell(0);
                    cell.SetCellValue(i - 4);
                    cell.CellStyle = sSFCellStyle;

                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(item.IdPpdbDaftar);
                    cell1.CellStyle = sSFCellStyle;

                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(item.Nama);
                    cell2.CellStyle = sSFCellStyle;

                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.TempatLahir);
                    cell3.CellStyle = sSFCellStyle;

                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(item.TanggalLahir);
                    cell4.CellStyle = sSFCellStyle;

                    //ICell cell5 = row.CreateCell(5);
                    //cell5.SetCellValue(item.NamaIbu);
                    //cell5.CellStyle = sSFCellStyle;
                    //
                    //ICell cell6 = row.CreateCell(6);
                    //cell6.SetCellValue(item.NamaAyah);
                    //cell6.CellStyle = sSFCellStyle;

                    //ICell cell7 = row.CreateCell(7);
                    //cell7.SetCellValue(item.TanggalDaftar);
                    //cell7.CellStyle = sSFCellStyle;

                    ICell cell8 = row.CreateCell(8);
                    cell8.SetCellValue(item.JenisPendaftaran);
                    cell8.CellStyle = sSFCellStyle;

                    ICell cell9 = row.CreateCell(9);
                    cell9.SetCellValue(item.JalurPendaftaran);
                    cell9.CellStyle = sSFCellStyle;

                    //ICell cell10 = row.CreateCell(10);
                    //cell10.SetCellValue(item.SekolahAsal);
                    //cell10.CellStyle = sSFCellStyle;

                    ICell cell11 = row.CreateCell(11);
                    cell11.SetCellValue(item.Unit);
                    cell11.CellStyle = sSFCellStyle;

                    //ICell cell12 = row.CreateCell(12);
                    //cell12.SetCellValue(item.KategoriPendaftaran);
                    //cell12.CellStyle = sSFCellStyle;

                    ICell cell13 = row.CreateCell(13);
                    cell13.SetCellValue(item.Kelas);
                    cell13.CellStyle = sSFCellStyle;

                    //string _anak_pegawai = "";
                    //if (item.IsAnakPegawai == 0)
                    //{
                    //    _anak_pegawai = "Tidak";
                    //}
                    //else
                    //{
                    //    _anak_pegawai = "Ya";
                    //}
                    //ICell cell14 = row.CreateCell(14);
                    //cell14.SetCellValue(_anak_pegawai);
                    //cell14.CellStyle = sSFCellStyle;

                    ICell cell15 = row.CreateCell(15);
                    cell15.SetCellValue("-");
                    cell15.CellStyle = sSFCellStyle;

                    ICell cell16 = row.CreateCell(16);
                    cell16.SetCellValue("-");
                    cell16.CellStyle = sSFCellStyle;

                    ICell cell17 = row.CreateCell(17); // uang pangkal
                    cell17.SetCellValue("T");
                    cell17.CellStyle = sSFCellStyle;

                    ICell cell18 = row.CreateCell(18); // boks
                    cell18.SetCellValue("T");
                    cell18.CellStyle = sSFCellStyle;

                    ICell cell19 = row.CreateCell(19); // spp
                    cell19.SetCellValue("T");
                    cell19.CellStyle = sSFCellStyle;

                    ICell cell20 = row.CreateCell(20); // infaq
                    cell20.SetCellValue("T");
                    cell20.CellStyle = sSFCellStyle;

                    ICell cell21 = row.CreateCell(21); // seragam
                    cell21.SetCellValue("T");
                    cell21.CellStyle = sSFCellStyle;

                    ICell cell22 = row.CreateCell(22); // wakaf
                    cell22.SetCellValue("T");
                    cell22.CellStyle = sSFCellStyle;

                    ICell cell23 = row.CreateCell(23); // catatan wakaf
                    cell23.SetCellValue("-");
                    cell23.CellStyle = sSFCellStyle;

                    ICell cell24 = row.CreateCell(24); // hasil observasi
                    cell24.SetCellValue("-");
                    cell24.CellStyle = sSFCellStyle;

                    i++;
                }
                for (int a = 0; a < 5; a++)
                {
                    xssfwb.RemoveAt(0);
                }


                xssfwb.Write(filex);
                FileStream file = new FileStream(_fileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                filex.Close();
                return _fileName;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return string.Empty;
            }
        }
        public List<PpdbObservasiSheetModel> ReadObservasi(IFormFile file, out string oMessage)
        {
            string err = string.Empty;
            try
            {
                oMessage = string.Empty;
                if (file == null || file.Length == 0)
                {
                    oMessage = "file tidak sesuai";
                    return null;
                }
                string fileExtension = Path.GetExtension(file.FileName);
                if (fileExtension == ".xlsx")
                {
                    XSSFWorkbook xssfwb = new XSSFWorkbook();
                    Stream fileStream = file.OpenReadStream();
                    List<PpdbObservasiSheetModel> ret = new List<PpdbObservasiSheetModel>();
                    if (fileExtension == ".xlsx") { xssfwb = new XSSFWorkbook(fileStream); }
                    ISheet sheet = xssfwb.GetSheetAt(0);
                    for (int row = 5; row <= sheet.LastRowNum; row++)
                    {
                        IRow cells = sheet.GetRow(row);
                        if (cells != null)
                        {
                            if (sheet.GetRow(row).GetCell(1) == null) continue;

                            PpdbObservasiSheetModel m = new PpdbObservasiSheetModel();
                            int cellNo = 1;
                            ICell cell = cells.GetCell(cellNo);
                            if (cell == null) { oMessage = GetCellErrorMessage(sheet.SheetName, row, cellNo); ; return null; }
                            m.IdPpdbDaftar = cell.ToString();

                            ++cellNo;
                            ++cellNo;
                            ++cellNo;
                            ++cellNo;
                            ++cellNo;
                            ++cellNo;
                            ++cellNo;
                            ++cellNo;
                            ++cellNo;
                            ++cellNo;
                            ++cellNo;
                            ++cellNo;
                            ++cellNo;

                            var strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.CatatanObservasi = strValue;

                            strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.CatatanWawancara = strValue;

                            strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.RpUp = strValue;

                            strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.RpBoks = strValue;

                            strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.RpSpp = strValue;

                            strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.RpInfaq = strValue;

                            strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.RpSeragam = strValue;

                            strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.RpWakaf = strValue;

                            // var numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            // if (!string.IsNullOrEmpty(oMessage)) return null;
                            // m.RpUp = (double)numValue;

                            // numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            // if (!string.IsNullOrEmpty(oMessage)) return null;
                            // m.RpBoks = (double)numValue;

                            // numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            // if (!string.IsNullOrEmpty(oMessage)) return null;
                            // m.RpSpp = (double)numValue;

                            // numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            // if (!string.IsNullOrEmpty(oMessage)) return null;
                            // m.RpInfaq = (double)numValue;

                            // numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            // if (!string.IsNullOrEmpty(oMessage)) return null;
                            // m.RpSeragam = (double)numValue;

                            // numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            // if (!string.IsNullOrEmpty(oMessage)) return null;
                            // m.RpWakaf = (double)numValue;

                            strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.CatatanWakaf = strValue;

                            strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Status = strValue;

                            ret.Add(m);
                        }
                    }
                    return ret;
                }
                else
                {
                    oMessage = "format file salah";
                    return null;
                }
            }
            catch (Exception ex)
            {
                oMessage = ex.Message + Environment.NewLine + err;
                return null;
            }

        }

        #endregion

        #region download excel

        public string GenerateDaftarSiswa(int IdUser, string TahunPelajaran, List<SiswaListModel> Data, out string oMessage)
        {
            try
            {
                oMessage = InitExcel(IdUser, "siswa", out XSSFWorkbook xssfwb, out ISheet sheet, out string OutFileName);
                sheet.GetRow(1).GetCell(0).SetCellValue(string.Format("Tahun Pelajaran {0}", TahunPelajaran));

                XSSFCellStyle TextStyle = SetTextStye(xssfwb);
                XSSFCellStyle RpStyle = SetRpStye(xssfwb);
                XSSFCellStyle NumberStyle = SetNumberStye(xssfwb);

                int _row = 4;
                int _noUrut = 0;
                foreach (var item in Data)
                {
                    Console.WriteLine(item.Nama);
                    IRow row = sheet.CreateRow(_row);
                    for (int _cell = 0; _cell <= 27; _cell++)
                    {
                        row.CreateCell(_cell);
                        switch (_cell)
                        {
                            default:
                                row.GetCell(_cell).CellStyle = TextStyle;
                                break;
                        }
                    }
                    _noUrut++;
                    row.GetCell(0).SetCellValue(_noUrut);
                    row.GetCell(1).SetCellValue(item.Nis);
                    row.GetCell(2).SetCellValue(item.Nisn);
                    row.GetCell(3).SetCellValue(item.Nik);
                    row.GetCell(4).SetCellValue(item.Nama);
                    row.GetCell(5).SetCellValue(item.NamaPanggilan);
                    row.GetCell(6).SetCellValue(item.TempatLahir);
                    row.GetCell(7).SetCellValue(item.TanggalLahir);
                    row.GetCell(8).SetCellValue(item.Agama);
                    row.GetCell(9).SetCellValue(item.JenisKelamin);
                    row.GetCell(10).SetCellValue(item.NoHandphone);
                    row.GetCell(11).SetCellValue(item.NoDarurat);
                    row.GetCell(12).SetCellValue(item.Email);
                    row.GetCell(13).SetCellValue(item.AlamatTinggal);
                    row.GetCell(14).SetCellValue(item.AlamatTinggal);
                    row.GetCell(15).SetCellValue(item.NikIbu);
                    row.GetCell(16).SetCellValue(item.NamaIbu);
                    row.GetCell(17).SetCellValue(item.PekerjaanIbu);
                    row.GetCell(18).SetCellValue(item.NamaInstansiIbu);
                    row.GetCell(19).SetCellValue(item.NikAyah);
                    row.GetCell(20).SetCellValue(item.NamaAyah);
                    row.GetCell(21).SetCellValue(item.PekerjaanAyah);
                    row.GetCell(22).SetCellValue(item.NamaInstansiAyah);
                    row.GetCell(23).SetCellValue(item.EmailOrtu);
                    row.GetCell(24).SetCellValue(item.Unit);
                    row.GetCell(25).SetCellValue(item.Kelas);
                    row.GetCell(26).SetCellValue(item.KelasParalel);
                    row.GetCell(27).SetCellValue("");
                    _row++;
                }

                FileStream file = new FileStream(OutFileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                return OutFileName;

            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage("GenerateDaftarSiswa", ex);
                return string.Empty;
            }
        }
        public string GenerateTemplateTagihan(int IdUser, string BulanTahun, List<SiswaListModel> Data, out string oMessage)
        {
            try
            {
                oMessage = InitExcel(IdUser, "tagihan_siswa", out XSSFWorkbook xssfwb, out ISheet sheet, out string OutFileName);
                sheet.GetRow(1).GetCell(2).SetCellValue(BulanTahun);

                XSSFCellStyle TextStyle = SetTextStye(xssfwb);
                XSSFCellStyle RpStyle = SetRpStye(xssfwb);
                XSSFCellStyle NumberStyle = SetNumberStye(xssfwb);

                int _row = 4;
                int _noUrut = 0;
                foreach (var item in Data.OrderBy(x => x.IdKelas).ThenBy(x => x.IdKelas).ThenBy(x => x.IdKelasParalel))
                {
                    IRow row = sheet.CreateRow(_row);
                    for (int _cell = 0; _cell <= 24; _cell++)
                    {
                        row.CreateCell(_cell);
                        switch (_cell)
                        {
                            case 0:
                                row.GetCell(_cell).CellStyle = NumberStyle;
                                break;
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                                row.GetCell(_cell).CellStyle = TextStyle;
                                break;
                            case 13:
                                row.GetCell(_cell).CellStyle = RpStyle;
                                row.GetCell(_cell).SetCellType(CellType.Formula);
                                row.GetCell(_cell).SetCellFormula(string.Format("SUM(H{0}:M{0})", _row + 1));
                                break;
                            case 20:
                                row.GetCell(_cell).CellStyle = RpStyle;
                                row.GetCell(_cell).SetCellType(CellType.Formula);
                                row.GetCell(_cell).SetCellFormula(string.Format("SUM(O{0}:T{0})", _row + 1));
                                break;
                            case 21:
                                row.GetCell(_cell).CellStyle = RpStyle;
                                row.GetCell(_cell).SetCellType(CellType.Formula);
                                row.GetCell(_cell).SetCellFormula(string.Format("N{0}+U{0}", _row + 1));
                                break;
                            case 22:
                                row.GetCell(_cell).CellStyle = TextStyle;
                                break;
                            case 23:
                                row.GetCell(_cell).CellStyle = TextStyle;
                                break;
                            case 24:
                                row.GetCell(_cell).CellStyle = TextStyle;
                                break;
                            default:
                                row.GetCell(_cell).CellStyle = RpStyle;
                                break;
                        }
                    }
                    _noUrut++;
                    row.GetCell(0).SetCellValue(_noUrut);
                    row.GetCell(1).SetCellValue(item.NisLama);
                    row.GetCell(2).SetCellValue(item.Nis);
                    row.GetCell(3).SetCellValue(item.TanggalLahir);
                    row.GetCell(4).SetCellValue(item.Nama);
                    row.GetCell(5).SetCellValue(item.Unit);
                    row.GetCell(6).SetCellValue(item.KelasParalel);
                    _row++;

                }
                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);
                FileStream file = new FileStream(OutFileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                return OutFileName;

            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage("GenerateTemplateTagihan", ex);
                return string.Empty;

            }
        }
        public string GenerateDaftarKelas(int IdUser, string TahunPelajaran, List<SheetKelasModel> Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                oMessage = InitExcel(IdUser, "kelas", out XSSFWorkbook xssfwb, out ISheet sheet, out string OutFileName);
                sheet.GetRow(1).GetCell(0).SetCellValue(string.Format("Tahun Pelajaran {0}", TahunPelajaran));

                XSSFCellStyle TextStyle = SetTextStye(xssfwb);
                XSSFCellStyle NumberStyle = SetNumberStye(xssfwb);

                int _row = 4;
                int _noUrut = 0;
                foreach (var item in Data)
                {
                    IRow row = sheet.CreateRow(_row);
                    for (int _cell = 0; _cell <= 7; _cell++)
                    {
                        row.CreateCell(_cell);
                        switch (_cell)
                        {
                            case 0:
                                row.GetCell(_cell).CellStyle = NumberStyle;
                                break;
                            default:
                                row.GetCell(_cell).CellStyle = TextStyle;
                                break;
                        }
                    }
                    _noUrut++;
                    row.GetCell(0).SetCellValue(_noUrut);
                    row.GetCell(1).SetCellValue(item.Unit);
                    row.GetCell(2).SetCellValue(item.Kelas);
                    row.GetCell(3).SetCellValue(item.KelasParalel);
                    row.GetCell(4).SetCellValue(item.WaliKelas);
                    row.GetCell(5).SetCellValue(item.WakilWaliKelas1);
                    row.GetCell(6).SetCellValue(item.WakilWaliKelas2);
                    row.GetCell(7).SetCellValue(item.WakilWaliKelas3);
                    _row++;
                }
                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);
                FileStream file = new FileStream(OutFileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                return OutFileName;
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage("GenerateDaftarKelas", ex);
                return string.Empty;
            }
        }
        public string GenerateDaftarMapel(int IdUser, string TahunPelajaran, List<SheetMapelModel> Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                oMessage = InitExcel(IdUser, "mata_pelajaran", out XSSFWorkbook xssfwb, out ISheet sheet, out string OutFileName);
                sheet.GetRow(1).GetCell(0).SetCellValue(string.Format("Tahun Pelajaran {0}", TahunPelajaran));

                XSSFCellStyle TextStyle = SetTextStye(xssfwb);
                XSSFCellStyle NumberStyle = SetNumberStye(xssfwb);

                int _row = 4;
                int _noUrut = 0;
                foreach (var item in Data)
                {
                    IRow row = sheet.CreateRow(_row);
                    for (int _cell = 0; _cell <= 2; _cell++)
                    {
                        row.CreateCell(_cell);
                        switch (_cell)
                        {
                            case 0:
                                row.GetCell(_cell).CellStyle = NumberStyle;
                                break;
                            default:
                                row.GetCell(_cell).CellStyle = TextStyle;
                                break;
                        }
                    }
                    _noUrut++;
                    row.GetCell(0).SetCellValue(_noUrut);
                    row.GetCell(1).SetCellValue(item.Kode);
                    row.GetCell(2).SetCellValue(item.NamaMapel);
                    _row++;
                }


                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);
                FileStream file = new FileStream(OutFileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                return OutFileName;
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage("GenerateDaftarMapel", ex);
                return string.Empty;
            }
        }
        public string GenerateKelQuran(int IdUser, string TahunPelajaran, List<SheetKelQuranModel> Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                oMessage = InitExcel(IdUser, "kel_quran", out XSSFWorkbook xssfwb, out ISheet sheet, out string OutFileName);
                sheet.GetRow(1).GetCell(0).SetCellValue(string.Format("Tahun Pelajaran {0}", TahunPelajaran));

                XSSFCellStyle TextStyle = SetTextStye(xssfwb);
                XSSFCellStyle RpStyle = SetRpStye(xssfwb);
                XSSFCellStyle NumberStyle = SetNumberStye(xssfwb);

                int _row = 4;
                int _noUrut = 0;
                foreach (var item in Data)
                {
                    IRow row = sheet.CreateRow(_row);
                    for (int _cell = 0; _cell <= 2; _cell++)
                    {
                        row.CreateCell(_cell);
                        switch (_cell)
                        {
                            case 0:
                                row.GetCell(_cell).CellStyle = NumberStyle;
                                break;
                            default:
                                row.GetCell(_cell).CellStyle = TextStyle;
                                break;
                        }
                    }
                    _noUrut++;
                    row.GetCell(0).SetCellValue(_noUrut);
                    row.GetCell(1).SetCellValue(item.NamaKelasQuran);
                    row.GetCell(2).SetCellValue(item.NamaGuru);
                    _row++;
                }


                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);
                FileStream file = new FileStream(OutFileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                return OutFileName;
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage("GenerateKelQuran", ex);
                return string.Empty;
            }
        }
        public string GenerateJadwalQuran(int IdUser, string TahunPelajaran, List<SheetJadwalQuranModel> Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                oMessage = InitExcel(IdUser, "jadwal_quran", out XSSFWorkbook xssfwb, out ISheet sheet, out string OutFileName);
                sheet.GetRow(1).GetCell(0).SetCellValue(string.Format("Tahun Pelajaran {0}", TahunPelajaran));

                XSSFCellStyle TextStyle = SetTextStye(xssfwb);
                XSSFCellStyle RpStyle = SetRpStye(xssfwb);
                XSSFCellStyle NumberStyle = SetNumberStye(xssfwb);

                int _row = 4;
                int _noUrut = 0;
                foreach (var item in Data)
                {
                    IRow row = sheet.CreateRow(_row);
                    for (int _cell = 0; _cell <= 5; _cell++)
                    {
                        row.CreateCell(_cell);
                        switch (_cell)
                        {
                            case 0:
                            case 3:
                                row.GetCell(_cell).CellStyle = NumberStyle;
                                break;
                            default:
                                row.GetCell(_cell).CellStyle = TextStyle;
                                break;
                        }
                    }
                    _noUrut++;
                    row.GetCell(0).SetCellValue(_noUrut);
                    row.GetCell(1).SetCellValue(item.NamaKelasQuran);
                    row.GetCell(2).SetCellValue(item.Hari);
                    //row.GetCell(3).SetCellValue(item.Periode);
                    row.GetCell(4).SetCellValue(item.JamMulai);
                    row.GetCell(5).SetCellValue(item.JamSelesai);
                    _row++;
                }

                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);
                FileStream file = new FileStream(OutFileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                return OutFileName;
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage("GenerateJadwalQuran", ex);
                return string.Empty;
            }
        }
        public string GenerateKelQuranSiswa(int IdUser, string TahunPelajaran, List<SheetKelQuranSiswaModel> Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                oMessage = InitExcel(IdUser, "kel_quran_siswa", out XSSFWorkbook xssfwb, out ISheet sheet, out string OutFileName);
                sheet.GetRow(1).GetCell(0).SetCellValue(string.Format("Tahun Pelajaran {0}", TahunPelajaran));

                XSSFCellStyle TextStyle = SetTextStye(xssfwb);
                XSSFCellStyle RpStyle = SetRpStye(xssfwb);
                XSSFCellStyle NumberStyle = SetNumberStye(xssfwb);

                int _row = 4;
                int _noUrut = 0;
                foreach (var item in Data)
                {
                    IRow row = sheet.CreateRow(_row);
                    for (int _cell = 0; _cell <= 4; _cell++)
                    {
                        row.CreateCell(_cell);
                        switch (_cell)
                        {
                            case 0:
                            case 3:
                                row.GetCell(_cell).CellStyle = NumberStyle;
                                break;
                            default:
                                row.GetCell(_cell).CellStyle = TextStyle;
                                break;
                        }
                    }
                    _noUrut++;
                    row.GetCell(0).SetCellValue(_noUrut);
                    row.GetCell(1).SetCellValue(item.NamaKelasQuran);
                    row.GetCell(2).SetCellValue(item.Nis);
                    row.GetCell(3).SetCellValue(item.NamaSiswa);
                    row.GetCell(4).SetCellValue(item.KelasParalel);
                    _row++;
                }

                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);
                FileStream file = new FileStream(OutFileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                return OutFileName;
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage("GenerateKelQuranSiswa", ex);
                return string.Empty;
            }
        }
        public string GenerateJenisEvaluasiHarian(int IdUser, string TahunPelajaran, List<SheetJenisEvaluasiHarianModel> Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                oMessage = InitExcel(IdUser, "evaluasi_harian", out XSSFWorkbook xssfwb, out ISheet sheet, out string OutFileName);
                sheet.GetRow(1).GetCell(0).SetCellValue(string.Format("Tahun Pelajaran {0}", TahunPelajaran));

                XSSFCellStyle TextStyle = SetTextStye(xssfwb);
                XSSFCellStyle RpStyle = SetRpStye(xssfwb);
                XSSFCellStyle NumberStyle = SetNumberStye(xssfwb);

                int _row = 4;
                int _noUrut = 0;
                foreach (var item in Data)
                {
                    IRow row = sheet.CreateRow(_row);
                    for (int _cell = 0; _cell <= 2; _cell++)
                    {
                        row.CreateCell(_cell);
                        switch (_cell)
                        {
                            case 0:
                                row.GetCell(_cell).CellStyle = NumberStyle;
                                break;
                            default:
                                row.GetCell(_cell).CellStyle = TextStyle;
                                break;
                        }
                    }
                    _noUrut++;
                    row.GetCell(0).SetCellValue(_noUrut);
                    row.GetCell(1).SetCellValue(item.Kode);
                    row.GetCell(2).SetCellValue(item.Nama);
                    _row++;
                }

                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);
                FileStream file = new FileStream(OutFileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                return OutFileName;
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage("GenerateJenisEvaluasiHarian", ex);
                return string.Empty;
            }
        }
        public string GenerateJenisJilidQuran(int IdUser, string TahunPelajaran, List<SheetJenisJilidQuranModel> Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                oMessage = InitExcel(IdUser, "jilid_quran", out XSSFWorkbook xssfwb, out ISheet sheet, out string OutFileName);
                sheet.GetRow(1).GetCell(0).SetCellValue(string.Format("Tahun Pelajaran {0}", TahunPelajaran));

                XSSFCellStyle TextStyle = SetTextStye(xssfwb);
                XSSFCellStyle RpStyle = SetRpStye(xssfwb);
                XSSFCellStyle NumberStyle = SetNumberStye(xssfwb);

                int _row = 4;
                int _noUrut = 0;
                foreach (var item in Data)
                {
                    IRow row = sheet.CreateRow(_row);
                    for (int _cell = 0; _cell <= 2; _cell++)
                    {
                        row.CreateCell(_cell);
                        switch (_cell)
                        {
                            case 0:
                                row.GetCell(_cell).CellStyle = NumberStyle;
                                break;
                            default:
                                row.GetCell(_cell).CellStyle = TextStyle;
                                break;
                        }
                    }
                    _noUrut++;
                    row.GetCell(0).SetCellValue(_noUrut);
                    row.GetCell(1).SetCellValue(item.Kode);
                    row.GetCell(2).SetCellValue(item.Nama);
                    _row++;
                }

                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);
                FileStream file = new FileStream(OutFileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                return OutFileName;
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage("GenerateJenisEvaluasiHarian", ex);
                return string.Empty;
            }
        }
        public string GeneratePegawai(int IdUser, string TahunPelajaran, List<PegawaiModel> Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                oMessage = InitExcel(IdUser, "pegawai", out XSSFWorkbook xssfwb, out ISheet sheet, out string OutFileName);
                sheet.GetRow(1).GetCell(2).SetCellValue(string.Format("{0}", TahunPelajaran));

                XSSFCellStyle TextStyle = SetTextStye(xssfwb);
                XSSFCellStyle RpStyle = SetRpStye(xssfwb);
                XSSFCellStyle NumberStyle = SetNumberStye(xssfwb);

                int _row = 4;
                int _noUrut = 0;
                int _cellNo = 0;
                foreach (var item in Data.OrderBy(x => x.IdUnit).ThenBy(x => x.Nip))
                {
                    IRow row = sheet.CreateRow(_row);
                    for (int _cell = 0; _cell <= 32; _cell++)
                    {
                        row.CreateCell(_cell);
                        switch (_cell)
                        {
                            case 0:
                                row.GetCell(_cell).CellStyle = NumberStyle;
                                break;
                            default:
                                row.GetCell(_cell).CellStyle = TextStyle;
                                break;
                        }
                    }
                    _noUrut++;
                    _cellNo = 0;
                    row.GetCell(_cellNo).SetCellValue(item.IdPegawai);
                    row.GetCell(++_cellNo).SetCellValue(item.Unit);
                    row.GetCell(++_cellNo).SetCellValue(item.Nip);
                    row.GetCell(++_cellNo).SetCellValue(item.Nama);
                    row.GetCell(++_cellNo).SetCellValue(item.JenjangPendidikan);
                    row.GetCell(++_cellNo).SetCellValue(item.TanggalMasuk);
                    row.GetCell(++_cellNo).SetCellValue(item.GolonganPegawai);
                    row.GetCell(++_cellNo).SetCellValue(item.JenisPegawai);
                    row.GetCell(++_cellNo).SetCellValue(item.NoKk);
                    row.GetCell(++_cellNo).SetCellValue(item.Nik);
                    row.GetCell(++_cellNo).SetCellValue(item.TempatLahir);
                    row.GetCell(++_cellNo).SetCellValue(item.TanggalLahir);
                    row.GetCell(++_cellNo).SetCellValue(item.JenisKelamin);
                    row.GetCell(++_cellNo).SetCellValue(item.Agama);
                    row.GetCell(++_cellNo).SetCellValue(item.NamaInstitusiPendidikan);
                    row.GetCell(++_cellNo).SetCellValue(item.JurusanPendidikan);
                    row.GetCell(++_cellNo).SetCellValue(item.NamaPasangan);
                    row.GetCell(++_cellNo).SetCellValue(item.NoDarurat);
                    row.GetCell(++_cellNo).SetCellValue(item.Jabatan);
                    row.GetCell(++_cellNo).SetCellValue(item.NoHandphone);
                    row.GetCell(++_cellNo).SetCellValue(item.Email);
                    row.GetCell(++_cellNo).SetCellValue(item.Alamat);
                    row.GetCell(++_cellNo).SetCellValue(item.NoTelpon);
                    row.GetCell(++_cellNo).SetCellValue(item.Username);
                    row.GetCell(++_cellNo).SetCellValue(item.Password);
                    row.GetCell(++_cellNo).SetCellValue(item.AktifitasDakwah);
                    row.GetCell(++_cellNo).SetCellValue(item.OrganisasiMasyarakat);
                    row.GetCell(++_cellNo).SetCellValue(item.PengalamanKerja);

                    string KelasParalel = "";
                    int index = 0;
                    if (item.KelasDiampu != null || item.KelasDiampu.Count() != 0)
                    {
                        foreach (var val in item.KelasDiampu)
                        {
                            if (index > 0) KelasParalel += ",";
                            KelasParalel += val.KelasParalel;
                            index++;
                        }
                    }
                    row.GetCell(++_cellNo).SetCellValue(KelasParalel);

                    string Kompetensi = "";
                    index = 0;
                    if (item.Kompetensi != null || item.Kompetensi.Count() != 0)
                    {
                        foreach (var val in item.Kompetensi)
                        {
                            if (index > 0) Kompetensi += ",";
                            Kompetensi += val.Kompetensi;
                            index++;
                        }
                    }
                    row.GetCell(++_cellNo).SetCellValue(Kompetensi);
                    row.GetCell(++_cellNo).SetCellValue(item.StrStatusKawin);
                    row.GetCell(++_cellNo).SetCellValue(item.JumlahAnak);
                    row.GetCell(++_cellNo).SetCellValue(string.IsNullOrEmpty(item.Npwp) ? "-" : item.Npwp);

                    _row++;
                }

                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);
                FileStream file = new FileStream(OutFileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                return OutFileName;
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage("GeneratePegawai", ex);
                return string.Empty;
            }
        }
        public string GenerateTemplateGajiPegawai(int IdUser, string BulanTahun, List<PegawaiModel> Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                oMessage = InitExcel(IdUser, "gaji", out XSSFWorkbook xssfwb, out ISheet sheet, out string OutFileName);
                sheet.GetRow(1).GetCell(2).SetCellValue(BulanTahun);
                XSSFCellStyle TextStyle = SetTextStye(xssfwb);
                XSSFCellStyle RpStyle = SetRpStye(xssfwb);
                XSSFCellStyle NumberStyle = SetNumberStye(xssfwb);

                int _row = 4;
                int _noUrut = 0;
                int _cellNo = 0;
                foreach (var item in Data.OrderBy(x => x.IdUnit).ThenBy(x => x.Nip))
                {
                    IRow row = sheet.CreateRow(_row);
                    for (int _cell = 0; _cell <= 43; _cell++)
                    {
                        row.CreateCell(_cell);
                        switch (_cell)
                        {
                            case 0:
                            case 8:
                                row.GetCell(_cell).CellStyle = NumberStyle;
                                break;
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                                row.GetCell(_cell).CellStyle = TextStyle;
                                break;
                            case 13:
                                row.GetCell(_cell).CellStyle = RpStyle;
                                row.GetCell(_cell).SetCellType(CellType.Formula);
                                row.GetCell(_cell).SetCellFormula(string.Format("SUM(J{0}:M{0})", _row + 1));
                                break;
                            case 20:
                                row.GetCell(_cell).CellStyle = RpStyle;
                                row.GetCell(_cell).SetCellType(CellType.Formula);
                                row.GetCell(_cell).SetCellFormula(string.Format("SUM(O{0}:T{0})", _row + 1));
                                break;
                            case 31:
                                row.GetCell(_cell).CellStyle = RpStyle;
                                row.GetCell(_cell).SetCellType(CellType.Formula);
                                row.GetCell(_cell).SetCellFormula(string.Format("SUM(V{0}:AE{0})", _row + 1));
                                break;
                            case 42:
                                row.GetCell(_cell).CellStyle = RpStyle;
                                row.GetCell(_cell).SetCellType(CellType.Formula);
                                row.GetCell(_cell).SetCellFormula(string.Format("SUM(AG{0}:AP{0})", _row + 1));
                                break;
                            case 43:
                                row.GetCell(_cell).CellStyle = RpStyle;
                                row.GetCell(_cell).SetCellType(CellType.Formula);
                                row.GetCell(_cell).SetCellFormula(string.Format("N{0}+U{0}-AF{0}+AQ{0}", _row + 1));
                                break;
                            default:
                                row.GetCell(_cell).CellStyle = RpStyle;
                                break;
                        }
                    }
                    _noUrut++;
                    _cellNo = 0;
                    row.GetCell(_cellNo).SetCellValue(_noUrut);
                    row.GetCell(++_cellNo).SetCellValue(item.Unit);
                    row.GetCell(++_cellNo).SetCellValue(item.Nip);
                    row.GetCell(++_cellNo).SetCellValue(item.Nama);
                    row.GetCell(++_cellNo).SetCellValue(item.JenjangPendidikan);
                    row.GetCell(++_cellNo).SetCellValue(item.TanggalMasuk);
                    row.GetCell(++_cellNo).SetCellValue(item.GolonganPegawai);
                    row.GetCell(++_cellNo).SetCellValue(item.JenisPegawai);
                    int mkg = DateTime.Now.Year - StrToDateTime(item.TanggalMasuk).Year;
                    if (mkg < 0) mkg = 0;
                    row.GetCell(++_cellNo).SetCellValue(mkg);

                    _row++;
                }

                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);
                FileStream file = new FileStream(OutFileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                return OutFileName;
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage("GeneratePegawai", ex);
                return string.Empty;
            }
        }
        public string GenerateTemplateNilaiSiswa(int IdUser, List<KbmSiswaListModel> Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                oMessage = InitExcel(IdUser, "nilai_siswa", out XSSFWorkbook xssfwb, out ISheet sheet, out string OutFileName);

                sheet.GetRow(1).GetCell(1).SetCellValue(Data[0].NamaKelasParalel);
                sheet.GetRow(2).GetCell(1).SetCellValue(Data[0].NamaMapel);
                sheet.GetRow(3).GetCell(1).SetCellValue(string.Format("{0}, {1}, {2} - {3}", Data[0].Tanggal, Data[0].Hari, Data[0].JamMulai, Data[0].JamSelesai));
                sheet.GetRow(4).GetCell(1).SetCellValue(string.Format("[{0}] - {1}", Data[0].IdKbmMateri, Data[0].JudulMateri));
                sheet.GetRow(3).GetCell(4).SetCellValue(Data[0].Guru);
                sheet.GetRow(4).GetCell(4).SetCellValue(Data[0].NamaUrlMeeting);
                sheet.GetRow(5).GetCell(1).SetCellValue("ANGKA");
                XSSFCellStyle TextStyle = SetTextStye(xssfwb);
                XSSFCellStyle RpStyle = SetRpStye(xssfwb);
                XSSFCellStyle NumberStyle = SetNumberStye(xssfwb);


                int _row = 7;
                int _cellNo = 0;
                bool isNilaiHuruf = false;
                foreach (var item in Data.OrderBy(x => x.NamaSiswa))
                {
                    if (!string.IsNullOrEmpty(item.NilaiHuruf)) isNilaiHuruf = true;
                    if (isNilaiHuruf) sheet.GetRow(5).GetCell(1).SetCellValue("HURUF");
                    IRow row = sheet.CreateRow(_row);
                    for (int _cell = 0; _cell <= 7; _cell++)
                    {
                        row.CreateCell(_cell);
                        switch (_cell)
                        {
                            case 4:
                                row.GetCell(_cell).CellStyle = NumberStyle;
                                break;
                            default:
                                row.GetCell(_cell).CellStyle = TextStyle;
                                break;
                        }
                    }
                    _cellNo = 0;
                    row.GetCell(_cellNo).SetCellValue(item.Nis);
                    row.GetCell(++_cellNo).SetCellValue(item.NamaSiswa);
                    row.GetCell(++_cellNo).SetCellValue(item.PathFileUrl);
                    row.GetCell(++_cellNo).SetCellValue(item.StatusMeeting == "Hadir" ? "" : "T");
                    ++_cellNo;
                    if (item.NilaiAngka != null)
                        row.GetCell(_cellNo).SetCellValue((double)item.NilaiAngka);
                    row.GetCell(++_cellNo).SetCellValue(item.NilaiHuruf);
                    row.GetCell(++_cellNo).SetCellValue(item.IsRemedial == 1 ? "REMEDIAL" : "TUNTAS");
                    row.GetCell(++_cellNo).SetCellValue(item.Catatan == null ? "" : StripHTML(item.Catatan));

                    _row++;
                }

                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);
                FileStream file = new FileStream(OutFileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                return OutFileName;
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage("GenerateTemplateNilaiSiswa", ex);
                return string.Empty;
            }
        }
        public string GenerateLaporanMutabaahSiswa(int IdUser, List<ElEvalHarianListModel> Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                oMessage = InitExcel(IdUser, "mutabaah_siswa", out XSSFWorkbook xssfwb, out ISheet sheet, out string OutFileName);

                XSSFCellStyle TextStyle = SetTextStye(xssfwb);
                XSSFCellStyle RpStyle = SetRpStye(xssfwb);
                XSSFCellStyle NumberStyle = SetNumberStye(xssfwb);


                int _row = 3;
                int _cellNo = 0;
                foreach (var item in Data.OrderBy(x => x.Nis))
                {
                    IRow row = sheet.CreateRow(_row);
                    for (int _cell = 0; _cell <= 11; _cell++)
                    {
                        row.CreateCell(_cell);
                        row.GetCell(_cell).CellStyle = TextStyle;
                    }
                    _cellNo = 0;
                    row.GetCell(_cellNo).SetCellValue(item.Nis);
                    row.GetCell(++_cellNo).SetCellValue(item.NamaSiswa);
                    row.GetCell(++_cellNo).SetCellValue(item.Unit);
                    row.GetCell(++_cellNo).SetCellValue(item.Kelas);
                    row.GetCell(++_cellNo).SetCellValue(item.KelasParalel);
                    row.GetCell(++_cellNo).SetCellValue(item.Minggu);
                    row.GetCell(++_cellNo).SetCellValue(item.Senin);
                    row.GetCell(++_cellNo).SetCellValue(item.Selasa);
                    row.GetCell(++_cellNo).SetCellValue(item.Rabu);
                    row.GetCell(++_cellNo).SetCellValue(item.Kamis);
                    row.GetCell(++_cellNo).SetCellValue(item.Jumat);
                    row.GetCell(++_cellNo).SetCellValue(item.Sabtu);
                    ++_cellNo;
                    _row++;
                }

                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);
                FileStream file = new FileStream(OutFileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                return OutFileName;
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage("GenerateLaporanMutabaahSiswa", ex);
                return string.Empty;
            }
        }

        public string GenerateTemplateJadwal(int IdUser, List<SheetJadwalKbmModel> Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                oMessage = InitExcel(IdUser, "jadwal_kbm", out XSSFWorkbook xssfwb, out ISheet sheet, out string OutFileName);

                sheet.GetRow(2).GetCell(1).SetCellValue(Data[0].Kelompok);
                XSSFCellStyle TextStyle = SetTextStye(xssfwb);
                XSSFCellStyle RpStyle = SetRpStye(xssfwb);
                XSSFCellStyle NumberStyle = SetNumberStye(xssfwb);


                int _row = 4;
                int _cellNo = 0;
                foreach (var item in Data.OrderBy(x => x.KelasParalel))
                {
                    IRow row = sheet.CreateRow(_row);
                    for (int _cell = 0; _cell <= 6; _cell++)
                    {
                        row.CreateCell(_cell);
                        row.GetCell(_cell).CellStyle = TextStyle;
                    }
                    _cellNo = 0;
                    row.GetCell(_cellNo).SetCellValue(item.KelasParalel);
                    row.GetCell(++_cellNo).SetCellValue(item.Hari);
                    row.GetCell(++_cellNo).SetCellValue(item.JamMulai);
                    row.GetCell(++_cellNo).SetCellValue(item.JamSelesai);
                    row.GetCell(++_cellNo).SetCellValue(item.NamaMapel);
                    row.GetCell(++_cellNo).SetCellValue(item.NipGuru);
                    row.GetCell(++_cellNo).SetCellValue(item.NamaGuru);
                    ++_cellNo;
                    _row++;
                }

                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);
                FileStream file = new FileStream(OutFileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                return OutFileName;
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage("GenerateTemplateJadwal", ex);
                return string.Empty;
            }
        }

        public string GeneratePpdbVa(int IdUser, string TahunPelajaran, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                oMessage = InitExcel(IdUser, "ppdb_va", out XSSFWorkbook xssfwb, out ISheet sheet, out string OutFileName);
                sheet.GetRow(1).GetCell(0).SetCellValue(string.Format("Tahun Pelajaran {0}", TahunPelajaran));

                XSSFCellStyle TextStyle = SetTextStye(xssfwb);
                XSSFCellStyle RpStyle = SetRpStye(xssfwb);
                XSSFCellStyle NumberStyle = SetNumberStye(xssfwb);


                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);
                FileStream file = new FileStream(OutFileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                return OutFileName;
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage("GeneratePpdbVa", ex);
                return string.Empty;
            }
        }
        #endregion
        public List<SheetJadwalKbmModel> ReadJadwalKbmTemp(IFormFile file, out string oMessage)
        {
            string err = string.Empty;
            try
            {
                oMessage = string.Empty;
                if (file == null || file.Length == 0)
                {
                    oMessage = "file tidak sesuai";
                    return null;
                }
                string fileExtension = Path.GetExtension(file.FileName);
                if (fileExtension != ".xlsx") { oMessage = string.Format("format file {0} salah", fileExtension); return null; }

                XSSFWorkbook xssfwb = new XSSFWorkbook();
                Stream fileStream = file.OpenReadStream();
                List<SheetJadwalKbmModel> ret = new List<SheetJadwalKbmModel>();

                xssfwb = new XSSFWorkbook(fileStream);
                ISheet sheet = xssfwb.GetSheetAt(0);

                for (int row = 1; row <= sheet.LastRowNum; row++)
                {
                    IRow cells = sheet.GetRow(row);
                    if (cells != null)
                    {
                        if (sheet.GetRow(row).GetCell(1) == null) continue;

                        SheetJadwalKbmModel m = new SheetJadwalKbmModel();
                        int cellNo = 0;
                        ICell cell = cells.GetCell(cellNo);
                        if (cell == null) { oMessage = GetCellErrorMessage(sheet.SheetName, row, cellNo); ; return null; }


                        var strValue = GetStringValue(sheet, row, cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.KelasParalel = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Hari = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.JamMulai = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.JamSelesai = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.NamaMapel = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.NipGuru = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.NamaGuru = strValue;

                        var numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Kelompok = (int)numValue;

                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = ex.Message + Environment.NewLine + err;
                return null;
            }

        }

        public string GenerateLaporanPpdbDaftars(List<PpdbDaftarModel> Data, int IdUser)
        {
            try
            {
                string _sourceFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/Laporan_ppdb.xlsx");
                string _fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/test.xlsx");
                //string SheetName = "LaporanPendaftaran";
                // Console.WriteLine(_fileName);
                // if (System.IO.File.Exists(_fileName))
                // System.IO.File.Delete(_fileName);

                XSSFWorkbook xssfwb = new XSSFWorkbook();


                FileStream filex = new FileStream(_sourceFile, FileMode.Open, FileAccess.Read);
                xssfwb = new XSSFWorkbook(filex);
                string _namaSekolah = Data[0].Sekolah;// (from a in Data
                                                      //select a.Sekolah).FirstOrDefault();

                ISheet sheetx = xssfwb.GetSheetAt(2);
                sheetx.CopySheet(_namaSekolah, true);
                ISheet sheet = xssfwb.GetSheet(_namaSekolah);



                sheet.GetRow(1).GetCell(0).SetCellValue(_namaSekolah);
                sheet.GetRow(2).GetCell(0).SetCellValue("Didownload oleh : " + IdUser + " pada tanggal " + DateTime.Now.ToString());

                XSSFCellStyle sSFCellStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
                sSFCellStyle.BorderBottom = BorderStyle.Thin;
                sSFCellStyle.BorderLeft = BorderStyle.Thin;
                sSFCellStyle.BorderRight = BorderStyle.Thin;
                sSFCellStyle.BorderTop = BorderStyle.Thin;


                int i = 5;
                foreach (var item in Data)
                {
                    IRow row = sheet.CreateRow(i);
                    ICell cell = row.CreateCell(0);
                    cell.SetCellValue(i - 4);
                    cell.CellStyle = sSFCellStyle;

                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(item.IdPpdbDaftar);
                    cell1.CellStyle = sSFCellStyle;

                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(item.Unit);
                    cell2.CellStyle = sSFCellStyle;

                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.Kelas + " " + item.Peminatan);
                    cell3.CellStyle = sSFCellStyle;

                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(item.JenisPendaftaran);
                    cell4.CellStyle = sSFCellStyle;

                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(item.JenisKategoriPendaftaran);
                    cell5.CellStyle = sSFCellStyle;

                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(item.JalurPendaftaran);
                    cell6.CellStyle = sSFCellStyle;

                    ICell cell7 = row.CreateCell(7);
                    cell7.SetCellValue(item.Nama);
                    cell7.CellStyle = sSFCellStyle;

                    ICell cell8 = row.CreateCell(8);
                    cell8.SetCellValue(item.TempatLahir);
                    cell8.CellStyle = sSFCellStyle;

                    ICell cell9 = row.CreateCell(9);
                    cell9.SetCellValue(item.TanggalLahir);
                    cell9.CellStyle = sSFCellStyle;

                    ICell cell10 = row.CreateCell(10);
                    cell10.SetCellValue(item.KdJenisKelamin);
                    cell10.CellStyle = sSFCellStyle;

                    ICell cell11 = row.CreateCell(11);
                    cell11.SetCellValue(item.NikOrtu);
                    cell11.CellStyle = sSFCellStyle;

                    ICell cell12 = row.CreateCell(12);
                    cell12.SetCellValue(item.Email);
                    cell12.CellStyle = sSFCellStyle;

                    string nope = SetNoHandphone(item.NoHandphone);
                    nope = nope.Substring(1, item.NoHandphone.Length - 1);
                    ICell cell13 = row.CreateCell(13);
                    cell13.SetCellValue("62" + nope);
                    cell13.CellStyle = sSFCellStyle;

                    string Status = "LUNAS";
                    if (item.Pin == null)
                        Status = item.Status;
                    ICell cell14 = row.CreateCell(14);
                    cell14.SetCellValue(Status);
                    cell14.CellStyle = sSFCellStyle;

                    i++;
                }


                for (int a = 0; a < 7; a++)
                {
                    xssfwb.RemoveAt(0);
                }


                xssfwb.Write(filex);
                FileStream file = new FileStream(_fileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                filex.Close();
                return _fileName;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return string.Empty;
            }
        }

        public string GenerateLaporanObservasiWawancara(List<PpdbSiswaObservasiWawancara> Data, int IdUser)
        {
            try
            {
                string _sourceFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/Laporan_ppdb.xlsx");
                string _fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/test.xlsx");
                //string SheetName = "LaporanPendaftaran";
                Console.WriteLine(_fileName);
                if (System.IO.File.Exists(_fileName))
                    System.IO.File.Delete(_fileName);

                XSSFWorkbook xssfwb = new XSSFWorkbook();


                FileStream filex = new FileStream(_sourceFile, FileMode.Open, FileAccess.Read);
                xssfwb = new XSSFWorkbook(filex);
                string _namaSekolah = "laporan observasi wawancara";// (from a in Data
                //select a.Sekolah).FirstOrDefault();

                ISheet sheetx = xssfwb.GetSheetAt(6);
                sheetx.CopySheet(_namaSekolah, true);
                ISheet sheet = xssfwb.GetSheet(_namaSekolah);



                // sheet.GetRow(1).GetCell(0).SetCellValue(_namaSekolah);
                // sheet.GetRow(2).GetCell(0).SetCellValue("Didownload oleh : " + IdUser + " pada tanggal " + DateTime.Now.ToString());

                XSSFCellStyle sSFCellStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
                sSFCellStyle.BorderBottom = BorderStyle.Thin;
                sSFCellStyle.BorderLeft = BorderStyle.Thin;
                sSFCellStyle.BorderRight = BorderStyle.Thin;
                sSFCellStyle.BorderTop = BorderStyle.Thin;


                int i = 4;
                foreach (var item in Data)
                {
                    IRow row = sheet.CreateRow(i);
                    ICell cell = row.CreateCell(0);
                    cell.SetCellValue(i - 3);
                    cell.CellStyle = sSFCellStyle;

                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(item.IdPpdbDaftar);
                    cell1.CellStyle = sSFCellStyle;

                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(item.Unit);
                    cell2.CellStyle = sSFCellStyle;

                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.Kelas + " " + item.Peminatan);
                    cell3.CellStyle = sSFCellStyle;

                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(item.JenisPendaftaran);
                    cell4.CellStyle = sSFCellStyle;

                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(item.JenisKategoriPendaftaran);
                    cell5.CellStyle = sSFCellStyle;

                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(item.JalurPendaftaran);
                    cell6.CellStyle = sSFCellStyle;

                    ICell cell7 = row.CreateCell(7);
                    cell7.SetCellValue(item.Nama);
                    cell7.CellStyle = sSFCellStyle;

                    ICell cell8 = row.CreateCell(8);
                    cell8.SetCellValue(item.NoHandphoneSiswa);
                    cell8.CellStyle = sSFCellStyle;

                    ICell cell9 = row.CreateCell(9);
                    cell9.SetCellValue(item.NamaPetugasObservasi);
                    cell9.CellStyle = sSFCellStyle;

                    ICell cell10 = row.CreateCell(10);
                    cell10.SetCellValue(item.EmailPetugasObservasi);
                    cell10.CellStyle = sSFCellStyle;

                    ICell cell11 = row.CreateCell(11);
                    cell11.SetCellValue(item.NoHpPetugasObservasi);
                    cell11.CellStyle = sSFCellStyle;

                    ICell cell12 = row.CreateCell(12);
                    cell12.SetCellValue(item.JamObservasi);
                    cell12.CellStyle = sSFCellStyle;

                    ICell cell13 = row.CreateCell(13);
                    cell13.SetCellValue(item.TanggalObservasi);
                    cell13.CellStyle = sSFCellStyle;

                    ICell cell14 = row.CreateCell(14);
                    cell14.SetCellValue(item.NamaPetugasWawancara);
                    cell14.CellStyle = sSFCellStyle;

                    ICell cell15 = row.CreateCell(15);
                    cell15.SetCellValue(item.EmailPetugasWawancara);
                    cell15.CellStyle = sSFCellStyle;

                    ICell cell16 = row.CreateCell(16);
                    cell16.SetCellValue(item.NoHpPetugasWawancara);
                    cell16.CellStyle = sSFCellStyle;

                    ICell cell17 = row.CreateCell(17);
                    cell17.SetCellValue(item.JamWawancara);
                    cell17.CellStyle = sSFCellStyle;

                    ICell cell18 = row.CreateCell(18);
                    cell18.SetCellValue(item.TanggalWawancara);
                    cell18.CellStyle = sSFCellStyle;

                    string StatusProses = "Menunggu Diproses";
                    if (item.StatusProses != "")
                    {
                        StatusProses = item.StatusProses;
                    }
                    ICell cell19 = row.CreateCell(19);
                    cell19.SetCellValue(StatusProses);
                    cell19.CellStyle = sSFCellStyle;

                    ICell cell20 = row.CreateCell(20);
                    cell20.SetCellValue(item.CatatanProses);
                    cell20.CellStyle = sSFCellStyle;

                    i++;
                }


                for (int a = 0; a < 7; a++)
                {
                    xssfwb.RemoveAt(0);
                }


                xssfwb.Write(filex);
                FileStream file = new FileStream(_fileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                filex.Close();
                return _fileName;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return string.Empty;
            }
        }

        public List<PpdbAbsensiSheetModel> ReadPpdbAbsensi(IFormFile file, out string oMessage)
        {
            oMessage = string.Empty;
            string fileExtension = Path.GetExtension(file.FileName);
            if (fileExtension == ".xls" || fileExtension == ".xlsx")
            {
                HSSFWorkbook hssfwb = new HSSFWorkbook();
                XSSFWorkbook xssfwb = new XSSFWorkbook();
                Stream fileStream = file.OpenReadStream();
                List<PpdbAbsensiSheetModel> ret = new List<PpdbAbsensiSheetModel>();
                if (fileExtension == ".xls") { hssfwb = new HSSFWorkbook(fileStream); }
                if (fileExtension == ".xlsx") { xssfwb = new XSSFWorkbook(fileStream); }
                ISheet sheet = (fileExtension == ".xls") ? hssfwb.GetSheetAt(0) : xssfwb.GetSheetAt(0);

                for (int row = 4; row <= sheet.LastRowNum; row++)
                {
                    IRow cells = sheet.GetRow(row);
                    if (cells != null)
                    {
                        PpdbAbsensiSheetModel m = new PpdbAbsensiSheetModel();
                        int cellNo = 0;
                        ICell cell = cells.GetCell(cellNo);
                        // for (int xxx = 0; xxx <= 18; xxx++)
                        // {
                        //     if (sheet.GetRow(row).GetCell(xxx) == null) continue;
                        // }

                        if (cell == null) { oMessage = GetCellErrorMessage(sheet.SheetName, row, cellNo); ; return null; }

                        ++cellNo;
                        if (cellNo == 1)
                        {
                            var numValue = GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.IdPpdbDaftar = (int)numValue;
                        }


                        for (int xxx = cellNo; xxx <= 18; xxx++)
                        {
                            ++cellNo;
                        }

                        if (cellNo == 19)
                        {
                            var strValue = GetStringValue(sheet, row, cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Status = strValue;
                        }
                        ++cellNo;
                        if (cellNo == 20)
                        {
                            var strValue = GetStringValue(sheet, row, cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m.Catatan = strValue;
                        }
                        ret.Add(m);
                    }
                }
                return ret;
            }
            else
            {
                oMessage = "format file salah";
                return null;
            }
        }

        public string GenerateLaporanGelombangPpdb(string TanggalAwal, string TanggalAkhir, SheetLaporanGelombangPpdbModel Data, int IdUser, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                string _sourceFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/Laporan_gelombang_ppdb.xlsx");
                string _fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/test.xlsx");
                Console.WriteLine(_fileName);
                if (System.IO.File.Exists(_fileName))
                    System.IO.File.Delete(_fileName);

                XSSFWorkbook xssfwb = new XSSFWorkbook();

                FileStream filex = new FileStream(_sourceFile, FileMode.Open, FileAccess.Read);
                xssfwb = new XSSFWorkbook(filex);


                XSSFCellStyle sSFCellStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
                sSFCellStyle.BorderBottom = BorderStyle.Thin;
                sSFCellStyle.BorderLeft = BorderStyle.Thin;
                sSFCellStyle.BorderRight = BorderStyle.Thin;
                sSFCellStyle.BorderTop = BorderStyle.Thin;


                ISheet sheet0copy = xssfwb.GetSheetAt(0);
                sheet0copy.CopySheet("PPDB TOTAL KESELURUHAN", true);
                ISheet sheet0 = xssfwb.GetSheet("PPDB TOTAL KESELURUHAN");
                sheet0.GetRow(1).GetCell(1).SetCellValue(TanggalAwal + " s/d" + TanggalAkhir);

                sheet0.GetRow(6).GetCell(3).SetCellValue(Data.StatikTotalPpdb.KuotaPpdbTkA.TotL);
                sheet0.GetRow(6).GetCell(4).SetCellValue(Data.StatikTotalPpdb.KuotaPpdbTkA.TotP);

                sheet0.GetRow(6).GetCell(6).SetCellValue(Data.StatikTotalPpdb.KuotaPpdbTkB.TotL);
                sheet0.GetRow(6).GetCell(7).SetCellValue(Data.StatikTotalPpdb.KuotaPpdbTkB.TotP);

                sheet0.GetRow(6).GetCell(11).SetCellValue(Data.StatikTotalPpdb.KuotaPpdbSd.TotL);
                sheet0.GetRow(6).GetCell(12).SetCellValue(Data.StatikTotalPpdb.KuotaPpdbSd.TotP);

                sheet0.GetRow(6).GetCell(15).SetCellValue(Data.StatikTotalPpdb.KuotaPpdbSmp.TotL);
                sheet0.GetRow(6).GetCell(16).SetCellValue(Data.StatikTotalPpdb.KuotaPpdbSmp.TotP);

                sheet0.GetRow(6).GetCell(19).SetCellValue(Data.StatikTotalPpdb.KuotaPpdbSmaIpa.TotL);
                sheet0.GetRow(6).GetCell(20).SetCellValue(Data.StatikTotalPpdb.KuotaPpdbSmaIpa.TotP);
                sheet0.GetRow(6).GetCell(21).SetCellValue(Data.StatikTotalPpdb.KuotaPpdbSmaIps.TotP);
                sheet0.GetRow(6).GetCell(22).SetCellValue(Data.StatikTotalPpdb.KuotaPpdbSmaIps.TotP);

                ////PENDAFTARAN
                sheet0.GetRow(7).GetCell(3).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarTkA.TotL);
                sheet0.GetRow(7).GetCell(4).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarTkA.TotP);

                sheet0.GetRow(7).GetCell(6).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarTkB.TotL);
                sheet0.GetRow(7).GetCell(7).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarTkB.TotP);

                sheet0.GetRow(7).GetCell(11).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarSd.TotL);
                sheet0.GetRow(7).GetCell(12).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarSd.TotP);

                sheet0.GetRow(7).GetCell(15).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarSmp.TotL);
                sheet0.GetRow(7).GetCell(16).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarSmp.TotP);

                sheet0.GetRow(7).GetCell(19).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarSmaIpa.TotL);
                sheet0.GetRow(7).GetCell(20).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarSmaIpa.TotP);
                sheet0.GetRow(7).GetCell(21).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarSmaIps.TotP);
                sheet0.GetRow(7).GetCell(22).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarSmaIps.TotP);

                ////DAFTAR TUNGGU PENDAFTARAN
                sheet0.GetRow(8).GetCell(3).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarTungguTkA.TotL);
                sheet0.GetRow(8).GetCell(4).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarTungguTkA.TotP);

                sheet0.GetRow(8).GetCell(6).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarTungguTkB.TotL);
                sheet0.GetRow(8).GetCell(7).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarTungguTkB.TotP);

                sheet0.GetRow(8).GetCell(11).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarTungguSd.TotL);
                sheet0.GetRow(8).GetCell(12).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarTungguSd.TotP);

                sheet0.GetRow(8).GetCell(15).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarTungguSmp.TotL);
                sheet0.GetRow(8).GetCell(16).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarTungguSmp.TotP);

                sheet0.GetRow(8).GetCell(19).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarTungguSmaIpa.TotL);
                sheet0.GetRow(8).GetCell(20).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarTungguSmaIpa.TotP);
                sheet0.GetRow(8).GetCell(21).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarTungguSmaIps.TotL);
                sheet0.GetRow(8).GetCell(22).SetCellValue(Data.StatikTotalPpdb.KuotaDaftarTungguSmaIps.TotP);

                ////LULUS OBSERVASI
                sheet0.GetRow(9).GetCell(3).SetCellValue(Data.StatikTotalPpdb.KuotaLulusObservasiTkA.TotL);
                sheet0.GetRow(9).GetCell(4).SetCellValue(Data.StatikTotalPpdb.KuotaLulusObservasiTkA.TotP);

                sheet0.GetRow(9).GetCell(6).SetCellValue(Data.StatikTotalPpdb.KuotaLulusObservasiTkB.TotL);
                sheet0.GetRow(9).GetCell(7).SetCellValue(Data.StatikTotalPpdb.KuotaLulusObservasiTkB.TotP);

                sheet0.GetRow(9).GetCell(11).SetCellValue(Data.StatikTotalPpdb.KuotaLulusObservasiSd.TotL);
                sheet0.GetRow(9).GetCell(12).SetCellValue(Data.StatikTotalPpdb.KuotaLulusObservasiSd.TotP);

                sheet0.GetRow(9).GetCell(15).SetCellValue(Data.StatikTotalPpdb.KuotaLulusObservasiSmp.TotL);
                sheet0.GetRow(9).GetCell(16).SetCellValue(Data.StatikTotalPpdb.KuotaLulusObservasiSmp.TotP);

                sheet0.GetRow(9).GetCell(19).SetCellValue(Data.StatikTotalPpdb.KuotaLulusObservasiSmaIpa.TotL);
                sheet0.GetRow(9).GetCell(20).SetCellValue(Data.StatikTotalPpdb.KuotaLulusObservasiSmaIpa.TotP);
                sheet0.GetRow(9).GetCell(21).SetCellValue(Data.StatikTotalPpdb.KuotaLulusObservasiSmaIps.TotL);
                sheet0.GetRow(9).GetCell(22).SetCellValue(Data.StatikTotalPpdb.KuotaLulusObservasiSmaIps.TotP);

                ////TIDAK LULUS OBSERVASI
                sheet0.GetRow(10).GetCell(3).SetCellValue(Data.StatikTotalPpdb.KuotaTidakLulusObservasiTkA.TotL);
                sheet0.GetRow(10).GetCell(4).SetCellValue(Data.StatikTotalPpdb.KuotaTidakLulusObservasiTkA.TotP);

                sheet0.GetRow(10).GetCell(6).SetCellValue(Data.StatikTotalPpdb.KuotaTidakLulusObservasiTkB.TotL);
                sheet0.GetRow(10).GetCell(7).SetCellValue(Data.StatikTotalPpdb.KuotaTidakLulusObservasiTkB.TotP);

                sheet0.GetRow(10).GetCell(11).SetCellValue(Data.StatikTotalPpdb.KuotaTidakLulusObservasiSd.TotL);
                sheet0.GetRow(10).GetCell(12).SetCellValue(Data.StatikTotalPpdb.KuotaTidakLulusObservasiSd.TotP);

                sheet0.GetRow(10).GetCell(15).SetCellValue(Data.StatikTotalPpdb.KuotaTidakLulusObservasiSmp.TotL);
                sheet0.GetRow(10).GetCell(16).SetCellValue(Data.StatikTotalPpdb.KuotaTidakLulusObservasiSmp.TotP);

                sheet0.GetRow(10).GetCell(19).SetCellValue(Data.StatikTotalPpdb.KuotaTidakLulusObservasiSmaIpa.TotL);
                sheet0.GetRow(10).GetCell(20).SetCellValue(Data.StatikTotalPpdb.KuotaTidakLulusObservasiSmaIpa.TotP);
                sheet0.GetRow(10).GetCell(21).SetCellValue(Data.StatikTotalPpdb.KuotaTidakLulusObservasiSmaIps.TotL);
                sheet0.GetRow(10).GetCell(22).SetCellValue(Data.StatikTotalPpdb.KuotaTidakLulusObservasiSmaIps.TotP);

                ////DAFTAR TUNGGU SISWA - CADANGAN
                sheet0.GetRow(11).GetCell(3).SetCellValue(Data.StatikTotalPpdb.KuotaCadanganObservasiTkA.TotL);
                sheet0.GetRow(11).GetCell(4).SetCellValue(Data.StatikTotalPpdb.KuotaCadanganObservasiTkA.TotP);

                sheet0.GetRow(11).GetCell(6).SetCellValue(Data.StatikTotalPpdb.KuotaCadanganObservasiTkB.TotL);
                sheet0.GetRow(11).GetCell(7).SetCellValue(Data.StatikTotalPpdb.KuotaCadanganObservasiTkB.TotP);

                sheet0.GetRow(11).GetCell(11).SetCellValue(Data.StatikTotalPpdb.KuotaCadanganObservasiSd.TotL);
                sheet0.GetRow(11).GetCell(12).SetCellValue(Data.StatikTotalPpdb.KuotaCadanganObservasiSd.TotP);

                sheet0.GetRow(11).GetCell(15).SetCellValue(Data.StatikTotalPpdb.KuotaCadanganObservasiSmp.TotL);
                sheet0.GetRow(11).GetCell(16).SetCellValue(Data.StatikTotalPpdb.KuotaCadanganObservasiSmp.TotP);

                sheet0.GetRow(11).GetCell(19).SetCellValue(Data.StatikTotalPpdb.KuotaCadanganObservasiSmaIpa.TotL);
                sheet0.GetRow(11).GetCell(20).SetCellValue(Data.StatikTotalPpdb.KuotaCadanganObservasiSmaIpa.TotP);
                sheet0.GetRow(11).GetCell(21).SetCellValue(Data.StatikTotalPpdb.KuotaCadanganObservasiSmaIps.TotL);
                sheet0.GetRow(11).GetCell(22).SetCellValue(Data.StatikTotalPpdb.KuotaCadanganObservasiSmaIps.TotP);

                ////BIAYA PENDIDIKAN BELUM LUNAS
                sheet0.GetRow(12).GetCell(3).SetCellValue(Data.StatikTotalPpdb.KuotaBiayaPendidikanTkA.TotL);
                sheet0.GetRow(12).GetCell(4).SetCellValue(Data.StatikTotalPpdb.KuotaBiayaPendidikanTkA.TotP);

                sheet0.GetRow(12).GetCell(6).SetCellValue(Data.StatikTotalPpdb.KuotaBiayaPendidikanTkB.TotL);
                sheet0.GetRow(12).GetCell(7).SetCellValue(Data.StatikTotalPpdb.KuotaBiayaPendidikanTkB.TotP);

                sheet0.GetRow(12).GetCell(11).SetCellValue(Data.StatikTotalPpdb.KuotaBiayaPendidikanSd.TotL);
                sheet0.GetRow(12).GetCell(12).SetCellValue(Data.StatikTotalPpdb.KuotaBiayaPendidikanSd.TotP);

                sheet0.GetRow(12).GetCell(15).SetCellValue(Data.StatikTotalPpdb.KuotaBiayaPendidikanSmp.TotL);
                sheet0.GetRow(12).GetCell(16).SetCellValue(Data.StatikTotalPpdb.KuotaBiayaPendidikanSmp.TotP);

                sheet0.GetRow(12).GetCell(19).SetCellValue(Data.StatikTotalPpdb.KuotaBiayaPendidikanSmaIpa.TotL);
                sheet0.GetRow(12).GetCell(20).SetCellValue(Data.StatikTotalPpdb.KuotaBiayaPendidikanSmaIpa.TotP);
                sheet0.GetRow(12).GetCell(21).SetCellValue(Data.StatikTotalPpdb.KuotaBiayaPendidikanSmaIps.TotL);
                sheet0.GetRow(12).GetCell(22).SetCellValue(Data.StatikTotalPpdb.KuotaBiayaPendidikanSmaIps.TotP);

                ISheet sheet2copy = xssfwb.GetSheetAt(1);
                sheet2copy.CopySheet("Siswa TK A dan TK B", true);
                ISheet sheet2 = xssfwb.GetSheet("Siswa TK A dan TK B");
                int i = 3;
                foreach (var item in Data.LapSiswaTk)
                {
                    IRow row = sheet2.CreateRow(i);
                    ICell cell = row.CreateCell(0);
                    cell.SetCellValue(i - 2);
                    cell.CellStyle = sSFCellStyle;

                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(item.IdPpdbDaftar);
                    cell1.CellStyle = sSFCellStyle;

                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(item.JenjangPendidikan);
                    cell2.CellStyle = sSFCellStyle;

                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.JalurPendaftaran);
                    cell3.CellStyle = sSFCellStyle;

                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(item.Nis);
                    cell4.CellStyle = sSFCellStyle;

                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(item.Nama);
                    cell5.CellStyle = sSFCellStyle;

                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(item.JenisKelamin);
                    cell6.CellStyle = sSFCellStyle;

                    ICell cell7 = row.CreateCell(7);
                    cell7.SetCellValue(item.TempatLahir);
                    cell7.CellStyle = sSFCellStyle;

                    ICell cell8 = row.CreateCell(8);
                    cell8.SetCellValue(item.TanggalLahir);
                    cell8.CellStyle = sSFCellStyle;

                    ICell cell9 = row.CreateCell(9);
                    cell9.SetCellValue(item.Alamat);
                    cell9.CellStyle = sSFCellStyle;

                    ICell cell10 = row.CreateCell(10);
                    cell10.SetCellValue(item.NamaAyah);
                    cell10.CellStyle = sSFCellStyle;

                    ICell cell11 = row.CreateCell(11);
                    cell11.SetCellValue(item.NoHpAyah);
                    cell11.CellStyle = sSFCellStyle;

                    ICell cell12 = row.CreateCell(12);
                    cell12.SetCellValue(item.EmailAyah);
                    cell12.CellStyle = sSFCellStyle;

                    ICell cell13 = row.CreateCell(13);
                    cell13.SetCellValue(item.NamaIbu);
                    cell13.CellStyle = sSFCellStyle;

                    ICell cell14 = row.CreateCell(14);
                    cell14.SetCellValue(item.NoHpIbu);
                    cell14.CellStyle = sSFCellStyle;

                    ICell cell15 = row.CreateCell(15);
                    cell15.SetCellValue(item.EmailIbu);
                    cell15.CellStyle = sSFCellStyle;

                    ICell cell16 = row.CreateCell(16);
                    cell16.SetCellValue(item.NamaPewakaf);
                    cell16.CellStyle = sSFCellStyle;

                    ICell cell17 = row.CreateCell(17);
                    cell17.SetCellValue(item.HubunganSiswa);
                    cell17.CellStyle = sSFCellStyle;

                    ICell cell18 = row.CreateCell(18);
                    cell18.SetCellValue(item.StatusProses);
                    cell18.CellStyle = sSFCellStyle;

                    ICell cell19 = row.CreateCell(19);
                    cell19.SetCellValue(item.StatusPpdb);
                    cell19.CellStyle = sSFCellStyle;

                    i++;
                }

                ISheet sheet3copy = xssfwb.GetSheetAt(2);
                sheet3copy.CopySheet("Siswa SD", true);
                ISheet sheet3 = xssfwb.GetSheet("Siswa SD");
                i = 3;
                foreach (var item in Data.LapSiswaSd)
                {
                    IRow row = sheet3.CreateRow(i);
                    ICell cell = row.CreateCell(0);
                    cell.SetCellValue(i - 2);
                    cell.CellStyle = sSFCellStyle;

                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(item.IdPpdbDaftar);
                    cell1.CellStyle = sSFCellStyle;

                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(item.JenjangPendidikan);
                    cell2.CellStyle = sSFCellStyle;

                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.JalurPendaftaran);
                    cell3.CellStyle = sSFCellStyle;

                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(item.Nis);
                    cell4.CellStyle = sSFCellStyle;

                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(item.Nama);
                    cell5.CellStyle = sSFCellStyle;

                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(item.JenisKelamin);
                    cell6.CellStyle = sSFCellStyle;

                    ICell cell7 = row.CreateCell(7);
                    cell7.SetCellValue(item.TempatLahir);
                    cell7.CellStyle = sSFCellStyle;

                    ICell cell8 = row.CreateCell(8);
                    cell8.SetCellValue(item.TanggalLahir);
                    cell8.CellStyle = sSFCellStyle;

                    ICell cell9 = row.CreateCell(9);
                    cell9.SetCellValue(item.Alamat);
                    cell9.CellStyle = sSFCellStyle;

                    ICell cell10 = row.CreateCell(10);
                    cell10.SetCellValue(item.NamaAyah);
                    cell10.CellStyle = sSFCellStyle;

                    ICell cell11 = row.CreateCell(11);
                    cell11.SetCellValue(item.NoHpAyah);
                    cell11.CellStyle = sSFCellStyle;

                    ICell cell12 = row.CreateCell(12);
                    cell12.SetCellValue(item.EmailAyah);
                    cell12.CellStyle = sSFCellStyle;

                    ICell cell13 = row.CreateCell(13);
                    cell13.SetCellValue(item.NamaIbu);
                    cell13.CellStyle = sSFCellStyle;

                    ICell cell14 = row.CreateCell(14);
                    cell14.SetCellValue(item.NoHpIbu);
                    cell14.CellStyle = sSFCellStyle;

                    ICell cell15 = row.CreateCell(15);
                    cell15.SetCellValue(item.EmailIbu);
                    cell15.CellStyle = sSFCellStyle;

                    ICell cell16 = row.CreateCell(16);
                    cell16.SetCellValue(item.NamaPewakaf);
                    cell16.CellStyle = sSFCellStyle;

                    ICell cell17 = row.CreateCell(17);
                    cell17.SetCellValue(item.HubunganSiswa);
                    cell17.CellStyle = sSFCellStyle;

                    ICell cell18 = row.CreateCell(18);
                    cell18.SetCellValue(item.StatusProses);
                    cell18.CellStyle = sSFCellStyle;

                    ICell cell19 = row.CreateCell(19);
                    cell19.SetCellValue(item.StatusPpdb);
                    cell19.CellStyle = sSFCellStyle;

                    i++;
                }

                ISheet sheet4copy = xssfwb.GetSheetAt(3);
                sheet4copy.CopySheet("Siswa SMP", true);
                ISheet sheet4 = xssfwb.GetSheet("Siswa SMP");
                i = 3;
                foreach (var item in Data.LapSiswaSmp)
                {
                    IRow row = sheet4.CreateRow(i);
                    ICell cell = row.CreateCell(0);
                    cell.SetCellValue(i - 2);
                    cell.CellStyle = sSFCellStyle;

                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(item.IdPpdbDaftar);
                    cell1.CellStyle = sSFCellStyle;

                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(item.JenjangPendidikan);
                    cell2.CellStyle = sSFCellStyle;

                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.JalurPendaftaran);
                    cell3.CellStyle = sSFCellStyle;

                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(item.Nis);
                    cell4.CellStyle = sSFCellStyle;

                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(item.Nama);
                    cell5.CellStyle = sSFCellStyle;

                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(item.JenisKelamin);
                    cell6.CellStyle = sSFCellStyle;

                    ICell cell7 = row.CreateCell(7);
                    cell7.SetCellValue(item.TempatLahir);
                    cell7.CellStyle = sSFCellStyle;

                    ICell cell8 = row.CreateCell(8);
                    cell8.SetCellValue(item.TanggalLahir);
                    cell8.CellStyle = sSFCellStyle;

                    ICell cell9 = row.CreateCell(9);
                    cell9.SetCellValue(item.Alamat);
                    cell9.CellStyle = sSFCellStyle;

                    ICell cell10 = row.CreateCell(10);
                    cell10.SetCellValue(item.NamaAyah);
                    cell10.CellStyle = sSFCellStyle;

                    ICell cell11 = row.CreateCell(11);
                    cell11.SetCellValue(item.NoHpAyah);
                    cell11.CellStyle = sSFCellStyle;

                    ICell cell12 = row.CreateCell(12);
                    cell12.SetCellValue(item.EmailAyah);
                    cell12.CellStyle = sSFCellStyle;

                    ICell cell13 = row.CreateCell(13);
                    cell13.SetCellValue(item.NamaIbu);
                    cell13.CellStyle = sSFCellStyle;

                    ICell cell14 = row.CreateCell(14);
                    cell14.SetCellValue(item.NoHpIbu);
                    cell14.CellStyle = sSFCellStyle;

                    ICell cell15 = row.CreateCell(15);
                    cell15.SetCellValue(item.EmailIbu);
                    cell15.CellStyle = sSFCellStyle;

                    ICell cell16 = row.CreateCell(16);
                    cell16.SetCellValue(item.NamaPewakaf);
                    cell16.CellStyle = sSFCellStyle;

                    ICell cell17 = row.CreateCell(17);
                    cell17.SetCellValue(item.HubunganSiswa);
                    cell17.CellStyle = sSFCellStyle;

                    ICell cell18 = row.CreateCell(18);
                    cell18.SetCellValue(item.StatusProses);
                    cell18.CellStyle = sSFCellStyle;

                    ICell cell19 = row.CreateCell(19);
                    cell19.SetCellValue(item.StatusPpdb);
                    cell19.CellStyle = sSFCellStyle;

                    i++;
                }

                ISheet sheet5copy = xssfwb.GetSheetAt(4);
                sheet5copy.CopySheet("Siswa SMA", true);
                ISheet sheet5 = xssfwb.GetSheet("Siswa SMA");
                i = 3;
                foreach (var item in Data.LapSiswaSma)
                {
                    IRow row = sheet5.CreateRow(i);
                    ICell cell = row.CreateCell(0);
                    cell.SetCellValue(i - 2);
                    cell.CellStyle = sSFCellStyle;

                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(item.IdPpdbDaftar);
                    cell1.CellStyle = sSFCellStyle;

                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(item.JenjangPendidikan);
                    cell2.CellStyle = sSFCellStyle;

                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.JurusanPendidikan);
                    cell3.CellStyle = sSFCellStyle;

                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(item.JalurPendaftaran);
                    cell4.CellStyle = sSFCellStyle;

                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(item.Nis);
                    cell5.CellStyle = sSFCellStyle;

                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(item.Nama);
                    cell6.CellStyle = sSFCellStyle;

                    ICell cell7 = row.CreateCell(7);
                    cell7.SetCellValue(item.JenisKelamin);
                    cell7.CellStyle = sSFCellStyle;

                    ICell cell8 = row.CreateCell(8);
                    cell8.SetCellValue(item.TempatLahir);
                    cell8.CellStyle = sSFCellStyle;

                    ICell cell9 = row.CreateCell(9);
                    cell9.SetCellValue(item.TanggalLahir);
                    cell9.CellStyle = sSFCellStyle;

                    ICell cell10 = row.CreateCell(10);
                    cell10.SetCellValue(item.Alamat);
                    cell10.CellStyle = sSFCellStyle;

                    ICell cell11 = row.CreateCell(11);
                    cell11.SetCellValue(item.NamaAyah);
                    cell11.CellStyle = sSFCellStyle;

                    ICell cell12 = row.CreateCell(12);
                    cell12.SetCellValue(item.NoHpAyah);
                    cell12.CellStyle = sSFCellStyle;

                    ICell cell13 = row.CreateCell(13);
                    cell13.SetCellValue(item.EmailAyah);
                    cell13.CellStyle = sSFCellStyle;

                    ICell cell14 = row.CreateCell(14);
                    cell14.SetCellValue(item.NamaIbu);
                    cell14.CellStyle = sSFCellStyle;

                    ICell cell15 = row.CreateCell(15);
                    cell15.SetCellValue(item.NoHpIbu);
                    cell15.CellStyle = sSFCellStyle;

                    ICell cell16 = row.CreateCell(16);
                    cell16.SetCellValue(item.EmailIbu);
                    cell16.CellStyle = sSFCellStyle;

                    ICell cell17 = row.CreateCell(17);
                    cell17.SetCellValue(item.NamaPewakaf);
                    cell17.CellStyle = sSFCellStyle;

                    ICell cell18 = row.CreateCell(18);
                    cell18.SetCellValue(item.HubunganSiswa);
                    cell18.CellStyle = sSFCellStyle;

                    ICell cell19 = row.CreateCell(19);
                    cell19.SetCellValue(item.StatusProses);
                    cell19.CellStyle = sSFCellStyle;

                    ICell cell20 = row.CreateCell(20);
                    cell20.SetCellValue(item.StatusPpdb);
                    cell20.CellStyle = sSFCellStyle;

                    i++;
                }


                for (int a = 0; a < 5; a++)
                {
                    xssfwb.RemoveAt(0);
                }

                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);


                xssfwb.Write(filex);
                FileStream file = new FileStream(_fileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                filex.Close();
                return _fileName;
            }
            catch (Exception ex)
            {
                oMessage = ex.Message;
                return string.Empty;
            }
        }
        public string GenerateLaporanGelombangPpdbTest(string TanggalAwal, string TanggalAkhir, PpdbLaporanModel Data, int IdUser, SheetLaporanStatikGelombangPpdbModel DataKuota, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                string _sourceFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/Laporan_gelombang_ppdb1.xlsx");
                string _fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/test.xlsx");
                Console.WriteLine(_fileName);
                if (System.IO.File.Exists(_fileName))
                    System.IO.File.Delete(_fileName);

                XSSFWorkbook xssfwb = new XSSFWorkbook();

                FileStream filex = new FileStream(_sourceFile, FileMode.Open, FileAccess.Read);
                xssfwb = new XSSFWorkbook(filex);


                XSSFCellStyle sSFCellStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
                sSFCellStyle.BorderBottom = BorderStyle.Thin;
                sSFCellStyle.BorderLeft = BorderStyle.Thin;
                sSFCellStyle.BorderRight = BorderStyle.Thin;
                sSFCellStyle.BorderTop = BorderStyle.Thin;


                ISheet sheet0copy = xssfwb.GetSheetAt(0);
                sheet0copy.CopySheet("PPDB TOTAL KESELURUHAN", true);
                ISheet sheet0 = xssfwb.GetSheet("PPDB TOTAL KESELURUHAN");

                var tglAwal = StrToDateTime(TanggalAwal).ToString("dd-MMMM-yyyy", new System.Globalization.CultureInfo("id-ID"));
                var tglAkhir = StrToDateTime(TanggalAkhir).ToString("dd-MMMM-yyyy", new System.Globalization.CultureInfo("id-ID"));
                sheet0.GetRow(1).GetCell(1).SetCellValue(tglAwal + " s/d " + tglAkhir);

                sheet0.GetRow(6).GetCell(3).SetCellValue(DataKuota.KuotaPpdbTkA.TotL);
                sheet0.GetRow(6).GetCell(4).SetCellValue(DataKuota.KuotaPpdbTkA.TotP);
                sheet0.GetRow(6).GetCell(6).SetCellValue(DataKuota.KuotaPpdbTkB.TotL);
                sheet0.GetRow(6).GetCell(7).SetCellValue(DataKuota.KuotaPpdbTkB.TotP);
                sheet0.GetRow(6).GetCell(11).SetCellValue(DataKuota.KuotaPpdbSd.TotL);
                sheet0.GetRow(6).GetCell(12).SetCellValue(DataKuota.KuotaPpdbSd.TotP);
                sheet0.GetRow(6).GetCell(15).SetCellValue(DataKuota.KuotaPpdbSmp.TotL);
                sheet0.GetRow(6).GetCell(16).SetCellValue(DataKuota.KuotaPpdbSmp.TotP);
                sheet0.GetRow(6).GetCell(19).SetCellValue(DataKuota.KuotaPpdbSmaIpa.TotL);
                sheet0.GetRow(6).GetCell(20).SetCellValue(DataKuota.KuotaPpdbSmaIpa.TotP);
                sheet0.GetRow(6).GetCell(21).SetCellValue(DataKuota.KuotaPpdbSmaIps.TotP);
                sheet0.GetRow(6).GetCell(22).SetCellValue(DataKuota.KuotaPpdbSmaIps.TotP);

                // //TKA
                sheet0.GetRow(7).GetCell(3).SetCellValue(Data.Rekap[0].DaftarTungguL);
                sheet0.GetRow(7).GetCell(4).SetCellValue(Data.Rekap[0].DaftarTungguP);
                sheet0.GetRow(8).GetCell(3).SetCellValue(Data.Rekap[0].DaftarL);
                sheet0.GetRow(8).GetCell(4).SetCellValue(Data.Rekap[0].DaftarP);
                sheet0.GetRow(9).GetCell(3).SetCellValue(Data.Rekap[0].TidakBayarDaftarL);
                sheet0.GetRow(9).GetCell(4).SetCellValue(Data.Rekap[0].TidakBayarDaftarP);
                sheet0.GetRow(10).GetCell(3).SetCellValue(Data.Rekap[0].BayarDaftarL);
                sheet0.GetRow(10).GetCell(4).SetCellValue(Data.Rekap[0].BayarDaftarP);
                sheet0.GetRow(11).GetCell(3).SetCellValue(Data.Rekap[0].TidakInputFormulirL);
                sheet0.GetRow(11).GetCell(4).SetCellValue(Data.Rekap[0].TidakInputFormulirP);
                sheet0.GetRow(12).GetCell(3).SetCellValue(Data.Rekap[0].IkutObservasiL);
                sheet0.GetRow(12).GetCell(4).SetCellValue(Data.Rekap[0].IkutObservasiP);
                sheet0.GetRow(13).GetCell(3).SetCellValue(Data.Rekap[0].TidakDirekomendasikanObservasiL);
                sheet0.GetRow(13).GetCell(4).SetCellValue(Data.Rekap[0].TidakDirekomendasikanObservasiP);
                sheet0.GetRow(14).GetCell(3).SetCellValue(Data.Rekap[0].DirekomendasikanObservasiL);
                sheet0.GetRow(14).GetCell(4).SetCellValue(Data.Rekap[0].DirekomendasikanObservasiP);
                sheet0.GetRow(15).GetCell(3).SetCellValue(Data.Rekap[0].VerifHasilObservasiDiterimaL);
                sheet0.GetRow(15).GetCell(4).SetCellValue(Data.Rekap[0].VerifHasilObservasiDiterimaP);
                sheet0.GetRow(16).GetCell(3).SetCellValue(Data.Rekap[0].VerifHasilObservasiTidakDiterimaL);
                sheet0.GetRow(16).GetCell(4).SetCellValue(Data.Rekap[0].VerifHasilObservasiTidakDiterimaP);
                sheet0.GetRow(17).GetCell(3).SetCellValue(Data.Rekap[0].VerifHasilObservasiCadanganL);
                sheet0.GetRow(17).GetCell(4).SetCellValue(Data.Rekap[0].VerifHasilObservasiCadanganP);
                sheet0.GetRow(18).GetCell(3).SetCellValue(Data.Rekap[0].VerifTidakBayarPendidikanL);
                sheet0.GetRow(18).GetCell(4).SetCellValue(Data.Rekap[0].VerifTidakBayarPendidikanP);
                sheet0.GetRow(19).GetCell(3).SetCellValue(Data.Rekap[0].PengesahanL);
                sheet0.GetRow(19).GetCell(4).SetCellValue(Data.Rekap[0].PengesahanP);
                // //TKB
                sheet0.GetRow(7).GetCell(6).SetCellValue(Data.Rekap[1].DaftarTungguL);
                sheet0.GetRow(7).GetCell(7).SetCellValue(Data.Rekap[1].DaftarTungguP);
                sheet0.GetRow(8).GetCell(6).SetCellValue(Data.Rekap[1].DaftarL);
                sheet0.GetRow(8).GetCell(7).SetCellValue(Data.Rekap[1].DaftarP);
                sheet0.GetRow(9).GetCell(6).SetCellValue(Data.Rekap[1].TidakBayarDaftarL);
                sheet0.GetRow(9).GetCell(7).SetCellValue(Data.Rekap[1].TidakBayarDaftarP);
                sheet0.GetRow(10).GetCell(6).SetCellValue(Data.Rekap[1].BayarDaftarL);
                sheet0.GetRow(10).GetCell(7).SetCellValue(Data.Rekap[1].BayarDaftarP);
                sheet0.GetRow(11).GetCell(6).SetCellValue(Data.Rekap[1].TidakInputFormulirL);
                sheet0.GetRow(11).GetCell(7).SetCellValue(Data.Rekap[1].TidakInputFormulirP);
                sheet0.GetRow(12).GetCell(6).SetCellValue(Data.Rekap[1].IkutObservasiL);
                sheet0.GetRow(12).GetCell(7).SetCellValue(Data.Rekap[1].IkutObservasiP);
                sheet0.GetRow(13).GetCell(6).SetCellValue(Data.Rekap[1].TidakDirekomendasikanObservasiL);
                sheet0.GetRow(13).GetCell(7).SetCellValue(Data.Rekap[1].TidakDirekomendasikanObservasiP);
                sheet0.GetRow(14).GetCell(6).SetCellValue(Data.Rekap[1].DirekomendasikanObservasiL);
                sheet0.GetRow(14).GetCell(7).SetCellValue(Data.Rekap[1].DirekomendasikanObservasiP);
                sheet0.GetRow(15).GetCell(6).SetCellValue(Data.Rekap[1].VerifHasilObservasiDiterimaL);
                sheet0.GetRow(15).GetCell(7).SetCellValue(Data.Rekap[1].VerifHasilObservasiDiterimaP);
                sheet0.GetRow(16).GetCell(6).SetCellValue(Data.Rekap[1].VerifHasilObservasiTidakDiterimaL);
                sheet0.GetRow(16).GetCell(7).SetCellValue(Data.Rekap[1].VerifHasilObservasiTidakDiterimaP);
                sheet0.GetRow(17).GetCell(6).SetCellValue(Data.Rekap[1].VerifHasilObservasiCadanganL);
                sheet0.GetRow(17).GetCell(7).SetCellValue(Data.Rekap[1].VerifHasilObservasiCadanganP);
                sheet0.GetRow(18).GetCell(6).SetCellValue(Data.Rekap[1].VerifTidakBayarPendidikanL);
                sheet0.GetRow(18).GetCell(7).SetCellValue(Data.Rekap[1].VerifTidakBayarPendidikanP);
                sheet0.GetRow(19).GetCell(6).SetCellValue(Data.Rekap[1].PengesahanL);
                sheet0.GetRow(19).GetCell(7).SetCellValue(Data.Rekap[1].PengesahanP);
                //SD
                sheet0.GetRow(7).GetCell(11).SetCellValue(Data.Rekap[2].DaftarTungguL);
                sheet0.GetRow(7).GetCell(12).SetCellValue(Data.Rekap[2].DaftarTungguP);
                sheet0.GetRow(8).GetCell(11).SetCellValue(Data.Rekap[2].DaftarL);
                sheet0.GetRow(8).GetCell(12).SetCellValue(Data.Rekap[2].DaftarP);
                sheet0.GetRow(9).GetCell(11).SetCellValue(Data.Rekap[2].TidakBayarDaftarL);
                sheet0.GetRow(9).GetCell(12).SetCellValue(Data.Rekap[2].TidakBayarDaftarP);
                sheet0.GetRow(10).GetCell(11).SetCellValue(Data.Rekap[2].BayarDaftarL);
                sheet0.GetRow(10).GetCell(12).SetCellValue(Data.Rekap[2].BayarDaftarP);
                sheet0.GetRow(11).GetCell(11).SetCellValue(Data.Rekap[2].TidakInputFormulirL);
                sheet0.GetRow(11).GetCell(12).SetCellValue(Data.Rekap[2].TidakInputFormulirP);
                sheet0.GetRow(12).GetCell(11).SetCellValue(Data.Rekap[2].IkutObservasiL);
                sheet0.GetRow(12).GetCell(12).SetCellValue(Data.Rekap[2].IkutObservasiP);
                sheet0.GetRow(13).GetCell(11).SetCellValue(Data.Rekap[2].TidakDirekomendasikanObservasiL);
                sheet0.GetRow(13).GetCell(12).SetCellValue(Data.Rekap[2].TidakDirekomendasikanObservasiP);
                sheet0.GetRow(14).GetCell(11).SetCellValue(Data.Rekap[2].DirekomendasikanObservasiL);
                sheet0.GetRow(14).GetCell(12).SetCellValue(Data.Rekap[2].DirekomendasikanObservasiP);
                sheet0.GetRow(15).GetCell(11).SetCellValue(Data.Rekap[2].VerifHasilObservasiDiterimaL);
                sheet0.GetRow(15).GetCell(12).SetCellValue(Data.Rekap[2].VerifHasilObservasiDiterimaP);
                sheet0.GetRow(16).GetCell(11).SetCellValue(Data.Rekap[2].VerifHasilObservasiTidakDiterimaL);
                sheet0.GetRow(16).GetCell(12).SetCellValue(Data.Rekap[2].VerifHasilObservasiTidakDiterimaP);
                sheet0.GetRow(17).GetCell(11).SetCellValue(Data.Rekap[2].VerifHasilObservasiCadanganL);
                sheet0.GetRow(17).GetCell(12).SetCellValue(Data.Rekap[2].VerifHasilObservasiCadanganP);
                sheet0.GetRow(18).GetCell(11).SetCellValue(Data.Rekap[2].VerifTidakBayarPendidikanL);
                sheet0.GetRow(18).GetCell(12).SetCellValue(Data.Rekap[2].VerifTidakBayarPendidikanP);
                sheet0.GetRow(19).GetCell(11).SetCellValue(Data.Rekap[2].PengesahanL);
                sheet0.GetRow(19).GetCell(12).SetCellValue(Data.Rekap[2].PengesahanP);
                // //SMP
                sheet0.GetRow(7).GetCell(15).SetCellValue(Data.Rekap[3].DaftarTungguL);
                sheet0.GetRow(7).GetCell(16).SetCellValue(Data.Rekap[3].DaftarTungguP);
                sheet0.GetRow(8).GetCell(15).SetCellValue(Data.Rekap[3].DaftarL);
                sheet0.GetRow(8).GetCell(16).SetCellValue(Data.Rekap[3].DaftarP);
                sheet0.GetRow(9).GetCell(15).SetCellValue(Data.Rekap[3].TidakBayarDaftarL);
                sheet0.GetRow(9).GetCell(16).SetCellValue(Data.Rekap[3].TidakBayarDaftarP);
                sheet0.GetRow(10).GetCell(15).SetCellValue(Data.Rekap[3].BayarDaftarL);
                sheet0.GetRow(10).GetCell(16).SetCellValue(Data.Rekap[3].BayarDaftarP);
                sheet0.GetRow(11).GetCell(15).SetCellValue(Data.Rekap[3].TidakInputFormulirL);
                sheet0.GetRow(11).GetCell(16).SetCellValue(Data.Rekap[3].TidakInputFormulirP);
                sheet0.GetRow(12).GetCell(15).SetCellValue(Data.Rekap[3].IkutObservasiL);
                sheet0.GetRow(12).GetCell(16).SetCellValue(Data.Rekap[3].IkutObservasiP);
                sheet0.GetRow(13).GetCell(15).SetCellValue(Data.Rekap[3].TidakDirekomendasikanObservasiL);
                sheet0.GetRow(13).GetCell(16).SetCellValue(Data.Rekap[3].TidakDirekomendasikanObservasiP);
                sheet0.GetRow(14).GetCell(15).SetCellValue(Data.Rekap[3].DirekomendasikanObservasiL);
                sheet0.GetRow(14).GetCell(16).SetCellValue(Data.Rekap[3].DirekomendasikanObservasiP);
                sheet0.GetRow(15).GetCell(15).SetCellValue(Data.Rekap[3].VerifHasilObservasiDiterimaL);
                sheet0.GetRow(15).GetCell(16).SetCellValue(Data.Rekap[3].VerifHasilObservasiDiterimaP);
                sheet0.GetRow(16).GetCell(15).SetCellValue(Data.Rekap[3].VerifHasilObservasiTidakDiterimaL);
                sheet0.GetRow(16).GetCell(16).SetCellValue(Data.Rekap[3].VerifHasilObservasiTidakDiterimaP);
                sheet0.GetRow(17).GetCell(15).SetCellValue(Data.Rekap[3].VerifHasilObservasiCadanganL);
                sheet0.GetRow(17).GetCell(16).SetCellValue(Data.Rekap[3].VerifHasilObservasiCadanganP);
                sheet0.GetRow(18).GetCell(15).SetCellValue(Data.Rekap[3].VerifTidakBayarPendidikanL);
                sheet0.GetRow(18).GetCell(16).SetCellValue(Data.Rekap[3].VerifTidakBayarPendidikanP);
                sheet0.GetRow(19).GetCell(15).SetCellValue(Data.Rekap[3].PengesahanL);
                sheet0.GetRow(19).GetCell(16).SetCellValue(Data.Rekap[3].PengesahanP);
                // //SMA IPA
                sheet0.GetRow(7).GetCell(19).SetCellValue(Data.Rekap[4].DaftarTungguL);
                sheet0.GetRow(7).GetCell(20).SetCellValue(Data.Rekap[4].DaftarTungguP);
                sheet0.GetRow(8).GetCell(19).SetCellValue(Data.Rekap[4].DaftarL);
                sheet0.GetRow(8).GetCell(20).SetCellValue(Data.Rekap[4].DaftarP);
                sheet0.GetRow(9).GetCell(19).SetCellValue(Data.Rekap[4].TidakBayarDaftarL);
                sheet0.GetRow(9).GetCell(20).SetCellValue(Data.Rekap[4].TidakBayarDaftarP);
                sheet0.GetRow(10).GetCell(19).SetCellValue(Data.Rekap[4].BayarDaftarL);
                sheet0.GetRow(10).GetCell(20).SetCellValue(Data.Rekap[4].BayarDaftarP);
                sheet0.GetRow(11).GetCell(19).SetCellValue(Data.Rekap[4].TidakInputFormulirL);
                sheet0.GetRow(11).GetCell(20).SetCellValue(Data.Rekap[4].TidakInputFormulirP);
                sheet0.GetRow(12).GetCell(19).SetCellValue(Data.Rekap[4].IkutObservasiL);
                sheet0.GetRow(12).GetCell(20).SetCellValue(Data.Rekap[4].IkutObservasiP);
                sheet0.GetRow(13).GetCell(19).SetCellValue(Data.Rekap[4].TidakDirekomendasikanObservasiL);
                sheet0.GetRow(13).GetCell(20).SetCellValue(Data.Rekap[4].TidakDirekomendasikanObservasiP);
                sheet0.GetRow(14).GetCell(19).SetCellValue(Data.Rekap[4].DirekomendasikanObservasiL);
                sheet0.GetRow(14).GetCell(20).SetCellValue(Data.Rekap[4].DirekomendasikanObservasiP);
                sheet0.GetRow(15).GetCell(19).SetCellValue(Data.Rekap[4].VerifHasilObservasiDiterimaL);
                sheet0.GetRow(15).GetCell(20).SetCellValue(Data.Rekap[4].VerifHasilObservasiDiterimaP);
                sheet0.GetRow(16).GetCell(19).SetCellValue(Data.Rekap[4].VerifHasilObservasiTidakDiterimaL);
                sheet0.GetRow(16).GetCell(20).SetCellValue(Data.Rekap[4].VerifHasilObservasiTidakDiterimaP);
                sheet0.GetRow(17).GetCell(19).SetCellValue(Data.Rekap[4].VerifHasilObservasiCadanganL);
                sheet0.GetRow(17).GetCell(20).SetCellValue(Data.Rekap[4].VerifHasilObservasiCadanganP);
                sheet0.GetRow(18).GetCell(19).SetCellValue(Data.Rekap[4].VerifTidakBayarPendidikanL);
                sheet0.GetRow(18).GetCell(20).SetCellValue(Data.Rekap[4].VerifTidakBayarPendidikanP);
                sheet0.GetRow(19).GetCell(19).SetCellValue(Data.Rekap[4].PengesahanL);
                sheet0.GetRow(19).GetCell(20).SetCellValue(Data.Rekap[4].PengesahanP);
                // //SMA IPS
                sheet0.GetRow(7).GetCell(21).SetCellValue(Data.Rekap[5].DaftarTungguL);
                sheet0.GetRow(7).GetCell(22).SetCellValue(Data.Rekap[5].DaftarTungguP);
                sheet0.GetRow(8).GetCell(21).SetCellValue(Data.Rekap[5].DaftarL);
                sheet0.GetRow(8).GetCell(22).SetCellValue(Data.Rekap[5].DaftarP);
                sheet0.GetRow(9).GetCell(21).SetCellValue(Data.Rekap[5].TidakBayarDaftarL);
                sheet0.GetRow(9).GetCell(22).SetCellValue(Data.Rekap[5].TidakBayarDaftarP);
                sheet0.GetRow(10).GetCell(21).SetCellValue(Data.Rekap[5].BayarDaftarL);
                sheet0.GetRow(10).GetCell(22).SetCellValue(Data.Rekap[5].BayarDaftarP);
                sheet0.GetRow(11).GetCell(21).SetCellValue(Data.Rekap[5].TidakInputFormulirL);
                sheet0.GetRow(11).GetCell(22).SetCellValue(Data.Rekap[5].TidakInputFormulirP);
                sheet0.GetRow(12).GetCell(21).SetCellValue(Data.Rekap[5].IkutObservasiL);
                sheet0.GetRow(12).GetCell(22).SetCellValue(Data.Rekap[5].IkutObservasiP);
                sheet0.GetRow(13).GetCell(21).SetCellValue(Data.Rekap[5].TidakDirekomendasikanObservasiL);
                sheet0.GetRow(13).GetCell(22).SetCellValue(Data.Rekap[5].TidakDirekomendasikanObservasiP);
                sheet0.GetRow(14).GetCell(21).SetCellValue(Data.Rekap[5].DirekomendasikanObservasiL);
                sheet0.GetRow(14).GetCell(22).SetCellValue(Data.Rekap[5].DirekomendasikanObservasiP);
                sheet0.GetRow(15).GetCell(21).SetCellValue(Data.Rekap[5].VerifHasilObservasiDiterimaL);
                sheet0.GetRow(15).GetCell(22).SetCellValue(Data.Rekap[5].VerifHasilObservasiDiterimaP);
                sheet0.GetRow(16).GetCell(21).SetCellValue(Data.Rekap[5].VerifHasilObservasiTidakDiterimaL);
                sheet0.GetRow(16).GetCell(22).SetCellValue(Data.Rekap[5].VerifHasilObservasiTidakDiterimaP);
                sheet0.GetRow(17).GetCell(21).SetCellValue(Data.Rekap[5].VerifHasilObservasiCadanganL);
                sheet0.GetRow(17).GetCell(22).SetCellValue(Data.Rekap[5].VerifHasilObservasiCadanganP);
                sheet0.GetRow(18).GetCell(21).SetCellValue(Data.Rekap[5].VerifTidakBayarPendidikanL);
                sheet0.GetRow(18).GetCell(22).SetCellValue(Data.Rekap[5].VerifTidakBayarPendidikanP);
                sheet0.GetRow(19).GetCell(21).SetCellValue(Data.Rekap[5].PengesahanL);
                sheet0.GetRow(19).GetCell(22).SetCellValue(Data.Rekap[5].PengesahanP);


                ISheet sheet2copy = xssfwb.GetSheetAt(1);
                sheet2copy.CopySheet("Siswa TK A dan TK B", true);
                ISheet sheet2 = xssfwb.GetSheet("Siswa TK A dan TK B");
                int i = 3;
                foreach (var item in Data.Tk)
                {
                    IRow row = sheet2.CreateRow(i);
                    ICell cell = row.CreateCell(0);
                    cell.SetCellValue(i - 2);
                    cell.CellStyle = sSFCellStyle;

                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(item.IdPpdbDaftar);
                    cell1.CellStyle = sSFCellStyle;

                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(item.JenjangPendidikan);
                    cell2.CellStyle = sSFCellStyle;

                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.JalurPendaftaran);
                    cell3.CellStyle = sSFCellStyle;

                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(item.Nis);
                    cell4.CellStyle = sSFCellStyle;

                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(item.Nama);
                    cell5.CellStyle = sSFCellStyle;

                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(item.JenisKelamin);
                    cell6.CellStyle = sSFCellStyle;

                    ICell cell7 = row.CreateCell(7);
                    cell7.SetCellValue(item.TempatLahir);
                    cell7.CellStyle = sSFCellStyle;

                    ICell cell8 = row.CreateCell(8);
                    cell8.SetCellValue(item.TanggalLahir);
                    cell8.CellStyle = sSFCellStyle;

                    ICell cell9 = row.CreateCell(9);
                    cell9.SetCellValue(item.Alamat);
                    cell9.CellStyle = sSFCellStyle;

                    ICell cell10 = row.CreateCell(10);
                    cell10.SetCellValue(item.NamaAyah);
                    cell10.CellStyle = sSFCellStyle;

                    ICell cell11 = row.CreateCell(11);
                    cell11.SetCellValue(item.NoHpAyah);
                    cell11.CellStyle = sSFCellStyle;

                    ICell cell12 = row.CreateCell(12);
                    cell12.SetCellValue(item.EmailAyah);
                    cell12.CellStyle = sSFCellStyle;

                    ICell cell13 = row.CreateCell(13);
                    cell13.SetCellValue(item.NamaIbu);
                    cell13.CellStyle = sSFCellStyle;

                    ICell cell14 = row.CreateCell(14);
                    cell14.SetCellValue(item.NoHpIbu);
                    cell14.CellStyle = sSFCellStyle;

                    ICell cell15 = row.CreateCell(15);
                    cell15.SetCellValue(item.EmailIbu);
                    cell15.CellStyle = sSFCellStyle;

                    ICell cell16 = row.CreateCell(16);
                    cell16.SetCellValue(item.NamaPewakaf);
                    cell16.CellStyle = sSFCellStyle;

                    ICell cell17 = row.CreateCell(17);
                    cell17.SetCellValue(item.HubunganSiswa);
                    cell17.CellStyle = sSFCellStyle;

                    ICell cell18 = row.CreateCell(18);
                    cell18.SetCellValue(item.StatusProses);
                    cell18.CellStyle = sSFCellStyle;

                    ICell cell19 = row.CreateCell(19);
                    cell19.SetCellValue(item.StatusPpdb);
                    cell19.CellStyle = sSFCellStyle;

                    int _cell = 19;
                    foreach (var status in item.StatusProsess)
                    {
                        ICell cell20 = row.CreateCell(_cell);
                        cell20.SetCellValue(status.StatusProses);
                        cell20.CellStyle = sSFCellStyle;
                        _cell++;
                    }


                    i++;
                }

                ISheet sheet3copy = xssfwb.GetSheetAt(2);
                sheet3copy.CopySheet("Siswa SD", true);
                ISheet sheet3 = xssfwb.GetSheet("Siswa SD");
                i = 3;
                foreach (var item in Data.Sd)
                {
                    IRow row = sheet3.CreateRow(i);
                    ICell cell = row.CreateCell(0);
                    cell.SetCellValue(i - 2);
                    cell.CellStyle = sSFCellStyle;

                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(item.IdPpdbDaftar);
                    cell1.CellStyle = sSFCellStyle;

                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(item.JenjangPendidikan);
                    cell2.CellStyle = sSFCellStyle;

                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.JalurPendaftaran);
                    cell3.CellStyle = sSFCellStyle;

                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(item.Nis);
                    cell4.CellStyle = sSFCellStyle;

                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(item.Nama);
                    cell5.CellStyle = sSFCellStyle;

                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(item.JenisKelamin);
                    cell6.CellStyle = sSFCellStyle;

                    ICell cell7 = row.CreateCell(7);
                    cell7.SetCellValue(item.TempatLahir);
                    cell7.CellStyle = sSFCellStyle;

                    ICell cell8 = row.CreateCell(8);
                    cell8.SetCellValue(item.TanggalLahir);
                    cell8.CellStyle = sSFCellStyle;

                    ICell cell9 = row.CreateCell(9);
                    cell9.SetCellValue(item.Alamat);
                    cell9.CellStyle = sSFCellStyle;

                    ICell cell10 = row.CreateCell(10);
                    cell10.SetCellValue(item.NamaAyah);
                    cell10.CellStyle = sSFCellStyle;

                    ICell cell11 = row.CreateCell(11);
                    cell11.SetCellValue(item.NoHpAyah);
                    cell11.CellStyle = sSFCellStyle;

                    ICell cell12 = row.CreateCell(12);
                    cell12.SetCellValue(item.EmailAyah);
                    cell12.CellStyle = sSFCellStyle;

                    ICell cell13 = row.CreateCell(13);
                    cell13.SetCellValue(item.NamaIbu);
                    cell13.CellStyle = sSFCellStyle;

                    ICell cell14 = row.CreateCell(14);
                    cell14.SetCellValue(item.NoHpIbu);
                    cell14.CellStyle = sSFCellStyle;

                    ICell cell15 = row.CreateCell(15);
                    cell15.SetCellValue(item.EmailIbu);
                    cell15.CellStyle = sSFCellStyle;

                    ICell cell16 = row.CreateCell(16);
                    cell16.SetCellValue(item.NamaPewakaf);
                    cell16.CellStyle = sSFCellStyle;

                    ICell cell17 = row.CreateCell(17);
                    cell17.SetCellValue(item.HubunganSiswa);
                    cell17.CellStyle = sSFCellStyle;

                    ICell cell18 = row.CreateCell(18);
                    cell18.SetCellValue(item.StatusProses);
                    cell18.CellStyle = sSFCellStyle;

                    ICell cell19 = row.CreateCell(19);
                    cell19.SetCellValue(item.StatusPpdb);
                    cell19.CellStyle = sSFCellStyle;

                    int _cell = 19;
                    foreach (var status in item.StatusProsess)
                    {
                        ICell cell20 = row.CreateCell(_cell);
                        cell20.SetCellValue(status.StatusProses);
                        cell20.CellStyle = sSFCellStyle;
                        _cell++;
                    }

                    i++;
                }

                ISheet sheet4copy = xssfwb.GetSheetAt(3);
                sheet4copy.CopySheet("Siswa SMP", true);
                ISheet sheet4 = xssfwb.GetSheet("Siswa SMP");
                i = 3;
                foreach (var item in Data.Smp)
                {
                    IRow row = sheet4.CreateRow(i);
                    ICell cell = row.CreateCell(0);
                    cell.SetCellValue(i - 2);
                    cell.CellStyle = sSFCellStyle;

                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(item.IdPpdbDaftar);
                    cell1.CellStyle = sSFCellStyle;

                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(item.JenjangPendidikan);
                    cell2.CellStyle = sSFCellStyle;

                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.JalurPendaftaran);
                    cell3.CellStyle = sSFCellStyle;

                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(item.Nis);
                    cell4.CellStyle = sSFCellStyle;

                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(item.Nama);
                    cell5.CellStyle = sSFCellStyle;

                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(item.JenisKelamin);
                    cell6.CellStyle = sSFCellStyle;

                    ICell cell7 = row.CreateCell(7);
                    cell7.SetCellValue(item.TempatLahir);
                    cell7.CellStyle = sSFCellStyle;

                    ICell cell8 = row.CreateCell(8);
                    cell8.SetCellValue(item.TanggalLahir);
                    cell8.CellStyle = sSFCellStyle;

                    ICell cell9 = row.CreateCell(9);
                    cell9.SetCellValue(item.Alamat);
                    cell9.CellStyle = sSFCellStyle;

                    ICell cell10 = row.CreateCell(10);
                    cell10.SetCellValue(item.NamaAyah);
                    cell10.CellStyle = sSFCellStyle;

                    ICell cell11 = row.CreateCell(11);
                    cell11.SetCellValue(item.NoHpAyah);
                    cell11.CellStyle = sSFCellStyle;

                    ICell cell12 = row.CreateCell(12);
                    cell12.SetCellValue(item.EmailAyah);
                    cell12.CellStyle = sSFCellStyle;

                    ICell cell13 = row.CreateCell(13);
                    cell13.SetCellValue(item.NamaIbu);
                    cell13.CellStyle = sSFCellStyle;

                    ICell cell14 = row.CreateCell(14);
                    cell14.SetCellValue(item.NoHpIbu);
                    cell14.CellStyle = sSFCellStyle;

                    ICell cell15 = row.CreateCell(15);
                    cell15.SetCellValue(item.EmailIbu);
                    cell15.CellStyle = sSFCellStyle;

                    ICell cell16 = row.CreateCell(16);
                    cell16.SetCellValue(item.NamaPewakaf);
                    cell16.CellStyle = sSFCellStyle;

                    ICell cell17 = row.CreateCell(17);
                    cell17.SetCellValue(item.HubunganSiswa);
                    cell17.CellStyle = sSFCellStyle;

                    ICell cell18 = row.CreateCell(18);
                    cell18.SetCellValue(item.StatusProses);
                    cell18.CellStyle = sSFCellStyle;

                    ICell cell19 = row.CreateCell(19);
                    cell19.SetCellValue(item.StatusPpdb);
                    cell19.CellStyle = sSFCellStyle;

                    int _cell = 19;
                    foreach (var status in item.StatusProsess)
                    {
                        ICell cell20 = row.CreateCell(_cell);
                        cell20.SetCellValue(status.StatusProses);
                        cell20.CellStyle = sSFCellStyle;
                        _cell++;
                    }

                    i++;
                }

                ISheet sheet5copy = xssfwb.GetSheetAt(4);
                sheet5copy.CopySheet("Siswa SMA", true);
                ISheet sheet5 = xssfwb.GetSheet("Siswa SMA");
                i = 3;
                foreach (var item in Data.Sma)
                {
                    IRow row = sheet5.CreateRow(i);
                    ICell cell = row.CreateCell(0);
                    cell.SetCellValue(i - 2);
                    cell.CellStyle = sSFCellStyle;

                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(item.IdPpdbDaftar);
                    cell1.CellStyle = sSFCellStyle;

                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(item.JenjangPendidikan);
                    cell2.CellStyle = sSFCellStyle;

                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.JurusanPendidikan);
                    cell3.CellStyle = sSFCellStyle;

                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(item.JalurPendaftaran);
                    cell4.CellStyle = sSFCellStyle;

                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(item.Nis);
                    cell5.CellStyle = sSFCellStyle;

                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(item.Nama);
                    cell6.CellStyle = sSFCellStyle;

                    ICell cell7 = row.CreateCell(7);
                    cell7.SetCellValue(item.JenisKelamin);
                    cell7.CellStyle = sSFCellStyle;

                    ICell cell8 = row.CreateCell(8);
                    cell8.SetCellValue(item.TempatLahir);
                    cell8.CellStyle = sSFCellStyle;

                    ICell cell9 = row.CreateCell(9);
                    cell9.SetCellValue(item.TanggalLahir);
                    cell9.CellStyle = sSFCellStyle;

                    ICell cell10 = row.CreateCell(10);
                    cell10.SetCellValue(item.Alamat);
                    cell10.CellStyle = sSFCellStyle;

                    ICell cell11 = row.CreateCell(11);
                    cell11.SetCellValue(item.NamaAyah);
                    cell11.CellStyle = sSFCellStyle;

                    ICell cell12 = row.CreateCell(12);
                    cell12.SetCellValue(item.NoHpAyah);
                    cell12.CellStyle = sSFCellStyle;

                    ICell cell13 = row.CreateCell(13);
                    cell13.SetCellValue(item.EmailAyah);
                    cell13.CellStyle = sSFCellStyle;

                    ICell cell14 = row.CreateCell(14);
                    cell14.SetCellValue(item.NamaIbu);
                    cell14.CellStyle = sSFCellStyle;

                    ICell cell15 = row.CreateCell(15);
                    cell15.SetCellValue(item.NoHpIbu);
                    cell15.CellStyle = sSFCellStyle;

                    ICell cell16 = row.CreateCell(16);
                    cell16.SetCellValue(item.EmailIbu);
                    cell16.CellStyle = sSFCellStyle;

                    ICell cell17 = row.CreateCell(17);
                    cell17.SetCellValue(item.NamaPewakaf);
                    cell17.CellStyle = sSFCellStyle;

                    ICell cell18 = row.CreateCell(18);
                    cell18.SetCellValue(item.HubunganSiswa);
                    cell18.CellStyle = sSFCellStyle;

                    ICell cell19 = row.CreateCell(19);
                    cell19.SetCellValue(item.StatusProses);
                    cell19.CellStyle = sSFCellStyle;

                    ICell cell20 = row.CreateCell(20);
                    cell20.SetCellValue(item.StatusPpdb);
                    cell20.CellStyle = sSFCellStyle;

                    int _cell = 20;
                    foreach (var status in item.StatusProsess)
                    {
                        ICell cell21 = row.CreateCell(_cell);
                        cell21.SetCellValue(status.StatusProses);
                        cell21.CellStyle = sSFCellStyle;
                        _cell++;
                    }

                    i++;
                }

                for (int a = 0; a < 5; a++)
                {
                    xssfwb.RemoveAt(0);
                }

                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);


                xssfwb.Write(filex);
                FileStream file = new FileStream(_fileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                filex.Close();
                return _fileName;
            }
            catch (Exception ex)
            {
                oMessage = ex.Message;
                return string.Empty;
            }
        }

        public string GenerateLaporanElearningProgressPegawai(string TanggalAwal, string TanggalAkhir, List<SheetLaporanElearningProgressPegawai> Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                string _sourceFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/Laporan_elearning_progress_pegawai.xlsx");
                string _fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/test.xlsx");
                Console.WriteLine(_fileName);
                if (System.IO.File.Exists(_fileName))
                    System.IO.File.Delete(_fileName);

                XSSFWorkbook xssfwb = new XSSFWorkbook();

                FileStream filex = new FileStream(_sourceFile, FileMode.Open, FileAccess.Read);
                xssfwb = new XSSFWorkbook(filex);


                XSSFCellStyle sSFCellStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
                sSFCellStyle.BorderBottom = BorderStyle.Thin;
                sSFCellStyle.BorderLeft = BorderStyle.Thin;
                sSFCellStyle.BorderRight = BorderStyle.Thin;
                sSFCellStyle.BorderTop = BorderStyle.Thin;


                ISheet sheet1copy = xssfwb.GetSheetAt(0);
                sheet1copy.CopySheet("Laporan Elearning", true);
                ISheet sheet1 = xssfwb.GetSheet("Laporan Elearning");
                var tglAwal = StrToDateTime(TanggalAwal).ToString("dd-MMMM-yyyy", new System.Globalization.CultureInfo("id-ID"));
                var tglAkhir = StrToDateTime(TanggalAkhir).ToString("dd-MMMM-yyyy", new System.Globalization.CultureInfo("id-ID"));
                sheet1.GetRow(0).GetCell(0).SetCellValue(string.Format("LAPORAN ELEARNING TANGGAL {0} SAMPAI TANGGAL {1}", tglAwal.ToUpper(), tglAkhir.ToUpper()));
                sheet1.GetRow(1).GetCell(0).SetCellValue(string.Format("UNIT KERJA : {0}", Data[0].UnitKerja));

                int i = 5;
                foreach (var item in Data)
                {
                    IRow row = sheet1.CreateRow(i);
                    ICell cell = row.CreateCell(0);
                    cell.SetCellValue(i - 2);
                    cell.CellStyle = sSFCellStyle;

                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(item.Nip);
                    cell1.CellStyle = sSFCellStyle;

                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(item.NamaPegawai);
                    cell2.CellStyle = sSFCellStyle;

                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.Materi);
                    cell3.CellStyle = sSFCellStyle;

                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(item.Soal);
                    cell4.CellStyle = sSFCellStyle;

                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(item.Menilai);
                    cell5.CellStyle = sSFCellStyle;

                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(item.KehadiranSiswa);
                    cell6.CellStyle = sSFCellStyle;

                    ICell cell7 = row.CreateCell(7);
                    cell7.SetCellValue(item.Keterangan);
                    cell7.CellStyle = sSFCellStyle;

                    i++;
                }

                for (int a = 0; a < 1; a++)
                {
                    xssfwb.RemoveAt(0);
                }

                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);


                xssfwb.Write(filex);
                FileStream file = new FileStream(_fileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                filex.Close();
                return _fileName;
            }
            catch (Exception ex)
            {
                oMessage = ex.Message;
                return string.Empty;
            }
        }

        public KelasQuranAllExcelModel ReadExcelKelompokQiroaty(IFormFile file, out string oMessage)
        {
            string err = string.Empty;
            try
            {
                oMessage = string.Empty;
                if (file == null || file.Length == 0)
                {
                    oMessage = "file tidak sesuai";
                    return null;
                }
                string fileExtension = Path.GetExtension(file.FileName);
                if (fileExtension != ".xlsx") { oMessage = string.Format("format file {0} salah", fileExtension); return null; }

                XSSFWorkbook xssfwb = new XSSFWorkbook();
                Stream fileStream = file.OpenReadStream();
                KelasQuranAllExcelModel ret = new KelasQuranAllExcelModel
                {
                    KelasQuran = new List<KelasQuranExcelModel>(),
                    KelasQuranJadwal = new List<KelasQuranJadwalExcelModel>(),
                    KelasQuranSiswa = new List<KelasQuranSiswaExcelModel>()
                };

                //kelompok
                xssfwb = new XSSFWorkbook(fileStream);
                ISheet sheet = xssfwb.GetSheetAt(0);
                for (int row = 1; row <= sheet.LastRowNum; row++)
                {
                    IRow cells = sheet.GetRow(row);
                    if (cells != null)
                    {
                        if (sheet.GetRow(row).GetCell(1) == null) continue;

                        KelasQuranExcelModel m = new KelasQuranExcelModel();

                        int cellNo = 0;
                        ICell cell = cells.GetCell(cellNo);
                        if (cell == null) { oMessage = GetCellErrorMessage(sheet.SheetName, row, cellNo); ; return null; }
                        m.Kode = cell.ToString();

                        var strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.NamaKelompok = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.NipGuru = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.NamaGuru = strValue;

                        ++cellNo;

                        ret.KelasQuran.Add(m);
                    }
                }

                //jadwal
                sheet = xssfwb.GetSheetAt(1);
                for (int row = 1; row <= sheet.LastRowNum; row++)
                {
                    IRow cells = sheet.GetRow(row);
                    if (cells != null)
                    {
                        if (sheet.GetRow(row).GetCell(1) == null) continue;

                        KelasQuranJadwalExcelModel m = new KelasQuranJadwalExcelModel();

                        int cellNo = 0;
                        ICell cell = cells.GetCell(cellNo);
                        if (cell == null) { oMessage = GetCellErrorMessage(sheet.SheetName, row, cellNo); ; return null; }
                        m.Kode = cell.ToString();

                        var strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Hari = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.JamMulai = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.JamSelesai = strValue;

                        ++cellNo;

                        ret.KelasQuranJadwal.Add(m);
                    }
                }

                //siswa
                sheet = xssfwb.GetSheetAt(2);
                for (int row = 1; row <= sheet.LastRowNum; row++)
                {
                    IRow cells = sheet.GetRow(row);
                    if (cells != null)
                    {
                        if (sheet.GetRow(row).GetCell(1) == null) continue;

                        KelasQuranSiswaExcelModel m = new KelasQuranSiswaExcelModel();

                        int cellNo = 0;
                        ICell cell = cells.GetCell(cellNo);
                        if (cell == null) { oMessage = GetCellErrorMessage(sheet.SheetName, row, cellNo); ; return null; }
                        m.Kode = cell.ToString();

                        var strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Nis = strValue;

                        ++cellNo;

                        ret.KelasQuranSiswa.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = ex.Message + Environment.NewLine + err;
                return null;
            }

        }
        public List<KelasQuranJadwalExcelModel> ReadExcelJadwalKelompokQiroaty(IFormFile file, out string oMessage)
        {
            string err = string.Empty;
            try
            {
                oMessage = string.Empty;
                if (file == null || file.Length == 0)
                {
                    oMessage = "file tidak sesuai";
                    return null;
                }
                string fileExtension = Path.GetExtension(file.FileName);
                if (fileExtension != ".xlsx") { oMessage = string.Format("format file {0} salah", fileExtension); return null; }

                XSSFWorkbook xssfwb = new XSSFWorkbook();
                Stream fileStream = file.OpenReadStream();
                List<KelasQuranJadwalExcelModel> ret = new List<KelasQuranJadwalExcelModel>();

                xssfwb = new XSSFWorkbook(fileStream);
                ISheet sheet = xssfwb.GetSheetAt(0);
                for (int row = 1; row <= sheet.LastRowNum; row++)
                {
                    IRow cells = sheet.GetRow(row);
                    if (cells != null)
                    {
                        if (sheet.GetRow(row).GetCell(1) == null) continue;

                        KelasQuranJadwalExcelModel m = new KelasQuranJadwalExcelModel();

                        int cellNo = 0;
                        ICell cell = cells.GetCell(cellNo);
                        if (cell == null) { oMessage = GetCellErrorMessage(sheet.SheetName, row, cellNo); ; return null; }
                        m.Kode = cell.ToString();

                        var strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Hari = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.JamMulai = strValue;

                        strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.JamSelesai = strValue;

                        ++cellNo;

                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = ex.Message + Environment.NewLine + err;
                return null;
            }

        }
        public List<KelasQuranSiswaExcelModel> ReadExcelSiswaKelompokQiroaty(IFormFile file, out string oMessage)
        {
            string err = string.Empty;
            try
            {
                oMessage = string.Empty;
                if (file == null || file.Length == 0)
                {
                    oMessage = "file tidak sesuai";
                    return null;
                }
                string fileExtension = Path.GetExtension(file.FileName);
                if (fileExtension != ".xlsx") { oMessage = string.Format("format file {0} salah", fileExtension); return null; }

                XSSFWorkbook xssfwb = new XSSFWorkbook();
                Stream fileStream = file.OpenReadStream();
                List<KelasQuranSiswaExcelModel> ret = new List<KelasQuranSiswaExcelModel>();

                xssfwb = new XSSFWorkbook(fileStream);
                ISheet sheet = xssfwb.GetSheetAt(0);
                for (int row = 1; row <= sheet.LastRowNum; row++)
                {
                    IRow cells = sheet.GetRow(row);
                    if (cells != null)
                    {
                        if (sheet.GetRow(row).GetCell(1) == null) continue;

                        KelasQuranSiswaExcelModel m = new KelasQuranSiswaExcelModel();

                        int cellNo = 0;
                        ICell cell = cells.GetCell(cellNo);
                        if (cell == null) { oMessage = GetCellErrorMessage(sheet.SheetName, row, cellNo); ; return null; }
                        m.Kode = cell.ToString();

                        var strValue = GetStringValue(sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Nis = strValue;

                        ++cellNo;

                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = ex.Message + Environment.NewLine + err;
                return null;
            }

        }

    }
}
