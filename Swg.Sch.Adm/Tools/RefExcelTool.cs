﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Swg.Sch.Adm.Tools
{
    public class RefExcelTool
    {
        private readonly string ServiceName = "Swg.Sch.Adm.Tools.RefExcelTool.";
        public RefExcelTool()
        {
        }
        public string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }
        public DateTime? StrToDateTime(string StrDate, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                return StrToDateTime(StrDate);
            }
            catch (Exception ex)
            {
                oMessage = "format tanggal salah";
                return null;
            }
            if (StrDate.Length == 10)
            {
                return DateTime.ParseExact(StrDate.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            else
            {
                return DateTime.ParseExact(StrDate.Replace("/", "-"), "dd-MM-yyyy HH24:mm:ss", CultureInfo.InvariantCulture);
            }
        }
        public DateTime StrToDateTime(string StrDate)
        {
            if (StrDate.Length == 10)
            {
                return DateTime.ParseExact(StrDate.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            else
            {
                return DateTime.ParseExact(StrDate.Replace("/", "-"), "dd-MM-yyyy HH24:mm:ss", CultureInfo.InvariantCulture);
            }
        }
        public string SetNoHandphone(string NoHp)
        {
            NoHp = NoHp.Replace("+62", "");
            NoHp = NoHp.Replace(" ", "");
            NoHp = NoHp.Replace("-", "");
            NoHp = NoHp.Trim();
            if (string.IsNullOrEmpty(NoHp)) return "-";
            if (NoHp.IndexOf('/') > 0)
                NoHp = NoHp.Substring(0, NoHp.IndexOf("/"));
            if (NoHp.Substring(0, 1) != "0") NoHp = "0" + NoHp;
            return NoHp;
        }
        public string GetColumnName(int index)
        {
            const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            var value = "";

            if (index >= letters.Length)
                value += letters[index / letters.Length - 1];

            value += letters[index % letters.Length];

            return value;
        }
        public string GetErrorMessage(string MethodeName, Exception ex)
        {
            string errMessage = ex.Message;
            if (ex.InnerException != null)
            {
                errMessage = ex.InnerException.Message;
                if (ex.InnerException.InnerException != null)
                {
                    errMessage = ex.InnerException.InnerException.Message;
                    if (ex.InnerException.InnerException.InnerException != null)
                    {
                        errMessage = ex.InnerException.InnerException.InnerException.Message;
                    }
                }
            }
            var lineNumber = 0;
            const string lineSearch = ":line ";
            var index = ex.StackTrace.LastIndexOf(lineSearch);
            if (index != -1)
            {
                var lineNumberText = ex.StackTrace.Substring(index + lineSearch.Length);
                if (int.TryParse(lineNumberText, out lineNumber))
                {
                }
            }
            return MethodeName + ", line: " + lineNumber + Environment.NewLine + "Error Message: " + errMessage;
        }

        public string GetCellErrorMessage(string SheetName, int row, int cell)
        {
            return string.Format("sheet {0} baris {1} kolom {2} salah", SheetName, row + 1, GetColumnName(cell));
        }
        public string GetStringValue(ISheet sheet, int rowNum, int cellNum, out string oMessage)
        {
            string err = GetCellErrorMessage(sheet.SheetName, rowNum, cellNum);
            try
            {
                oMessage = string.Empty;
                IRow row = sheet.GetRow(rowNum);
                if (row == null)
                {
                    oMessage = err + ": baris kosong";
                    return string.Empty;
                }
                ICell cell = row.GetCell(cellNum);
                if (cell == null)
                {
                    return string.Empty;
                }
                return cell.ToString();
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage(ServiceName + "GetStringValue", ex);
                return null;
            }
        }
        public double? GetNumberValue(XSSFWorkbook xssfwb, ISheet sheet, int rowNum, int cellNum, out string oMessage)
        {
            string err = GetCellErrorMessage(sheet.SheetName, rowNum, cellNum);
            try
            {
                oMessage = string.Empty;

                IRow row = sheet.GetRow(rowNum);
                if (row == null)
                {
                    oMessage = err + ": baris kosong";
                    return null;
                }
                ICell cell = row.GetCell(cellNum);
                if (cell == null)
                {
                    return 0;
                }
                XSSFFormulaEvaluator formula = new XSSFFormulaEvaluator(xssfwb);
                if (cell.CellType == CellType.Formula)
                    formula.Evaluate(cell);
                return double.Parse(cell.NumericCellValue.ToString());
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage(ServiceName + "GetNumberValue", ex);
                return null;
            }
        }
        public string GetDateValue(XSSFWorkbook xssfwb, ISheet sheet, int rowNum, int cellNum, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;

                IRow row = sheet.GetRow(rowNum);
                if (row == null)
                {
                    oMessage = GetCellErrorMessage(sheet.SheetName, rowNum, cellNum);
                    return null;
                }
                ICell cell = row.GetCell(cellNum);
                if (cell == null)
                {
                    oMessage = GetCellErrorMessage(sheet.SheetName, rowNum, cellNum);
                    return null;
                }
                if (cell.CellType == CellType.String) return cell.ToString();

                if (DateUtil.IsCellDateFormatted(cell))
                {
                    DateTime date = cell.DateCellValue;
                    return date.ToString("dd-MM-yyyy");
                }
                else
                {
                    return cell.ToString();
                }
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage(ServiceName + "GetDateValue", ex);
                return null;
            }
        }
        public int? GetIntValue(XSSFWorkbook xssfwb, ISheet sheet, int rowNum, int cellNum, out string oMessage)
        {
            string err = GetCellErrorMessage(sheet.SheetName, rowNum, cellNum);
            try
            {
                oMessage = string.Empty;

                IRow row = sheet.GetRow(rowNum);
                if (row == null)
                {
                    oMessage = err + ": baris kosong";
                    return null;
                }
                ICell cell = row.GetCell(cellNum);
                if (cell == null)
                {
                    return 0;
                }
                XSSFFormulaEvaluator formula = new XSSFFormulaEvaluator(xssfwb);
                if (cell.CellType == CellType.Formula)
                    formula.Evaluate(cell);
                return int.Parse(cell.NumericCellValue.ToString());
            }
            catch (Exception ex)
            {
                oMessage = GetErrorMessage(ServiceName + "GetIntValue", ex);
                return null;
            }
        }

        public IFont SetFontStyle(XSSFWorkbook xssfwb)
        {
            XSSFFont font = (XSSFFont)xssfwb.CreateFont();
            font.FontHeightInPoints = 12;
            font.Boldweight = (short)FontBoldWeight.Bold;
            font.IsBold = true;

            IFont ifont = xssfwb.CreateFont();
            ifont.FontHeightInPoints = 12;
            ifont.IsItalic = false;
            return ifont;

        }
        
        public XSSFCellStyle SetTextStyle(
            XSSFWorkbook xssfwb,
            bool left,
            bool bottom,
            bool rigth,
            bool top
            )
        {
            XSSFCellStyle TextStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
            TextStyle.DataFormat = (short)CellType.String;
            TextStyle.WrapText = true;
            TextStyle.SetFont(SetFontStyle(xssfwb));

            TextStyle.BorderLeft = left ? BorderStyle.Thin : BorderStyle.None;
            TextStyle.BorderBottom = bottom ? BorderStyle.Thin : BorderStyle.None;
            TextStyle.BorderRight = rigth ? BorderStyle.Thin : BorderStyle.None;
            TextStyle.BorderTop = top ? BorderStyle.Thin : BorderStyle.None;
            return TextStyle;
        }


        public XSSFCellStyle SetRpStyle(
            XSSFWorkbook xssfwb,
            bool left,
            bool bottom,
            bool rigth,
            bool top)
        {
            XSSFCellStyle RpStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
            RpStyle.DataFormat = xssfwb.CreateDataFormat().GetFormat("#,##0.00");
            RpStyle.Alignment = HorizontalAlignment.Right;
            RpStyle.SetFont(SetFontStyle(xssfwb));

            RpStyle.BorderLeft = left ? BorderStyle.Thin : BorderStyle.None;
            RpStyle.BorderBottom = bottom ? BorderStyle.Thin : BorderStyle.None;
            RpStyle.BorderRight = rigth ? BorderStyle.Thin : BorderStyle.None;
            RpStyle.BorderTop = top ? BorderStyle.Thin : BorderStyle.None;

            return RpStyle;
        }
        public XSSFCellStyle SetNumberStyleWithDecimal(
            XSSFWorkbook xssfwb,
            int numDec,
            bool left,
            bool bottom,
            bool rigth,
            bool top)
        {
            XSSFCellStyle NumberStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
            string format = "#,##0";
            for (int i = 0; i < numDec; i++)
            {
                if (i == 0)
                    format += ".";
                format += "0";
            }
            NumberStyle.DataFormat = xssfwb.CreateDataFormat().GetFormat(format);
            NumberStyle.Alignment = HorizontalAlignment.Right;
            NumberStyle.SetFont(SetFontStyle(xssfwb));

            NumberStyle.BorderLeft = left ? BorderStyle.Thin : BorderStyle.None;
            NumberStyle.BorderBottom = bottom ? BorderStyle.Thin : BorderStyle.None;
            NumberStyle.BorderRight = rigth ? BorderStyle.Thin : BorderStyle.None;
            NumberStyle.BorderTop = top ? BorderStyle.Thin : BorderStyle.None;

            return NumberStyle;
        }
        public XSSFCellStyle SetNumberStyle(
            XSSFWorkbook xssfwb,
            bool left,
            bool bottom,
            bool rigth,
            bool top)
        {
            XSSFCellStyle NumberStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();

            NumberStyle.DataFormat = xssfwb.CreateDataFormat().GetFormat("#,##0");
            NumberStyle.Alignment = HorizontalAlignment.Right;
            NumberStyle.SetFont(SetFontStyle(xssfwb));

            NumberStyle.BorderLeft = left ? BorderStyle.Thin : BorderStyle.None;
            NumberStyle.BorderBottom = bottom ? BorderStyle.Thin : BorderStyle.None;
            NumberStyle.BorderRight = rigth ? BorderStyle.Thin : BorderStyle.None;
            NumberStyle.BorderTop = top ? BorderStyle.Thin : BorderStyle.None;

            return NumberStyle;
        }
        public XSSFCellStyle SetDoubleStyle(
            XSSFWorkbook xssfwb,
            bool left,
            bool bottom,
            bool rigth,
            bool top)
        {
            XSSFCellStyle RpStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
            RpStyle.DataFormat = xssfwb.CreateDataFormat().GetFormat("#,##0.00000000000000");
            RpStyle.Alignment = HorizontalAlignment.Right;
            RpStyle.SetFont(SetFontStyle(xssfwb));

            RpStyle.BorderLeft = left ? BorderStyle.Thin : BorderStyle.None;
            RpStyle.BorderBottom = bottom ? BorderStyle.Thin : BorderStyle.None;
            RpStyle.BorderRight = rigth ? BorderStyle.Thin : BorderStyle.None;
            RpStyle.BorderTop = top ? BorderStyle.Thin : BorderStyle.None;

            return RpStyle;
        }
    }
}
