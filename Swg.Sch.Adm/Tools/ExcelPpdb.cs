﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Adm.Tools
{
    public class ExcelPpdb
    {
        ExcelTool excelTool = new ExcelTool();
        string ServiceName = "ExcelPpdb";

        public ExcelPpdb()
        {
        }
        public string GenerateFileLaporanPPdbSiswa(int IdUser, List<SheetPpdbDaftarSiswaModel> Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                string templateSheet = "LapPpdbSiswa";
                oMessage = excelTool.InitExcel(IdUser, "Laporan_ppdb", templateSheet, out XSSFWorkbook xssfwb, out ISheet sheetx, out string OutFileName);
                XSSFCellStyle numberStyle = excelTool.SetNumberStye(xssfwb);
                XSSFCellStyle textStyle = excelTool.SetTextStye(xssfwb);
                XSSFCellStyle textStyleWithoutBorder = excelTool.SetTextStyleWithoutBorder(xssfwb);
                XSSFCellStyle rpStyle = excelTool.SetRpStye(xssfwb);
                XSSFCellStyle numberStyle3Dec = excelTool.SetNumberStyeWithDecimal(xssfwb, 3);


                string namaSheet = string.Format("detil");
                sheetx.CopySheet(namaSheet);
                ISheet sheet = xssfwb.GetSheet(namaSheet);
                sheet.GetRow(0).GetCell(0).SetCellValue(Data[0].Judul);
                sheet.GetRow(2).GetCell(0).SetCellValue(Data[0].DownloadOleh);

                int _row = 5;
                int no = 0;
                foreach (var item in Data)
                {
                    IRow row = sheet.CreateRow(_row);
                    for (int _cell = 0; _cell <= 155; _cell++)
                    {
                        row.CreateCell(_cell);
                        switch (_cell)
                        {
                            case 0:
                            case 22:
                            case 23:
                            case 24:
                            case 25:
                            case 26:
                            case 27:
                            case 29:
                                row.GetCell(_cell).CellStyle = numberStyle;
                                break;
                            case 30:
                                row.GetCell(_cell).CellStyle = numberStyle3Dec;
                                break;
                            case 134:
                            case 135:
                            case 136:
                            case 137:
                            case 138:
                            case 139:
                                row.GetCell(_cell).CellStyle = rpStyle;
                                break;
                            default:
                                row.GetCell(_cell).CellStyle = textStyle;
                                break;
                        }
                    }
                    int _cellNo = 0;
                    row.GetCell(_cellNo).SetCellValue(++no);
                    row.GetCell(++_cellNo).SetCellValue(item.IdPpdbDaftar);
                    row.GetCell(++_cellNo).SetCellValue(item.JenisKategoriPendaftaran);
                    row.GetCell(++_cellNo).SetCellValue(item.JalurPendaftaran);
                    row.GetCell(++_cellNo).SetCellValue(item.JenisPendaftaran);
                    row.GetCell(++_cellNo).SetCellValue(item.Kelas);
                    row.GetCell(++_cellNo).SetCellValue(item.Nik);
                    row.GetCell(++_cellNo).SetCellValue(item.Nisn);
                    row.GetCell(++_cellNo).SetCellValue(item.Nama);
                    row.GetCell(++_cellNo).SetCellValue(item.NamaPanggilan);
                    row.GetCell(++_cellNo).SetCellValue(item.TempatLahir);
                    row.GetCell(++_cellNo).SetCellValue(item.TanggalLahir);
                    row.GetCell(++_cellNo).SetCellValue(item.IdAgama);
                    row.GetCell(++_cellNo).SetCellValue(item.IdKewarganegaraan);
                    row.GetCell(++_cellNo).SetCellValue(item.KdJenisKelamin);
                    row.GetCell(++_cellNo).SetCellValue(item.KdGolonganDarah);
                    row.GetCell(++_cellNo).SetCellValue(item.AlamatTinggal);
                    row.GetCell(++_cellNo).SetCellValue(item.IdTinggal);
                    row.GetCell(++_cellNo).SetCellValue(item.Email);
                    row.GetCell(++_cellNo).SetCellValue(item.NoHandphone);
                    row.GetCell(++_cellNo).SetCellValue(item.NoDarurat);
                    row.GetCell(++_cellNo).SetCellValue(item.Bahasa);
                    row.GetCell(++_cellNo).SetCellValue(item.TinggiBadan);
                    row.GetCell(++_cellNo).SetCellValue(item.BeratBadan);
                    row.GetCell(++_cellNo).SetCellValue(item.AnakKe);
                    row.GetCell(++_cellNo).SetCellValue(item.JumlahSodaraKandung);
                    row.GetCell(++_cellNo).SetCellValue(item.JumlahSodaraTiri);
                    row.GetCell(++_cellNo).SetCellValue(item.JumlahSodaraAngkat);
                    row.GetCell(++_cellNo).SetCellValue(item.Transportasi);
                    row.GetCell(++_cellNo).SetCellValue(item.JarakKeSekolah);
                    row.GetCell(++_cellNo).SetCellValue(item.WaktuTempuh);
                    row.GetCell(++_cellNo).SetCellValue(item.PenyakitDiderita);
                    row.GetCell(++_cellNo).SetCellValue(item.KelainanJasmani);
                    row.GetCell(++_cellNo).SetCellValue(item.SekolahAsal);
                    row.GetCell(++_cellNo).SetCellValue(item.AlamatSekolahAsal);
                    row.GetCell(++_cellNo).SetCellValue(item.NspnSekolahAsal);
                    row.GetCell(++_cellNo).SetCellValue(item.UkuranSeragam);

                    row.GetCell(++_cellNo).SetCellValue(item.IbuNama);
                    row.GetCell(++_cellNo).SetCellValue(item.IbuAlamat);
                    row.GetCell(++_cellNo).SetCellValue(item.IbuNik);
                    row.GetCell(++_cellNo).SetCellValue(item.IbuJenisPekerjaan);
                    row.GetCell(++_cellNo).SetCellValue(item.IbuNamaInstansi);
                    row.GetCell(++_cellNo).SetCellValue(item.IbuJabatan);
                    row.GetCell(++_cellNo).SetCellValue(item.IbuEmail);
                    row.GetCell(++_cellNo).SetCellValue(item.IbuNoHandphone);
                    row.GetCell(++_cellNo).SetCellValue(item.IbuNoTelpRumah);
                    row.GetCell(++_cellNo).SetCellValue(item.IbuAgama);
                    row.GetCell(++_cellNo).SetCellValue(item.IbuKewarganegaraan);
                    row.GetCell(++_cellNo).SetCellValue(item.IbuJenjangPendidikan);
                    row.GetCell(++_cellNo).SetCellValue(item.IbuIdStatusPenikahan);
                    row.GetCell(++_cellNo).SetCellValue(item.IbuTempatLahir);
                    row.GetCell(++_cellNo).SetCellValue(item.IbuTanggalLahir);
                    row.GetCell(++_cellNo).SetCellValue(item.IbuIdStatusHidup);
                    row.GetCell(++_cellNo).SetCellValue(item.IbuTanggalMeninggal);
                    row.GetCell(++_cellNo).SetCellValue(item.AyahNama);
                    row.GetCell(++_cellNo).SetCellValue(item.AyahAlamat);
                    row.GetCell(++_cellNo).SetCellValue(item.AyahNik);
                    row.GetCell(++_cellNo).SetCellValue(item.AyahJenisPekerjaan);
                    row.GetCell(++_cellNo).SetCellValue(item.AyahNamaInstansi);
                    row.GetCell(++_cellNo).SetCellValue(item.AyahJabatan);
                    row.GetCell(++_cellNo).SetCellValue(item.AyahEmail);
                    row.GetCell(++_cellNo).SetCellValue(item.AyahNoHandphone);
                    row.GetCell(++_cellNo).SetCellValue(item.AyahNoTelpRumah);
                    row.GetCell(++_cellNo).SetCellValue(item.AyahAgama);
                    row.GetCell(++_cellNo).SetCellValue(item.AyahKewarganegaraan);
                    row.GetCell(++_cellNo).SetCellValue(item.AyahJenjangPendidikan);
                    row.GetCell(++_cellNo).SetCellValue(item.AyahIdStatusPenikahan);
                    row.GetCell(++_cellNo).SetCellValue(item.AyahTempatLahir);
                    row.GetCell(++_cellNo).SetCellValue(item.AyahTanggalLahir);
                    row.GetCell(++_cellNo).SetCellValue(item.AyahIdStatusHidup);
                    row.GetCell(++_cellNo).SetCellValue(item.AyahTanggalMeninggal);
                    row.GetCell(++_cellNo).SetCellValue(item.WaliNama);
                    row.GetCell(++_cellNo).SetCellValue(item.WaliAlamat);
                    row.GetCell(++_cellNo).SetCellValue(item.WaliNik);
                    row.GetCell(++_cellNo).SetCellValue(item.WaliJenisPekerjaan);
                    row.GetCell(++_cellNo).SetCellValue(item.WaliNamaInstansi);
                    row.GetCell(++_cellNo).SetCellValue(item.WaliJabatan);
                    row.GetCell(++_cellNo).SetCellValue(item.WaliEmail);
                    row.GetCell(++_cellNo).SetCellValue(item.WaliNoHandphone);
                    row.GetCell(++_cellNo).SetCellValue(item.WaliNoTelpRumah);
                    row.GetCell(++_cellNo).SetCellValue(item.WaliAgama);
                    row.GetCell(++_cellNo).SetCellValue(item.WaliKewarganegaraan);
                    row.GetCell(++_cellNo).SetCellValue(item.WaliJenjangPendidikan);
                    row.GetCell(++_cellNo).SetCellValue(item.WaliIdStatusPenikahan);
                    row.GetCell(++_cellNo).SetCellValue(item.WaliTempatLahir);
                    row.GetCell(++_cellNo).SetCellValue(item.WaliTanggalLahir);
                    row.GetCell(++_cellNo).SetCellValue(item.WaliIdStatusHidup);
                    row.GetCell(++_cellNo).SetCellValue(item.WaliTanggalMeninggal);
                    row.GetCell(++_cellNo).SetCellValue(item.RaporMatematika);
                    row.GetCell(++_cellNo).SetCellValue(item.RaporIndo);
                    row.GetCell(++_cellNo).SetCellValue(item.RaporIpa);
                    row.GetCell(++_cellNo).SetCellValue(item.RaporEng);
                    row.GetCell(++_cellNo).SetCellValue(item.RaporIps);
                    row.GetCell(++_cellNo).SetCellValue(item.JenisPrestasi);
                    row.GetCell(++_cellNo).SetCellValue(item.JenisTingkatPrestasi);
                    row.GetCell(++_cellNo).SetCellValue(item.Tahun);
                    row.GetCell(++_cellNo).SetCellValue(item.NamaPenyelenggara);
                    row.GetCell(++_cellNo).SetCellValue(item.Keterangan);
                    int x = 0;
                    foreach (var jawaban in item.Jawabans)
                    {
                        ++x;
                        row.GetCell(++_cellNo).SetCellValue(jawaban);
                    }
                    if (x < 37)
                        for (int x1 = x; x1 < 24; x1++)
                        {
                            ++_cellNo;
                        }
                    //x = 0;
                    //foreach (var komitmen in item.Komitmens)
                    //{
                    //    ++x;
                    //    row.GetCell(++_cellNo).SetCellValue(komitmen);
                    //}
                    //if (x < 24)
                    //    for (int x1 = x; x1 < 24; x1++)
                    //    {
                    //        ++_cellNo;
                    //    }
                    row.GetCell(++_cellNo).SetCellValue(item.UangPangkal);
                    row.GetCell(++_cellNo).SetCellValue(item.Boks);
                    row.GetCell(++_cellNo).SetCellValue(item.Spp);
                    row.GetCell(++_cellNo).SetCellValue(item.Infaq);
                    row.GetCell(++_cellNo).SetCellValue(item.Seragam);
                    row.GetCell(++_cellNo).SetCellValue(item.Wakaf);
                    row.GetCell(++_cellNo).SetCellValue(item.IsCicil);
                    row.GetCell(++_cellNo).SetCellValue(item.LastStatus);
                    row.GetCell(++_cellNo).SetCellValue(item.LastStatusProses);
                    row.GetCell(++_cellNo).SetCellValue(item.LastCatatan);
                    row.GetCell(++_cellNo).SetCellValue(item.TanggalDaftar);
                    row.GetCell(++_cellNo).SetCellValue(item.TanggalBayarDaftar);
                    row.GetCell(++_cellNo).SetCellValue(item.TanggalInputFormulir);
                    row.GetCell(++_cellNo).SetCellValue(item.TanggalWawancara);
                    row.GetCell(++_cellNo).SetCellValue(item.TanggalTestObservasi);
                    row.GetCell(++_cellNo).SetCellValue(item.TanggalInputObservasi);
                    row.GetCell(++_cellNo).SetCellValue(item.TanggalVerifikasiObservasi);
                    row.GetCell(++_cellNo).SetCellValue(item.TanggalBayarPendidikan);
                    row.GetCell(++_cellNo).SetCellValue(item.IsAnakPegawai);
                    row.GetCell(++_cellNo).SetCellValue(item.NisSiswa);
                    row.GetCell(++_cellNo).SetCellValue(item.NisSodara);
                    _row++;
                }
                xssfwb.RemoveAt(0);
                FileStream file = new FileStream(OutFileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                return OutFileName;
            }
            catch (Exception ex)
            {
                oMessage = excelTool.GetErrorMessage(ServiceName + "GenerateFileLaporanPPdbSiswa", ex);
                return string.Empty;
            }
        }
        public string GenerateTemplateInputObservasi(int IdUser, List<PpdbSiswaModel> Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                oMessage = excelTool.InitExcel(IdUser, "input_observasi", out XSSFWorkbook xssfwb, out ISheet sheet, out string OutFileName);

                XSSFCellStyle TextStyle = excelTool.SetTextStye(xssfwb);
                XSSFCellStyle RpStyle = excelTool.SetRpStye(xssfwb);
                XSSFCellStyle NumberStyle = excelTool.SetNumberStye(xssfwb);


                int _row = 4;
                int _cellNo = 0;
                foreach (var item in Data.OrderBy(x => x.IdPpdbDaftar))
                {
                    IRow row = sheet.CreateRow(_row);
                    for (int _cell = 0; _cell <= 26; _cell++)
                    {
                        row.CreateCell(_cell);
                        row.GetCell(_cell).CellStyle = TextStyle;
                    }
                    _cellNo = 0;
                    row.GetCell(_cellNo).SetCellValue(item.IdPpdbDaftar);
                    row.GetCell(++_cellNo).SetCellValue(item.Nama);
                    row.GetCell(++_cellNo).SetCellValue(item.Unit);
                    row.GetCell(++_cellNo).SetCellValue(item.Kelas + " " + item.Peminatan);
                    row.GetCell(++_cellNo).SetCellValue(item.JenisKategoriPendaftaran);
                    row.GetCell(++_cellNo).SetCellValue(item.JenisPendaftaran);
                    row.GetCell(++_cellNo).SetCellValue(item.JalurPendaftaran);
                    row.GetCell(++_cellNo).SetCellValue(item.NamaPegawai);
                    row.GetCell(++_cellNo).SetCellValue(item.NamaSodara);
                    row.GetCell(++_cellNo).SetCellValue(item.Status);
                    row.GetCell(++_cellNo).SetCellValue(item.StatusProses);
                    row.GetCell(++_cellNo).SetCellValue(item.CatatanProses);
                    ++_cellNo;
                    ++_cellNo;
                    //biaya
                    //1. UANG PANGKAL;RpUp
                    //2. BOKS;RpBoks
                    //3. SPP;RpSpp
                    //4. INFAQ;RpInfaq
                    //5. SERAGAM;RpSeragam
                    //6. WAKAF;RpWakaf
                    for (int i = 1; i < 7; i++)
                    {
                        var tagPPdb = item.TagPpdb.Where(x => x.IdJenisBiayaPpdb == i).FirstOrDefault();
                        if (tagPPdb != null)
                        {
                            row.GetCell(++_cellNo).SetCellValue(tagPPdb.Rp);
                        }
                        else
                        {
                            ++_cellNo;
                        }
                    }
                    row.GetCell(++_cellNo).SetCellValue(item.RpBiayaTotal);

                    if (item.TagCicil.Count() != 0)
                    {
                        foreach (var tag in item.TagCicil.OrderBy(x => x.CilKe))
                        {
                            row.GetCell(++_cellNo).SetCellValue(tag.RpCilKe);
                            row.GetCell(++_cellNo).SetCellValue(tag.TanggalCilKe);
                        }
                    }
                    ++_cellNo;
                    _row++;
                }

                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);
                FileStream file = new FileStream(OutFileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                return OutFileName;
            }
            catch (Exception ex)
            {
                oMessage = excelTool.GetErrorMessage("GenerateTemplateInputObservasi", ex);
                return string.Empty;
            }
        }

        public List<PpdbInputObservasiSheetModel> ReadFilePpdbInputObservasi(IFormFile FileUpload, out string oMessage)
        {
            oMessage = string.Empty;
            List<PpdbInputObservasiSheetModel> ret = new List<PpdbInputObservasiSheetModel>();
            try
            {
                XSSFWorkbook xssfwb = new XSSFWorkbook();
                Stream fileStream = FileUpload.OpenReadStream();
                xssfwb = new XSSFWorkbook(fileStream);
                ISheet sheet = xssfwb.GetSheetAt(0);
                for (int row = 4; row <= sheet.LastRowNum; row++)
                {
                    PpdbInputObservasiSheetModel m = new PpdbInputObservasiSheetModel
                    {
                        Biayas = new List<PpdbBiayaSiswaDetilAddModel>(),
                        Cicils = new List<PpdbBiayaSiswaCicilAddModel>(),
                    };
                    int cellNo = 0;
                    var intValue = excelTool.GetIntValue((XSSFWorkbook)sheet.Workbook, sheet, row, cellNo, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.IdPpdbDaftar = (int)intValue;

                    for (int i = 1; i < 12; i++)
                    {
                        ++cellNo;
                    }

                    var strValue = excelTool.GetStringValue(sheet, row, ++cellNo, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.StatusHasilObservasi = strValue;
                    switch (m.StatusHasilObservasi)
                    {
                        case "LULUS":
                        case "TIDAK LULUS":
                            break;
                        default:
                            oMessage = "Status harus LULUS atau TIDAK LULUS";
                            return null;
                    }
                    strValue = excelTool.GetStringValue(sheet, row, ++cellNo, out oMessage);
                    if (!string.IsNullOrEmpty(oMessage)) return null;
                    m.Catatan = strValue;
                    //biaya
                    //1. UANG PANGKAL;RpUp
                    //2. BOKS;RpBoks
                    //3. SPP;RpSpp
                    //4. INFAQ;RpInfaq
                    //5. SERAGAM;RpSeragam
                    //6. WAKAF;RpWakaf
                    double? doubleValue;
                    for (int i = 1; i < 7; i++)
                    {
                        doubleValue = excelTool.GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m.Biayas.Add(new PpdbBiayaSiswaDetilAddModel
                        {
                            IdJenisBiayaPpdb = i,
                            Rp = (double)doubleValue,
                            Catatan = string.Format("Upload Observasi dari file {0}", FileUpload.FileName)
                        });
                    }
                    ++cellNo; //Total tidak perlu
                    for (int i = 1; i < 4; i++)
                    {
                        PpdbBiayaSiswaCicilAddModel m2 = new PpdbBiayaSiswaCicilAddModel();
                        doubleValue = excelTool.GetNumberValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                        if (!string.IsNullOrEmpty(oMessage)) return null;
                        m2.CilKe = i;
                        m2.RpCilKe = (double)doubleValue;
                        if (m2.RpCilKe > 0)
                        {
                            string dateValue = excelTool.GetDateValue((XSSFWorkbook)sheet.Workbook, sheet, row, ++cellNo, out oMessage);
                            if (!string.IsNullOrEmpty(oMessage)) return null;
                            m2.TanggalCilKe = dateValue;
                        }
                        else
                        {
                            ++cellNo;
                            m2.CilKe = 0;
                        }
                        if (m2.CilKe > 0)
                            m.Cicils.Add(m2);
                    }
                    ret.Add(m);
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = excelTool.GetErrorMessage(ServiceName + "ReadPpdbInputObservasi", ex);
                return null;
            }
        }
    }
}
