﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using Swg.Sch.Shared.ViewModels;
using System.IO;
using Swg.Sch.Shared.Constants;
using Microsoft.AspNetCore.Http;
using System.Linq;
using Dapper;
using Swg.Services;
using Swg.Entities.Ref;
using Swg.Entities.Kbm;
using Swg.Entities.His;

namespace Swg.Sch.Adm.Tools
{
    public class PdfTool
    {
        public PdfTool()
        {
        }
        public string DownloadKartuPeserta(int IdUser, string PathBerkas, List<int> Data)
        {
            using (ZipFile zip = new ZipFile())
            {
                foreach (var item in Data)
                {
                    string _fileName = Path.Combine(PathBerkas, item + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.KartuPeserta] + ".pdf");
                    if (File.Exists(_fileName))
                        zip.AddFile(_fileName, "KartuPeserta");
                }
                string fileDownload = Path.Combine(PathBerkas, IdUser.ToString() + "KartuPeserta.zip");
                if (File.Exists(fileDownload))
                    File.Delete(fileDownload);

                var stream = new FileStream(fileDownload, FileMode.Create);
                zip.Save(stream);
                stream.Close();
                return fileDownload;
            }
        }

        public string DownloadDokumenJapres(int IdUser, string PathBerkas, List<int> Data)
        {
            using (ZipFile zip = new ZipFile())
            {
                foreach (var item in Data)
                {
                    string _name = item + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.BuktiPrestasi];
                    string _fileName = Path.Combine(PathBerkas, _name + ".pdf");
                    if (!System.IO.File.Exists(_fileName))
                    {
                        _fileName = Path.Combine(PathBerkas, _name + ".pdf");
                        if (!System.IO.File.Exists(_fileName))
                        {
                            _fileName = Path.Combine(PathBerkas, _name + ".jpg");
                            if (!System.IO.File.Exists(_fileName))
                            {
                                _fileName = Path.Combine(PathBerkas, _name + ".jpeg");
                            }
                        }
                    }
                    //string _fileName = Path.Combine(PathBerkas, item.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.BuktiPrestasi] + ".pdf");
                    //if (string.IsNullOrEmpty(_fileName))
                    //{
                    //    _fileName = Path.Combine(PathBerkas, item.IdPpdbDaftar + NamaFileUploadConstant.DictNamaFileUpload[NamaFileUploadConstant.BuktiPrestasi] + ".jpg");
                    //}

                    if (File.Exists(_fileName))
                        zip.AddFile(_fileName, "DokumenJapres");
                }
                string fileDownload = Path.Combine(PathBerkas, IdUser.ToString() + "DokumenJapres.zip");
                if (File.Exists(fileDownload))
                    File.Delete(fileDownload);

                var stream = new FileStream(fileDownload, FileMode.Create);
                zip.Save(stream);
                stream.Close();
                return fileDownload;
            }
        }
    }
}
