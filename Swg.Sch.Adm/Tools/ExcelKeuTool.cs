﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Http;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Adm.Tools
{
    public class ExcelKeuTool
    {
        RefExcelTool refExcel = new RefExcelTool();
        private readonly string ServiceName = "Swg.Sch.Adm.Tools.ExcelKeuTool.";

        public ExcelKeuTool()
        {
        }
        public string GetTemplatePengajuan(RkasReffDownloadModel ReffData, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                string _sourceFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/TemplateRkas.xlsx");
                string _fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/test.xlsx");
                Console.WriteLine(_fileName);
                if (System.IO.File.Exists(_fileName))
                    System.IO.File.Delete(_fileName);

                XSSFWorkbook xssfwb = new XSSFWorkbook();

                FileStream filex = new FileStream(_sourceFile, FileMode.Open, FileAccess.Read);
                xssfwb = new XSSFWorkbook(filex);

                ISheet sheet = xssfwb.GetSheet("Reff");
                int _row = ReffData.KodeCoas.Count;
                if (_row < ReffData.JenisSatuans.Count)
                    _row = ReffData.JenisSatuans.Count;
                if (_row < ReffData.JenisVolumes.Count)
                    _row = ReffData.JenisVolumes.Count;
                for (int i = 0; i < _row+1; i++)
                {
                    IRow row = sheet.CreateRow(i + 1);
                }
                for (int i = 0; i < ReffData.KodeCoas.Count; i++)
                {
                    IRow row = sheet.GetRow(i + 1);
                    row.CreateCell(0);
                    row.GetCell(0).SetCellValue(ReffData.KodeCoas[i]);
                }

                for (int i = 0; i < ReffData.JenisSatuans.Count; i++)
                {
                    IRow row = sheet.GetRow(i + 1);
                    row.CreateCell(2);
                    row.GetCell(2).SetCellValue(ReffData.JenisSatuans[i]);
                }

                for (int i = 0; i < ReffData.JenisVolumes.Count; i++)
                {
                    IRow row = sheet.GetRow(i + 1);
                    row.CreateCell(4);
                    row.GetCell(4).SetCellValue(ReffData.JenisVolumes[i]);
                }

                ISheet sheet1 = xssfwb.GetSheet("PENGAJUAN");
                sheet1.GetRow(0).GetCell(0).SetCellValue("PENGAJUAN ANGGARAN KEGIATAN SEKOLAH");
                sheet1.GetRow(2).GetCell(9).SetCellValue(ReffData.KodeFile);
                sheet1.GetRow(3).GetCell(9).SetCellValue(ReffData.NamaUnit);
                sheet1.GetRow(5).GetCell(9).SetCellValue(ReffData.TahunAjaran);
                xssfwb.RemoveAt(xssfwb.GetSheetIndex("RKAS"));
                xssfwb.Write(filex);
                FileStream file = new FileStream(_fileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                filex.Close();
                return _fileName;
            }
            catch (Exception ex)
            {
                oMessage = refExcel.GetErrorMessage(ServiceName + "GetTemplatePengajuan", ex);
                return null;
            }
        }
        private void SetCellRenPen(IRow row, XSSFWorkbook xSSFWorkbook)
        {
            for (int i = 0; i <= 2; i++)
            {
                row.CreateCell(i);
                switch (i)
                {
                    case 0:
                    case 1:
                        row.GetCell(i).CellStyle = refExcel.SetTextStyle(xSSFWorkbook, true, true, true, true);
                        break;
                    case 2:
                        row.GetCell(i).CellStyle = refExcel.SetNumberStyleWithDecimal(xSSFWorkbook, 2, true, true, true, true);
                        break;
                    default:
                        row.GetCell(i).CellStyle = refExcel.SetTextStyle(xSSFWorkbook, true, true, true, true);
                        break;
                }
            }
        }

        public string GenerateFileRenPen(KeuUnitRenPenDownloadModel Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                string _sourceFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/TemplateKeu.xlsx");
                string _fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/test.xlsx");
                Console.WriteLine(_fileName);
                if (System.IO.File.Exists(_fileName))
                    System.IO.File.Delete(_fileName);

                XSSFWorkbook xssfwb = new XSSFWorkbook();

                FileStream filex = new FileStream(_sourceFile, FileMode.Open, FileAccess.Read);
                xssfwb = new XSSFWorkbook(filex);


                ISheet sheet = xssfwb.GetSheet("RPS");
                sheet.GetRow(1).GetCell(0).SetCellValue(Data.TahunAjaran);
                sheet.GetRow(3).GetCell(1).SetCellValue(Data.NamaUnit);
                sheet.GetRow(4).GetCell(1).SetCellValue(Data.Nomor);
                sheet.GetRow(5).GetCell(1).SetCellValue(Data.TotalRp);
                sheet.GetRow(6).GetCell(1).SetCellValue(Data.Status);
                int _row = 8;
                for (int c1 = 0; c1 < Data.Det.Count; c1++)
                {
                    ++_row;
                    IRow row = sheet.CreateRow(_row);
                    this.SetCellRenPen(row, xssfwb);

                    row.GetCell(0).SetCellValue(Data.Det[c1].KodeCoa);
                    row.GetCell(1).SetCellValue(Data.Det[c1].NamaCoa);
                    row.GetCell(2).SetCellValue(Data.Det[c1].Rp);
                    if (Data.Det[c1].Det != null)
                    {
                        for (int c2 = 0; c2 < Data.Det[c1].Det.Count; c2++)
                        {
                            ++_row;
                            row = sheet.CreateRow(_row);
                            this.SetCellRenPen(row, xssfwb);
                            row.GetCell(0).SetCellValue(Data.Det[c1].Det[c2].KodeCoa);
                            row.GetCell(1).SetCellValue(Data.Det[c1].Det[c2].NamaCoa);
                            row.GetCell(2).SetCellValue(Data.Det[c1].Det[c2].Rp);
                            if (Data.Det[c1].Det[c2].Det != null)
                            {
                                for (int c3 = 0; c3 < Data.Det[c1].Det[c2].Det.Count; c3++)
                                {
                                    ++_row;
                                    row = sheet.CreateRow(_row);
                                    this.SetCellRenPen(row, xssfwb);
                                    row.GetCell(0).SetCellValue(Data.Det[c1].Det[c2].Det[c3].KodeCoa);
                                    row.GetCell(1).SetCellValue(Data.Det[c1].Det[c2].Det[c3].NamaCoa);
                                    row.GetCell(2).SetCellValue(Data.Det[c1].Det[c2].Det[c3].Rp);
                                    if (Data.Det[c1].Det[c2].Det[c3].Det != null)
                                    {
                                        for (int c4 = 0; c4 < Data.Det[c1].Det[c2].Det[c3].Det.Count; c4++)
                                        {
                                            ++_row;
                                            row = sheet.CreateRow(_row);
                                            this.SetCellRenPen(row, xssfwb);
                                            row.GetCell(0).SetCellValue(Data.Det[c1].Det[c2].Det[c3].Det[c4].KodeCoa);
                                            row.GetCell(1).SetCellValue(Data.Det[c1].Det[c2].Det[c3].Det[c4].NamaCoa);
                                            row.GetCell(2).SetCellValue(Data.Det[c1].Det[c2].Det[c3].Det[c4].Rp);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                xssfwb.RemoveAt(xssfwb.GetSheetIndex("RKAS"));
                xssfwb.RemoveAt(xssfwb.GetSheetIndex("PENGAJUAN"));
                xssfwb.RemoveAt(xssfwb.GetSheetIndex("PTGJWB"));
                xssfwb.RemoveAt(xssfwb.GetSheetIndex("Reff"));
                xssfwb.Write(filex);
                FileStream file = new FileStream(_fileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                filex.Close();
                return _fileName;
            }
            catch (Exception ex)
            {
                oMessage = refExcel.GetErrorMessage(ServiceName + "GenerateFileRenPen", ex);
                return string.Empty;
            }
        }

        private void SetCellProKeg(IRow row, XSSFWorkbook xSSFWorkbook)
        {
            for (int i = 0; i <= 12; i++)
            {
                row.CreateCell(i);
                switch (i)
                {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        row.GetCell(i).CellStyle = refExcel.SetTextStyle(xSSFWorkbook, true, true, true, true);
                        break;
                    case 8:
                    case 10:
                        row.GetCell(i).CellStyle = refExcel.SetTextStyle(xSSFWorkbook, false, true, true, true);
                        break;
                    case 7:
                    case 9:
                        row.GetCell(i).CellStyle = refExcel.SetNumberStyle(xSSFWorkbook, true, true, false, true);
                        break;
                    case 11:
                    case 12:
                        row.GetCell(i).CellStyle = refExcel.SetNumberStyleWithDecimal(xSSFWorkbook, 2, true, true, true, true);
                        break;
                    default:
                        row.GetCell(i).CellStyle = refExcel.SetTextStyle(xSSFWorkbook, true, true, true, true);
                        break;
                }
            }
        }
        public string GenerateFileProKeg(KeuUnitProKegDownloadModel Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                string _sourceFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/TemplateKeu.xlsx");
                string _fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/test.xlsx");
                Console.WriteLine(_fileName);
                if (System.IO.File.Exists(_fileName))
                    System.IO.File.Delete(_fileName);

                XSSFWorkbook xssfwb = new XSSFWorkbook();

                FileStream filex = new FileStream(_sourceFile, FileMode.Open, FileAccess.Read);
                xssfwb = new XSSFWorkbook(filex);


                ISheet sheet = xssfwb.GetSheet("RKAS");
                sheet.GetRow(1).GetCell(0).SetCellValue(Data.TahunAjaran);
                sheet.GetRow(3).GetCell(2).SetCellValue(Data.NamaUnit);
                sheet.GetRow(4).GetCell(2).SetCellValue(Data.Kecamatan);
                sheet.GetRow(5).GetCell(2).SetCellValue(Data.KabKot);
                sheet.GetRow(6).GetCell(2).SetCellValue(Data.Provinsi);
                sheet.GetRow(9).GetCell(2).SetCellValue(Data.TotalRp);
                sheet.GetRow(10).GetCell(2).SetCellValue(Data.Status);
                int _row = 12;
                for (int c1 = 0; c1 < Data.Det.Count; c1++)
                {
                    ++_row;
                    IRow row = sheet.CreateRow(_row);
                    this.SetCellProKeg(row, xssfwb);

                    row.GetCell(1).SetCellValue(Data.Det[c1].Kode);
                    row.GetCell(2).SetCellValue(Data.Det[c1].Nama);
                    row.GetCell(12).SetCellValue(Data.Det[c1].TotalRp);
                    if (Data.Det[c1].Det != null)
                    {
                        for (int c2 = 0; c2 < Data.Det[c1].Det.Count; c2++)
                        {
                            ++_row;
                            row = sheet.CreateRow(_row);
                            this.SetCellProKeg(row, xssfwb);
                            row.GetCell(1).SetCellValue(Data.Det[c1].Det[c2].Kode);
                            row.GetCell(3).SetCellValue(Data.Det[c1].Det[c2].Nama);
                            row.GetCell(12).SetCellValue(Data.Det[c1].Det[c2].TotalRp);
                            if (Data.Det[c1].Det[c2].Det != null)
                            {
                                for (int c3 = 0; c3 < Data.Det[c1].Det[c2].Det.Count; c3++)
                                {
                                    ++_row;
                                    row = sheet.CreateRow(_row);
                                    this.SetCellProKeg(row, xssfwb);
                                    row.GetCell(1).SetCellValue(Data.Det[c1].Det[c2].Det[c3].Kode);
                                    row.GetCell(4).SetCellValue(Data.Det[c1].Det[c2].Det[c3].Nama);
                                    row.GetCell(12).SetCellValue(Data.Det[c1].Det[c2].Det[c3].TotalRp);
                                    if (Data.Det[c1].Det[c2].Det[c3].Det != null)
                                    {
                                        for (int c4 = 0; c4 < Data.Det[c1].Det[c2].Det[c3].Det.Count; c4++)
                                        {
                                            ++_row;
                                            row = sheet.CreateRow(_row);
                                            this.SetCellProKeg(row, xssfwb);
                                            row.GetCell(1).SetCellValue(Data.Det[c1].Det[c2].Det[c3].Det[c4].Kode);
                                            row.GetCell(5).SetCellValue(Data.Det[c1].Det[c2].Det[c3].Det[c4].Nama);
                                            row.GetCell(7).SetCellValue(Data.Det[c1].Det[c2].Det[c3].Det[c4].JumlahSatuan);
                                            row.GetCell(8).SetCellValue(Data.Det[c1].Det[c2].Det[c3].Det[c4].Satuan);
                                            row.GetCell(9).SetCellValue(Data.Det[c1].Det[c2].Det[c3].Det[c4].JumlahVolume);
                                            row.GetCell(10).SetCellValue(Data.Det[c1].Det[c2].Det[c3].Det[c4].Volume);
                                            row.GetCell(11).SetCellValue(Data.Det[c1].Det[c2].Det[c3].Det[c4].HargaSatuan);
                                            row.GetCell(12).SetCellValue(Data.Det[c1].Det[c2].Det[c3].Det[c4].TotalRp);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                xssfwb.RemoveAt(xssfwb.GetSheetIndex("RPS"));
                xssfwb.RemoveAt(xssfwb.GetSheetIndex("PENGAJUAN"));
                xssfwb.RemoveAt(xssfwb.GetSheetIndex("PTGJWB"));
                xssfwb.RemoveAt(xssfwb.GetSheetIndex("Reff"));
                xssfwb.Write(filex);
                FileStream file = new FileStream(_fileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                filex.Close();
                return _fileName;
            }
            catch (Exception ex)
            {
                oMessage = refExcel.GetErrorMessage(ServiceName + "GenerateFileRenPen", ex);
                return string.Empty;
            }
        }

        private void SetCellPengajuan(IRow row, XSSFWorkbook xSSFWorkbook)
        {
            for (int i = 0; i <= 9; i++)
            {
                row.CreateCell(i);
                switch (i)
                {
                    case 1:
                        row.GetCell(i).CellStyle = refExcel.SetTextStyle(xSSFWorkbook, true, true, false, true);
                        break;
                    case 2:
                        row.GetCell(i).CellStyle = refExcel.SetTextStyle(xSSFWorkbook, false, true, true, true);
                        break;
                    case 0:
                    case 4:
                    case 6:
                    case 9:
                        row.GetCell(i).CellStyle = refExcel.SetTextStyle(xSSFWorkbook, true, true, true, true);
                        break;
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                        row.GetCell(i).CellStyle = refExcel.SetNumberStyleWithDecimal(xSSFWorkbook, 2, true, true, true, true);
                        break;
                    default:
                        row.GetCell(i).CellStyle = refExcel.SetTextStyle(xSSFWorkbook,true,true,true,true);
                        break;
                }
            }
        }
        public string GenerateFilePengajuan(KeuPengajuanDownloadModel Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                string _sourceFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/TemplateKeu.xlsx");
                string _fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/test.xlsx");
                Console.WriteLine(_fileName);
                if (System.IO.File.Exists(_fileName))
                    System.IO.File.Delete(_fileName);

                XSSFWorkbook xssfwb = new XSSFWorkbook();

                FileStream filex = new FileStream(_sourceFile, FileMode.Open, FileAccess.Read);
                xssfwb = new XSSFWorkbook(filex);


                ISheet sheet = xssfwb.GetSheet("PENGAJUAN");
                sheet.GetRow(1).GetCell(0).SetCellValue(Data.TahunAjaran);
                sheet.GetRow(3).GetCell(2).SetCellValue(Data.NamaUnit);
                sheet.GetRow(4).GetCell(2).SetCellValue(Data.Program);
                sheet.GetRow(5).GetCell(2).SetCellValue(Data.Kegiatan);
                sheet.GetRow(6).GetCell(2).SetCellValue(Data.Nomor);
                sheet.GetRow(7).GetCell(2).SetCellValue(Data.Tanggal);
                //sheet.GetRow(8).GetCell(2).SetCellValue(Data.TotalRp);
                sheet.GetRow(9).GetCell(2).SetCellValue(Data.Status);
                int _row = 11;
                for (int c1 = 0; c1 < Data.Det.Count; c1++)
                {
                    ++_row;
                    IRow row = sheet.CreateRow(_row);
                    this.SetCellPengajuan(row, xssfwb);

                    row.GetCell(0).SetCellValue(Data.Det[c1].Kode);
                    row.GetCell(2).SetCellValue(Data.Det[c1].Nama);
                    row.GetCell(3).SetCellValue(Data.Det[c1].JumlahSatuan);
                    row.GetCell(4).SetCellValue(Data.Det[c1].Satuan);
                    row.GetCell(5).SetCellValue(Data.Det[c1].JumlahVolume);
                    row.GetCell(6).SetCellValue(Data.Det[c1].Volume);
                    row.GetCell(7).SetCellValue(Data.Det[c1].HargaSatuan);
                    row.GetCell(8).SetCellValue(Data.Det[c1].TotalRp);
                    row.GetCell(9).SetCellValue(Data.Det[c1].Catatan);
                }
                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);
                xssfwb.RemoveAt(xssfwb.GetSheetIndex("RPS"));
                xssfwb.RemoveAt(xssfwb.GetSheetIndex("RKAS"));
                xssfwb.RemoveAt(xssfwb.GetSheetIndex("PTGJWB"));
                xssfwb.RemoveAt(xssfwb.GetSheetIndex("Reff"));
                xssfwb.Write(filex);
                FileStream file = new FileStream(_fileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                filex.Close();
                return _fileName;
            }
            catch (Exception ex)
            {
                oMessage = refExcel.GetErrorMessage(ServiceName + "GenerateFilePengajuan", ex);
                return string.Empty;
            }
        }
        private void SetCellPtgjwb(IRow row, XSSFWorkbook xSSFWorkbook)
        {
            for (int i = 0; i <= 10; i++)
            {
                row.CreateCell(i);
                switch (i)
                {
                    case 1:
                        row.GetCell(i).CellStyle = refExcel.SetTextStyle(xSSFWorkbook, true, true, false, true);
                        break;
                    case 2:
                        row.GetCell(i).CellStyle = refExcel.SetTextStyle(xSSFWorkbook, false, true, true, true);
                        break;
                    case 0:
                    case 4:
                    case 6:
                    case 9:
                    case 10:
                        row.GetCell(i).CellStyle = refExcel.SetTextStyle(xSSFWorkbook, true, true, true, true);
                        break;
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                        row.GetCell(i).CellStyle = refExcel.SetNumberStyleWithDecimal(xSSFWorkbook, 2, true, true, true, true);
                        break;
                    default:
                        row.GetCell(i).CellStyle = refExcel.SetTextStyle(xSSFWorkbook, true, true, true, true);
                        break;
                }
            }
        }
        public string GenerateFilePtgjwb(KeuPtgjwbDownloadModel Data, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                string _sourceFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/TemplateKeu.xlsx");
                string _fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileTemplates/test.xlsx");
                Console.WriteLine(_fileName);
                if (System.IO.File.Exists(_fileName))
                    System.IO.File.Delete(_fileName);

                XSSFWorkbook xssfwb = new XSSFWorkbook();

                FileStream filex = new FileStream(_sourceFile, FileMode.Open, FileAccess.Read);
                xssfwb = new XSSFWorkbook(filex);


                ISheet sheet = xssfwb.GetSheet("PTGJWB");
                sheet.GetRow(1).GetCell(0).SetCellValue(Data.TahunAjaran);
                sheet.GetRow(3).GetCell(2).SetCellValue(Data.NamaUnit);
                sheet.GetRow(4).GetCell(2).SetCellValue(Data.Program);
                sheet.GetRow(5).GetCell(2).SetCellValue(Data.Kegiatan);
                sheet.GetRow(6).GetCell(2).SetCellValue(Data.Nomor);
                sheet.GetRow(7).GetCell(2).SetCellValue(Data.Tanggal);
                //sheet.GetRow(8).GetCell(2).SetCellValue(Data.TotalRp);
                sheet.GetRow(9).GetCell(2).SetCellValue(Data.Status);
                int _row = 11;
                for (int c1 = 0; c1 < Data.Det.Count; c1++)
                {
                    ++_row;
                    IRow row = sheet.CreateRow(_row);
                    this.SetCellPtgjwb(row, xssfwb);

                    row.GetCell(0).SetCellValue(Data.Det[c1].Kode);
                    row.GetCell(2).SetCellValue(Data.Det[c1].Nama);
                    row.GetCell(3).SetCellValue(Data.Det[c1].JumlahSatuan);
                    row.GetCell(4).SetCellValue(Data.Det[c1].Satuan);
                    row.GetCell(5).SetCellValue(Data.Det[c1].JumlahVolume);
                    row.GetCell(6).SetCellValue(Data.Det[c1].Volume);
                    row.GetCell(7).SetCellValue(Data.Det[c1].HargaSatuan);
                    row.GetCell(8).SetCellValue(Data.Det[c1].TotalRp);
                    row.GetCell(9).SetCellValue(Data.Det[c1].Catatan);
                    row.GetCell(10).SetCellValue(Data.Det[c1].UrlFileBukti);
                }
                XSSFFormulaEvaluator.EvaluateAllFormulaCells(xssfwb);
                xssfwb.RemoveAt(xssfwb.GetSheetIndex("RPS"));
                xssfwb.RemoveAt(xssfwb.GetSheetIndex("RKAS"));
                xssfwb.RemoveAt(xssfwb.GetSheetIndex("PENGAJUAN"));
                xssfwb.RemoveAt(xssfwb.GetSheetIndex("Reff"));
                xssfwb.Write(filex);
                FileStream file = new FileStream(_fileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                filex.Close();
                return _fileName;
            }
            catch (Exception ex)
            {
                oMessage = refExcel.GetErrorMessage(ServiceName + "GenerateFilePtgjwb", ex);
                return string.Empty;
            }
        }

    }

}
