﻿using System;
using System.Collections.Generic;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Adm.Tools
{
    public class ExcelDapodik
    {
        ExcelTool excelTool = new ExcelTool();
        public ExcelDapodik()
        {
        }
        public string GenerateFileDapodikSiswa(int IdUser, SheetSiswaViewProfileModel Data)
        {
            try
            {
                string templateSheet = "DAPODIK";
                excelTool.InitExcel(IdUser, "TemplateDapodik", templateSheet, out XSSFWorkbook xssfwb, out ISheet sheetx, out string OutFileName);
                XSSFCellStyle numberStyle = excelTool.SetNumberStye(xssfwb);
                XSSFCellStyle textStyle = excelTool.SetTextStye(xssfwb);
                XSSFCellStyle textStyleWithoutBorder = excelTool.SetTextStyleWithoutBorder(xssfwb);
                XSSFCellStyle rpStyle = excelTool.SetRpStye(xssfwb);
                XSSFCellStyle numberStyle3Dec = excelTool.SetNumberStyeWithDecimal(xssfwb, 3);


                string namaSheet = string.Format("Sheet1");
                sheetx.CopySheet(namaSheet);
                ISheet sheet = xssfwb.GetSheet(namaSheet);
                sheet.GetRow(0).GetCell(0).SetCellValue(Data.Judul);
                sheet.GetRow(1).GetCell(0).SetCellValue(Data.DownloadOleh);
                int _row = 3;
                int no = 0;
                foreach (var item in Data.ListData)
                {
                    IRow row = sheet.CreateRow(_row);
                    for (int _cell = 0; _cell <= 72; _cell++)
                    {
                        row.CreateCell(_cell);
                        switch (_cell)
                        {
                            case 0:
                            case 30:
                            case 31:
                            case 33:
                            case 34:
                            case 35:
                            case 36:
                            case 38:
                                row.GetCell(_cell).CellStyle = numberStyle;
                                break;
                            case 32:
                            case 39:
                                row.GetCell(_cell).CellStyle = numberStyle3Dec;
                                break;
                            default:
                                row.GetCell(_cell).CellStyle = textStyle;
                                break;
                        }
                    }
                    int _cellNo = 0;
                    row.GetCell(_cellNo).SetCellValue(++no);
                    row.GetCell(++_cellNo).SetCellValue(item.KelasParalel);
                    row.GetCell(++_cellNo).SetCellValue(item.Nis);
                    row.GetCell(++_cellNo).SetCellValue(item.Nisn);
                    row.GetCell(++_cellNo).SetCellValue(item.Nik);
                    row.GetCell(++_cellNo).SetCellValue(item.Nama);
                    row.GetCell(++_cellNo).SetCellValue(item.NamaPanggilan);
                    row.GetCell(++_cellNo).SetCellValue(item.TempatLahir);
                    row.GetCell(++_cellNo).SetCellValue(item.TanggalLahir);
                    row.GetCell(++_cellNo).SetCellValue(item.Agama.Nama);
                    row.GetCell(++_cellNo).SetCellValue(item.JenisKelamin.Nama);
                    row.GetCell(++_cellNo).SetCellValue(item.NoHandphone);
                    row.GetCell(++_cellNo).SetCellValue(item.NoDarurat);
                    row.GetCell(++_cellNo).SetCellValue(item.Email);
                    row.GetCell(++_cellNo).SetCellValue(item.AlamatTinggal);
                    row.GetCell(++_cellNo).SetCellValue(item.AlamatOrtu);
                    row.GetCell(++_cellNo).SetCellValue(item.NikIbu);
                    row.GetCell(++_cellNo).SetCellValue(item.NamaIbu);
                    row.GetCell(++_cellNo).SetCellValue(item.JenisPekerjaanIbu.Nama);
                    row.GetCell(++_cellNo).SetCellValue(item.NamaInstansiIbu);
                    row.GetCell(++_cellNo).SetCellValue(item.NikAyah);
                    row.GetCell(++_cellNo).SetCellValue(item.NamaAyah);
                    row.GetCell(++_cellNo).SetCellValue(item.JenisPekerjaanAyah.Nama);
                    row.GetCell(++_cellNo).SetCellValue(item.NamaInstansiAyah);
                    row.GetCell(++_cellNo).SetCellValue(item.EmailOrtu);
                    row.GetCell(++_cellNo).SetCellValue(item.NoAktaLahir);
                    row.GetCell(++_cellNo).SetCellValue(item.NoKip);
                    row.GetCell(++_cellNo).SetCellValue(item.GolonganDarah.Nama);
                    row.GetCell(++_cellNo).SetCellValue(item.Kewarganegaraan.Nama);
                    row.GetCell(++_cellNo).SetCellValue(item.Bahasa.Nama);
                    row.GetCell(++_cellNo).SetCellValue(item.TinggiBadan);
                    row.GetCell(++_cellNo).SetCellValue(item.BeratBadan);
                    row.GetCell(++_cellNo).SetCellValue(item.LingkarKepala);
                    row.GetCell(++_cellNo).SetCellValue(item.AnakKe);
                    row.GetCell(++_cellNo).SetCellValue(item.JumlahSodaraKandung);
                    row.GetCell(++_cellNo).SetCellValue(item.JumlahSodaraTiri);
                    row.GetCell(++_cellNo).SetCellValue(item.JumlahSodaraAngkat);
                    row.GetCell(++_cellNo).SetCellValue(item.Transportasi.Nama);
                    row.GetCell(++_cellNo).SetCellValue(item.JarakKeSekolah);
                    row.GetCell(++_cellNo).SetCellValue(item.WaktuTempuh);
                    row.GetCell(++_cellNo).SetCellValue(item.PenyakitDiderita);
                    row.GetCell(++_cellNo).SetCellValue(item.KelainanJasmani);
                    row.GetCell(++_cellNo).SetCellValue(item.SekolahAsal);
                    row.GetCell(++_cellNo).SetCellValue(item.AlamatSekolahAsal);
                    row.GetCell(++_cellNo).SetCellValue(item.NspnSekolahAsal);
                    row.GetCell(++_cellNo).SetCellValue(item.PernahTkFormal);
                    row.GetCell(++_cellNo).SetCellValue(item.PernahTlInformal);
                    row.GetCell(++_cellNo).SetCellValue(item.Hobi.Nama);
                    row.GetCell(++_cellNo).SetCellValue(item.Cita2.Nama);
                    row.GetCell(++_cellNo).SetCellValue(item.NoUjian);
                    row.GetCell(++_cellNo).SetCellValue(item.NoIjazah);
                    row.GetCell(++_cellNo).SetCellValue(item.NoShusbn);
                    row.GetCell(++_cellNo).SetCellValue(item.KelasDapodik);
                    row.GetCell(++_cellNo).SetCellValue(item.BerkebutuhanKhusus);
                    row.GetCell(++_cellNo).SetCellValue(item.AlamatKk);
                    row.GetCell(++_cellNo).SetCellValue(item.PunyaWali);
                    row.GetCell(++_cellNo).SetCellValue(item.TanggalLahirAyah);
                    row.GetCell(++_cellNo).SetCellValue(item.BerkebutuhanKhususAyah);
                    row.GetCell(++_cellNo).SetCellValue(item.PendidikanTerakhirAyah);
                    row.GetCell(++_cellNo).SetCellValue(item.PenghasilanAyah);
                    row.GetCell(++_cellNo).SetCellValue(item.NomorHpAyah);
                    row.GetCell(++_cellNo).SetCellValue(item.TanggalLahirIbu);
                    row.GetCell(++_cellNo).SetCellValue(item.BerkebutuhanKhususIbu);
                    row.GetCell(++_cellNo).SetCellValue(item.PendidikanTerakhirIbu);
                    row.GetCell(++_cellNo).SetCellValue(item.PenghasilanIbu);
                    row.GetCell(++_cellNo).SetCellValue(item.NomorHpIbu);
                    row.GetCell(++_cellNo).SetCellValue(item.NikWali);
                    row.GetCell(++_cellNo).SetCellValue(item.NamaWali);
                    row.GetCell(++_cellNo).SetCellValue(item.AlamatWali);
                    row.GetCell(++_cellNo).SetCellValue(item.IdJenisPekerjaanWali);
                    row.GetCell(++_cellNo).SetCellValue(item.NamaInstansiWali);
                    row.GetCell(++_cellNo).SetCellValue(item.EmailWali);
                    row.GetCell(++_cellNo).SetCellValue(item.NoHandphoneWali);
                    _row++;
                }
                xssfwb.RemoveAt(0);
                FileStream file = new FileStream(OutFileName, FileMode.Create);
                xssfwb.Write(file);
                file.Close();
                return OutFileName;

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
