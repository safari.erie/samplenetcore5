using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Swg.Sch.Shared.ViewModels;
using System;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public class AuthorizeAttribute : Attribute, IAuthorizationFilter
{
    public void OnAuthorization(AuthorizationFilterContext context)
    {
        var user = (SiswaModel)context.HttpContext.Items["User"];
        if (user == null)
        {
            context.Result = new JsonResult(new
            {
                IsSuccess = false,
                ReturnMessage = "Unauthorized",
                Data = string.Empty
            })
            { StatusCode = StatusCodes.Status401Unauthorized };

        }
    }
}
