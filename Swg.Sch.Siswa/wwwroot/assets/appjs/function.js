﻿/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 14042020
================================================================================================================= */
var base_url = location.protocol + "//" + document.location.host;
function GlobalAjax(Url) {
  var ResultArray = $.ajax({
    url: Url,
    type: "GET",
    dataType: "json",
    async: false,
  });

  var result = ResultArray.responseJSON; //JSON.stringify
  if (result.IsSuccess == false) {
    iziToast.error({
      title: "Gagal Menampilkan Data",
      message: result.ReturnMessage,
      position: "topRight",
    });
  } else {
    return result.Data;
  }
}
jQuery.fn.serializeObject = function () {
  var arrayData, objectData;
  arrayData = this.serializeArray();
  objectData = {};

  $.each(arrayData, function () {
    var value;

    if (this.value != null) {
      value = this.value;
    } else {
      value = "";
    }

    if (objectData[this.name] != null) {
      if (!objectData[this.name].push) {
        objectData[this.name] = [objectData[this.name]];
      }

      objectData[this.name].push(value);
    } else {
      objectData[this.name] = value;
    }
  });

  return objectData;
};
$("body")
  .on("click", 'input[name="datefilter"]', function () {
    $(this).datepicker();
  })
  .on("focus", 'input[name="datefilter"]', function () {
    $(this).datepicker();
  });

function getDayName(dateStr, locale) {
  var date = new Date(dateStr);
  return date.toLocaleDateString(locale, { weekday: "long" });
}

SetDatepicker();
function SetDatepicker() {
  $('[data-toggle="datepicker"]').datepicker({
    format: "dd-mm-yyyy",
    pick: function (e) {
      var date = e.date.getDate();
      jQuery(this).attr("value", date);
    },
  });

  $('[data-toggle="datepicker-maxtoday"]').datepicker({
    format: "dd-mm-yyyy",
    startDate: "01-01-0001",
    endDate: GetCurrentDate("TanggalOnly"),
  });
  $(".clockpickers").clockpicker({
    donetext: "Done",
  });
}

function Logout() {
  sessionStorage.clear();
  localStorage.clear();
  window.location = base_url + "/Home/Index";
}

function ucwords(str, force) {
  str = force ? str.toLowerCase() : str;
  return str.replace(/(\b)([a-zA-Z])/g, function (firstLetter) {
    return firstLetter.toUpperCase();
  });
}

function GetCurrentDate(Tipe) {
  if (Tipe == "TanggalOnly") {
    var ResponseCurrDate = GlobalAjax(base_url + "/Commons/GetCurrentDate");
    return ResponseCurrDate;
  }
}

function GetCurrentDateLocal(Tipe) {
  var d = new Date($.now());
  if (Tipe == "TanggalOnly") {
    var date_now = d.getDate();
    var month_now = d.getMonth() + 1;
    if (date_now < 10) {
      date_now = "0" + date_now;
    }
    if (month_now < 10) {
      month_now = "0" + month_now;
    }

    return date_now + "-" + month_now + "-" + d.getFullYear();
  }
}

// modifikasi fungsi seperti dual list box
$(".ComboSumber").on("change", function () {
  var ValueSelected = this.value;
  var TextSelected = $("option:selected", this).text();

  var HtmlSelected =
    "<option value='" +
    ValueSelected +
    "' selected>" +
    TextSelected +
    "</option>";
  $(".ComboTerpilih").append(HtmlSelected);
  $("option:selected", this).remove();
});

$(".ComboTerpilih").on("change", function () {
  var ValueSelected = this.value;
  var TextSelected = $("option:selected", this).text();

  var HtmlSelected =
    "<option value='" + ValueSelected + "'>" + TextSelected + "</option>";
  $(".ComboSumber").append(HtmlSelected);
  $("option:selected", this).remove();
});

function PilihSemua() {
  $(".ComboSumber")
    .find("option")
    .each(function (index, item) {
      var ValueSelected = $(item).val();
      var TextSelected = $(item).text();

      var HtmlSelected =
        "<option value='" +
        ValueSelected +
        "' selected>" +
        TextSelected +
        "</option>";
      $(".ComboTerpilih").append(HtmlSelected);

      $(this).remove();
    });
}

function BatalPilihSemua() {
  $(".ComboTerpilih")
    .find("option")
    .each(function (index, item) {
      var ValueSelected = $(item).val();
      var TextSelected = $(item).text();

      var HtmlSelected =
        "<option value='" + ValueSelected + "'>" + TextSelected + "</option>";
      $(".ComboSumber").append(HtmlSelected);

      $(this).remove();
    });
}

function formatRupiahtoAngka(Rupiah) {
  return parseInt(Rupiah.replace(/,.*|[^0-9]/g, ""), 10);
}

function formatRupiah(angka) {
  var TipeData = jQuery.type(angka);
  if (TipeData == "number") {
    var bilangan = angka;
    var reverse = bilangan.toString().split("").reverse().join(""),
      ribuan = reverse.match(/\d{1,3}/g);
    ribuan = ribuan.join(".").split("").reverse().join("");
    return ribuan;
  }
  return "Tidak Ada";
}

function indoDate(Date) {
  var SplitTanggal = Date.split("-");
  var Hari = SplitTanggal[0];
  var Bulan = SplitTanggal[1];
  var Tahun = SplitTanggal[2];

  var ArrayBulan = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember",
  ];
  if (Bulan < 10) {
    Bulan = Bulan.replace("0", "");
  }

  return Hari + " " + ArrayBulan[Bulan - 1] + " " + Tahun;
}

function setFocusOnDivWithId(elementId) {
  const scrollIntoViewOptions = { behavior: "smooth", block: "center" };
  document.getElementById(elementId).scrollIntoView(scrollIntoViewOptions);
}

//add by eri
// add date 20200316
// validate email and number only
function validateEmail(email) {
  var re =
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
function numOnly(event) {
  var key = event.keyCode;
  return (key > 47 && key < 58) || key == 8 || key == 9 || key == 46;
}
function preventNumberInput(e) {
  var keyCode = e.keyCode ? e.keyCode : e.which;
  if ((keyCode > 47 && keyCode < 58) || (keyCode > 95 && keyCode < 107)) {
    e.preventDefault();
  }
}

/// TANGGAL & JAM REALTIME ///
setInterval(function () {
  waktu_realtime();
}, 1);
function waktu_realtime() {
  var tanggal = new Date();
  if (tanggal.getHours() < 10) {
    var jam_saat_ini = "0" + tanggal.getHours();
  } else {
    var jam_saat_ini = tanggal.getHours();
  }
  if (tanggal.getMinutes() < 10) {
    var menit_saat_ini = "0" + tanggal.getMinutes();
  } else {
    var menit_saat_ini = tanggal.getMinutes();
  }
  if (tanggal.getSeconds() < 10) {
    var detik_saat_ini = "0" + tanggal.getSeconds();
  } else {
    var detik_saat_ini = tanggal.getSeconds();
  }
  $("#time_now").html(
    jam_saat_ini + ":" + menit_saat_ini + ":" + detik_saat_ini
  );

  var tanggallengkap = new String();
  var namahari = "Minggu Senin Selasa Rabu Kamis Jumat Sabtu";
  namahari = namahari.split(" ");
  var namabulan =
    "Januari Februari Maret April Mei Juni Juli Agustus September Oktober November Desember";
  namabulan = namabulan.split(" ");
  var tgl = new Date();
  var hari = tgl.getDay();
  var tanggal = tgl.getDate();
  var bulan = tgl.getMonth();
  var tahun = tgl.getFullYear();

  $("#date_now").html(
    namahari[hari] +
      ", " +
      tanggal +
      " " +
      namabulan[bulan] +
      " " +
      tahun +
      " - "
  );
}
/// TANGGAL & JAM REAL TIME ///

function SetFullName(FirstName, MiddleName, LastName) {
  if (MiddleName == null) {
    MiddleName = "";
  }
  if (LastName == null) {
    LastName = "";
  }
  var Url =
    base_url +
    "/Commons/SetFullName?FirstName=" +
    FirstName +
    "&MiddleName=" +
    MiddleName +
    "&LastName=" +
    LastName;
  return GlobalAjax(Url);
}

function DownloadFile(JenisFile, FileName) {
  var url =
    base_url +
    "/Commons/DownloadFile?JenisFile=" +
    JenisFile +
    "&FileName=" +
    FileName;
  window.open(url, "_blank");
}

createYoutubeEmbed = (key) => {
  return "https://www.youtube.com/embed/" + key;
};
getEmbedYt = (text) => {
  if (!text) return text;
  const self = this;

  const linkreg = /(?:)<a([^>]+)>(.+?)<\/a>/g;
  const fullreg =
    /(https?:\/\/)?(www\.)?(youtube\.com\/watch\?v=|youtu\.be\/)([^& \n<]+)(?:[^ \n<]+)?/g;
  const regex =
    /(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/watch\?v=|youtu\.be\/)([^& \n<]+)(?:[^ \n<]+)?/g;

  let resultHtml = text;

  // get all the matches for youtube links using the first regex
  const match = text.match(fullreg);
  if (match && match.length > 0) {
    // get all links and put in placeholders
    const matchlinks = text.match(linkreg);
    if (matchlinks && matchlinks.length > 0) {
      for (var i = 0; i < matchlinks.length; i++) {
        resultHtml = resultHtml.replace(
          matchlinks[i],
          "#placeholder" + i + "#"
        );
      }
    }

    // now go through the matches one by one
    for (var i = 0; i < match.length; i++) {
      // get the key out of the match using the second regex
      let matchParts = match[i].split(regex);
      // replace the full match with the embedded youtube code
      resultHtml = resultHtml.replace(
        match[i],
        self.createYoutubeEmbed(matchParts[1])
      );
    }

    // ok now put our links back where the placeholders were.
    if (matchlinks && matchlinks.length > 0) {
      for (var i = 0; i < matchlinks.length; i++) {
        resultHtml = resultHtml.replace(
          "#placeholder" + i + "#",
          matchlinks[i]
        );
      }
    }
  }
  return resultHtml;
};

cekQuotes = (param1) => {
  let regex = /[^\w\s]/gi;
  let retChar;
  if (regex.test(param1) == true) {
    retChar = param1.replace("'", "\\'");
    retChar = param1.replace(".", "\\ ");
  } else {
    retChar = param1;
  }
  return retChar;
};

function regexDownload(Text) {
  var a = Text;
  var b = /[^0-9a-zA-Z]+/g;
  var c = a.replace(b, "-");

  var Hasil = $.trim(c).toLowerCase();
  return Hasil;
}

const FUNCValidateUploadFileSize = (fi, maxSize = 2048, strMaxSize = "2MB") => {
  if (fi.files.length > 0) {
    for (var i = 0; i <= fi.files.length - 1; i++) {
      const fsize = fi.files.item(i).size;
      const file = Math.round(fsize / 1024);
      if (file >= maxSize) {
        swal({
          title: "Gagal",
          text: `Ukuran file terlalu besar, batas ukuran ${strMaxSize}`,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
        fi.value = "";
        return null;
      }
    }
  }
};

const FUNCValidateUploadFileExtension = (
  oInput,
  _validFileExtensions = [".jpg", ".jpeg", ".pdf", ".png"]
) => {
  var sFileName = oInput.value;
  if (sFileName.length > 0) {
    var blnValid = false;
    var msgExtension = "";
    for (var j = 0; j < _validFileExtensions.length; j++) {
      msgExtension += _validFileExtensions[j] + " ";
      var sCurExtension = _validFileExtensions[j];
      if (
        sFileName
          .substr(sFileName.length - sCurExtension.length, sCurExtension.length)
          .toLowerCase() == sCurExtension.toLowerCase()
      ) {
        blnValid = true;
        break;
      }
    }

    if (!blnValid) {
      swal({
        title: "Gagal",
        text: `Ekstensi file tidak didukung! format harus ${msgExtension}`,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
      oInput.value = "";
      return false;
    }
  }
};

$.fn.removeAttrs = function () {
  // Convert all passed arguments to an array
  var args = Array.prototype.slice.call(arguments),
    attr;

  // Handle passing arrays or space-separated strings
  if (args.length == 1)
    args = $.isArray(args[0]) ? args[0] : args[0].split(" ");

  // Loop, removing the first array item on each iteration
  while ((attr = args.shift())) this.removeAttr(attr);

  // Return the jQuery object for chaining
  return this;
};
