$(document).ready(function () {
    ComboGetTahunAjaran(function (obj) {
        $("select[name='IdTahunAjaran']").html(obj);
    },null,true);
    ComboGetSemester(function (obj) {
        $("select[name='IdSemester']").html(obj);
    });
})

const UrlService = {
    GetPenilaianDiriSiswa: base_url + '/api/siswas/GetPenilaianDiriSiswa',
    PenilaianDiriSiswaAdd: base_url + '/api/siswas/PenilaianDiriSiswaAdd'
};

function ModalCari() {
    $('#ModalPencarian').modal('show');
}

function Pencarian() {
    var TahunAjaran = $("select[name='IdTahunAjaran'] :selected").text();
    var TextSemester = $("select[name='IdSemester'] :selected").text();
    GetTabelPenilaianDiri();
    $('#ModalPencarian').modal('hide');
    $("#HeadingTabel").text("PENILAIAN DIRI SISWA Tahun Pelajaran : "+TahunAjaran+" "+TextSemester)
}

function GetTabelPenilaianDiri() {
    var IdTahunAjaran = $("select[name='IdTahunAjaran'] :selected").val();
    var Semester = $("select[name='IdSemester'] :selected").val();

    var TabelPenilaian = $('#TabelPenilaian').DataTable({
        "paging": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "pageLength": 5,
        "lengthChange": false,
        "scrollX": true,
        "scrollY": "500px",
        "scrollCollapse": true,
        "processing": true,
        "columnDefs": [{
            "targets": 1,
            "type": "date-id",
            "searchable": true
        }],
        "ajax": {
            "url": UrlService.GetPenilaianDiriSiswa + "?IdTahunAjaran=" + IdTahunAjaran+"&Semester=" + Semester,
            "method": 'GET',
            "beforeSend": function (xhr) {
            },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    swal({ title: 'Gagal Menampilkan Data Tabel Penilaian Siswa', text: json.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
                    return json;
                } else {
                    var IdDatas = new Array();
                    $.each( json.Data, function( key, value ) {
                        IdDatas.push(value.IdJenisPenilaianDiri);
                    });
                    sessionStorage.setItem("IdJenisPenilaianDiris",IdDatas);
                    //$("input[name='IdJenisPenilaianDiris']").val([Ids]);
                    return json.Data;
                }
            }
        },
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    return "<strong>" + full.JenisPenilaianDiri + "</strong>";
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var HtmlData = "";
                    if (full.Penilaian == 1) {
                        HtmlData += "<div style='text-align:center;'><input style='text-align:center;' type='radio' name='Question["+full.IdJenisPenilaianDiri+"]' value='1|" + full.IdJenisPenilaianDiri + "' checked='true'></div>";
                    } else {
                        HtmlData += "<div style='text-align:center;'><input style='text-align:center;' type='radio' name='Question["+full.IdJenisPenilaianDiri+"]' value='1|" + full.IdJenisPenilaianDiri + "'></div>";
                    }
                    
                    // if (full.Penilaian == 1) {
                    //     HtmlData += "<input type='radio' name='Question[0]' value='" + full.Penilaian + "|" + full.IdJenisPenilaianDiri + "' checked='true'>";
                    // } else {
                    //     HtmlData += "<input type='radio' name='Question[0]' value='" + full.Penilaian + "|" + full.IdJenisPenilaianDiri + "'>";
                    // }
                    return HtmlData;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var HtmlData = "";
                    if (full.Penilaian == 2) {
                        HtmlData += "<div style='text-align:center;'><input style='text-align:center;' type='radio' name='Question["+full.IdJenisPenilaianDiri+"]' value='2|" + full.IdJenisPenilaianDiri + "' checked='true'></div>";
                    } else {
                        HtmlData += "<div style='text-align:center;'><input style='text-align:center;' type='radio' name='Question["+full.IdJenisPenilaianDiri+"]' value='2|" + full.IdJenisPenilaianDiri + "'></div>";
                    }
                    return HtmlData;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var HtmlData = "";
                    if (full.Penilaian == 3) {
                        HtmlData += "<div style='text-align:center;'><input style='text-align:center;' type='radio' name='Question["+full.IdJenisPenilaianDiri+"]' value='3|" + full.IdJenisPenilaianDiri + "' checked='true'></div>";
                    } else {
                        HtmlData += "<div style='text-align:center;'><input style='text-align:center;' type='radio' name='Question["+full.IdJenisPenilaianDiri+"]' value='3|" + full.IdJenisPenilaianDiri + "'></div>";
                    }
                    return HtmlData;
                }
            },
        ],
        "bDestroy": true
    });
}

TambahPenilaian();
function TambahPenilaian() {
    ComboGetJenisEvaluasiHarian(function (obj) {
        $("select[name='IdJenisEvaluasiHarian']").html(obj);
    });
}

function ProsesPenilaian() {
    var DataLogin = JSON.parse(localStorage.getItem("DataLogin"));
    var IdTahunAjaran = $("select[name='IdTahunAjaran'] :selected").val();
    var Semester = $("select[name='IdSemester'] :selected").val();

    var PenilaianDatas = [];
    $.each(sessionStorage.getItem("IdJenisPenilaianDiris").split(","), function( key, value ) {
        //console.log(value);
        $("input[name='Question["+value+"]']", "#FormTabelPenilaianSiswa").each(function (i) {
            //console.log($(this).val());

            var SplitValue = $(this).val().split("|");
            
            if (this.checked) {
                var PenilaianData = {};
                PenilaianData.penilaian = parseInt(SplitValue[0]);
                PenilaianData.idJenisPenilaianDiri = parseInt(SplitValue[1]);
                PenilaianData.catatan = "-";

                PenilaianDatas.push(PenilaianData);
            } 
            else {
                if(parseInt(SplitValue[0]) == 1){
                    console.log("masuk sini", parseInt(SplitValue[1]));
                }
                
                // var PenilaianData = {};
                // PenilaianData.penilaian = 0;
                // PenilaianData.idJenisPenilaianDiri = parseInt(SplitValue[1]);
                // PenilaianData.catatan = "-";
            }
           
        });
    });
    
    console.log("DATA NYA : "+JSON.stringify(PenilaianDatas));
    SimpanPenilaian({
        idSiswa:DataLogin.IdSiswa,
        idTahunAjaran:IdTahunAjaran,
        idSemester:Semester,
        penilaianDiris:PenilaianDatas
    });
}

function SimpanPenilaian(Data) {
    var Url = UrlService.PenilaianDiriSiswaAdd;
    var JsonData = JSON.stringify(Data);
    console.log(JsonData);
    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        data: JsonData,
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function (before) {
            ProgressBar("wait");
        },
        success: function (responsesave) {
            ProgressBar("success");
            if (responsesave.IsSuccess == true) {
                GetTabelPenilaianDiri();
                swal({ title: 'Berhasil Menyimpan Penilaian Siswa', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-success text-white', confirmButtonText: 'Oke, Mengerti', type: 'success' });
            } else if (responsesave.IsSuccess == false) {
                swal({ title: 'Gagal Menyimpan Penilaian Siswa', text: responsesave.ReturnMessage, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
            }
        }, error: function (responserror, a, e) {
            ProgressBar("success");
            swal({ title: 'Error :(', text: JSON.stringify(responserror) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
        }
    });
}