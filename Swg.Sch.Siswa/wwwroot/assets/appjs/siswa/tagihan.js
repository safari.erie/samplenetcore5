﻿$(document).ready(function () {
    DataTabelTagihanBulanBerjalan();
    DataTabelTagihanBulanSebelumnya();
    $('#htmlTotalTagihan').html('Rp. ' + formatRupiah(JSON.parse(localStorage.getItem('TotalTagihan'))));
});
AutentikasiPinOrtu();

function DataTabelTagihanBulanBerjalan() {
    $('#TabelTagihanBulanBerjalan').DataTable({
        "paging": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "pageLength": 5,
        "lengthChange": false,
        "scrollX": true,
        "data": JSON.parse(localStorage.getItem('TagihanSiswaBulanBerjalan')),
        "columns": [
            {
                "data": "JenisTagihan"
            },
            {
                "render": function (data, type, full, meta) {
                    data = formatRupiah(full.Rp);
                    return data;
                }
            },
        ],
        "bDestroy": true
    });
}

function DataTabelTagihanBulanSebelumnya() {
    $('#TabelTagihanBulanSebelumnya').DataTable({
        "paging": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "pageLength": 5,
        "lengthChange": false,
        "scrollX": true,
        "data": JSON.parse(localStorage.getItem('TagihanSiswaBulanSebelumnya')),
        "columns": [
            {
                "data": "JenisTagihan"
            },
            {
                "data": "Rp"
            },
        ],
        "bDestroy": true
    });
    $('.dataTables_scrollHeadInner, .table-bordered').css('width', 'inherit');
}