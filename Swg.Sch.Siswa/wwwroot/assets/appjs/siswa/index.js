$(document).ready((e) => {
  var DataLogin = JSON.parse(localStorage.getItem("DataLogin"));
  if (DataLogin.IdUnit == 1 || DataLogin.IdUnit == 2) {
    $("#NoIjazah").removeAttrs("data-input", "label", "required");
    $("#FileIjazah").removeAttrs("data-input", "label", "required");
  }
  ComboGetAgamas(function (obj) {
    $("#IdAgama").html(obj);
  });
  ComboGetJenisPekerjaans(function (obj) {
    $("#IdJenisPekerjaanIbu").html(obj);
    $("#IdJenisPekerjaanAyah").html(obj);
    $("#IdJenisPekerjaanWali").html(obj);
  });
  ComboGetKelasParalels(function (obj) {
    $("#KelasDapodik").html(obj);
  });
  ComboGetJenisHobis(function (obj) {
    $("#IdHobi").html(obj);
  });
  ComboGetJenisCita2s(function (obj) {
    $("#IdCita2").html(obj);
  });
  $("#FotoProfile").change(function (e) {
    FUNCValidateUploadFileSize(e.target, 2048, "2MB");
    FUNCValidateUploadFileExtension(e.target, [".jpg", ".jpeg", ".png"]);
  });
  $("#FileKk").change(function (e) {
    FUNCValidateUploadFileSize(e.target, 2048, "2MB");
    FUNCValidateUploadFileExtension(e.target, [
      ".jpg",
      ".jpeg",
      ".png",
      ".pdf",
    ]);
  });
  $("#FileAktaLahir").change(function (e) {
    FUNCValidateUploadFileSize(e.target, 2048, "2MB");
    FUNCValidateUploadFileExtension(e.target, [
      ".jpg",
      ".jpeg",
      ".png",
      ".pdf",
    ]);
  });
  $("#FileKip").change(function (e) {
    FUNCValidateUploadFileSize(e.target, 2048, "2MB");
    FUNCValidateUploadFileExtension(e.target, [
      ".jpg",
      ".jpeg",
      ".png",
      ".pdf",
    ]);
  });
  $("#FileIjazah").change(function (e) {
    FUNCValidateUploadFileSize(e.target, 2048, "2MB");
    FUNCValidateUploadFileExtension(e.target, [
      ".jpg",
      ".jpeg",
      ".png",
      ".pdf",
    ]);
  });
  $("#FileShusbn").change(function (e) {
    FUNCValidateUploadFileSize(e.target, 2048, "2MB");
    FUNCValidateUploadFileExtension(e.target, [
      ".jpg",
      ".jpeg",
      ".png",
      ".pdf",
    ]);
  });
  $("#PunyaWali").change(function (e) {
    let val = e.target.value;
    if (val === "Ya") {
      ValidatePunyaWali("Ya");
    } else {
      ValidatePunyaWali("Tidak");
    }
  });

  $("select[name='IdProvinsi']").change((e) => {
    ComboGetKabkot(
      function (obj) {
        $("#IdKabkot").html(obj);
      },
      null,
      e.target.value
    );
  });
  $("select[name='IdKabkot']").change((e) => {
    ComboGetKecamatan(
      function (obj) {
        $("#IdKecamatan").html(obj);
      },
      null,
      e.target.value
    );
  });
  $("select[name='IdKecamatan']").change((e) => {
    ComboGetDesa(
      function (obj) {
        $("#IdDesa").html(obj);
      },
      null,
      e.target.value
    );
  });
  $("select[name='TerimaKip']").change((e) => {
    if (e.target.value === "Tidak") {
      $("#DivAlasanKip").fadeIn();
      $("#AlasanKip").attr({
        "data-input": "wajib",
        required: true,
      });
    } else {
      $("#DivAlasanKip").fadeOut();
      $("#AlasanKip").removeAttrs("data-input", "required");
    }
  });
  $("select[name='IdKewarganegaraan']").change((e) => {
    ValidateWni(e.target.value == 1);
  });
});

const ValidatePunyaWali = (type) => {
  if (type === "Ya") {
    $("#DivWali").fadeIn();
    $("#NikWali").attr("data-input", "wajib");
    $("#NikWali").attr("required", "true");

    $("#NamaWali").attr("data-input", "wajib");
    $("#NamaWali").attr("required", "true");

    $("#AlamatWali").attr("data-input", "wajib");
    $("#AlamatWali").attr("required", "true");

    $("#IdJenisPekerjaanWali").attr("data-input", "wajib");
    $("#IdJenisPekerjaanWali").attr("required", "true");

    $("#EmailWali").attr("data-input", "wajib");
    $("#EmailWali").attr("required", "true");

    $("#NoHandphoneWali").attr("data-input", "wajib");
    $("#NoHandphoneWali").attr("required", "true");

    $("#NamaInstansiWali").attr("data-input", "wajib");
    $("#NamaInstansiWali").attr("required", "true");
  } else {
    $("#DivWali").fadeOut();
    $("#NikWali").removeAttrs("data-input", "required");
    $("#NamaWali").removeAttrs("data-input", "required");
    $("#AlamatWali").removeAttrs("data-input", "required");
    $("#IdJenisPekerjaanWali").removeAttrs("data-input", "required");
    $("#EmailWali").removeAttrs("data-input", "required");
    $("#NoHandphoneWali").removeAttrs("data-input", "required");
    $("#NamaInstansiWali").removeAttrs("data-input", "required");
  }
};

const ValidateWni = (type) => {
  console.log("type:", type);
  if (!type) {
    $("#DivDataKartuKeluarga").fadeOut();
    $("#NoKk").removeAttrs("data-input", "required");
    $("#AlamatKk").removeAttrs("data-input", "required");
    $("#Rt").removeAttrs("data-input", "required");
    $("#Rw").removeAttrs("data-input", "required");
    $("#IdProvinsi").removeAttrs("data-input", "required");
    $("#IdKabkot").removeAttrs("data-input", "required");
    $("#Dusun").removeAttrs("data-input", "required");
    $("#KdPos").removeAttrs("data-input", "required");
    $("#NoTelp").removeAttrs("data-input", "required");
    $("#Latitude").removeAttrs("data-input", "required");
    $("#Longitude").removeAttrs("data-input", "required");
  } else {
    $("#DivDataKartuKeluarga").fadeIn();
    $("#NoKk").attr({
      "data-input": "wajib",
      required: true,
    });
    $("#AlamatKk").attr({
      "data-input": "wajib",
      required: true,
    });
    $("#Rt").attr({
      "data-input": "wajib",
      required: true,
    });
    $("#Rw").attr({
      "data-input": "wajib",
      required: true,
    });
    $("#IdProvinsi").attr({
      "data-input": "wajib",
      required: true,
    });
    $("#IdKabkot").attr({
      "data-input": "wajib",
      required: true,
    });
    $("#IdKecamatan").attr({
      "data-input": "wajib",
      required: true,
    });
    $("#Dusun").attr({
      "data-input": "wajib",
      required: true,
    });
    $("#KdPos").attr({
      "data-input": "wajib",
      required: true,
    });
    $("#NoTelp").attr({
      "data-input": "wajib",
      required: true,
    });
    $("#Latitude").attr({
      "data-input": "wajib",
      required: true,
    });
    $("#Longitude").attr({
      "data-input": "wajib",
      required: true,
    });
  }
};

function GetSiswaProfile() {
  var Url = base_url + "/api/Siswas/GetSiswaProfile";
  $.ajax({
    url: Url,
    type: "GET",
    dataType: "json",
    beforeSend: function (beforesend) {
      ProgressBar("wait");
    },
    success: function (res) {
      ProgressBar("success");
      if (res.IsSuccess == true) {
        var data = res.Data;
        $("input[name='IdSiswa']", "#FormEditProfilSiswa").val(data.IdSiswa);
        $("input[name='KelasParalel']", "#FormEditProfilSiswa").val(
          data.KelasParalel
        );
        $("input[name='Nis']", "#FormEditProfilSiswa").val(data.Nis);
        $("input[name='Nisn']", "#FormEditProfilSiswa").val(data.Nisn);
        $("input[name='Nik']", "#FormEditProfilSiswa").val(data.Nik);
        $("input[name='Nama']", "#FormEditProfilSiswa").val(data.Nama);
        $("input[name='NamaPanggilan']", "#FormEditProfilSiswa").val(
          data.NamaPanggilan
        );
        ComboGetKelasParalels(function (obj) {
          $("select[name='KelasDapodik']", "#FormEditProfilSiswa").html(obj);
        }, data.KelasDapodik);

        $("select[name='BerkebutuhanKhusus']", "#FormEditProfilSiswa").val(
          data.BerkebutuhanKhusus
        );
        $("input[name='AlamatKk']", "#FormEditProfilSiswa").val(data.AlamatKk);
        $("input[name='TempatLahir']", "#FormEditProfilSiswa").val(
          data.TempatLahir
        );
        $("input[name='TanggalLahir']", "#FormEditProfilSiswa").val(
          data.TanggalLahir
        );
        ComboGetAgamas(function (obj) {
          $("select[name='IdAgama']", "#FormEditProfilSiswa").html(obj);
        }, data.Agama.Id);
        $("select[name='KdJenisKelamin']").val(data.JenisKelamin.Kode);

        $("input[name='NoHandphone']", "#FormEditProfilSiswa").val(
          data.NoHandphone
        );
        $("input[name='NoDarurat']", "#FormEditProfilSiswa").val(
          data.NoDarurat
        );
        $("input[name='Email']", "#FormEditProfilSiswa").val(data.Email);
        $("input[name='AlamatTinggal']", "#FormEditProfilSiswa").val(
          data.AlamatTinggal
        );
        $("input[name='AlamatOrtu']", "#FormEditProfilSiswa").val(
          data.AlamatOrtu
        );
        $("input[name='NikIbu']", "#FormEditProfilSiswa").val(data.NikIbu);
        $("input[name='NamaIbu']", "#FormEditProfilSiswa").val(data.NamaIbu);

        ComboGetJenisPekerjaans(function (obj) {
          $("select[name='IdJenisPekerjaanIbu']", "#FormEditProfilSiswa").html(
            obj
          );
        }, data.JenisPekerjaanIbu.Id);

        $("input[name='NamaInstansiIbu']", "#FormEditProfilSiswa").val(
          data.NamaInstansiIbu
        );
        $("input[name='TanggalLahirIbu']", "#FormEditProfilSiswa").val(
          data.TanggalLahirIbu
        );
        $("select[name='BerkebutuhanKhususIbu']", "#FormEditProfilSiswa").val(
          data.BerkebutuhanKhususIbu
        );
        $("input[name='PendidikanTerakhirIbu']", "#FormEditProfilSiswa").val(
          data.PendidikanTerakhirIbu
        );
        $("select[name='PenghasilanIbu']", "#FormEditProfilSiswa").val(
          data.PenghasilanIbu
        );
        $("input[name='NomorHpIbu']", "#FormEditProfilSiswa").val(
          data.NomorHpIbu
        );
        $("input[name='NikAyah']", "#FormEditProfilSiswa").val(data.NikAyah);
        $("input[name='NamaAyah']", "#FormEditProfilSiswa").val(data.NamaAyah);
        ComboGetJenisPekerjaans(function (obj) {
          $("select[name='IdJenisPekerjaanAyah']", "#FormEditProfilSiswa").html(
            obj
          );
        }, data.JenisPekerjaanAyah?.Id);

        $("input[name='NamaInstansiAyah']", "#FormEditProfilSiswa").val(
          data.NamaInstansiAyah
        );
        $("input[name='TanggalLahirAyah']", "#FormEditProfilSiswa").val(
          data.TanggalLahirAyah
        );
        $("select[name='BerkebutuhanKhususAyah']", "#FormEditProfilSiswa").val(
          data.BerkebutuhanKhususAyah
        );
        $("input[name='PendidikanTerakhirAyah']", "#FormEditProfilSiswa").val(
          data.PendidikanTerakhirAyah
        );
        $("select[name='PenghasilanAyah']", "#FormEditProfilSiswa").val(
          data.PenghasilanAyah
        );
        $("input[name='NomorHpAyah']", "#FormEditProfilSiswa").val(
          data.NomorHpAyah
        );
        $("input[name='EmailOrtu']", "#FormEditProfilSiswa").val(
          data.EmailOrtu
        );
        $("input[name='NoAktaLahir']", "#FormEditProfilSiswa").val(
          data.NoAktaLahir
        );
        $("input[name='TinggiBadan']", "#FormEditProfilSiswa").val(
          data.TinggiBadan
        );
        $("input[name='BeratBadan']", "#FormEditProfilSiswa").val(
          data.BeratBadan
        );
        $("input[name='LingkarKepala']", "#FormEditProfilSiswa").val(
          data.LingkarKepala
        );
        $("input[name='AnakKe']", "#FormEditProfilSiswa").val(data.AnakKe);
        $("input[name='JumlahSodaraKandung']", "#FormEditProfilSiswa").val(
          data.JumlahSodaraKandung
        );
        $("input[name='JumlahSodaraTiri']", "#FormEditProfilSiswa").val(
          data.JumlahSodaraTiri
        );
        $("input[name='JumlahSodaraAngkat']", "#FormEditProfilSiswa").val(
          data.JumlahSodaraAngkat
        );
        $("input[name='JarakKeSekolah']", "#FormEditProfilSiswa").val(
          data.JarakKeSekolah
        );
        $("input[name='WaktuTempuh']", "#FormEditProfilSiswa").val(
          data.WaktuTempuh
        );
        $("input[name='PenyakitDiderita']", "#FormEditProfilSiswa").val(
          data.PenyakitDiderita
        );
        $("input[name='KelainanJasmani']", "#FormEditProfilSiswa").val(
          data.KelainanJasmani
        );
        $("input[name='SekolahAsal']", "#FormEditProfilSiswa").val(
          data.SekolahAsal
        );
        $("input[name='AlamatSekolahAsal']", "#FormEditProfilSiswa").val(
          data.AlamatSekolahAsal
        );
        $("input[name='NspnSekolahAsal']", "#FormEditProfilSiswa").val(
          data.NspnSekolahAsal
        );
        $("input[name='PernahTkFormal']", "#FormEditProfilSiswa").val(
          data.PernahTkFormal
        );
        $("input[name='PernahTlInformal']", "#FormEditProfilSiswa").val(
          data.PernahTlInformal
        );
        $("select[name='KdGolonganDarah']").val(data.GolonganDarah?.Kode);
        $("select[name='IdKewarganegaraan']").val(data.Kewarganegaraan?.Id);
        $("select[name='IdBahasa']").val(data.Bahasa?.Id);
        $("select[name='IdTransportasi']", "#FormEditProfilSiswa").val(
          data.Transportasi?.Id
        );
        ComboGetJenisHobis(function (obj) {
          $("select[name='IdHobi']", "#FormEditProfilSiswa").html(obj);
        }, data.Hobi?.Id);
        ComboGetJenisCita2s(function (obj) {
          $("select[name='IdCita2']", "#FormEditProfilSiswa").html(obj);
        }, data.Cita2?.Id);

        $("input[name='NoUjian']", "#FormEditProfilSiswa").val(data.NoUjian);
        $("input[name='NoIjazah']", "#FormEditProfilSiswa").val(data.NoIjazah);
        $("input[name='NoShusbn']", "#FormEditProfilSiswa").val(data.NoShusbn);

        $('select[name="PernahTkFormal"]').val("false");
        if (data.PernahTkFormal) $('select[name="PernahTkFormal"]').val("true");
        $('select[name="PernahTlInformal"]').val("false");
        if (data.PernahTlInformal)
          $('select[name="PernahTlInformal"]').val("true");

        $('select[name="PunyaWali"').val(data.PunyaWali);
        if (data.PunyaWali === "Ya") {
          ValidatePunyaWali("Ya");
          $("#NikWali").val(data.NikWali);
          $("#NamaWali").val(data.NamaWali);
          $("#AlamatWali").val(data.AlamatWali);
          $("#IdJenisPekerjaanWali").val(data.IdJenisPekerjaanWali);
          $("#NamaInstansiWali").val(data.NamaInstansiWali);
          $("#EmailWali").val(data.EmailWali);
          $("#NoHandphoneWali").val(data.NoHandphoneWali);
        }
        if (data.PunyaWali === "Tidak") ValidatePunyaWali("Tidak");

        $("input[name='NoKip']", "#FormEditProfilSiswa").val(data.NoKip);
        $('select[name="TerimaKip"').val(data.TerimaKip ? "Ya" : "Tidak");
        $("#AlasanKip").val(data.TerimaKip ? data.AlasanKip : "");
        $("#NoKk").val(data.NoKk);
        $("#KdPos").val(data.KdPos);
        $("#AlamatKk").val(data.AlamatKk);
        $("#Rt").val(data.Rt);
        $("#Rw").val(data.Rw);
        $("#NoTelp").val(data.NoTelp);
        $("#Longitude").val(data.Longitude);
        $("#Latitude").val(data.Latitude);

        ValidateWni(data.Kewarganegaraan?.Id == 1);

        ComboGetProvinsi(function (obj) {
          $("select[name='IdProvinsi']", "#FormEditProfilSiswa").html(obj);
        }, data.WilProv?.Id);

        if (data.WilProv !== null) {
          ComboGetKabkot(
            function (obj) {
              $("select[name='IdKabkot']", "#FormEditProfilSiswa").html(obj);
            },
            data.WilKabkot?.Id,
            data.WilProv?.Id
          );
        }
        if (data.WilKabkot !== null) {
          ComboGetKecamatan(
            function (obj) {
              $("select[name='IdKecamatan']", "#FormEditProfilSiswa").html(obj);
            },
            data.WilKec?.Id,
            data.WilKabkot?.Id
          );
        }
        if (data.WilKec !== null) {
          ComboGetDesa(
            function (obj) {
              $("select[name='IdDesa']", "#FormEditProfilSiswa").html(obj);
            },
            data.WilDesa?.Id,
            data.WilKec?.Id
          );
        }

        $("#PrvFotoProfil").attr(
          "src",
          `${base_url}/Asset/Files/Siswa/Foto/${data.FotoProfile}`
        );
        $("#PrvFileKk").attr(
          "href",
          `${base_url}/Asset/Files/Siswa/Foto/${data.FileKk}`
        );
        $("#PrvFileAktaLahir").attr(
          "href",
          `${base_url}/Asset/Files/Siswa/Foto/${data.FileAktaLahir}`
        );
        $("#PrvFileKip").attr(
          "href",
          `${base_url}/Asset/Files/Siswa/Foto/${data.FileKip}`
        );
        $("#PrvFileIjazah").attr(
          "href",
          `${base_url}/Asset/Files/Siswa/Foto/${data.FileIjazah}`
        );
        $("#PrvFileShusbn").attr(
          "href",
          `${base_url}/Asset/Files/Siswa/Foto/${data.FileShusbn}`
        );

        if (data.FileIjazah !== null)
          $("#FileIjazah").removeAttrs("data-input", "label", "required");
        if (data.FileKk !== null)
          $("#FileKk").removeAttrs("data-input", "label", "required");
        if (data.FileAktaLahir !== null)
          $("#FileAktaLahir").removeAttrs("data-input", "label", "required");
        if (data.FileKip !== null)
          $("#FileKip").removeAttrs("data-input", "label", "required");
        if (data.FileShusbn !== null)
          $("#FileShusbn").removeAttrs("data-input", "label", "required");
      } else {
        swal({
          title: "Gagal ",
          text: res.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (err, a, e) {
      ProgressBar("success");
    },
  });
}

function EditSiswaProfil() {
  var TypeInput = true;
  $('[data-input="wajib"]').each(function (i, v) {
    if ($(this).val() == "") {
      TypeInput = false;
      $("#FormEditProfilSiswa").addClass("was-validated");
      swal({
        title: "Gagal",
        text: "Kolom " + $(this).attr("label") + " Wajib Diisi",
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
      return i < 0;
    }
  });

  if (TypeInput) {
    var form = new FormData($("#FormEditProfilSiswa")[0]);

    form.append("TerimaKip", $('select[name="TerimaKip"]').val() === "Ya");
    form.append("IdWilDesa", $('select[name="IdDesa"]').val());

    form.append(
      "PernahTkFormal",
      $('select[name="PernahTkFormal"]').val() === "true"
    );
    form.append(
      "PernahTlInformal",
      $('select[name="PernahTlInformal"]').val() === "true"
    );
    form.append(
      "FotoProfile",
      $('input[type="file"][name="FotoProfile"]')[0].files[0]
    );
    form.append("FileKk", $('input[type="file"][name="FileKk"]')[0].files[0]);
    form.append(
      "FileAktaLahir",
      $('input[type="file"][name="FileAktaLahir"]')[0].files[0]
    );
    form.append("FileKip", $('input[type="file"][name="FileKip"]')[0].files[0]);
    form.append(
      "FileIjazah",
      $('input[type="file"][name="FileIjazah"]')[0].files[0]
    );
    form.append(
      "FileShusbn",
      $('input[type="file"][name="FileShusbn"]')[0].files[0]
    );
    $.ajax({
      url: base_url + "/api/Siswas/EditSiswaProfil",
      method: "POST",
      dataType: "json",
      data: form,
      contentType: false,
      cache: true,
      processData: false,
      beforeSend: function (before) {
        ProgressBar("wait");
      },
      success: function (res) {
        ProgressBar("success");
        if (res.IsSuccess == true) {
          $("#ModalEditProfilSiswa").modal("hide");
          iziToast.success({
            title: "Berhasil simpan profil",
            message: res.ReturnMessage,
            position: "topRight",
            timeout: 3000,
          });
          $("#FotoProfile").val("");
          $("#FileKk").val("");
          $("#FileAktaLahir").val("");
          $("#FileKip").val("");
          $("#FileIjazah").val("");
          $("#FileShusbn").val("");
          $("#FormEditProfilSiswa").removeClass("was-validated");
          $("#FormEditProfilSiswa")[0].reset();
        } else {
          iziToast.error({
            title: "Gagal",
            message: res.ReturnMessage,
            position: "topRight",
            timeout: 3000,
          });
        }
      },
      error: function (err, a, msg) {
        ProgressBar("success");
        swal({
          title: "Error",
          text: `${err.status} ${msg}`,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      },
    });
  }
}
