﻿$(document).ready(function (e) {
    DataQiroatyProgress("");
    DataQiroatyUjian("");
});

const UrlService = {
    GetKelasQuranSiswa: base_url + '/api/siswas/GetKelasQuranSiswa',
};

function ModalCariByTanggal() {
    $('#ModalTanggal').modal('show');
}

function PencarianByTanggal() {
    var Tanggal = $('#TanggalPencarian').val();
    DataQiroatyProgress(Tanggal);
    DataQiroatyUjian(Tanggal);
    $('#ModalTanggal').modal('hide');
}

function DataQiroatyProgress(Param) {
    $('#TabelProgress').DataTable({
        "paging": true,
        "searching": false,
        "ordering": false,
        "info": true,
        "pageLength": 100,
        "lengthChange": false,
        "processing": true,
        "order": [
            [1, "asc"]
        ],
        "columnDefs": [{
            "targets": 1,
            "type": "date-id",
            "searchable": true
        }],
        "ajax": {
            "url": UrlService.GetKelasQuranSiswa + "?Tanggal=" + Param,
            "method": 'GET',
            "beforeSend": function (xhr) { },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    swal({
                        title: 'Gagal',
                        text: json.ReturnMessage,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                    return json;
                } else {
                    return json.Data.Progress;
                }
            }
        },
        "columns": [
            {
                "data": "Hari"
            },
            {
                "render": function (data, type, full, meta) {
                    data = indoDate(full.Tanggal);
                    return data;
                }
            },
            {
                "data": "Jilid"
            },
            {
                "data": "Halaman"
            },
            {
                "data": "Kelompok"
            },
            {
                "data": "NamaGuru"
            },
        ],
        "bDestroy": true
    });
}

function DataQiroatyUjian(Param) {
    $('#TabelUjian').DataTable({
        "paging": true,
        "searching": false,
        "ordering": false,
        "info": true,
        "pageLength": 100,
        "lengthChange": false,
        "processing": true,
        "order": [
            [1, "asc"]
        ],
        "columnDefs": [{
            "targets": 1,
            "type": "date-id",
            "searchable": true
        }],
        "ajax": {
            "url": UrlService.GetKelasQuranSiswa + "?Tanggal=" + Param,
            "method": 'GET',
            "beforeSend": function (xhr) { },
            "dataSrc": function (json) {
                if (json.Data == null) {
                    swal({
                        title: 'Gagal',
                        text: json.ReturnMessage,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                    return json;
                } else {
                    return json.Data.Ujians;
                }
            }
        },
        "columns": [
            {
                "data": "Hari"
            },
            {
                "render": function (data, type, full, meta) {
                    data = indoDate(full.Tanggal);
                    return data;
                }
            },
            {
                "data": "Jilid"
            },
            {
                "data": "Halaman"
            },
            {
                "data": "Kelompok"
            },
            {
                "data": "NamaGuru"
            },
        ],
        "bDestroy": true
    });
}