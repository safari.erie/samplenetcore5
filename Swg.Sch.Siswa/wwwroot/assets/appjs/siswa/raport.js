﻿$(document).ready(function () {
  GetTabelRaport();

  ComboGetTahunAjaran(function (obj) {
    $("select#IdTahunAjaran").html(obj);
  });

  $("select#IdTahunAjaran").change((e) => {
    GetTabelRaport(e.target.value);
  });
});

const UrlService = {
  GetSiswaRaporsBySiswa: base_url + "/api/Siswas/GetSiswaRaporsBySiswa",
  GetSiswaRapors: base_url + "/api/Siswas/GetSiswaRapors",
};

function GetTabelRaport(IdTahunAjaran = 0) {
  var Url = "";
  if (IdTahunAjaran == 0) {
    Url = UrlService.GetSiswaRaporsBySiswa;
  } else {
    Url = UrlService.GetSiswaRapors + "?IdTahunAjaran=" + IdTahunAjaran;
  }
  //////// START SECTION TABEL RAPORT ///////////
  $("#TabelRaport").DataTable({
    paging: true,
    searching: true,
    ordering: false,
    info: false,
    pageLength: 5,
    lengthChange: false,
    scrollX: true,
    processing: true,
    ajax: {
      url: Url,
      method: "GET",
      beforeSend: function (xhr) {},
      dataSrc: function (json) {
        if (json.Data == null) {
          //iziToast.error({
          //    title: 'Gagal Menampilkan Data Tabel Raport',
          //    message: json.ReturnMessage,
          //    position: 'topRight'
          //});
          return json;
        } else {
          return json.Data;
        }
      },
    },
    columns: [
      { data: "JenisRapor" },
      {
        render: function (data, type, full, meta) {
          var data = "";
          var TotalTagihanSebelumnya = localStorage.getItem("TotalTagihan1");
          // if (TotalTagihanSebelumnya > 0) {
          // data = `<button type="button" onclick="AutentikasiPinOrtu('rapor');" class="btn btn-primary"><i class="fa fa-download"></i> Download</button>`;
          // } else {
          data =
            '<a href="' +
            base_url +
            "/Asset/Files/Siswa/Rapor/" +
            full.FileRapor +
            '" class="btn btn-primary"><i class="fa fa-download"></i> Download</a>';
          // }

          return data;
        },
      },
      {
        render: function (data, type, full, meta) {
          var data =
            "<b><i>[Disclaimer]</i></b> Dokumen raport asli yang sudah ditandatangan Kepala Sekolah disimpan di sekolah";
          return data;
        },
      },
    ],
    bDestroy: true,
  });
  $(".dataTables_filter").css("display", "none");

  //////// START SECTION TABEL RAPORT ///////////
}

function DownloadFile() {
  var Url = base_url + "/api/Siswas/ValidasiPin?Pin=" + formObj.PinOrtu;
  $.ajax({
    url: Url,
    type: "GET",
    dataType: "json",
    beforeSend: function (beforesend) {
      ProgressBar("wait");
    },
    success: function (responsesuccess) {
      ProgressBar("success");
      if (responsesuccess.IsSuccess == true) {
        NextAutentikasiPinOrtu("true", Proses);
      } else {
        $("#DivInputPinOrtu").addClass("animate__animated animate__shakeX");
        $("input[name='PinOrtu']", "#FormAutentikasiPinOrtu").val(null);
        NextAutentikasiPinOrtu("false", Proses);
        iziToast.error({
          title: "Gagal Autentikasi PIN :(",
          message: responsesuccess.ReturnMessage,
          position: "topRight",
          timeout: 3000,
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}
