﻿$(document).ready(function () {
    TabelRuangBelajar("");

    var DataLogin = JSON.parse(localStorage.getItem('DataLogin'))

    if (DataLogin.IdUnit == 1) {
        $('#jenis_bantuan_ortu').fadeIn();
        GetJenisBantuan();
    } else {
        $('#jenis_bantuan_ortu').hide();
    }

    $('#JawabanUrl').change(function (e) {
        if (e.target.checked) {
            $('#NamaUrl').val('Ya');
        } else {
            $('#NamaUrl').val('');
        }
    })

    $("input[type='file']").on("change", function () {
        if (this.files[0].size > 5000000) {
            swal({
                title: 'Gagal',
                text: 'Maksimum File 5MB, Silahkan dicompress terlebih dahulu',
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
            $(this).val('');
        }
    });
})

const UrlService = {
    GetJenisBantuan: base_url + '/api/siswas/GetJenisBantuan',
    GetKbmBySiswas: base_url + '/api/siswas/GetKbmBySiswas',
    GetKbmBySiswa: base_url + '/api/siswas/GetKbmBySiswa',
    UploadJawaban: base_url + '/api/siswas/UploadJawabans',
};

function ModalCariByTanggal() {
    $('#ModalTanggal').modal('show');
}

function PencarianByTanggal() {
    var Tanggal = $('#TanggalPencarian').val();
    TabelRuangBelajar(Tanggal);
    $('#ModalTanggal').modal('hide');
}

function TabelRuangBelajar(Param) {
    $('#TabelRuangBelajar tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="CARI ' + title + '" />');
    });
    var TabelRuangBelajar = $('#TabelRuangBelajar').DataTable({
        "paging": false,
        "searching": true,
        "ordering": true,
        "responsive": true,
        "info": true,
        "pageLength": 100,
        "lengthChange": false,
        "processing": true,
        "scrollX": true,
        "scrollY": "500px",
        "scrollCollapse": true,
        "order": [
            [1, "desc"]
        ],
        "columnDefs": [{
            "targets": 1,
            "type": "date-id",
            "searchable": true
        }],
        "ajax": {
            "url": UrlService.GetKbmBySiswas + "?Tanggal=" + Param,
            "method": 'GET',
            "beforeSend": function (xhr) {},
            "dataSrc": function (json) {
                if (json.Data == null) {
                    swal({
                        title: 'Gagal',
                        text: json.ReturnMessage,
                        confirmButtonClass: 'btn-danger text-white',
                        confirmButtonText: 'Oke, Mengerti',
                        type: 'error'
                    });
                    return json;
                } else {
                    $('html, body').animate({
                        scrollTop: $(".table-responsive").offset().top
                    }, 100);
                    return json.Data;
                }
            }
        },
        "columns": [{
                "render": function (data, type, full, meta) {
                    var str = full.NamaMapel;
                    var cekLibur = str.slice(0, 5);
                    if (cekLibur == "LIBUR") {
                        var Btn = '<button type="button" class="btn btn-info btn-sm">Libur &nbsp;<i class="fa fa-bullhorn"></i></button>';
                    } else {
                        var Btn = '<button type="button" onClick="GetSiswaMapel(' + full.IdKbmMateri + ')" class="btn btn-warning btn-sm">Ruang Belajar &nbsp;<i class="fas fa-arrow-right"></i></button>';
                    }
                    data = Btn;
                    return data;
                }
            },
            {
                "data": "Hari"
            },
            {
                "render": function (data, type, full, meta) {
                    data = full.JamMulai + " - " + full.JamSelesai;
                    return data;
                }
            },
            {
                "data": "NamaMapel"
            },
            {
                "render": function (data, type, full, meta) {
                    var str = full.NamaMapel;
                    var cekLibur = str.slice(0, 5);
                    var data = '';
                    if (full.IsRemedial == 1) {
                        data = '<span class="badge badge-danger">Remedial</span>';
                    } else if (full.PathFileUrl == null) {
                        if (full.JawabanSusulan != null) {
                            data = '<span class="badge badge-success">Sudah Mengerjakan</span>';
                        } else {
                            if (full.NilaiAngka == 0 || full.NilaiAngka == null) {
                                if (full.NilaiHuruf == null) {
                                    if (cekLibur == "LIBUR") {
                                        data = '<span class="badge badge-info">Libur</span>';
                                    } else {
                                        data = '<span class="badge badge-warning">Belum Mengerjakan</span>';
                                    }
                                } else {
                                    data = '<span class="badge badge-success">Sudah Mengerjakan</span>';
                                }
                            } else {

                            }
                        }
                    } else {
                        data = '<span class="badge badge-success">Sudah Mengerjakan</span>';
                    }

                    var catatan = '';
                    if (full.Catatan != null || full.JawabanSusulan != null) {
                        var CatatanSusulan = "";
                        if (full.JawabanSusulan != null) {
                            CatatanSusulan = full.JawabanSusulan.Catatan;
                        } else {
                            CatatanSusulan = null;
                        }

                        var ParamCatatan = "";
                        ParamCatatan = "ModalCatatan('" + full.Catatan + "','" + CatatanSusulan + "')";
                        catatan = '<a href="javascript:void(0);" onclick="' + ParamCatatan + '"><i class="fa fa-info-circle"></i></a>';
                    }
                    // return data + '&nbsp;&nbsp;' + catatan;
                    return data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var str = full.NamaMapel;
                    var cekLibur = str.slice(0, 5);
                    var data = '';
                    if (full.NilaiHuruf == null) {
                        if (cekLibur == "LIBUR") {
                            data = '-';
                        } else {
                            data = full.NilaiAngka;
                        }
                    } else {
                        data = full.NilaiHuruf;
                    }
                    return data;
                }
            },
            {
                "data": "Catatan"
            },
            {
                "data": "Guru"
            },
            {
                "render": function (data, type, full, meta) {
                    var str = full.NamaMapel;
                    var cekLibur = str.slice(0, 5);
                    var data = '';
                    if (cekLibur == "LIBUR") {
                        data = 'Tidak ada';
                    } else {
                        data = full.JudulMateri;
                    }
                    return data;
                }
            },
            {
                "render": function (data, type, full, meta) {
                    data = indoDate(full.Tanggal);
                    return data;
                }
            },
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            //console.log(aData.JenisData);
            $(nRow).find('td:eq(0)').css('font-weight', '300');
            $(nRow).find('td:eq(1)').css('font-weight', '300');
            $(nRow).find('td:eq(2)').css('font-weight', '300');
            $(nRow).find('td:eq(3)').css('font-weight', '300');
            $(nRow).find('td:eq(4)').css('font-weight', '300');
            $(nRow).find('td:eq(5)').css('font-weight', '300');
            $(nRow).find('td:eq(6)').css('font-weight', '300');
            $(nRow).find('td:eq(7)').css('font-weight', '300');
            $(nRow).find('td:eq(8)').css('font-weight', '300');
            var str = aData.NamaMapel;
            var cekLibur = str.slice(0, 5);
            if (cekLibur == "LIBUR") {
                //cell background color
                $(nRow).find('td:eq(0)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(0)').css('color', '#5e5f61');
                $(nRow).find('td:eq(1)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(1)').css('color', '#5e5f61');
                $(nRow).find('td:eq(2)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(2)').css('color', '#5e5f61');
                $(nRow).find('td:eq(3)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(3)').css('color', '#5e5f61');
                $(nRow).find('td:eq(3)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(3)').css('color', '#5e5f61');
                $(nRow).find('td:eq(4)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(4)').css('color', '#5e5f61');
                $(nRow).find('td:eq(5)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(5)').css('color', '#5e5f61');
                $(nRow).find('td:eq(6)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(6)').css('color', '#5e5f61');
                $(nRow).find('td:eq(7)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(7)').css('color', '#5e5f61');
                $(nRow).find('td:eq(8)').css('background-color', '#ffceced4');
                $(nRow).find('td:eq(8)').css('color', '#5e5f61');
            }
        },
        "bDestroy": true
    });
    TabelRuangBelajar.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
    $(".dataTables_filter").css("display", "none");
    $(".dataTables_length").css("display", "none");


    // MOBILE SECTION
    $.ajax({
        url: UrlService.GetKbmBySiswas + "?Tanggal=" + Param,
        type: 'GET',
        success: function (res) {
            if (res.IsSuccess) {

                var data = res.Data;

                var totalSoal = 0;
                $.each(data, (i, v) => {
                    var Mapel = v.NamaMapel;
                    var cekLibur = Mapel.slice(0, 5);
                    if (cekLibur != "LIBUR") {
                        totalSoal += 1;
                    }
                });
                $('#totalSoal').html(totalSoal);

                var totalSoalPending = 0;
                var totalSoalRemedial = 0;
                var totalSoalSudah = 0;
                $.each(data, (i, v) => {
                    var Mapel = v.NamaMapel;
                    var cekLibur = Mapel.slice(0, 5);
                    if (cekLibur != "LIBUR") {

                        if (v.IsRemedial == 1) {
                            totalSoalRemedial += 1;
                        } else if (v.PathFileUrl == null) {
                            if (v.JawabanSusulan != null) {
                                totalSoalSudah += 1;
                            } else {
                                if (v.NilaiAngka == 0 || v.NilaiAngka == null) {
                                    if (v.NilaiHuruf == null) {
                                        totalSoalPending += 1;
                                    } else {
                                        totalSoalSudah += 1;
                                    }
                                }
                            }
                        } else {
                            totalSoalSudah += 1;
                        }
                    }
                });
                $('#totalSoalPending').html(totalSoalPending);
                $('#totalSoalRemedial').html(totalSoalRemedial);
                $('#totalSoalSudah').html(totalSoalSudah);
                var htmlMobile = '';
                $.each(data, (i, v) => {
                    var Mapel = v.NamaMapel;
                    var cekLibur = Mapel.slice(0, 5);
                    var data = '';
                    if (v.IsRemedial == 1) {
                        data = '<span class="badge badge-danger">Remedial</span>';
                    } else if (v.PathFileUrl == null) {
                        if (v.JawabanSusulan != null) {
                            data = '<span class="badge badge-success">Sudah Mengerjakan</span>';
                        } else {
                            if (v.NilaiAngka == 0 || v.NilaiAngka == null) {
                                if (v.NilaiHuruf == null) {
                                    if (cekLibur == "LIBUR") {
                                        data = '<span class="badge badge-info">Libur</span>';
                                    } else {
                                        data = '<span class="badge badge-warning">Belum Mengerjakan</span>';
                                    }
                                } else {
                                    data = '<span class="badge badge-success">Sudah Mengerjakan</span>';
                                }
                            } else {

                            }
                        }
                    } else {
                        data = '<span class="badge badge-success">Sudah Mengerjakan</span>';
                    }

                    var catatan = '';
                    if (v.Catatan != null || v.JawabanSusulan != null) {
                        var CatatanSusulan = "";
                        if (v.JawabanSusulan != null) {
                            CatatanSusulan = v.JawabanSusulan.Catatan;
                        } else {
                            CatatanSusulan = null;
                        }

                        var ParamCatatan = "";
                        ParamCatatan = "ModalCatatan('" + v.Catatan + "','" + CatatanSusulan + "')";
                        catatan = '<a href="javascript:void(0);" onclick="' + ParamCatatan + '"><i class="fa fa-info-circle"></i></a>';
                    }

                    var nilai = '';
                    if (v.NilaiHuruf == null) {
                        if (v.NilaiAngka == 0 || v.NilaiAngka == null) {
                            nilai = '';
                        } else {
                            nilai = '<b>Nilai : ' + v.NilaiAngka + '</b>';
                        }
                    } else {
                        nilai = '<b>Nilai : ' + v.NilaiHuruf + '</b>';
                    }

                    var JudulMateri = v.JudulMateri == null ? 'Tidak Ada' : v.JudulMateri;


                    if (cekLibur == "LIBUR") {
                        htmlMobile +=
                            '<li class="media">' +
                            '    <img class="mr-3 rounded" width="55" src="' + base_url + '/assets/img/avatar/avatar-0.png" alt="product" />' +
                            '    <div class="media-body">' +
                            '        <div class="float-right"><div class="font-weight-600 text-muted text-small">' + v.Hari + '</div></div>' +
                            '        <div class="media-title">' + v.NamaMapel + ' ' + data + '&nbsp;&nbsp;' + catatan + '</div>' +
                            '        <div class="mt-1">' +
                            '            <div class="budget-price">' +
                            '                <div class="budget-price-label">Guru: ' + v.Guru + '</div>' +
                            '            </div>' +
                            '            <div class="budget-price">' +
                            '                <div class="budget-price-label">Materi: ' + JudulMateri + '</div>' +
                            '            </div>' +
                            '            <div class="budget-price">' +
                            '                <div class="budget-price-label">(' + v.JamMulai + ' s/d ' + v.JamSelesai + ')</div>' +
                            '            </div>' +
                            '            <div class="budget-price">' +
                            '                <div class="budget-price-label">' + nilai + '</div>' +
                            '            </div>' +
                            '        </div>' +
                            '    </div>' +
                            '</li>';
                    } else {
                        htmlMobile +=
                            '<li class="media">' +
                            '    <img class="mr-3 rounded" width="55" src="' + base_url + '/assets/img/avatar/avatar-0.png" alt="product" />' +
                            '    <div class="media-body">' +
                            '        <a href="javascript:void(0)" onclick="GetSiswaMapel(' + v.IdKbmMateri + ')" style="text-decoration:none;color:inherit"><div class="float-right"><div class="font-weight-600 text-muted text-small">' + v.Hari + '</div></div>' +
                            '        <div class="media-title">' + v.NamaMapel + ' ' + data + '&nbsp;&nbsp;' + catatan + '</div>' +
                            '        <div class="mt-1">' +
                            '            <div class="budget-price">' +
                            '                <div class="budget-price-label">Guru: ' + v.Guru + '</div>' +
                            '            </div>' +
                            '            <div class="budget-price">' +
                            '                <div class="budget-price-label">Materi: ' + JudulMateri + '</div>' +
                            '            </div>' +
                            '            <div class="budget-price">' +
                            '                <div class="budget-price-label">(' + v.JamMulai + ' s/d ' + v.JamSelesai + ')</div>' +
                            '            </div>' +
                            '            <div class="budget-price">' +
                            '                <div class="budget-price-label">' + nilai + '</div>' +
                            '            </div>' +
                            '            <div class="budget-price">' +
                            '                <div class="budget-price-label">'+ (v.Catatan != null ? '<b>Catatan</b> : ' : '') + (v.Catatan != null ? v.Catatan : '') + '</div>' +
                            '            </div>' +
                            '        </div></a>' +
                            '    </div>' +
                            '</li>';
                    }

                });
                $('#RuangMobile').html(htmlMobile);

            } else {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    })
}

function GetSiswaMapel(IdJadwal) {
    $('#myTab a[href="#materi"]').tab('show');
    $('#FileUpload').val('');
    $('#NamaUrl').val('');
    $.ajax({
        url: UrlService.GetKbmBySiswa + '?IdKbmMateri=' + IdJadwal,
        type: 'GET',
        dataType: 'JSON',
        beforeSend: (e) => {
            $('#secMapel').hide();
            $('#secLoading').fadeIn();
        },
        success: (res) => {
            var data = res.Data;
            console.log(UrlService.GetKbmBySiswa + '?IdKbmMateri=' + IdJadwal)
            if (res.IsSuccess) {

                setTimeout(() => {
                    $('#secLoading').hide();
                    $('#secSoal').fadeIn();

                    $('#cardSoalEmpty').hide();
                    $('#cardSoal').fadeIn();
                }, 2000)

                $('#IdKbmMateri').val(data.IdKbmMateri);

                $("#materi_foto_guru").attr("src", base_url + "/Assets/User/FotoProfil/" + data.FotoGuru);

                $('#materi_guru').html(data.Guru);
                $('#materi_mapel').html(data.NamaMapel);
                $('#materi_judul').html(data.JudulMateri);
                $('#materi_deskripsi').html(data.Deskripsi);

                var htmlDownloadFileMateri = '';
                if (data.NamaFileMateri != null) {
                    $('#materi_downloadfile').html('<a download="MATERI ' + regexDownload(data.NamaMapel) + " " + regexDownload(data.JudulMateri) + " " + regexDownload(data.Tanggal) + " " + regexDownload(data.Guru) + '" href="' + base_url + '/Asset/Files/Elearning/' + data.NamaFileMateri + '" class="btn btn-warning" download>Download File Materi</a>');
                } else {
                    $('#materi_downloadfile').html('');
                }

                let htmlGambar = '';
                let htmlVideo = '';
                $.each(data.FileKbmMedias, (i, v) => {
                    if (v.IdJenisPathFile == 1) {
                        var Path = '';
                        Path = "MateriGambar('" + v.PathFileUrl + "')";

                        htmlGambar += '<div class="col-md-3">';
                        htmlGambar += '<img src="' + base_url + '/Asset/Files/Elearning/' + v.PathFileUrl + '" class="img-fluid" onclick="' + Path + '" style="cursor:pointer;" />'
                        htmlGambar += '</div>';
                    } else if (v.IdJenisPathFile == 2) {
                        var urlEmbedYt = getEmbedYt('"' + v.PathFileUrl + '"');
                        htmlVideo += '<div class="col-md-3">';
                        htmlVideo += '<iframe width="100%" height="300px" class="img-fluid" src=' + urlEmbedYt + ' frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
                        htmlVideo += '</div>';
                    }
                });
                $('#materi_gambar').html(htmlGambar);
                $('#materi_video').html(htmlVideo);

                $('#soal_deskripsi').html(data.DeskripsiTugas);
                var htmlDownloadSoal = '';

                if (data.IsRemedial == 0) {
                    if (data.SoalReguler != null) {
                        $('#IdJenisSoal').val(data.SoalReguler.IdJenisSoal);
                        if (data.IdJenisSoal == 1) {
                            if (data.SoalReguler.NamaFile != null) {
                                htmlDownloadSoal = '<a download="SOAL ' + regexDownload(data.SoalReguler.JenisSoal) + " " + regexDownload(data.NamaMapel) + " " + regexDownload(data.JudulMateri) + " " + regexDownload(data.Tanggal) + " " + regexDownload(data.Guru) + '" class="btn btn-warning" href="' + base_url + '/Asset/Files/Elearning/' + data.SoalReguler.NamaFile + '" download><i class="fas fa-download"></i> Download Soal</a>';
                            } else if (data.SoalReguler.NamaUrl != null) {
                                var urlEmbedYt = getEmbedYt('"' + data.SoalReguler.NamaUrl + '"');
                                htmlDownloadSoal = '<iframe width="500px" height="500px" class="img-fluid" src=' + urlEmbedYt + ' frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                                //htmlDownloadSoal = '<iframe width="500px" height="500px" class="img-fluid" src="' + data.SoalReguler.NamaUrl + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                            } else {
                                htmlDownloadSoal = '';
                            }
                        } else if (data.IdJenisSoal == 2) {
                            if (data.SoalRemedial.NamaFile != null) {
                                htmlDownloadSoal = '<a download="SOAL ' + regexDownload(data.SoalRemedial.JenisSoal) + " " + regexDownload(data.NamaMapel) + " " + regexDownload(data.JudulMateri) + " " + regexDownload(data.Tanggal) + " " + regexDownload(data.Guru) + '" class="btn btn-warning" href="' + base_url + '/Asset/Files/Elearning/' + data.SoalRemedial.NamaFile + '" download><i class="fas fa-download"></i> Download Soal Remedial</a>';
                            } else if (data.SoalRemedial.NamaUrl != null) {
                                var urlEmbedYt = getEmbedYt('"' + data.SoalRemedial.NamaUrl + '"');
                                htmlDownloadSoal = '<iframe width="500px" height="500px" class="img-fluid" src=' + urlEmbedYt + ' frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                                //htmlDownloadSoal = '<iframe width="500px" height="500px" class="img-fluid" src="' + data.SoalRemedial.NamaUrl + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                            } else {
                                htmlDownloadSoal = '';
                            }
                        } else {
                            if (data.SoalReguler != null) {
                                if (data.SoalReguler.NamaFile != null) {
                                    htmlDownloadSoal = '<a download="SOAL ' + regexDownload(data.SoalReguler.JenisSoal) + " " + regexDownload(data.NamaMapel) + " " + regexDownload(data.JudulMateri) + " " + regexDownload(data.Tanggal) + " " + regexDownload(data.Guru) + '" class="btn btn-warning" href="' + base_url + '/Asset/Files/Elearning/' + data.SoalReguler.NamaFile + '" download><i class="fas fa-download"></i> Download Soal</a>';
                                }
                            } else if (data.SoalRemedial != null) {
                                if (data.SoalRemedial.NamaUrl != null) {
                                    var urlEmbedYt = getEmbedYt('"' + data.SoalReguler.NamaUrl + '"');
                                    htmlDownloadSoal = '<iframe width="500px" height="500px" class="img-fluid" src=' + urlEmbedYt + ' frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                                }
                            } else {
                                htmlDownloadSoal = '';
                            }
                        }

                        // if (data.IdJenisSoal == 1) {
                        //     if (data.SoalReguler.IdJenisJawaban == 1) {
                        //         if (data.NilaiAngka == 0 || data.NilaiAngka == null) {
                        //             if (data.NilaiHuruf == null) {
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').hide();
                        //                 $('#jawaban_upload').show();
                        //             } else {
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').hide();
                        //                 $('#jawaban_upload').show();
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //             }
                        //         } else {
                        //             $('#DivUploadJawabanClose').fadeIn();
                        //             $('#DivUploadJawabanOpen').hide();
                        //             $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //         }

                        //     } else if (data.SoalReguler.IdJenisJawaban == 2) {
                        //         if (data.NilaiAngka == 0 || data.NilaiAngka == null) {
                        //             if (data.NilaiHuruf == null) {
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_rubrik').hide();
                        //                 $('#jawaban_youtube').show();
                        //             } else {
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_rubrik').hide();
                        //                 $('#jawaban_youtube').show();
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //             }
                        //         } else {
                        //             $('#DivUploadJawabanClose').fadeIn();
                        //             $('#DivUploadJawabanOpen').hide();
                        //             $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //         }
                        //     } else if (data.SoalReguler.IdJenisJawaban == 3) {
                        //         if (data.NilaiAngka == 0 || data.NilaiAngka == null) {
                        //             if (data.NilaiHuruf == null) {
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').show();
                        //             } else {
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').show();
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //             }
                        //         } else {
                        //             $('#DivUploadJawabanClose').fadeIn();
                        //             $('#DivUploadJawabanOpen').hide();
                        //             $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //         }
                        //     } else if (data.SoalReguler.IdJenisJawaban == 0) {
                        //         // $('#jawaban_upload').hide();
                        //         // $('#jawaban_youtube').hide();
                        //         // $('#jawaban_rubrik').hide();
                        //         // $('#DivUploadJawabanClose').fadeIn();
                        //         // $('#DivUploadJawabanOpen').hide();
                        //         // $('#htmlInfoUploadJawaban').html('Tidak ada upload jawabans');
                        //         if (data.NilaiAngka == 0 || data.NilaiAngka == null) {
                        //             if (data.NilaiHuruf == null) {
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').show();
                        //             } else {
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').show();
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //             }
                        //         } else {
                        //             $('#DivUploadJawabanClose').fadeIn();
                        //             $('#DivUploadJawabanOpen').hide();
                        //             $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //         }
                        //         /*end*/
                        //     }
                        // } else if (data.IdJenisSoal == 2) {
                        //     if (data.SoalRemedial.IdJenisJawaban == 1) {
                        //         $('#jawaban_upload').show();
                        //         $('#jawaban_youtube').hide();
                        //         $('#jawaban_rubrik').hide();
                        //         $('#DivUploadJawabanClose').hide();
                        //         $('#DivUploadJawabanOpen').fadeIn();
                        //         $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //     } else if (data.SoalRemedial.IdJenisJawaban == 2) {
                        //         $('#jawaban_youtube').show();
                        //         $('#jawaban_upload').hide();
                        //         $('#jawaban_rubrik').hide();
                        //         $('#DivUploadJawabanClose').hide();
                        //         $('#DivUploadJawabanOpen').fadeIn();
                        //         $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //     } else if (data.SoalRemedial.IdJenisJawaban == 3) {
                        //         $('#jawaban_rubrik').show();
                        //         $('#jawaban_youtube').hide();
                        //         $('#jawaban_upload').hide();
                        //         $('#DivUploadJawabanClose').hide();
                        //         $('#DivUploadJawabanOpen').fadeIn();
                        //         $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //     } else if (data.SoalRemedial.IdJenisJawaban == 0) {
                        //         // $('#jawaban_upload').hide();
                        //         // $('#jawaban_youtube').hide();
                        //         // $('#jawaban_rubrik').hide();
                        //         // $('#DivUploadJawabanClose').fadeIn();
                        //         // $('#DivUploadJawabanOpen').hide();
                        //         // $('#htmlInfoUploadJawaban').html('Tidak ada upload jawabans');
                        //         if (data.NilaiAngka == 0 || data.NilaiAngka == null) {
                        //             if (data.NilaiHuruf == null) {
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').show();
                        //             } else {
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').show();
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //             }
                        //         } else {
                        //             $('#DivUploadJawabanClose').fadeIn();
                        //             $('#DivUploadJawabanOpen').hide();
                        //             $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //         }
                        //         /*end*/
                        //     }
                        // } else {
                        //     if (data.SoalReguler.IdJenisJawaban == 1) {
                        //         if (data.NilaiAngka == 0 || data.NilaiAngka == null) {
                        //             if (data.NilaiHuruf == null) {
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').hide();
                        //                 $('#jawaban_upload').show();
                        //             } else {
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').hide();
                        //                 $('#jawaban_upload').show();
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //             }
                        //         } else {
                        //             $('#DivUploadJawabanClose').fadeIn();
                        //             $('#DivUploadJawabanOpen').hide();
                        //             $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //         }

                        //     } else if (data.SoalReguler.IdJenisJawaban == 2) {
                        //         if (data.NilaiAngka == 0 || data.NilaiAngka == null) {
                        //             if (data.NilaiHuruf == null) {
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //                 $('#jawaban_rubrik').hide();
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_youtube').show();
                        //             } else {
                        //                 $('#jawaban_rubrik').hide();
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_youtube').show();
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //             }
                        //         } else {
                        //             $('#DivUploadJawabanClose').fadeIn();
                        //             $('#DivUploadJawabanOpen').hide();
                        //             $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //         }
                        //     } else if (data.SoalReguler.IdJenisJawaban == 3) {
                        //         if (data.NilaiAngka == 0 || data.NilaiAngka == null) {
                        //             if (data.NilaiHuruf == null) {
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_rubrik').show();
                        //             } else {
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_rubrik').show();
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //             }
                        //         } else {
                        //             $('#DivUploadJawabanClose').fadeIn();
                        //             $('#DivUploadJawabanOpen').hide();
                        //             $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //         }
                        //     } else if (data.SoalReguler.IdJenisJawaban == 0) {
                        //         // $('#jawaban_upload').hide();
                        //         // $('#jawaban_youtube').hide();
                        //         // $('#jawaban_rubrik').hide();
                        //         // $('#DivUploadJawabanClose').fadeIn();
                        //         // $('#DivUploadJawabanOpen').hide();
                        //         // $('#htmlInfoUploadJawaban').html('Tidak ada upload jawabans');
                        //         if (data.NilaiAngka == 0 || data.NilaiAngka == null) {
                        //             if (data.NilaiHuruf == null) {
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').show();
                        //             } else {
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').show();
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //             }
                        //         } else {
                        //             $('#DivUploadJawabanClose').fadeIn();
                        //             $('#DivUploadJawabanOpen').hide();
                        //             $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //         }
                        //         /*end*/
                        //     }
                        // }

                    } else {
                        htmlDownloadSoal = '<button class="btn btn-danger" type="button"><i class="fas fa-times"></i> Tidak Ada Soal</button>';
                        //$('#DivUploadJawabanClose').fadeIn();
                        //$('#DivUploadJawabanOpen').hide();
                        $('#DivUploadJawabanClose').hide();
                        $('#DivUploadJawabanOpen').fadeIn();
                        $('#jawaban_upload').hide();
                        $('#jawaban_youtube').hide();
                        $('#jawaban_rubrik').show();
                    }
                } else if (data.IsRemedial == 1) {
                    if (data.SoalRemedial != null) {
                        $('#IdJenisSoal').val(data.SoalRemedial.IdJenisSoal);
                        if (data.IdJenisSoal == 1) {
                            if (data.SoalReguler.NamaFile != null) {
                                htmlDownloadSoal = '<a download="SOAL ' + regexDownload(data.SoalReguler.JenisSoal) + " " + regexDownload(data.NamaMapel) + " " + regexDownload(data.JudulMateri) + " " + regexDownload(data.Tanggal) + " " + regexDownload(data.Guru) + '" class="btn btn-warning" href="' + base_url + '/Asset/Files/Elearning/' + data.SoalReguler.NamaFile + '" download><i class="fas fa-download"></i> Download Soal</a>';
                            } else {
                                var urlEmbedYt = getEmbedYt('"' + data.SoalReguler.NamaUrl + '"');
                                htmlDownloadSoal = '<iframe width="500px" height="500px" class="img-fluid" src=' + urlEmbedYt + ' frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                            }
                        } else if (data.IdJenisSoal == 2) {
                            if (data.SoalRemedial.NamaFile != null) {
                                htmlDownloadSoal = '<a download="SOAL ' + regexDownload(data.SoalRemedial.JenisSoal) + " " + regexDownload(data.NamaMapel) + " " + regexDownload(data.JudulMateri) + " " + regexDownload(data.Tanggal) + " " + regexDownload(data.Guru) + '" class="btn btn-warning" href="' + base_url + '/Asset/Files/Elearning/' + data.SoalRemedial.NamaFile + '" download><i class="fas fa-download"></i> Download Soal Remedial</a>';
                            } else {
                                var urlEmbedYt = getEmbedYt('"' + data.SoalRemedial.NamaUrl + '"');
                                htmlDownloadSoal = '<iframe width="500px" height="500px" class="img-fluid" src=' + urlEmbedYt + ' frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                                //htmlDownloadSoal = '<iframe width="500px" height="500px" class="img-fluid" src="' + data.SoalRemedial.NamaUrl + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                            }
                        } else {
                            if (data.SoalRemedial.NamaFile != null) {
                                htmlDownloadSoal = '<a download="SOAL ' + regexDownload(data.SoalRemedial.JenisSoal) + " " + regexDownload(data.NamaMapel) + " " + regexDownload(data.JudulMateri) + " " + regexDownload(data.Tanggal) + " " + regexDownload(data.Guru) + '" class="btn btn-warning" href="' + base_url + '/Asset/Files/Elearning/' + data.SoalRemedial.NamaFile + '" download><i class="fas fa-download"></i> Download Soal Remedial</a>';
                            } else {
                                var urlEmbedYt = getEmbedYt('"' + data.SoalRemedial.NamaUrl + '"');
                                htmlDownloadSoal = '<iframe width="500px" height="500px" class="img-fluid" src=' + urlEmbedYt + ' frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                            }
                        }

                        // if (data.IdJenisSoal == 1) {
                        //     if (data.SoalReguler.IdJenisJawaban == 1) {
                        //         if (data.NilaiAngka == 0 || data.NilaiAngka == null) {
                        //             if (data.NilaiHuruf == null) {
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').hide();
                        //                 $('#jawaban_upload').show();
                        //             } else {
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').hide();
                        //                 $('#jawaban_upload').show();
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //             }
                        //         } else {
                        //             $('#DivUploadJawabanClose').fadeIn();
                        //             $('#DivUploadJawabanOpen').hide();
                        //         }

                        //         $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //     } else if (data.SoalReguler.IdJenisJawaban == 2) {
                        //         if (data.NilaiAngka == 0 || data.NilaiAngka == null) {
                        //             if (data.NilaiHuruf == null) {
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_rubrik').hide();
                        //                 $('#jawaban_youtube').show();
                        //             } else {
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_rubrik').hide();
                        //                 $('#jawaban_youtube').show();
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //             }
                        //         } else {
                        //             $('#DivUploadJawabanClose').fadeIn();
                        //             $('#DivUploadJawabanOpen').hide();
                        //         }
                        //         $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //     } else if (data.SoalReguler.IdJenisJawaban == 3) {
                        //         if (data.NilaiAngka == 0 || data.NilaiAngka == null) {
                        //             if (data.NilaiHuruf == null) {
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').show();
                        //             } else {
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').show();
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //             }
                        //         } else {
                        //             $('#DivUploadJawabanClose').fadeIn();
                        //             $('#DivUploadJawabanOpen').hide();
                        //         }
                        //         $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //     } else if (data.SoalReguler.IdJenisJawaban == 0) {
                        //         // $('#jawaban_upload').hide();
                        //         // $('#jawaban_rubrik').hide();
                        //         // $('#jawaban_youtube').hide();
                        //         // $('#DivUploadJawabanClose').fadeIn();
                        //         // $('#DivUploadJawabanOpen').hide();
                        //         // $('#htmlInfoUploadJawaban').html('Tidak ada upload jawabans');
                        //         if (data.NilaiAngka == 0 || data.NilaiAngka == null) {
                        //             if (data.NilaiHuruf == null) {
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').show();
                        //             } else {
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').show();
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //             }
                        //         } else {
                        //             $('#DivUploadJawabanClose').fadeIn();
                        //             $('#DivUploadJawabanOpen').hide();
                        //             $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //         }
                        //         /*end*/
                        //     }
                        // } else if (data.IdJenisSoal == 2) {
                        //     if (data.SoalRemedial.IdJenisJawaban == 1) {
                        //         $('#jawaban_rubrik').hide();
                        //         $('#jawaban_youtube').hide();
                        //         $('#jawaban_upload').show();
                        //         $('#DivUploadJawabanClose').hide();
                        //         $('#DivUploadJawabanOpen').fadeIn();
                        //         $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //     } else if (data.SoalRemedial.IdJenisJawaban == 2) {
                        //         $('#jawaban_rubrik').hide();
                        //         $('#jawaban_upload').hide();
                        //         $('#jawaban_youtube').show();
                        //         $('#DivUploadJawabanClose').hide();
                        //         $('#DivUploadJawabanOpen').fadeIn();
                        //         $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //     } else if (data.SoalRemedial.IdJenisJawaban == 3) {
                        //         $('#jawaban_upload').hide();
                        //         $('#jawaban_youtube').hide();
                        //         $('#jawaban_rubrik').show();
                        //         $('#DivUploadJawabanClose').hide();
                        //         $('#DivUploadJawabanOpen').fadeIn();
                        //         $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //     } else if (data.SoalRemedial.IdJenisJawaban == 0) {
                        //         // $('#jawaban_upload').hide();
                        //         // $('#jawaban_youtube').hide();
                        //         // $('#jawaban_rubrik').hide();
                        //         // $('#DivUploadJawabanClose').fadeIn();
                        //         // $('#DivUploadJawabanOpen').hide();
                        //         // $('#htmlInfoUploadJawaban').html('Tidak ada upload jawabans');
                        //         if (data.NilaiAngka == 0 || data.NilaiAngka == null) {
                        //             if (data.NilaiHuruf == null) {
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').show();
                        //             } else {
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').show();
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //             }
                        //         } else {
                        //             $('#DivUploadJawabanClose').fadeIn();
                        //             $('#DivUploadJawabanOpen').hide();
                        //             $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //         }
                        //         /*end*/
                        //     }
                        // } else {
                        //     if (data.SoalRemedial.IdJenisJawaban == 1) {
                        //         $('#jawaban_rubrik').hide();
                        //         $('#jawaban_youtube').hide();
                        //         $('#jawaban_upload').show();
                        //         $('#DivUploadJawabanClose').hide();
                        //         $('#DivUploadJawabanOpen').fadeIn();
                        //         $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //     } else if (data.SoalRemedial.IdJenisJawaban == 2) {
                        //         $('#jawaban_rubrik').hide();
                        //         $('#jawaban_upload').hide();
                        //         $('#jawaban_youtube').show();
                        //         $('#DivUploadJawabanClose').hide();
                        //         $('#DivUploadJawabanOpen').fadeIn();
                        //         $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //     } else if (data.SoalRemedial.IdJenisJawaban == 3) {
                        //         $('#jawaban_youtube').hide();
                        //         $('#jawaban_upload').hide();
                        //         $('#jawaban_rubrik').show();
                        //         $('#DivUploadJawabanClose').hide();
                        //         $('#DivUploadJawabanOpen').fadeIn();
                        //         $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //     } else if (data.SoalRemedial.IdJenisJawaban == 0) {
                        //         // $('#jawaban_upload').hide();
                        //         // $('#jawaban_youtube').hide();
                        //         // $('#jawaban_rubrik').hide();
                        //         // $('#DivUploadJawabanClose').fadeIn();
                        //         // $('#DivUploadJawabanOpen').hide();
                        //         // $('#htmlInfoUploadJawaban').html('Tidak ada upload jawabans');
                        //         if (data.NilaiAngka == 0 || data.NilaiAngka == null) {
                        //             if (data.NilaiHuruf == null) {
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').show();
                        //             } else {
                        //                 $('#jawaban_upload').hide();
                        //                 $('#jawaban_youtube').hide();
                        //                 $('#jawaban_rubrik').show();
                        //                 $('#DivUploadJawabanClose').hide();
                        //                 $('#DivUploadJawabanOpen').fadeIn();
                        //             }
                        //         } else {
                        //             $('#DivUploadJawabanClose').fadeIn();
                        //             $('#DivUploadJawabanOpen').hide();
                        //             $('#htmlInfoUploadJawaban').html('Kamu sudah upload jawaban pada materi ini');
                        //         }
                        //         /*end*/
                        //     }
                        // }


                    } else {
                        htmlDownloadSoal = '<button class="btn btn-danger" type="button"><i class="fas fa-times"></i> Tidak Ada Soal Remedial</button>';
                    }
                }



                //// new 

                if (data.IsRemedial == 0) {
                    if (data.SoalReguler.IdJenisJawaban === 1) {
                        $('#DivUploadJawabanClose').hide();
                        $('#DivUploadJawabanOpen').fadeIn();
                        $('#jawaban_youtube').hide();
                        $('#jawaban_rubrik').hide();
                        $('#jawaban_upload').show();
                    }
                    if (data.SoalReguler.IdJenisJawaban === 2) {
                        $('#DivUploadJawabanClose').hide();
                        $('#DivUploadJawabanOpen').fadeIn();
                        $('#jawaban_youtube').show();
                        $('#jawaban_rubrik').hide();
                        $('#jawaban_upload').hide();
                    }
                    if (data.SoalReguler.IdJenisJawaban === 3) {
                        $('#DivUploadJawabanClose').hide();
                        $('#DivUploadJawabanOpen').fadeIn();
                        $('#jawaban_youtube').hide();
                        $('#jawaban_rubrik').show();
                        $('#jawaban_upload').hide();
                    }
                }

                if (data.IsRemedial == 1) {
                    if (data.SoalRemedial.IdJenisJawaban === 1) {
                        $('#DivUploadJawabanClose').hide();
                        $('#DivUploadJawabanOpen').fadeIn();
                        $('#jawaban_youtube').hide();
                        $('#jawaban_rubrik').hide();
                        $('#jawaban_upload').show();
                    }
                    if (data.SoalRemedial.IdJenisJawaban === 2) {
                        $('#DivUploadJawabanClose').hide();
                        $('#DivUploadJawabanOpen').fadeIn();
                        $('#jawaban_youtube').show();
                        $('#jawaban_rubrik').hide();
                        $('#jawaban_upload').hide();
                    }
                    if (data.SoalRemedial.IdJenisJawaban === 3) {
                        $('#DivUploadJawabanClose').hide();
                        $('#DivUploadJawabanOpen').fadeIn();
                        $('#jawaban_youtube').hide();
                        $('#jawaban_rubrik').show();
                        $('#jawaban_upload').hide();
                    }
                }


                
                $('#htmlDownloadSoal').html(htmlDownloadSoal);


                if (data.NamaUrlMeeting != null) {
                    $('#meeting_ada').show();
                    $('#meeting_empty').hide();
                    var urlEmbedYt = getEmbedYt('"' + data.NamaUrlMeeting + '"');
                    $('#meeting_url').html('<iframe class="img-responsive" src=' + urlEmbedYt + ' frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
                } else {
                    $('#meeting_empty').show();
                    $('#meeting_ada').hide();
                }

                $('#TabelSiswaProgress').DataTable({
                    "paging": true,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "pageLength": 5,
                    "lengthChange": false,
                    "scrollX": true,
                    "data": data.TugasSiswas,
                    "columns": [{
                            "render": function (data, type, full, meta) {
                                data = meta.row + 1;
                                return data;
                            }
                        },
                        {
                            "data": "NamaSiswa"
                        },
                        {
                            "render": function (data, type, full, meta) {
                                if (full.TugasStatus != "Belum Mengerjakan") {
                                    data = "<span class='badge badge-success'><i class='fas fa-check'></i> Sudah Mengerjakan</span>";
                                } else {
                                    data = "<span class='badge badge-warning'><i class='fa fa-times'></i> Belum Mengerjakan</span>";
                                }
                                return data;
                            }
                        },
                    ],
                    "bDestroy": true
                });

                $('.dataTables_scrollHeadInner, .table-bordered').css('width', 'inherit');

                //if (data.IsRemedial == 1) { //remedial
                //    $('#DivUploadJawabanClose').hide();
                //    $('#DivUploadJawabanOpen').fadeIn();
                //} else { //tidak remedial
                //if (data.NilaiAngka == 0 || data.NilaiAngka == null) {
                //    if (data.NilaiHuruf == null) {
                //        $('#DivUploadJawabanClose').hide();
                //        $('#DivUploadJawabanOpen').fadeIn();
                //    } else {
                //        $('#DivUploadJawabanClose').fadeIn();
                //        $('#DivUploadJawabanOpen').hide();
                //    }
                //} else {
                //    $('#DivUploadJawabanClose').fadeIn();
                //    $('#DivUploadJawabanOpen').hide();
                //}
                //}

                console.log(res)
            } else if (!res.IsSuccess) {
                alert(res.ReturnMessage)
            }

        },
        error: (xhr, err, status) => {
            console.log(xhr)
        }
    })
}

function GetJenisBantuan() {
    $.ajax({
        url: UrlService.GetJenisBantuan,
        type: 'GET',
        dataType: 'JSON',
        success: (res) => {
            var data = res.Data;
            console.log(data)
            var htmlJenisBantuan = '';
            $.each(data, (i, v) => {
                htmlJenisBantuan += '<option value="' + v.Id + '">' + v.Nama + '</option>';
            })
            $('#IdJenisBantuanOrtu').html(htmlJenisBantuan);
        }
    })
}

function MateriGambar(Param) {
    $('#ModalMateriGambar').modal('show');
    $('#htmlMateriGambar').html('<center><img src="' + base_url + '/Asset/Files/Elearning/' + Param + '" class="img-fluid" /></center>');
}

function Kembali() {
    //history.back() 
    $('#TabelRuangBelajar').DataTable().ajax.reload();
    $('.dataTables_scrollHeadInner, .table-bordered').css('width', 'inherit');

    $('#secSoal').hide();
    $('#secMapel').fadeIn();
}

function ModalCatatan(CatatanOriginal, CatatanSusulan) {
    $('#ModalCatatan').modal('show');
    var htmlCatatanOriginal = '';
    if (CatatanOriginal != 'null') {
        htmlCatatanOriginal = CatatanOriginal;
    } else {
        htmlCatatanOriginal = '';
    }

    var htmlCatatanSusulan = '';
    if (CatatanSusulan != 'null') {
        htmlCatatanSusulan = '<hr/><font color="red"><b>Catatan Susulan:</b></font> ' + CatatanSusulan;
    } else {
        htmlCatatanSusulan = '';
    }
    $('#htmlCatatan').html(htmlCatatanOriginal + htmlCatatanSusulan);
}

function SerahkanTugas() {
    var formObj = $('#FormRuangBelajar').serializeObject();
    var fd = new FormData($('#FormRuangBelajar')[0]);
    // fd.append('FileUpload', $('input[type="file"][name="FileUpload"]')[0].files[0]);
    var Bantuan = '';
    if (formObj.IdJenisBantuanOrtu == undefined) {
        Bantuan = 0;
    } else {
        Bantuan = formObj.IdJenisBantuanOrtu;
    }

    var IdJenisSoalBaru = formObj.IdJenisSoal;
    if (formObj.IdJenisSoal == 0)
        IdJenisSoalBaru = 1;

    $.ajax({
        url: UrlService.UploadJawaban + '?IdKbmMateri=' + formObj.IdKbmMateri + '&IdJenisSoal=' + IdJenisSoalBaru + '&IdJenisBantuanOrtu=' + Bantuan + '&NamaUrl=' + formObj.NamaUrl,
        method: 'POST',
        processData: false,
        contentType: false,
        data: fd,
        dataType: 'JSON',
        beforeSend: function () {
            ProgressBar("wait");
        },
        success: function (res) {
            ProgressBar("success");
            var data = res.Data;
            if (res.IsSuccess) {
                swal({
                    title: 'Berhasil',
                    text: 'Anda berhasil mengirim jawaban kepada guru',
                    confirmButtonClass: 'btn-success text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'success'
                });
                $('#TabelRuangBelajar').DataTable().ajax.reload();
                $('#ModalFormBuatSoal').modal('hide');
            } else if (!res.IsSuccess) {
                swal({
                    title: 'Gagal',
                    text: res.ReturnMessage,
                    confirmButtonClass: 'btn-danger text-white',
                    confirmButtonText: 'Oke, Mengerti',
                    type: 'error'
                });
            }
        },
        error: function (responserror, a, e) {
            ProgressBar("success");
            swal({
                title: 'Terjadi Kesalahan',
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: 'btn-danger text-white',
                confirmButtonText: 'Oke, Mengerti',
                type: 'error'
            });
        }
    });
}