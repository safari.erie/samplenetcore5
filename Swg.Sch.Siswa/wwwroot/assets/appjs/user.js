﻿/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 14042020
================================================================================================================= */

//$("input[name='UsernameOrEmail']").val(sessionStorage.getItem("AuthUsername"));
//$("#SayHello").html("Hi " + sessionStorage.getItem("AuthFirstName"));

function ResetPassword() {
  $("#ModalFormResetPassword").modal("show");
}

function GantiPassword() {
  $("#ModalFormGantiPassword").modal("show");
}

function UpdateProfile() {
  $("#ModalFormUpdateProfile").modal("show");
  $("#FormUpdateProfile")[0].reset();
  $("#BtnSaveUpdateProfile").attr("onClick", "SaveUpdateProfile();");

  var Url = base_url + "/Users/GetUserUpdateProfile";
  $.ajax({
    url: Url,
    type: "GET",
    dataType: "json",
    beforeSend: function (beforesend) {
      ProgressBar("wait");
    },
    success: function (responsesuccess) {
      ProgressBar("success");
      if (responsesuccess.IsSuccess == true) {
        var ResponseData = responsesuccess.Data;
        var FullName = (
          ResponseData.FirstName +
          " " +
          ResponseData.MiddleName +
          " " +
          ResponseData.LastName
        )
          .toLowerCase()
          .replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
          });
        $("#TitleFormUpdateProfile").html("Edit Profil - " + FullName);
        $("input[name='Email']", "#FormUpdateProfile").val(ResponseData.Email);
        $("input[name='FirstName']", "#FormUpdateProfile").val(
          ResponseData.FirstName
        );
        $("input[name='MiddleName']", "#FormUpdateProfile").val(
          ResponseData.MiddleName
        );
        $("input[name='LastName']", "#FormUpdateProfile").val(
          ResponseData.LastName
        );
        $("textarea[name='Address']", "#FormUpdateProfile").val(
          ResponseData.Address
        );
        $("input[name='PhoneNumber']", "#FormUpdateProfile").val(
          ResponseData.PhoneNumber
        );
        $("input[name='MobileNumber']", "#FormUpdateProfile").val(
          ResponseData.MobileNumber
        );
        $("input[name='NoLisensi']", "#FormUpdateProfile").val(
          ResponseData.NoLisensi
        );
        $("#ThumbnailImage").attr(
          "src",
          base_url + "/Assets/User/FotoProfil/" + ResponseData.FileImage
        );
      } else {
        swal({
          title: "Gagal ",
          text: responsesuccess.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (err, a, e) {
      ProgressBar("success");
      swal({
        title: "Error ",
        text: JSON.stringify(err) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function SaveUpdateProfile() {
  var form = new FormData($("#FormUpdateProfile")[0]);
  var Url = base_url + "/Users/UpdateProfile";
  $.ajax({
    url: Url,
    method: "POST",
    dataType: "json",
    data: form,
    contentType: false,
    cache: true,
    processData: false,
    beforeSend: function (before) {
      ProgressBar("wait");
    },
    success: function (responsesave) {
      ProgressBar("success");
      if (responsesave.IsSuccess == true) {
        $("#ModalFormUpdateProfile").modal("hide");
        swal({
          title: "Berhasil Edit Profil",
          text: "",
          confirmButtonClass: "btn-success text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "success",
        });
      } else if (responsesave.IsSuccess == false) {
        swal({
          title: "Gagal Edit Profil",
          text: responsesave.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (errorresponse) {
      ProgressBar("success");
      swal({
        title: "Gagal Edit Profil",
        text: errorresponse,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function Logout() {
  swal(
    {
      title: "Apakah Anda Yakin ?",
      text: "Anda akan keluar dari sesi login akun anda",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Ya, Saya Yakin!",
      cancelButtonClass: "btn-success",
      cancelButtonText: "Tidak, Batalkan!",
      closeOnConfirm: false,
    },
    function () {
      var Url = base_url + "/Users/Logout";
      $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        headers: {
          "Content-Type": "application/json",
        },
        beforeSend: function (before) {
          ProgressBar("wait");
        },
        success: function (responsesuccess) {
          ProgressBar("success");
          if (responsesuccess.IsSuccess == true) {
            window.open(base_url + "/User/Login", "_self");
            iziToast.success({
              title: "Berhasil Logout",
              message: "Kamu Berhasil Keluar Dari Akun",
              position: "topRight",
            });
          } else if (responsesuccess.IsSuccess == false) {
            iziToast.error({
              title: "Gagal Logout",
              message: "Kamu Gagal Keluar Dari Akun",
              position: "topRight",
            });
          }
        },
        error: function (err, a, e) {
          ProgressBar("success");
          swal({
            title: "Error ",
            text: JSON.stringify(err) + " : " + e,
            confirmButtonClass: "btn-danger text-white",
            confirmButtonText: "Oke, Mengerti",
            type: "error",
          });
        },
      });
    }
  );
}

// function SaveResetPassword() {
//     var formObj = $('#FormResetPassword').serializeObject();
//     var Url = base_url + "/Auth/SiswaResetPassword?Nis=" + formObj.Nis;
//     $.ajax({
//         url: Url,
//         method: "POST",
//         dataType: "json",
//         beforeSend: function (before) {
//             ProgressBar("wait");
//         },
//         success: function (responsesave) {
//             ProgressBar("success");
//             if (responsesave.IsSuccess == true) {
//                 $("#ModalFormResetPassword").modal("hide");
//                 var res = '';
//                 if (responsesave.Data != null) {
//                     var itung = responsesave.Data.length;
//                     var pecah = responsesave.Data.split('@');
//                     var str = pecah[0];
//                     var domain = '@' + pecah[1];
//                     res = str.slice(0, 3);
//                     for (var i = 0; i < itung - 3 - domain.length; i++) {
//                         res += '*';
//                     }
//                     res += domain;
//                 }
//                 swal({
//                     title: 'Berhasil Reset Password',
//                     text: res,
//                     confirmButtonClass: 'btn-success text-white',
//                     confirmButtonText: 'Oke, Mengerti',
//                     type: 'success'
//                 });
//             } else if (responsesave.IsSuccess == false) {
//                 swal({
//                     title: 'Gagal Reset Password',
//                     text: responsesave.ReturnMessage,
//                     confirmButtonClass: 'btn-danger text-white',
//                     confirmButtonText: 'Oke, Mengerti',
//                     type: 'error'
//                 });
//             }
//         },
//         error: function (err, a, e) {
//             ProgressBar("success");
//             swal({
//                 title: 'Error ',
//                 text: JSON.stringify(err) + " : " + e,
//                 confirmButtonClass: 'btn-danger text-white',
//                 confirmButtonText: 'Oke, Mengerti',
//                 type: 'error'
//             });
//         }
//     });
// }

function SaveResetPassword() {
  var formObj = $("#FormResetPassword").serializeObject();
  swal(
    {
      title: "Apakah Anda Yakin ?",
      text: "Anda akan mendapatkan password baru ke email anda",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-success",
      confirmButtonText: "Ya, Reset Password!",
      cancelButtonClass: "btn-danger",
      cancelButtonText: "Tidak, Batalkan!",
      closeOnConfirm: true,
    },
    function () {
      var Url = base_url + "/Auth/SiswaResetPassword?Nis=" + formObj.Nis;
      $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        beforeSend: function (before) {
          ProgressBar("wait");
        },
        success: function (responsesave) {
          ProgressBar("success");
          if (responsesave.IsSuccess == true) {
            $("#ModalFormResetPassword").modal("hide");
            var res = "";
            if (responsesave.Data != null) {
              var itung = responsesave.Data.length;
              var pecah = responsesave.Data.split("@");
              var str = pecah[0];
              var domain = "@" + pecah[1];
              res = str.slice(0, 3);
              for (var i = 0; i < itung - 3 - domain.length; i++) {
                res += "*";
              }
              res += domain;
            }
            swal({
              title: "Berhasil Reset Password",
              text: "Silahkan cek email " + res,
              confirmButtonClass: "btn-success text-white",
              confirmButtonText: "Oke, Mengerti",
              type: "success",
            });
          } else if (responsesave.IsSuccess == false) {
            swal({
              title: "Gagal Reset Password",
              text: responsesave.ReturnMessage,
              confirmButtonClass: "btn-danger text-white",
              confirmButtonText: "Oke, Mengerti",
              type: "error",
            });
          }
        },
        error: function (err, a, e) {
          ProgressBar("success");
          swal({
            title: "Error ",
            text: JSON.stringify(err) + " : " + e,
            confirmButtonClass: "btn-danger text-white",
            confirmButtonText: "Oke, Mengerti",
            type: "error",
          });
        },
      });
    }
  );
}

function SaveGantiPassword() {
  var formObj = $("#FormChangePassword").serializeObject();
  var Url =
    base_url +
    "/Auth/SiswaChangePassword?Nis=" +
    formObj.Nis +
    "&OldPassword=" +
    formObj.OldPassword +
    "&NewPassword1=" +
    formObj.NewPassword1 +
    "&NewPassword2=" +
    formObj.NewPassword2;
  $.ajax({
    url: Url,
    method: "POST",
    dataType: "json",
    processData: false,
    contentType: false,
    headers: {
      "Content-Type": "application/json",
    },
    beforeSend: function (before) {
      ProgressBar("wait");
    },
    success: function (responsesave) {
      ProgressBar("success");
      if (responsesave.IsSuccess == true) {
        $("#ModalFormGantiPassword").modal("hide");
        swal({
          title: "Berhasil Merubah Password",
          text: "",
          confirmButtonClass: "btn-success text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "success",
        });
      } else if (responsesave.IsSuccess == false) {
        swal({
          title: "Gagal Merubah Password",
          text: responsesave.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (errorresponse) {
      swal({
        title: "Gagal Merubah Password",
        text: errorresponse,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function ShowPassword() {
  $("input[type='password']").attr("type", "text");
  $("button[onClick='ShowPassword();']")
    .attr("onClick", "HidePassword();")
    .html('<i class="fa fa-eye-slash fa-2x"></i>');
}

function HidePassword() {
  $("input[type='text'][name='Password']").attr("type", "password");
  $("button[onClick='HidePassword();']")
    .attr("onClick", "ShowPassword();")
    .html('<i class="fa fa-eye fa-2x"></i>');
}

$("#BtnValidasiPinOrtu").attr("onClick", "ValidasiPinOrtu()");

function GantiPinOrtu() {
  $("#ModalPinOrtu").modal("hide");
  $("section").css("display", "none");
  $("#ModalGantiPinOrtu")
    .modal({
      backdrop: "static",
      keyboard: false,
    })
    .css("top", "15%");
}

GetSiswa();

function GetSiswa() {
  var Url = base_url + "/api/Siswas/GetSiswa";
  $.ajax({
    url: Url,
    type: "GET",
    dataType: "json",
    beforeSend: function (beforesend) {
      ProgressBar("wait");
    },
    success: function (responsesuccess) {
      ProgressBar("success");
      if (responsesuccess.IsSuccess == true) {
        var ResponseData = responsesuccess.Data;
        $("input[name='Email']", "#FormEditProfilSiswa").val(
          ResponseData.Email
        );
        $("input[name='EmailOrtu']", "#FormEditProfilSiswa").val(
          ResponseData.EmailOrtu
        );
        $("input[name='NoHandphone']", "#FormEditProfilSiswa").val(
          ResponseData.NoHandphone
        );
        $("#TbNoHandphone").html(": &nbsp;&nbsp;" + ResponseData.NoHandphone);
        $("#TbEmail").html(": &nbsp;&nbsp;" + ResponseData.Email);
        $("#TbEmailOrtu").html(": &nbsp;&nbsp;" + ResponseData.EmailOrtu);
        $("#TbTotTghSebelumnya").html(
          ": &nbsp;&nbsp; Rp. " + formatRupiah(ResponseData.TotalTagihan1)
        );
        $("#TbTotTghBerjalan").html(
          ": &nbsp;&nbsp; Rp. " + formatRupiah(ResponseData.TotalTagihan2)
        );
        $("#TbTotTgh").html(
          ": &nbsp;&nbsp; Rp. " + formatRupiah(ResponseData.TotalTagihan)
        );
      } else {
        swal({
          title: "Gagal ",
          text: responsesuccess.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (err, a, e) {
      ProgressBar("success");
    },
  });
}

function NextAutentikasiPinOrtu(Status = null, Proses = null) {
  if (Status == "true") {
    $("section").removeAttr("style");
    $("#ModalPinOrtu").modal("hide");
    if (Proses == "rapor") {
      $("#ModalTagihan").modal("show");
    } else if (Proses != null) {
      GetSiswaProfile();
      ProgressBar("wait");
      setTimeout(() => {
        ProgressBar("success");
        $("#ModalEditProfilSiswa").modal("show");
      }, 1000);
      $("#BtnTutupModalEditProfil").attr("data-dismiss", "modal");

      // $("#HeaderEditProfilSiswa").text("Perbarui Profil");
    }
  } else if (Status == "false") {
    $("section").css("display", "none");
    $("#ModalPinOrtu")
      .modal({
        backdrop: "static",
        keyboard: false,
      })
      .css("top", "25%");
  }
}

function ValidasiPinOrtu(Proses = null) {
  var formObj = $("#FormAutentikasiPinOrtu").serializeObject();
  var Url = base_url + "/api/Siswas/ValidasiPin?Pin=" + formObj.PinOrtu;
  $.ajax({
    url: Url,
    type: "GET",
    dataType: "json",
    beforeSend: function (beforesend) {
      ProgressBar("wait");
    },
    success: function (responsesuccess) {
      ProgressBar("success");
      if (responsesuccess.IsSuccess == true) {
        NextAutentikasiPinOrtu("true", Proses);
      } else {
        $("#DivInputPinOrtu").addClass("animate__animated animate__shakeX");
        $("input[name='PinOrtu']", "#FormAutentikasiPinOrtu").val(null);
        NextAutentikasiPinOrtu("false", Proses);
        iziToast.error({
          title: "Gagal Autentikasi PIN ",
          message: responsesuccess.ReturnMessage,
          position: "topRight",
          timeout: 3000,
        });
      }
    },
    error: function (err, a, e) {
      ProgressBar("success");
      swal({
        title: "Error ",
        text: JSON.stringify(err) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function SaveProfilSiswa() {
  var formObj = $("#FormEditProfilSiswa").serializeObject();
  var fd = new FormData($("#FormEditProfilSiswa")[0]);
  fd.append(
    "FotoProfile",
    $('input[type="file"][name="FotoProfile"]')[0].files[0]
  );
  var Url =
    base_url +
    "/api/Siswas/EditProfilSiswa?Email=" +
    formObj.Email +
    "&EmailOrtu=" +
    formObj.EmailOrtu +
    "&NoHandphone=" +
    formObj.NoHandphone;
  $.ajax({
    url: Url,
    method: "POST",
    contentType: false,
    cache: true,
    processData: false,
    timeout: 60000,
    data: fd,
    mimeType: "multipart/form-data",
    dataType: "JSON",
    beforeSend: function (beforesend) {
      ProgressBar("wait");
    },
    success: function (responsesuccess) {
      ProgressBar("success");
      if (responsesuccess.IsSuccess == true) {
        $("#ModalEditProfilSiswa").modal("hide");
        GetSiswa();
        NextAutentikasiPinOrtu("true");
        iziToast.success({
          title: "Berhasil Edit Profil Siswa",
          message: responsesuccess.ReturnMessage,
          position: "topRight",
          timeout: 3000,
        });
      } else {
        iziToast.error({
          title: "Gagal Edit Profil Siswa ",
          message: responsesuccess.ReturnMessage,
          position: "topRight",
          timeout: 3000,
        });
      }
    },
    error: function (err, a, e) {
      ProgressBar("success");
      swal({
        title: "Error ",
        text: JSON.stringify(err) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function SaveGantiPinOrtu() {
  var formObj = $("#FormGantiPinOrtu").serializeObject();
  var Url =
    base_url +
    "/api/Siswas/GantiPin?PinLama=" +
    formObj.PinLama +
    "&PinBaru=" +
    formObj.PinBaru;
  $.ajax({
    url: Url,
    type: "POST",
    dataType: "json",
    beforeSend: function (beforesend) {
      ProgressBar("wait");
    },
    success: function (responsesuccess) {
      ProgressBar("success");
      if (responsesuccess.IsSuccess == true) {
        $("#ModalGantiPinOrtu").modal("hide");
        NextAutentikasiPinOrtu("true");
        iziToast.success({
          title: "Berhasil Mengganti PIN Orang Tua ",
          message: responsesuccess.ReturnMessage,
          position: "topRight",
          timeout: 3000,
        });
      } else {
        iziToast.error({
          title: "Gagal Mengganti PIN Orang Tua ",
          message: responsesuccess.ReturnMessage,
          position: "topRight",
          timeout: 3000,
        });
      }
    },
    error: function (err, a, e) {
      ProgressBar("success");
      swal({
        title: "Error ",
        text: JSON.stringify(err) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

//function SaveGantiEmail() {
//    var formObj = $('#FormGantiEmail').serializeObject();
//    var Url = base_url + "/api/Siswas/ValidasiPin?Pin=" + formObj.PinOrtu;
//    $.ajax({
//        url: Url,
//        type: "POST",
//        dataType: "json",
//        beforeSend: function (beforesend) {
//            ProgressBar("wait");
//        },
//        success: function (responsesuccess) {
//            ProgressBar("success");
//            if (responsesuccess.IsSuccess == true) {
//                NextAutentikasiPinOrtu("true", Proses);
//            } else {
//                NextAutentikasiPinOrtu("false", Proses);
//                iziToast.error({
//                    title: "Gagal Autentikasi PIN ",
//                    message: responsesuccess.ReturnMessage,
//                    position: 'topRight',
//                    timeout: 3000
//                });
//            }
//        }, error: function (err, a, e) {
//            ProgressBar("success");
//            swal({ title: 'Error ', text: JSON.stringify(err) + " : " + e, confirmButtonClass: 'btn-danger text-white', confirmButtonText: 'Oke, Mengerti', type: 'error' });
//        }
//    });
//}

function SaveResetPinOrtu() {
  swal(
    {
      title: "Apakah Anda Yakin ?",
      text: "Anda akan me-reset PIN Orang Tua!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-success",
      confirmButtonText: "Ya, Saya Yakin!",
      cancelButtonClass: "btn-danger",
      cancelButtonText: "Tidak, Batalkan!",
      closeOnConfirm: true,
    },
    function () {
      var formObj = $("#FormLupaPinOrtu").serializeObject();
      var Url = base_url + "/api/Siswas/ResetPin";
      $.ajax({
        url: Url,
        type: "POST",
        dataType: "json",
        beforeSend: function (beforesend) {
          ProgressBar("wait");
        },
        success: function (responsesuccess) {
          ProgressBar("success");
          if (responsesuccess.IsSuccess == true) {
            NextAutentikasiPinOrtu("false");
            iziToast.success({
              title: "Berhasil Me-Reset PIN Orang Tua ",
              message:
                "PIN sudah direset silahkan buka email orang tua yang terdaftar di Sekolah AT Taufiq",
              position: "topRight",
              timeout: 3000,
            });
          } else {
            NextAutentikasiPinOrtu("false");
            iziToast.error({
              title: "Gagal Me-Reset PIN Orang Tua ",
              message: responsesuccess.ReturnMessage,
              position: "topRight",
              timeout: 3000,
            });
          }
        },
        error: function (err, a, e) {
          ProgressBar("success");
          swal({
            title: "Error ",
            text: JSON.stringify(err) + " : " + e,
            confirmButtonClass: "btn-danger text-white",
            confirmButtonText: "Oke, Mengerti",
            type: "error",
          });
        },
      });
    }
  );
}

function AutentikasiPinOrtu(Proses = null) {
  $("#PinOrtu").val("");
  $("section").css("display", "none");
  $("#ModalPinOrtu")
    .modal({
      backdrop: "static",
      keyboard: false,
    })
    .css("top", "25%");

  $("input[name='PinOrtu']").keyup(function () {
    $("#DivInputPinOrtu").removeClass("animate__animated animate__shakeX");
    var ValInput = this.value;
    if (ValInput.length == 6) {
      ValidasiPinOrtu(Proses);
    }
  });
  $("input[name='PinOrtu']").on("cut copy paste", function (e) {
    iziToast.error({
      title: "Terjadi Kesalahan",
      message:
        "Tidak diperbolehkan melakukan COPY PASTE pada kolom isian PIN Orang Tua",
      position: "topRight",
      timeout: 3000,
    });
    e.preventDefault();
  });
}

function ShowPinOrtu() {
  $("input[type='password']").attr("type", "text");
  $("button[onClick='ShowPinOrtu();']")
    .attr("onClick", "HidePinOrtu();")
    .html('<i class="fa fa-eye-slash fa-2x"></i>');
}

function HidePinOrtu() {
  $("input[type='text']").attr("type", "password");
  $("button[onClick='HidePinOrtu();']")
    .attr("onClick", "ShowPinOrtu();")
    .html('<i class="fa fa-eye-slash fa-2x"></i>');
}

function ProsesLogin() {
  var formObj = $("#FormLogin").serializeObject();
  var Nis = formObj.Nis;
  var Password = formObj.Password;
  var Url = base_url + "/Auth/SiswaLogin?Nis=" + Nis + "&Password=" + Password;
  $.ajax({
    url: Url,
    type: "POST",
    dataType: "json",
    beforeSend: function (before) {
      $("button[name='BtnLogin']")
        .text("Sedang memuat, mohon tunggu...")
        .attr("disabled", true);
      ProgressBar("wait");
    },
    success: function (res) {
      ProgressBar("success");
      if (res.IsSuccess == true) {
        iziToast.success({
          title: "Selamat Datang " + res.Data.Nama,
          message: "Anda akan diarahkan kehalaman utama",
          position: "topRight",
          timeout: 3000,
        });
        setTimeout(function () {
          location.href = base_url + "/siswa/index";
          $("#prosesloading").hide();
          $("#btnloading").show();
        }, 3000);
        localStorage.setItem(
          "TagihanSiswaBulanSebelumnya",
          JSON.stringify(res.Data.Tagihans1)
        );
        localStorage.setItem(
          "TagihanSiswaBulanBerjalan",
          JSON.stringify(res.Data.Tagihans2)
        );
        localStorage.setItem(
          "TotalTagihan",
          JSON.stringify(res.Data.TotalTagihan)
        );
        localStorage.setItem(
          "TotalTagihan1",
          JSON.stringify(res.Data.TotalTagihan1)
        );
        localStorage.setItem(
          "TotalTagihan2",
          JSON.stringify(res.Data.TotalTagihan2)
        );
        localStorage.setItem("DataLogin", JSON.stringify(res.Data));
      } else if (res.IsSuccess == false) {
        $("button[name='BtnLogin']")
          .html('<i class="fa fa-sign-in-alt"></i> Login')
          .removeAttr("disabled");
        if (
          res.ReturnMessage ==
          "password sudah satu bulan tidak diganti, silahkan ganti password anda"
        ) {
          swal(
            {
              title: "Silahkan Ganti Password",
              text: "Demi keamanan akun milik anda, kami menyarankan anda untuk mengganti password sebulan sekali",
              type: "warning",
              showCancelButton: false,
              confirmButtonClass: "btn-warning text-white",
              confirmButtonText: "Oke, Mengerti",
              closeOnConfirm: false,
            },
            function () {
              location.href = base_url + "/Home/GantiPassword";
            }
          );
        } else {
          iziToast.error({
            title: "Login Gagal",
            message: res.ReturnMessage,
            position: "topRight",
            timeout: 3000,
          });
        }
      }
    },
    error: function (err, a, e) {
      ProgressBar("success");
      swal({
        title: "Error ",
        text: JSON.stringify(err) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function formatRupiah(angka) {
  var TipeData = jQuery.type(angka);
  if (TipeData == "number") {
    var bilangan = angka;
    var reverse = bilangan.toString().split("").reverse().join(""),
      ribuan = reverse.match(/\d{1,3}/g);
    ribuan = ribuan.join(".").split("").reverse().join("");
    return ribuan;
  }
  return "Tidak Ada";
}

$(".toggle-password").click(function () {
  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
