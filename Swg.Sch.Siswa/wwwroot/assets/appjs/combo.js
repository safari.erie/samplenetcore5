﻿/*===============================================================================================================
CreatedBy    : Fatih Syauqi
CreatedDate  : 14042020
================================================================================================================= */

function ComboGetRoles(handleData, IdRoles) {
  var Url = base_url + "/Users/GetRoles";
  $.ajax({
    url: Url,
    type: "GET",
    dataType: "json",
    headers: {
      "Content-Type": "application/json",
    },
    beforeSend: function (beforesend) {
      ProgressBar("wait");
    },
    success: function (responsesuccess) {
      ProgressBar("success");
      if (responsesuccess.IsSuccess == true) {
        var TipeData = jQuery.type(responsesuccess.Data);
        var HtmlCombo = "";
        HtmlCombo +=
          "<option value='' style='display:none;'>- Pilih Salah Satu -</option>";
        if (TipeData == "array") {
          var JmlJL = responsesuccess.Data.length;
          var Roles = IdRoles;
          for (var ijl = 0; ijl < JmlJL; ijl++) {
            if (responsesuccess.Data[ijl].IdRole != Roles[ijl]) {
              HtmlCombo +=
                "<option value='" +
                responsesuccess.Data[ijl].IdRole +
                "'>" +
                responsesuccess.Data[ijl].RoleName +
                "</option>";
            }
          }
        } else {
          HtmlCombo +=
            "<option value='" +
            responsesuccess.Data.IdRole +
            "' selected>" +
            responsesuccess.Data.RoleName +
            "</option>";
        }

        handleData(HtmlCombo);
        // umum nya seperti ini bentuk penamaan element combo nya : $("select[name='IdRole[]']").html(HtmlCombo);
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function ComboGetRole(handleData, IdRole) {
  var Url = base_url + "/Users/GetRole?IdRole=" + parseInt(IdRole);
  $.ajax({
    url: Url,
    type: "GET",
    dataType: "json",
    headers: {
      "Content-Type": "application/json",
    },
    beforeSend: function (beforesend) {
      ProgressBar("wait");
    },
    success: function (responsesuccess) {
      ProgressBar("success");
      if (responsesuccess.IsSuccess == true) {
        var TipeData = jQuery.type(responsesuccess.Data);
        var HtmlCombo = "";
        HtmlCombo +=
          "<option value='' style='display:none;'>- Pilih Salah Satu -</option>";
        if (TipeData == "array") {
          var JmlJL = responsesuccess.Data.length;
          for (var ijl = 0; ijl < JmlJL; ijl++) {
            if (responsesuccess.Data[ijl].IdRole == IdUser) {
              HtmlCombo +=
                "<option value='" +
                responsesuccess.Data[ijl].Id +
                "' selected>" +
                responsesuccess.Data[ijl].Nama +
                "</option>";
            } else {
              HtmlCombo +=
                "<option value='" +
                responsesuccess.Data[ijl].Id +
                "'>" +
                responsesuccess.Data[ijl].Nama +
                "</option>";
            }
          }
        } else {
          HtmlCombo +=
            "<option value='" +
            responsesuccess.Data.IdRole +
            "'>" +
            responsesuccess.Data.RoleName +
            "</option>";
        }

        handleData(HtmlCombo);
        // umum nya seperti ini bentuk penamaan element combo nya : $("select[name='IdRoleSelected[]']").html(HtmlCombo);
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function ComboGetApplTaskByRole(handleData, IdRole) {
  var Url =
    base_url + "/ApplTasks/GetApplTaskByRole?IdRole=" + parseInt(IdRole);
  $.ajax({
    url: Url,
    type: "GET",
    dataType: "json",
    headers: {
      "Content-Type": "application/json",
    },
    beforeSend: function (beforesend) {
      ProgressBar("wait");
    },
    success: function (responsesuccess) {
      ProgressBar("success");
      if (responsesuccess.IsSuccess == true) {
        var HtmlComboNoAssignTasks = "";
        var HtmlComboAssignTasks = "";
        var JmlNoAssignTasks = responsesuccess.Data.NoAssignTasks.length;
        var JmlAssignTasks = responsesuccess.Data.AssignTasks.length;

        for (var inat = 0; inat < JmlNoAssignTasks; inat++) {
          if (responsesuccess.Data.NoAssignTasks[inat].ControllerName != null) {
            HtmlComboNoAssignTasks +=
              "<option value='" +
              responsesuccess.Data.NoAssignTasks[inat].IdApplTask +
              "'>" +
              responsesuccess.Data.NoAssignTasks[inat].ApplTaskName +
              "</option>";
          }
        }

        for (var iat = 0; iat < JmlAssignTasks; iat++) {
          HtmlComboAssignTasks +=
            "<option value='" +
            responsesuccess.Data.AssignTasks[iat].IdApplTask +
            "' selected>" +
            responsesuccess.Data.AssignTasks[iat].ApplTaskName +
            "</option>";
        }

        handleData(HtmlComboNoAssignTasks, HtmlComboAssignTasks);
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function ComboGetAccessByRole(handleData, IdRole) {
  var Url =
    base_url +
    "/HakAksess/GetHakAksesByJabatan?IdJabatanProyek=" +
    parseInt(IdRole);
  $.ajax({
    url: Url,
    type: "GET",
    dataType: "json",
    headers: {
      "Content-Type": "application/json",
    },
    beforeSend: function (beforesend) {
      ProgressBar("wait");
    },
    success: function (responsesuccess) {
      ProgressBar("success");
      if (responsesuccess.IsSuccess == true) {
        var HtmlComboNoAssignAccesses = "";
        var HtmlComboAssignAccesses = "";
        var JmlNoAssignAccesses = responsesuccess.Data.NoHakAksess.length;
        var JmlAssignAccesses = responsesuccess.Data.HakAksess.length;

        for (var inat = 0; inat < JmlNoAssignAccesses; inat++) {
          HtmlComboNoAssignAccesses +=
            "<option value='" +
            responsesuccess.Data.NoHakAksess[inat].IdHakAkses +
            "'>" +
            responsesuccess.Data.NoHakAksess[inat].NamaHakAkses +
            "</option>";
        }
        for (var iat = 0; iat < JmlAssignAccesses; iat++) {
          HtmlComboAssignAccesses +=
            "<option value='" +
            responsesuccess.Data.HakAksess[iat].IdHakAkses +
            "' selected>" +
            responsesuccess.Data.HakAksess[iat].NamaHakAkses +
            "</option>";
        }

        handleData(HtmlComboNoAssignAccesses, HtmlComboAssignAccesses);
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function GetTahuns(handleData, Tipe) {
  var StartDate = new Date().getFullYear() - 4;
  var EndDate = new Date().getFullYear();
  var HtmlCombo = "";
  HtmlCombo += "<option value=''>- Pilih Salah Satu -</option>";
  for (var i = EndDate; i >= StartDate; i--) {
    if (Tipe == "NoSelect") {
      HtmlCombo += "<option value='" + i + "'>" + i + "</option>";
    } else {
      if (i == EndDate) {
        HtmlCombo += "<option value='" + i + "' selected>" + i + "</option>";
      } else {
        HtmlCombo += "<option value='" + i + "'>" + i + "</option>";
      }
    }
  }
  handleData(HtmlCombo);
}

function ComboGetParentMenu(handleData, IdApplTaskParent, IdAppl) {
  var Url = base_url + "/ApplTasks/GetApplTasks?IdAppl=" + IdAppl;
  $.ajax({
    url: Url,
    type: "GET",
    dataType: "json",
    beforeSend: function (beforesend) {
      ProgressBar("wait");
    },
    success: function (responsesuccess) {
      ProgressBar("success");
      if (responsesuccess.IsSuccess == true) {
        var HtmlCombo = "";
        var JmlData = responsesuccess.Data.length;
        HtmlCombo += "<option value=''>- Pilih Salah Satu -</option>";
        for (var idata = 0; idata < JmlData; idata++) {
          if (responsesuccess.Data[idata].IdApplTaskParent == null) {
            if (responsesuccess.Data[idata].IdApplTask == IdApplTaskParent) {
              HtmlCombo +=
                "<option value='" +
                responsesuccess.Data[idata].IdApplTask +
                "' selected>" +
                responsesuccess.Data[idata].ApplTaskName +
                "</option>";
            } else {
              HtmlCombo +=
                "<option value='" +
                responsesuccess.Data[idata].IdApplTask +
                "'>" +
                responsesuccess.Data[idata].ApplTaskName +
                "</option>";
            }
          }
        }

        handleData(HtmlCombo);
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function ComboGetAppl(handleData, IdAppl) {
  var Url = base_url + "/ApplTasks/GetAppls";
  $.ajax({
    url: Url,
    type: "GET",
    dataType: "json",
    beforeSend: function (beforesend) {
      ProgressBar("wait");
    },
    success: function (responsesuccess) {
      ProgressBar("success");
      if (responsesuccess.IsSuccess == true) {
        var HtmlCombo = "";
        var JmlData = responsesuccess.Data.length;
        HtmlCombo += "<option value=''>- Pilih Salah Satu -</option>";
        for (var idata = 0; idata < JmlData; idata++) {
          if (responsesuccess.Data[idata].IdAppl == IdAppl) {
            HtmlCombo +=
              "<option value='" +
              responsesuccess.Data[idata].IdAppl +
              "' selected>" +
              responsesuccess.Data[idata].ApplName +
              "</option>";
          } else {
            HtmlCombo +=
              "<option value='" +
              responsesuccess.Data[idata].IdAppl +
              "'>" +
              responsesuccess.Data[idata].ApplName +
              "</option>";
          }
        }

        handleData(HtmlCombo);
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function GetUsers(handleData) {
  var Url = base_url + "/Users/GetUsers";
  $.ajax({
    url: Url,
    method: "GET",
    beforeSend: function (before) {
      ProgressBar("wait");
    },
    success: function (responsesave) {
      ProgressBar("success");
      if (responsesave.IsSuccess == true) {
        var html_status = "";
        html_status = '<option value="">- Pilih Salah Satu -</option>';
        for (var i = 0; i < responsesave.Data.length; i++) {
          html_status +=
            '<option value="' +
            responsesave.Data[i].IdUser +
            '">' +
            SetFullName(
              responsesave.Data[i].FirstName,
              responsesave.Data[i].MiddleName,
              responsesave.Data[i].LastName
            ) +
            " - (" +
            responsesave.Data[i].Roles[0].RoleName +
            ")</option>";
        }
        handleData(html_status);
      } else if (responsesave.IsSuccess == false) {
        swal({
          title: "Gagal",
          text: responsesave.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function ComboGetJenisEvaluasiHarian(handleData) {
  var Url = base_url + "/api/Siswas/GetJenisEvaluasiHarian";
  $.ajax({
    url: Url,
    method: "GET",
    beforeSend: function (before) {
      ProgressBar("wait");
    },
    success: function (responsesave) {
      ProgressBar("success");
      if (responsesave.IsSuccess == true) {
        var html_status = "";
        html_status = '<option value="">- Pilih Salah Satu -</option>';
        for (var i = 0; i < responsesave.Data.length; i++) {
          html_status +=
            '<option value="' +
            responsesave.Data[i].Id +
            '">' +
            responsesave.Data[i].Nama +
            "</option>";
        }
        handleData(html_status);
      } else if (responsesave.IsSuccess == false) {
        swal({
          title: "Gagal",
          text: responsesave.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function ComboGetTahunAjaran(handleData, IdTahunAjaran, IsActive) {
  var Url = base_url + "/api/Siswas/GetTahunAjarans";
  $.ajax({
    url: Url,
    method: "GET",
    beforeSend: function (before) {
      ProgressBar("wait");
    },
    success: function (responsesave) {
      ProgressBar("success");
      if (responsesave.IsSuccess == true) {
        var html_combo = "";
        html_combo = '<option value="">- Pilih Salah Satu -</option>';
        for (var i = 0; i < responsesave.Data.length; i++) {
          if (IsActive) {
            if (responsesave.Data[i].Status == 1) {
              html_combo +=
                '<option value="' +
                responsesave.Data[i].IdTahunAjaran +
                '" selected>' +
                responsesave.Data[i].Nama +
                "</option>";
            }
          } else {
            if (responsesave.Data[i].IdTahunAjaran == IdTahunAjaran) {
              html_combo +=
                '<option value="' +
                responsesave.Data[i].IdTahunAjaran +
                '" selected>' +
                responsesave.Data[i].Nama +
                "</option>";
            } else {
              html_combo +=
                '<option value="' +
                responsesave.Data[i].IdTahunAjaran +
                '">' +
                responsesave.Data[i].Nama +
                "</option>";
            }
          }
        }
        handleData(html_combo);
      } else if (responsesave.IsSuccess == false) {
        swal({
          title: "Gagal",
          text: responsesave.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function ComboGetSemester(handleData, IdSemester) {
  var Url = base_url + "/api/Siswas/GetSemester";
  $.ajax({
    url: Url,
    method: "GET",
    beforeSend: function (before) {
      ProgressBar("wait");
    },
    success: function (responsesave) {
      ProgressBar("success");
      if (responsesave.IsSuccess == true) {
        var html_combo = "";
        html_combo = '<option value="">- Pilih Salah Satu -</option>';
        html_combo +=
          '<option value="' +
          responsesave.Data.IdSemester +
          '" selected>' +
          responsesave.Data.Semester +
          "</option>";
        // for (var i = 0; i < (responsesave.Data).length; i++) {
        //     if (responsesave.Data[i].IdSemester == IdSemester) {
        //         html_combo += '<option value="' + responsesave.Data[i].IdSemester + '" selected>' + responsesave.Data[i].Semester + '</option>';
        //     } else {
        //         html_combo += '<option value="' + responsesave.Data[i].IdSemester + '">' + responsesave.Data[i].Semester + '</option>';
        //     }
        // }
        handleData(html_combo);
      } else if (responsesave.IsSuccess == false) {
        swal({
          title: "Gagal",
          text: responsesave.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}

function ComboGetAgamas(handleData, Id) {
  var Url = base_url + "/api/Siswas/GetAgamas";
  $.ajax({
    url: Url,
    method: "GET",
    beforeSend: function (before) {
      ProgressBar("wait");
    },
    success: function (responsesave) {
      ProgressBar("success");
      if (responsesave.IsSuccess == true) {
        var html_combo = "";
        html_combo = '<option value="">- Pilih Salah Satu -</option>';
        for (var i = 0; i < responsesave.Data.length; i++) {
          if (responsesave.Data[i].Id == Id) {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].Id +
              '" selected>' +
              responsesave.Data[i].Nama +
              "</option>";
          } else {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].Id +
              '">' +
              responsesave.Data[i].Nama +
              "</option>";
          }
        }
        handleData(html_combo);
      } else if (responsesave.IsSuccess == false) {
        swal({
          title: "Gagal",
          text: responsesave.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}
function ComboGetJenisPekerjaans(handleData, Id) {
  var Url = base_url + "/api/Siswas/GetJenisPekerjaans";
  $.ajax({
    url: Url,
    method: "GET",
    beforeSend: function (before) {
      ProgressBar("wait");
    },
    success: function (responsesave) {
      ProgressBar("success");
      if (responsesave.IsSuccess == true) {
        var html_combo = "";
        html_combo = '<option value="">- Pilih Salah Satu -</option>';
        for (var i = 0; i < responsesave.Data.length; i++) {
          if (responsesave.Data[i].Id == Id) {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].Id +
              '" selected>' +
              responsesave.Data[i].Nama +
              "</option>";
          } else {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].Id +
              '">' +
              responsesave.Data[i].Nama +
              "</option>";
          }
        }
        handleData(html_combo);
      } else if (responsesave.IsSuccess == false) {
        swal({
          title: "Gagal",
          text: responsesave.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}
function ComboGetJenisPegawais(handleData, Id) {
  var Url = base_url + "/api/Siswas/GetJenisPegawais";
  $.ajax({
    url: Url,
    method: "GET",
    beforeSend: function (before) {
      ProgressBar("wait");
    },
    success: function (responsesave) {
      ProgressBar("success");
      if (responsesave.IsSuccess == true) {
        var html_combo = "";
        html_combo = '<option value="">- Pilih Salah Satu -</option>';
        for (var i = 0; i < responsesave.Data.length; i++) {
          if (responsesave.Data[i].Id == Id) {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].Id +
              '" selected>' +
              responsesave.Data[i].Nama +
              "</option>";
          } else {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].Id +
              '">' +
              responsesave.Data[i].Nama +
              "</option>";
          }
        }
        handleData(html_combo);
      } else if (responsesave.IsSuccess == false) {
        swal({
          title: "Gagal",
          text: responsesave.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}
function ComboGetJenisCita2s(handleData, Id) {
  var Url = base_url + "/api/Siswas/GetJenisCita2s";
  $.ajax({
    url: Url,
    method: "GET",
    beforeSend: function (before) {
      ProgressBar("wait");
    },
    success: function (responsesave) {
      ProgressBar("success");
      if (responsesave.IsSuccess == true) {
        var html_combo = "";
        html_combo = '<option value="">- Pilih Salah Satu -</option>';
        for (var i = 0; i < responsesave.Data.length; i++) {
          if (responsesave.Data[i].Id == Id) {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].Id +
              '" selected>' +
              responsesave.Data[i].Nama +
              "</option>";
          } else {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].Id +
              '">' +
              responsesave.Data[i].Nama +
              "</option>";
          }
        }
        handleData(html_combo);
      } else if (responsesave.IsSuccess == false) {
        swal({
          title: "Gagal",
          text: responsesave.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}
function ComboGetJenisHobis(handleData, Id) {
  var Url = base_url + "/api/Siswas/GetJenisHobis";
  $.ajax({
    url: Url,
    method: "GET",
    beforeSend: function (before) {
      ProgressBar("wait");
    },
    success: function (responsesave) {
      ProgressBar("success");
      if (responsesave.IsSuccess == true) {
        var html_combo = "";
        html_combo = '<option value="">- Pilih Salah Satu -</option>';
        for (var i = 0; i < responsesave.Data.length; i++) {
          if (responsesave.Data[i].Id == Id) {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].Id +
              '" selected>' +
              responsesave.Data[i].Nama +
              "</option>";
          } else {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].Id +
              '">' +
              responsesave.Data[i].Nama +
              "</option>";
          }
        }
        handleData(html_combo);
      } else if (responsesave.IsSuccess == false) {
        swal({
          title: "Gagal",
          text: responsesave.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}
function ComboGetKelasParalels(handleData, Id) {
  var Url = base_url + "/api/Siswas/GetKelasParalels";
  $.ajax({
    url: Url,
    method: "GET",
    beforeSend: function (before) {
      ProgressBar("wait");
    },
    success: function (responsesave) {
      ProgressBar("success");
      if (responsesave.IsSuccess == true) {
        var html_combo = "";
        html_combo = '<option value="">- Pilih Salah Satu -</option>';
        for (var i = 0; i < responsesave.Data.length; i++) {
          if (responsesave.Data[i].IdKelasParalel == Id) {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].IdKelasParalel +
              '" selected>' +
              responsesave.Data[i].Nama +
              "</option>";
          } else {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].IdKelasParalel +
              '">' +
              responsesave.Data[i].Nama +
              "</option>";
          }
        }
        handleData(html_combo);
      } else if (responsesave.IsSuccess == false) {
        swal({
          title: "Gagal",
          text: responsesave.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}
function ComboGetProvinsi(handleData, Id) {
  var Url = base_url + "/api/Siswas/GetProvinsi";
  $.ajax({
    url: Url,
    method: "GET",
    beforeSend: function (before) {
      ProgressBar("wait");
    },
    success: function (responsesave) {
      ProgressBar("success");
      if (responsesave.IsSuccess == true) {
        var html_combo = "";
        html_combo = '<option value="">- Pilih Salah Satu -</option>';
        for (var i = 0; i < responsesave.Data.length; i++) {
          if (responsesave.Data[i].Id == Id) {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].Id +
              '" selected>' +
              responsesave.Data[i].Nama +
              "</option>";
          } else {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].Id +
              '">' +
              responsesave.Data[i].Nama +
              "</option>";
          }
        }
        handleData(html_combo);
      } else if (responsesave.IsSuccess == false) {
        swal({
          title: "Gagal",
          text: responsesave.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}
function ComboGetKabkot(handleData, Id, IdProv) {
  var Url = base_url + "/api/Siswas/GetKabkot?IdProvinsi=" + IdProv;
  $.ajax({
    url: Url,
    method: "GET",
    beforeSend: function (before) {
      ProgressBar("wait");
    },
    success: function (responsesave) {
      ProgressBar("success");
      if (responsesave.IsSuccess == true) {
        var html_combo = "";
        html_combo = '<option value="">- Pilih Salah Satu -</option>';
        for (var i = 0; i < responsesave.Data.length; i++) {
          if (responsesave.Data[i].Id == Id) {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].Id +
              '" selected>' +
              responsesave.Data[i].Nama +
              "</option>";
          } else {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].Id +
              '">' +
              responsesave.Data[i].Nama +
              "</option>";
          }
        }
        handleData(html_combo);
      } else if (responsesave.IsSuccess == false) {
        swal({
          title: "Gagal",
          text: responsesave.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}
function ComboGetKecamatan(handleData, Id, IdKabkot) {
  var Url = base_url + "/api/Siswas/GetKecamatan?IdKabkot=" + IdKabkot;
  $.ajax({
    url: Url,
    method: "GET",
    beforeSend: function (before) {
      ProgressBar("wait");
    },
    success: function (responsesave) {
      ProgressBar("success");
      if (responsesave.IsSuccess == true) {
        var html_combo = "";
        html_combo = '<option value="">- Pilih Salah Satu -</option>';
        for (var i = 0; i < responsesave.Data.length; i++) {
          if (responsesave.Data[i].Id == Id) {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].Id +
              '" selected>' +
              responsesave.Data[i].Nama +
              "</option>";
          } else {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].Id +
              '">' +
              responsesave.Data[i].Nama +
              "</option>";
          }
        }
        handleData(html_combo);
      } else if (responsesave.IsSuccess == false) {
        swal({
          title: "Gagal",
          text: responsesave.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}
function ComboGetDesa(handleData, Id, IdKecamatan) {
  var Url = base_url + "/api/Siswas/GetDesa?IdKecamatan=" + IdKecamatan;
  $.ajax({
    url: Url,
    method: "GET",
    beforeSend: function (before) {
      ProgressBar("wait");
    },
    success: function (responsesave) {
      ProgressBar("success");
      if (responsesave.IsSuccess == true) {
        var html_combo = "";
        html_combo = '<option value="">- Pilih Salah Satu -</option>';
        for (var i = 0; i < responsesave.Data.length; i++) {
          if (responsesave.Data[i].Id == Id) {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].Id +
              '" selected>' +
              responsesave.Data[i].Nama +
              "</option>";
          } else {
            html_combo +=
              '<option value="' +
              responsesave.Data[i].Id +
              '">' +
              responsesave.Data[i].Nama +
              "</option>";
          }
        }
        handleData(html_combo);
      } else if (responsesave.IsSuccess == false) {
        swal({
          title: "Gagal",
          text: responsesave.ReturnMessage,
          confirmButtonClass: "btn-danger text-white",
          confirmButtonText: "Oke, Mengerti",
          type: "error",
        });
      }
    },
    error: function (responserror, a, e) {
      ProgressBar("success");
      swal({
        title: "Error :(",
        text: JSON.stringify(responserror) + " : " + e,
        confirmButtonClass: "btn-danger text-white",
        confirmButtonText: "Oke, Mengerti",
        type: "error",
      });
    },
  });
}
