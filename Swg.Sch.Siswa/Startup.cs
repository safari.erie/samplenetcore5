using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Swg.Models;
using Swg.Sch.Services;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Siswa.Helpers;
using Swg.Services;

namespace Swg.Sch.Siswa
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddControllersWithViews();
            services.AddControllersWithViews().AddRazorRuntimeCompilation();

            services.AddControllersWithViews().AddJsonOptions(o =>
            {
                o.JsonSerializerOptions.PropertyNamingPolicy = null;
                o.JsonSerializerOptions.DictionaryKeyPolicy = null;
            });
            services.AddControllersWithViews().AddRazorRuntimeCompilation();
            services.AddMemoryCache();
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddSession(opts =>
            {
                opts.IdleTimeout = TimeSpan.FromMinutes(1000);//You can set Time   
                //opts.Cookie.IsEssential = true; // make the session cookie Essential
            });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            var appSettingsSection = Configuration.GetSection("AppSettings");

            services.Configure<AppSettingModel>(appSettingsSection);
            var appSettings = appSettingsSection.Get<AppSettingModel>();
            AppSetting.Secret = appSettings.Secret;
            AppSetting.ConnectionString = appSettings.ConnectionString;

            //set path
            AppSetting.PathApplFile = Path.Combine(appSettings.BasePath, appSettings.PathApplFile);
            AppSetting.PathApplUser = Path.Combine(appSettings.BasePath, appSettings.PathApplUser);
            AppSetting.PathApplImage = Path.Combine(appSettings.BasePath, appSettings.PathApplImage);
            AppSetting.PathApplVideo = Path.Combine(appSettings.BasePath, appSettings.PathApplVideo);
            AppSetting.PathSiswaRapor = Path.Combine(appSettings.BasePath, appSettings.PathSiswaRapor);
            AppSetting.PathSiswaFoto = Path.Combine(appSettings.BasePath, appSettings.PathSiswaFoto);
            AppSetting.PathKegiatanUnit = Path.Combine(appSettings.BasePath, appSettings.PathKegiatanUnit);
            AppSetting.PathGaleriUnit = Path.Combine(appSettings.BasePath, appSettings.PathGaleriUnit);
            AppSetting.PathPrestasiSekolah = Path.Combine(appSettings.BasePath, appSettings.PathPrestasiSekolah);
            AppSetting.PathFasilitasSekolah = Path.Combine(appSettings.BasePath, appSettings.PathFasilitasSekolah);
            AppSetting.PathKegiatanSekolah = Path.Combine(appSettings.BasePath, appSettings.PathKegiatanSekolah);
            AppSetting.PathGaleriSekolah = Path.Combine(appSettings.BasePath, appSettings.PathGaleriSekolah);
            AppSetting.PathPpdbPrestasi = Path.Combine(appSettings.BasePath, appSettings.PathPpdbPrestasi);
            AppSetting.PathPpdbBerkas = Path.Combine(appSettings.BasePath, appSettings.PathPpdbBerkas);
            AppSetting.PathPpdbCicil = Path.Combine(appSettings.BasePath, appSettings.PathPpdbCicil);
            AppSetting.PathElearning = Path.Combine(appSettings.BasePath, appSettings.PathElearning);
            AppSetting.PathEkskulUnit = Path.Combine(appSettings.BasePath, appSettings.PathEkskulUnit);
            AppSetting.PathTestimoni = Path.Combine(appSettings.BasePath, appSettings.PathTestimoni);

            //set path url
            AppSetting.PathUrlApplFile = Path.Combine(appSettings.BasePathUrl, appSettings.PathApplFile);
            AppSetting.PathUrlApplUser = Path.Combine(appSettings.BasePathUrl, appSettings.PathApplUser);
            AppSetting.PathUrlApplImage = Path.Combine(appSettings.BasePathUrl, appSettings.PathApplImage);
            AppSetting.PathUrlApplVideo = Path.Combine(appSettings.BasePathUrl, appSettings.PathApplVideo);
            AppSetting.PathUrlSiswaRapor = Path.Combine(appSettings.BasePathUrl, appSettings.PathSiswaRapor);
            AppSetting.PathUrlSiswaFoto = Path.Combine(appSettings.BasePathUrl, appSettings.PathSiswaFoto);
            AppSetting.PathUrlKegiatanUnit = Path.Combine(appSettings.BasePathUrl, appSettings.PathKegiatanUnit);
            AppSetting.PathUrlGaleriUnit = Path.Combine(appSettings.BasePathUrl, appSettings.PathGaleriUnit);
            AppSetting.PathUrlPrestasiSekolah = Path.Combine(appSettings.BasePathUrl, appSettings.PathPrestasiSekolah);
            AppSetting.PathUrlFasilitasSekolah = Path.Combine(appSettings.BasePathUrl, appSettings.PathFasilitasSekolah);
            AppSetting.PathUrlKegiatanSekolah = Path.Combine(appSettings.BasePathUrl, appSettings.PathKegiatanSekolah);
            AppSetting.PathUrlGaleriSekolah = Path.Combine(appSettings.BasePathUrl, appSettings.PathGaleriSekolah);
            AppSetting.PathUrlPpdbPrestasi = Path.Combine(appSettings.BasePathUrl, appSettings.PathPpdbPrestasi);
            AppSetting.PathUrlPpdbBerkas = Path.Combine(appSettings.BasePathUrl, appSettings.PathPpdbBerkas);
            AppSetting.PathUrlPpdbCicil = Path.Combine(appSettings.BasePathUrl, appSettings.PathPpdbCicil);
            AppSetting.PathUrlElearning = Path.Combine(appSettings.BasePathUrl, appSettings.PathElearning);
            AppSetting.PathUrlEkskulUnit = Path.Combine(appSettings.BasePathUrl, appSettings.PathEkskulUnit);
            AppSetting.PathUrlTestimoni = Path.Combine(appSettings.BasePathUrl, appSettings.PathTestimoni);


            services.AddScoped<ICommonService, CommonService>();
            services.AddScoped<IDateTimeService, DateTimeService>();
            services.AddScoped<IUserAppService, UserAppService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IPdfService, PdfService>();
            services.AddScoped<IKelasService, KelasService>();
            services.AddScoped<IPegawaiService, PegawaiService>();
            services.AddScoped<IPpdbService, PpdbService>();
            services.AddScoped<ISchService, SchService>();
            services.AddScoped<ISekolahService, SekolahService>();
            services.AddScoped<ISiswaService, SiswaService>();
            services.AddScoped<IUnitService, UnitService>();
            services.AddScoped<IWebService, WebService>();
            services.AddScoped<IELearningService, ELearningService>();
            services.AddScoped<IEskulService, EskulService>();
            services.AddScoped<IBankService, BankService>();
            services.AddScoped<IWebService, WebService>();


            services.AddScoped<IELearningLaporanService, ELearningLaporanService>();
            CommonService common = new CommonService();
            common.InitApplSetting();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseSession();
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathApplFile),
                RequestPath = new PathString(AppSetting.PathUrlApplFile)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathApplUser),
                RequestPath = new PathString(AppSetting.PathUrlApplUser)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathApplImage),
                RequestPath = new PathString(AppSetting.PathUrlApplImage)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathApplVideo),
                RequestPath = new PathString(AppSetting.PathUrlApplVideo)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathSiswaRapor),
                RequestPath = new PathString(AppSetting.PathUrlSiswaRapor)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathSiswaFoto),
                RequestPath = new PathString(AppSetting.PathUrlSiswaFoto)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathKegiatanUnit),
                RequestPath = new PathString(AppSetting.PathUrlKegiatanUnit)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathGaleriUnit),
                RequestPath = new PathString(AppSetting.PathUrlGaleriUnit)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathPrestasiSekolah),
                RequestPath = new PathString(AppSetting.PathUrlPrestasiSekolah)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathFasilitasSekolah),
                RequestPath = new PathString(AppSetting.PathUrlFasilitasSekolah)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathKegiatanSekolah),
                RequestPath = new PathString(AppSetting.PathUrlKegiatanSekolah)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathGaleriSekolah),
                RequestPath = new PathString(AppSetting.PathUrlGaleriSekolah)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathPpdbPrestasi),
                RequestPath = new PathString(AppSetting.PathUrlPpdbPrestasi)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathPpdbBerkas),
                RequestPath = new PathString(AppSetting.PathUrlPpdbBerkas)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathPpdbCicil),
                RequestPath = new PathString(AppSetting.PathUrlPpdbCicil)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathElearning),
                RequestPath = new PathString(AppSetting.PathUrlElearning)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathEkskulUnit),
                RequestPath = new PathString(AppSetting.PathUrlEkskulUnit)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathTestimoni),
                RequestPath = new PathString(AppSetting.PathUrlTestimoni)
            });
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseRouting();

            app.UseAuthorization();
            app.UseMiddleware<JwtMiddleware>();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Siswa}/{action=Index}/{id?}");
            });
        }
    }
}
