using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Shared.Interfaces;

namespace Swg.Sch.Siswa.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PublicController : Controller
    {
        private readonly ISiswaService siswaService;
        public PublicController(ISiswaService SiswaService)
        {
            siswaService = SiswaService;
        }
    }
}
