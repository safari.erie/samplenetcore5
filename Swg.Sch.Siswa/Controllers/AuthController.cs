﻿using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;
using Microsoft.AspNetCore.Http;
using Swg.Services;
using Swg.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System;

namespace Swg.Sch.Siswa.Controllers
{
    public class AuthController : Controller
    {
        private readonly IUserAppService _userService;
        private readonly int IdUser;
        private readonly string Token = string.Empty;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;

        IELearningService eLearningService;
        ISchService schService;
        ISiswaService siswaService;
        public AuthController(IHttpContextAccessor httpContextAccessor, IUserAppService userService, IELearningService ELearningService, ISchService SchService, ISiswaService SiswaService)
        {
            eLearningService = ELearningService;
            schService = SchService;
            siswaService = SiswaService;
            _userService = userService;
            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }

        }
        private string generateJwtToken(SiswaModel user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(AppSetting.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] {
                    new Claim("id", user.IdSiswa.ToString()),
                   }
                ),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public ResponseModel<string> MobileLogin(string Nis, string Password)
        {
            var ret = siswaService.SiswaLogin(Nis, Password, out string oMessage);
            var token = string.Empty;
            if (string.IsNullOrEmpty(oMessage))
                token = generateJwtToken(ret);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = token,
            };
        }

        public ResponseModel<SiswaModel> SiswaLogin(string Nis, string Password)
        {
            var ret = siswaService.SiswaLogin(Nis, Password, out string oMessage);
            var IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false;

            if (IsSuccess == true)
            {
                HttpContext.Session.SetInt32("IdSiswa", ret.IdSiswa);
                HttpContext.Session.SetInt32("IdKelas", ret.IdKelas);
                HttpContext.Session.SetString("Nama", ret.Nama);
                HttpContext.Session.SetString("Nis", ret.Nis);
                HttpContext.Session.SetString("KelasParalel", ret.KelasParalel);
                HttpContext.Session.SetString("Email", ret.Email == null ? "" : ret.Email);
                HttpContext.Session.SetString("EmailOrtu", ret.EmailOrtu == null ? "" : ret.EmailOrtu);
                HttpContext.Session.SetString("NoHandphone", ret.NoHandphone == null ? "" : ret.NoHandphone);
                HttpContext.Session.SetString("NamaPanggilan", ret.NamaPanggilan);
                HttpContext.Session.SetString("FotoProfile", string.IsNullOrEmpty(ret.FotoProfile) ? "" : ret.FotoProfile);
                return new ResponseModel<SiswaModel>
                {
                    IsSuccess = string.IsNullOrEmpty(oMessage),
                    ReturnMessage = oMessage,
                    Data = ret
                };
            }
            else
            {
                return new ResponseModel<SiswaModel>
                {
                    IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                    ReturnMessage = oMessage,
                    Data = ret
                };
            }
        }

        public ResponseModel<string> SiswaResetPassword(string Nis)
        {
            string ret = siswaService.SiswaResetPassword(Nis, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        public ResponseModel<string> SiswaChangePassword(string Nis, string OldPassword, string NewPassword1, string NewPassword2)
        {
            string ret = siswaService.SiswaChangePassword(Nis, OldPassword, NewPassword1, NewPassword2);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = (string.IsNullOrEmpty(ret)) ? ret : ""
            };
        }
    }
}