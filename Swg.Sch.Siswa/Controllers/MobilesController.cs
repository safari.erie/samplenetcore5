﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Models;
using Swg.Sch.Shared.Constants;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Siswa.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class MobilesController : ControllerBase
    {
        private readonly IELearningService eLearningService;
        private readonly ISiswaService siswaService;
        private readonly IEskulService eskulService;
        private readonly ISchService schService;
        private readonly IWebService webService;
        public MobilesController(IELearningService ELearningService,
            ISiswaService SiswaService,
            IEskulService EskulService,
            ISchService SchService,
            IWebService WebService)
        {
            eLearningService = ELearningService;
            siswaService = SiswaService;
            eskulService = EskulService;
            schService = SchService;
            webService = WebService;
        }

        protected SiswaModel UserInfo()
        {
            SiswaModel ret = (SiswaModel)HttpContext.Items["User"];
            return ret;
        }

        [HttpGet("UpdateTokenFcmSiswa", Name = "UpdateTokenFcmSiswa" + "Mobile")]
        public ResponseModel<string> UpdateTokenFcmSiswa(string Token, string UserAgent)
        {
            var ret = siswaService.UpdateTokenFcmSiswa(UserInfo().IdSiswa, 2, Token, UserAgent, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }

        [HttpGet("GetInfoUser", Name = "GetInfoUser" + "Mobile")]
        public ResponseModel<SiswaModel> GetInfoUser()
        {
            return new ResponseModel<SiswaModel>
            {
                IsSuccess = string.IsNullOrEmpty(null) ? true : false,
                ReturnMessage = string.Empty,
                Data = UserInfo()
            };
        }
        [HttpGet("Banner", Name = "Banner" + "Mobile")]
        public ResponseModel<string> Banner()
        {
            var ret = webService.GetApplSetting("mobile_banner", out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = ret.ValueString
            };
        }
        [HttpGet("CekVersi", Name = "CekVersi" + "Mobile")]
        public ResponseModel<string> CekVersi(string Versi)
        {
            var ret = webService.GetApplSetting("mobile_versi", out string oMessage);
            if (Versi != ret.ValueString)
            {
                oMessage = "versi tidak sama";
            }
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage) ? true : false,
                ReturnMessage = oMessage,
                Data = string.IsNullOrEmpty(oMessage) ? ret.ValueString : null,
            };
        }
        [HttpGet("GetInboxSiswas", Name = "GetInboxSiswas" + "Mobile")]
        public ResponseModel<List<SiswaInboxModel>> GetInboxSiswas()
        {
            var ret = siswaService.GetInboxSiswas(UserInfo().IdSiswa, out string oMessage);
            return new ResponseModel<List<SiswaInboxModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetInboxSiswa", Name = "GetInboxSiswa" + "Mobile")]
        public ResponseModel<SiswaInboxModel> GetInboxSiswa()
        {
            var ret = siswaService.GetInboxSiswa(UserInfo().IdSiswa, out string oMessage);
            return new ResponseModel<SiswaInboxModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("ReadInboxSiswa", Name = "ReadInboxSiswa" + "Mobile")]
        public ResponseModel<string> ReadInboxSiswa(int IdSiswaInbox)
        {
            var ret = siswaService.ReadInboxSiswa(IdSiswaInbox);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = ret
            };
        }
        [HttpGet("GetKbmBySiswas", Name = "GetKbmBySiswas" + "Mobile")]
        public ResponseModel<List<KbmSiswaListModel>> GetKbmBySiswas(string Tanggal)
        {
            var ret = eLearningService.GetKbmBySiswa(UserInfo().IdSiswa, Tanggal, out string oMessage);
            return new ResponseModel<List<KbmSiswaListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKbmBySiswa", Name = "GetKbmBySiswa" + "Mobile")]
        public ResponseModel<KbmSiswaModel> GetKbmBySiswa(int IdKbmMateri)
        {
            var ret = eLearningService.GetKbmBySiswa(UserInfo().IdSiswa, IdKbmMateri, out string oMessage);
            return new ResponseModel<KbmSiswaModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetJenisBantuan", Name = "GetJenisBantuan" + "Mobile")]
        public ResponseModel<List<ComboModel>> GetJenisBantuan()
        {
            var ret = eLearningService.GetJenisBantuan();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }
        [HttpPost("UploadJawabans", Name = "UploadJawabans" + "Mobile")]
        public ResponseModel<string> UploadJawabans(int IdKbmMateri, int IdJenisSoal, int IdJenisBantuanOrtu, string NamaUrl, List<IFormFile> FileUpload)
        {
            var ret = eLearningService.UploadJawaban(UserInfo().IdSiswa, IdKbmMateri, IdJenisSoal, IdJenisBantuanOrtu, NamaUrl, FileUpload);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }
        [HttpGet("GetSiswaTagihan", Name = "GetSiswaTagihan" + "Mobile")]
        public ResponseModel<SiswaTagihanModel> GetSiswaTagihan()
        {
            SiswaTagihanModel ret = siswaService.GetSiswaTagihan(UserInfo().Nis, out string oMessage);
            string tanggalLunas = ret.TanggalLunas;
            string refNo = ret.RefNo;
            string periode = ret.Periode;
            if (oMessage == "data tagihan siswa tidak ada")
            {
                oMessage = string.Empty;
                ret = new SiswaTagihanModel();
                ret.TagihanBerjalan = new List<SiswaTagihanDetilModel>();
                ret.TagihanSebelumnya = new List<SiswaTagihanDetilModel>();
                ret.RpBerjalan = 0;
                ret.RpSebelumnya = 0;
            }
            if (ret.Status == StatusLunasConstant.Dict[StatusLunasConstant.Lunas])
            {
                oMessage = string.Empty;

                ret = new SiswaTagihanModel();
                ret.TagihanBerjalan = new List<SiswaTagihanDetilModel>();
                ret.TagihanSebelumnya = new List<SiswaTagihanDetilModel>();
                ret.RpBerjalan = 0;
                ret.RpSebelumnya = 0;
                ret.Status = StatusLunasConstant.Dict[StatusLunasConstant.Lunas];
                ret.IdSiswa = UserInfo().IdSiswa;
                ret.TanggalLunas = tanggalLunas;
                ret.RefNo = refNo;
                ret.Periode = periode;
            }
            return new ResponseModel<SiswaTagihanModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("ValidasiPin", Name = "ValidasiPin" + "Mobile")]
        public ResponseModel<string> ValidasiPin(string Pin)
        {
            var ret = siswaService.ValidasiPin(UserInfo().IdSiswa, Pin);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = null
            };
        }
        [HttpGet("ResetPin", Name = "ResetPin" + "Mobile")]
        public ResponseModel<string> ResetPin()
        {
            var ret = siswaService.ResetPin(UserInfo().IdSiswa);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = null
            };
        }
        [HttpGet("GantiPin", Name = "GantiPin" + "Mobile")]
        public ResponseModel<string> GantiPin(string PinLama, string PinBaru)
        {
            var ret = siswaService.GantiPin(UserInfo().IdSiswa, PinLama, PinBaru);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = null
            };
        }

        [HttpGet("GetKbmEvalHarians", Name = "GetKbmEvalHarians" + "Mobile")]
        public ResponseModel<List<KbmEvalHarianListModel>> GetKbmEvalHarians(string Tanggal)
        {
            var ret = eLearningService.GetKbmEvalHarians(UserInfo().IdSiswa, Tanggal, out string oMessage);
            return new ResponseModel<List<KbmEvalHarianListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("EvaluasiHarianAdd", Name = "EvaluasiHarianAdd" + "Mobile")]
        public ResponseModel<string> EvaluasiHarianAdd(List<KbmEvalHarianAddModel> Data)
        {
            var ret = eLearningService.EvaluasiHarianAdd(UserInfo().IdSiswa, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }
        [HttpGet("GetKelasQuranSiswa", Name = "GetKelasQuranSiswa" + "Mobile")]
        public ResponseModel<KelasQuranSiswaModel> GetKelasQuranSiswa(string Tanggal)
        {
            var ret = eskulService.GetKelasQuranSiswa(UserInfo().IdSiswa, Tanggal, out string oMessage);
            return new ResponseModel<KelasQuranSiswaModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetTahunAjarans", Name = "GetTahunAjarans" + "Mobile")]
        public ResponseModel<List<TahunAjaranModel>> GetTahunAjarans()
        {
            var ret = schService.GetTahunAjarans(out string oMessage);
            return new ResponseModel<List<TahunAjaranModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetSiswaRaporsBySiswa", Name = "GetSiswaRaporsBySiswa" + "Mobile")]
        public ResponseModel<List<SiswaRaporModel>> GetSiswaRaporsBySiswa()
        {
            var ret = siswaService.GetSiswaRapors(UserInfo().IdSiswa, out string oMessage);
            return new ResponseModel<List<SiswaRaporModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetSiswaRapors", Name = "GetSiswaRapors" + "Mobile")]
        public ResponseModel<List<SiswaRaporModel>> GetSiswaRapors(int IdTahunAjaran)
        {
            var ret = siswaService.GetSiswaRapors(UserInfo().IdSiswa, IdTahunAjaran, out string oMessage);
            return new ResponseModel<List<SiswaRaporModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("EditProfilSiswa", Name = "EditProfilSiswa" + "Mobile")]
        public ResponseModel<string> EditProfilSiswa(string Email, string EmailOrtu, string NoHandphone, IFormFile FotoProfile)
        {
            var ret = siswaService.EditProfilSiswa(UserInfo().IdSiswa, Email, EmailOrtu, NoHandphone, FotoProfile);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = null
            };
        }

        [HttpGet("GetSiswa", Name = "GetSiswa" + "Mobile")]
        public ResponseModel<SiswaModel> GetSiswa()
        {
            var ret = siswaService.GetSiswa(UserInfo().IdSiswa, out string oMessage);
            return new ResponseModel<SiswaModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GantiPassword", Name = "GantiPassword" + "Mobile")]
        public ResponseModel<string> GantiPassword(string OldPassword, string NewPassword1, string NewPassword2)
        {
            string ret = siswaService.SiswaChangePassword(UserInfo().Nis, OldPassword, NewPassword1, NewPassword2);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = (string.IsNullOrEmpty(ret)) ? ret : ""
            };
        }
        [HttpGet("GetBanners", Name = "GetBanners" + "Mobile")]
        public ResponseModel<List<BannerModel>> GetBanners()
        {
            List<BannerModel> ret = schService.GetBanners("siswamobile", out string oMessage);
            return new ResponseModel<List<BannerModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("PenilaianDiriSiswaAdd", Name = "PenilaianDiriSiswaAdd" + "Mobile")]
        public ResponseModel<string> PenilaianDiriSiswaAdd(PenilaianDiriSiswaAddModel Data)
        {
            var oMessage = eLearningService.PenilaianDiriSiswaAdd(Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = string.Empty
            };
        }
        [HttpGet("GetPenilaianDiriSiswa", Name = "GetPenilaianDiriSiswa" + "Mobile")]
        public ResponseModel<List<PenilaianDiriSiswaListModel>> GetPenilaianDiriSiswa(int IdTahunAjaran, int Semester)
        {
            var ret = eLearningService.GetPenilaianDiriSiswa(UserInfo().IdSiswa, IdTahunAjaran, Semester, out string oMessage);
            return new ResponseModel<List<PenilaianDiriSiswaListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetSemester", Name = "GetSemester" + "Mobile")]
        public ResponseModel<SemesterModel> GetSemester()
        {
            var ret = schService.GetSemester(out string oMessage);
            return new ResponseModel<SemesterModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

    }
}
