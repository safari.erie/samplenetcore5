﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Models;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;

namespace Swg.Sch.Siswa.Controllers
{
    [Route("api/[controller]")]
    public class SiswasController : BaseController
    {
        private readonly IELearningService eLearningService;
        private readonly ISiswaService siswaService;
        private readonly ISchService schService;
        private readonly IEskulService eskulService;
        private readonly IKelasService kelasService;
        private readonly int IdSiswa;
        private readonly int IdKelas;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        public SiswasController(IHttpContextAccessor httpContextAccessor, IELearningService ELearningService, ISchService SchService, ISiswaService SiswaService, IEskulService EskulService, IKelasService KelasService)
        {
            eLearningService = ELearningService;
            schService = SchService;
            siswaService = SiswaService;
            eskulService = EskulService;
            kelasService = KelasService;
            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdSiswa") != null)
                    IdSiswa = (int)_session.GetInt32("IdSiswa");
                if (_session.GetInt32("IdKelas") != null)
                    IdKelas = (int)_session.GetInt32("IdKelas");
            }
        }
        [HttpPost("GeneratePasswordAllSiswa", Name = "GeneratePasswordAllSiswa")]
        public ResponseModel<string> GeneratePasswordAllSiswa()
        {
            var ret = schService.GeneratePasswordAllSiswa();
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = ret
            };
        }
        [HttpGet("GetKbmBySiswas", Name = "GetKbmBySiswas")]
        public ResponseModel<List<KbmSiswaListModel>> GetKbmBySiswas(string Tanggal)
        {
            var ret = eLearningService.GetKbmBySiswa(IdSiswa, Tanggal, out string oMessage);
            return new ResponseModel<List<KbmSiswaListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKbmBySiswa", Name = "GetKbmBySiswa")]
        public ResponseModel<KbmSiswaModel> GetKbmBySiswa(int IdKbmMateri)
        {
            var ret = eLearningService.GetKbmBySiswa(IdSiswa, IdKbmMateri, out string oMessage);
            return new ResponseModel<KbmSiswaModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetJenisEvaluasiHarian", Name = "GetJenisEvaluasiHarian")]
        public ResponseModel<List<ComboModel>> GetJenisEvaluasiHarian()
        {
            var ret = eLearningService.GetJenisEvaluasiHarian(IdSiswa, out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKbmEvalHarians", Name = "GetKbmEvalHarians")]
        public ResponseModel<List<KbmEvalHarianListModel>> GetKbmEvalHarians(string Tanggal)
        {
            var ret = eLearningService.GetKbmEvalHarians(IdSiswa, Tanggal, out string oMessage);
            return new ResponseModel<List<KbmEvalHarianListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetEvaluasiHarian", Name = "GetEvaluasiHarian")]
        public ResponseModel<KbmEvalHarianModel> GetEvaluasiHarian(int IdSiswa, string Tanggal, int IdJenisEvaluasiHarian)
        {
            var ret = eLearningService.GetKbmEvalHarian(IdSiswa, Tanggal, IdJenisEvaluasiHarian, out string oMessage);
            return new ResponseModel<KbmEvalHarianModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("EvaluasiHarianAdd", Name = "EvaluasiHarianAdd")]
        public ResponseModel<string> EvaluasiHarianAdd([FromBody] List<KbmEvalHarianAddModel> Data)
        {
            var ret = eLearningService.EvaluasiHarianAdd(IdSiswa, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }




        [HttpGet("GetSiswaRaporsBySiswa", Name = "GetSiswaRaporsBySiswa")]
        public ResponseModel<List<SiswaRaporModel>> GetSiswaRaporsBySiswa()
        {
            var ret = siswaService.GetSiswaRapors(IdSiswa, out string oMessage);
            return new ResponseModel<List<SiswaRaporModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetSiswaRapors", Name = "GetSiswaRapors")]
        public ResponseModel<List<SiswaRaporModel>> GetSiswaRapors(int IdTahunAjaran)
        {
            var ret = siswaService.GetSiswaRapors(IdSiswa, IdTahunAjaran, out string oMessage);
            return new ResponseModel<List<SiswaRaporModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetSiswaRapor", Name = "GetSiswaRapor")]
        public ResponseModel<SiswaRaporModel> GetSiswaRapor(int IdSiswa, int IdJenisRaport)
        {
            var ret = siswaService.GetSiswaRapor(IdSiswa, IdJenisRaport, out string oMessage);
            return new ResponseModel<SiswaRaporModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetJenisBantuan", Name = "GetJenisBantuan")]
        public ResponseModel<List<ComboModel>> GetJenisBantuan()
        {
            var ret = eLearningService.GetJenisBantuan();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = string.Empty,
                Data = ret
            };
        }
        [HttpPost("UploadJawaban", Name = "UploadJawaban")]
        public ResponseModel<string> UploadJawaban(int IdKbmMateri, int IdJenisSoal, int IdJenisBantuanOrtu, string NamaUrl, IFormFile FileUpload)
        {
            var ret = eLearningService.UploadJawaban(IdSiswa, IdKbmMateri, IdJenisSoal, IdJenisBantuanOrtu, NamaUrl, FileUpload);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }
        [HttpPost("UploadJawabans", Name = "UploadJawabans")]
        public ResponseModel<string> UploadJawabans(int IdKbmMateri, int IdJenisSoal, int IdJenisBantuanOrtu, string NamaUrl, List<IFormFile> FileUpload)
        {
            var ret = eLearningService.UploadJawaban(IdSiswa, IdKbmMateri, IdJenisSoal, IdJenisBantuanOrtu, NamaUrl, FileUpload);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = string.IsNullOrEmpty(ret) ? ret : null
            };
        }
        [HttpGet("GetKelasQuranSiswa", Name = "GetKelasQuranSiswa")]
        public ResponseModel<KelasQuranSiswaModel> GetKelasQuranSiswa(string Tanggal)
        {
            var ret = eskulService.GetKelasQuranSiswa(IdSiswa, Tanggal, out string oMessage);
            return new ResponseModel<KelasQuranSiswaModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("ValidasiPin", Name = "ValidasiPin")]
        public ResponseModel<string> ValidasiPin(string Pin)
        {
            var ret = siswaService.ValidasiPin(IdSiswa, Pin);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = null
            };
        }

        [HttpPost("GantiPin", Name = "GantiPin")]
        public ResponseModel<string> GantiPin(string PinLama, string PinBaru)
        {
            var ret = siswaService.GantiPin(IdSiswa, PinLama, PinBaru);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = null
            };
        }
        [HttpPost("ResetPin", Name = "ResetPin")]
        public ResponseModel<string> ResetPin()
        {
            var ret = siswaService.ResetPin(IdSiswa);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = null
            };
        }
        [HttpPost("EditProfilSiswa", Name = "EditProfilSiswa")]
        public ResponseModel<string> EditProfilSiswa(string Email, string EmailOrtu, string NoHandphone, IFormFile FotoProfile)
        {
            var ret = siswaService.EditProfilSiswa(IdSiswa, Email, EmailOrtu, NoHandphone, FotoProfile);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(ret),
                ReturnMessage = ret,
                Data = null
            };
        }

        [HttpGet("GetSiswa", Name = "GetSiswa")]
        public ResponseModel<SiswaModel> GetSiswa()
        {
            var ret = siswaService.GetSiswa(IdSiswa, out string oMessage);
            return new ResponseModel<SiswaModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetTahunAjarans", Name = "GetTahunAjarans")]
        public ResponseModel<List<TahunAjaranModel>> GetTahunAjarans()
        {
            var ret = schService.GetTahunAjarans(out string oMessage);
            return new ResponseModel<List<TahunAjaranModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("UpdateTokenFcmSiswa", Name = "UpdateTokenFcmSiswa")]
        public ResponseModel<string> UpdateTokenFcmSiswa(string Token, string UserAgent)
        {
            var ret = siswaService.UpdateTokenFcmSiswa(IdSiswa, 1, Token, UserAgent, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = ret
            };
        }
        [HttpGet("GetBanners", Name = "GetBanners")]
        public ResponseModel<List<BannerModel>> GetBanners()
        {
            List<BannerModel> ret = schService.GetBanners("siswaweb", out string oMessage);
            return new ResponseModel<List<BannerModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("PenilaianDiriSiswaAdd", Name = "PenilaianDiriSiswaAdd")]
        public ResponseModel<string> PenilaianDiriSiswaAdd(PenilaianDiriSiswaAddModel Data)
        {
            var oMessage = eLearningService.PenilaianDiriSiswaAdd(Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = string.Empty
            };
        }
        [HttpGet("GetPenilaianDiriSiswa", Name = "GetPenilaianDiriSiswa")]
        public ResponseModel<List<PenilaianDiriSiswaListModel>> GetPenilaianDiriSiswa(int IdTahunAjaran, int Semester)
        {
            var ret = eLearningService.GetPenilaianDiriSiswa(IdSiswa, IdTahunAjaran, Semester, out string oMessage);
            return new ResponseModel<List<PenilaianDiriSiswaListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetSemester", Name = "GetSemester")]
        public ResponseModel<SemesterModel> GetSemester()
        {
            var ret = schService.GetSemester(out string oMessage);
            return new ResponseModel<SemesterModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        #region ref
        [HttpGet("GetJenisHobis", Name = "GetJenisHobis")]
        public ResponseModel<List<ComboModel>> GetJenisHobis()
        {
            var ret = schService.GetJenisHobis(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetAgamas", Name = "GetAgamas")]
        public ResponseModel<List<ComboModel>> GetAgamas()
        {
            var ret = schService.GetAgamas();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = null,
                Data = ret
            };
        }
        [HttpGet("GetJenisPekerjaans", Name = "GetJenisPekerjaans")]
        public ResponseModel<List<ComboModel>> GetJenisPekerjaans()
        {
            var ret = schService.GetJenisPekerjaans(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetJenisPegawais", Name = "GetJenisPegawais")]
        public ResponseModel<List<ComboModel>> GetJenisPegawais()
        {
            var ret = schService.GetJenisPegawais();
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = true,
                ReturnMessage = null,
                Data = ret
            };
        }

        [HttpGet("GetJenisCita2s", Name = "GetJenisCita2s")]
        public ResponseModel<List<ComboModel>> GetJenisCita2s()
        {
            var ret = schService.GetJenisCita2s(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        #endregion
        [HttpGet("GetSiswaProfile", Name = "GetSiswaProfile")]
        public ResponseModel<SiswaViewProfileModel> GetSiswaProfile()
        {
            var ret = siswaService.GetSiswaProfile(IdSiswa, out string oMessage);
            return new ResponseModel<SiswaViewProfileModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("EditSiswaProfil", Name = "EditSiswaProfil")]
        public ResponseModel<string> EditSiswaProfil(SiswaEditProfileModel Data)
        {
            var oMessage = siswaService.EditSiswaProfil(IdSiswa, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpGet("IsEditSiswaProfil", Name = "IsEditSiswaProfil")]
        public ResponseModel<string> IsEditSiswaProfil()
        {
            var ret = siswaService.IsEditSiswaProfil(IdSiswa, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = ret,
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpGet("GetKelasParalels", Name = "GetKelasParalels")]
        public ResponseModel<List<KelasParalelModel>> GetKelasParalels()
        {
            var ret = kelasService.GetKelasParalels(IdKelas, out string oMessage);
            return new ResponseModel<List<KelasParalelModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetProvinsi", Name = "GetProvinsi" + "Webs")]
        public ResponseModel<List<ComboModel>> GetProvinsi()
        {
            List<ComboModel> ret = schService.GetProvinsi(out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKabkot", Name = "GetKabkot" + "Webs")]
        public ResponseModel<List<ComboModel>> GetKabkot(int IdProvinsi)
        {
            List<ComboModel> ret = schService.GetKabkot(IdProvinsi, out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKecamatan", Name = "GetKecamatan" + "Webs")]
        public ResponseModel<List<ComboModel>> GetKecamatan(int IdKabkot)
        {
            List<ComboModel> ret = schService.GetKecamatan(IdKabkot, out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetDesa", Name = "GetDesa" + "Webs")]
        public ResponseModel<List<ComboModel>> GetDesa(int IdKecamatan)
        {
            List<ComboModel> ret = schService.GetDesa(IdKecamatan, out string oMessage);
            return new ResponseModel<List<ComboModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
    }
}
