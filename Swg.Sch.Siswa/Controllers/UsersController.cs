﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Swg.Services;
using Swg.Models;

namespace Swg.Sch.Siswa.Controllers
{
    public class UsersController : BaseController
    {
        private readonly IUserAppService _userService;
        private readonly int IdUser;
        private readonly string Token = string.Empty;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        public UsersController(IHttpContextAccessor httpContextAccessor, IUserAppService userService)
        {
            _userService = userService;
            _httpContextAccessor = httpContextAccessor;
            if (_session.IsAvailable)
            {
                if (_session.GetInt32("IdUser") != null)
                    IdUser = (int)_session.GetInt32("IdUser");
            }
        }

        public ResponseModel<string> Logout()
        {
            HttpContext.Session.Clear();
            return new ResponseModel<string>
            {
                IsSuccess = true,
                ReturnMessage = null,
                Data = null
            };
        }

        public ResponseModel<List<ApplTaskModel>> GetMenuss(int IdAppl)
        {
            List<ApplTaskModel> ret = _userService.GetMenus(IdAppl, IdUser, out string oMessage);
            return new ResponseModel<List<ApplTaskModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        public ResponseModel<List<RoleModel>> GetRoles()
        {
            List<RoleModel> ret = _userService.GetRoles(out string oMessage);
            return new ResponseModel<List<RoleModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        public ResponseModel<UserModel> GetUser(int IdUser)
        {
            UserModel ret = _userService.GetUser(IdUser, out string oMessage);
            return new ResponseModel<UserModel>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<UserModel> GetUserUpdateProfile()
        {
            UserModel ret = _userService.GetUser(IdUser, out string oMessage);
            return new ResponseModel<UserModel>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        public ResponseModel<RoleModel> GetRole(int IdRole)
        {
            RoleModel ret = _userService.GetRole(IdRole, out string oMessage);
            return new ResponseModel<RoleModel>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        public ResponseModel<string> EditUser([FromBody] UserModel Data)
        {
            string ret = _userService.EditUser(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }

        public ResponseModel<string> ChangePassword(string UsernameOrEmail, string OldPassword, string NewPassword1, string NewPassword2)
        {
            string ret = _userService.ChangePassword(UsernameOrEmail, OldPassword, NewPassword1, NewPassword2);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }
        public ResponseModel<string> UpdateProfile(string Email, string FirstName, string MiddleName, string LastName, string Address, string PhoneNumber, string MobileNumber, IFormFile FileImage)
        {
            string ret = _userService.UpdateProfile(IdUser, Email, FirstName, MiddleName, LastName, Address, PhoneNumber, MobileNumber, FileImage);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }
        public ResponseModel<List<UserModel>> GetUsers()
        {

            var ret = _userService.GetUsers(out string oMessage);
            return new ResponseModel<List<UserModel>>
            {
                IsSuccess = ret != null,
                ReturnMessage = ret != null ? "" : "data tidak ada",
                Data = ret
            };
        }

        public ResponseModel<string> AddUser([FromBody] UserModel Data)
        {
            string ret = _userService.AddUser(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }

        public ResponseModel<string> AddRole(int IdRole, string RoleName)
        {
            string ret = _userService.AddRole(IdRole, RoleName);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }

        public ResponseModel<string> DeleteRole(int IdRole)
        {
            string ret = _userService.DeleteRole(IdRole);
            return new ResponseModel<string>
            {
                IsSuccess = (string.IsNullOrEmpty(ret)) ? true : false,
                ReturnMessage = ret,
                Data = null
            };
        }

    }
}