﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Siswa.Models;

namespace Swg.Sch.Siswa.Controllers
{
    public class SiswaController : BaseController
    {
        private readonly IWebService webService;
        private readonly ISchService schService;
        public SiswaController(IWebService WebService, ISchService SchService)
        {
            webService = WebService;
            schService = SchService;
        }
        public IActionResult Index()
        {
            var ret = schService.GetBanners("siswaweb", out string oMessage);
            if (string.IsNullOrEmpty(oMessage)) ViewBag.siswabanner = ret;
            return View();
        }

        public IActionResult Learning()
        {
            var jss = ModuleJs("elearning");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult MutabaahSiswa()
        {
            var jss = ModuleJs("mutabaah_siswa");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult PenilaianSiswa()
        {
            var jss = ModuleJs("penilaian_siswa");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult KelompokQiroaty()
        {
            var jss = ModuleJs("kelompok_qiroaty");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult Tagihan()
        {
            var jss = ModuleJs("tagihan");
            ViewBag.ModuleJs = jss;
            return View();
        }
        public IActionResult Raport()
        {
            var jss = ModuleJs("raport");
            ViewBag.ModuleJs = jss;
            return View();
        }

        public List<LibraryJsModel> ModuleJs(string Module)
        {
            List<LibraryJsModel> js = new List<LibraryJsModel>();
            LibraryJsModel elearning_js = new LibraryJsModel
            {
                NameJs = "elearning.js?n=1",
                TypeJs = "module",
                Path = "siswa"
            };
            LibraryJsModel mutabaah_siswa_js = new LibraryJsModel
            {
                NameJs = "mutabaah_siswa.js?n=1",
                TypeJs = "module",
                Path = "siswa"
            };
            LibraryJsModel penilaian_siswa_js = new LibraryJsModel
            {
                NameJs = "penilaian_siswa.js?n=1",
                TypeJs = "module",
                Path = "siswa"
            };
            LibraryJsModel kelompok_qiroaty_js = new LibraryJsModel
            {
                NameJs = "kelompok_qiroaty.js?n=1",
                TypeJs = "module",
                Path = "siswa"
            };
            LibraryJsModel tagihan_js = new LibraryJsModel
            {
                NameJs = "tagihan.js?n=1",
                TypeJs = "module",
                Path = "siswa"
            };
            LibraryJsModel raport_js = new LibraryJsModel
            {
                NameJs = "raport.js?n=1",
                TypeJs = "module",
                Path = "siswa"
            };

            if (Module == "elearning")
            {
                js.Add(elearning_js);
            }
            if (Module == "tagihan")
            {
                js.Add(tagihan_js);
            }
            if (Module == "raport")
            {
                js.Add(raport_js);
            }
            if (Module == "mutabaah_siswa")
            {
                js.Add(mutabaah_siswa_js);
            }
            if (Module == "penilaian_siswa")
            {
                js.Add(penilaian_siswa_js);
            }
            if (Module == "kelompok_qiroaty")
            {
                js.Add(kelompok_qiroaty_js);
            }
            return js;
        }
    }
}
