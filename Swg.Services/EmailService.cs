﻿using System;
using Swg.Models;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using System.Threading.Tasks;

namespace Swg.Services
{
    public interface IEmailService
    {
        Task SendEmailAsync(SendEmailModel Email);

        Task SendHtmlEmailAsync(SendEmailModel Email);
    }
    public class EmailService : IEmailService
    {
        public EmailService()
        {
        }

        private string EmailAdressTitle { get; set; }
        public async Task SendEmailAsync(SendEmailModel Email)
        {
            var mimeMessage = new MimeMessage();
            mimeMessage.From.Add(new MailboxAddress(EmailAdressTitle, AppSetting.OrgEmail));
            for (int i = 0; i < Email.To.Count; i++)
            {
                mimeMessage.To.Add(new MailboxAddress(Email.Title, Email.To[i]));
            }
            for (int i = 0; i < Email.Cc.Count; i++)
            {
                mimeMessage.Cc.Add(new MailboxAddress(Email.Title, Email.Cc[i]));
            }

            mimeMessage.Subject = Email.Subject;

            var builder = new BodyBuilder();
            builder.TextBody = Email.Message;
            if (Email.Attachments != null)
            {
                foreach (var attacthment in Email.Attachments)
                {
                    builder.Attachments.Add(attacthment);
                }
            }
            mimeMessage.Body = builder.ToMessageBody();
            try
            {
                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    await client.ConnectAsync(AppSetting.OrgEmailSmtpServer, (int)AppSetting.OrgEmailSmtpPort, SecureSocketOptions.Auto);
                    await client.AuthenticateAsync(AppSetting.OrgEmail, AppSetting.OrgEmailPassword);
                    client.AuthenticationMechanisms.Remove("XOAUTH");
                    await client.SendAsync(mimeMessage);
                    await client.DisconnectAsync(true);

                }
            }
            catch (Exception ex)
            {
                var e = ex;
                throw;
            }
        }

        public async Task SendHtmlEmailAsync(SendEmailModel Email)
        {
            var mimeMessage = new MimeMessage();
            mimeMessage.From.Add(new MailboxAddress(EmailAdressTitle, AppSetting.OrgEmail));
            for (int i = 0; i < Email.To.Count; i++)
            {
                mimeMessage.To.Add(new MailboxAddress(Email.Title, Email.To[i]));
            }
            for (int i = 0; i < Email.Cc.Count; i++)
            {
                mimeMessage.Cc.Add(new MailboxAddress(Email.Title, Email.Cc[i]));
            }
            mimeMessage.Subject = Email.Subject;

            var builder = new BodyBuilder();
            builder.HtmlBody = Email.Message;
            if (Email.Attachments != null)
            {
                foreach (var attacthment in Email.Attachments)
                {
                    builder.Attachments.Add(attacthment);
                }
            }
            mimeMessage.Body = builder.ToMessageBody();
            try
            {
                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    await client.ConnectAsync(AppSetting.OrgEmailSmtpServer, (int)AppSetting.OrgEmailSmtpPort, SecureSocketOptions.Auto);
                    await client.AuthenticateAsync(AppSetting.OrgEmail, AppSetting.OrgEmailPassword);
                    client.AuthenticationMechanisms.Remove("XOAUTH");
                    await client.SendAsync(mimeMessage);
                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception ex)
            {
                var e = ex;
                throw;
            }
        }
    }

}
