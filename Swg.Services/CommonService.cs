﻿using Dapper;
using Npgsql;
using RestSharp;
using Swg.Entities.Keu;
using Swg.Entities.Lmg;
using Swg.Entities.Public;
using Swg.Entities.Ref;
using Swg.Models;
using Swg.Models.Constants;
using Swg.Sch.Shared.Constants;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Swg.Services
{
    public enum ImageFormat
    {
        bmp,
        jpeg,
        gif,
        tiff,
        png,
        unknown
    }
    public interface ICommonService
    {
        ImageFormat GetImageFormat(byte[] bytes);
        bool ValidateUsername(string Username);
        bool ValidatePassword(string password, out string oMessage);
        bool ValidateEmail(string Email, out string oMessage);
        string CreateRandomPassword(int length);
        string GetErrorMessage(string MethodeName, Exception ex);
        string DecryptString(string cipherText);
        string EncryptString(string text);
        void InitApplSetting();
        IDbConnection DbConnection();
        string SetNoHandphone(string NoHp);
        double BulatkanRpPuluh(double Rp);
        double BulatkanRpRatus(double Rp);
        double BulatkanRpRibu(double Rp);
        double BulatkanRpJuta(double Rp);
        double BulatkanRpMilyar(double Rp);
        string Terbilang(decimal Rp);
        string SetStrRupiah(double Rp);
        bool DictCheck(Dictionary<int, string> dics, int Id);
        bool DictCheck(Dictionary<string, string> dics, string Kd);
        string SetAlamat(string AlTunjuk, string AlAlamat, string AlBlokNo, string AlRw, string AlRt, string Desa, string Kec, string Kab, string Prov);
        string PushNotifMobile(string Token, string Judul, string Deskripsi);
        string ToSlug(string phrase);
        string SetStringNull(string text);
        int SetIntegerNUll(int? number);
        double SetNumberNull(double? number);
    }
    public class CommonService : ICommonService
    {
        private readonly string KeyString = AppSetting.Secret;
        private static Regex sUserNameAllowedRegEx = new Regex(@"^[a-zA-Z]{1}[a-zA-Z0-9\._\-]{0,23}[^.-]$", RegexOptions.Compiled);
        private static Regex sUserNameIllegalEndingRegEx = new Regex(@"(\.|\-|\._|\-_)$", RegexOptions.Compiled);
        private void InitApplSetting(IDbConnection conn)
        {
            AppSetting.OrgNama = conn.Get<TbApplSetting>("org_nama").ValueString;
            AppSetting.OrgNamaPendek = conn.Get<TbApplSetting>("org_nama_pendek").ValueString;
            AppSetting.OrgAlamat = conn.Get<TbApplSetting>("org_alamat").ValueString;
            AppSetting.OrgKodePos = conn.Get<TbApplSetting>("org_kode_pos").ValueString;
            AppSetting.OrgLongitude = (double)conn.Get<TbApplSetting>("org_longitude").ValueNumber;
            AppSetting.OrgLatitude = (double)conn.Get<TbApplSetting>("org_latitude").ValueNumber;
            AppSetting.OrgTelpon = conn.Get<TbApplSetting>("org_telpon").ValueString;
            AppSetting.OrgFaximili = conn.Get<TbApplSetting>("org_faximili").ValueString;
            AppSetting.OrgWeb = conn.Get<TbApplSetting>("org_web").ValueString;
            AppSetting.OrgEmail = conn.Get<TbApplSetting>("org_email").ValueString;
            AppSetting.OrgEmailPassword = conn.Get<TbApplSetting>("org_email_password").ValueString;
            AppSetting.OrgEmailSmtpServer = conn.Get<TbApplSetting>("org_emailsmtp_server").ValueString;
            AppSetting.OrgEmailSmtpPort = (int)conn.Get<TbApplSetting>("org_emailsmtp_port").ValueNumber;
        }
        private void InitHakAses(IDbConnection conn)
        {
            var data = conn.GetList<TbJenisHakAkses>();
            HakAksesConstant.Dict = new Dictionary<int, string>();
            foreach (var item in data)
            {
                HakAksesConstant.Dict.Add(item.IdJenisHakAkses, item.Nama);
                switch (item.NamaSingkat)
                {
                    case "CanReadGajiAll":
                        HakAksesConstant.CanReadGajiAll = item.IdJenisHakAkses;
                        break;
                    case "CanReadGajiGT":
                        HakAksesConstant.CanReadGajiGT = item.IdJenisHakAkses;
                        break;
                    case "CanReadGajiGTT":
                        HakAksesConstant.CanReadGajiGTT = item.IdJenisHakAkses;
                        break;
                    case "CanReadGajiPot":
                        HakAksesConstant.CanReadGajiPot = item.IdJenisHakAkses;
                        break;
                    case "CanReadGajiTam":
                        HakAksesConstant.CanReadGajiTam = item.IdJenisHakAkses;
                        break;
                    case "CanReadGajiTot":
                        HakAksesConstant.CanReadGajiTot = item.IdJenisHakAkses;
                        break;

                    case "CanReadKonten":
                        HakAksesConstant.CanReadKonten = item.IdJenisHakAkses;
                        break;
                    case "CanCreateKonten":
                        HakAksesConstant.CanCreateKonten = item.IdJenisHakAkses;
                        break;
                    case "CanApproveKonten":
                        HakAksesConstant.CanApproveKonten = item.IdJenisHakAkses;
                        break;
                    case "CanPublishKonten":
                        HakAksesConstant.CanPublishKonten = item.IdJenisHakAkses;
                        break;
                    case "CanRejectKonten":
                        HakAksesConstant.CanRejectKonten = item.IdJenisHakAkses;
                        break;
                    case "CanDeleteKonten":
                        HakAksesConstant.CanDeleteKonten = item.IdJenisHakAkses;
                        break;
                    case "CanEditKonten":
                        HakAksesConstant.CanEditKonten = item.IdJenisHakAkses;
                        break;

                    case "CanAddRkas":
                        HakAksesConstant.CanAddRkas = item.IdJenisHakAkses;
                        break;
                    case "CanEditRkas":
                        HakAksesConstant.CanEditRkas = item.IdJenisHakAkses;
                        break;
                    case "CanDeleteRkas":
                        HakAksesConstant.CanDeleteRkas = item.IdJenisHakAkses;
                        break;
                    case "CanApproveRkas":
                        HakAksesConstant.CanApproveRkas = item.IdJenisHakAkses;
                        break;
                }
            }

        }
        private void InitUnitSekolah(IDbConnection conn)
        {
            var data = conn.GetList<TbUnit>();
            UnitConstant.DictUnit = new Dictionary<int, string>();
            foreach (var item in data)
            {
                UnitConstant.DictUnit.Add(item.IdUnit, item.Nama);
                switch (item.NamaSingkat)
                {
                    case "TK":
                        UnitConstant.Tk = item.IdUnit;
                        break;
                    case "SD":
                        UnitConstant.Sd = item.IdUnit;
                        break;
                    case "SMP":
                        UnitConstant.Smp = item.IdUnit;
                        break;
                    case "SMA":
                        UnitConstant.Sma = item.IdUnit;
                        break;
                    case "TPQ":
                        UnitConstant.Tpq = item.IdUnit;
                        break;
                    case "DIR":
                        UnitConstant.DIR = item.IdUnit;
                        break;
                    case "PGD":
                        UnitConstant.PGD = item.IdUnit;
                        break;
                    case "PML":
                        UnitConstant.PML = item.IdUnit;
                        break;
                    case "ALQ":
                        UnitConstant.ALQ = item.IdUnit;
                        break;
                    case "PER":
                        UnitConstant.PER = item.IdUnit;
                        break;
                    case "PMG":
                        UnitConstant.PMG = item.IdUnit;
                        break;
                    case "PMB":
                        UnitConstant.PMB = item.IdUnit;
                        break;
                    case "MDC":
                        UnitConstant.MDC = item.IdUnit;
                        break;
                    case "KAM":
                        UnitConstant.KAM = item.IdUnit;
                        break;
                    case "DKM":
                        UnitConstant.DKM = item.IdUnit;
                        break;
                    case "ZIS":
                        UnitConstant.ZIS = item.IdUnit;
                        break;
                    case "AKT":
                        UnitConstant.AKT = item.IdUnit;
                        break;
                    case "FNC":
                        UnitConstant.FNC = item.IdUnit;
                        break;
                }
            }
        }
        private void InitJenisJabatan(IDbConnection conn)
        {
            var data = conn.GetList<TbJenisJabatan>();
            JenisJabatanPegawaiContant.Dict = new Dictionary<int, string>();
            foreach (var item in data)
            {
                JenisJabatanPegawaiContant.Dict.Add(item.IdJenisJabatan, item.Nama);
                switch (item.NamaSingkat)
                {
                    case "Yay":
                        JenisJabatanPegawaiContant.Yay = item.IdJenisJabatan;
                        break;
                    case "Dir":
                        JenisJabatanPegawaiContant.Dir = item.IdJenisJabatan;
                        break;
                    case "SekDir":
                        JenisJabatanPegawaiContant.SekDir = item.IdJenisJabatan;
                        break;
                    case "ManDik":
                        JenisJabatanPegawaiContant.ManDik = item.IdJenisJabatan;
                        break;
                    case "ManUm":
                        JenisJabatanPegawaiContant.ManUm = item.IdJenisJabatan;
                        break;
                    case "ManHum":
                        JenisJabatanPegawaiContant.ManHum = item.IdJenisJabatan;
                        break;
                    case "ManSos":
                        JenisJabatanPegawaiContant.ManSos = item.IdJenisJabatan;
                        break;
                    case "ManKeu":
                        JenisJabatanPegawaiContant.ManKeu = item.IdJenisJabatan;
                        break;
                    case "KaUnit":
                        JenisJabatanPegawaiContant.KaUnit = item.IdJenisJabatan;
                        break;
                    case "WakaUnit":
                        JenisJabatanPegawaiContant.WakaUnit = item.IdJenisJabatan;
                        break;
                    case "SekUnit":
                        JenisJabatanPegawaiContant.SekUnit = item.IdJenisJabatan;
                        break;
                    case "BendUnit":
                        JenisJabatanPegawaiContant.BendUnit = item.IdJenisJabatan;
                        break;
                    case "StafUnit":
                        JenisJabatanPegawaiContant.StafUnit = item.IdJenisJabatan;
                        break;
                    case "AdmDir":
                        JenisJabatanPegawaiContant.AdmDir = item.IdJenisJabatan;
                        break;
                    case "WaliKelas":
                        JenisJabatanPegawaiContant.WaliKelas = item.IdJenisJabatan;
                        break;
                    case "WaWaliKelas":
                        JenisJabatanPegawaiContant.WaWaliKelas = item.IdJenisJabatan;
                        break;
                }
            }
        }

        private void InitJenisAkunKeu(IDbConnection conn)
        {
            var data = conn.GetList<TbJenisAkun>();
            JenisAkunConstant.Dict = new Dictionary<int, string>();
            foreach (var item in data)
            {
                JenisAkunConstant.Dict.Add(item.IdJenisAkun, item.Nama);
                switch (item.Nama)
                {
                    case "Golongan":
                        JenisAkunConstant.Golongan = item.IdJenisAkun;
                        break;
                    case "Utama":
                        JenisAkunConstant.Utama = item.IdJenisAkun;
                        break;
                    case "Madya":
                        JenisAkunConstant.Madya = item.IdJenisAkun;
                        break;
                    case "Pratama":
                        JenisAkunConstant.Pratama = item.IdJenisAkun;
                        break;
                    //                    case "Buku Besar":
                    //                        JenisAkunConstant.BukuBesar = item.IdJenisAkun;
                    //                        break;
                    case "Detil":
                        JenisAkunConstant.Detil = item.IdJenisAkun;
                        break;
                    default:
                        break;
                }
            }
        }
        private void InitJenisTransaksiKeu(IDbConnection conn)
        {
            var data = conn.GetList<TbJenisTransaksi>();
            JenisTransaksiConstant.Dict = new Dictionary<int, string>();
            foreach (var item in data)
            {
                JenisTransaksiConstant.Dict.Add(item.IdJenisTransaksi, item.Nama);
                switch (item.Nama)
                {
                    case "Debet":
                        JenisTransaksiConstant.Debet = item.IdJenisTransaksi;
                        break;
                    case "Kredit":
                        JenisTransaksiConstant.Kredit = item.IdJenisTransaksi;
                        break;
                    default:
                        break;
                }
            }
        }
        private void InitStatusKeuangan(IDbConnection conn)
        {
            var dt0 = conn.GetList<TbJenisStatusRenpen>();
            JenisStatusRenPenConstant.Dict = new Dictionary<int, string>();
            foreach (var item in dt0)
            {
                JenisStatusRenPenConstant.Dict.Add(item.IdJenisStatusRenpen, item.Nama);
                switch (item.NamaSingkat)
                {
                    case "Initial":
                        JenisStatusRenPenConstant.Initial = item.IdJenisStatusRenpen;
                        break;
                    case "Baru":
                        JenisStatusRenPenConstant.Baru = item.IdJenisStatusRenpen;
                        break;
                    case "DisetujuiManKeu":
                        JenisStatusRenPenConstant.DisetujuiManKeu = item.IdJenisStatusRenpen;
                        break;
                    case "DisetujuiDirektur":
                        JenisStatusRenPenConstant.DisetujuiDirektur = item.IdJenisStatusRenpen;
                        break;
                    case "Disahkan":
                        JenisStatusRenPenConstant.Disahkan = item.IdJenisStatusRenpen;
                        break;
                    default:
                        break;
                }
            }
            var dt1 = conn.GetList<TbJenisStatusProkeg>();
            JenisStatusProgKegConstant.Dict = new Dictionary<int, string>();
            foreach (var item in dt1)
            {
                JenisStatusProgKegConstant.Dict.Add(item.IdJenisStatusProkeg, item.Nama);
                switch (item.NamaSingkat)
                {
                    case "Initial":
                        JenisStatusProgKegConstant.Initial = item.IdJenisStatusProkeg;
                        break;
                    case "Baru":
                        JenisStatusProgKegConstant.Baru = item.IdJenisStatusProkeg;
                        break;
                    case "DisetujuiKaUnit":
                        JenisStatusProgKegConstant.DisetujuiKaUnit = item.IdJenisStatusProkeg;
                        break;
                    case "DisetujuiManUnit":
                        JenisStatusProgKegConstant.DisetujuiManUnit = item.IdJenisStatusProkeg;
                        break;
                    case "DisetujuiManKeu":
                        JenisStatusProgKegConstant.DisetujuiManKeu = item.IdJenisStatusProkeg;
                        break;
                    case "DisetujuiDirektur":
                        JenisStatusProgKegConstant.DisetujuiDirektur = item.IdJenisStatusProkeg;
                        break;
                    case "Disahkan":
                        JenisStatusProgKegConstant.Disahkan = item.IdJenisStatusProkeg;
                        break;
                    default:
                        break;
                }
            }
            var dt2 = conn.GetList<TbJenisStatusPengajuan>();
            JenisStatusPengajuanConstant.Dict = new Dictionary<int, string>();
            foreach (var item in dt2)
            {
                JenisStatusPengajuanConstant.Dict.Add(item.IdJenisStatusPengajuan, item.Nama);
                switch (item.NamaSingkat)
                {
                    case "Baru":
                        JenisStatusPengajuanConstant.Baru = item.IdJenisStatusPengajuan;
                        break;
                    case "DisetujuiKaUnit":
                        JenisStatusPengajuanConstant.DisetujuiKaUnit = item.IdJenisStatusPengajuan;
                        break;
                    case "DisetujuiManUnit":
                        JenisStatusPengajuanConstant.DisetujuiManUnit = item.IdJenisStatusPengajuan;
                        break;
                    case "DisetujuiManKeu":
                        JenisStatusPengajuanConstant.DisetujuiManKeu = item.IdJenisStatusPengajuan;
                        break;
                    case "DisetujuiDirektur":
                        JenisStatusPengajuanConstant.DisetujuiDirektur = item.IdJenisStatusPengajuan;
                        break;
                    case "Direalisasikan":
                        JenisStatusPengajuanConstant.Direalisasikan = item.IdJenisStatusPengajuan;
                        break;
                    case "Dilaporkan":
                        JenisStatusPengajuanConstant.Dilaporkan = item.IdJenisStatusPengajuan;
                        break;
                    case "InitDilaporkan":
                        JenisStatusPengajuanConstant.InitDilaporkan = item.IdJenisStatusPengajuan;
                        break;
                    case "Dibukukan":
                        JenisStatusPengajuanConstant.Dibukukan = item.IdJenisStatusPengajuan;
                        break;
                    default:
                        break;
                }
            }
        }
        private void InitConstant()
        {
            try
            {
                using (var conn = DbConnection())
                {
                    InitApplSetting(conn);
                    InitHakAses(conn);
                    InitUnitSekolah(conn);
                    InitJenisJabatan(conn);
                    InitJenisAkunKeu(conn);
                    InitJenisTransaksiKeu(conn);
                    InitStatusKeuangan(conn);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public ImageFormat GetImageFormat(byte[] bytes)
        {
            // see http://www.mikekunz.com/image_file_header.html  
            var bmp = Encoding.ASCII.GetBytes("BM");     // BMP
            var gif = Encoding.ASCII.GetBytes("GIF");    // GIF
            var png = new byte[] { 137, 80, 78, 71 };    // PNG
            var tiff = new byte[] { 73, 73, 42 };         // TIFF
            var tiff2 = new byte[] { 77, 77, 42 };         // TIFF
            var jpeg = new byte[] { 255, 216, 255, 224 }; // jpeg
            var jpeg2 = new byte[] { 255, 216, 255, 225 }; // jpeg canon

            if (bmp.SequenceEqual(bytes.Take(bmp.Length)))
                return ImageFormat.bmp;

            if (gif.SequenceEqual(bytes.Take(gif.Length)))
                return ImageFormat.gif;

            if (png.SequenceEqual(bytes.Take(png.Length)))
                return ImageFormat.png;

            if (tiff.SequenceEqual(bytes.Take(tiff.Length)))
                return ImageFormat.tiff;

            if (tiff2.SequenceEqual(bytes.Take(tiff2.Length)))
                return ImageFormat.tiff;

            if (jpeg.SequenceEqual(bytes.Take(jpeg.Length)))
                return ImageFormat.jpeg;

            if (jpeg2.SequenceEqual(bytes.Take(jpeg2.Length)))
                return ImageFormat.jpeg;

            return ImageFormat.unknown;
        }
        public bool ValidateUsername(string Username)
        {
            if (string.IsNullOrEmpty(Username)
                || !sUserNameAllowedRegEx.IsMatch(Username)
                || sUserNameIllegalEndingRegEx.IsMatch(Username))
            {
                return false;
            }
            return true;
        }

        public bool ValidatePassword(string password, out string oMessage)
        {
            oMessage = string.Empty;
            var input = password;

            if (string.IsNullOrWhiteSpace(input))
            {
                throw new Exception("Password tidak boleh kosong");
            }

            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMiniMaxChars = new Regex(@".{8,15}");
            var hasLowerChar = new Regex(@"[a-z]+");
            //            var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");

            if (!hasLowerChar.IsMatch(input))
            {
                oMessage = "Password harus mempunyai minimal satu huruf kecil";
                return false;
            }
            else if (!hasUpperChar.IsMatch(input))
            {
                oMessage = "Password harus mempunyai minimal satu huruf besar";
                return false;
            }
            else if (input.Length < 8)
            {
                oMessage = "Password minimal 8 karakter";
                return false;
            }
            else if (!hasNumber.IsMatch(input))
            {
                oMessage = "Password harus mempunyai nilai numerik";
                return false;
            }
            else
            {
                return true; ;
            }
        }
        public bool ValidateEmail(string Email, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                var addr = new System.Net.Mail.MailAddress(Email);
                if (addr.Address != Email)
                {
                    oMessage = "alamat email salah<br>";
                    return false;
                }
                return true;
            }
            catch (Exception)
            {
                oMessage = "alamat email salah<br>";
                return false;

            }
        }
        public string CreateRandomPassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }
        public string GetErrorMessage(string MethodeName, Exception ex)
        {
            string errMessage = ex.Message;
            if (ex.InnerException != null)
            {
                errMessage = ex.InnerException.Message;
                if (ex.InnerException.InnerException != null)
                {
                    errMessage = ex.InnerException.InnerException.Message;
                    if (ex.InnerException.InnerException.InnerException != null)
                    {
                        errMessage = ex.InnerException.InnerException.InnerException.Message;
                    }
                }
            }
            var lineNumber = 0;
            const string lineSearch = ":line ";
            var index = ex.StackTrace.LastIndexOf(lineSearch);
            if (index != -1)
            {
                var lineNumberText = ex.StackTrace.Substring(index + lineSearch.Length);
                if (int.TryParse(lineNumberText, out lineNumber))
                {
                }
            }
            return MethodeName + ", line: " + lineNumber + Environment.NewLine + "Error Message: " + errMessage;
        }
        public string DecryptString(string cipherText)
        {
            var fullCipher = Convert.FromBase64String(cipherText);

            var iv = new byte[16];
            var cipher = new byte[16];

            Buffer.BlockCopy(fullCipher, 0, iv, 0, iv.Length);
            Buffer.BlockCopy(fullCipher, iv.Length, cipher, 0, iv.Length);
            var key = Encoding.UTF8.GetBytes(KeyString);

            using (var aesAlg = Aes.Create())
            {
                using (var decryptor = aesAlg.CreateDecryptor(key, iv))
                {
                    string result;
                    using (var msDecrypt = new MemoryStream(cipher))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                result = srDecrypt.ReadToEnd();
                            }
                        }
                    }

                    return result;
                }
            }
        }
        public string EncryptString(string text)
        {
            var key = Encoding.UTF8.GetBytes(KeyString);

            using (var aesAlg = Aes.Create())
            {
                using (var encryptor = aesAlg.CreateEncryptor(key, aesAlg.IV))
                {
                    using (var msEncrypt = new MemoryStream())
                    {
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(text);
                        }

                        var iv = aesAlg.IV;

                        var decryptedContent = msEncrypt.ToArray();

                        var result = new byte[iv.Length + decryptedContent.Length];

                        Buffer.BlockCopy(iv, 0, result, 0, iv.Length);
                        Buffer.BlockCopy(decryptedContent, 0, result, iv.Length, decryptedContent.Length);
                        string tempEncrypt = Convert.ToBase64String(result);
                        string tempDecrypt = DecryptString(tempEncrypt);
                        if (tempDecrypt == text)
                            return tempEncrypt;
                        return string.Empty;
                        //                        return Convert.ToBase64String(result);
                    }
                }
            }
        }
        public void InitApplSetting()
        {
            AppSetting appSetting = new AppSetting();
            appSetting.GeneratePath();
            InitConstant();

        }
        public IDbConnection DbConnection()
        {
            SimpleCRUD.SetDialect(SimpleCRUD.Dialect.PostgreSQL);
            return new NpgsqlConnection(AppSetting.ConnectionString);
        }
        public string SetNoHandphone(string NoHp)
        {
            NoHp = NoHp.Replace("+62", "");
            NoHp = NoHp.Replace(" ", "");
            NoHp = NoHp.Replace("-", "");
            NoHp = NoHp.Trim();
            if (string.IsNullOrEmpty(NoHp)) return "-";
            if (NoHp.IndexOf('/') > 0)
                NoHp = NoHp.Substring(0, NoHp.IndexOf("/"));
            if (NoHp.Substring(0, 1) != "0") NoHp = "0" + NoHp;
            return NoHp;
        }

        private double BulatkanRp(double Rp, double RoundTo)
        {
            return Math.Round(Rp / RoundTo, 0) * RoundTo;
        }
        public double BulatkanRpPuluh(double Rp)
        {
            return BulatkanRp(Rp, 10);
        }
        public double BulatkanRpRatus(double Rp)
        {
            return BulatkanRp(Rp, 100);
        }
        public double BulatkanRpRibu(double Rp)
        {
            return BulatkanRp(Rp, 1000);
        }
        public double BulatkanRpJuta(double Rp)
        {
            return BulatkanRp(Rp, 1000000);
        }
        public double BulatkanRpMilyar(double Rp)
        {
            return BulatkanRp(Rp, 1000000000);
        }
        public string Terbilang(decimal Rp)
        {
            string[] satuan = new string[10] { "nol", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan" };
            string[] belasan = new string[10] { "sepuluh", "sebelas", "dua belas", "tiga belas", "empat belas", "lima belas", "enam belas", "tujuh belas", "delapan belas", "sembilan belas" };
            string[] puluhan = new string[10] { "", "", "dua puluh", "tiga puluh", "empat puluh", "lima puluh", "enam puluh", "tujuh puluh", "delapan puluh", "sembilan puluh" };
            string[] ribuan = new string[5] { "", "ribu", "juta", "milyar", "triliyun" };
            decimal frac = Rp - decimal.Truncate(Rp);

            string strHasil;
            if (decimal.Compare(frac, 0.0m) != 0)
                strHasil = Terbilang(decimal.Round(frac * 100)) + " sen";
            else
                strHasil = "rupiah";
            string strTemp = Decimal.Truncate(Rp).ToString();
            for (int i = strTemp.Length; i > 0; i--)
            {
                int xDigit = Convert.ToInt32(strTemp.Substring(i - 1, 1));
                int xPosisi = (strTemp.Length - i) + 1;
                switch (xPosisi % 3)
                {
                    case 1:
                        bool allNull = false;
                        string tmpx;
                        if (i == 1)
                            tmpx = satuan[xDigit] + " ";
                        else if (strTemp.Substring(i - 2, 1) == "1")
                            tmpx = belasan[xDigit] + " ";
                        else if (xDigit > 0)
                            tmpx = satuan[xDigit] + " ";
                        else
                        {
                            allNull = true;
                            if (i > 1)
                                if (strTemp.Substring(i - 2, 1) != "0")
                                    allNull = false;
                            if (i > 2)
                                if (strTemp.Substring(i - 3, 1) != "0")
                                    allNull = false;
                            tmpx = "";
                        }

                        if ((!allNull) && (xPosisi > 1))
                            if ((strTemp.Length == 4) && (strTemp.Substring(0, 1) == "1"))
                                tmpx = "se" + ribuan[(int)Decimal.Round(xPosisi / 3m)] + " ";
                            else
                                tmpx = tmpx + ribuan[(int)Decimal.Round(xPosisi / 3)] + " ";
                        strHasil = tmpx + strHasil;
                        break;
                    case 2:
                        if (xDigit > 0)
                            strHasil = puluhan[xDigit] + " " + strHasil;
                        break;
                    case 0:
                        if (xDigit > 0)
                            if (xDigit == 1)
                                strHasil = "seratus " + strHasil;
                            else
                                strHasil = satuan[xDigit] + " ratus " + strHasil;
                        break;
                }
            }
            strHasil = strHasil.Trim().ToLower();
            if (strHasil.Length > 0)
            {
                strHasil = strHasil.Substring(0, 1).ToUpper() +
                  strHasil.Substring(1, strHasil.Length - 1);
            }
            return strHasil;
        }
        public string SetStrRupiah(double Rp)
        {
            CultureInfo cultureInfo = new CultureInfo("id-ID");
            string rp = string.Format(cultureInfo, "{0,14:0,0.#0}", Rp);
            rp = rp.PadLeft(14, ' ');
            rp = "Rp. " + rp;
            return rp;
        }
        public bool DictCheck(Dictionary<int, string> dics, int Id)
        {
            var cek2 = from a in dics
                       where a.Key == Id
                       select a;
            if (cek2 == null || cek2.Count() == 0)
                return false;
            return true;
        }
        public bool DictCheck(Dictionary<string, string> dics, string Kd)
        {
            var cek2 = from a in dics
                       where a.Key == Kd
                       select a;
            if (cek2 == null || cek2.Count() == 0)
                return false;
            return true;
        }
        public string SetAlamat(string AlTunjuk, string AlAlamat, string AlBlokNo, string AlRw, string AlRt, string Desa, string Kec, string Kab, string Prov)
        {
            string ret = AlTunjuk;
            ret += " " + AlAlamat;
            if (string.IsNullOrEmpty(AlBlokNo)) AlBlokNo = "-";
            if (string.IsNullOrEmpty(AlRw)) AlRw = "-";
            if (string.IsNullOrEmpty(AlRt)) AlRt = "-";
            ret += " No. " + AlBlokNo;
            ret += " RT/RW: " + AlRt + "/" + AlRw;
            if (!string.IsNullOrEmpty(Desa))
                ret += " Desa/Kel. " + Desa;
            if (!string.IsNullOrEmpty(Kec))
                ret += " Kec. " + Kec;
            if (!string.IsNullOrEmpty(Kab))
                ret += " Kab/Kota " + Kab;
            if (!string.IsNullOrEmpty(Prov))
                ret += " Prov. " + Prov;
            return ret;
        }
        public string PushNotifMobile(string Token, string Judul, string Deskripsi)
        {
            var client = new RestClient("https://onesignal.com/api/v1/notifications");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            var json = new
            {
                app_id = "8ff8cf94-4642-45ae-9f77-e80aca03e548",
                headings = new
                {
                    id = Judul
                },
                contents = new
                {
                    id = Deskripsi
                },
                content_available = true,
                priority = 10,
                large_icon = "https://user-images.githubusercontent.com/49223890/126100969-767c639e-e06b-403e-83b0-f0e2fee942aa.png",
                // small_icon = "https://www.google.co.in/images/branding/googleg/1x/googleg_standard_color_128dp.png",
                // big_picture = "https://cdn-images-1.medium.com/max/300/1*7xHdCFeYfD8zrIivMiQcCQ.png",
                android_channel_id = "b45c87a5-1440-43c8-a937-b8a80d98b868",
                android_sound = "notif1",
                include_player_ids = new List<string> { Token }
            };
            string parameter = Newtonsoft.Json.JsonConvert.SerializeObject(json);
            request.AddHeader("Content-Type", "application/json; charset=utf-8");
            request.AddHeader("Authorization", "Basic MjI0NjViNGItZmE5MS00NDIzLWIwNTUtOTdiMThlMDEwN2Iz");
            request.AddParameter("application/json; charset=utf-8", parameter, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return null;
        }
        public string ToSlug(string phrase)
        {
            string str = RemoveAccent(phrase).ToLower();

            str = Regex.Replace(str, @"[^a-z0-9\s-]", ""); // invalid chars          
            str = Regex.Replace(str, @"\s+", " ").Trim(); // convert multiple spaces into one space  
            str = str.Substring(0, str.Length <= 255 ? str.Length : 255).Trim(); // cut and trim it  
            str = Regex.Replace(str, @"\s", "-"); // hyphens  

            return str;
        }

        static string RemoveAccent(string txt)
        {
            //byte[] bytes = Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            byte[] bytes = Encoding.Unicode.GetBytes(txt);
            return Encoding.ASCII.GetString(bytes);
        }
        public string SetStringNull(string text)
        {
            return string.IsNullOrEmpty(text) ? "-" : text;
        }
        public int SetIntegerNUll(int? number)
        {
            return number == null ? 0 : (int)number;
        }
        public double SetNumberNull(double? number)
        {
            return number == null ? 0 : (double)number;
        }
    }
}
