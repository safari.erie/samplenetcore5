﻿using Dapper;
using Swg.Entities.Lmg;
using Swg.Entities.Ref;
using Swg.Models;
using Swg.Sch.Shared.Constants;
using Swg.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace Swg.Sch.Ref
{
    public class RefService
    {
        private readonly ICommonService commonService;
        public RefService(ICommonService CommonService )
        {
            commonService = CommonService;
        }
        private void InitPathFiles()
        {
            ConstPathFilesConstant.PathImageSekolahKegiatan = Path.Combine(AppSetting.PathFileUpload, @"sekolah", @"kegiatan");
            ConstPathFilesConstant.RequestImageSekolahKegiatan = "/File/Sekolah/Kegiatan";

            ConstPathFilesConstant.PathImageUnitKegiatan = Path.Combine(AppSetting.PathFileUpload, @"unit", @"kegiatan");
            ConstPathFilesConstant.RequestImageUnitKegiatan = "/File/Unit/Kegiatan";

            ConstPathFilesConstant.PathImageUnitGaleri = Path.Combine(AppSetting.PathFileUpload, @"unit", @"galeri");
            ConstPathFilesConstant.RequestImageUnitGaleri = "/File/Unit/Galeri";

            ConstPathFilesConstant.PathImageSekolahGaleri = Path.Combine(AppSetting.PathFileUpload, @"sekolah", @"galeri");
            ConstPathFilesConstant.RequestImageSekolahGaleri = "/File/Sekolah/Galeri";

            ConstPathFilesConstant.PathImageFotoProfil = Path.Combine(AppSetting.PathFileUserProfile);
            ConstPathFilesConstant.RequestImageFotoProfil = "/FotoProfile";

            ConstPathFilesConstant.PathImageLogoUnit = Path.Combine(AppSetting.PathFileUpload, @"unit");
            ConstPathFilesConstant.RequestImageLogoUnit = "/File/Unit";

            //ConstPathFilesConstant.PathImageLogoSekolah = Path.Combine(AppSetting.PathFileUpload, @"sekolah");
            //ConstPathFilesConstant.RequestImageLogoSekolah = "/File/Sekolah";

            ConstPathFilesConstant.PathWebSekolah = Path.Combine(AppSetting.PathFileUpload, @"sekolah");
            ConstPathFilesConstant.RequestWebSekolah = "/File/Sekolah";

            //ConstPathFilesConstant.PathImageFotoWebSekolah = Path.Combine(AppSetting.PathFileUpload, @"sekolah");
            //ConstPathFilesConstant.RequestImageFotoWebSekolah = "/File/Sekolah";

            ConstPathFilesConstant.PathFilePpdbPrestasi = Path.Combine(AppSetting.PathFileUpload, @"ppdb", @"prestasi");
            ConstPathFilesConstant.RequestFilePpdbPrestasi = "/FilePrestasi";

            ConstPathFilesConstant.PathFilePpdbBerkas = Path.Combine(AppSetting.PathFileUpload, @"ppdb", @"berkas");
            ConstPathFilesConstant.RequestFilePpdbBerkas = "/FileBerkas";

            ConstPathFilesConstant.PathFilePpdbCicil = Path.Combine(AppSetting.PathFileUpload, @"ppdb", @"cicil");
            ConstPathFilesConstant.RequestFilePpdbCicil = "/FileCicil";

            ConstPathFilesConstant.PathFileRaport = Path.Combine(AppSetting.PathFileUpload, @"unit", @"raport");
            ConstPathFilesConstant.RequestFileRaport = "/File/Raport";

            ConstPathFilesConstant.PathFileElearning = Path.Combine(AppSetting.PathFileUpload, @"elearning", @"berkas");
            ConstPathFilesConstant.RequestFileElearning = "/File/ELearning";

            ConstPathFilesConstant.PathFileAppl = Path.Combine(AppSetting.PathFileUpload, @"Appl");
            ConstPathFilesConstant.RequestFileAppl = "/File/Appl";


            if (!Directory.Exists(ConstPathFilesConstant.PathImageSekolahKegiatan))
                Directory.CreateDirectory(ConstPathFilesConstant.PathImageSekolahKegiatan);

            if (!Directory.Exists(ConstPathFilesConstant.PathImageUnitKegiatan))
                Directory.CreateDirectory(ConstPathFilesConstant.PathImageUnitKegiatan);

            if (!Directory.Exists(ConstPathFilesConstant.PathImageUnitGaleri))
                Directory.CreateDirectory(ConstPathFilesConstant.PathImageUnitGaleri);

            if (!Directory.Exists(ConstPathFilesConstant.PathImageSekolahGaleri))
                Directory.CreateDirectory(ConstPathFilesConstant.PathImageSekolahGaleri);

            if (!Directory.Exists(ConstPathFilesConstant.PathImageFotoProfil))
                Directory.CreateDirectory(ConstPathFilesConstant.PathImageFotoProfil);

            if (!Directory.Exists(ConstPathFilesConstant.PathImageLogoUnit))
                Directory.CreateDirectory(ConstPathFilesConstant.PathImageLogoUnit);

            if (!Directory.Exists(ConstPathFilesConstant.PathWebSekolah))
                Directory.CreateDirectory(ConstPathFilesConstant.PathWebSekolah);

            if (!Directory.Exists(ConstPathFilesConstant.PathFilePpdbPrestasi))
                Directory.CreateDirectory(ConstPathFilesConstant.PathFilePpdbPrestasi);

            if (!Directory.Exists(ConstPathFilesConstant.PathFilePpdbBerkas))
                Directory.CreateDirectory(ConstPathFilesConstant.PathFilePpdbBerkas);

            if (!Directory.Exists(ConstPathFilesConstant.PathFilePpdbCicil))
                Directory.CreateDirectory(ConstPathFilesConstant.PathFilePpdbCicil);

            if (!Directory.Exists(ConstPathFilesConstant.PathFileRaport))
                Directory.CreateDirectory(ConstPathFilesConstant.PathFileRaport);

            if (!Directory.Exists(ConstPathFilesConstant.PathFileElearning))
                Directory.CreateDirectory(ConstPathFilesConstant.PathFileElearning);

            if (!Directory.Exists(ConstPathFilesConstant.PathFileAppl))
                Directory.CreateDirectory(ConstPathFilesConstant.PathFileAppl);
        }

        public void InitConstant()
        {
            InitPathFiles();
        }

    }

}
