using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Swg.Models;
using Swg.Sch.Services;
using Swg.Sch.Shared.Interfaces;
using Swg.Services;

namespace Swg.Sch.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddControllersWithViews().AddJsonOptions(o =>
            {
                o.JsonSerializerOptions.PropertyNamingPolicy = null;
                o.JsonSerializerOptions.DictionaryKeyPolicy = null;
            });
            ///services.AddHttpsRedirection(opt => opt.HttpsPort = 443);
            var appSettingsSection = Configuration.GetSection("AppSettings");

            services.Configure<AppSettingModel>(appSettingsSection);
            var appSettings = appSettingsSection.Get<AppSettingModel>();
            AppSetting.Secret = appSettings.Secret;
            AppSetting.ConnectionString = appSettings.ConnectionString;

            AppSetting.PathApplFile = Path.Combine(appSettings.BasePath, appSettings.PathApplFile);
            AppSetting.PathApplUser = Path.Combine(appSettings.BasePath, appSettings.PathApplUser);
            AppSetting.PathApplImage = Path.Combine(appSettings.BasePath, appSettings.PathApplImage);
            AppSetting.PathApplVideo = Path.Combine(appSettings.BasePath, appSettings.PathApplVideo);
            AppSetting.PathSiswaRapor = Path.Combine(appSettings.BasePath, appSettings.PathSiswaRapor);
            AppSetting.PathSiswaFoto = Path.Combine(appSettings.BasePath, appSettings.PathSiswaFoto);
            AppSetting.PathKegiatanUnit = Path.Combine(appSettings.BasePath, appSettings.PathKegiatanUnit);
            AppSetting.PathGaleriUnit = Path.Combine(appSettings.BasePath, appSettings.PathGaleriUnit);
            AppSetting.PathKegiatanSekolah = Path.Combine(appSettings.BasePath, appSettings.PathKegiatanSekolah);
            AppSetting.PathGaleriSekolah = Path.Combine(appSettings.BasePath, appSettings.PathGaleriSekolah);
            AppSetting.PathPpdbPrestasi = Path.Combine(appSettings.BasePath, appSettings.PathPpdbPrestasi);
            AppSetting.PathPpdbBerkas = Path.Combine(appSettings.BasePath, appSettings.PathPpdbBerkas);
            AppSetting.PathPpdbCicil = Path.Combine(appSettings.BasePath, appSettings.PathPpdbCicil);
            AppSetting.PathElearning = Path.Combine(appSettings.BasePath, appSettings.PathElearning);

            services.AddScoped<IUserAppService, UserAppService>();
            services.AddScoped<ICommonService, CommonService>();
            services.AddScoped<IDateTimeService, DateTimeService>();
            services.AddScoped<IBankService, BankService>();
            services.AddScoped<ISchService, SchService>();
            services.AddScoped<ISiswaService, SiswaService>();
            services.AddScoped<IEmailService, EmailService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathApplFile),
                RequestPath = new PathString(AppSetting.PathUrlApplFile)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathApplUser),
                RequestPath = new PathString(AppSetting.PathUrlApplUser)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathApplImage),
                RequestPath = new PathString(AppSetting.PathUrlApplImage)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathApplVideo),
                RequestPath = new PathString(AppSetting.PathUrlApplVideo)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathSiswaRapor),
                RequestPath = new PathString(AppSetting.PathUrlSiswaRapor)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathSiswaFoto),
                RequestPath = new PathString(AppSetting.PathUrlSiswaFoto)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathKegiatanUnit),
                RequestPath = new PathString(AppSetting.PathUrlKegiatanUnit)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathGaleriUnit),
                RequestPath = new PathString(AppSetting.PathUrlGaleriUnit)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathKegiatanSekolah),
                RequestPath = new PathString(AppSetting.PathUrlKegiatanSekolah)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathGaleriSekolah),
                RequestPath = new PathString(AppSetting.PathUrlGaleriSekolah)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathPpdbPrestasi),
                RequestPath = new PathString(AppSetting.PathUrlPpdbPrestasi)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathPpdbBerkas),
                RequestPath = new PathString(AppSetting.PathUrlPpdbBerkas)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathPpdbCicil),
                RequestPath = new PathString(AppSetting.PathUrlPpdbCicil)
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppSetting.PathElearning),
                RequestPath = new PathString(AppSetting.PathUrlElearning)
            });
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
