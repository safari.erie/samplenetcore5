﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Swg.Sch.Api.Models
{
    public class ResponseModel<T>
    {
        public string ResponseStatus { get; set; }
        public string ResponseMessage { get; set; }
        public T Result { get; set; }
    }
}
