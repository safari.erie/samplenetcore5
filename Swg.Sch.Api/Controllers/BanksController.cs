﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using Microsoft.AspNetCore.Mvc;
using Swg.Sch.Api.Models;
using Swg.Sch.Shared.Interfaces;
using Swg.Sch.Shared.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Swg.Sch.Api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class BanksController : ControllerBase
    {
        private readonly IBankService bankService;
        public BanksController(IBankService BankService)
        {
            bankService = BankService;
        }

        private string LogPartnerRequestx(
            string KdPartner,
            string MethodName,
            string ResponseStatus,
            string ResponseMessage,
            string ResponseResult,
            string ParamName,
            string ParamValue,
            string IpAddress
        )
        {
            PartnerRequestModel data = new PartnerRequestModel
            {
                KdPartner = KdPartner,
                MethodName = MethodName,
                ResponseStatus = ResponseStatus,
                ResponseMessage = ResponseMessage,
                ResponseResult = ResponseResult,
                ParamName = ParamName,
                ParamValue = ParamValue,
                IpAddress = IpAddress
            };
            var ret = bankService.LogPartnerRequest(data);
            return ret;
        }
        private IPAddress GetIp()
        {
            IPAddress ip = Request.HttpContext.Connection.RemoteIpAddress;
            return ip;
        }

        [HttpGet("GetInfoSppSiswa", Name = "GetInfoSppSiswa")]
        public ResponseModel<PartnerSppModel> GetInfoSppSiswa(string PartnerId, string Nis)
        {
            PartnerSppModel data = new PartnerSppModel();
            var ret = bankService.GetInfoSppSiswa(PartnerId, Nis, out data);
            ResponseModel<PartnerSppModel> json = new ResponseModel<PartnerSppModel>
            {
                ResponseStatus = string.IsNullOrEmpty(ret) ? "SUKSES" : "GAGAL",
                ResponseMessage = ret,
                Result = data
            };
            IPAddress ip = Request.HttpContext.Connection.RemoteIpAddress;
            var result = Newtonsoft.Json.JsonConvert.SerializeObject(json.Result);
            LogPartnerRequestx(
                PartnerId,
                "[GET] GetInfoSppSiswa",
                json.ResponseStatus,
                json.ResponseMessage,
                result,
                "PartnerId | Nis ",
                string.Format("{0} | {1}", PartnerId, Nis),
                ip.ToString()
            );
            return json;
        }

        [HttpPost("SetLunasSppSiswa", Name = "SetLunasSppSiswa")]
        public ResponseModel<PartnerSppTempModel> SetLunasSppSiswa(string PartnerId, string Nis, double Rp)
        {
            var ret = bankService.SetLunasSppSiswa(PartnerId, Nis, Rp, out string RefNo);
            ResponseModel<PartnerSppTempModel> json = new ResponseModel<PartnerSppTempModel>
            {
                ResponseStatus = string.IsNullOrEmpty(ret) ? "SUKSES" : "GAGAL",
                ResponseMessage = ret,
                Result = new PartnerSppTempModel
                {
                    RefNo = RefNo
                }
            };
            IPAddress ip = Request.HttpContext.Connection.RemoteIpAddress;
            var result = Newtonsoft.Json.JsonConvert.SerializeObject(json.Result);
            LogPartnerRequestx(
                PartnerId,
                "[POST] SetLunasSppSiswa",
                json.ResponseStatus,
                json.ResponseMessage,
                result,
                "PartnerId | Nis | Rp",
                string.Format("{0} | {1} | {2}", PartnerId, Nis, Rp),
                ip.ToString()
            );
            bankService.LogPartnerPayment(
                Nis,
                "SPP",
                Rp,
                json.ResponseMessage
            );
            return json;
        }

        [HttpPost("SetBatalSppSiswa", Name = "SetBatalSppSiswa")]
        public ResponseModel<PartnerSppTempModel> SetBatalSppSiswa(string PartnerId, string Nis, double Rp, string RefNo)
        {
            var ret = bankService.SetBatalSppSiswa(PartnerId, Nis, Rp, RefNo);
            ResponseModel<PartnerSppTempModel> json = new ResponseModel<PartnerSppTempModel>
            {
                ResponseStatus = string.IsNullOrEmpty(ret) ? "SUKSES" : "GAGAL",
                ResponseMessage = ret,
                Result = new PartnerSppTempModel
                {
                    RefNo = RefNo
                }
            };
            IPAddress ip = Request.HttpContext.Connection.RemoteIpAddress;
            var result = Newtonsoft.Json.JsonConvert.SerializeObject(json.Result);
            LogPartnerRequestx(
                PartnerId,
                "[POST] SetBatalSppSiswa",
                json.ResponseStatus,
                json.ResponseMessage,
                result,
                "PartnerId | Nis | Rp | RefNo",
                string.Format("{0} | {1} | {2} | {3}", PartnerId, Nis, Rp, RefNo),
                ip.ToString()
            );
            return json;
        }

        [HttpGet("GetInfoTagihanPpdb", Name = "GetInfoTagihanPpdb")]
        public ResponseModel<PartnerPpdbModel> GetInfoTagihanPpdb(string PartnerId, string NoVa)
        {
            PartnerPpdbModel data = new PartnerPpdbModel();
            var ret = bankService.GetInfoTagihanPpdb(PartnerId, NoVa, out data);
            ResponseModel<PartnerPpdbModel> json = new ResponseModel<PartnerPpdbModel>
            {
                ResponseStatus = string.IsNullOrEmpty(ret) ? "SUKSES" : "GAGAL",
                ResponseMessage = ret,
                Result = data
            };
            IPAddress ip = Request.HttpContext.Connection.RemoteIpAddress;
            var result = Newtonsoft.Json.JsonConvert.SerializeObject(json.Result);
            LogPartnerRequestx(
                PartnerId,
                "[GET] GetInfoTagihanPpdb",
                json.ResponseStatus,
                json.ResponseMessage,
                result,
                "PartnerId | NoVa ",
                string.Format("{0} | {1} ", PartnerId, NoVa),
                ip.ToString()
            );
            return json;
        }

        [HttpPost("SetLunasTagihanPpdb", Name = "SetLunasTagihanPpdb")]
        public ResponseModel<PartnerPpdbTempModel> SetLunasTagihanPpdb(string PartnerId, string NoVa, double Rp)
        {
            var ret = bankService.SetLunasTagihanPpdb(PartnerId, NoVa, Rp, out string RefNo);
            ResponseModel<PartnerPpdbTempModel> json = new ResponseModel<PartnerPpdbTempModel>
            {
                ResponseStatus = string.IsNullOrEmpty(ret) ? "SUKSES" : "GAGAL",
                ResponseMessage = ret,
                Result = new PartnerPpdbTempModel
                {
                    RefNo = RefNo
                }
            };
            string ipAddress = string.Empty;
            IPAddress ip = Request.HttpContext.Connection.RemoteIpAddress;
            var result = Newtonsoft.Json.JsonConvert.SerializeObject(json.Result);
            LogPartnerRequestx(
                PartnerId,
                "[POST] SetLunasTagihanPpdb",
                json.ResponseStatus,
                json.ResponseMessage,
                result,
                "PartnerId | NoVa | Rp",
                string.Format("{0} | {1} | {2}", PartnerId, NoVa, Rp),
                ip.ToString()
            );
            bankService.LogPartnerPayment(
                NoVa,
                "PPDB-DAFTAR-BARU",
                Rp,
                json.ResponseMessage
            );
            return json;

        }

        [HttpPost("SetBatalTagihanPpdb", Name = "SetBatalTagihanPpdb")]
        public ResponseModel<PartnerPpdbTempModel> SetBatalTagihanPpdb(string PartnerId, string NoVa, double Rp, string RefNo)
        {
            var ret = bankService.SetBatalTagihanPpdb(PartnerId, NoVa, Rp, RefNo);
            ResponseModel<PartnerPpdbTempModel> json = new ResponseModel<PartnerPpdbTempModel>
            {
                ResponseStatus = string.IsNullOrEmpty(ret) ? "SUKSES" : "GAGAL",
                ResponseMessage = ret,
                Result = new PartnerPpdbTempModel
                {
                    RefNo = RefNo
                }
            };
            IPAddress ip = Request.HttpContext.Connection.RemoteIpAddress;
            var result = Newtonsoft.Json.JsonConvert.SerializeObject(json.Result);
            LogPartnerRequestx(
                PartnerId,
                "[POST] SetBatalTagihanPpdb",
                json.ResponseStatus,
                json.ResponseMessage,
                result,
                "PartnerId | NoVa | Rp",
                string.Format("{0} | {1} | {2}", PartnerId, NoVa, Rp),
                ip.ToString()
            );
            return json;
        }

        [HttpGet("GetInfoTagihanSiswaBaru", Name = "GetInfoTagihanSiswaBaru")]
        public ResponseModel<PpdbBiayaDaftarModel> GetInfoTagihanSiswaBaru(string PartnerId, string NoVa)
        {
            PpdbBiayaDaftarModel data = new PpdbBiayaDaftarModel();
            var ret = bankService.GetInfoTagihanSiswaBaru(PartnerId, NoVa, out data);
            ResponseModel<PpdbBiayaDaftarModel> json = new ResponseModel<PpdbBiayaDaftarModel>
            {
                ResponseStatus = string.IsNullOrEmpty(ret) ? "SUKSES" : "GAGAL",
                ResponseMessage = ret,
                Result = data
            };
            IPAddress ip = Request.HttpContext.Connection.RemoteIpAddress;
            var result = Newtonsoft.Json.JsonConvert.SerializeObject(json.Result);
            LogPartnerRequestx(
                PartnerId,
                "[GET] GetInfoTagihanSiswaBaru",
                json.ResponseStatus,
                json.ResponseMessage,
                result,
                "PartnerId | NoVa",
                string.Format("{0} | {1}", PartnerId, NoVa),
                ip.ToString()
            );
            return json;
        }

        [HttpPost("SetLunasTagihanSiswaBaru", Name = "SetLunasTagihanSiswaBaru")]
        public ResponseModel<PartnerPpdbTempModel> SetLunasTagihanSiswaBaru(string PartnerId, string NoVa, double Rp)
        {
            var ret = bankService.SetLunasTagihanSiswaBaru(PartnerId, NoVa, Rp, out string RefNo);
            ResponseModel<PartnerPpdbTempModel> json = new ResponseModel<PartnerPpdbTempModel>
            {
                ResponseStatus = string.IsNullOrEmpty(ret) ? "SUKSES" : "GAGAL",
                ResponseMessage = ret,
                Result = new PartnerPpdbTempModel
                {
                    RefNo = RefNo
                }
            };
            IPAddress ip = Request.HttpContext.Connection.RemoteIpAddress;
            var result = Newtonsoft.Json.JsonConvert.SerializeObject(json.Result);
            LogPartnerRequestx(
                PartnerId,
                "[POST] SetLunasTagihanSiswaBaru",
                json.ResponseStatus,
                json.ResponseMessage,
                result,
                "PartnerId | NoVa | Rp",
                string.Format("{0} | {1} | {2}", PartnerId, NoVa, Rp),
                ip.ToString()
            );
            bankService.LogPartnerPayment(
                NoVa,
                "PPDB-DAFTAR-ULANG",
                Rp,
                json.ResponseMessage
            );
            return json;
        }

        [HttpPost("SetBatalTagihanSiswaBaru", Name = "SetBatalTagihanSiswaBaru")]
        public ResponseModel<PartnerPpdbTempModel> SetBatalTagihanSiswaBaru(string PartnerId, string NoVa, double Rp, string RefNo)
        {
            var ret = bankService.SetBatalTagihanSiswaBaru(PartnerId, NoVa, Rp, RefNo);
            ResponseModel<PartnerPpdbTempModel> json = new ResponseModel<PartnerPpdbTempModel>
            {
                ResponseStatus = string.IsNullOrEmpty(ret) ? "SUKSES" : "GAGAL",
                ResponseMessage = ret,
                Result = new PartnerPpdbTempModel
                {
                    RefNo = RefNo
                }
            };
            IPAddress ip = Request.HttpContext.Connection.RemoteIpAddress;
            var result = Newtonsoft.Json.JsonConvert.SerializeObject(json.Result);
            LogPartnerRequestx(
                PartnerId,
                "[POST] SetBatalTagihanSiswaBaru",
                json.ResponseStatus,
                json.ResponseMessage,
                result,
                "PartnerId | NoVa | Rp | RefNo",
                string.Format("{0} | {1} | {2} | {3}", PartnerId, NoVa, Rp, RefNo),
                ip.ToString()
            );
            return json;
        }

        [HttpPost("SignIn", Name = "SignIn")]
        public ResponseModel<string> SignIn(string PartnerId, string Password)
        {
            var ret = bankService.PartnerLogin(PartnerId, Password);
            ResponseModel<string> json = new ResponseModel<string>
            {
                ResponseStatus = string.IsNullOrEmpty(ret) ? "SUKSES" : "GAGAL",
                ResponseMessage = ret,
                Result = ""
            };
            IPAddress ip = Request.HttpContext.Connection.RemoteIpAddress;
            var result = Newtonsoft.Json.JsonConvert.SerializeObject(json.Result);
            LogPartnerRequestx(
                PartnerId,
                "[POST] SignIn",
                json.ResponseStatus,
                json.ResponseMessage,
                result,
                "PartnerId | Password",
                string.Format("{0} | {1} ", PartnerId, Password),
                ip.ToString()
            );
            return json;
        }

        [HttpPost("SignOut", Name = "SignOut")]
        public ResponseModel<string> SignOut(string PartnerId)
        {
            var ret = bankService.PartnerLogout(PartnerId);
            ResponseModel<string> json = new ResponseModel<string>
            {
                ResponseStatus = string.IsNullOrEmpty(ret) ? "SUKSES" : "GAGAL",
                ResponseMessage = ret,
                Result = ""
            };
            IPAddress ip = Request.HttpContext.Connection.RemoteIpAddress;
            var result = Newtonsoft.Json.JsonConvert.SerializeObject(json.Result);
            LogPartnerRequestx(
                PartnerId,
                "[POST] SignOut",
                json.ResponseStatus,
                json.ResponseMessage,
                result,
                "PartnerId ",
                string.Format("{0}", PartnerId),
                ip.ToString()
            );
            return json;
        }

        [HttpPost("ChangePassword", Name = "ChangePassword")]
        public ResponseModel<string> ChangePassword(string PartnerId, string OldPassword, string NewPassword, string ConfirmPassword)
        {
            var ret = bankService.PartnerChangePassword(PartnerId, OldPassword, NewPassword, ConfirmPassword);
            return new ResponseModel<string>
            {
                ResponseStatus = string.IsNullOrEmpty(ret) ? "SUKSES" : "GAGAL",
                ResponseMessage = ret,
                Result = ""
            };
        }
    }
}
