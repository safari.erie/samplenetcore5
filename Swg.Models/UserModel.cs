﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Models
{
    public class UserModel : UserAddModel
    {
        public int IdUser { get; set; }
        public string LastLogin { get; set; }
        public string FullName { get; set; }
        public string StrStatus { get; set; }
    }
    public class UserAddModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string ProfileFile { get; set; }
        public List<RoleModel> Roles { get; set; }
    }
    public class UserRoleModel
    {
        public int IdUser { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string ProfileFile { get; set; }
        public int IdRole { get; set; }
        public string RoleName { get; set; }
    }
}
