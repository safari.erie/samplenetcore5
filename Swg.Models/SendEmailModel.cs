﻿using System;
using System.Collections.Generic;

namespace Swg.Models
{
    public class SendEmailModel
    {
        public List<string> To { get; set; }
        public List<string> Cc { get; set; }
        public string Title { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public List<string> Attachments { get; set; }
    }
}
