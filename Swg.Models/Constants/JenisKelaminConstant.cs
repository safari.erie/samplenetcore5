﻿using System;
using System.Collections.Generic;

namespace Swg.Models.Constants
{
    public class JenisKelaminConstant
    {
		public static Dictionary<string, string> Dict = new Dictionary<string, string> {
		{
			"L",
			"Laki-laki"
		},
		{
			"P",
			"Perempuan"
		}
	};

		public const string Lakilaki = "L";

		public const string Perempuan = "P";
	}
}
