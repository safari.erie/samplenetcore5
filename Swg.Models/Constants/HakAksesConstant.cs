﻿using System;
using System.Collections.Generic;

namespace Swg.Models.Constants
{
    public class HakAksesConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>();
        //baca informasi gaji pegawai
        public static int CanReadGajiGT;
        public static int CanReadGajiGTT;
        public static int CanReadGajiPot;
        public static int CanReadGajiTam;
        public static int CanReadGajiTot;
        public static int CanReadGajiAll;

        public static int CanReadKonten;
        public static int CanCreateKonten;
        public static int CanApproveKonten;
        public static int CanPublishKonten;
        public static int CanRejectKonten;
        public static int CanDeleteKonten;
        public static int CanEditKonten;

        public static int CanAddRkas;
        public static int CanEditRkas;
        public static int CanDeleteRkas;
        public static int CanApproveRkas;
    }
}
