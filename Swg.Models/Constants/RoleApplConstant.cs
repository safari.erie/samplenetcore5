﻿using System;
using System.Collections.Generic;

namespace Swg.Models.Constants
{

    public class RoleApplConstant
    {
        public static Dictionary<int, string> Dic = new Dictionary<int, string>()
        {
            {Admin,"Administrator" },
            {Admin2,"Administrator2" },
            {KetuaDkm,"Ketua DKM" },
            {BendUmum,"Bendahara Umum" },
            {BendMasjid,"Bendahara Masjid" },
            {BendSos,"Bendahara Sosial" },
            {BendOps,"Bendahara Operasional" },
            {StafDkm,"Staf DKM" }
        };

        public const int Admin = 1;
        public const int Admin2 = 3;
        public const int KetuaDkm = 10;
        public const int BendUmum = 20;
        public const int BendMasjid = 21;
        public const int BendSos = 22;
        public const int BendOps = 23;
        public const int StafDkm = 90;
    }
}

//INSERT INTO public.tb_role(id_role, role_name) VALUES(10, 'Ketua DKM');
//INSERT INTO public.tb_role(id_role, role_name) VALUES(20, 'Bendahara Umum');
//INSERT INTO public.tb_role(id_role, role_name) VALUES(21, 'Bendahara Pembangunan Masjid');
//INSERT INTO public.tb_role(id_role, role_name) VALUES(22, 'Bendahara Sosial');
//INSERT INTO public.tb_role(id_role, role_name) VALUES(23, 'Bendahara Operasional');
//INSERT INTO public.tb_role(id_role, role_name) VALUES(90, 'Staf DKM');
//