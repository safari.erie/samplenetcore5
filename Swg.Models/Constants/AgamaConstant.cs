﻿using System;
using System.Collections.Generic;

namespace Swg.Models.Constants
{
    public class AgamaConstant
    {
		public static Dictionary<int, string> Dict = new Dictionary<int, string> {
		{
			1,
			"Islam"
		},
		{
			2,
			"Kristen Protestan"
		},
		{
			3,
			"Katholik"
		},
		{
			4,
			"Hindu"
		},
		{
			5,
			"Buda"
		},
		{
			6,
			"Khong Hu Cu"
		}
	};

		public const int Islam = 1;

		public const int Kristen = 2;

		public const int Katholik = 3;

		public const int Hindu = 4;

		public const int Buda = 5;

		public const int KhongHuCu = 6;
	}
}
