﻿using System;
using System.Collections.Generic;

namespace Swg.Models.Constants
{
    public class JenisDataConstant
    {
        public Dictionary<int, string> Dict = new Dictionary<int, string>()
        {
            {Tanggal,"Tipe Data Tanggal" },
            {Bilangan, "Tipe Data Bilangan" },
            {Karakter,"Tipe Data Karakter" }
        };
        public const int Tanggal = 1;
        public const int Bilangan = 2;
        public const int Karakter = 3;
    }
}
