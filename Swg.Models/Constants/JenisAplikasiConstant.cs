﻿using System;
using System.Collections.Generic;

namespace Swg.Models.Constants
{
    public class JenisAplikasiConstant
    {
        public Dictionary<int, string> Dict = new Dictionary<int, string>()
        {
            {Web,"Aplikasi Web" },
            {Dekstop, "Aplikasi Desaktop" },
            {MobileAndroid,"Aplikasi Android" },
            {MobileIos,"Aplikasi IOS" },
            {FrontendWeb,"Aplikasi Frontend Web" },
        };
        public const int Web = 1;
        public const int Dekstop = 2;
        public const int MobileAndroid = 3;
        public const int MobileIos = 4;
        public const int FrontendWeb = 5;
    }
}
