﻿using System;
using System.Collections.Generic;

namespace Swg.Models.Constants
{
    public class UnitConstant
    {
        public static Dictionary<int, string> DictUnit = new Dictionary<int, string>();

        public static int Tk;
        public static int Sd;
        public static int Smp;
        public static int Sma;
        public static int Tpq;

        public static int DIR;
        public static int PGD;
        public static int PML;
        public static int ALQ;
        public static int PER;
        public static int PMG;
        public static int PMB;
        public static int MDC;
        public static int KAM;
        public static int DKM;
        public static int ZIS;
        public static int AKT;
        public static int FNC;
    }
}
