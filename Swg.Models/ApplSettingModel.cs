﻿using System;
using Microsoft.AspNetCore.Http;

namespace Swg.Models
{
    public class ApplSettingModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int ValueType { get; set; }
        public string ValueDate { get; set; }
        public double ValueNumber { get; set; }
        public string ValueString { get; set; }
        public IFormFile FileUpload { get; set; }
    }
}
