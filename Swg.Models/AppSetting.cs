﻿using System;
using System.IO;

namespace Swg.Models
{
    public class AppSetting
    {
        public static string Secret { get; set; }
        public static string ConnectionString { get; set; }

        public static string OrgNama { get; set; }
        public static string OrgNamaPendek { get; set; }
        public static string OrgAlamat { get; set; }
        public static string OrgKodePos { get; set; }
        public static double OrgLatitude { get; set; }
        public static double OrgLongitude { get; set; }
        public static string OrgTelpon { get; set; }
        public static string OrgFaximili { get; set; }
        public static string OrgWeb { get; set; }
        public static string OrgEmail { get; set; }
        public static string OrgEmailPassword { get; set; }
        public static string OrgEmailSmtpServer { get; set; }
        public static double OrgEmailSmtpPort { get; set; }

        public static string BasePath { get; set; }
        public static string BaseUrl { get; set; }
        public static string BasePathUrl { get; set; }

        public static string PathApplFile { get; set; }
        public static string PathApplUser { get; set; }
        public static string PathApplImage { get; set; }
        public static string PathApplVideo { get; set; }
        public static string PathSiswaRapor { get; set; }
        public static string PathSiswaFoto { get; set; }
        public static string PathEkskulUnit { get; set; }
        public static string PathKegiatanUnit { get; set; }
        public static string PathGaleriUnit { get; set; }
        public static string PathPrestasiSekolah { get; set; }
        public static string PathFasilitasSekolah { get; set; }
        public static string PathKegiatanSekolah { get; set; }
        public static string PathGaleriSekolah { get; set; }
        public static string PathPpdbPrestasi { get; set; }
        public static string PathPpdbBerkas { get; set; }
        public static string PathPpdbCicil { get; set; }
        public static string PathElearning { get; set; }
        public static string PathTestimoni { get; set; }

        public static string PathUrlApplFile { get; set; }
        public static string PathUrlApplUser { get; set; }
        public static string PathUrlApplImage { get; set; }
        public static string PathUrlApplVideo { get; set; }
        public static string PathUrlSiswaRapor { get; set; }
        public static string PathUrlSiswaFoto { get; set; }
        public static string PathUrlKegiatanUnit { get; set; }
        public static string PathUrlEkskulUnit { get; set; }
        public static string PathUrlGaleriUnit { get; set; }
        public static string PathUrlPrestasiSekolah { get; set; }
        public static string PathUrlFasilitasSekolah { get; set; }
        public static string PathUrlKegiatanSekolah { get; set; }
        public static string PathUrlGaleriSekolah { get; set; }
        public static string PathUrlPpdbPrestasi { get; set; }
        public static string PathUrlPpdbBerkas { get; set; }
        public static string PathUrlPpdbCicil { get; set; }
        public static string PathUrlElearning { get; set; }
        public static string PathUrlTestimoni { get; set; }
        public void GeneratePath()
        {

            //if (!Directory.Exists(AppSetting.BasePath))
            //    Directory.CreateDirectory(AppSetting.BasePath);
            if (!Directory.Exists(AppSetting.PathApplFile))
                Directory.CreateDirectory(AppSetting.PathApplFile);
            if (!Directory.Exists(AppSetting.PathApplUser))
                Directory.CreateDirectory(AppSetting.PathApplUser);
            if (!Directory.Exists(AppSetting.PathApplImage))
                Directory.CreateDirectory(AppSetting.PathApplImage);
            if (!Directory.Exists(AppSetting.PathApplVideo))
                Directory.CreateDirectory(AppSetting.PathApplVideo);
            if (!Directory.Exists(AppSetting.PathSiswaRapor))
                Directory.CreateDirectory(AppSetting.PathSiswaRapor);
            if (!Directory.Exists(AppSetting.PathSiswaFoto))
                Directory.CreateDirectory(AppSetting.PathSiswaFoto);
            if (!Directory.Exists(AppSetting.PathEkskulUnit))
                Directory.CreateDirectory(AppSetting.PathEkskulUnit);
            if (!Directory.Exists(AppSetting.PathKegiatanUnit))
                Directory.CreateDirectory(AppSetting.PathKegiatanUnit);
            if (!Directory.Exists(AppSetting.PathGaleriUnit))
                Directory.CreateDirectory(AppSetting.PathGaleriUnit);
            //if (!Directory.Exists(AppSetting.PathPrestasiSekolah))
            //    Directory.CreateDirectory(AppSetting.PathPrestasiSekolah);
            //if (!Directory.Exists(AppSetting.PathFasilitasSekolah))
            //    Directory.CreateDirectory(AppSetting.PathFasilitasSekolah);
            if (!Directory.Exists(AppSetting.PathKegiatanSekolah))
                Directory.CreateDirectory(AppSetting.PathKegiatanSekolah);
            if (!Directory.Exists(AppSetting.PathGaleriSekolah))
                Directory.CreateDirectory(AppSetting.PathGaleriSekolah);
            if (!Directory.Exists(AppSetting.PathPpdbPrestasi))
                Directory.CreateDirectory(AppSetting.PathPpdbPrestasi);
            if (!Directory.Exists(AppSetting.PathPpdbBerkas))
                Directory.CreateDirectory(AppSetting.PathPpdbBerkas);
            if (!Directory.Exists(AppSetting.PathPpdbCicil))
                Directory.CreateDirectory(AppSetting.PathPpdbCicil);
            if (!Directory.Exists(AppSetting.PathElearning))
                Directory.CreateDirectory(AppSetting.PathElearning);
            if (!Directory.Exists(AppSetting.PathTestimoni))
                Directory.CreateDirectory(AppSetting.PathTestimoni);

        }

    }
}
