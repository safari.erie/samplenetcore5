﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Models
{
    public class AppSettingModel
    {
        public string Secret { get; set; }
        public string ConnectionString { get; set; }

        public string BasePath { get; set; }
        public string BaseUrl { get; set; }
        public string BasePathUrl { get; set; }

        public string PathApplFile { get; set; }
        public string PathApplUser { get; set; }
        public string PathApplImage { get; set; }
        public string PathApplVideo { get; set; }
        public string PathSiswaRapor { get; set; }
        public string PathSiswaFoto { get; set; }
        public string PathEkskulUnit { get; set; }
        public string PathKegiatanUnit { get; set; }
        public string PathGaleriUnit { get; set; }
        public string PathPrestasiSekolah { get; set; }
        public string PathFasilitasSekolah { get; set; }
        public string PathKegiatanSekolah { get; set; }
        public string PathGaleriSekolah { get; set; }
        public string PathPpdbPrestasi { get; set; }
        public string PathPpdbBerkas { get; set; }
        public string PathPpdbCicil { get; set; }
        public string PathElearning { get; set; }
        public string PathTestimoni { get; set; }

    }
}
